/* This file is auto generated, version 1 */
/* SMP */
#define UTS_MACHINE "x86_64"
#define UTS_VERSION "#1 SMP Debian 4.19.37-5+deb10u1 (2019-07-19)"
#define LINUX_COMPILE_BY "debian-kernel"
#define LINUX_COMPILE_HOST "lists.debian.org"
#define LINUX_COMPILER "gcc version 8.3.0 (Debian 8.3.0-6)"
