# -*- encoding: utf-8 -*-
# stub: gist 5.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "gist".freeze
  s.version = "5.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Conrad Irwin".freeze, "\u2608king".freeze]
  s.date = "2018-08-01"
  s.description = "Provides a single function (Gist.gist) that uploads a gist.".freeze
  s.email = ["conrad.irwin@gmail.com".freeze, "rkingist@sharpsaw.org".freeze]
  s.executables = ["gist-paste".freeze]
  s.files = ["LICENSE.MIT".freeze, "README.md".freeze, "bin/gist-paste".freeze, "lib/gist.rb".freeze, "spec/auth_token_file_spec.rb".freeze, "spec/clipboard_spec.rb".freeze, "spec/ghe_spec.rb".freeze, "spec/gist_spec.rb".freeze, "spec/proxy_spec.rb".freeze, "spec/rawify_spec.rb".freeze, "spec/shorten_spec.rb".freeze, "spec/spec_helper.rb".freeze]
  s.homepage = "https://github.com/defunkt/gist".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Just allows you to upload gists".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, [">= 0"])
      s.add_development_dependency(%q<ronn>.freeze, [">= 0"])
      s.add_development_dependency(%q<webmock>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec>.freeze, ["> 3"])
    else
      s.add_dependency(%q<rake>.freeze, [">= 0"])
      s.add_dependency(%q<ronn>.freeze, [">= 0"])
      s.add_dependency(%q<webmock>.freeze, [">= 0"])
      s.add_dependency(%q<rspec>.freeze, ["> 3"])
    end
  else
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<ronn>.freeze, [">= 0"])
    s.add_dependency(%q<webmock>.freeze, [">= 0"])
    s.add_dependency(%q<rspec>.freeze, ["> 3"])
  end
end
