# Generated file - DO NOT EDIT.  Edit config.py.in instead.

prefix="/usr"
datadir="/usr/share"
localedir=datadir+"/locale"
pkgdatadir="/usr/share/cinnamon-screensaver"
libdir="/usr/lib/x86_64-linux-gnu"
libexecdir="/usr/lib/x86_64-linux-gnu/cinnamon-screensaver"
PACKAGE="cinnamon-screensaver"
VERSION="3.8.2"
GETTEXT_PACKAGE="cinnamon-screensaver"
