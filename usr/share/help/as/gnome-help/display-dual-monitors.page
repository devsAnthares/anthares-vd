<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="as">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>টিফানি এন্টপলস্কি</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set up an additional monitor.</desc>
  </info>

<title>Connect another monitor to your computer</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Set up an additional monitor</title>
  <p>To set up an additional monitor, connect the monitor to your computer. If
  your system does not recognize it immediately, or you would like to adjust
  the settings:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Display Arrangement</gui> diagram, drag your displays to
      the relative positions you want.</p>
    </item>
    <item>
      <p>Click <gui>Primary Display</gui> to choose your primary display.</p>

      <note>
        <p>The primary display is the one with the
        <link xref="shell-introduction">top bar</link>, and where the
        <gui>Activities</gui> overview is shown.</p>
      </note>
    </item>
    <item>
      <p>Select the resolution or scale, and choose the orientation.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. The new settings will be applied for 20
      seconds before reverting back. That way, if you cannot see anything with
      the new settings, your old settings will be automatically restored. If
      you are happy with the new settings, click <gui>Keep Changes</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Display modes</title>
    <p>With two screens, these display modes are available:</p>
    <list>
      <item><p><gui>Join Displays:</gui> screen edges are joined so things can
      pass from one display to another.</p></item>
      <item><p><gui>Mirror:</gui> the same content is shown on two displays,
      with the same resolution and orientation for both.</p></item>
      <item><p><gui>Single Display:</gui> only one display is configured,
      effectively turning off the other one. For instance, an external monitor
      connected to a docked laptop with the lid closed would be the single
      configured display.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Adding more than one monitor</title>
    <p>With more than two screens, <gui>Join Displays</gui> is the only mode
    available.</p>
    <list>
      <item>
        <p>Use the drop-down menu to choose which screen to configure.</p>
      </item>
      <item>
        <p>Drag the screens to the desired relative positions.</p>
      </item>
      <item>
        <p>Each screen can be turned off or on using the <gui>ON | OFF</gui>
        switch.</p>
      </item>
    </list>

</section>
</page>
