<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="files-search" xml:lang="as">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Locate files based on file name and type.</desc>
  </info>

  <title>ফাইলসমূহ সন্ধান কৰক</title>

  <p>You can search for files based on their name or file type directly
  within the file manager.</p>

  <links type="topic" style="linklist">
    <title>অন্য সন্ধান এপ্লিকেচনসমূহ</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>সন্ধান কৰক</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>যদি আপুনি যানে যে আপুনি বিচৰা ফাইলসমূহ এটা নিৰ্দিষ্ট ফোল্ডাৰত আছে, সেই ফোল্ডাৰলৈ যাওক।</p>
    </item>
    <item>
      <p>Type a word or words that you know appear in the file name, and they
      will be shown in the search bar. For example, if you name all your
      invoices with the word “Invoice”, type <input>invoice</input>. Words are
      matched regardless of case.</p>
      <note>
        <p>Instead of typing words directly to bring up the search bar, you can
	click the magnifying glass in the toolbar, or press
        <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>অৱস্থান আৰু ফাইল ধৰণ ব্যৱহাৰ কৰি আপুনি ফলাফল সংক্ষিপ্ত কৰিব পাৰিব।</p>
      <list>
        <item>
          <p>আপোনাৰ সন্ধানৰ ফলাফলসমূহ আপোনাৰ <file>ঘৰ</file> ফোল্ডাৰলৈ সীমিত কৰিবলে <gui>ঘৰ</gui> ক্লিক কৰক, অথবা সকলো ঠাইত সন্ধান কৰিবলৈ <gui>সকলো ফাইল</gui> ক্লিক কৰক।</p>
        </item>
        <item>
          <p>Click the <gui>+</gui> button and pick a <gui>File Type</gui> from
	  the drop-down list to narrow the search results based on file type.
	  Click the <gui>×</gui> button to remove this option and widen the
	  search results.</p>
        </item>
      </list>
    </item>
    <item>
      <p>আপুনি সন্ধানৰ ফলাফলৰ পৰা ফাইলসমূহক খোলিব, কপি কৰিব, মচিব, অথবা অন্য কাম কৰিব পাৰিব, ঠিক যেনেকৈ আপুনি ফাইল ব্যৱস্থাপকত যিকোনো ফোল্ডাৰত কৰে।</p>
    </item>
    <item>
      <p>সন্ধান প্ৰস্থান কৰি ফোল্ডাৰলৈ উভতি যাবলৈ সঁজুলিবাৰত মেগ্নিফায়িং গ্লাচত ক্লিক কৰক।</p>
    </item>
  </steps>

</page>
