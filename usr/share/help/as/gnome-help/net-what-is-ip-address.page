<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="as">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>এটা IP ঠিকনা আপোনাৰ কমপিউটাৰৰ বাবে এটা ফোন নম্বৰৰ নিচিনা।</desc>
  </info>

  <title>এটা IP ঠিকনা কি?</title>

  <p>“IP address” stands for <em>Internet Protocol address</em>, and each
  device that is connected to a network (like the internet) has one.</p>

  <p>An IP address is similar to your phone number. Your phone number is a
  unique set of numbers that identifies your phone so that other people can
  call you. Similarly, an IP address is a unique set of numbers that identifies
  your computer so that it can send and receive data with other computers.</p>

  <p>বৰ্তমানে, বেছিৰভাগ IP ঠিকনায় সংখ্যাবোৰৰ চাৰিটা সংহতি অন্তৰ্ভুক্ত কৰে, প্ৰত্যকটো এটা পিৰিয়ড (.) দ্বাৰা পৃথকিত। <code>192.168.1.42</code> এটা IP ঠিকনাৰ উদাহৰণ।</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
