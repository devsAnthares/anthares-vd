<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-removedrive" xml:lang="ru">

  <info>
    <link type="guide" xref="files#removable"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-08" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Извлечение или отключение флэш-диска USB, CD, DVD и других устройств.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Безопасное извлечение съёмного носителя</title>

  <p>При использовании внешних носителей информации, таких как USB-накопители перед отсоединением их от компьютера нужно выполнить безопасное извлечение. Если вы просто отсоедините носитель, существует риск того, что он в это время ещё используется каким-нибудь приложением. Это может привести к потере или повреждению файлов. При использовании оптического диска, например, CD или DVD, ту же процедуру можно выполнить для извлечения лотка с диском из привода.</p>

  <steps>
    <title>Чтобы извлечь съёмный носитель:</title>
    <item>
      <p>From the <gui xref="shell-introduction#activities">Activities</gui> overview,
      open <app>Files</app>.</p>
    </item>
    <item>
      <p>Найдите устройство в боковой панели. Рядом с его именем должен быть небольшой значок для извлечения устройства. Нажмите на значок для безопасного удаления или извлечения устройства.</p>
      <p>Можно также нажать правой кнопкой на имени устройства в боковой панели и выбрать <gui>Извлечь</gui>.</p>
    </item>
  </steps>

  <section id="remove-busy-device">
    <title>Безопасное извлечение используемого устройства</title>

  <p>Если любой из файлов на устройстве открыт и используется приложением, безопасно извлечь устройство будет невозможно. Будет выведено сообщение <gui>Том занят</gui>. Чтобы безопасно извлечь устройство:</p>

  <steps>
    <item><p>Нажмите <gui>Отменить</gui>.</p></item>
    <item><p>Закройте все файлы на устройстве.</p></item>
    <item><p>Нажмите значок извлечения для безопасного отключения или извлечения устройства.</p></item>
    <item><p>Можно также нажать правой кнопкой на имени устройства в боковой панели и выбрать <gui>Извлечь</gui>.</p></item>
  </steps>

  <note style="warning"><p>Также можно выбрать <gui>Всё равно извлечь</gui> чтобы извлечь устройство, не закрывая файлов. Это может привести к ошибкам в тех приложениях, с помощью которых были открыты эти файлы.</p></note>

  </section>

</page>
