<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="ru">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Маловероятно, что вирусы заразят ваш компьютер, но они могут заразить компьютеры других людей через электронную почту.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Нужно ли сканировать свою электронную почту на наличие вирусов?</title>

  <p>Вирусы — это программы, вызывающие проблемы при проникновении на компьютер. Один из типичных способов их попадания на компьютер — через электронную почту.</p>

  <p>Viruses that can affect computers running Linux are quite rare, so you are
  <link xref="net-antivirus">unlikely to get a virus through email or
  otherwise</link>. If you receive an email with a virus hidden in it, it will
  probably have no effect on your computer. As such, you probably don’t need to
  scan your email for viruses.</p>

  <p>You may, however, wish to scan your email for viruses in case you happen
  to forward a virus from one person to another. For example, if one of your
  friends has a Windows computer with a virus and sends you a virus-infected
  email, and you then forward that email to another friend with a Windows
  computer, then the second friend might get the virus too. You could install
  an anti-virus application to scan your emails to prevent this, but it’s
  unlikely to happen and most people using Windows and Mac OS have anti-virus
  software of their own anyway.</p>

</page>
