<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="ru">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Возможно, выполняется загрузка, у вас плохое соединение, или это занятое время дня.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Интернет работает медленно</title>

  <p>Если скорость работы с Интернетом низкая, это может быть вызвано рядом причин.</p>

  <p>Попробуйте закрыть веб-браузер и снова его запустить, отключиться от Интернета и снова подключиться. (При этом сбрасываются к исходным значениям многие параметры, которые могли быть причиной медленной работы.)</p>

  <list>
    <item>
      <p><em style="strong">Занятое время дня</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">Загрузка нескольких вещей одновременно</em></p>
      <p>Если вы или кто-то другой, использующий ваше соединение с Интернетом, загружает несколько файлов одновременно или смотрит видео, скорость соединения может оказаться недостаточной для удовлетворения всех потребностей. В этом случае вы ощутите снижение скорости.</p>
    </item>
    <item>
      <p><em style="strong">Неустойчивое соединение</em></p>
      <p>Некоторые соединения ненадёжны, особенно временные или в зонах, обслуживающих множество пользователей. Если вы находитесь в кафе или конференц-центре, соединение может быть слишком занятым или просто ненадёжным.</p>
    </item>
    <item>
      <p><em style="strong">Слабый сигнал беспроводной сети</em></p>
      <p>If you’re connected to the internet by wireless (wifi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">Используется более медленное мобильное соединение</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Проблема в веб-браузере</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
