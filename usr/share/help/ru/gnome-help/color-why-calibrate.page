<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-why-calibrate" xml:lang="ru">

  <info>
    <link type="guide" xref="color#calibration"/>
    <desc>Калибровка важна, если вы заботитесь о точности передачи цветов на экране или при печати.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Зачем выполнять калибровку самостоятельно?</title>

  <p>Универсальные профили, как правило, имеют плохое качество. Когда производитель создаёт новую модель, он просто берёт несколько образцов с технологической линии и усредняет их показатели:</p>

  <media its:translate="no" type="image" src="figures/color-average.png">
    <p its:translate="yes">Усреднённые профили</p>
  </media>

  <p>Панели дисплеев довольно сильно отличаются от устройства к устройству и значительно меняют свои характеристики по мере старения дисплея. С принтерами всё ещё сложнее, так как даже изменение типа или плотности бумаги может изменить данные характеризации и сделать профиль неточным.</p>

  <p>Наилучший способ обеспечить точность используемого вами профиля — это выполнить калибровку самостоятельно или воспользоваться услугами специализированной организации по предоставлению вам профиля, основанного на точных данных характеризации.</p>

</page>
