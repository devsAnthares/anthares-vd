<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="ru">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Объединение данных о контакте из различных источников.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Связывание и разделение контактов</title>

<section id="link-contacts">
  <title>Связывание контактов</title>

  <p>Дублированные контакты из локальной адресной книги и сетевых учётных записей можно связать в единую запись в <app>Контактах</app>. Это помогает держать адресную книгу в порядке, храня все данные об одном контакте в одном месте.</p>

  <steps>
    <item>
      <p>Включите <em>режим выделения</em>, нажав кнопку с галочкой над списком контактов.</p>
    </item>
    <item>
      <p>A checkbox will appear next to each contact. Tick the checkboxes next
      to the contacts that you want to merge.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Связать</gui> чтобы связать выбранные контакты.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Разделение контактов</title>

  <p>Если вы случайно связали контакты, которые не нужно было связывать, то контакты можно разделить.</p>

  <steps>
    <item>
      <p>Выберите в левой панели контакт, который нужно разделить.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Редактировать</gui> в верхнем правом углу окна <app>Контакты</app>.</p>
    </item>
    <item>
      <p>Press <gui style="button">Linked Accounts</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Разделить</gui>, чтобы разделить записи контакта.</p>
    </item>
    <item>
      <p>Close the window once you have finished unlinking the entries.</p>
    </item>
    <item>
      <p>Нажмите <gui style="button">Готово</gui> чтобы завершить добавление данных.</p>
    </item>
  </steps>

</section>

</page>
