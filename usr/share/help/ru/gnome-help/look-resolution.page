<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шобха Тьяги (Shobha Tyagi)</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Изменение разрешения экрана и его ориентации (поворота).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Change the resolution or orientation of the screen</title>

  <p>Изменяя <em>разрешение экрана</em>, можно настроить, насколько крупно (или насколько подробно) показываются элементы изображения на экране. А изменяя <em>ориентацию</em> экрана (если, например, у вас есть вращающийся экран), можно настроить, какая сторона экрана будет верхней.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Если у вас есть несколько мониторов, и они не отражаются зеркально, то к каждому монитору можно применить отдельные настройки. Выберите монитор в области предварительного просмотра.</p>
    </item>
    <item>
      <p>Select the resolution or scale, and choose the orientation.</p>
    </item>
    <item>
      <p>Нажмите <gui>Применить</gui>. Новые настройки сначала применяются в течение 20 секунд, и если эффект применения новых настроек вас не устроит, откат к старым настройкам произойдёт автоматически. Если же всё хорошо, нажмите <gui>Оставить эту конфигурацию</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Разрешение</title>

  <p>The resolution is the number of pixels (dots on the screen) in each
  direction that can be displayed. Each resolution has an <em>aspect
  ratio</em>, the ratio of the width to the height. Wide-screen displays use a
  16∶9 aspect ratio, while traditional displays use 4∶3. If you choose a
  resolution that does not match the aspect ratio of your display, the screen
  will be letterboxed to avoid distortion, by adding black bars to the top and
  bottom or both sides of the screen.</p>

  <p>Выбрать нужное разрешение можно из выпадающего списка <gui>Разрешение</gui>. Если выбрано разрешение, не подходящее для вашего экрана, изображение может стать <link xref="look-display-fuzzy">размытым или мозаичным</link>.</p>

</section>

<section id="native">
  <title>Native Resolution</title>

  <p>The <em>native resolution</em> of a laptop screen or LCD monitor is the
  one that works best: the pixels in the video signal will line up precisely
  with the pixels on the screen. When the screen is required to show other
  resolutions, interpolation is necessary to represent the pixels, causing a
  loss of image quality.</p>

</section>

<section id="scale">
  <title>Scale</title>

  <p>The scale setting increases the size of objects shown on the screen to
  match the density of your display, making them easier to read. Choose from
  <gui>100%</gui>, <gui>200%</gui>, or <gui>300%</gui>.</p>

</section>

<section id="orientation">
  <title>Orientation</title>

  <p>On some laptops and monitors, you can physically rotate the screen in many
  directions. Click <gui>Orientation</gui> in the panel and choose from
  <gui>Landscape</gui>, <gui>Portrait Right</gui>, <gui>Portrait Left</gui>, or
  <gui>Landscape (flipped)</gui>.</p>

</section>

</page>
