<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="ru">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Управление подписями к значкам в менеджере файлов.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Параметры вкладки <gui>Отображение</gui> менеджера файлов</title>

<p>Пользователь может настраивать порядок сведений, показываемый под именами значков менеджера файлов. Нажмите <gui>Nautilus</gui> в верхней панели, выберите пункт <gui>Параметры</gui> и вкладку <gui>Просмотр</gui>.</p>

<section id="icon-captions">
  <title>Подписи к значкам</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Значки с подписями в менеджере файлов</p>
  </media>
  <p>При просмотре в виде значков можно выбрать отображение дополнительной информации о файлах и папках в подписях к значкам. Это полезно, например, если вам часто необходимо знать, кто является владельцем файла или когда файл был изменён в последний раз.</p>
  <p>You can zoom in a folder by clicking the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choosing a zoom level with the slider. As you zoom in, the
  file manager will display more and more information in captions. You can
  choose up to three things to show in captions. The first will be displayed at
  most zoom levels. The last will only be shown at very large sizes.</p>
  <p>В подписях к значкам можно отобразить ту же информацию, что и в столбцах при просмотре в виде списка. Подробнее смотрите <link xref="nautilus-list"/>.</p>
</section>

<section id="list-view">

  <title>List View</title>

  <p>When viewing files as a list, you can <gui>Navigate folders in a
  tree</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
