<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="ru">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Несколько советов по использования справочного руководства.</desc>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Об этом руководстве</title>
<p>This guide is designed to give you a tour of the features of your desktop, answer your computer-related questions, and provide tips on using your computer more effectively. We’ve tried to make this guide as easy to use as possible:</p>

<list>
  <item><p>The guide is sorted into small, task-oriented topics — not chapters. This means that you don’t need to skim through an entire manual to find the answer to your questions.</p></item>
  <item><p>Related items are linked together. “See Also” links at the bottom of some pages will direct you to related topics. This makes it easy to find similar topics that might help you perform a certain task.</p></item>
  <item><p>Руководство содержит встроенный поиск. Панель наверху окна браузера справки — это <em>панель поиска</em>, в которой по мере набора поискового запроса будут появляться подходящие результаты.</p></item>
  <item><p>The guide is constantly being improved. Although we attempt to provide you with a comprehensive set of helpful information, we know we won’t answer all of your questions here. We will keep adding more information to make things more helpful, though.</p></item>
</list>

<p>Спасибо за то, что вы нашли время прочитать это справочное руководство. Мы искренне надеемся, что вам никогда не придётся им пользоваться.</p>

<p>-- Проект документирования GNOME</p>
</page>
