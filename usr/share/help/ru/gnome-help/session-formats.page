<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Выберите региональный формат даты и времени, чисел, валюты и единиц измерения.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Изменение форматов даты и единиц измерения</title>

  <p>Можно настроить форматы, используемые для дат, времени, чисел, валюты и единиц измерения в соответствии со стандартами, принятыми в вашем регионе.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Region &amp; Language</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Язык и регион</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите <gui>Форматы</gui>.</p>
    </item>
    <item>
      <p>Выберите регион и язык, формат которых наиболее близок к вашему. Если вашего региона и языка нет в списке, нажмите <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> в нижней части списка, чтобы выбрать регион и язык из всех доступных регионов и языков.</p>
    </item>
    <item>
      <p>Нажмите кнопку <gui style="button">Готово</gui>.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Справа от списка показаны примеры форматов дат и других данных. Хотя этого и не видно из примеров, выбор региона также влияет на день, с которого в календарях начинается неделя.</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
