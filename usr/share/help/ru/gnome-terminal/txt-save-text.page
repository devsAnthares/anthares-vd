<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="txt-save-text" xml:lang="ru">

  <info>
    <revision version="0.1" date="2013-02-17" status="candidate"/>
    <link type="guide" xref="index#textoutput"/>

    <credit type="author copyright">
      <name>С. Синдху (Sindhu S)</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Save <app>Terminal</app> output to a file.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Сергей В. Миронов</mal:name>
      <mal:email>sergo@bk.ru</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Save contents</title>

  <p>You can save all the output from a <app>Terminal</app> tab or window to a
  text file. This may be useful if you have to submit the terminal output as
  debugging information.</p>

  <steps>
    <item>
      <p>Go to <gui style="menu">File</gui>.</p>
    </item>
    <item>
      <p>Select <gui style="menuitem">Save Contents…</gui>.</p>
    </item>
    <item>
      <p>Choose your desired directory and enter a filename.</p>
    </item>
   <item>
      <p>Click <gui style="button">Save</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You may optionally input a file extension along with the filename, for
    example <input>.txt</input>. This may be useful if you plan to view the
    file when using another operating system.</p>
  </note>

</page>
