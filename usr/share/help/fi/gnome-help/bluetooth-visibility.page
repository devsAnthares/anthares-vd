<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="fi">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Onko tietokoneesi muiden laitteiden löydettävissä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

  <title>Mitä Bluetooth-näkyvyys tarkoittaa?</title>

  <p>Bluetooth-näkyvyydellä tarkoitetaan yksinkertaisesti sitä, että muut laitteet voivat havaita tietokoneesi, kun niillä etsitään Bluetooth-laitteita. Kun Bluetooth on käytössä ja <gui>Bluetooth</gui>-paneeli avoinna, tietokoneesi näkyy kaikille muille laitteille, jotka ovat Bluetooth-kantaman sisällä, jolloin ne voivat yrittää ottaa yhteyden tietokoneeseesi.</p>

  <note style="tip">
    <p>You can <link xref="sharing-displayname">change</link> the name your
    computer displays to other devices.</p>
  </note>

  <p>Kun olet <link xref="bluetooth-connect-device">muodostanut yhteyden laitteeseen</link>, kyseisen laitteen tai tietokoneesi ei tarvitse enää olla näkyvissä ollakseen yhteydessä toisiinsa.</p>

</page>
