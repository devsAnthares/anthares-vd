<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="fi">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Vinkkejä kuten “Älä anna akun varauksen purkautua liikaa”.</desc>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Hyödynnä akkusi täysimittaisesti</title>

<p>As laptop batteries age, they get worse at storing charge and their capacity
gradually decreases. There are a few techniques that you can use to prolong
their useful lifetime, although you should not expect a big difference.</p>

<list>
  <item>
    <p>Do not let the battery run all the way down. Always recharge
    <em>before</em> the battery gets very low, although most batteries have
    built-in safeguards to prevent the battery running too low. Recharging when
    it is only partially discharged is more efficient, but recharging when it
    is only slightly discharged is worse for the battery.</p>
  </item>
  <item>
    <p>Heat has a detrimental effect on the charging efficiency of the battery.
    Do not let the battery get any warmer than it has to.</p>
  </item>
  <item>
    <p>Batteries age even if you leave them in storage. There is little
    advantage in buying a replacement battery at the same time as you get the
    original battery — always buy replacements when you need them.</p>
  </item>
</list>

<note>
  <p>Nämä ohjeet on suunniteltu litiumioni-akuille (Li-Ion), jollainen on käytössä suurimmassa osassa tietokoneista. Muun tyyppisiin akkuihin nämä ohjeet eivät välttämättä sovellu.</p>
</note>

</page>
