<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-lowpower" xml:lang="fi">

  <info>
    <link type="guide" xref="power#faq"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Jos sallit akun tyhjentyä täysin, akku kärsii siitä.</desc>
    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Miksi tietokone sammui, kun akun varaustaso oli 10 prosenttia?</title>

<p>When the charge level of the battery gets too low, your computer will
automatically turn off. It does this to make sure that the battery does not
completely discharge, since this is bad for
the battery. If the battery just ran out, the computer would not have time to
shut down properly either.</p>

<p>Bear in mind that when your computer automatically turns off, your
applications and documents <em>are not</em> saved. To avoid losing your
work, save it before the battery gets too low.</p>

</page>
