<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="fi">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Paina ja pidä hiiren vasenta nappia pohjassa painaaksesi hiiren oikeaa nappia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

  <title>Simuloi hiiren oikean napin painallusta</title>

  <p>Voit simuloida hiiren oikean painikkeen painallusta pitämällä hiiren vasenta painiketta pohjassa hetken. Tämä on hyödyllistä, jos sinun on vaikea liikuttaa tiettyjä sormia tai jos hiiressäsi on vain yksi painike.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Asetukset</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Click Assist</gui> in the <gui>Pointing &amp; Clicking</gui>
      section.</p>
    </item>
    <item>
      <p>Switch <gui>Simulated Secondary Click</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <p>You can change how long you must hold down the left mouse button before it
  is registered as a right click by changing the <gui>Acceptance
  delay</gui>.</p>

  <p>To right-click with simulated secondary click, hold down the left mouse
  button where you would normally right-click, then release. The pointer fills
  with a different color as you hold down the left mouse button. Once it will
  change this color entirely, release the mouse button to right-click.</p>

  <p>Some special pointers, such as the resize pointers, do not change colors.
  You can still use simulated secondary click as normal, even if you do not get
  visual feedback from the pointer.</p>

  <p>Jos käytät <link xref="mouse-mousekeys">hiirinäppäimiä</link>, hiiren oikean painikkeen painallus onnistuu pitämällä hetken pohjassa numeronäppäintä <key>5</key>.</p>

  <note>
    <p>In the <gui>Activities</gui> overview, you are always able to long-press
    to right-click, even with this feature disabled. Long-press works slightly
    differently in the overview: you do not have to release the button to
    right-click.</p>
  </note>

</page>
