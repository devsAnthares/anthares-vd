<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="fi">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vinkkejä tietokoneen virrankäytön vähentämiseksi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

  <title>Käytä vähemmän virtaa ja säästä siten akkua</title>

  <p>Tietokoneet käyttävät paljon sähköä. Yksinkertaisilla energiansäästövinkeillä voit pienentää sähkölaskuasi ja auttaa säästämään ympäristöä.</p>

<section id="general">
  <title>Yleisiä neuvoja</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Aseta tietokone valmiustilaan</link> kun et käytä sitä. Koneen virrankulutus vähenee huomattavasti, mutta kone käynnistyy silti nopeasti.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Sammuta</link> tietokone, kun et käytä sitä vähään aikaan. Jotkut saattavat olla huolissaan, että tietokoneen jatkuva sammuttaminen kuluttaa koneen osia, mutta se ei pidä paikkaansa.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="display-blank">automatically blank the screen</link>
    after a certain time, reduce the <link xref="display-brightness">screen
    brightness</link>, and have the computer
    <link xref="power-autosuspend">automatically suspend</link> if you have not
    used it for a certain period of time.</p>
  </item>
  <item>
    <p>Turn off any external devices (like printers and scanners) when you are
    not using them.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Kannettavat, miniläppärit ja muut akkua käyttävät laitteet</title>

 <list>
   <item>
     <p>Vähennä <link xref="display-brightness">näytön kirkkautta</link>. Näytön pitäminen päällä muodostaa suuren osan kannettavan tietokoneen energiankulutuksesta.</p>
     <p>Useimpien kannettavien näppäimistöissä on valoisuuden säätöä varten omat näppäimet (tai pikanäppäimet).</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless or Bluetooth cards. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>Joistain koneista löytyy kytkin Internet-yhteyden sammuttamiseen kun taas toisissa koneissa Internet-yhteys sammutetaan näppäimistön avulla. Käynnistä Internet-yhteys tarvittaessa uudelleen.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Lisävinkkejä</title>

 <list>
   <item>
     <p>Vähennä taustalla pyörivien tehtävien määrää. Tietokoneet käyttävät sitä enemmän virtaa, mitä enemmän tehtäviä niillä on hoidettavana.</p>
     <p>Most of your running applications do very little when you are not
     actively using them. However, applications that frequently grab data from
     the internet or play music or movies can impact your power consumption.</p>
   </item>
 </list>

</section>

</page>
