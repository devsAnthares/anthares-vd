<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="fi">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Tiedostojen avaaminen, suorittaminen ja roskakorin toiminta.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Tiedostonhallinnan toiminnan asetukset</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click
<gui>Files</gui> in the top bar, pick <gui>Preferences</gui> and select the <gui>Behavior</gui> tab.</p>

<section id="behavior">
<title>Toiminta</title>
<terms>
 <item>
  <title><gui>Yksi napsautus kohteiden avaamiseksi</gui></title>
  <title><gui>Kaksoisnapsautus kohteiden avaamiseksi</gui></title>
  <p>Jos käytetään oletusasetuksia, yksi napsautus valitsee tiedoston ja kaksoisnapsautus avaa sen. Asetuksia muuttamalla voit asettaa tiedostot ja kansiot avautumaan yhdellä napsautuksella. Kun tämä tila on käytössä, tiedostoja voi valita pitämällä <key>Ctrl</key>-näppäintä pohjassa.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Suoritettavat tekstitiedostot</title>
 <p>An executable text file is a file that contains a program that you can run
 (execute). The <link xref="nautilus-file-properties-permissions">file
 permissions</link> must also allow for the file to run as a program. The most
 common are <sys>Shell</sys>, <sys>Python</sys> and <sys>Perl</sys> scripts.
 These have extensions <file>.sh</file>, <file>.py</file> and <file>.pl</file>,
 respectively.</p>
 
 <p>When you open an executable text file, you can select from:</p>
 
 <list>
  <item>
    <p><gui>Run executable text files when they are opened</gui></p>
  </item>
  <item>
    <p><gui>View executable text files when they are opened</gui></p>
  </item>
  <item>
    <p><gui>Kysy joka kerta</gui></p>
  </item>
 </list>

 <p>If <gui>Ask each time</gui> is selected, a dialog will pop up asking if you
 wish to run or view the selected text file.</p>

 <p>Executable text files are also called <em>scripts</em>. All scripts in the
 <file>~/.local/share/nautilus/scripts</file> folder will appear in the context
 menu for a file under the <gui style="menuitem">Scripts</gui> submenu. When a
 script is executed from a local folder, all selected files will be pasted to
 the script as parameters. To execute a script on a file:</p>

<steps>
  <item>
    <p>Navigate to the desired folder.</p>
  </item>
  <item>
    <p>Valitse haluttu tiedosto.</p>
  </item>
  <item>
    <p>Right click on the file to open the context menu and select the desired
    script to execute from the <gui style="menuitem">Scripts</gui> menu.</p>
  </item>
</steps>

 <note style="important">
  <p>A script will not be passed any parameters when executed from a remote
  folder such as a folder showing web or <sys>ftp</sys> content.</p>
 </note>

</section>

<section id="trash">
<info>
<link type="seealso" xref="files-delete"/>
<title type="link">Tiedostonhallinnan roskakorin ominaisuudet</title>
</info>
<title>Roskakori</title>

<terms>
 <item>
  <title><gui>Kysy ennen roskakorin tyhjennystä</gui></title>
  <p>Tämä valinta on oletuksena valittu. Roskakorin tyhjennyksen yhteydessä voit valita roskakorin tyhjennyksen tai tiedostojen poistaminen.</p>
 </item>
</terms>
</section>

</page>
