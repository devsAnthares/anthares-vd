<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="fi">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Käytä pitkiä ja monimutkaisia salasanoja.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

  <title>Valitse turvallinen salasana</title>

  <note style="important">
    <p>Tee salasanoista sellaisia, että ne ovat sinulle helposti muistettavia, mutta muille (mukaan lukien tietokoneille) vaikeasti arvattavia.</p>
  </note>

  <p>Hyvän salasanan valinta auttaa pitämään tietokoneen turvassa. Jos salasanasi on helppo arvata, joku saattaa selvittää sen ja saa näin pääsyn henkilökohtaisiin tietoihisi.</p>

  <p>Ihmiset käyttävät usein tietokoneita avuksi salasanoja murtaessa. Ihmiselle vaikeasti arvattava salasana saattaa murtua nopeasti, kun yksi tai useampi tietokone yrittää selvittää sitä. Seuraavassa muutamia vinkkejä hyvän salasanan valintaan:</p>
  
  <list>
    <item>
      <p>Käytä isoja ja pieniä kirjaimia, numeroita, välilyöntejä sekä muita merkkejä salasanassa. Mitä enemmän erilaisia merkkejä salasana sisältää, sitä vaikeammin se on arvattavissa.</p>
      <note>
        <p>Yksi hyvä tapa muodostaa salasana on ottaa esimerkiksi jonkin tietyn lauseen sanoista ensimmäiset kirjaimet. Lauseesta "Syyskuun viimeinen päivä on Suomessa usein sateinen" saisi muodostettua esimerkiksi salasanan "SvpoSus".</p>
      </note>
    </item>
    <item>
      <p>Tee salasanasta mahdollisimman pitkä. Mitä enemmän merkkejä se sisältää, sitä enemmän salasanan arvaamiseen kuluu aikaa.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Älä käytä henkilökohtaisia tietoja, kuten syntymäpäiviä, rekisterinumeroita tai perheenjäsenten nimiä.</p>
    </item>
    <item>
      <p>Älä käytä substantiiveja salasanassasi.</p>
    </item>
    <item>
      <p>Valitse salasana, joka on nopeasti kirjoitettavissa. Näin muut eivät ehdi katsoa sivusta, kun kirjoitat salasanaasi.</p>
      <note style="tip">
        <p>Älä koskaan kirjoita salasanojasi muistiin sellaisenaan. Joku saattaa löytää salasanalappusi!</p>
      </note>
    </item>
    <item>
      <p>Käytä eri salasanoja eri järjestelmissä ja palveluissa.</p>
    </item>
    <item>
      <p>Käytä eri salasanoja kaikissa tileissäsi.</p>
      <p>Jos käytät samaa salasanaa kaikkiin käyttäjätunnuksiisi, ja joku arvaa salasanasi, hänellä on pääsy kaikkiin käyttämiisi järjestelmiin ja palveluihin.</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>Vaihda salasanasi säännöllisesti.</p>
   </item>
  </list>

</page>
