<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="fi">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tarkista äänikaapelit ja äänikortin ajurit.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Kuulen rätinää tai surinaa, kun ääntä toistetaan</title>

  <p>Jos kuulet rasahtavan tai surisevan äänen tietokoneesi toistaessa ääntä, ongelma voi johtua äänikaapeleista tai -liittimistä, tai äänikortin ajureista.</p>

<list>
 <item>
  <p>Varmista, että kaiuttimet on kytketty kunnolla.</p>
  <p>If the speakers are not fully plugged in, or if they are plugged into the
  wrong socket, you might hear a buzzing sound.</p>
 </item>

 <item>
  <p>Varmista, ettei kaiuttimien/kuulokkeiden johto ole rikki.</p>
  <p>Äänikaapelit- ja liittimet kuluvat ajan myötä. Yritä liittää kaapeli tai kuulokkeet toiseen äänilähteeseen (esimerkiksi MP3-tai CD-soittimeen) ja tarkista, onko äänessä vielä ongelmia. Jos on, saattaa olla tarpeen hankkia uusi kaapeli tai kuulokkeet.</p>
 </item>

 <item>
  <p>Tarkista äänikortin ajureiden laatu.</p>
  <p>Some sound cards do not work very well on Linux because they do not have very
  good drivers. This problem is more difficult to identify. Try searching for
  the make and model of your sound card on the internet, plus the search term
  “Linux”, to see if other people are having the same problem.</p>
  <p>You can use the <cmd>lspci</cmd> command to get more information about your
  sound card.</p>
 </item>
</list>

</page>
