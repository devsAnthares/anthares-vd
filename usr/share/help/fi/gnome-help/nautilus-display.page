<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="fi">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Control icon captions used in the file manager.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Tiedostonhallinnan näkymäasetukset</title>

<p>You can control how the file manager displays captions under icons.  Click
<gui>Files</gui> in the top bar, pick <gui>Preferences</gui> and select the
<gui>Display</gui> tab.</p>

<section id="icon-captions">
  <title>Kuvakkeiden tekstit</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Tiedostohallinnan kuvakkeet nimillä</p>
  </media>
  <p>Kun kuvakenäkymä on käytössä, tiedostonhallinta voi näyttää lisätietoja tiedostoista ja kansioista. Tämä on hyödyllistä, jos esimerkiksi tarvitset usein tietoa, kuka omistaa tiedoston tai milloin sitä on viimeksi muokattu.</p>
  <p>You can zoom in a folder by clicking the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choosing a zoom level with the slider. As you zoom in, the
  file manager will display more and more information in captions. You can
  choose up to three things to show in captions. The first will be displayed at
  most zoom levels. The last will only be shown at very large sizes.</p>
  <p>Kuvakkeiden alla on mahdollista näyttää samat tiedot kuin luettelonäkymän sarakkeissa. Lue <link xref="nautilus-list"/> saadaksesi lisätietoa.</p>
</section>

<section id="list-view">

  <title>List View</title>

  <p>When viewing files as a list, you can <gui>Navigate folders in a
  tree</gui>. This shows expanders on each directory in the file list, so
  that the contents of several folders can be shown at once. This is useful if
  the folder structure is relevant, such as if your music files are organized
  with a folder per artist, and a subfolder per album.</p>

</section>

</page>
