<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="fi">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kiinteän IP-osoitteen käyttö saattaa helpottaa joidenkin verkkopalvelujen hallintaa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

  <title>Luo yhteys käyttämällä kiinteää IP-osoitetta</title>

  <p>Useimmat langalliset verkot osaavat antaa tietokoneelle yhteyden muodostamiseen tarvittavat  <link xref="net-what-is-ip-address">tiedot (kuten IP-osoitteen)</link> automaattisesti. Nämä tiedot voivat muuttua silloin tällöin, mutta haluat mahdollisesti käyttää kiinteää IP-osoitetta, jolloin se on aina tiedossasi (tämä voi olla hyödyllistä esimerkiksi tiedostopalvelinta käytettäessä).</p>

  <steps>
    <title>Määritä tietokoneellesi kiinteä (staattinen) IP-osoite:</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Verkko</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Verkko</gui>.</p>
    </item>
    <item>
      <p>In the left pane, select the network connection that you want to have
      a fixed address. If you plug in to the network with a cable, click
      <gui>Wired</gui>, then click the
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button in the lower right corner of
      the panel. For a <gui>Wi-Fi</gui> connection, the 
      <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media>
      button will be located next to the active network.</p>
    </item>
    <item>
      <p>Click on <gui>IPv4</gui> or <gui>IPv6</gui> in the left pane
      and change the <gui>Addresses</gui> to <em>Manual</em>.</p>
    </item>
    <item>
      <p>Type in the <gui xref="net-what-is-ip-address">IP Address</gui> and
      <gui>Gateway</gui>, as well as the appropriate <gui>Netmask</gui>.</p>
    </item>
    <item>
      <p>In the <gui>DNS</gui> section, switch <gui>Automatic</gui> to
      <gui>OFF</gui>. Enter the IP address of a DNS server you want to use.
      Enter additional DNS server addresses using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>In the <gui>Routes</gui> section, switch <gui>Automatic</gui> to
      <gui>OFF</gui>. Enter the <gui>Address</gui>, <gui>Netmask</gui>,
      <gui>Gateway</gui> and <gui>Metric</gui> for a route you want to use.
      Enter additional routes using the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. The network connection should now have a fixed
      IP address.</p>
    </item>
  </steps>

</page>
