<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-adhoc" xml:lang="fi">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Ad-hoc-verkko mahdollistaa Internet-yhteyden jakamisen muille laitteille.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  </info>

<title>Langattoman yhteyspisteen luominen</title>

  <p>You can use your computer as a wireless hotspot. This allows other devices
  to connect to you without a separate network, and allows you to share an
  internet connection you’ve made with another interface, such as to a wired
  network or over the cellular network.</p>

<steps>
  <item>
    <p>Avaa <gui xref="shell-introduction#yourname">järjestelmävalikko</gui> yläpalkin oikeasta laidasta.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>
      Wi-Fi Not Connected</gui> or the name of the wireless network to which
    you are already connected. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>Wi-Fi Settings</gui>.</p></item>
  <item><p>Napsauta <gui>Käytä yhteyspisteenä…</gui></p></item>
  <item><p>If you are already connected to a wireless network, you will be
  asked if you want to disconnect from that network. A single wireless adapter
  can connect to or create only one network at a time. Click <gui>Turn On</gui>
  to confirm.</p></item>
</steps>

<p>A network name (SSID) and security key are automatically generated.
The network name will be based on the name of your computer. Other devices
will need this information to connect to the hotspot you’ve just created.</p>

</page>
