<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-menubar" xml:lang="pt-BR">

  <info>
    <revision version="0.1" date="2013-02-21" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref#global"/>
    <link type="seealso" xref="pref-keyboard-access"/>
    <link type="seealso" xref="app-fullscreen"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Oculte e mostre a barra de menu.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luciana Bastos de Freitas Menezes</mal:name>
      <mal:email>lubfm@gnome.org</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Pedro Vaz de Mello de Medeiros</mal:name>
      <mal:email>pedrovmm@gmail.com</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonardo Ferreira Fontenelle</mal:name>
      <mal:email>leonardof@gnome.org</mal:email>
      <mal:years>2008</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andrius da Costa Ribas</mal:name>
      <mal:email>andriusmao@gmail.com</mal:email>
      <mal:years>2008</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felipe Braga</mal:name>
      <mal:email>fbobraga@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Visibilidade da barra de menu</title>

  <p>Você pode habilitar ou desativar a barra de menu se desejar. Isto pode ser útil se você tiver um espaço limitado de tela. Para esconder a barra de menu:</p>

  <steps>
    <item>
      <p>Selecione o menu <gui style="menu">Ver</gui> e desmarque a opção <gui style="menuitem">Mostrar barra de menu</gui>.</p>
    </item>
  </steps>

  <p>Para restaurar a barra de menu:</p>

  <steps>
    <item>
      <p>Clique com o botão direito na janela do <app>Terminal</app>, e marque a opção <gui style="menuitem">Mostrar barra de menu</gui>.</p>
    </item>
  </steps>

  <p>Para habilitar ou desabilitar a barra de menu por padrão em todas as janelas do <app>Terminal</app> que você abrir:</p>

  <steps>
    <item>
      <p>Selecione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferências</gui> <gui style="tab">Geral</gui></guiseq>.</p>
    </item>
    <item>
      <p>Selecione ou desmarque <gui>Mostrar barra de menu, por padrão, em novos terminais</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Você pode <link xref="adv-keyboard-shortcuts">definir um atalho de teclado</link> para esconder e mostrar a barra de menu.</p>
  </note>

</page>
