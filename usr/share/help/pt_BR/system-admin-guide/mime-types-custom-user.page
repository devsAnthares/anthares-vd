<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom-user" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

    <title>Adicionando um tipo MIME personalizado para usuários individuais</title>
    <p>Para adicionar um tipo MIME personalizado para usuários individuais e registrar um aplicativo padrão para aquele tipo MIME, você precisa criar um novo arquivo de especificação de tipo MIME no diretório <file>~/.local/share/mime/packages/</file> e um arquivo <file>.desktop</file> no diretório <file>~/.local/share/applications/</file>.</p>
    <steps>
      <title>Adicionando um tipo MIME personalizado <code>aplicativo/x-novotipo</code> para usuários individuais</title>
      <item>
        <p>Crie o arquivo <file>~/.local/share/mime/packages/aplicativo-x-novotipo.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="aplicativo/x-novotipo"&gt;
    &lt;comment&gt;novo tipo mime&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>O arquivo exemplo <file>aplicativo-x-novotipo.xml</file> acima define um novo tipo MIME <sys>aplicativo/x-novotipo</sys> e atribui nomes de arquivos com a extensão <file>.xyz</file> àquele tipo MIME.</p>
      </item>
      <item>
        <p>Crie um novo arquivo <file>.desktop</file> com nome, por exemplo, <file>meuaplicativo1.desktop</file>, e coloque-o no aplicativo <file>~/.local/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=aplicativo/x-novotipo
Name=<var>Meu Aplicativo 1</var>
Exec=<var>meuaplicativo1</var></code>
      <p>O arquivo exemplo <file>meuaplicativo1.desktop</file> acima associa o tipo MIME <code>aplicativo/x-novotipo</code> com um aplicativo chamado <app>Meu Aplicativo 1</app>, que é executado por um comando <cmd>meuaplicativo1</cmd>.</p>
      </item>
      <item>
        <p>Atualize o banco de dados MIME para que suas alterações tenham efeito:</p>
        <screen><output>$ </output><input>update-mime-database ~/.local/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Atualize o banco de dados de aplicativo:</p>
        <screen><output>$ </output><input>update-desktop-database ~/.local/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Para verificar se você associou com sucesso arquivos <file>*.xyz</file> com o tipo MIME <sys>aplicativo/x-novotipo</sys>, primeiro crie um arquivo vazio, por exemplo <file>teste.xyz</file>:</p>
        <screen><output>$ </output><input>touch teste.xyz</input></screen>
        <p>Então, execute o comando <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info teste.xyz | grep "standard::content-type"</input>
  standard::content-type: aplicativo/x-novotipo</screen>
        </item>
        <item>
          <p>Para verificar que <file>meuaplicativo1.desktop</file> foi definido corretamente como o aplicativo registrado padrão para o tipo MIME <sys>aplicativo/x-novotipo</sys>, execute o comando <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime aplicativo/x-novotipo</input>
Aplicativo padrão para “aplicativo/x-”: meuaplicativo1.desktop
Aplicativos registrados:
	meuaplicativo1.desktop
Aplicativos recomendados:
	meuaplicativo1.desktop</screen>
      </item>
    </steps>
</page>
