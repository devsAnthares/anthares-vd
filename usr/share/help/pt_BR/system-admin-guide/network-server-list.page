<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-server-list" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="network"/>
    <revision pkgversion="3.8" date="2013-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
   <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
   <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@gvolny.cz</email>
      <years>2013</years>
    </credit>

    <desc>Como faço para que seja fácil para diferentes usuários acessar diferentes compartilhamentos de arquivos?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Configurando uma lista de servidores padrão</title>

  <p><app>Nautilus</app> (o aplicativo <app>Arquivos</app>) armazena uma lista de servidores de compartilhamento de arquivos no arquivo <file>~/.config/nautilus/servers</file> no formato XBEL. Adicione a lista de servidores de compartilhamento de arquivos para aquele arquivo para tornar os compartilhamentos de arquivos facilmente acessíveis ao seu usuário.</p>
  
  <note style="tip">
    <p><em>XBEL</em> (<em>XML Bookmark Exchange Language</em>) é um padrão XML que permite compartilhar URIs (Uniform Resource Identifiers). No GNOME, XBEL é usado para compartilhar marcadores em aplicativos como o <app>Nautilus</app>.</p>
  </note>

  <example>
    <p>Um exemplo do arquivo <file>~/.config/nautilus/servers</file>:</p>
    <code>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xbel version="1.0"
      xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"
      xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"&gt;
   &lt;bookmark href="<input>ftp://ftp.gnome.org/</input>"&gt;
      &lt;title&gt;<input>GNOME FTP</input>&lt;/title&gt;
   &lt;/bookmark&gt;
&lt;/xbel&gt;
</code>
    <p>No exemplo acima, <app>Nautilus</app> cria um marcador com nome <em>GNOME FTP</em> com a URI <code>ftp://ftp.gnome.org/</code>.</p>
  </example>

</page>
