<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="logout-automatic" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="login#management"/>
<!--    <link type="seealso" xref="dconf-profiles" />-->
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="login-automatic"/>

    <revision pkgversion="3.12" date="2014-06-18" status="review"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Encerre uma sessão de usuário inativo.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Configurando encerramento de sessão automático</title>

  <p>Sessões de usuários que estiveram inativos por um período de tempo específico podem ser encerradas automaticamente. Você pode definir diferentes comportamentos no caso da máquina está funcionando na bateria ou na energia principal definindo a chave <sys its:translate="no">dconf</sys> correspondente, e então travando-a.</p>

  <note style="warning">
    <p>Tenha em mente que usuários podem, potencialmente, perder dados não salvos se uma sessão inativa for encerrada automaticamente.</p>
  </note>

  <steps>
    <title>Definindo encerramento automático para uma máquina na energia principal</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Crie um banco de dados <code>local</code> para configuração de toda máquina em <file>/etc/dconf/db/local.d/00-autologout</file>:</p>
      <listing>
        <code>
[org/gnome/settings-daemon/plugins/power]
# Defina o tempo limite para 900 segundos quando na energia principal
sleep-inactive-ac-timeout=900
# Defina ação após tempo limite para ser encerrada a sessão na energia principal
sleep-inactive-ac-type='logout'
</code>
      </listing>
    </item>
    <item>
      <p>Sobrescreva a configuração do usuário e evita que o usuário altere-a em <file>/etc/dconf/db/local.d/locks/autologout</file>:</p>
      <listing>
        <code>
# Trata as configurações de logout automaticamente
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-type
</code>
</listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

  <!-- There will shortly be a way to get key descriptions using gsettings, we
       should recommend this instead of listing the terms here. See
       https://bugzilla.gnome.org/show_bug.cgi?id=668232 -->
  <p>As seguintes chaves GSettings são relevantes:</p>
  <terms>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-timeout</code></title>
      <p>O número de segundos que o computador precisa para estar inativo antes de ir dormir se estiver ligado na tomada.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-type</code></title>
      <p>O que deve acontecer quando o tempo limite extrapolou se o computador estiver ligado na tomada.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-timeout</code></title>
      <p>O número de segundos que o computador precisa para estar inativo antes de ir dormir se estiver funcionando na bateria.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-type</code></title>
      <p>O que deve acontecer quando o tempo limite extrapolou se o computador estiver funcionando na bateria.</p>
    </item>
  </terms>

  <p>Você pode executer <cmd>gsettings range</cmd> em uma chave para uma lista de valores que você pode usar. Por exemplo:</p>
  
  <screen>$ <input>gsettings range org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type</input>
enum
'blank' # apaga a tela
'suspend' # suspende o sistema
'shutdown' # inicia o procedimento de desligamento padrão
'hibernate' # hiberna o sistema
'interactive' # mostra uma diálogo perguntando o que fazer
'nothing' # faz nada
'logout' # encerra a sessão</screen>

</page>
