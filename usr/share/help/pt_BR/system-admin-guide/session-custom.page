<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="session-custom" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Crie uma sessão personalizada instalando um arquivo desktop de sessão.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Criando uma sessão personalizada</title>

 <p>Para criar sua própria sessão com configuração personalizada, siga essas etapas:</p>

<steps>
  <item><p>Crie um arquivo <file>.desktop</file> em <file>/etc/X11/sessions/<var>nova-sessao</var>.desktop</file>. Certifique-se de que o arquivo especifica as seguintes entradas:</p>
  <code>[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=<input>Sessão personalizada</input>
Comment=<input>Essa é nossa sessão personalizada</input>
Exec=<input>gnome-session --session=nova-sessao</input></code>
   <p>A entrada <code>Exec</code> especifica o comando, possivelmente com argumentos, para executar. Você pode executar a sessão personalizada com o comando <cmd>gnome-session --session=<var>nova-sessao</var></cmd>.</p>
   <p>Para mais informações sobre os parâmetros que você pode usar com <cmd>gnome-session</cmd>, veja a página man do <link href="man:gnome-session">gnome-session</link>.</p>
  </item>
  <item><p>Crie um arquivo de sessão personalizada no <file>/usr/share/gnome-session/sessions/<var>nova-sessao</var>.session</file> no qual você pode especificar o nome e os componentes exigidos para a sessão:</p>
   <code>[GNOME Session]
Name=<input>Sessão personalizada</input>
RequiredComponents=<input>gnome-shell-classic;gnome-settings-daemon;</input></code>
  <p>Note que qualquer item que você especificar no <code>RequiredComponents</code> precisa ter seu correspondente arquivo <file>.desktop</file> no <file>/usr/share/applications/</file>.</p>
  </item>
</steps>

  <p>Após configurar os arquivos de sessão personalizada, a nova sessão estará disponível na lista de sessão na tela de autenticação do GDM.</p>

  <section id="custom-session-issues">
  <title>Problemas conhecidos</title>
    <p>Em um sistema Debian, ou baseado no Debian, o seguinte erro pode acontecer:</p>

    <screen>Xsession: unable to launch ""
    Xsession --- "" not found; falling back to default
    session.</screen>

    <p>Caso isso aconteça com você, siga os seguintes passos para alterar o arquivo <file>/etc/X11/Xsession.d/20x11-common_process-args</file>:</p>
    <steps>
    <item><p>Altere <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>"$1"</input>|| true)</code> para <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>$1</input> || true)</code></p></item>
    <item><p>Altere <code>STARTUP=<input>"$1"</input></code> para <code>STARTUP=<input>$1</input></code></p></item>
    </steps>
  </section>

</page>
