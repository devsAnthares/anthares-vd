<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="a11y task" id="a11y" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc><link xref="a11y#vision">Visão</link>, <link xref="a11y#sound">audição</link>, <link xref="a11y#mobility">mobilidade</link>, <link xref="a11y-braille">braille</link>, <link xref="a11y-mag">ampliador de tela</link>…</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Acessibilidade</title>

  <p>O ambiente GNOME inclui tecnologias assistivas para dar suporte a usuários com variadas deficiências ou necessidades especiais, e para interagir com dispositivos assistivos comuns. Um menu de acessibilidade pode ser adicionado à barra superior, dando acesso mais fácil a muitas funcionalidades de acessibilidade.</p>

  <section id="vision">
    <title>Deficiências visuais</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Cegueira</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Visão fraca</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Daltonismo</title>
    </links>
    <links type="topic" style="linklist">
      <title>Outros tópicos</title>
    </links>
  </section>

  <section id="sound">
    <title>Deficiências auditivas</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Deficiências motoras</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Movimento do mouse</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Clicando e arrastando</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Uso do teclado</title>
    </links>
    <links type="topic" style="linklist">
      <title>Outros tópicos</title>
    </links>
  </section>
</page>
