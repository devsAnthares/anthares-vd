<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Diminua ou aumente um sistema de arquivos e sua partição.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Ajustando o tamanho de um sistema de arquivos</title>

  <p>Um sistema de arquivos pode ser aumentado para usar o espaço livre após sua partição. Geralmente isso é possível até mesmo quando o sistema de arquivos está montado.</p>
  <p>Para fazer espaço para outra partição após o sistema de arquivos, ele pode ser diminuído conforme o espaço livre nele.</p>
  <p>Nem todos os sistemas de arquivos possuem suporte a redimensionamento.</p>
  <p>O tamanho da partição será alterado junto com o tamanho do sistema de arquivos. Também é possível redimensionar uma partição sem um sistema de arquivos na mesma forma.</p>

<steps>
  <title>Redimensionar um sistema de arquivos/uma partição</title>
  <item>
    <p>Abra <app>Discos</app> no panorama de <gui>Atividades</gui>.</p>
  </item>
  <item>
    <p>Selecione o disco contendo o sistema de arquivos em questão a partir da lista de dispositivos de armazenamento à esquerda. Se houver mais de um volume no disco, selecione o volume que contém o sistema de arquivos.</p>
  </item>
  <item>
    <p>Na barra de ferramentes sob a seção <gui>Volumes</gui>, clique no ícone de menu. Então, clique em <gui>Redimensionar sistema de arquivos…</gui> ou <gui>Redimensionar…</gui>, se não houver sistema de arquivos.</p>
  </item>
  <item>
    <p>Será aberta uma caixa de diálogo no qual o novo tamanho pode ser escolhido. O sistema de arquivos será montado para calcular o tamanho mínimo pela quantidade de conteúdo atual. Se não houver suporte à diminuição, o tamanho mínimo é o tamanho atual. Deixe espaço suficiente no sistema de arquivos ao diminuir para garantir que ele possa funcionar de forma rápida e confiável.</p>
    <p>Dependendo da quantidade de dados a serem movidos da parte encolhida, o redimensionamento do sistema de arquivos pode demorar mais tempo.</p>
    <note style="warning">
      <p>O redimensionamento do sistema de arquivos envolve automaticamente <link xref="disk-repair">correção</link> do sistema de arquivos. Portanto, é aconselhável fazer cópia de segurança de dados importantes antes de começar. A ação não deve ser interrompida ou resultará em um sistema de arquivos danificado.</p>
    </note>
  </item>
  <item>
      <p>Confirme para começar a ação clicando em <gui style="button">Redimensionar</gui>.</p>
   <p>A ação irá desmontar o sistema de arquivos se não houver suporte ao redimensionamento de um sistema de arquivos montado. Seja paciente enquanto o sistema de arquivos é redimensionado.</p>
  </item>
  <item>
    <p>Após a conclusão das ações necessárias de redimensionamento e correção, o sistema de arquivos está pronto para ser usado novamente.</p>
  </item>
</steps>

</page>
