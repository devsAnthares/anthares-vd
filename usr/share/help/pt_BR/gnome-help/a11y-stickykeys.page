<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Digite os atalhos de teclado uma tecla de cada vez em vez de ter que mantê-las pressionadas todas ao mesmo tempo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Ativando teclas de aderência</title>

  <p>As <em>Teclas de aderência</em> permitem digitar atalhos de teclado uma tecla de cada vez em vez de ter que pressioná-las todas ao mesmo tempo. Por exemplo, o atalho <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> alterna entre janelas. Sem as teclas de aderência ativadas, você teria mantê-las ambas as teclas pressionadas ao mesmo tempo; com as teclas de aderência ativas, você pressionaria <key>Super</key> e então a <key>Tab</key> para fazer o mesmo.</p>

  <p>Você pode querer ativar as teclas de aderência se você achar difícil manter pressionadas várias teclas ao mesmo tempo.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Assistência de digitação (AccessX)</gui> na seção <gui>Digitação</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Teclas de aderência</gui> para <key>❙</key>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ativar ou desativar rapidamente as teclas de aderência</title>
    <p>Em <gui>Habilitar por teclado</gui>, selecione <gui>Ativar os recursos de acessibilidade do teclado</gui> para ativar e desativar as teclas de aderência no seu teclado. Quando esta opção estiver marcada, você pode pressionar <key>Shift</key> cinco vezes seguidas para habilitar ou desabilitar as teclas de aderência.</p>
    <p>Você pode ativar ou desativar as teclas de aderência clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Teclas de aderência</gui>. O ícone de acessibilidade fica visível quando uma ou mais configurações de <gui>Acessibilidade</gui> foram habilitadas.</p>
  </note>

  <p>Se você pressionar duas teclas de uma vez, você pode ter as teclas de aderência desabilitadas sozinhas temporariamente para lhe deixar usar um atalho de teclado na forma normal.</p>

  <p>Por exemplo, se você estiver com as teclas de aderência ativadas, mas pressionar <key>Super</key> e <key>Tab</key> simultaneamente, as teclas de aderência não vão esperar você pressionar outra tecla como se você estivesse com a opção ativa. Ele <em>esperaria</em> se só tivesse pressionado uma tecla. Isso é útil se você for capaz de pressionar alguns atalhos de teclado simultaneamente (por exemplo, teclas que sejam próximas), mas não outros.</p>

  <p>Selecione <gui>Desabilitar se duas teclas forem pressionadas juntas</gui> para habilitar isso.</p>

  <p>Seu computador pode emitir um “bip” quando você começa a digitar um atalho de teclado com as teclas de aderência estiverem ativadas. Isso é útil se quiser saber se as teclas de aderência estão esperando que um atalho de teclado seja digitado, e que então as próximas teclas pressionadas seriam interpretadas como parte de um atalho. Selecione <gui>Soar um bip quando uma tecla modificadora for pressionada</gui> para habilitar isso.</p>

</page>
