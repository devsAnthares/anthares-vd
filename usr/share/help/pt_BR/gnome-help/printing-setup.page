<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer" group="#first"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configure uma impressora que está conectada a seu computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Configurando uma impressora local</title>

  <p>Seu sistema pode reconhecer automaticamente muitos tipos de impressoras assim que tenham sido conectadas. A maioria das impressoras são conectadas com um cabo USB.</p>

  <note style="tip">
    <p>Você não precisa selecionar se você deseja instalar impressora de rede ou local. Elas são listadas em uma janela.</p>
  </note>

  <steps>
    <item>
      <p>Certifique-se de que a impressora esteja ligada.</p>
    </item>
    <item>
      <p>Conecte sua impressora a seu sistema via o cabo apropriado. Você pode ver atividade na tela na medida em que o sistema procura por drivers, e você pode ser solicitado por autenticação para instalá-los.</p>
    </item>
    <item>
      <p>Uma mensagem aparecerá quando o sistema tiver finalizado a instalação da impressora. Selecione <gui>Página de teste de impressão</gui> para imprimir uma página de teste ou <gui>Opções</gui> para fazer alterações adicionais na página de impressão.</p>
    </item>
  </steps>

  <p>Se sua impressora não foi configurada automaticamente, você pode adicioná-la nas configurações de impressão:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Clique no botão <gui>Desbloquear</gui> no canto superior direito e insira sua senha.</p>
    </item>
    <item>
      <p>Clique no botão <gui>+</gui>.</p>
    </item>
    <item>
      <p>Na janela de instantânea, selecione uma nova impressora. Clique <gui>Adicionar</gui>.</p>
    </item>
  </steps>

  <p>Se sua impressora não aparecer na janela <gui>Adicionar uma nova impressora</gui>, você pode ter que instalar drivers de impressora.</p>

  <p>Após a instalação da impressora, você pode desejar <link xref="printing-setup-default-printer">alterar sua impressora padrão</link>.</p>

</page>
