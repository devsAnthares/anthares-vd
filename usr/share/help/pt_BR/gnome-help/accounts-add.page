<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="accounts" group="add"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.9.92" date="2012-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permite que aplicativos acessem suas fotos, contatos, agendas e outros recursos de suas contas on-line.</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Adicionando uma conta</title>

  <p>Adicionar uma conta vai ajudá-lo a vincular suas contas on-line a seu ambiente GNOME. Dessa forma, seu programa de e-mail, programa de bate-papo e outros aplicativos relacionados serão configurados por você.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Contas on-line</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Contas on-line</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione uma conta na lista à direita.</p>
    </item>
    <item>
      <p>Selecione o tipo de conta que você deseja adicionar.</p>
    </item>
    <item>
      <p>Um diálogo ou uma pequena janela de site será aberta, na qual você pode inserir as credenciais da sua conta on-line. Por exemplo, se você está configurando uma conta Google, digite seu usuário e senha no Google. Alguns provedores permitem que você crie uma nova conta no próprio diálogo de autenticação.</p>
    </item>
    <item>
      <p>Se você digitou suas credenciais corretamente, você será solicitado a permitir acesso do GNOME a sua conta on-line. Autorize acesso para continuar.</p>
    </item>
    <item>
      <p>Todos os serviços que são oferecidos por um provedor de conta serão habilitados por padrão. <link xref="accounts-disable-service">Alterne</link> serviços individuais para <key>○</key> para desabilitá-los.</p>
    </item>
  </steps>

  <p>Após ter adicionado as contas, os aplicativos podem usá-las para os serviços que você escolheu permitir. Veja <link xref="accounts-disable-service"/> para informação sobre controlar quais servidores permitir.</p>

  <note style="tip">
    <p>Muitos serviços on-line fornecem um token de autorização, o que o GNOME armazena ao invés de sua senha. Se você remover uma conta, você também deveria revogar aquele certificado no serviço on-line. Veja <link xref="accounts-remove"/> para mais informação.</p>
  </note>

</page>
