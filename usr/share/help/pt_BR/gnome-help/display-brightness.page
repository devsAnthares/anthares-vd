<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision pkgversion="3.8" version="0.4" date="2013-03-28" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o brilho da tela para tornar mais legível em ambientes muito claros.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Definindo brilho da tela</title>

  <p>Dependendo de seu hardware, você pode alterar o brilho de sua tela para economizar energia ou para tornar mais legível em ambientes muitos claros.</p>

  <p>Para alterar o brilho de sua tela, clique no ícone de <gui xref="shell-introduction#yourname">sistema de menu</gui> no lado direito da barra superior e ajuste o controle deslizante de brilho da tela para usar o valor desejado. A alteração deve fazer efeito imediatamente.</p>

  <note style="tip">
    <p>Muitos teclados de laptop possuem teclas especiais para ajustar o brilho. Eles geralmente possuem uma imagem que se parece com o Sol. Segure a tecla <key>Fn</key> para usar essas teclas.</p>
  </note>

  <p>Você também pode ajustar o brilho de tela usando o painel <gui>Energia</gui>.</p>

  <steps>
    <title>Para definir o brilho de tela usando o painel de Energia:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Energia</gui> na barra lateral.</p>
    </item>
    <item>
      <p>Ajuste o controle deslizante do <gui>Brilho da tela</gui> para o valor que você deseja usar. A alteração deve fazer efeito imediatamente.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Se seu computador possui um sensor de luminosidade integrado, o brilho de tela vai ser ajustado automaticamente. Você pode desabilitar o brilho de tela automático em <gui>Energia</gui>.</p>
  </note>

  <p>Se for possível definir o brilho de sua tela, você também pode ter escurecimento de tela automático para economizar energia. Para mais informação, veja <link xref="power-whydim"/>.</p>

</page>
