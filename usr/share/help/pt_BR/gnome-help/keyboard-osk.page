<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Use um teclado em tela para digitar um texto clicando nos botões com o mouse ou um touchscreen.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Usando um teclado em tela</title>

  <p>Se você não tem um teclado conectado a seu computador ou prefere não utilizá-lo, você pode ativar o <em>teclado em tela</em> para digitar um texto.</p>

  <note>
    <p>O teclado em tela é habilitado automaticamente se você usa um touchscreen</p>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Teclado virtual</gui> na seção <gui>Visão</gui>.</p>
    </item>
  </steps>

  <p>Quando você tiver outra oportunidade de digitar, o teclado virtual vai abrir embaixo na tela.</p>

  <p>Pressione o botão <gui style="button">123</gui> para digitar números e símbolos. Mais símbolos ficam disponíveis se você, então, pressionar o botão <gui style="button">=/&lt;</gui>. Para voltar ao teclado alfabético, pressione o botão <gui style="button">ABC</gui>.</p>

  <p>Você pode pressionar o botão <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">para baixo</span></media></gui> para ocultar o teclado temporariamente. O teclado será automaticamente mostrado novamente quando você pressionar em seguida algo no qual você possa usá-lo.</p>
  <p>Pressione o botão <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">bandeira</span></media></gui> para alterar suas configurações para <link xref="session-language">Idiona</link> ou <link xref="keyboard-layouts">Fontes de entrada</link>.</p>
  <!-- obsolete <p>To make the keyboard show again, open the
  <link xref="shell-notifications">messagetray</link> (by moving your mouse to
  the bottom of the screen), and press the keyboard icon.</p> -->

</page>
