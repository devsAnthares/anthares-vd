<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Ignore uma mesma tecla pressionada repetidamente rapidamente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Ativando teclas de repercussão</title>

  <p>Ative as <em>teclas de repercussão</em> para ignorar teclas que são pressionadas rapidamente repetidas vezes. Por exemplo, se você tem tremores na mão, o que pode fazer com que você pressione uma tecla múltiplas vezes quando você só queria pressioná-la apenas uma vez, você deveria ativar teclas de repercussão.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Assistência de digitação (AccessX)</gui> na seção <gui>Digitação</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Teclas de repercussão</gui> para <key>❙</key>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ative e desative rapidamente as teclas de repercussão</title>
    <p>Você pode ativar e desativar as teclas de repercussão clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Teclas de repercussão</gui>. O ícone de acessibilidade fica visível quando uma ou mais configurações de <gui>Acessibilidade</gui> forem habilitadas.</p>
  </note>

  <p>Use o controle deslizante de <gui>Atraso de aceitação</gui> para alterar quanto tempo as teclas de repercussão esperam até registrar outra tecla pressionada, depois que você pressionou a tecla pela primeira vez. Selecione <gui>Soar um bip quando a tecla for rejeitada</gui> se você deseja que o computador faça um som a cada vez que ele ignorar uma tecla pressionada por ter acontecido muito cedo, depois da tecla pressionada anteriormente.</p>

</page>
