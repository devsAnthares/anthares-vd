<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Siga essas dicas se você não consegue localizar um arquivo que você criou ou baixou.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Localizando um arquivo perdido</title>

<p>Se você criou ou baixou um arquivo, mas agora não consegue encontrá-lo, siga essas dicas.</p>

<list>
  <item><p>Se você não lembra onde salvou o arquivo, mas tem alguma ideia de qual o nome dele, você pode <link xref="files-search">pesquisar pelo arquivo com seu nome</link>.</p></item>

  <item><p>Se você acabou de baixar o arquivo, seu navegador web pode ter salvo automaticamente em uma pasta comum. Verifique as pastas <file>Área de trabalho</file> e <file>Downloads</file> na sua pasta pessoal.</p></item>

  <item><p>Talvez você tenha acidentalmente excluído o arquivo. Quando você exclui um, ele vai para a lixeira, onde ficará até que você a esvazie. Veja <link xref="files-recover"/> para aprender como recuperar um arquivo excluído.</p></item>

  <item><p>Você pode ter renomeado o arquivo de forma que ele fique oculto. Arquivos com nomes que começam com um <file>.</file> ou terminam com um <file>~</file> ficam ocultos no gerenciador de arquivos. Clique no botão de opções de visualização da barra de ferramentas do <app>Arquivos</app> e habilite <gui>Mostrar arquivos ocultos</gui> para exibi-los. Veja <link xref="files-hidden"/> para saber mais.</p></item>
</list>

</page>
