<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remova acesso a um provedor de serviço on-line de seu aplicativo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Removendo uma conta</title>

  <p>Você pode remover uma conta on-line que você não deseja mais usar.</p>

  <note style="tip">
    <p>Muitos serviços on-line fornecem um token de autorização, o que o GNOME armazena ao invés de sua senha. Se você remover uma conta, você também deveria revogar aquele certificado no serviço on-line. Isso garante que nenhum outro aplicativo ou site pode se conectar àquele serviço usando a autorização do GNOME.</p>

    <p>Como revogar a autorização depende do provedor de serviço. Verifique suas configurações no site do provedor por aplicativos ou sites conectados ou autorizados. Procure por um aplicativo chamado “GNOME” e remova-o.</p>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Contas on-line</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Contas on-line</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione a conta que você queira remover.</p>
    </item>
    <item>
      <p>Clique no botão <gui>-</gui> no canto inferior esquerdo da janela.</p>
    </item>
    <item>
      <p>Clique em <gui>Remover</gui> no diálogo de confirmação.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Ao invés de excluir a conta completamente, é possível <link xref="accounts-disable-service">restringir os serviços</link> acessados pelo seu computador.</p>
  </note>
</page>
