<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Salve um documento como um arquivo PDF, PostScript ou SVG em vez de enviá-lo para a impressora.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Imprimindo para arquivo</title>

  <p>Você pode escolher imprimir um documento ao invés de enviá-lo para impressão em uma impressora. Imprimir para arquivo vai criar um arquivo <sys>PDF</sys>, <sys>PostScript</sys> ou <sys>SVG</sys> que contém o documento. Isso pode ser útil se você deseja transferir o documento para outra máquina ou compartilhá-lo com alguém.</p>

  <steps>
    <title>Para imprimir para arquivo:</title>
    <item>
      <p>Abra o diálogo de impressão pressionando <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Selecione <gui>Imprimir para arquivo</gui> sob <gui>Impressora</gui> na aba <gui style="tab">Geral</gui>.</p>
    </item>
    <item>
      <p>Para alterar o nome padrão de arquivo e onde o arquivo será salvo, clique no nome do arquivo em baixo da seleção de impressora. Clique <gui style="button">Selecionar</gui> assim que você tiver concluído a seleção.</p>
    </item>
    <item>
      <p><sys>PDF</sys> é o tipo padrão de arquivo apara o documento. Se você deseja usar um <gui>Formato de saída</gui> diferente, selecione <sys>PostScript</sys> ou <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Escolha suas outras preferências de página.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Imprimir</gui> para salvar o arquivo.</p>
    </item>
  </steps>

</page>
