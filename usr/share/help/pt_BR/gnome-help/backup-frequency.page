<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aprenda o quão frequente deveria se fazer cópias de segurança dos seus arquivos importantes para que estejam seguros.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Frequência das cópias de segurança</title>

  <p>Quão frequentemente você faz as suas cópias de segurança depende do tipo de dados a serem assegurados. Por exemplo, se estiver executando um ambiente de rede com dados críticos armazenados em seus servidores, então as cópias noturnas podem não ser suficientes.</p>

  <p>Por outro lado, se estiver protegendo os dados de seu computador de casa, então cópias a cada hora provavelmente seriam desnecessárias. Você pode achar útil considerar os seguintes pontos ao planejar seu cronograma de cópia de segurança:</p>

<list style="compact">
<item><p>A quantidade de tempo que você passa no computador.</p></item>
<item><p>O quão frequentemente e quantas vezes os dados no computador se alteram.</p></item>
</list>

  <p>Se os dados que queira fazer cópia de segurança são de baixa prioridade, ou estão sujeitas a poucas mudanças, como e-mails, músicas e fotos de família, então cópias semanais ou mesmo mensais podem ser suficientes. Entretanto, se você se encontrar no meio de uma auditoria de impostos, são necessárias cópias de segurança mais frequentes.</p>

  <p>Como regra geral, a quantidade de tempo entre as criações de cópia de segurança não deveria ser maior do que aquela que você gostaria de gastar refazendo qualquer trabalho perdido. Por exemplo, se gastar uma semana reescrevendo arquivos perdidos é demais para você, então deveria fazer pelo menos uma vez na semana.</p>

</page>
