<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Organize as janelas em um espaço de trabalho para ajudar você a trabalhar com mais eficiência.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Movendo e redimensionando janelas</title>

  <p>Você pode mover e redimensionar janelas para lhe ajudar a trabalhar mais eficientemente. Além do comportamento de arrastar que é de se esperar, GNOME fornece atalhos e modificadores para ajudar você a organizar janelas rapidamente.</p>

  <list>
    <item>
      <p>Mova uma janela arrastando a barra de título, segure <key xref="keyboard-key-super">Super</key> e arraste para qualquer lugar na janela. Segure <key>Shift</key> enquanto move para atrair a janela para as bordas da tela e outras janelas.</p>
    </item>
    <item>
      <p>Redimensione uma janela arrastando as bordas ou contas da janela. Segure <key>Shift</key> enquanto redimensiona para atribuir a janela para as bordas da tela e outras janelas.</p>
      <p if:test="platform:gnome-classic">Você também pode redimensionar uma janela maximizada clicando no botão de maximizar na barra de título.</p>
    </item>
    <item>
      <p>Mova ou redimensione uma janela usando apenas o teclado. Pressione <keyseq><key>Alt</key><key>F7</key></keyseq> para mover uma janela ou <keyseq><key>Alt</key><key>F8</key></keyseq> para redimensionar. Use as teclas de seta para mover ou redimensionar, então pressione <key>Enter</key> para finalizar, ou pressione <key>Esc</key> para retornar à posição e tamanho original.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Maximize uma janela</link> arrastando-a para o top da tela. Arraste uma janela para um lado da tela para maximizá-la ao longo da lateral, permitindo que você <link xref="shell-windows-tiled">juntar janelas lado a lado</link>.</p>
    </item>
  </list>

</page>
