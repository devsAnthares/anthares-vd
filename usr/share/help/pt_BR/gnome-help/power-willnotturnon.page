<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Cabos mal conectados e problemas de hardware são possíveis motivos.</desc>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Meu computador não está ligando</title>

<p>Há vários motivos pelos quais seu computador não está ligando. Esse tópico fornece uma visão geral de alguns dos possíveis motivos.</p>
	
<section id="nopower">
  <title>Computador não conectado, bateria vazia ou cabo mal conectado</title>
  <p>Certifique-se de que os cabos de energia do computador estejam conectados firmemente, e as tomadas de energia estejam ligadas. Certifique-se, também, de que o monitor esteja conectado e ligado. Se você tem um notebook, conecte o cabo de carregamento (no caso dele estar sem bateria). Você também pode querer conferir que a bateria esteja encaixada corretamente no lugar (verifique embaixo do notebook), se for removível.</p>
</section>

<section id="hardwareproblem">
  <title>Problema com o hardware do computador</title>
  <p>Um componente de seu computador pode estar quebrado ou não funcionando adequadamente. Se esse for o caso, você precisará consertar o computador. Falhas comuns incluem uma unidade de fonte de alimentação quebrada, componentes incorretamente montados (tais como memória ou RAM) e placa-mãe defeituosa.</p>
</section>

<section id="beeps">
  <title>O computador soa e, então, desliga</title>
  <p>Se o computador bipa várias vezes quando você o liga e desliga (ou falha em inicializar), este pode ser um indício que ele detectou um problema. Esses bipes são, algumas vezes, referidos como <em>códigos sonoros</em>, e o padrão de bipes tem a intenção de dizer a você qual é o problema do computador. Fabricantes diferentes usam códigos sonoros diferentes, então você terá que consultar o manual da placa-mãe de seu computador, ou leve seu computador para conserto.</p>
</section>

<section id="fans">
  <title>As ventoinhas do computador estão girando, mas nada aparece na tela</title>
  <p>A primeira coisa para conferir é que seu monitor esteja conectado e ligado.</p>
  <p>Esse problema também poderia ocorrer por causa de defeito de hardware. As ventoinhas podem ligar quando você pressiona o botão de energia, mas outras partes essenciais do computador podem falhar em ligar. Neste caso, leve seu computador para conserto.</p>
</section>

</page>
