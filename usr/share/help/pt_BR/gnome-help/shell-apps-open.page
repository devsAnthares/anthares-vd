<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Inicie aplicativos a partir do panorama de <gui>Atividades</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Iniciando aplicativos</title>

  <p if:test="!platform:gnome-classic">Mova o ponteiro do seu mouse para <gui>Atividades</gui> no canto superior esquerda da tela para mostrar o panorama de <gui xref="shell-introduction#activities">Atividades</gui>. É ali que você pode encontrar todos os seus aplicativos. Você também pode abrir o panorama pressionando a tecla <key xref="keyboard-key-super">Super</key>.</p>
  
  <p if:test="platform:gnome-classic">Você pode iniciar os aplicativos do menu <gui xref="shell-introduction#activities">Aplicativos</gui> no canto esquerdo superior na tela ou você pode usar o panorama de <gui>Atividades</gui> pressionando a tecla <key xref="keyboard-key-super">Super</key>.</p>

  <p>Existem várias maneiras de se abrir um aplicativo uma vez que esteja no panorama de <gui>Atividades</gui>:</p>

  <list>
    <item>
      <p>Comece digitando o nome de um aplicativo — a pesquisa começa instantaneamente (se isso não acontecer, clique na barra de pesquisa na região superior da tela e comece a digitar). Se você não sabe exatamente o nome de um aplicativo, tente digitar um termo relacionado. Clique no ícone do aplicativo para iniciá-lo.</p>
    </item>
    <item>
      <p>Alguns aplicativos possuem ícones no <em>dash</em>, a listra vertical de ícones no lado esquerdo do panorama de <gui>Atividades</gui>. Clique em um desses para iniciar o aplicativo correspondente.</p>
      <p>Se você tiver aplicativos que você usa muito frequentemente, você pode <link xref="shell-apps-favorites">adicioná-los ao dash</link>.</p>
    </item>
    <item>
      <p>Clique no botão de grade na parte inferior do dash. Você verá os aplicativos frequentemente usados se a visão <gui style="button">Frequente</gui> estiver habilitada. Se você quiser executar um novo aplicativo, pressione o botão <gui style="button">Todos</gui> na parte inferior para ver todos os aplicativos. Pressione o aplicativo para iniciá-lo.</p>
    </item>
    <item>
      <p>Você pode inciar um aplicativo em um <link xref="shell-workspaces">espaço de trabalho</link> separado arrastando seu ícone do dash (ou da lista de aplicativos) e soltando-o em um outro espaço de trabalho no lado direito da tela. O aplicativo vai abrir espaço de trabalho escolhido.</p>
      <p>Você pode iniciar um aplicativo em um <em>novo</em> espaço de trabalho arrastando seu ícone para um espaço de trabalho vazio mais em baixo no alternador do espaço de trabalho, ou para um espaço entre dois espaços de trabalho.</p>
    </item>
  </list>

  <note style="tip">
    <title>Executando um comando rapidamente</title>
    <p>Uma outra forma de lançar um aplicativo é pressionar <keyseq><key>Alt</key><key>F2</key></keyseq>, digitar o <em>nome do comando</em> e, então, pressionar a tecla <key>Enter</key>.</p>
    <p>Por exemplo, para iniciar <app>Rhythmbox</app>, pressione <keyseq><key>Alt</key><key>F2</key></keyseq> e digite “<cmd>rhythmbox</cmd>” (sem as aspas). O nome do aplicativo é o comando para lançar o programa.</p>
  </note>

</page>
