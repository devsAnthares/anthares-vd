<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alterne para um idioma diferente para a interface com usuário e para o texto de ajuda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando que idioma você usa</title>

  <p>Você pode usar sua área de trabalho e seus aplicativos em qualquer um dos muitos idiomas disponíveis, desde que tenha os pacotes de idioma adequados no seu computador.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Região &amp; idioma</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Região &amp; idioma</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Clique em <gui>Idioma</gui>.</p>
    </item>
    <item>
      <p>Selecione sua região e seu idioma desejados. Se sua região e idioma não estiverem listados, clique em <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> logo abaixo da lista para selecionar todas regiões e idiomas disponíveis.</p>
    </item>
    <item>
      <p>Clique em <gui style="button">Concluído</gui> para salvar.</p>
    </item>
    <item>
      <p>Responda a confirmação, <gui>Sua sessão precisa ser reiniciada para que as alterações tenham efeito</gui> clicando em <gui style="button">Reiniciar agora</gui> ou em <gui style="button">×</gui> para reiniciar posteriormente.</p>
    </item>
  </steps>

  <p>Algumas traduções podem estar incompletas e certos aplicativos podem não oferecer suporte algum a esse idioma. Quaisquer textos não traduzidos aparecerão no idioma em que o programa foi originalmente desenvolvido, normalmente em inglês americano.</p>

  <p>Existem algumas pastas especiais na sua pasta pessoal, onde aplicativos podem armazenar coisas como música, figuras e documentos. Essas pastas usam nomes padrões conforme seu idioma. Quando você iniciar uma nova sessão, você será questionado se deseja renomeá-las para os nomes padrões para seu idioma selecionado. Se você planeja usa o novo idioma o tempo todo, você deveria atualizar os nomes das pastas.</p>

  <note style="tip">
    <p>Se houver várias contas de usuário em seu sistema, há uma instância separada da região e do painel <gui>Região &amp; idioma</gui> para a tela de início de sessão. Clique no botão <gui>Tela de início de sessão</gui> no canto superior direito para alternar entre as duas instâncias.</p>
  </note>

</page>
