<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Arquivos excluídos são normalmente enviados para a lixeira, mas podem ser restaurados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Restaurando um arquivo da lixeira</title>

  <p>Se você excluir um arquivo com o gerenciador de arquivos, o arquivo normalmente é colocado na <gui>Lixeira</gui> e pode ser restaurado.</p>

  <steps>
    <title>Para restaurar um arquivo da lixeira:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <app>Arquivos</app>.</p>
    </item>
    <item>
      <p>Clique em <app>Arquivos</app> para abrir o gerenciador de arquivos.</p>
    </item>
    <item>
      <p>Clique em <gui>Lixeira</gui> na barra lateral. Se você não vê a barra lateral, clique em <gui>Arquivos</gui> na barra superior e escolha <gui>Barra lateral</gui>.</p>
    </item>
    <item>
      <p>Se o seu arquivo excluído estiver ali, clique nele e selecione <gui>Restaurar</gui>. Ele será restaurado à pasta de onde foi excluído.</p>
    </item>
  </steps>

  <p>Se você excluiu o arquivo usando <keyseq><key>Shift</key><key>Delete</key></keyseq> ou pela linha de comando, o arquivo foi excluído permanentemente. Arquivos que foram excluídos permanentemente não podem ser restaurados da <gui>Lixeira</gui>.</p>

  <p>Existem algumas ferramentas de recuperação disponíveis que, às vezes, são capazes de recuperar arquivos que foram permanentemente excluídos. No entanto, elas não são muito fáceis de usar. Se você excluiu um arquivo assim por acidente, talvez seja melhor pedir conselho em um fórum de suporte para ver se você consegue recuperá-lo.</p>

</page>
