<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="ca">

  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.6.1" date="2012-11-10" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Feu una foto o registreu un vídeo del que està passant a la vostra pantalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Captures i enregistraments de pantalla</title>

  <p>Podeu fer una imatge de la vostra pantalla (una <em>captura de pantalla</em>) o gravar un vídeo del que està passant a la pantalla (un <em>enregistrament de pantalla</em>). Això és útil si, per exemple, voleu mostrar a algú com fer alguna cosa a l'ordinador. Les captures de pantalla i els enregistraments són fitxers d'imatge i de vídeo normals, de manera que podeu enviar-les per correu electrònic i compartir-les a la web.</p>

<section id="screenshot">
  <title>Fer una captura de pantalla</title>

  <steps>
    <item>
      <p>Obrir <app>Captura de pantalla</app> des de la vista general d'<gui xref="shell-introduction#activities">Activitats</gui>.</p>
    </item>
    <item>
      <p>A la finestra de <app>Captura de pantalla</app>, seleccioneu si s'ha d'agafar tota la pantalla, la finestra actual o una àrea de la pantalla. Establiu un retard si heu de seleccionar una finestra o configurar l'escriptori per a la captura de pantalla. A continuació, trieu els efectes que desitgeu.</p>
    </item>
    <item>
       <p>Feu clic a <gui>Fer una captura de pantalla</gui>.</p>
       <p>Si seleccioneu <gui>Seleccionar l'àrea a agafar</gui>, el punter es transforma en una creu. Feu clic i arrossegueu l'àrea que voleu per a la captura de pantalla.</p>
    </item>
    <item>
      <p>A la finestra <gui>Desar la captura</gui>, entreu un nom de fitxer i trieu una carpeta, aleshores feu clic a <gui>Desar</gui>.</p>
      <p>Com a alternativa, importeu la captura de pantalla directament a una aplicació d'edició d'imatges sense haver-la de desar primer. Feu clic a <gui>Copiar a l'escriptori</gui> aleshores enganxeu la imatge a l'altra aplicació o arrossegueu la miniatura de la captura de pantalla a l'aplicació.</p>
    </item>
  </steps>

  <section id="keyboard-shortcuts">
    <title>Dreceres de teclat</title>

    <p>Feu ràpidament una captura de pantalla de l'escriptori, una finestra, o una àrea de pantalla, mitjançant aquestes dreceres de teclat globals:</p>

    <list style="compact">
      <item>
        <p><key>Prt Scrn</key> per fer una captura de pantalla de l'escriptori.</p>
      </item>
      <item>
        <p><keyseq><key>Alt</key><key>Prt Scrn</key></keyseq> per fer una captura de pantalla d'una finestra.</p>
      </item>
      <item>
        <p><keyseq><key>Shift</key><key>Prt Scrn</key></keyseq> per fer una captura de pantalla d'una àrea que seleccioneu.</p>
      </item>
    </list>

    <p>Quan feu servir una drecera de teclat, la imatge es desarà automàticament a la vostra carpeta <file>Imatges</file> a la vostra carpeta d'inici amb un nom que comença per <file>Screenshot</file> i inclou la data i l'hora que es va fer.</p>
    <note style="note">
      <p>Si no teniu una carpeta <file>Imatges</file>, les imatges es desaran a la vostra carpeta d'inici.</p>
    </note>
    <p>Mantingueu premuda la tecla <key>Ctrl</key>amb qualsevol dels accessos directes anteriors per copiar la imatge de captura de pantalla al porta-retalls en comptes de desar-la.</p>
  </section>

</section>

<section id="screencast">
  <title>Fer un enregistrament de pantalla</title>

  <p>Podeu fer un enregistrament de vídeo del que està passant a la pantalla:</p>

  <steps>
    <item>
      <p>Premeu <keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq> per començar a gravar el que hi ha a la pantalla.</p>
      <p>Es mostra un cercle vermell a l'extrem superior dret de la pantalla quan l'enregistrament està en curs.</p>
    </item>
    <item>
      <p>Un cop hàgeu acabat, premeu <keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq> de nou per aturar l'enregistrament.</p>
    </item>
    <item>
      <p>El vídeo es guarda automàticament a la vostra carpeta <file>Videos</file> a la vostra carpeta s'inici, amb un nom que comença per <file>Screencast</file> i inclou la data i hora en que es va fer.</p>
    </item>
  </steps>

  <note style="note">
    <p>Si no teniu una carpeta <file>Vídeos</file>, els vídeos es desaran a la vostra carpeta d'inici.</p>
  </note>

</section>

</page>
