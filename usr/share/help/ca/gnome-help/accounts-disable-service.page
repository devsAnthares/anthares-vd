<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="ca">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alguns comptes en línia es poden utilitzar per accedir a diversos serveis (com ara el calendari i el correu electrònic). Podeu controlar quins d'aquests serveis poden ser utilitzats per les aplicacions.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Controleu quins serveis en línia es pot utilitzar per accedir a un compte</title>

  <p>Alguns tipus de proveïdors de comptes en línia permeten accedir a diversos serveis amb un mateix compte d'usuari. Per exemple, els comptes de Google proporcionen accés al calendari, correu electrònic, contactes i xat. És possible que vulgueu utilitzar el vostre compte per a alguns serveis, però no d'altres. Per exemple, és possible que vulgueu utilitzar el vostre compte de Google per correu electrònic, però no per xatejar si teniu un compte en línia diferent que feu servir per xatejar.</p>

  <p>Podeu desactivar algun dels serveis que es proporcionen a tots els comptes en línia: </p>

  <steps>
    <item>
      <p>Obriu el resum de les <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Comptes en línia</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Comptes en línia</gui> per obrir el panell.</p>
    </item>
    <item>
      <p>Seleccioneu el compte que voleu canviar de la llista de la dreta.</p>
    </item>
    <item>
      <p>Una llista de serveis disponibles amb aquest compte es mostrarà a <gui>Usar per</gui>. Veure <link xref="accounts-which-application"/> per veure quines aplicacions poden accedir a quins serveis.</p>
    </item>
    <item>
      <p>Desactiveu qualsevol dels serveis que no vulgueu utilitzar.</p>
    </item>
  </steps>

  <p>Una vegada que un servei s'ha desactivat per a un compte, les aplicacions de l'ordinador no podran utilitzar el compte per connectar-se a aquest servei.</p>

  <p>Per activar un servei que hàgeu desactivat, torneu al quadre de <gui>Comptes en línia</gui> i activeu-lo.</p>

</page>
