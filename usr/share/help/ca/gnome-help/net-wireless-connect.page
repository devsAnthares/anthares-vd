<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-connect" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Vés a Internet — sense fil.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Connecteu-vos a una xarxa sense fil</title>

<p>Si teniu un ordinador amb accés sense fil, podeu connectar-vos a una xarxa sense fil que tingueu a l'abast per obtenir accés a Internet, veure fitxers compartits a la xarxa, etc.</p>

<steps>
  <item>
    <p>Obriu el <gui xref="shell-introduction#yourname">menú de sistema</gui> des de la part dreta a la barra superior.</p>
  </item>
  <item>
    <p>Seleccioneu <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/> Wi-Fi No Connectat</gui>. S'expandirà la secció Wi-Fi del menú.</p>
  </item>
  <item>
    <p>Feu clic a <gui>Seleccionar Xarxa</gui>.</p>
  </item>
  <item>
    <p>Feu clic al nom de la xarxa que vulgueu i, a continuació, feu clic a <gui>Connectar</gui>.</p>
    <p>Si el nom de la xarxa no es troba a la llista, proveu de fer clic a <gui>Més</gui> per veure si la xarxa està a la llista. Si encara no veu la xarxa, és possible que estigui fora de rang, o que la xarxa <link xref="net-wireless-hidden">estigui oculta</link>.</p>
  </item>
  <item>
    <p>Si la xarxa està protegida per una contrasenya (<link xref="net-wireless-wepwpa">clau de xifratge</link>), introduïu la contrasenya quan se us demani i feu clic a <gui>Connectar</gui>.</p>
    <p>Si no coneixeu la clau, pot estar escrita a la part inferior de l'encaminador sense fil, o de l'estació base, o en el manual d'instruccions, o bé haureu de preguntar a la persona que administra la xarxa sense fil.</p>
  </item>
  <item>
    <p>La icona de la xarxa canviarà d'aparença a mesura que l'ordinador intenti connectar-se a la xarxa.</p>
  </item>
  <item>
    <p>Si la connexió s'estableix correctament, la icona canviarà a un punt amb diverses barres corbes a sobre (<media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>). Més barres indiquen una connexió més forta a la xarxa. Menys barres signifiquen que la connexió és més feble i pot ser que no sigui molt fiable.</p>
  </item>
</steps>

  <p>Si la connexió no s'ha realitzat correctament, pot ser que se us demani la contrasenya de nou o que se us informi que la connexió s'ha desconnectat. Hi ha diverses coses que ho podrien haver provocat. Podríeu haver introduït una contrasenya incorrecta, el senyal podria ser massa feble, o la targeta de xarxa sense fil de l'ordinador podria tenir un problema, per exemple. Veure <link xref="net-wireless-troubleshooting"/> per a més ajuda.</p>

  <p>Una connexió més forta a una xarxa sense fil no significa necessàriament que tingueu una connexió a Internet més ràpida o que tingui velocitats de descàrrega més ràpides. La connexió sense fil connecta l'ordinador al <em>dispositiu que proporciona la connexió a Internet (com un encaminador o mòdem), però les dues connexions són realment diferents, i així s'executaran a diferents velocitats.</em></p>

</page>
