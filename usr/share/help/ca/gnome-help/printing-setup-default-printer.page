<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Trieu la impressora que utilitzeu amb més freqüència.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Establiu la impressora predeterminada</title>

  <p>Si teniu més d'una impressora disponible, podeu seleccionar quina serà la vostra impressora predeterminada. És possible que vulgueu triar la impressora que utilitzeu més sovint.</p>

  <note>
    <p>Necessiteu <link xref="user-admin-explain">privilegis d'administrador</link> al sistema per la impressora predeterminada.</p>
  </note>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activititats</gui> i comenceu a escriure <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Seleccioneu la impressora predeterminada de la llista d'impressores disponibles.</p>
    </item>
    <item>
      <p>Feu clic al botó <gui>Desbloquejar</gui> a la cantonada superior dreta i introduïu la vostra contrasenya.</p>
    </item>
    <item>
      <p>Seleccioneu el botó de selecció <gui>Impressora per defecte</gui>.</p>
    </item>
  </steps>

  <p>Quan imprimiu en una aplicació, la impressora predeterminada s'utilitza automàticament, tret que trieu una altra.</p>

</page>
