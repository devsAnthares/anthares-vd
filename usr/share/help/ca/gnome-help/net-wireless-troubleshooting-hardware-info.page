<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="ca">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Col·laboradors a la wiki de documentació d'Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>És possible que necessiteu detalls com ara el número de model de l'adaptador en els passos següents de la resolució de problemes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Solució de problemes de xarxa sense fil</title>
  <subtitle>Recopileu informació sobre el vostre maquinari de xarxa</subtitle>

  <p>En aquest pas, recopilarà informació sobre el vostre dispositiu de xarxa sense fil. La manera de solucionar molts dels problemes del dispositiu depèn del model de fabricació i model de l'adaptador, de manera que haureu de prendre nota d'aquests detalls. També pot ser útil tenir alguns dels elements que venen amb l'ordinador, com ara els discs d'instal·lació del controlador del dispositiu. Busqueu els següents elements, si encara els teniu:</p>

  <list>
    <item>
      <p>L'embalatge i les instruccions dels vostres dispositius sense fil (especialment la guia d'usuari del vostre encaminador)</p>
    </item>
    <item>
      <p>El disc que conté controladors per a l'adaptador sense fil (fins i tot si només conté controladors de Windows)</p>
    </item>
    <item>
      <p>Els fabricants i els números de model de l'ordinador, adaptador sense fil i encaminador. Normalment, aquesta informació es troba a la part inferior o al revers del dispositiu.</p>
    </item>
    <item>
      <p>Qualsevol versió o número de revisió que pugui estar impresa als dispositius de xarxa o al seu embalatge. Aquests poden ser especialment útils, així que mireu-ho amb atenció.</p>
    </item>
    <item>
      <p>Qualsevol cosa al disc del controlador que identifiqui el dispositiu, la versió del "firmware" o els components (chipset) que utilitza.</p>
    </item>
  </list>

  <p>Si és possible, intenteu accedir a una connexió d'Internet alternativa per treballar, de manera que pugueu descarregar programari i controladors si fos necessari. (Connectar l'ordinador directament a l'encaminador amb un cable de xarxa Ethernet és una manera de proporcionar-ho, però només connecteu-lo quan ho necessiteu).</p>

  <p>Un cop tingueu tants d'aquests elements com sigui possible, feu clic a <gui>Següent</gui>.</p>

</page>
