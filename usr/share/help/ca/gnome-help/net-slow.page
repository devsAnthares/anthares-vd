<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="ca">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>És possible que es descarreguin altres coses, podria tenir una connexió deficient, o podria ser una hora del dia molt plena.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Internet sembla lent</title>

  <p>Si utilitzeu Internet i sembla lent, hi ha moltes coses que poden provocar aquesta lentitud.</p>

  <p>Intenteu tancar el vostre navegador i tornar-lo a obrir, i desconnectar Internet i tornar-lo a connectar de nou. (Fer això restablirà moltes coses que podrien estar provocant que Internet funcioni lentament).</p>

  <list>
    <item>
      <p><em style="strong">Hora del dia molt ocupada</em></p>
      <p>Els proveïdors de serveis d'Internet solen establir connexions a Internet perquè es comparteixin diverses llars. Tot i que es connecta per separat, a través de la seva pròpia línia telefònica o connexió per cable, la connexió amb la resta d'Internet a la central d'intercanvi telefònica pot ser compartida. Si és aquest el cas, i molts dels vostres veïns estan utilitzant Internet al mateix temps que vosaltres, podeu observar una baixada de velocitat de la connexió. És més probable que experimenti això en moments en què els vostres veïns estiguin probablement a Internet (a la tarda, per exemple).</p>
    </item>
    <item>
      <p><em style="strong">Descarregant moltes coses alhora</em></p>
      <p>Si vostè o algú que utilitza la vostra connexió a Internet descarrega diversos fitxers alhora o veu vídeos, és possible que la connexió a Internet no sigui prou ràpida com per mantenir la demanda. En aquest cas, es notarà més lenta.</p>
    </item>
    <item>
      <p><em style="strong">Connexió poc fiable</em></p>
      <p>Algunes connexions a Internet són poc fiables, especialment les temporals o en àrees d'alta demanda. Si es troba en una cafeteria molt plena o en un centre de conferències, la connexió a Internet podria estar massa col·lapsada o simplement ser poc fiable.</p>
    </item>
    <item>
      <p><em style="strong">Senyal baixa de connexió sense fil</em></p>
      <p>Si esteu connectat a Internet mitjançant connexió sense fil (Wi-Fi), marqueu la icona de la xarxa a la barra superior per veure si teniu un bon senyal. Si no, Internet pot ser que vagi lent perquè no té una senyala molt forta.</p>
    </item>
    <item>
      <p><em style="strong">Utilitzeu una connexió a internet mòbil lenta</em></p>
      <p>Si teniu una connexió a Internet mòbil i observeu que és lenta, és possible que hagi passat a una zona on la recepció del senyal sigui deficient. Quan això passa, la connexió a Internet canviarà automàticament d'una connexió ràpida de "banda ampla mòbil" com 3G a una connexió més fiable, però més lenta, com GPRS.</p>
    </item>
    <item>
      <p><em style="strong">El navegador web té un problema</em></p>
      <p>De vegades, els navegadors web troben un problema que els fa funcionar lentament. Això podria ser degut a diversos motius: podria haver visitat un lloc web que hagi costat molt de carregar, o potser ha tingut el navegador obert durant molt de temps, per exemple. Intenteu tancar totes les finestres del navegador i obrir el navegador de nou per veure si hi ha alguna diferència.</p>
    </item>
  </list>

</page>
