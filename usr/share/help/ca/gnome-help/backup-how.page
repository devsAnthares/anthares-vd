<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="ca">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Com utilitzar el Déjà Dup (o algun altre programa de còpies de seguretat) per fer les còpies dels vostres fitxers i paràmetres de configuració valuosos i protegir-vos de possibles pèrdues.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Fer una còpia de seguretat</title>

  <p>La manera més senzilla de fer les còpies de seguretat dels fitxers i paràmetres de configuració és deixar que una aplicació de còpies de seguretat se'n faci càrrec. Hi ha nombroses aplicacions d'aquest tipus, com per exemple el <app>Déjà Dup</app>.</p>

  <p>L'ajuda de l'aplicació de còpies de seguretat que hàgiu escollit us indicarà com configurar l'aplicació per fer les còpies, i què haureu de fer per recuperar la informació que teniu copiada.</p>

  <p>Una altra opció és simplement <link xref="files-copy">copiar els fitxers</link> a una ubicació segura, com per exemple un disc dur extern, un altre ordinador de la xarxa, o un llapis USB. Els vostres <link xref="backup-thinkabout">fitxers personals</link> i paràmetres de configuració solen estar al directori personal i, per tant, els podeu copiar d'allà.</p>

  <p>La quantitat d'informació de la que podeu fer una còpia de seguretat depèn de la mida del dispositiu d'emmagatzemament que utilitzeu. Si disposeu de prou espai al dispositiu on realitzareu la còpia, el millor que podeu fer és copiar tot el directori personal, amb les següents excepcions:</p>

<list>
 <item><p>Fitxers que ja s'han copiat en un altre lloc, com ara un CD, DVD o altres suports extraïbles.</p></item>
 <item><p>Fitxers que podeu recrear fàcilment. Per exemple, si sou programador, no heu de fer una còpia de seguretat dels fitxers que es produeixen quan compileu els vostres programes. En lloc d'això, assegureu-vos que feu una còpia de seguretat dels fitxers font originals.</p></item>
 <item><p>Qualsevol fitxer de la paperera. Trobareu el directori de la Paperera a <cmd>~/.local/share/Trash</cmd>.</p></item>
</list>

</page>
