<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer" group="#first"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configureu una impressora que estigui connectada a l'ordinador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Configureu una impressora local</title>

  <p>El vostre sistema pot reconèixer diversos tipus d'impressores automàticament una vegada que estiguin connectades. La majoria d'impressores estan connectades amb un cable USB que es connecta a l'ordinador.</p>

  <note style="tip">
    <p>No heu de seleccionar si ara voleu instal·lar una impressora en xarxa o local. Es llisten en una finestra.</p>
  </note>

  <steps>
    <item>
      <p>Assegureu-vos que la impressora està activada.</p>
    </item>
    <item>
      <p>Connecteu la impressora al vostre sistema mitjançant el cable adequat. Podeu veure l'activitat a la pantalla a mesura que el sistema cerca els controladors, i se us pot demanar d'autentificar-vos per instal·lar-los.</p>
    </item>
    <item>
      <p>Apareixerà un missatge quan el sistema hagi acabat d'instal·lar la impressora. Seleccioneu <gui>Imprimir una pàgina de prova</gui> per imprimir una pàgina de prova, o <gui>Opcions</gui> per fer canvis addicionals a la configuració de la impressora.</p>
    </item>
  </steps>

  <p>Si la impressora no s'ha configurat automàticament, la podeu afegir a la configuració de les impressores:</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activititats</gui> i comenceu a escriure <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Feu clic al botó <gui>Desbloquejar</gui> a la cantonada superior dreta i introduïu la vostra contrasenya.</p>
    </item>
    <item>
      <p>Feu clic al botó <gui>+</gui>.</p>
    </item>
    <item>
      <p>A la finestra emergent, seleccioneu la vostra nova impressora. Feu clic a <gui>Afegir</gui>.</p>
    </item>
  </steps>

  <p>Si la vostra impressora no apareix a Afegir impressores, heu d'instal·lar els controladors d'impressió.</p>

  <p>Un cop instal3lada la impressora, podeu voler <link xref="printing-setup-default-printer">Canviar la impressora per defecte</link>.</p>

</page>
