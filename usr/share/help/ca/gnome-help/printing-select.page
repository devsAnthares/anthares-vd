<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-select" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imprimiu només pàgines concretes o només una sèrie de pàgines.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Imprimiu només certes pàgines</title>

  <p>Per imprimir només algunes pàgines del document:</p>

  <steps>
    <item>
      <p>Obriu el diàleg d'impressió prement <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>A la pestanya <gui>General</gui>, trieu <gui>Pàgines</gui> de la secció <gui>Rangs</gui>.</p>
    </item>
    <item><p>Escriviu al quadre de text, separats per comes, els números de les pàgines que voleu imprimir, Utilitzeu un guió per indicar un rang de pàgines.</p></item>
  </steps>

  <note>
    <p>Per exemple, si introduïu "1,3,5-7,9" al quadre de text <gui>Pàgines</gui>, s'imprimiran les pàgines 1,3,5,6,7 i 9.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
