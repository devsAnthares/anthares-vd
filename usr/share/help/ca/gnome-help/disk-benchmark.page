<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="ca">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Executeu "benchmarks" al vostre disc dur per comprovar-ne la rapidesa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Proveu el rendiment del vostre disc dur</title>

  <p>Per testejar la velocitat del vostre disc dur:</p>

  <steps>
    <item>
      <p>Obriu <app>Discs</app> des de la descripció general d'<gui xref="shell-introduction#activities">Activitats</gui>.</p>
    </item>
    <item>
      <p>Trieu el disc de la llista al panell esquerre.</p>
    </item>
    <item>
      <p>Feu clic al botó de menú i seleccioneu <gui>Disc de referència</gui> des del menú.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Iniciar test…</gui> i ajusteu el <gui>Ratio de Transferència</gui> i <gui>Temps d'Accés</gui> als paràmetres desitjats.</p>
    </item>
    <item>
      <p>Feu clic <gui>Iniciar el test</gui> per comprovar com de ràpid es poden llegir les dades des del disc. Poden ser necessaris <link xref="user-admin-explain">Privilegis administratius</link>. Entreu la vostra contrasenya, o la contrasenya pel compte d'administrador.</p>
      <note>
        <p>Si està seleccionat <gui>Efectuar un test d'escriptura</gui>, el test comprovarà com de ràpid es poden llegir i escriure les dades al disc. Aquesta opció trigarà més a completar-se.</p>
      </note>
    </item>
  </steps>

  <p>Quan finalitzi la prova, els resultats apareixeran en un gràfic. Els punts verds i les línies de connexió indiquen les mostres preses; Aquests corresponen a l'eix de la dreta, mostrant el temps d'accés, traçat contra l'eix inferior, que representa el percentatge temps de transcorregut durant el test. La línia blava representa les taxes de lectura, mentre que la línia vermella representa les taxes d'escriptura; es mostren com a velocitats de dades d'accés a l'eix esquerre, representades en percentatge del disc recorregut, des de l'exterior fins a l'eix, al llarg de l'eix inferior.</p>

  <p>A sota del gràfic, es mostren els valors per a les taxes de lectura i escriptura mínimes, màximes i mitjanes, el temps d'accés mitjà i el temps transcorregut des de l'última prova de referència.</p>

</page>
