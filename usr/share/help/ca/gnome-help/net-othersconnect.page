<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="ca">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podeu desar la configuració (com ara la contrasenya) d'una connexió de xarxa perquè tots els usuaris de l'ordinador puguin connectar-s'hi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Altres usuaris no poden connectar-se a Internet</title>

  <p>Quan configureu una connexió de xarxa, normalment tots els altres usuaris de l'ordinador podran utilitzar-la. Si no es comparteix la informació de la connexió, hauríeu de comprovar la configuració de la connexió.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Xarxa</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Xarxa</gui> per obrir el panell.</p>
    </item>
    <item>
      <p>Seleccionar <gui>Wi-Fi</gui> de la llista a l'esquerra.</p>
    </item>
    <item>
      <p>Feu clic al botó <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">configuració</span></media> per obrir els detalls de la connexió.</p>
    </item>
    <item>
      <p>Seleccioneu <gui>Identitat</gui> del panell de l'esquerra.</p>
    </item>
    <item>
      <p>A la part inferior del panell <gui>Identitat</gui>, marqueu l'opció <gui>Posar a disposició d'altres usuaris</gui> per permetre que altres usuaris utilitzin la connexió de xarxa.</p>
    </item>
    <item>
      <p>Premeu <gui style="button">Aplicar</gui> per desar els canvis.</p>
    </item>
  </steps>

  <p>Altres usuaris de l'ordinador podran utilitzar aquesta connexió sense introduir més detalls.</p>

  <note>
    <p>Qualsevol usuari pot canviar aquesta configuració.</p>
  </note>

</page>
