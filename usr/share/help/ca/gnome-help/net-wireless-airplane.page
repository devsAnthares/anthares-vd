<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Obriu la configuració de xarxa i canvieu el mode d'avió a ON.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Apagar xarxa sense fil (mode avió)</title>

<p>Si teniu l'ordinador en un avió (o en una altra àrea on no es permeten connexions sense fil), cal que la desactiveu. També la podeu desactivar per altres motius (per estalviar energia de la bateria, per exemple).</p>

  <note>
    <p>Utilitzar <em>Mode avió</em> desactivarà completament totes les connexions sense fil, incloses les connexions WiFi, 3G i Bluetooth.</p>
  </note>

  <p>Activar mode avió:</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Xarxa</gui>.</p>
    </item>
    <item>
      <p>Feu clic a <gui>Xarxa</gui> per obrir el panell.</p>
    </item>
    <item>
      <p>Canvieu el <gui>Mode Avió</gui> a <gui>ON</gui>. Això desactivarà la vostra connexió sense fil fins que torneu a deshabilitar el mode avió.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Podeu desactivar la vostra connexió Wi-Fi des del <gui xref="shell-introduction#yourname">menú del sistema</gui> fent clic al nom de la connexió i seleccionant <gui>Apagar</gui>.</p>
  </note>

</page>
