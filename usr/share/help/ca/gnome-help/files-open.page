<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="ca">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Obriu fitxers amb una aplicació que no sigui la predeterminada per a aquest tipus de fitxer. També podeu canviar el valor per defecte.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Obriu fitxers amb altres aplicacions</title>

  <p>Quan feu doble clic (o feu clic amb el botó del mig) a un fitxer al gestor de fitxers, s'obrirà amb l'aplicació per defecte d'aquest tipus de fitxer. Podeu obrir-lo amb una aplicació diferent, cercar aplicacions en línia, o establir l'aplicació predeterminada per a tots els fitxers del mateix tipus.</p>

  <p>Per obrir un fitxer amb una aplicació diferent de la predeterminada, feu clic amb el botó secundari en el fitxer i seleccioneu l'aplicació que vulgueu a la part superior del menú. Si no veieu l'aplicació que voleu, seleccioneu <gui>Obrir amb una altra aplicació</gui>. Per defecte, l'administrador de fitxers només mostra aplicacions conegudes per gestionar el fitxer. Per examinar totes les aplicacions de l'ordinador, feu clic a <gui>Veure totes les Aplicacions</gui>.</p>

<p>Si encara no trobeu l'aplicació que voleu, podeu cercar més aplicacions fent clic <gui>Trobar aplicacions noves</gui>. L'administrador de fitxers buscarà en línia paquets que continguin aplicacions que se sap que gestionen fitxers d'aquest tipus.</p>

<section id="default">
  <title>Canviar l'aplicació per defecte</title>
  <p>Podeu canviar l'aplicació predeterminada que s'utilitza per obrir els fitxers d'un determinat tipus. Això us permetrà obrir la vostra aplicació preferida quan feu doble clic per obrir un fitxer. Per exemple, és possible que vulgueu que el vostre reproductor de música preferit s'obri quan feu doble clic a un fitxer MP3.</p>

  <steps>
    <item><p>Seleccioneu un fitxer del tipus l'aplicació predeterminada que vulgueu canviar. Per exemple, per canviar quina aplicació s'utilitza per obrir fitxers MP3, seleccioneu un fitxer <file>.mp3</file>.</p></item>
    <item><p>Feu clic a l'arxiu amb el botó dret i seleccioneu <gui>Propietats</gui>.</p></item>
    <item><p>Seleccioneu la pestanya <gui>Obrir amb</gui>.</p></item>
    <item><p>Seleccioneu l'aplicació que vulgueu i feu clic a <gui>Establir com a predeterminada</gui>.</p>
    <p>Si <gui>Altres Aplicacions</gui> conté una aplicació que voleu utilitzar sovint, però no voleu fer-la predeterminada, seleccioneu aquesta aplicació i feu clic a <gui>Afegir</gui>. Això l'afegirà a <gui>Aplicacions Recomanades</gui>. A continuació, podreu utilitzar aquesta aplicació fent clic amb el botó de la dreta al fitxer i seleccionant-la a la llista.</p></item>
  </steps>

  <p>Això canvia l'aplicació per defecte no només per al fitxer seleccionat, sinó per a tots els fitxers del mateix tipus.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
