<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Assegureu-vos que teniu el sobre ben encarat i que heu triat la mida del paper correcte.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Imprimeix sobres</title>

  <p>La majoria d'impressores us permeten imprimir directament en un sobre. Això és especialment útil si teniu moltes cartes per enviar, per exemple.</p>

  <section id="envelope">
    <title>Imprimir sobre sobres</title>

  <p>Hi ha dues coses que cal comprovar quan intenteu imprimir en un sobre.</p>
  <p>La primera és que la vostra impressora sàpiga quina és la mida del sobre. Premeu <keyseq><key>Ctrl</key><key>P</key></keyseq> el diàleg d'impressió, aneu a la pestanya <gui>Configuració de la pàgina</gui> i trieu el <gui>Tipus de paper</gui> com “Sobre” si podeu. Si no podeu fer-ho, mireu de canviar la <gui>Mida del paper</gui> a mida sobre (per exemple, <gui>C5</gui>). El paquet de sobres ha de dir quina mida tenen; La majoria dels sobres vénen en mides estàndard.</p>

  <p>En segon lloc, cal assegurar-se que els sobres es carreguin pel costat correcte a la safata de la impressora. Comproveu el manual de la impressora, o intenteu imprimir un sobre i verifiqueu en quina part s'imprimeixi per veure quina és la forma correcta.</p>

  <note style="warning">
    <p>Algunes impressores no estan dissenyades per poder imprimir sobres, especialment algunes impressores làser. Consulteu el manual de la vostra impressora per veure si accepta sobres. En cas contrari, podeu malmetre la impressora introduint-ne un.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
