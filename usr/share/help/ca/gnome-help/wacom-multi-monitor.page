<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="ca">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Assigneu tauleta Wacom a un monitor específic.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Trieu un monitor</title>

<steps>
  <item>
    <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
  </item>
  <item>
    <p>Féu clic a <gui>Configuració</gui>.</p>
  </item>
  <item>
    <p>Féu clic a <gui>Dispositius</gui> a la barra lateral.</p>
  </item>
  <item>
    <p>Click <gui>Wacom Tablet</gui> in the sidebar to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Si no es detecta cap tauleta, se us demanarà <gui>Connecteu o engegueu la vostra tauleta Wacom</gui>. Feu clic al vincle <gui>Configuració Bluetooth</gui> per connectar la tauleta.</p></note>
  </item>
  <item><p>Clic <gui>Assignar al Monitor…</gui></p></item>
  <item><p>Validar <gui>Assignar a un únic monitor</gui>.</p></item>
  <item><p>Al costat de <gui>Sortida</gui>, seleccioneu el monitor que voleu rebi entrada de la vostra tauleta gràfica.</p>
     <note style="tip"><p>Només es podran seleccionar els monitors que estiguin configurats.</p></note>
  </item>
  <item>
    <p>Switch <gui>Keep aspect ratio (letterbox)</gui> to <gui>ON</gui> to
    match the drawing area of the tablet to the proportions of the monitor.
      This setting, also called <em>force proportions</em>,
      “letterboxes” the drawing area on a tablet to correspond more
      directly to a display. For example, a 4∶3 tablet would be mapped so that
      the drawing area would correspond to a widescreen display.</p>
  </item>
  <item><p>Feu clic a <gui>Tancar</gui>.</p></item>
</steps>

</page>
