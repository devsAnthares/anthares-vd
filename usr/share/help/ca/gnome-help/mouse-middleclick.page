<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="ca">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Utilitzeu el botó del mig del ratolí per obrir les aplicacions, obrir pestanyes i més.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Clic al mig</title>

<p>Molts ratolins i algunes tauletes tenen un botó al mig del ratolí. En un ratolí amb una roda de desplaçament, normalment podeu prémer la roda per fer-hi clic. Si no teniu un botó al mig del ratolí, podeu prémer els botons esquerre i dret al mateix temps per fer clic.</p>

<p>A les tauletes que admeten tocs simultanis amb els dits, podeu tocar amb tres dits alhora per fer clic amb el botó del mig. Heu <link xref="mouse-touchpad-click">d'activar els multi tocs</link>a la configuració de la tauleta per a què funcioni.</p>

<p>Moltes aplicacions fan servir el botó del mig per a les dreceres de clic avançades.</p>

<list>
  <item><p>A la descripció general de les <gui>Activitats</gui>, podeu obrir ràpidament una nova finestra per a una aplicació amb clic del mig. Simplement feu clic al mig a la icona de l'aplicació, ja sigui al tauler de l'esquerra o a la descripció general de les aplicacions. La descripció general de les aplicacions es mostra utilitzant el botó de graella del tauler.</p></item>

  <item><p>La majoria de navegadors web permeten obrir ràpidament enllaços a noves pestanyes amb el botó del mig del ratolí. Feu clic a qualsevol enllaç amb el botó central del ratolí i s'obrirà en una pestanya nova.</p></item>

  <item><p>Al gestor de fitxers, el clic del mig té dos rols. Si es fa clic a una carpeta, s'obrirà en una nova pestanya. Això imita el comportament dels navegadors web populars. Si es fa clic a un arxiu, s'obrirà el fitxer com si hagués fet doble clic.</p></item>
</list>

<p>Algunes aplicacions especialitzades permeten utilitzar el botó del mig del ratolí per a altres funcions. Busqueu a l'ajuda de la vostra aplicació <em>clic del mig</em> o <em>botó del mig</em>.</p>

</page>
