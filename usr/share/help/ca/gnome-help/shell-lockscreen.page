<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="ca">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>La decorativa i funcional pantalla de bloqueig proporciona informació útil.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Aguilera</mal:name>
      <mal:email>david.aguilera.moncusi@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>La pantalla de bloqueig</title>

  <p>La pantalla de bloqueig significa que podeu veure què passa mentre l'ordinador està bloquejat, i us permet obtenir un resum del que ha estat passant mentre heu estat fora. La cortina de la pantalla de bloqueig mostra una imatge atractiva a la pantalla mentre l'ordinador està bloquejat i proporciona informació útil:</p>

  <list>
    <item><p>el nom d'usuari de la sessió iniciada</p></item>
    <item><p>data i hora, i certes notificacions</p></item>
    <item><p>estat de la bateria i de la xarxa</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Per desbloquejar l'ordinador, aixequeu la cortina de la pantalla de bloqueig arrossegant-la cap amunt amb el cursor o prement <key>Esc</key> o <key>Retorn</key>. Això mostrarà la pantalla d'inici de sessió, on podeu introduir la vostra contrasenya per desbloquejar-la. Com a alternativa, simplement comenceu a escriure la vostra contrasenya i la cortina s'aixecarà automàticament mentre escriviu. També podeu canviar els usuaris si l'equip està configurat per a més d'un.</p>

</page>
