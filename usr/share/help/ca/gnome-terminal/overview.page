<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="overview" xml:lang="ca">
  <info>
    <revision version="0.1" date="2013-01-10" status="draft"/>
    <link type="guide" xref="index"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Que és un terminal?</desc>
  </info>

  <title>Resum de terminal</title>

  <p><app>Terminal</app> és un programa terminal per a <gui>GNOME</gui>. Els següents termes i les seves descripcions us ajudaran a familiaritzar-vos amb el <app>Terminal</app> i les seves capacitats.</p>

  <terms>
    <item>
      <title>Un terminal</title>
      <p>A terminal is a text input point in a computer that is also called
       the Command Line Interface (CLI).</p>
    </item>
    <item>
      <title>Terminals físics</title>
      <p>IBM 3270, VT100 i molts altres són terminals de maquinari que ja no es fabriquen com a dispositius físics. Per emular aquests terminals, hi ha emuladors de terminal.</p>
    </item>
    <item>
      <title>Emuladors de terminal</title>
      <p>L'emulació és l'habilitat d'un programa d'ordinador d'imitar un altre programa o dispositiu.</p>

      <p>A terminal emulator, also called tty, is a software program that emulates
      a video terminal in modern computers that use graphical user interfaces
      and provide interactive access to applications that run only in the
      command line environments. These applications may be running either on the
      same machine or on a different one via <app>telnet</app>, <app>ssh</app>,
      or <app>dial-up</app>.</p>
    </item>
    <item>
      <title>VTE</title>
      <p>L'entorn terminal virtual (VTE) és un emulador de terminal que emula un terminal de text dins d'una interfície gràfica d'usuari (GUI). <app>Terminal</app> està basat en <app>VTE</app>. <app>VTE</app> té ginys que implementen un emulador de terminal completament funcional.</p>
    </item>
    <item>
      <title>Intèrpret d'ordres</title>
      <p>A <em>shell</em> is a program that provides an interface to invoke or
      “launch” commands or another program inside a terminal. It also allows you
      to view and browse the contents of directories. Popular shells include
      <app>bash</app>, <app>zsh</app>, <app>fish</app>.</p>
    </item>
    <item>
      <title>Seqüències d'escapament</title>
      <p>Una seqüència d'escapament és una sèrie de caràcters usats per canviar el significat d'una dada en un terminal. Les seqüències d'escapament són utilitzades quan un ordinador té un sol canal per enviar informació d'anada i tornada. Les seqüències d'escapament s'usen per distingir si la dada que s'envia és una ordre per a executar o informació per emmagatzemar o mostrar.</p>
    </item>

    <item>
      <title>Indicador</title>
      <p>A prompt is also called a <em>command prompt</em>. It is a sequence of
      characters used in the command line environment to indicate the readiness
      of the shell to accept commands.</p>

      <p>A prompt usually ends with the characters <sys>$</sys>, <sys>%</sys>,
      <sys>#</sys> or <sys>&gt;</sys> and includes
      information about the path of the present working directory. On Unix based
      systems, it is common for the prompt to end in a <sys>$</sys> or 
      <sys>#</sys> character depending on the user role such as <sys>$</sys>
      for user and <sys>#</sys> for superuser (also called <sys>root</sys>).
      </p>
    </item>
    <item>
      <title>Ordre</title>
      <p>Una entrada escrita a l'indicador per a ser executada s'anomena <em>ordre</em>. És una combinació del nom del programa amb paràmetres addicionals passats com a senyals per alterar l'execució del programa.</p>
    </item>
  </terms>

</page>
