<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="app-fonts" xml:lang="ca">

  <info>
    <revision version="0.1" date="2013-02-22" status="candidate"/>
    <link type="guide" xref="index#appearance"/>
    <link type="guide" xref="pref#profile"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Usa tipus de lletra del sistema o personalitzades per al vostre terminal.</desc>

  </info>

  <title>Canvia el tipus de lletra i l'estil</title>

  <p>Quan treballeu amb molt de text al <app>Terminal</app>, podeu preferir canviar la mida de la lletra a preferències. Teniu les següents opcions:</p>

  <section id="system-font">
  <title>Tipus de lletra d'amplada fixa del sistema</title>

  <p>Per usar tipus de lletra per defecte al sistema:</p>

  <steps>
    <item>
      <p>Open <guiseq><gui style="menu">Edit</gui>
      <gui style="menuitem">Preferences</gui></guiseq>.</p>
    </item>
    <item>
      <p>Your current profile is selected in the sidebar. If you wish to edit
      a different profile, click on its name.</p>
    </item>
    <item>
      <p>Select <gui style="tab">Text</gui>.</p>
    </item>
    <item>
      <p>Unselect <gui style="checkbox">Custom font</gui>.</p>
    </item>
  </steps>

  </section>

  <section id="custom-font">
  <title>Configura un tipus de lletra personalitzat</title>

  <p>Per configurar un tipus de lletra personalitzat i la mida:</p>

  <steps>
    <item>
      <p>Open <guiseq><gui style="menu">Edit</gui>
      <gui style="menuitem">Preferences</gui></guiseq>.</p>
    </item>
    <item>
      <p>Your current profile is selected in the sidebar. If you wish to edit
      a different profile, click on its name.</p>
    </item>
    <item>
      <p>Select <gui style="tab">Text</gui>.</p>
    </item>
    <item>
      <p>Select <gui style="checkbox">Custom font</gui>.</p>
    </item>
    <item>
      <p>Click on the button next to <gui style="checkbox">Custom font</gui>.</p>
    </item>
    <item>
      <p>Escriu el nom del tipus de lletra al camp de cerca o navega per la llista de tipus de lletres.</p>
    </item>
    <item>
      <p>Arrossegueu el control lliscant que és a sota de la llista del tipus de lletra per a configurar la mida del tipus de lletra. De forma alternativa, podeu escriure la mida del tipus de lletra dins del camp al costat del control lliscant, o prémer<gui style="button">+</gui>per augmentar la mida del tipus de lletra o <gui style="button">-</gui> per disminuir la mida del tipus de lletra seleccionat.</p>
    </item>
    <item>
      <p>Prem <gui style="button">Selecciona</gui> per aplicar els canvis. Per revertir els canvis i tornar enrere al diàleg previ, premeu <gui style="button">Cancel·la</gui>.</p>
    </item>
  </steps>

  </section>

  <section id="cell-spacing">
  <title>Line spacing and character spacing</title>

  <p><app>Terminal</app> allows you to pull its characters apart for improved
  readability. Line spacing and character spacing can be adjusted from 1.0
  (the regular look) to 2.0 (“double spacing”), including fractional values
  in between.</p>

  <steps>
    <item>
      <p>Open <guiseq><gui style="menu">Edit</gui>
      <gui style="menuitem">Preferences</gui></guiseq>.</p>
    </item>
    <item>
      <p>Your current profile is selected in the sidebar. If you wish to edit
      a different profile, click on its name.</p>
    </item>
    <item>
      <p>Select <gui style="tab">Text</gui>.</p>
    </item>
    <item>
      <p>To set the horizontal character spacing, adjust the multiplier number
      in front of <gui>width</gui>.</p>
    </item>
    <item>
      <p>To set the line spacing, adjust the multiplier number in front of
      <gui>height</gui>.</p>
    </item>
  </steps>

  </section>

</page>
