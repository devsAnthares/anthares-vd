<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koristite duže, složenije lozinke.</desc>
  </info>

  <title>Izaberite bezbednu lozinku</title>

  <note style="important">
    <p>Učinite vaše lozinke vama lake za pamćenje, ali vrlo teške drugima (uključujući računarske programe) da ih pogode.</p>
  </note>

  <p>Izbor dobre lozinke će pomoći očuvanju bezbednosti vašeg računara. Ako je vaša lozinka laka za pogoditi, neko bi mogao da je otkrije i da zadobije pristup vašim ličnim podacima.</p>

  <p>Neko bi čak mogao da upotrebi računare da sistemski pokuša da pogodi vašu lozinku, tako da čak i ona koja može ljudima biti teška da je pogode može biti krajnje laka računarima da je provale. Evo nekoliko saveta kako izabrati dobru lozinku:</p>
  
  <list>
    <item>
      <p>Koristite mešavinu velikih i malih slova, brojeva, simbola i razmaka u lozinkama. Ovo će ih učiniti težim za pogađanje; postoji veliki broj simbola za izbor, tako da će neko morati da proveri više mogućih lozinki kada bude pokušao da pogodi vašu.</p>
      <note>
        <p>Dobar način za izbor lozinke je da uzmete prvo slovo svake reči u rečenici koju možete da zapamtite. Rečenica može biti naziv filma, knjige, pesme ili nekog albuma. Na primer, „Bijelo Dugme: Ala je glupo zaboravit njen broj“ će postati „BD:Ajgznjb“ ili „bdajgznjb“ ili „bd: ajgnjzb“.</p>
      </note>
    </item>
    <item>
      <p>Učinite vašu lozinku što je moguće dužom. Što više znakova sadrži, više će vremena biti potrebno osobi ili računaru da je pogodi.</p>
    </item>
    <item>
      <p>Ne koristite nijednu reč koja se nalazi u rečnicima ili u nekom jeziku. Lozinkolomci će prvo njih isprobati. Najčešća lozinka je „lozinka“ — ovakvu lozinku će svako za čas da pogodi!</p>
    </item>
    <item>
      <p>Ne koristite nikakve lične podatke kao što je datum, broj registarske tablice, ili ime nekog člana porodice.</p>
    </item>
    <item>
      <p>Ne koristite nikakve imenice.</p>
    </item>
    <item>
      <p>Izaberite lozinku koja može biti brzo ukucana, da umanjite šanse da neko ko vas posmatra može da dozna šta ste upisali.</p>
      <note style="tip">
        <p>Nikada nigde nemojte da zapisujete vaše lozinke. Jer bi neko mogao da ih pronađe!</p>
      </note>
    </item>
    <item>
      <p>Koristite različite lozinke za različite stvari.</p>
    </item>
    <item>
      <p>Koristite različite lozinke za različite naloge.</p>
      <p>Ako koristite istu lozinku za sve vaše naloge, svako ko je bude otkrio biće u mogućnosti da pristupi svim vašim nalozima.</p>
      <p>Međutim, može biti teško pamtiti mnogo lozinki. Iako nije tako bezbedno kao korišćenje različitih lozinki za sve, možda je lakše da koristite jednu istu za stvari koje nisu važne (kao veb sajtovi), a jednu drugu za važne stvari (kao što je vaš bankovni račun na mreži i vaša e-pošta).</p>
   </item>
   <item>
     <p>Redovno menjajte vaše lozinke.</p>
   </item>
  </list>

</page>
