<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Šoba Tjagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pokrenite programe iz pregleda <gui>Aktivnosti</gui>.</desc>
  </info>

  <title>Početni programi</title>

  <p if:test="!platform:gnome-classic">Pomerite pokazivač miša u ugao <gui>Aktivnosti</gui> gore levo na ekranu da prikažete pregled <gui xref="shell-introduction#activities">Aktivnosti</gui>. To je mesto gde možete pronaći sve vaše programe. Pregled možete takođe da otvorite pritiskom na taster <link xref="keyboard-key-super">Super</link>.</p>
  
  <p if:test="platform:gnome-classic">Možete da pokrenete programe iz izbornika <gui xref="shell-introduction#activities">Programi</gui> u levom gornjem uglu ekrana, ili možete da koristite pregled <gui>Aktivnosti</gui> pritiskom na taster <key xref="keyboard-key-super">Super</key>.</p>

  <p>Postoji nekoliko načina za otvaranje programa kada ste u pregledu <gui>Aktivnosti</gui>:</p>

  <list>
    <item>
      <p>Počnite da upisujete naziv programa — pretraga počinje trenutno. (Ako do ovoga ne dođe, kliknite na polje za pretragu pri vrhu ekrana i počnite da upisujete.) Ako ne znate tačan naziv programa, pokušajte da upišete neki izraz koji se odnosi na njega. Kliknite na ikonicu programa da ga pokrenete.</p>
    </item>
    <item>
      <p>Neki programi imaju ikonice u <em>poletniku</em>, uspravnoj traci sa ikonicama na levoj strani pregleda <gui>Aktivnosti</gui>. Kliknite na neku od njih da pokrenete odgovarajući program.</p>
      <p>Ako imate programe koje koristite vrlo često, možete sami <link xref="shell-apps-favorites">da ih dodate poletniku</link>.</p>
    </item>
    <item>
      <p>Pritisnite dugme mreže na dnu poletnika. Videćete često korišćene programe ako je uključen pregled <gui style="button">Česti</gui>. Ako želite da pokrenete novi program, pritisnite dugme <gui style="button">Svi</gui> na dnu da prikažete sve programe. Pritisnite program da ga pokrenete.</p>
    </item>
    <item>
      <p>Možete da pokrenete program u posebnom <link xref="shell-workspaces">radnom prostoru</link> prevlačenjem njegove ikonice iz poletnika, i puštanjem u jedan od radnih prostora na desnoj strani ekrana. Program će se otvoriti u izabranom radnom prostoru.</p>
      <p>Možete da pokrenete program u <em>novom</em> radnom prostoru prevlačenjem njegove ikonice na prazan radni prostor na dno prebacivača radnih prostora, ili na mali jaz između dva radna prostora.</p>
    </item>
  </list>

  <note style="tip">
    <title>Brzo pokretanje naredbe</title>
    <p>Program možete pokrenuti na još jedan od načina, tako što ćete pritisnuti tastere <keyseq><key>Alt</key><key>F2</key></keyseq>, upisati njegov <em>naziv naredbe</em>, i na kraju pritisnuti taster <key>Unesi</key>.</p>
    <p>Na primer, da pokrenete <app>Ritam mašinu</app>, pritisnite <keyseq><key>Alt</key><key>F2</key></keyseq> i upišite „<cmd>rhythmbox</cmd>“ (bez navodnika). Naziv programa je naredba za pokretanje programa.</p>
  </note>

</page>
