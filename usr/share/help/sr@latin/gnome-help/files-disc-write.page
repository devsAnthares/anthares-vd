<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nabacite datoteke i dokumenta na prazan CD ili DVD koristeći program za rezanje CD/DVD diskova.</desc>
  </info>

  <title>Zapišite datoteke na CD ili DVD</title>

  <p>Možete da nabacite datoteke na prazan disk koristeći <gui>Tvorca CD/DVD-a</gui>. Opcija za rezanje Cd-a ili DVD-a će se pojaviti u upravniku datoteka čim ubacite disk u čitač diskova. Upravnik datoteka vam dopušta da prenesete datoteke na druge računare ili da obavite stvaranje <link xref="backup-why">rezervi</link> nabacivanjem datoteka na prazan disk. Da narežete datoteke na CD ili DVD:</p>

  <steps>
    <item>
      <p>Ubacite prazan disk u vaš uređaj za rezanje diskova.</p></item>
    <item>
      <p>U obaveštenju <gui>Prazan prepisivi CD/DVD</gui> koji će se pojaviti pri dnu ekrana, izaberite <gui>Otvori Tvorcem CD/DVD-a</gui>. Otvoriće se prozor fascikle <gui>Tvorca CD/DVD-a</gui>.</p>
      <p>(Možete takođe da kliknete i na <gui>Prazan prepisivi CD/DVD</gui> u odeljku <gui>Uređaji</gui> u bočnoj površi upravnika datoteka.)</p>
    </item>
    <item>
      <p>U polje <gui>Naziv diska</gui>, upišite naziv za disk.</p>
    </item>
    <item>
      <p>Prevucite ili umnožite željene datoteke u prozor.</p>
    </item>
    <item>
      <p>Kliknite <gui>Nareži na disk</gui>.</p>
    </item>
    <item>
      <p>U prozorčetu pod <gui>Izaberite disk za zapisivanje</gui>, izaberite prazan disk.</p>
      <p>(Možete da izaberete i <gui>Datoteka odraza</gui>. Ovim ćete nabaciti datoteke u <em>odraz diska</em>, koji će biti sačuvan na vašem računaru. Ovaj odraz diska ćete moći da narežete na prazan disk nekom drugom prilikom.)</p>
    </item>
    <item>
      <p>Kliknite <gui>Osobine</gui> ako želite da podesite brzinu rezanja, mesto privremenih datoteka, i ostale opcije. Osnovne opcije bi trebale da budu zadovoljavajuće.</p>
    </item>
    <item>
      <p>Kliknite na dugme <gui>Nareži</gui> da započnete zapisivanje.</p>
      <p>Ako izaberete <gui>Nareži više primeraka</gui>, od vas će biti zatraženi dodatni diskovi.</p>
    </item>
    <item>
      <p>Nakon završenog narezivanja diska, isti će biti samostalno izbačen. Izaberite <gui>Napravi još primeraka</gui> ili <gui>Zatvori</gui> da izađete.</p>
    </item>
  </steps>

<section id="problem">
  <title>Ako disk nije ispravno narezan</title>

  <p>Ponekada računar neće ispravno zapisati podatke, i vi nećete biti u mogućnosti da vidite datoteke koje ste nabacili na disk kada ga budete ubacili u računar.</p>

  <p>U tom slučaju, pokušajte ponovo da narežete disk ali koristite manju brzinu rezanja, na primer, 12h umesto 48h. Rezanje manjim brzinama je pouzdanije. Možete da izaberete brzinu klikom na dugme <gui>Osobine</gui> u prozoru <gui>Tvorcu CD/DVD-a</gui>.</p>

</section>

</page>
