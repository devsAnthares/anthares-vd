<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-connect-device" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="seealso" xref="sharing-bluetooth"/>
    <link type="seealso" xref="bluetooth-remove-connection"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pol V. Frilds</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Dejvid King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uparite Blutut uređaje.</desc>
  </info>

  <title>Povežite vaš računar na Blutut uređaj</title>

  <p>Pre nego što ćete moći da koristite Blutut uređaj kao što je miš ili slušalice sa mikrofonom, kao prvo morate da povežete vaš računar na uređaj. Ovo se takođe naziva <em>uparivanjem</em> Blutut uređaja.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Blutut</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Blutut</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Uverite se da je blutut uključen: prekidač na vrhu treba biti postavljen na <gui>UKLJ.</gui>. Sa otvorenim panelom i <gui>UKLJ.</gui> prekidačem, vaš računar će početi da traži uređaje.</p>
    </item>
    <item>
      <p>Namestite da drugi Blutut uređaj bude <link xref="bluetooth-visibility">otkriven ili vidljiv</link> i postavite ga na 5-10 metara od vašeg računara.</p>
    </item>
    <item>
      <p>Kliknite na uređaj u spisku <gui>Uređaji</gui>. Otvoriće se panel za uređaje.</p>
    </item>
    <item>
      <p>Ako je zatraženo, potvrdite PIN na vašem drugom uređaju. Uređaj treba da vam pokaže PIN koji ste videli na ekranu vašeg računara. Potvrdite PIN na uređaju (možda ćete morati da kliknete na <gui>Upari</gui> ili <gui>Potvrdi</gui>), zatim na računaru kliknite <gui>Potvrdi</gui>.</p>
      <p>Morate da završite vaš unos za 20 sekunde na većini uređaja, ili veza neće biti uspostavljena. Ako do ovoga dođe, vratite se na spisak uređaja i počnite opet.</p>
    </item>
    <item>
      <p>Unos za uređaj u spisku <gui>uređaja</gui> prikazaće stanje <gui>Povezan</gui>.</p>
    </item>
    <item>
      <p>Da uredite uređaj, kliknite na njega u spisku <gui>uređaja</gui>. Videćete naročiti panel za uređaj. Može prikazivati dodatne opcije primenjive na vrstu uređaja na koji se povezujete.</p>
    </item>
    <item>
      <p>Zatvorite panel kada izmenite podešavanja.</p>
    </item>
  </steps>

  <media its:translate="no" type="image" src="figures/bluetooth-menu.png" style="floatend">
    <p its:translate="yes">Ikonica blututa na gornjoj traci</p>
  </media>
  <p>Kada je povezan jedan ili više blutut uređaja, ikonica blututa se pojavljuje u oblasti stanja sistema.</p>

</page>
