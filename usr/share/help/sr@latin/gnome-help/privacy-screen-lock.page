<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ne dozvolite drugima da koriste vašu radnu površ kada se udaljite od vašeg računara.</desc>
  </info>

  <title>Samostalno zaključajte vaš ekran</title>
  
  <p>Kada se udaljite od vašeg računara, trebali biste da <link xref="shell-exit#lock-screen">zaključate ekran</link> kako drugi nebi mogli da koriste vašu radnu površ i da pristupe vašim datotekama. Ako vam se desi da nekad zaboravite da zaključate ekran, možda ćete poželeti da se ekran vašeg računara sam zaključa nakon određenog vremena. To će vam pomoći da osigurate vaš računar kada ga ne koristite.</p>

  <note><p>Kada je vaš ekran zaključan, vaši programi i sistemski procesi će nastaviti da rade, ali vi ćete morati da unesete vašu lozinku da biste ponovo počeli da ih koristite.</p></note>
  
  <steps>
    <title>Da postavite vreme nakon koga će se vaš ekran sam zaključati:</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Privatnost</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Privatnost</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Izaberite <gui>Zaključaj ekran</gui>.</p>
    </item>
    <item>
      <p>Uverite se da je <gui>Sam zaključaj ekran</gui> prebačeno na <gui>UKLJ.</gui>, zatim izaberite vremenski period iz padajućeg spiska.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Programi mogu da vam prikažu obaveštenja čak i na zaključanom ekranu. Na ovaj način možete, na primer, da vidite da li vam je pristigla pošta a da ne otključate ekran. Ako se brinete da neko drugi može da vidi ova obaveštenja, isključite <gui>prikazivanje obaveštenja</gui>.</p>
  </note>

  <p>Kada je vaš ekran zaključan, a želite da ga otključate, pritisnite <key>Esc</key>, ili prevucite mišem po ekranu od dna na gore. Zatim unesite vašu lozinku, i pritisnite <key>Unesi</key> ili kliknite na <gui>Otključaj</gui>. Drugačije, jednostavno počnite da kucate vašu lozinku a zastor katanca će biti samostalno podignut kako budete kucali.</p>

</page>
