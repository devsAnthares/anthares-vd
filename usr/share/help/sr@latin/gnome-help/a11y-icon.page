<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Izbornik univerzalnog pristupa jeste ikonica na gornjoj traci koja liči na čoveka.</desc>
  </info>

  <title>Pronađite izbornik univerzalnog pristupa</title>

  <p><em>Izbornik univerzalnog pristupa</em> je mesto gde možete da uključite neka podešavanja pristupačnosti. Ovaj izbornik možete da pronađete tako što ćete kliknuti na ikonicu koja liči na osobu okruženu krugom na gornjoj traci.</p>

  <figure>
    <desc>Izbornik univerzalnog pristupa može biti pronađen na gornjoj traci.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Ako ne vidite izbornik univerzalnog pristupa, možete da ga uključite sa panela podešavanja <gui>Univerzalnog pristupa</gui>:</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Univerzalni pristup</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Univerzalni pristup</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Prebacite <gui>Uvek prikaži izbornik univerzalnog pristupa</gui> na <gui>UKLJ.</gui>.</p>
    </item>
  </steps>

  <p>Da pristupite ovom izborniku koristeći tastaturu umesto miša, pritisnite <keyseq><key>Ktrl</key><key>Alt</key><key>Tab</key></keyseq> da prebacite prvi plan tastature na gornju traku. Pojaviće se bela linija ispod dugmeta <gui>Aktivnosti</gui> — ovo vam govori o tome koja stavka na gornjoj traci je izabrana. Koristite tastere strelica na tastaturi da pomerite belu liniju ispod ikonice izbornika univerzalnog pristupa a zatim pritisnite <key>Unesi</key> da ga otvorite. Možete da koristite tastere strelica na gore i na dole da izaberete stavke u izborniku. Pritisnite <key>Unesi</key> da uključite izabranu stavku.</p>

</page>
