<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-slowkeys" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neka postoji zastoj između pritiska tastera i prikazivanja slova na ekranu.</desc>
  </info>

  <title>Uključite spore tastere</title>

  <p>Uključite <em>spore tastere</em> ako želite da postoji kašnjenje između pritiska tastera i pojavljivanja slova na ekranu. Ovo znači da ćete morati da na trenutak da zadržite pritisnut svaki taster koji želite da otkucate pre nego što se pojavi slovo na ekranu. Koristite spore tastere ako nehotice pritiskate nekoliko tastera u isto vreme dok kucate, ili ako nalazite da je teško prvi put da pritisnete desni taster na tastaturi.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Pritisnite <gui>Pomoć kucanja (Iks pristup)</gui> u odeljku <gui>Kucanje</gui>.</p>
    </item>
    <item>
      <p>Prebacite <gui>Spori tasteri</gui> na <gui>UKLJ.</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Brzo uključite ili isključite spore tastere</title>
    <p>Pod <gui>Uključi tastaturom</gui>, izaberite <gui>Uključite funkcije pristupačnosti sa tastature</gui> da uključite ili isključite spore tastere sa tastature. Kada je ova opcija izabrana, možete da pritisnete i da držite <key>Šift</key> osam sekundi da uključite ili isključite spore tastere.</p>
    <p>Možete takođe da uključite ili da isključite spore tastere tako što ćete kliknuti na <link xref="a11y-icon">ikonicu pristupačnosti</link> na gornjoj traci i izabrati <gui>Spori tasteri</gui>. Ikonica pristupačnosti je vidljiva kada je uključeno jedno ili više podešavanja sa panela <gui>Univerzalnog pristupa</gui>.</p>
  </note>

  <p>Koristite klizač <gui>Kašnjenje prihvatanja</gui> da odredite koliko dugo morate da držite pritisnut taster da bi bio zabeležen.</p>

  <p>Možete da podesite vaš računar da pusti zvuk kada pritisnete taster, kada je pritisak tastera prihvaćen, ili kada je pritisak tastera odbačen zato što niste držali taster dovoljno dugo.</p>

</page>
