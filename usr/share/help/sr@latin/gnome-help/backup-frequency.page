<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Saznajte koliko često treba da pravite rezervu vaših važnih datoteka da biste bili sigurni da su bezbedni.</desc>
  </info>

<title>Učestanost pravljenja rezerve</title>

  <p>Koliko često ćete praviti rezerve zavisiće od vrste podataka za pričuvavanje. Na primer, ako radite u mrežnom okruženju sa rizičnim podacima sačuvanim na vašim serverima, onda možda neće biti dovoljno čak ni svakodnevno pravljenje rezerve.</p>

  <p>Sa druge strane, ako pravite rezervu podataka na vašem kućnom računaru onda će pravljenje rezerve na svakih sat vremena biti stvarno nepotrebno. Možda će vam biti od pomoći da razmotrite sledeće tačke kada budete želeli da isplanirate raspored pravljenja rezerve:</p>

<list style="compact">
<item><p>Koliko vremena provodite u radu za računarom.</p></item>
<item><p>Koliko često i koliko podataka na računaru se izmeni.</p></item>
</list>

  <p>Ako su podaci za koje želite da napravite rezervu niže važnosti, ili podložni manjim izmenama, kao muzika, e-pošta i porodične fotografije, onda će nedeljno ili čak mesečno pravljenje rezerve biti sasvim dovoljno. Međutim, ako se desi da se nalazite u sred obračunavanja poreza, učestalije stvaranje rezervi će možda biti neophodno.</p>

  <p>Kao opšte pravilo, vreme između pravljenja rezervi ne bi trebalo da bude veće od vremena koje biste potrošili ako biste morali ponovo da uradite sve što ste do tada uradili. Na primer, ako vam je nedelju dana previše vremena da utrošite na ponovno pisanje izgubljenih dokumenata, onda biste trebali da pravite rezerve barem jednom nedeljno.</p>

</page>
