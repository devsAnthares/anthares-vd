<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Uvećajte vaš ekran tako da vam bude lakše da vidite stvari.</desc>
  </info>

  <title>Povećajte oblast ekrana</title>

  <p>Povećavanje ekrana je drugačije od samog uvećavanja <link xref="a11y-font-size">veličine teksta</link>. Ova funkcija je kao da imate uveličavajuće staklo, koje vam omogućava da se krećete unaokolo i da približavate delove ekrana.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Univerzalni pristup</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Univerzalni pristup</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Pritisnite <gui>Uveličanje</gui> u odeljku <gui>Videti</gui>.</p>
    </item>
    <item>
      <p>Prebacite <gui>Uveličanje</gui> na <gui>UKLJ.</gui> u gornjem desnom uglu u prozoru <gui>Opcije uveličanja</gui>.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Sada možete da se krećete unaokolo po oblasti ekrana. Pomerajući vašeg miša do ivica ekrana, pomeraćete uveličanu oblast u raznim pravcima, što će vam omogućiti da vidite oblast vašeg izbora.</p>

  <note style="tip">
    <p>Možete da uključite ili da isključite zumiranje tako što ćete kliknuti na <link xref="a11y-icon">ikonicu pristupačnosti</link> na gornjoj traci i izabrati <gui>Zumiraj</gui>.</p>
  </note>

  <p>Možete da izmenite činilac uveličanja, praćenje miša, i položaj uveličanog pregleda na ekranu. Doterajte ovo u jezičku <gui>Lupa</gui> u prozoru <gui>Opcija uvećanja</gui>.</p>

  <p>Možete da aktivirate nišan da vam pomogne da pronađete miša ili pokazivač dodirne table. Uključite ih i doterajte njihovu dužinu, boju i debljinu, u jezičku <gui>Nišana</gui> u prozoru podešavanja <gui>Uvećanja</gui>.</p>

  <p>Možete da se prebacite na obrnuti video ili <gui>Belo na crnom</gui>, i da podesite opcije osvetljenosti, kontrasta i sivila za lupu. Kombinacija ovih opcija je korisna za ljude sa slabijim vidom, svaki stepen fotofobije, ili samo za korišćenje računara pod nepovoljnim uslovima osvetljenosti. Izaberite jezičak <gui>Efekti boje</gui> u prozoru podešavanja <gui>Uvećanja</gui> da uključite i izmenite ove opcije.</p>

</page>
