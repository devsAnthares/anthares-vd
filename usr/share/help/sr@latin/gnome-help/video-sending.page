<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Proverite da li imaju instalirane odgovarajuće video kodeke.</desc>
  </info>

  <title>Druge osobe ne mogu da puštaju snimke koje sam napravio</title>

  <p>Ako ste napravili snimak na vašem linuks računaru i poslali ga nekome ko koristi Vindouz ili Mek OS, možete uvideti da imaju problema sa puštanjem snimka.</p>

  <p>Da bi bila u mogućnosti da pusti vaš snimak, osoba kojoj ste ga poslali mora da ima instalirane odgovarajuće <em>kodeke</em>. Kodek je mali komad softvera koji zna kako da obradi snimak i da ga prikaže na ekranu. Postoji veliki broj video formata i svaki zahteva drugačiji kodek za puštanje. Možete da proverite u kom je formatu vaš snimak na sledeći način:</p>

  <list>
    <item>
      <p>Otvorite <app>Datoteke</app> iz pregleda <gui xref="shell-introduction#activities">aktivnosti</gui>.</p>
    </item>
    <item>
      <p>Kliknite desnim tasterom miša na datoteku snimka i izaberite <gui>Osobine</gui>.</p>
    </item>
    <item>
      <p>Idite na jezičak <gui>Zvuk/video</gui> ili <gui>Video</gui> i pogledajte koji su <gui>kodeci</gui> ispisani pod <gui>Video</gui> i <gui>Zvuk</gui> (ako i video ima zvuk).</p>
    </item>
  </list>

  <p>Pitajte osobu koja ima probleme sa puštanjem da li ima instaliran odgovarajući kodek. Mogu naći korisnim da na vebu potraže naziv kodeka i naziv njihovog programa za puštanje snimaka. Na primer, ako vaš snimak koristi format <em>Teora</em> a vaš prijatelj koristi Vindouz medija plejer da bi ga gledao, neka potraži „theora windows media player“. Često ćete biti u mogućnosti da besplatno preuzmete odgovarajući kodek ako nije instaliran.</p>

  <p>Ako ne možete da pronađete odgovarajući kodek, probajte <link href="http://www.videolan.org/vlc/">VLC medijski puštač</link>. Radi na Vindouzu i Mek OS-u kao i na Linuksu, i podržava mnogo različitih video formata. Ako ni ovo ne uspe, probajte da pretvorite vaš snimak u neki drugi format. Većina programa za uređivanje snimaka mogu to da urade, a dostupni su i naročiti programi za pretvaranje snimaka. Proverite u programu za instalaciju da vidite šta je dostupno.</p>

  <note>
    <p>Postoji još nekoliko drugih problema koji mogu nekoga da spreče da pusti vaš snimak. Može biti da se snimak oštetio prilikom slanja (ponekad velike datoteke ne bivaju umnožene savršeno), oni mogu imati problema sa njihovim programom za puštanje snimaka, ili može biti da snimak nije ispravno napravljen (možda je došlo do greške kada ste sačuvali snimak).</p>
  </note>

</page>
