<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koristite srednji taster miša za otvaranje programa, otvaranje jezičaka, i drugo.</desc>
  </info>

<title>Srednji klik</title>

<p>Mnogi miševi i neke dodirne table imaju i srednji taster. Na miševima sa kliznim točkićem, obično možete da pritisnete na dole klizni točkić da izvršite srednji klik. Ako nemate srednji taster na mišu, možete da pritisnete i levi i desni taster istovremeno zarad srednjeg klika.</p>

<p>Na dodirnim tablama koje podržavaju lupkanje sa više prstiju, možete da lupnete sa tri prsta odjednom da biste izvršili srednji klik. Da bi ovo radilo, moraćete da <link xref="mouse-touchpad-click">uključite klik na dodirnoj tabli</link> u podešavanjima dodirne table.</p>

<p>Mnogi programi koriste srednji klik za naprednije prečice na klik.</p>

<list>
  <item><p>U pregledu <gui>Aktivnosti</gui>, srednjim klikom možete brzo da otvorite novi prozor nekog programa. Jednostavno kliknite srednjim tasterom na ikonicu programa, bilo na poletniku na levoj strani, ili na pregledu programa. Pregled programa se prikazuje korišćenjem dugmeta mreže u poletniku.</p></item>

  <item><p>Većina veb preglednika vam omogućava da brzo otvorite veze u jezičcima pomoću srednjeg tastera miša. Samo kliknite na bilo koju vezu srednjim tasterom miša i otvoriće se u novom jezičku.</p></item>

  <item><p>U upravniku datoteka, srednji klik ima dve uloge. Ako izvršite srednji klik nad fasciklom, otvoriće je u novom jezičku. Ovo imitira ponašanje poznatih veb preglednika. Ako izvršite srednji klik nad datotekom, otvoriće datoteku, kao da ste dva puta kliknuli na nju.</p></item>
</list>

<p>Neki namenski programi vam dopuštaju da koristite srednji taster miša za druge funkcije. Pregledajte pomoć vaših programa za <em>srednjim klikom</em> ili <em>srednjim tasterom miša</em>.</p>

</page>
