<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tifani Antopoloski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koristite Sigurni sef (ili neki drugi program za rezerve) da napravite umnoške vaših vrednih datoteka i podešavanja da ih zaštitite od gubljenja.</desc>
  </info>

<title>Kako da napravite rezervu</title>

  <p>Najlakši način da napravite rezervu vaših datoteka i podešavanja je da dopustite programu za rezerve da odradi proces pravljenja rezerve za vas. Dostupan je veliki broj različitih programa za rezervu, na primer <app>Sigurni sef</app>.</p>

  <p>Pomoć izabranog programa za rezerve će vas voditi kroz podešavanje postavki za pravljenje rezerve, kao i za povraćaj podataka.</p>

  <p>Još jedan od načina je da <link xref="files-copy">umnožite vaše datoteke</link> na bezbedno mesto, kao što je spoljni čvrsti disk, drugi računar na mreži, ili USB uređaj. Vaši <link xref="backup-thinkabout">lični podaci</link> i podešavanja se obično nalaze u vašoj ličnoj fascikli, tako da možete da ih umnožite odatle.</p>

  <p>Količina podataka koju možete da umnožite je ograničena veličinom skladišnog uređaja. Ako imate prostora na uređaju za rezervu, najbolje bi bilo da napravite rezervu čitave lične fascikle izuzev sledećih stavki:</p>

<list>
 <item><p>Datoteke čije rezerve ste već napravili negde drugde, na CD, DVD, ili drugim prenosivim medijima.</p></item>
 <item><p>Datoteke koje možete s lakoćom ponovo da napravite. Na primer, ako ste programer, nećete morati da pravite rezerve datoteka koje se stvaraju kada vršite prevođenje programa. Umesto toga, uverite se da ste napravili rezervu originalne izvorne datoteke.</p></item>
 <item><p>Sve datoteke u fascikli smeća. Vaša fascikla smeća se nalazi u <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
