<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="sr-Latn">

  <info>

    <link type="guide" xref="power#faq"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Radni vek baterije koji biva prikazan kada kliknete na <gui>ikonicu baterije</gui> je samo procena.</desc>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Procenjeni radni vek baterije je pogrešan</title>

<p>Kada proveravate preostalo radno vreme baterije, možete uočiti da se preostalo vreme o kome izveštava razlikuje od stvarnog preostalog radnog vremena baterije. Zato što iznos preostalog radnog vremena baterije može biti samo procenjen. Procenjivanje će se poboljšati s vremenom.</p>

<p>Da bi se procenilo preostalo radno vreme baterije, veliki broj činilaca treba uzeti u obzir. Jedan od njih je iznos napajanja koje trenutno koristi računar: potrošnja napajanja se menja u zavisnosti od toga koliko programa ste otvorili, koji uređaji su priključeni, i da li ste pokrenuli neki zahtevan zadatak (kao što je gledanje snimaka visoke rezolucije ili pretvaranje muzičke datoteke, na primer). Ovo se menja iz trenutka u trenutak i teško je za predviđanje.</p>

<p>Još jedan činilac je način pražnjenja baterije. Neke baterije brže gube napajanje kako postaju praznije. Bez tačnog poznavanja pražnjenja baterije, samo približna procena o preostalom radnom vremenu baterije može biti urađena.</p>

<p>Kako se baterija bude praznila, upravnik napajanja će saznavati njena svojstva pražnjenja i naučiće kako da obezbedi bolju procenu radnog vremena baterije. Ipak, ovo neće nikada biti u potpunosti tačno.</p>

<note>
  <p>Ako dobijete potpuno neverovatnu procenu radnog vremena baterije (recimo, stotinu dana), upravniku napajanja verovatno nedostaje neki podatak koji mu je potreban za osetljiviju procenu.</p>
  <p>Ako isključite napajanje i duže vreme koristite napajanje iz baterije za rad računara, zatim ga priključite na mrežu i ostavite da se baterija napuni, upravnik napajanja će biti u mogućnosti da dobavi podatke koji su mu potrebni.</p>
</note>

</page>
