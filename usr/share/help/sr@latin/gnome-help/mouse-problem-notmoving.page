<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>Fil Bul</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Šon Mek Kens</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kako da proverite zašto vaš miš ne radi.</desc>
  </info>

<title>Pokazivač miša se ne pomera</title>

<links type="section"/>

<section id="plugged-in">
 <title>Proverite da li je miš priključen</title>
 <p>Ako imate miša sa kablom, proverite da li je čvrsto priključen na vaš računar.</p>
 <p>Ako je to USB miš (sa pravougaonim priključkom), pokušajte da ga priključite na drugu USB priključnicu. Ako je to PS/2 miš (sa malim, okruglim priključkom sa šest nožica), uverite sa da je priključen u zelenu priključnicu miša umesto u ljubičastu priključnicu tastature. Moraćete ponovo da pokrenete računar ako nije bio priključen.</p>
</section>

<section id="broken">
 <title>Proverite da li miš zapravo radi</title>
 <p>Priključite miša na neki drugi računar i vidite da li radi.</p>

 <p>Ako je miš optički ili laser miš, sa donje strane bi trebalo da vidite svetlosni zrak kada ga upalite. Ako nema svetla, proverite da li je upaljen. Ako jeste a još uvek nema svetla, najverovatnije da je miš pokvaren.</p>
</section>

<section id="wireless-mice">
 <title>Proverite bežični miš</title>

  <list>
    <item>
      <p>Uverite se da je miš upaljen. Na donjoj strani miša često se nalazi prekidač za potpuno gašenje miša, tako da možete da ga ponesete s vama a da ga ne budite stalno.</p>
    </item>
   <item><p>Ako koristite blutut miša, uverite se da ste uparili miša sa vašim računarom. Pogledajte <link xref="bluetooth-connect-device"/>.</p></item>
  <item>
   <p>Kliknite na dugme i vidite da li se pokazivač miša pomera. Neki bežični miševi se uspavljuju da bi sačuvali energiju, tako da neće odgovarati sve dok ne kliknete na dugme. Pogledajte <link xref="mouse-wakeup"/>.</p>
  </item>
  <item>
   <p>Proverite da li je baterija miša napunjena.</p>
  </item>
  <item>
   <p>Uverite se da je prijemnik čvrsto priključen na računar.</p>
  </item>
  <item>
   <p>Ako vaš miš i prijemnik mogu da rade na različitim radio kanalima, uverite se da su podešeni na isti kanal.</p>
  </item>
  <item>
   <p>Moraćete da pritisnete taster na mišu, prijemniku ili na oboma da uspostavite vezu. Uputstvo vašeg miša može da sadrži više detalja ako je ovo slučaj.</p>
  </item>
 </list>

 <p>Većina RF (radio) bežičnih miševa treba odmah da proradi kada ih priključite na vaš računar. Ako imate blutut ili IR (infracrveni) bežični miš, moraćete da izvršite neke dodatne korake da bi proradili. Koraci mogu da zavise od marke i modela vašeg miša.</p>
</section>

</page>
