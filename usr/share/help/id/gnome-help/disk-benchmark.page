<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="id">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Michael Hill</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jalankan benchmark pada hard disk Anda untuk memeriksa seberapa cepat dia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Uji kinerja hard disk Anda</title>

  <p>Untuk menguji kecepatan hard disk Anda:</p>

  <steps>
    <item>
      <p>Buka <app>Disk</app> dari ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui>.</p>
    </item>
    <item>
      <p>Pilih disk dari daftar yang ada di panel kiri</p>
    </item>
    <item>
      <p>Klik tombol menu dan pilih <gui>Uji banding disk...</gui> pada menu.</p>
    </item>
    <item>
      <p>Klik <gui>Mulai Uji Banding...</gui> dan setel parameter <gui>Laju Transfer</gui> dan <gui>Waktu Akses</gui> sesuai keinginan.</p>
    </item>
    <item>
      <p>Klik <gui>Mulai Uji Banding</gui> untuk menguji sebarapa cepat data dapat dibaca dari disk. <link xref="user-admin-explain">Privilese administrasif</link> mungkin diperlukan. Masukkan sandi Anda, atau sandi bagi akun administrator yang diminta.</p>
      <note>
        <p>Bila <gui>Juga lakukan uji banding tulis</gui> dicontreng, uji banding akan menguji seberapa cepat data dapat dibaca dari dan ditulis ke disk. Ini membutuhkan waktu lebih lama.</p>
      </note>
    </item>
  </steps>

  <p>Ketika pengujian selesai, hasil akan muncul pada diagram. Titik-titik hijau dan garis-garis penghubung menunjukkan cuplikan yang diambil; ini berhubungan dengan sumbu kanan, yang menunjukkan waktu akses, diplot terhadap sumbu dasar, yang merepresentasikan persentase waktu berlangsung selama benchmark. Garis biru merepresentasikan laju baca, sementara garis merah merepresentasikan laju tulis; ini ditunjukkan sebagai laju data akses pada sumbu kiri, yang diplot terhadap persentase dari disk yang ditempuh, dari spindle luar, sepanjang sumbu dasar.</p>

  <p>Di bawah diagram, nilai-nilai ditampilkan untuk laju baca dan tulis minimum, maksium, dan rata-rata, waktu akses rata-rata, dan waktu berjalan sejak uji benchmark terakhir.</p>

</page>
