<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="id">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Stop or limit your computer from tracking your recently-used
    files.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Turn off or limit file history tracking</title>
  
  <p>Tracking recently used files and folders makes it easier to find
  items that you have been working on in the file manager and in file
  dialogs in applications. You may wish to keep your file usage history
  private instead, or only track your very recent history.</p>

  <steps>
    <title>Turn off file history tracking</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Privasi</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Privasi</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Select <gui>Usage &amp; History</gui>.</p>
    </item>
    <item>
     <p>Set the <gui>Recently Used</gui> switch to <gui>OFF</gui>.</p>
     <p>To re-enable this feature, set the <gui>Recently Used</gui> switch to
     <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Use the <gui>Clear Recent History</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>
  
  <note><p>This setting will not affect how your web browser stores information
  about the web sites you visit.</p></note>

  <steps>
    <title>Restrict the amount of time your file history is tracked</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Privasi</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Privasi</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Select <gui>Usage &amp; History</gui>.</p>
    </item>
    <item>
     <p>Ensure the <gui>Recently Used</gui> switch is <gui>ON</gui>.</p>
    </item>
    <item>
     <p>Select the length of time to <gui>Retain History</gui>. Choose from
     options <gui>1 day</gui>, <gui>7 days</gui>, <gui>30 days</gui>, or
     <gui>Forever</gui>.</p>
    </item>
    <item>
      <p>Use the <gui>Clear Recent History</gui> button to purge the history
      immediately.</p>
    </item>
  </steps>

</page>
