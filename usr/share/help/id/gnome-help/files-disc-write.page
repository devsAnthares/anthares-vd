<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="id">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Simpan berkas dan dokumen ke dalam CD atau DVD kosong memakai pembakar CD/DVD.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Menulis berkas ke CD atau DVD</title>

  <p>Anda dapat meletakkan berkas-berkas ke cakram kosong dengan menggunakan <gui>Pembuat CD/DVD</gui>. Pilihan untuk membuat CD atau DVD akan tampil di pengelola berkas saat Anda meletakkan CD ke penulis CD/DVD Anda. Manajer berkas memungkinkan Anda untuk mentransfer berkas-berkas ke komputer lain atau melakukan <link xref="backup-why">pencadangan</link> dengan meletakkan berkas-berkas pada cakram kosong. Untuk menulis berkas-berkas tersebut ke CD atau DVD:</p>

  <steps>
    <item>
      <p>Tempatkan cakram kosong ke dalam penulis CD / DVD Anda.</p></item>
    <item>
      <p>Pada notifikasi <gui>CD/DVD-R Kosong</gui> yang muncul pada bagian bawah layar, pilih <gui>Buka dengan Pembuat CD/DVD</gui>. Jendela folder <gui>Pembuat CD/DVD</gui> akan terbuka.</p>
      <p>(Anda juga dapat mengklik <gui>Disc CD/DVD-R Kosong</gui> di bawah <gui>Perangkat</gui> di bilah sisi pengelola berkas.)</p>
    </item>
    <item>
      <p>Dalam kotak teks <gui>Nama Cakram</gui>, ketikkan nama bagi cakram.</p>
    </item>
    <item>
      <p>Tarik atau salin berkas yang diinginkan ke dalam jendela.</p>
    </item>
    <item>
      <p>Klik <gui>Tulis ke Disc</gui>.</p>
    </item>
    <item>
      <p>Di bawah <gui>Pilih disc yang akan ditulisi</gui>, pilih disc kosong.</p>
      <p>(Anda bisa memilih <gui>Berkas gambar</gui> sebagai gantinya. Hal ini akan menempatkan berkas ke dalam <em>image disc</em>, yang akan disimpan pada komputer Anda. Anda kemudian dapat membakar bahwa image disc ke disc kosong di kemudian hari.)</p>
    </item>
    <item>
      <p>Klik <gui>Properti</gui> bila Anda ingin mengatur kecepatan pembakaran, lokasi berkas sementara, dan opsi-opsi lainnya. Opsi baku seharusnya sudah tepat.</p>
    </item>
    <item>
      <p>Klik tombol <gui>Bakar</gui> untuk mulai merekam.</p>
      <p>Bila <gui>Bakar Beberapa Salinan</gui> dipilih, Anda akan dimintai disc tambahan.</p>
    </item>
    <item>
      <p>Ketika pembakaran disc selesai, itu akan otomatis keluar. Pilih <gui>Buat Lebih Banyak Salinan</gui> atau <gui>Tutup</gui> untuk keluar.</p>
    </item>
  </steps>

<section id="problem">
  <title>Bila cakram tidak dibakar secara benar</title>

  <p>Terkadang komputer tidak merekam data dengan benar, dan Anda tidak akan dapat melihat file yang Anda masukkan ke cakram ketika Anda memasukkannya ke komputer.</p>

  <p>Dalam hal ini, coba bakar ulang cakram namun gunakan kecepatan pemakaran yang rendah, misal, 12x dari pada menggunakan 48x. Membakar dengan kecepatan rendah lebih bisa diandalkan. Anda dapat memilih kecepatan dengan mengklik tombol <gui>Properti</gui> di jendela <gui>Pembuat CD/DVD</gui>.</p>

</section>

</page>
