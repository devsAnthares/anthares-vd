<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-contrast" xml:lang="id">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Membuat jendela dan tombol pada layar lebih (atau kurang) tajam, sehingga lebih mudah dilihat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Atur kontras</title>

  <p>Anda dapat menyetel kontras jendela dan tombol sehungga mereka lebih mudah dilihat. Ini tidak sama dengan mengubah kecerahan seluruh layar; hanya bagian-bagian dari <em>antar muka pengguna</em> yang akan berubah.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Akses Universal</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Akses Universal</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Nyalakan <gui>Kontras Tinggi</gui> dalam seksi <gui>Penglihatan</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Nyalakan dan matikan kontras tinggi secara cepat</title>
    <p>Anda dapat secara cepat menyalakan dan mematikan kontras tinggi dengan mengklik <link xref="a11y-icon">ikon aksesibilitas</link> pada bilah puncak dan memilih <gui>Kontras Tinggi</gui>.</p>
  </note>

</page>
