<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="id">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision version="0.1" date="2013-02-23" status="review"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Explains the meanings of the icons located on the right of the top bar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Apa arti ikon-ikon pada bilah puncak?</title>
<p>This section explains the meaning of icons located on the top right corner of the screen. More specifically, the different variations of the icons provided by the GNOME interface are described.</p>

<if:choose>
<if:when test="!platform:gnome-classic">
<media type="image" src="figures/top-bar-icons.png" style="floatend">
  <p>Bilah puncak Shell GNOME</p>
</media>
</if:when>
<if:when test="platform:gnome-classic">
<media type="image" src="figures/top-bar-icons-classic.png" width="395" height="70" style="floatend">
  <p>Bilah puncak Shell GNOME</p>
</media>
</if:when>
</if:choose>

<links type="section"/>

<section id="universalicons">
<title>Ikon Menu Akses Universal</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/preferences-desktop-accessibility-symbolic.svg"/></td>
      <td><p>Leads to a menu that turns on accessibility settings.</p></td>
    </tr>
    
  </table>
</section>


<section id="audioicons">
<title>Ikon Kendali Keras Suara</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-high-symbolic.svg"/></td>
      <td><p>Volume diatur ke tinggi.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-medium-symbolic.svg"/></td>
      <td><p>Volume diatur ke sedang.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-low-symbolic.svg"/></td>
      <td><p>Volume diatur ke rendah.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-muted-symbolic.svg"/></td>
      <td><p>Volume dibisukan.</p></td>
    </tr>
  </table>
</section>


<section id="bluetoothicons">
<title>Ikon Manajer Bluetooth</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/></td>
      <td><p>Bluetooth telah diaktifkan.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-disabled-symbolic.svg"/></td>
      <td><p>Bluetooth telah dinonaktifkan.</p></td>
    </tr>
  </table>
</section>

<section id="networkicons">
<title>Ikon Manajer Jaringan</title>
<p/>
<p><app>Koneksi Seluler</app></p>
 <table shade="rows">

    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-3g-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan 3G.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-4g-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan 4G.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-edge-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan EDGE.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-gprs-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan GPRS.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-umts-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan UMTS.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-connected-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan seluler.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-acquiring-symbolic.svg"/></td>
      <td><p>Menjalin sambungan data seluler.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg"/></td>
      <td><p>Kuat sinyal sangat tinggi.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-good-symbolic.svg"/></td>
      <td><p>Kuat sinyal tinggi.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-ok-symbolic.svg"/></td>
      <td><p>Kuat sinyal sedang.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-weak-symbolic.svg"/></td>
      <td><p>Kuat sinyal rendah.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-none-symbolic.svg"/></td>
      <td><p>Kuat sinyal amat sangat lemah.</p></td>
    </tr></table>




<p><app>Koneksi Local Area Network (LAN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-error-symbolic.svg"/></td>
      <td><p>Ada galat sewaktu mencari jaringan.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-idle-symbolic.svg"/></td>
      <td><p>Jaringan sedang menganggur.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-no-route-symbolic.svg"/></td>
      <td><p>Tidak ditemukan rute bagi jaringan tersebut.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-offline-symbolic.svg"/></td>
      <td><p>Jaringan sedang mati.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-receive-symbolic.svg"/></td>
      <td><p>Jaringan sedang menerima data.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-receive-symbolic.svg"/></td>
      <td><p>Jaringan sedang memancarkan dan menerima data.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-symbolic.svg"/></td>
      <td><p>Jaringan sedang memancarkan data.</p></td>
    </tr>
</table>



<p><app>Koneksi Virtual Private Network (VPN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-acquiring-symbolic.svg"/></td>
      <td><p>Menjalin sambungan jaringan.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan VPN.</p></td>
    </tr>
</table>


<p><app>Sambungan Kabel</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-acquiring-symbolic.svg"/></td>
      <td><p>Menjalin sambungan jaringan.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-disconnected-symbolic.svg"/></td>
      <td><p>Terputus dari jaringan.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan kabel.</p></td>
    </tr>
</table>


<p><app>Sambungan Nirkabel</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-acquiring-symbolic.svg"/></td>
      <td><p>Menjalin sambungan nirkabel.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-encrypted-symbolic.svg"/></td>
      <td><p>Jaringan nirkabel dienkripsi.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-connected-symbolic.svg"/></td>
      <td><p>Tersambung ke jaringan nirkabel.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg"/></td>
      <td><p>Kuat sinyal sangat tinggi.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-good-symbolic.svg"/></td>
      <td><p>Kuat sinyal tinggi.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-ok-symbolic.svg"/></td>
      <td><p>Kuat sinyal sedang.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-weak-symbolic.svg"/></td>
      <td><p>Kuat sinyal rendah.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-none-symbolic.svg"/></td>
      <td><p>Kuat sinyal sangat rendah.</p></td>
    </tr>

  </table>
</section>

<section id="batteryicons">
<title>Ikon Manajer Daya</title>

 <table shade="rows">
   <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-symbolic.svg"/></td>
      <td><p>Baterai penuh.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-symbolic.svg"/></td>
      <td><p>Baterai terpakai sebagian.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-symbolic.svg"/></td>
      <td><p>Baterai lemah.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-symbolic.svg"/></td>
      <td><p>Awas: Baterai sangat lemah.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-symbolic.svg"/></td>
      <td><p>Baterai nyaris habis.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-missing-symbolic.svg"/></td>
      <td><p>Baterai telah dilepas.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charged-symbolic.svg"/></td>
      <td><p>Baterai terisi penuh.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charging-symbolic.svg"/></td>
      <td><p>Baterai penuh dan sedang diisi.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-charging-symbolic.svg"/></td>
      <td><p>Baterai hampir penuh dan sedang diisi.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-charging-symbolic.svg"/></td>
      <td><p>Baterai lemah dan sedang diisi.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-charging-symbolic.svg"/></td>
      <td><p>Baterai sangat lemah dan sedang diisi.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-charging-symbolic.svg"/></td>
      <td><p>Baterai kosong dan sedang diisi.</p></td>
    </tr>
  </table>
</section>


</page>
