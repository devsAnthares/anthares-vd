<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="id">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Proksi adalah perantara untuk trafik web, itu bisa dipakai untuk mengakses situs web secara anonim, untuk tujuan kendali atau keamanan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Menentukan pengaturan proksi</title>

<section id="what">
  <title>Apa itu proksi?</title>

  <p>Sebuah <em>proksi web</em> menyaring situs web yang Anda lihat, itu menenerima permintaan dari peramban web Anda untuk mengambil halaman web dan elemen-elemennya, dan mengikuti kebijakan akan memutuskan untuk meneruskannya kembali. Mereka umumnya digunakan dalam bisnis dan di hotspot nirkabel publik untuk mengontrol situs web apa yang dapat Anda lihat, mencegah Anda dari mengakses internet tanpa log masuk, atau untuk melakukan pemeriksaan keamanan di situs web.</p>

</section>

<section id="change">
  <title>Mengubah metoda proksi</title>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Jaringan</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Jaringan</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Pilih <gui>Proksi jaringan</gui> dari daftar di kiri.</p>
    </item>
    <item>
      <p>Choose which proxy method you want to use from:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Nihil</gui></title>
          <p>The applications will use a direct connection to fetch the content
          from the web.</p>
        </item>
        <item>
          <title><gui>Manual</gui></title>
          <p>For each proxied protocol, define the address of a proxy and port
          for the protocols. The protocols are <gui>HTTP</gui>,
          <gui>HTTPS</gui>, <gui>FTP</gui> and <gui>SOCKS</gui>.</p>
        </item>
        <item>
          <title><gui>Otomatis</gui></title>
          <p>URL menunjuk ke suatu sumber daya, yang memuat konfigurasi yang sesuai bagi sistem Anda.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Applications that use the network connection will use your specified proxy
  settings.</p>

</section>

</page>
