<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="id">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengkalibrasi layar Anda penting untuk menampilkan warna-warna akurat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya mengkalibrasi layar saya?</title>

  <p>Anda dapat mengkalibrasi layar Anda sehingga itu menampilkan warna yang lebih akurat. Ini khususnya berguna bila Anda terlibat dalam fotografi dijital, disain, atau karya seni.</p>

  <p>Anda akan perlu colorimeter atau spectrophotometer untuk melakukan ini. Kedua perangkat dipakai untuk membuat profil layar, tapi mereka bekerja dengan cara yang sedikit berbeda.</p>

  <steps>
    <item>
      <p>Pastikan perangkat kalibrasi Anda terhubung ke komputer Anda.</p>
    </item>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select your screen.</p>
    </item>
    <item>
      <p>Tekan <gui style="button">Kalibrasikan…</gui> untuk melakukan kalibrasi.</p>
    </item>
  </steps>

  <p>Layar berubah setiap saat: kecerahan cahaya latar dalam suatu tampilan TFT akan menjadi separuhnya kira-kira setiap 18 bulan, dan akan menjadi semakin kuning ketika semakin tua. Ini berarti Anda mesti mengkalibrasi layar Anda ketika ikon [!] muncul pada panel <gui>Warna</gui>.</p>

  <p>Layar LED juga berubah dengan berjalannya waktu, tapi dengan laju jauh lebih rendah daripada TFT.</p>

</page>
