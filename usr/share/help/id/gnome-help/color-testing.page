<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="id">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Gunakan profil uji yang disediakan untuk memeriksa bahwa profil Anda sedang diterapkan secara benar ke layar Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana saya menguji bila manajemen warna bekerja dengan benar?</title>

  <p>Efek profil warna kadang halus dan mungkin sulit dilihat apakah ada yang berubah.</p>

  <p>GNOME datang dengan beberapa profil untuk pengujian yang membuatnya sangat jelas ketika profil sedang diterapkan:</p>

  <terms>
    <item>
      <title>Biru</title>
      <p>Ini akan mengubah layar menjadi biru dan menguji apakah kurva kalibrasi sedang dikirim ke tampilan.</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Pilih perangkat yang ingin Anda tambahi profil. Anda mungkin ingin membuat catatan tentang profile mana yang saat ini sedang dipakai.</p>
    </item>
    <item>
      <p>Klik <gui>Tambah profil</gui> untuk memilih suatu profil uji, yang mestinya ada di bagian bawah daftar.</p>
    </item>
    <item>
      <p>Tekan <gui>Tambah</gui> untuk mengkonfirmasi pilihan Anda.</p>
    </item>
    <item>
      <p>Untuk kembali ke profil sebelumnya, pilih perangkat dalam <gui>Warna</gui> Panel, lalu pilih profil yang Anda gunakan sebelum Anda mencoba salah satu profil tes dan tekan <gui>Aktifkan</gui> untuk menggunakan lagi.</p>
    </item>
  </steps>


  <p>Menggunakan profil ini Anda dapat melihat dengan jelas ketika aplikasi mendukung manajemen warna.</p>

</page>
