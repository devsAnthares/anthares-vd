<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="id">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Driver perangkat/perangkat keras memungkinkan komputer Anda memakai perangkat yang tercantol padanya.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Apa itu penggerak?</title>

<p>Devices are the physical “parts” of your computer. They may be
<em>external</em> like printers and monitor or <em>internal</em> like graphics
and audio cards.</p>

<p>In order for your computer to be able to use these devices, it needs to know
how to communicate with them. This is done by a piece of software called a
<em>device driver</em>.</p>

<p>When you attach a device to your computer, you must have the correct driver
installed for that device to work. For example, if you plug in a printer but
the correct driver is not available, you will not be able to use the printer.
Normally, each model of device uses a driver that is not compatible with any
other model.</p>

<p>On Linux, the drivers for most devices are installed by default, so
everything should work when you plug it in. However, the drivers may need to be
installed manually or may not be available at all. </p>

<p>In addition, some existing drivers are incomplete or partially
non-functional. For example, you might find that your printer cannot do
double-sided printing, but is otherwise completely functional.</p>

</page>
