<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="id">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ke ringkasan <gui>Aktivitas</gui> dan seret jendela ke ruang kerja lain.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

  <title>Memindahkan jendela ke area kerja lain</title>

  <steps>
    <title>Memakai tetikus:</title>
    <item>
      <p if:test="!platform:gnome-classic">Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui>.</p>
      <p if:test="platform:gnome-classic">Open the
      <gui xref="shell-introduction#activities">Activities Overview</gui> from the
      <gui xref="shell-introduction#activities">Applications</gui> menu
      at the top left of the screen.</p>
    </item>
    <item>
      <p>Click and drag the window toward the right of the screen.</p>
    </item>
    <item>
      <p>The <em xref="shell-workspaces">workspace selector</em> will
      appear.</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Drop the window onto an empty
      workspace. This workspace now contains the window you have dropped, and a
      new empty workspace appears at the bottom of the <em>workspace
      selector</em>.</p>
      <p if:test="platform:gnome-classic">Drop the window onto an empty
      workspace. This workspace now contains the window you have dropped.</p>
    </item>
  </steps>

  <steps>
    <title>Memakai papan tik:</title>
    <item>
      <p>Select the window that you want to move (for example, using the
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      <em xref="shell-windows-switching">window switcher</em>).</p>
    </item>
    <item>
      <p>Press <keyseq><key>Super</key><key>Shift</key><key>Page
      Up</key></keyseq> to move the window to a workspace which is above the
      current workspace on the <em>workspace selector</em>.</p>
      <p>Press <keyseq><key>Super</key><key>Shift</key><key>Page
      Down</key></keyseq> to move the window to a workspace which is below the
      current workspace on the <em>workspace selector</em>.</p>
    </item>
  </steps>

</page>
