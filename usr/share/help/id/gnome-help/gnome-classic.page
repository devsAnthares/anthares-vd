<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="id">

  <info>
<!--    <link type="guide" xref="shell-overview#desktop" />-->

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pertimbangkan untuk beralih ke GNOME Klasik bila Anda lebih suka pengalaman dekstop yang lebih tradisional.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  </info>

<title>Apa itu GNOME Klasik?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME Klasik</em> adalah fitur bagi pengguna yang lebih suka pengalaman desktop yang lebih tradisional. Walaupun <em>GNOME Klasik</em> berbasis pada teknologi <em>GNOME 3</em>, itu menawarkan sejumlah perubahan ke antar muka pengguna, seperti misalnya menu <gui>Aplikasi</gui> dan <gui>Tempat</gui> pada bilah puncak, dan daftar jendela di dasar layar.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME Klasik</em> adalah fitur bagi pengguna yang lebih suka pengalaman desktop yang lebih tradisional. Walaupun <em>GNOME Klasik</em> berbasis pada teknologi <em>GNOME 3</em>, itu menawarkan sejumlah perubahan ke antar muka pengguna, seperti misalnya menu <gui xref="shell-introduction#activities">Aplikasi</gui> dan <gui>Tempat</gui> pada bilah puncak, dan daftar jendela di dasar layar.</p>
  </if:when>
</if:choose>

<p>You can use the <gui>Applications</gui> menu on the top bar to launch
 applications. The <gui xref="shell-introduction#activities">Activities</gui>
 overview is available by selecting the <gui>Activities
 Overview</gui> item from the menu.</p>

<p>To access the <em><gui>Activities</gui> overview</em>,
 you can also press the <key xref="keyboard-key-super">Super</key> key.</p>

<section id="gnome-classic-window-list">
<title>Daftar jendela</title>

<p>Daftar jendela di bagian dasar layar menyediakan akses ke semua jendela dan aplikasi Anda yang terbuka serta memungkinkan Anda dengan cepat meminimalkan dan memulihkan mereka.</p>

<p>At the right-hand side of the window list, GNOME displays a short
 identifier for the current workspace, such as <gui>1</gui> for the first
 (top) workspace. In addition, the identifier also displays the total number
 of available workspaces. To switch to a different workspace, you can click
 the identifier and select the workspace you want to use from the menu.</p>

<!-- <p>If an application or a system component wants to get your attention, it
 will display a blue icon at the right-hand side of the window list. Clicking
 the blue icon shows the <link xref="shell-notifications">message tray</link>,
 which lets you access all your notifications.</p> -->

</section>

<section id="gnome-classic-switch">
<title>Bertukar ke dan dari GNOME Klasik</title>

<note if:test="!platform:gnome-classic" style="important">
<p>GNOME Classic is only available on systems with certain GNOME Shell extensions
 installed. Some Linux distributions may not have these extensions available or
 installed by default.</p>
</note>

  <steps>
    <title>To switch from <em>GNOME</em> to <em>GNOME Classic</em>:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar, click your name and then choose the right option.</p>
    </item>
    <item>
      <p>Pesan konfirmasi akan muncul. Pilih <gui>Log Keluar</gui> untuk konfirmasi.</p>
    </item>
    <item>
      <p>At the login screen, select your name from the list.</p>
    </item>
    <item>
      <p>Enter your password in the password entry box.</p>
    </item>
    <item>
      <p>Click the options icon, which is displayed to the left of the <gui>Sign
      In</gui> button, and select <gui>GNOME Classic</gui>.</p>
    </item>
    <item>
      <p>Click the <gui>Sign In</gui> button.</p>
    </item>
  </steps>

  <steps>
    <title>To switch from <em>GNOME Classic</em> to <em>GNOME</em>:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar, click your name and then choose the right option.</p>
    </item>
    <item>
      <p>Pesan konfirmasi akan muncul. Pilih <gui>Log Keluar</gui> untuk konfirmasi.</p>
    </item>
    <item>
      <p>At the login screen, select your name from the list.</p>
    </item>
    <item>
      <p>Enter your password in the password entry box.</p>
    </item>
    <item>
      <p>Click the options icon, which is displayed to the left of the <gui>Sign
      In</gui> button, and select <gui>GNOME</gui>.</p>
    </item>
    <item>
      <p>Click the <gui>Sign In</gui> button.</p>
    </item>
  </steps>
  
</section>

</page>
