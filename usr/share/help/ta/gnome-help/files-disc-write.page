<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="ta">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ஒரு CD/DVD எழுது பயன்பாட்டைப் பயன்படுத்தி கோப்புகள் மற்றும் ஆவணங்களை ஒரு காலியான CD அல்லது DVD இல் சேமித்தல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>ஒரு CD அல்லது DVD க்கு கோப்புகளை எழுதுதல்</title>

  <p>நீங்கள் <gui>CD/DVD உருவாக்கி</gui> பயன்பாட்டைப் பயன்படுத்தி ஒரு காலி வட்டில் கோப்புகளை சேமிக்க முடியும். நீங்கள் உங்கள் CD/DVD எழுது கருவியில் ஒரு CD ஐ செருகிய உடனே கோப்பு மேலாளரில் ஒரு CD அல்லது DVD ஐ உருவாக்குவதற்கான விருப்பம் தோன்றும். நீங்கள் மற்ற கணினிகளுக்கு கோப்புகளை அனுப்ப அல்லது காலி வட்டில் கோப்புகளை எழுதுவதன் மூலம் <link xref="backup-why">மறுபிரதி</link> எடுக்க கோப்பு மேலாளர் உதவும். ஒரு CD அல்லது DVD க்கு கோப்புகளை எழுத:</p>

  <steps>
    <item>
      <p>உங்கள் CD/DVD எழுது இயக்கியில் ஒரு காலி வட்டை இடவும்.</p></item>
    <item>
      <p>திரையின் கீழே பாப்-அப் ஆகும் <gui>காலி CD/DVD வட்டு</gui> அறிவிப்பில், <gui>CD/DVD உருவாக்கி பயன்பாட்டைக் கொண்டு திற</gui> என்பதைத் தேர்ந்தெடுக்கவும். <gui>CD/DVD உருவாக்கி பயன்பாட்டின்</gui> கோப்புறை சாளரம் திறக்கும்.</p>
      <p>(நீங்கள் கோப்பு மேலாளர் பக்கப்பட்டியில் உள்ள <gui>சாதனங்கள்</gui> என்பதன் கீழ் உள்ள <gui>காலி CD/DVD</gui> என்பதை சொடுக்கியும் இதைச் செய்யலாம்.)</p>
    </item>
    <item>
      <p><gui>வட்டு பெயர்</gui> புலத்தில், வட்டுக்கு ஒரு பெயரைத் தட்டச்சு செய்யவும்.</p>
    </item>
    <item>
      <p>தேவையான கோப்புகளை அந்த சாளரத்திற்கு நகலெடுக்கவும் அல்லது இழுத்து இடவும்.</p>
    </item>
    <item>
      <p><gui>வட்டில் எழுது</gui> என்பதை சொடுக்கவும்.</p>
    </item>
    <item>
      <p><gui>எழுத ஒரு வட்டைத் தேர்வு செய்யவும்</gui> என்பதில் காலி வட்டு என்பதைத் தேர்வு செய்யவும்.</p>
      <p>(நீங்கள் மாறாக <gui>படக் கோப்பு</gui> என்பதையும் தேர்வு செய்யலாம். இப்படிச் செய்தால் கோப்புகள் ஒரு <em>வட்டுப் படத்தில்</em> வைக்கப்படும். அது உங்கள் கணினியில் சேமிக்கப்படும். அதன் பிறகு அந்த வட்டு படத்தை ஒரு காலி வட்டில் எழுதிக்கொள்ளலாம்.)</p>
    </item>
    <item>
      <p>நீங்கள் எழுதும் வேகம், தற்காலிக கோப்புகளின் இருப்பிடம் மற்றும் பிற விருப்பங்களை சரி செய்ய விரும்பினால் <gui>பண்புகள்</gui> ஐ சொடுக்கவும். முன்னிருப்பு விருப்பங்களே சரியாக இருக்கும்.</p>
    </item>
    <item>
      <p>பதிவு தொடங்க <gui>எழுது</gui> பொத்தானை சொடுக்கவும்.</p>
      <p><gui>சில நகல்களை எழுதவும்</gui> என்பதைத் தேர்ந்தெடுத்தால், கூடுதல் வட்டுகள் வேண்டும் எனக் கேட்கப்படும்.</p>
    </item>
    <item>
      <p>வட்டு எழுதுதல் செயல் நிறைவடையும் போது, அது தானாகவே வெளியே தள்ளப்படும். அப்போது <gui>மேலும் நகல்களை உருவாக்கு</gui> என்பதைத் தேர்வு செய்யவும் அல்லது வெளியேற <gui>மூடு</gui> என்பதைத் தேர்வு செய்யவும்.</p>
    </item>
  </steps>

<section id="problem">
  <title>If the disc wasn’t burned properly</title>

  <p>Sometimes the computer doesn’t record the data correctly, and you won’t be
  able to see the files you put onto the disc when you insert it into a
  computer.</p>

  <p>In this case, try burning the disc again but use a lower burning speed,
  for example, 12x rather than 48x. Burning at slower speeds is more reliable.
  You can choose the speed by clicking the <gui>Properties</gui> button in the
  <gui>CD/DVD Creator</gui> window.</p>

</section>

</page>
