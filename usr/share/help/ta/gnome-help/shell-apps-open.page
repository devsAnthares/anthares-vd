<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="ta">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ஷோபா தியாகி</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>செயல்பாடுகள் மேலோட்டத்தில்</gui> இருந்து பயன்பாடுகளை துவக்குதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>பயன்பாடுகளைத் தொடங்குதல்</title>

  <p if:test="!platform:gnome-classic">Move your mouse pointer to the
  <gui>Activities</gui> corner at the top left of the screen to show the
  <gui xref="shell-introduction#activities">Activities</gui> overview. This is where you
  can find all of your applications. You can also open the overview by pressing
  the <key xref="keyboard-key-super">Super</key> key.</p>
  
  <p if:test="platform:gnome-classic">You can start applications from the
  <gui xref="shell-introduction#activities">Applications</gui> menu at the top
  left of the screen, or you can use the <gui>Activities</gui> overview by
  pressing the <key xref="keyboard-key-super">Super</key> key.</p>

  <p>There are several ways of opening an application once you’re in the
  <gui>Activities</gui> overview:</p>

  <list>
    <item>
      <p>Start typing the name of an application — searching begins instantly.
      (If this doesn’t happen, click the search bar at the top of the screen
      and start typing.) If you don’t know the exact name of an application, try
      to type an related term. Click the application’s icon to start it.</p>
    </item>
    <item>
      <p>சில பயன்பாடுகளின் சின்னங்கள் <em>டேஷில்</em> இருக்கும், <gui>செயல்பாடுகள்</gui> மேலோட்டத்தின் இடப்புறம் உள்ள சின்னங்கள் நிரம்பிய பட்டையே டேஷ் ஆகும். பயன்பாடு ஒன்றை தொடங்க அதன் சின்னத்தை அங்கு சொடுக்கவும்.</p>
      <p>நீங்கள் சில பயன்பாடுகளை மிக அடிக்கடி பயன்படுத்தினால், <link xref="shell-apps-favorites">அவற்றை டேஷில் சேர்க்கலாம்</link>.</p>
    </item>
    <item>
      <p>டேஷின் அடிப்பகுதியில் உள்ள கிரிட் பொத்தானை சொடுக்கவும். <gui style="button">அடிக்கடி</gui> காட்சி செயல்படுத்தப்பட்டிருந்தால் அடிக்கடி பயன்படுத்தப்படும் பயன்பாடுகள் காண்பிக்கப்படும். ஒரு புதிய பயன்பாட்டை இயக்க விரும்பினால், அனைத்து பயன்பாடுகளையும் காண்பிக்க அடிப்பகுதியில் உள்ள <gui style="button">அனைத்தும்</gui> பொத்தானை அழுத்தவும். ஒரு பயன்பாட்டை தொடங்க அதை அழுத்தவும்.</p>
    </item>
    <item>
      <p>You can launch an application in a separate
      <link xref="shell-workspaces">workspace</link> by dragging its icon from
      the dash, and dropping it onto one of the workspaces on the right-hand
      side of the screen. The application will open in the chosen
      workspace.</p>
      <p>You can launch an application in a <em>new</em> workspace by dragging
      its icon to the empty workspace at the bottom of the workspace
      switcher, or to the small gap between two workspaces.</p>
    </item>
  </list>

  <note style="tip">
    <title>ஒரு கட்டளையை விரைவாக இயக்குதல்</title>
    <p><keyseq><key>Alt</key><key>F2</key></keyseq> ஐ அழுத்தி ஒரு பயன்பாட்டின் <em>கட்டளைப் பெயரை</em> தட்டச்சு செய்து பிறகு <key>Enter</key> விசையை அழுத்தியும் ஒரு பயன்பாட்டைத் திறக்கலாம்.</p>
    <p>For example, to launch <app>Rhythmbox</app>, press
    <keyseq><key>Alt</key><key>F2</key></keyseq> and type
    ‘<cmd>rhythmbox</cmd>’ (without the single-quotes). The name of the app is
    the command to launch the program.</p>
  </note>

</page>
