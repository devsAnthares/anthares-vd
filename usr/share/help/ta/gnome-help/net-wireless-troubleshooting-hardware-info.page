<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="ta">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu documentation wiki இன் பங்களிப்பாளர்கள்</name>
    </credit>
    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>அடுத்து செயல்படுத்தும் சிக்கல் தீர்வு செயல்களில் உங்கள் வயர்லெஸ் அடாப்ட்டரின் மாடல் எண் போன்ற குறிப்பிட்ட விவரங்கள் தேவைப்படலாம்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>வயர்லெஸ் பிணைய சிக்கல்தீர்வி</title>
  <subtitle>உங்கள் பிணைய வன்பொருள் பற்றிய தகவலைச் சேகரிக்கவும்</subtitle>

  <p>இந்த செயல்படியில், நீங்கள் உங்கள் வயர்லெஸ் பிணைய சாதனம் பற்றிய தகவல்களைச் சேகரிப்பீர்கள். வயர்லெஸ் சிக்கல்களை நீங்கள் தீர்க்கும் விதமானது வயர்லெஸ் அடாப்ட்டரின் நிறுவனம் மற்றும் மாடல் எண்ணைப் பொறுத்தது, ஆகவே இந்த விவரங்களைக் குறித்துக் கொள்ள வேண்டும். சாதன இயக்கி நிறுவல் வட்டுகள் போன்று உங்கள் கணினியுடன் வழங்கப்பட்ட பொருள்களை தயாராக வைத்திருப்பதும் உதவியாக இருக்கும். உங்களிடம் இவை உள்ளதா எனப் பார்க்கவும்:</p>

  <list>
    <item>
      <p>உங்கள் வயர்லெஸ் சாதனங்களுக்கான பேக்கேஜிங் மற்றும் அறிவுறுத்தல்கள் (குறிப்பாக உங்கள் ரௌட்டருக்கான பயனர் கையேடு)</p>
    </item>
    <item>
      <p>உங்கள் வயர்லெஸ் அடாப்ட்டருக்கான இயக்கிகளைக் கொண்டுள்ள வட்டு (அது Windows இயக்கிகளை மட்டுமே கொண்டிருந்தாலும்)</p>
    </item>
    <item>
      <p>The manufacturers and model numbers of your computer, wireless adapter
      and router. This information can usually be found on the
      underside or reverse of the device.</p>
    </item>
    <item>
      <p>Any version or revision numbers that may be printed on your wireless
      network devices or their packaging. These can be especially helpful, so
      look carefully.</p>
    </item>
    <item>
      <p>Anything on the driver disc that identifies either the device itself,
      its “firmware” version, or the components (chipset) it uses.</p>
    </item>
  </list>

  <p>முடிந்தால், வேலை செய்யக்கூடிய மாற்று இணைய இணைப்பை பெற முயற்சிக்கவும், தேவைப்பட்டால் இயக்கிகள் மற்றும் மென்பொருளைப் பதிவிறக்க இது உதவும் (உங்கள் கணினியை ஈத்தர்நெட் கேபிள் மூலம் ரௌட்டரில் நேரடியாக இணைப்பது இதற்கான ஒரு வழி, ஆனால் தேவைப்பட்டால் மட்டும் செய்யவும்).</p>

  <p>இவற்றில் கூடுமானவற்றை தயாராக சேகரித்துவைத்த பிறகு <gui>அடுத்து</gui> ஐ சொடுக்கவும்.</p>

</page>
