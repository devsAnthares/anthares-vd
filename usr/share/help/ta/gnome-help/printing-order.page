<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="ta">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>அச்சு வரிசையை குழுப்படுத்துதல் மற்றும் எதிர்வரிசைப்படுத்துதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>பக்கங்களை வித்தியாசமான வரிசையில் அச்சிட வைத்தல்</title>

  <section id="reverse">
    <title>எதிர்த்திசை</title>

    <p>அச்சுப்பொறிகள் வழக்கமாக முதல் பக்கத்தை முதலிலும் கடைசி பக்கத்தை கடைசியிலுமே அச்சிடும், அதனால் நீங்கள் அச்சிட்ட தாள்களை எடுக்கும் போது அவை எதிர்த்திசையில் அமைந்திருக்கும். தேவையெனில் நீங்கள் அச்சிடும் வரிசையை எதிர்வரிசையில் அமைக்கலாம்.</p>

    <steps>
      <title>வரிசையை எதிர்த்திசையில் அமைக்க:</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
        dialog.</p>
      </item>
      <item>
        <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
        <gui>Reverse</gui>. The last page will be printed first, and so on.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>குழுப்படுத்துதல்</title>

  <p>If you are printing more than one copy of the document, the print-outs
  will be grouped by page number by default (that is, all of the copies of page
  one come out, then the copies of page two, and so on). <em>Collating</em>
  will make each copy come out with its pages grouped together in the right
  order instead.</p>

  <steps>
    <title>To collate:</title>
    <item>
     <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
     dialog.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
      <gui>Collate</gui>.</p>
    </item>
  </steps>

</section>

</page>
