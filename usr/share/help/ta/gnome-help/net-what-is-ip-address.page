<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="ta">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>IP முகவரி என்பது உங்கள் கணினிக்கு தொலைபேசி எண் போன்றது.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>IP முகவரி என்றால் என்ன?</title>

  <p>“IP address” stands for <em>Internet Protocol address</em>, and each
  device that is connected to a network (like the internet) has one.</p>

  <p>IP முகவரி என்பது உங்கள் தொலைபேசி எண் போன்றது. உங்கள் தொலைபேசி எண் என்பது மற்றவர்கள் உங்களை அழைக்க உதவுகின்ற ஒரு குறிப்பிட்ட எண் தொகுப்பாகும், அது உங்கள் தொலைபேசியை அடையாளப்படுத்துகிறது.இதே போல், IP முகவரி என்பது உங்கள் கணினியை அடையாளப்படுத்தும் ஒரு எண் தொகுப்பாகும், அது மற்ற கணினிகளிடையே தரவை அனுப்ப பெற உதவும் தனித்துவமான அடையாளமாகும்.</p>

  <p>தற்போது, பெரும்பாலான IP முகவரிகள் நான்கு எண் தொகுதிகளைக் கொண்டுள்ளன, அவை ஒவ்வொன்றும் ஒரு புள்ளியால் பிரிக்கப்படும். <code>192.168.1.42</code> என்பது IP முகவரிக்கு ஒரு எடுத்துக்காட்டு.</p>

  <note style="tip">
    <p>An IP address can either be <em>dynamic</em> or <em>static</em>. Dynamic
    IP addresses are temporarily assigned each time your computer connects to a
    network. Static IP addresses are fixed, and do not change. Dynamic IP
    addresses are more common that static addresses — static addresses are
    typically only used when there is a special need for them, such as in the
    administration of a server.</p>
  </note>

</page>
