<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="ta">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>ஷோபா தியாகி</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><keyseq><key>Super</key><key>Tab</key></keyseq> ஐ அழுத்தவும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>சாளரங்களிடையே மாறுதல்</title>

  <p>You can see all the running applications that have a graphical user
  interface in the <em>window switcher</em>. This makes
  switching between tasks a single-step process and provides a full picture of
  which applications are running.</p>

  <p>ஒரு பணியிடத்தில் இருந்து:</p>

  <steps>
    <item>
      <p><gui>சாளர மாற்றியை</gui> கொண்டு வர <keyseq><key xref="keyboard-key-super">Super</key><key>Tab </key></keyseq> ஐ அழுத்தவும்.</p>
    </item>
    <item>
      <p>மாற்றியில் உள்ள அடுத்த (தனிப்படுத்தப்பட்ட) அடுத்த சாளரத்தைத் தேர்ந்தெடுக்க <key xref="keyboard-key-super">Super</key> ஐ விடவும்.</p>
    </item>
    <item>
      <p>இல்லாவிட்டால், <key xref="keyboard-key-super"> Super</key> விசையை அழுத்திக்கொண்டே <key>Tab</key> ஐ அழுத்தினால் திறந்துள்ள சாளரங்களிடையே மாறலாம் அல்லது பின் திசையில் மாறிச் செல்ல <keyseq><key>Shift</key><key>Tab</key></keyseq> ஐ அழுத்தலாம்.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">திறந்துள்ள உங்கள் சாளரங்கள் அனைத்தையும் அணுகி அவற்றுக்கிடையே மாற நீங்கள் அடிப் பட்டியில் உள்ள சாளர பட்டியலையும் பயன்படுத்தலாம்.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>சாளர மாற்றியில் உள்ள சாளரங்கள், பயன்பாட்டின் படி குழுப்படுத்தப்பட்டிருக்கும். நீங்கள் பயன்பாட்டை சொடுக்கும் போது அவற்றின் பல சாளரங்களுடன் மாதிரிக்காட்சி கீழ் இறக்கிக் காட்டப்படும். பட்டியலில் ஒவ்வொன்றாக மாற <key xref="keyboard-key-super">Super</key> ஐ அழுத்திக் கொண்டு <key>`</key> ஐ (அல்லது <key>Tab</key> க்கு மேல் உள்ள விசையை) அழுத்தவும்.</p>
  </note>

  <p>நீங்கள் சாளர மாற்றியில் உள்ள பயன்பாட்டு சின்னங்களிடையே மாற <key>→</key> அல்லது <key>←</key> விசைகளையும் பயன்படுத்தலாம் அல்லது சொடுக்கியில் சொடுக்கியும் ஒன்றைத் தேர்ந்தெடுக்கலாம்.</p>

  <p>ஒரே சாளரத்தைக் கொண்ட பயன்பாடுகளின் மாதிரிக்காட்சிகளைக் காண்பிக்க <key>↓</key> விசையைப் பயன்படுத்தலாம்.</p>

  <p>From the <gui>Activities</gui> overview, click on a
  <link xref="shell-windows">window</link> to switch to it and leave the
  overview. If you have multiple
  <link xref="shell-windows#working-with-workspaces">workspaces</link> open,
  you can click on each workspace to view the open windows on each
  workspace.</p>

</page>
