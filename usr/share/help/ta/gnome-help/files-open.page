<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="ta">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>கிறிஸ்ட்டோஃபர் தாமஸ்</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Open files using an application that isn’t the default one for that
    type of file. You can change the default too.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>மற்ற பயன்பாடுகளைக் கொண்டு கோப்புகளைத் திறக்கவும்</title>

  <p>When you double-click (or middle-click) a file in the file manager, it
  will be opened with the default application for that file type. You can open
  it in a different application, search online for applications, or set the
  default application for all files of the same type.</p>

  <p>To open a file with an application other than the default, right-click
  the file and select the application you want from the top of the menu. If
  you do not see the application you want, select <gui>Open With Other
  Application</gui>. By default, the file manager only shows applications that
  are known to handle the file. To look through all the applications on your
  computer, click <gui>View All Applications</gui>.</p>

<p>If you still cannot find the application you want, you can search for
more applications by clicking <gui>Find New Applications</gui>. The
file manager will search online for packages containing applications
that are known to handle files of that type.</p>

<section id="default">
  <title>முன்னிருப்பு பயன்பாட்டை மாற்றுதல்</title>
  <p>ஒரு குறிப்பிட்ட வகை கோப்புகளைத் திறக்க பயன்படும் முன்னிருப்பு பயன்பாட்டை நீங்கள் மாற்ற முடியும். இதன் மூலம் நீங்கள் ஒரு கோப்பை திறக்க இரு சொடுக்கும் போது நீங்கள் விரும்பும் பயன்பாட்டால் அது திறக்கப்படும். உதாரணமாக, நீங்கள் ஒரு MP3 கோப்பை இரு சொடுக்கும் போது உங்கள் பிடித்தமான இசை பிளேயரே அதைத் திறக்க வேண்டும் என நீங்கள் அமைக்க விரும்பலாம்.</p>

  <steps>
    <item><p>நீங்கள் முன்னிருப்பு பயன்பாட்டை மாற்ற விரும்பும் வகை கொண்ட ஒரு கோப்பை தேர்ந்தெடுக்கவும். உதாரணமாக, MP3 கோப்புகளைத் திறக்க பயன்படுத்தப்படும் பயன்பாட்டை மாற்ற ஒரு <file>.mp3</file> கோப்பைபத் தேர்ந்தெடுக்கவும்.</p></item>
    <item><p>கோப்பை வலது சொடுக்கம் செய்து, <gui>பண்புகள்</gui> -ஐத் தேர்ந்தெடுக்கவும்.</p></item>
    <item><p><gui>இதைக் கொண்டு திற</gui> தாவலைத் தேர்ந்தெடுக்கவும்.</p></item>
    <item><p>Select the application you want and click
    <gui>Set as default</gui>.</p>
    <p>If <gui>Other Applications</gui> contains an application you sometimes
    want to use, but do not want to make the default, select that application
    and click <gui>Add</gui>. This will add it to <gui>Recommended
    Applications</gui>. You will then be able to use this application by
    right-clicking the file and selecting it from the list.</p></item>
  </steps>

  <p>இப்படி செய்வதால், தேர்ந்தெடுக்கப்பட்ட கோப்புக்கான முன்னிருப்பு பயன்பாடு மட்டுமல்லாமல் அந்த வகை கோப்புகள் அனைத்துக்குமான முன்னிருப்பு பயன்பாடும் மாற்றப்படுகிறது.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
