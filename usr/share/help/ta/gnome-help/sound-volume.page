<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="ta">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>

    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>கணினிக்கான ஒலியளவை அமைத்தல் மற்றும் ஒவ்வொரு பயன்பாட்டுக்குமான ஒலியளவைக் கட்டுப்படுத்துதல்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>ஒலியளவை மாற்றுதல்</title>

  <p>To change the sound volume, open the <gui xref="shell-introduction#yourname">system
  menu</gui> from the right side of the top bar and move the volume slider left
  or right. You can completely turn off sound by dragging the slider to the
  left.</p>

  <p>Some keyboards have keys that let you control the volume. They normally
  look like stylized speakers with waves coming out of them. They are often
  near the “F” keys at the top. On laptop keyboards, they are usually on the
  “F” keys.  Hold down the <key>Fn</key> key on your keyboard to use them.</p>

  <p>If you have external speakers, you can also change the volume
  using the speakers’ volume control. Some headphones have a
  volume control too.</p>

<section id="apps">
 <title>தனித்தனி பயன்பாடுகளுக்கான ஒலியளவை மாற்றுதல்</title>

  <p>You can change the volume for one application and leave the volume for
  others unchanged. This is useful if you are listening to music and browsing
  the web, for example. You might want to turn off the audio in the web browser
  so sounds from websites do not interrupt the music.</p>

  <p>Some applications have volume controls in their main windows. If your
  application has its volume control, use that to change the volume. If not:</p>

    <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sound</gui> to open the panel.</p>
    </item>
    <item>
      <p>Go to the <gui>Applications</gui> tab and change the volume of the
      application listed there.</p>

  <note style="tip">
    <p>Only applications that are playing sounds are listed. If an
    application is playing sounds but is not listed, it might not support the
    feature that lets you control its volume in this way. In such case, you
    cannot change its volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
