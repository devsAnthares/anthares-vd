<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-scanner" xml:lang="ta">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>ரிச்சர்ட் ஹியூகஸ்</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>உங்கள் ஸ்கேனரைக் கொண்டு துல்லியமான நிறங்களை ஸ்கேன் செய்ய அதனை அளவை வகுப்பது முக்கியம்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>என் ஸ்கேனரை எப்படி அளவை வகுப்பது?</title>

  <p>If you want your scanner to represent the color accurately in scans, you
  should calibrate it.</p>

  <steps>
    <item>
      <p>Make sure your scanner is connected to your computer with a cable or
      over the network.</p>
    </item>
    <item>
      <p>Scan your calibration target and save it as an uncompressed TIFF
      file.</p>
    </item>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select your scanner.</p>
    </item>
    <item>
      <p>Press <gui style="button">Calibrate…</gui> to commence the
      calibration.</p>
    </item>
  </steps>

  <note style="tip">
    <p>ஸ்கேனர் சாதனங்கள் நாட்கள் சென்றாலும் வெப்பநிலை மாற்றங்கள் இருந்தாலும் மிகவும் நிலைத்தன்மை வாய்ந்தவை, ஆகவே பொதுவாக அவற்றை மீண்டும் அளவை வகுக்க வேண்டியதில்லை.</p>
  </note>

</page>
