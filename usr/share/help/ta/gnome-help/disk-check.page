<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-check" xml:lang="ta">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>நடாலியா ருஸ் லெய்வா</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Test your hard disk for problems to make sure that it’s healthy.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

<title>உங்கள் வன்வட்டில் ஏதேனும் பிரச்சினைகள் உள்ளதா என சோதித்தல்</title>

<section id="disk-status">
 <title>வன் வட்டை சோதித்தல்</title>
  <p>வன் வட்டுகளில் <app> SMART</app> (சுய கண்காணிப்பு, பகுப்பாய்வு மற்றும் அறிக்கையிடுதல் தொழில்நுட்பம்) எனப்படும் ஒரு உள்ளமைக்கப்பட்ட ஆரோக்கிய பரிசோதனைக் கருவி உள்ளது, அது வட்டில் ஏதேனும் சிக்கல்கள் ஏற்பட வாய்ப்புள்ளதா என தொடர்ந்து சோதிக்கிறது. அது உங்கள் வட்டு செயலிழக்கும் நிலையில் இருந்தால் எச்சரித்து முக்கியமான தரவு இழப்புகளைத் தவிர்க்க உதவுகிறது.</p>

  <p>Although SMART runs automatically, you can also check your disk’s
 health by running the <app>Disks</app> application:</p>

<steps>
 <title>Check your disk’s health using the Disks application</title>

  <item>
    <p>Open <app>Disks</app> from the <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p>Select the disk you want to check from the list of storage devices on
    the left. Information and status of the disk will be shown.</p>
  </item>
  <item>
    <p>Click the menu button and select <gui>SMART Data &amp; Self-Tests…</gui>.
    The <gui>Overall Assessment</gui> should say “Disk is OK”.</p>
  </item>
  <item>
    <p>See more information under <gui>SMART Attributes</gui>, or click the
    <gui style="button">Start Self-test</gui> button to run a self-test.</p>
  </item>

</steps>

</section>

<section id="disk-not-healthy">

 <title>What if the disk isn’t healthy?</title>

  <p>Even if the <gui>Overall Assessment</gui> indicates that the disk
  <em>isn’t</em> healthy, there may be no cause for alarm. However, it’s better
  to be prepared with a <link xref="backup-why">backup</link> to prevent data
  loss.</p>

  <p>If the status says “Pre-fail”, the disk is still reasonably healthy but
 signs of wear have been detected which mean it might fail in the near future.
 If your hard disk (or computer) is a few years old, you are likely to see
 this message on at least some of the health checks. You should
 <link xref="backup-how">backup your important files regularly</link> and check
 the disk status periodically to see if it gets worse.</p>

  <p>அது இன்னும் மோசமாகிப் போனால், கணினியை அல்லது வன் வட்டின் பிரச்சனையைக் கண்டறிய அல்லது சரி செய்ய அதை ஒரு தொழில்முறை வல்லுநரிடம் கொண்டு செல்ல வேண்டும்.</p>

</section>

</page>
