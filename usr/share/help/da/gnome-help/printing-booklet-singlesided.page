<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-singlesided" xml:lang="da">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Udskriv en brochure/pjece fra en PDF ved brug af en enkeltsidet printer.</desc>
  </info>

  <title>Udskriv en brochure/pjece på en enkeltsidet printer</title>

  <note>
    <p>Dette er instruktioner til at udskrive en brochure/pjece fra et PDF-dokument.</p>
    <p>If you want to print a booklet from a <app>LibreOffice</app> document,
    first export it to a PDF by choosing <guiseq><gui>File</gui><gui>Export
    as PDF…</gui></guiseq>. Your document needs to have a multiple of 4
    number of pages (4, 8, 12, 16,…). You may need to add up to 3 blank
    pages.</p>
  </note>

  <p>To print:</p>

  <steps>
    <item>
      <p>Open the print dialog. This can normally be done through
      <gui style="menuitem">Print</gui> in the menu or using the
      <keyseq><key>Ctrl</key><key>P</key></keyseq> keyboard shortcut.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>In the <gui>Orientation</gui> drop-down list, make sure that
      <gui>Landscape</gui> is selected.</p>
      <p>Click <gui>OK</gui> to go back to the print dialog.</p>
    </item>
    <item>
      <p>Under <gui>Range and Copies</gui>, choose <gui>Pages</gui>.</p>
      <p>Type the numbers of the pages in this order (n is the total number of
      pages, and a multiple of 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>…indtil du har skrevet alle siderne.</p>
    <note>
      <p>Eksempler:</p>
      <p>4-siders brochure/pjece: Skriv <input>4,1,2,3</input></p>
      <p>8-siders brochure/pjece: Skriv <input>8,1,2,7,6,3,4,5</input></p>
      <p>12-siders brochure/pjece: Skriv <input>12,1,2,11,10,3,4,9,8,5,6,7</input></p>
      <p>16-siders brochure/pjece: Skriv <input>16,1,2,15,14,3,4,13,12,5,6,11,10,7,8,9</input></p>
      <p>20-siders brochure/pjece: Skriv <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p>
     </note>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Layout</gui>, select <gui>Brochure</gui>.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>Front sides / right pages</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Udskriv</gui>.</p>
    </item>
    <item>
      <p>When all the pages have printed, flip the pages over and place them
      back in the printer.</p>
    </item>
    <item>
      <p>Open the print dialog. This can normally be done through
      <gui style="menuitem">Print</gui> in the menu or using the
      <keyseq><key>Ctrl</key><key>P</key></keyseq> keyboard shortcut.</p>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>Back sides / left pages</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Udskriv</gui>.</p>
    </item>
  </steps>

</page>
