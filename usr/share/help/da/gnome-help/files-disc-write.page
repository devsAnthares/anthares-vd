<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="da">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Læg filer og dokumenter på en tom CD eller DVD ved brug af en CD/DVD-brænder.</desc>
  </info>

  <title>Write files to a CD or DVD</title>

  <p>You can put files onto a blank disc by using <gui>CD/DVD Creator</gui>.
  The option to create a CD or DVD will appear in the file manager as soon as
  you place the CD into your CD/DVD writer. The file manager lets you transfer
  files to other computers or perform <link xref="backup-why">backups</link> by
  putting files onto a blank disc. To write files to a CD or DVD:</p>

  <steps>
    <item>
      <p>Place an empty disc into your CD/DVD writable drive.</p></item>
    <item>
      <p>In the <gui>Blank CD/DVD-R Disc</gui> notification that pops up at the
      bottom of the screen, select <gui>Open with CD/DVD Creator</gui>. The
      <gui>CD/DVD Creator</gui> folder window will open.</p>
      <p>(du kan også klikke på <gui>Tom CD/DVD-R-disk</gui> under <gui>Enheder</gui> i filhåndteringens sidepanel).</p>
    </item>
    <item>
      <p>In the <gui>Disc Name</gui> field, type a name for the disc.</p>
    </item>
    <item>
      <p>Drag or copy the desired files into the window.</p>
    </item>
    <item>
      <p>Click <gui>Write to Disc</gui>.</p>
    </item>
    <item>
      <p>Under <gui>Select a disc to write to</gui>, choose the blank disc.</p>
      <p>(du kan i stedet vælge <gui>Aftryksfil</gui>. Dette lægger filerne i et <em>diskaftryk</em>, som gemmes på din computer. Du kan herefter brænde diskaftrykket på en tom disk på et senere tidspunkt).</p>
    </item>
    <item>
      <p>Klik på <gui>Egenskaber</gui> hvis du vil justere brændingshastigheden, placeringen af midlertidige filer, og andre valgmuligheder. Standardindstillingerne skulle virke fint.</p>
    </item>
    <item>
      <p>Klik på <gui>Brænd</gui>-knappen for at starte optagelsen.</p>
      <p>If <gui>Burn Several Copies</gui> is selected, you will be prompted
      for additional discs.</p>
    </item>
    <item>
      <p>When the disc burning is complete, it will eject automatically. Choose
      <gui>Make More Copies</gui> or <gui>Close</gui> to exit.</p>
    </item>
  </steps>

<section id="problem">
  <title>If the disc wasn’t burned properly</title>

  <p>Sometimes the computer doesn’t record the data correctly, and you won’t be
  able to see the files you put onto the disc when you insert it into a
  computer.</p>

  <p>In this case, try burning the disc again but use a lower burning speed,
  for example, 12x rather than 48x. Burning at slower speeds is more reliable.
  You can choose the speed by clicking the <gui>Properties</gui> button in the
  <gui>CD/DVD Creator</gui> window.</p>

</section>

</page>
