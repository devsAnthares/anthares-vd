<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="hi">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>ग्नोम प्रलेखन परियोजना</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>आपके पास शायद सही कोडेक संस्थापित ना हो या फिर डीवीडी के ग़लत क्षेत्र से होना भी करण हो सकता है।</desc>
  </info>

  <title>Why won’t DVDs play?</title>

  <p>If you insert a DVD into your computer and it doesn’t play, you may not
  have the right DVD <em>codecs</em> installed, or the DVD might be from a
  different <em>region</em>.</p>

<section id="codecs">
  <title>डीवीडी चलाने के लिए सही कोडेक की संस्थापना करना।</title>

  <p>In order to play DVDs, you need to have the right <em>codecs</em>
  installed.  A codec is a piece of software that allows applications to read a
  video or audio format. If your movie player software doesn’t find the right
  codecs, it may offer to install them for you. If not, you’ll have to install
  the codecs manually — ask for help on how to do this, for example on your
  Linux distribution’s support forums.</p>

  <p>DVDs are also <em>copy-protected</em> using a system called CSS. This
  prevents you from copying DVDs, but it also prevents you from playing them
  unless you have extra software to handle the copy protection. This software
  is available from a number of Linux distributions, but cannot be legally used
  in all countries. You can buy a commercial DVD decoder that can handle copy
  protection from
  <link href="http://fluendo.com/shop/product/oneplay-dvd-player/">Fluendo</link>.
  It works with Linux and should be legal to use in all countries.</p>

  </section>

<section id="region">
  <title>डीवीडी के क्षेत्र को जाँचना।</title>

  <p>DVDs have a <em>region code</em>, which tells you in which region of the
  world they are allowed to be played. If the region of your computer’s DVD
  player does not match the region of the DVD you are trying to play, you won’t
  be able to play the DVD. For example, if you have a Region 1 DVD player, you
  will only be allowed to play DVDs from North America.</p>

  <p>It is often possible to change the region used by your DVD player, but it
  can only be done a few times before it locks into one region permanently. To
  change the DVD region of your computer’s DVD player, use <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>You can find
  <link href="https://en.wikipedia.org/wiki/DVD_region_code">more information
  about DVD region codes on Wikipedia</link>.</p>

</section>

</page>
