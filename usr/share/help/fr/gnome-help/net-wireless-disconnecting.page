<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="fr">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Avoir un signal faible, ou ne pas pouvoir se connecter correctement au réseau.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Interruptions intempestives du réseau sans fil</title>

<p>Il peut arriver que vous soyez déconnecté intempestivement d'un réseau sans fil. Normalement, votre ordinateur essaie de se reconnecter dès que cela arrive (l'icône réseau de la barre supérieure affiche trois points pendant les essais de reconnexion), mais ce peut être gênant, notamment si vous étiez sur Internet à ce moment là.</p>

<section id="signal">
 <title>Signal sans fil faible</title>

 <p>Un signal réseau sans fil faible est la principale cause de déconnexions intempestives. Les réseaux sans fil ont un rayon de capture limité, et si vous vous trouvez un peu trop éloigné de la station émettrice, le signal devient trop faible pour maintenir une connexion fiable. Des murs ou d'autres obstacles entre vous et l'émetteur peuvent aussi affaiblir le signal.</p>

 <p>L'icône réseau dans la barre supérieure affiche la puissance du signal sans fil. Si elle vous semble faible, essayez de vous rapprocher de la base émettrice.</p>

</section>

<section id="network">
 <title>Mauvaise connexion au réseau</title>

 <p>Sometimes, when you connect to a wireless network, it may appear that you
 have successfully connected at first, but then you will be disconnected soon
 after. This normally happens because your computer was only partially
 successful in connecting to the network — it managed to establish a connection,
 but was unable to finalize the connection for some reason and so was
 disconnected.</p>

 <p>De probables raisons à ceci sont la saisie d'une phrase de passe erronée ou l'interdiction de votre ordinateur sur le réseau (un nom d'utilisateur manquant pendant l'identification par exemple).</p>

</section>

<section id="hardware">
 <title>Pilote ou périphérique réseau sans fil instables</title>

 <p>Certains équipements pour réseau sans fil peuvent être quelque peu instables. Les réseaux sans fil sont complexes et les cartes réseau comme les émetteurs peuvent connaître parfois des problèmes mineurs et perdre la connexion. C'est ennuyeux, mais cela arrive fréquemment avec beaucoup de périphériques. S'il vous arrive d'être déconnecté de temps en temps du réseau sans fil, ce peut être l'unique raison. Si cela vous arrive très fréquemment, il serait bon d'envisager de changer de matériel.</p>

</section>

<section id="busy">
 <title>Réseaux sans fil encombrés</title>

 <p>Dans certains endroits très fréquentés (comme dans les universités et les cybercafés par exemple), il y a souvent de très nombreux ordinateurs qui essaient de se connecter tous en même temps. Les réseaux deviennent alors surchargés et n'arrivent plus à traiter toutes ces connexions, il y a donc des ordinateurs qui se retrouvent déconnectés.</p>

</section>

</page>
