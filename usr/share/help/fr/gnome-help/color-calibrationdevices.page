<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="fr">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Nous prenons en charge de nombreux périphériques d'étalonnage.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Prise en charge d'instruments de mesure de la couleur</title>

  <p>GNOME s'appuie sur le système de gestion de couleurs Argyll pour prendre en charge les instruments de mesure de couleurs. Ainsi, les instruments de mesure de couleurs suivants sont pris en charge :</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (spectromètre)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (spectromètre)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 or LT (colorimètre)</p></item>
    <item><p>X-Rite i1 Display Pro (colorimètre)</p></item>
    <item><p>X-Rite ColorMunki Design ou Photo (spectromètre)</p></item>
    <item><p>X-Rite ColorMunki Création (colorimètre)</p></item>
    <item><p>X-Rite ColorMunki Display (colorimètre)</p></item>
    <item><p>Pantone Huey (colorimètre)</p></item>
    <item><p>MonacoOPTIX (colorimètre)</p></item>
    <item><p>ColorVision Spyder 2 and 3 (colorimètre)</p></item>
    <item><p>Colorimètre HCFR (colorimètre)</p></item>
  </list>

  <note style="tip">
   <p>Le Pantone Huey est actuellement le matériel le moins cher et le mieux pris en charge par Linux.</p>
  </note>

  <p>
    Thanks to Argyll there’s also a number of spot and strip reading
    reflective spectrometers supported to help you calibrating and
    characterizing your printers:
  </p>

  <list>
    <item><p>X-Rite DTP20 “Pulse” (“swipe” type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP22 Digital Swatchbook (spot type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP41 (spot and strip reading reflective spectrometer)</p></item>
    <item><p>X-Rite DTP41T (spot and strip reading reflective spectrometer)</p></item>
    <item><p>X-Rite DTP51 (spot reading reflective spectrometer)</p></item>
  </list>

</page>
