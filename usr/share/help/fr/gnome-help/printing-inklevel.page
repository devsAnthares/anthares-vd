<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="fr">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vérifier le niveau d'encre ou de toner des cartouches de l'imprimante.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>How can I check my printer’s ink or toner levels?</title>

  <p>La façon de vérifier le niveau d'encre ou de toner de votre imprimante dépend de son modèle, de sa marque et des pilotes et applications installés sur votre ordinateur.</p>

  <p>Quelques imprimantes ont un petit écran intégré qui affiche l'état des niveaux et d'autres informations.</p>

  <p>Certaines imprimantes indiquent leur niveau d'encre à l'ordinateur dans le panneau <gui>Imprimantes</gui> des <gui>Paramètres</gui>.</p>

  <p>Les pilotes et outils de diagnostic pour la plupart des imprimantes HP sont disponibles dans le projet HPLIP (Linux Imaging and Printing Project). D'autres fabricants sont susceptibles de fournir des pilotes propriétaires avec des fonctionnalités similaires.</p>

  <p>Une autre possibilité consiste à installer une application de gestion ou de contrôle des niveaux d'encre. <app>Inkblot</app> affiche les niveaux d'encre de beaucoup d'imprimantes HP, Epson et Canon. Vous pouvez vérifier ici la <link href="http://libinklevel.sourceforge.net/#supported">liste des modèles pris en charge</link> pour voir si votre imprimante en fait partie. <app>mtink</app> est une autre application pour les imprimantes Epson et quelques autres.</p>

  <p>Quelques imprimantes ne sont pas encore bien prises en charge par Linux et d'autres ne sont pas conçues pour retourner leur niveau d'encre.</p>

</page>
