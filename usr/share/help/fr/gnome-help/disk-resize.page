<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="fr">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Shrink or grow a filesystem and its partition.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Adjust the size of a filesystem</title>

  <p>A filesystem can be grown to make use of the free space after its
     partition. Often this is even possible while the filesystem is
     mounted.</p>
  <p>To make space for another partition after the filesystem, it can be
     shrunk according to the free space within it.</p>
  <p>Not all filesystems have resize support.</p>
  <p>The partition size will be changed together with the filesystem size.
     It is also possible to resize a partition without a filesystem in the
     same way.</p>

<steps>
  <title>Resize a filesystem/partition</title>
  <item>
    <p>Ouvrez <app>Disques</app> à partir de la vue d'ensemble des <gui>Activités</gui>.</p>
  </item>
  <item>
    <p>Select the disk containing the filesystem in question from the list
       of storage devices on the left. If there is more than one volume on
       the disk, select the volume which contains the filesystem.</p>
  </item>
  <item>
    <p>In the toolbar underneath the <gui>Volumes</gui> section, click the
       menu button. Then click <gui>Resize Filesystem…</gui> or
       <gui>Resize…</gui> if there is no filesystem.</p>
  </item>
  <item>
    <p>A dialog will open where the new size can be chosen. The filesystem will
       be mounted to calculate the minimum size by the amount of current
       content. If shrinking is not supported the minimum size is the current
       size. Leave enough space within the filesystem when shrinking to ensure
       that it can work fast and reliably.</p>
    <p>Depending on how much data has to be moved from the shrunk part, the
       filesystem resize may take longer time.</p>
    <note style="warning">
      <p>The filesystem resize automatically involves
         <link xref="disk-repair">repairing</link> of the filesystem. Therefore
         it is advised to back up important data before starting. The action
         must not be stopped or it will result in a damaged filesystem.</p>
    </note>
  </item>
  <item>
      <p>Confirm to start the action by clicking
         <gui style="button">Resize</gui>.</p>
   <p>The action will unmount the filesystem if resizing a mounted filesystem
      is not supported. Be patient while the filesystem is resized.</p>
  </item>
  <item>
    <p>After completion of the needed resize and repair actions the filesystem
       is ready to be used again.</p>
  </item>
</steps>

</page>
