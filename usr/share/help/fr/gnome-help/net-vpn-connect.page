<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="fr">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Configurer une connexion VPN à un réseau local via Internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Connexion à un VPN</title>

<p>A VPN (or <em>Virtual Private Network</em>) is a way of connecting to a
 local network over the internet. For example, say you want to connect to the
 local network at your workplace while you’re on a business trip. You would
 find an internet connection somewhere (like at a hotel) and then connect to
 your workplace’s VPN. It would be as if you were directly connected to the
 network at work, but the actual network connection would be through the
 hotel’s internet connection. VPN connections are usually <em>encrypted</em>
 to prevent people from accessing the local network you’re connecting to
 without logging in.</p>

<p>There are a number of different types of VPN. You may have to install some
 extra software depending on what type of VPN you’re connecting to. Find out
 the connection details from whoever is in charge of the VPN and see which
 <em>VPN client</em> you need to use. Then, go to the software installer
 application and search for the <app>NetworkManager</app> package which works
 with your VPN (if there is one) and install it.</p>

<note>
 <p>If there isn’t a NetworkManager package for your type of VPN, you will
 probably have to download and install some client software from the company
 that provides the VPN software. You’ll probably have to follow some different
 instructions to get that working.</p>
</note>

<p>To set up the VPN connection:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Réseau</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>au bas de la liste de gauche, cliquez sur le bouton <gui>+</gui> pour ajouter une nouvelle connexion,</p>
    </item>
    <item>
      <p>choisissez <gui>VPN</gui> dans la liste de l'interface,</p>
    </item>
    <item>
      <p>choisissez votre type de connexion VPN,</p>
    </item>
    <item>
      <p>renseignez les détails de la connexion VPN et enfin cliquez sur <gui>Ajouter</gui> quand vous avez terminé.</p>
    </item>
    <item>
      <p>When you have finished setting-up the VPN, open the
      <gui xref="shell-introduction#yourname">system menu</gui> from the right side of
      the top bar, click <gui>VPN off</gui> and select <gui>Connect</gui>. You
      may need to enter a password for the connection before it is established.
      Once the connection is made, you will see a lock shaped icon in the top
      bar.</p>
    </item>
    <item>
      <p>Hopefully you will successfully connect to the VPN. If not, you may
      need to double-check the VPN settings you entered. You can do this from
      the <gui>Network</gui> panel that you used to create the connection.
      Select the VPN connection from the list, then press the
<media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">settings</span></media> button to review the settings.</p>
    </item>
    <item>
      <p>To disconnect from the VPN, click the system menu on the top bar and
      click <gui>Turn Off</gui> under the name of your VPN connection.</p>
    </item>
  </steps>

</page>
