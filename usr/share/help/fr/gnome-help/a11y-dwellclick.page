<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="fr">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>La fonctionnalité <gui>Clic par survol</gui> permet de simuler un clic en gardant la souris immobile.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Simulation d'un clic lors du survol par le pointeur</title>

  <p>Vous pouvez cliquer ou faire glisser simplement en survolant un contrôle ou un objet sur l'écran avec le pointeur de la souris. C'est utile si vous avez des difficultés à déplacer la souris et à cliquer en même temps. Cette fonctionnalité s'appelle <gui>Clic par survol</gui>.</p>

  <p>Quand <gui>Clic par survol</gui> est activé, vous pouvez déplacer le pointeur de la souris au-dessus d'un contrôle, le maintenir immobile et attendre un peu avant que le bouton ne soit cliqué pour vous.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Assistant de clic</gui> dans la rubrique <gui>Pointage et clic de souris</gui>.</p>
    </item>
    <item>
      <p>Basculez l'interrupteur <gui>Clic par survol</gui> sur <gui>Activé</gui>.</p>
    </item>
  </steps>

  <p>La fenêtre <gui>Clic par survol</gui> s'ouvre et reste au premier plan. Servez-vous en pour choisir le type de clic à effectuer lors d'un survol. Par exemple, si vous sélectionnez <gui>Clic secondaire</gui>, un clic droit est effectué après le survol. Après un double-clic, un clic secondaire ou un glisser, le mode clic simple est automatiquement sélectionné.</p>

  <p>Quand vous survolez un bouton avec le pointeur de la souris, il change progressivement de couleur. Quand la couleur a complètement changé, le bouton est cliqué.</p>

  <p>Réglez le paramètre <gui>Délai</gui> pour déterminer combien de temps vous devez maintenir le pointeur de souris immobile avant le clic automatique.</p>

  <p>Il est inutile de maintenir la souris parfaitement immobile lors du clic par survol. Le pointeur peut se déplacer légèrement sans empêcher le clic automatique au bout d'un moment. Mais s'il se déplace trop, aucun clic ne survient.</p>

  <p>Réglez le paramètre <gui>Seuil de déplacement</gui> pour modifier le seuil toléré de déplacement du pointeur lors d'un survol.</p>

</page>
