<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="hr">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Define or change keyboard shortcuts in <gui>Keyboard</gui>
    settings.</desc>
  </info>

  <title>Set keyboard shortcuts</title>

<p>To change the key or keys to be pressed for a keyboard shortcut:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the row for the desired action. The <gui>Set shortcut</gui>
      window will be shown.</p>
    </item>
    <item>
      <p>Hold down the desired key combination, or press <key>Backspace</key> to
      reset, or press <key>Esc</key> to cancel.</p>
    </item>
  </steps>


<section id="defined">
<title>Pre-defined shortcuts</title>
  <p>There are a number of pre-configured shortcuts that can be changed,
  grouped into these categories:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Launchers</title>
  <tr>
	<td><p>Osobna mapa</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> or <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>Pokreni kalkulator</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> or <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>Pokreni klijent e-pošte</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> or <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>Pokreni preglednik pomoći</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Pokreni internetski preglednik</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> or <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Pretraga</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> or <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>Settings</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigacija</title>
  <tr>
	<td><p>Hide all normal windows</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace above</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace below</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor down</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the left</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the right</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace down</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Move window one workspace up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key>
  <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window to last workspace</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Move window to workspace 1</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Move window to workspace 2</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Move window to workspace 3</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Move window to workspace 4</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Switch applications</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch system controls</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch system controls directly</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to last workspace</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to workspace 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to workspace 2</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Switch to workspace 3</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Switch to workspace 4</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
        <td><p>Switch windows</p></td>
        <td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Switch windows directly</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch windows of an app directly</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch windows of an application</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Slike zaslona</title>
  <tr>
	<td><p>Kopiraj sliku prozora u međuspremnik</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Kopiraj sliku područja u međuspremnik</p></td>
	<td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Kopiraj sliku zaslona u međuspremnik</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Record a short screencast</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key>
        </keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of a window to Pictures</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of an area to Pictures</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot to Pictures</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sound and Media</title>
  <tr>
	<td><p>Izbaci</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Eject)</p></td>
  </tr>
  <tr>
	<td><p>Pokreni medijski reproduktor</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Audio media)</p></td>
  </tr>
  <tr>
	<td><p>Sljedeća pjesma</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio next)</p></td>
  </tr>  <tr>
	<td><p>Pauziraj reprodukciju</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Audio pause)</p></td>
  </tr>
  <tr>
	<td><p>Reprodukcija (ili reprodukcija/pauza)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Audio play)</p></td>
  </tr>
  <tr>
	<td><p>Prijašnja pjesma</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio previous)</p></td>
  </tr>
  <tr>
	<td><p>Zaustavi reprodukciju</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Audio stop)</p></td>
  </tr>
  <tr>
	<td><p>Smanji glasnoću zvuka</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Audio lower volume)</p></td>
  </tr>
  <tr>
	<td><p>Utišaj zvuk</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Audio mute)</p></td>
  </tr>
  <tr>
	<td><p>Pojačaj glasnoću zvuka</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Audio raise volume)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sustav</title>
  <tr>
        <td><p>Fokusiraj aktivnu obavijest</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zaključaj zaslon</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Odjavi se</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Otvori izbornik aplikacija</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restore the keyboard shortcuts</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Prikaži sve aplikacije</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the activities overview</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the notification list</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the overview</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the run command prompt</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Typing</title>
  <tr>
  <td><p>Switch to next input source</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Switch to previous input source</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Universal Access</title>
  <tr>
	<td><p>Smanji veličinu teksta</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>High contrast on or off</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Uvećaj veličinu teksta</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Turn on-screen keyboard on or off</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Turn screen reader on or off</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Turn zoom on or off</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Uvećaj</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Umanji</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Prozori</title>
  <tr>
	<td><p>Activate the window menu</p></td>
	<td><p><keyseq><key>Alt</key><key>Space</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Close window</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Hide window</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Lower window below other windows</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Maximize window</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximize window horizontally</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Maximize window vertically</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Move window</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Raise window above other windows</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Raise window if covered, otherwise lower it</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Resize window</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restore window</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Toggle fullscreen mode</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
	<td><p>Toggle maximization state</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Toggle window on all workspaces or one</p></td>
	<td><p>Onemogućeno</p></td>
  </tr>
  <tr>
        <td><p>View split on left</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>View split on right</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Custom shortcuts</title>

  <p>To create your own application keyboard shortcut in the
  <app>Keyboard</app> settings:</p>

  <steps>
    <item>
      <p>Click the <gui style="button">+</gui> button. The <gui>Add Custom
      Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Click the row that was just added. When the
      <gui>Set Custom Shortcut</gui> window opens, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Click <gui>Add</gui>.</p>
    </item>
  </steps>

  <p>The command name that you type should be a valid system command. You can
  check that the command works by opening a Terminal and typing it in there.
  The command that opens an application cannot have the same name as the
  application itself.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the <em>name</em> of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
