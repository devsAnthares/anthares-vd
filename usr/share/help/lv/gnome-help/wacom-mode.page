<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="lv">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pārslēdziet planšeti starp planšetes un peles režīmu.</desc>
  </info>

  <title>Iestatīt Wacom planšetes izsekošanas režīmu</title>

<p><gui>Izsekošanas režīms</gui> nosaka, kā rādītājs tiek attēlots uz ekrāna.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Spiediet <gui>Iestatījumi</gui>.</p>
  </item>
  <item>
    <p>Sānu joslā nospiediet <gui>Ierīces</gui>.</p>
  </item>
  <item>
    <p>Sānu joslā spiediet <gui>Wacom planšete</gui>, lai atvērtu paneli.</p>
  </item>
  <item>
    <p>Spiediet pogu <gui>planšete</gui> virsraksta joslā.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Ja nav atklātu planšetu, jums prasīs <gui>Lūdzu, pievienojiet un ieslēdziet savu Wacom planšeti</gui>, spiediet uz saites <gui>Bluetooth iestatījumi</gui>, lai savienotos ar bezvadu planšeti.</p></note>
  </item>
  <item><p>Pie <gui>Izsekošanas režīma</gui>, izvēlieties <gui>Planšete (absolūta)</gui> vai <gui>Skārienpaliktnis (relatīva)</gui>.</p></item>
</steps>

<note style="info"><p><em>Absolūtajā</em> režīmā, katrs punkts uz planšetes attēlojas par punktu uz ekrāna. Piemēram, ekrāna augšējais kreisais stūris vienmēr atbilst tam pašam punktam uz planšetes.</p>
 <p><em>Relatīvajā</em> režīmā, ja jūs paceļat rādītāju nost no planšetes un novietojiet to kaut kur citur, kursora pozīcija uz ekrāna nemainās. Šādā veidā darbojas pele.</p>
  </note>

</page>
