<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="lv">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Samazināt vai palielināt datņu sistēmu un tās nodalījumu.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Pielāgot datņu sistēmas izmēru</title>

  <p>Datņu sistēmu var palielināt, lai tā varētu izmantot vietu pēc tās nodalījuma. Nereti tas ir iespējams pat tad, ja datņu sistēma ir montēta.</p>
  <p>Lai atbrīvotu vietu citam nodalījumam pēc datņu sistēmas, to var samazināt, ja tajā ir pietiekami daudz brīvas vietas.</p>
  <p>Ne visas datņu sistēmas atbalsta izmēra maiņu.</p>
  <p>Nodalījuma izmērs tiks mainīts kopā ar datņu sistēmas izmēru. Var arī mainīt nodalījuma izmēru, nemainot datņu sistēmas izmēru tādā pat veidā.</p>

<steps>
  <title>Mainīt datņu sistēmas / nodalījuma izmēru</title>
  <item>
    <p>Atveriet lietotni <app>Diski</app> no <gui>Aktivitāšu</gui> pārskata.</p>
  </item>
  <item>
    <p>Izvēlieties disku, kurš satur vajadzīgo datņu sistēmu, no krātuvju ierīču saraksta pa kreisi. Ja uz diska ir vairāk kā viens sējums, izvēlieties to sējumu, kurš satur vajadzīgo datņu sistēmu.</p>
  </item>
  <item>
    <p>Rīkjoslā zem <gui>Sējumu</gui> sadaļas spiediet izvēlnes pogu. Tad spiediet <gui>Mainīt datņu sistēmas izmēru…</gui> vai <gui>Mainīt izmēru…</gui> ja tur nav datņu sistēmas.</p>
  </item>
  <item>
    <p>Dialoglodziņš tiks atvērts, kur varēs izvēlēties jauno izmēru. Datņu sistēma tiks montēta, lai aprēķinātu minimālo izmēru, vadoties no esošā satura izmēra. Ja nav atbalstīta samazināšana, minimālais izmērs būs pašreizējais izmērs. Samazinot izmēru, atstājiet datņu sistēmā pietiekami daudz brīvas vietas, lai nodrošinātu, ka samazināšana notiks ātri no uzticami.</p>
    <p>Atkarībā no tā, cik daudz dati ir jāpārvieto no samazinātās daļas, datņu sistēmas izmēra maiņa var aizņemt daudz laika.</p>
    <note style="warning">
      <p>Datņu sistēmas izmēra maiņa iekļauj arī datņu sistēmas <link xref="disk-repair">labošanu</link>. Tāpēc vēlams izveidot rezerves kopijas svarīgajiem datiem. Šo darbību nevar apturēt, citādi tiks sabojāta datņu sistēma.</p>
    </note>
  </item>
  <item>
      <p>Apstipriniet darbības sākumu, spiežot <gui style="button">Mainīt izmēru</gui>.</p>
   <p>Darbība atmontēs datņu sistēmu, ja montētas datņu sistēmas izmēra maiņa nav atbalstīta. Datņu sistēmas izmēra maiņa var aizņemt daudz laika.</p>
  </item>
  <item>
    <p>Pēc izmēra maiņas un labošanas darbību pabeigšanas, datņu sistēma ir atkal gatava lietošanai.</p>
  </item>
</steps>

</page>
