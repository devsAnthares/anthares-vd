<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="lv">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kontrolē to, cik ātri jāspiež peles poga otro reizi, lai izpildītos dubultklikšķis.</desc>
  </info>

  <title>Pielāgo dubultklikšķa ātrumu</title>

  <p>Dubultklikšķis izpildās tikai tad, kad peles poga ir nospiesta divas reizes pēc kārtas pietiekami ātri. Ja otrais klikšķis notiek vēlāk, nekā atļauts, iegūsiet divus atsevišķus klikšķus, nevis dubultklikšķi. Ja peles nospiešana otro reizi pietiekami ātri sagādā problēmas, jums vajadzētu palielināt atļauto laiku.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Under <gui>Pointing &amp; Clicking</gui>, adjust the
      <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>Ja pele izpilda dubultklikšķi tad, kad esat nospiedis tikai 1 klikšķi, pelei varētu būt tehniskas problēmas. Pamēģiniet datoram pievienot citu peli un pārbaudiet, vai tad viss ir kārtībā. Vai arī, pievieno savu peli citam datoram un pārbaudiet, vai tai vēl jo projām ir šāda problēma.</p>

  <note>
    <p>Šis iestatījums ietekmēs gan peli, gan skārienpaliktni, kā arī jebkuru citu norādīšanas ierīci.</p>
  </note>

</page>
