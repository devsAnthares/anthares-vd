<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="lv">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ejiet uz <gui>aktivitāšu</gui> pārskatu un velciet logu uz citu darbvietu.</desc>
  </info>

  <title>Pārvietot logu uz citu darbvietu</title>

  <steps>
    <title>Lietojot peli:</title>
    <item>
      <p if:test="!platform:gnome-classic">Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu.</p>
      <p if:test="platform:gnome-classic">Atveriet <gui xref="shell-introduction#activities">Aktivitāšu pārskatu</gui> no izvēlnes <gui xref="shell-introduction#activities">Lietotnes</gui> ekrāna augšējā kreisajā stūrī.</p>
    </item>
    <item>
      <p>Spiediet un velciet logu pret labo ekrāna pusi.</p>
    </item>
    <item>
      <p>Parādīsies <em xref="shell-workspaces">darbvietu atlasītājs</em>.</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Nometiet logu uz tukšas darbvietas. Šī darbvieta tagad satur nomesto logu un <em>darbvietu atlasītājā</em> parādās jauna, tukša darbvieta.</p>
      <p if:test="platform:gnome-classic">Nometiet logu uz tukšas darbvietas. Šī darbvieta tagad satur nomesto logu.</p>
    </item>
  </steps>

  <steps>
    <title>Lietojot tastatūru:</title>
    <item>
      <p>Izvēlieties logu, ko vēlaties pārvietot (piemēram, lietojot <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> <em xref="shell-windows-switching">logu pārslēdzēju</em>).</p>
    </item>
    <item>
      <p>Spiediet <keyseq><key>Super</key><key>Shift</key><key>Page Up</key></keyseq>, lai pārvietotu logu uz darbvietu, kas ir virs pašreizējās darbvietas <em>darbvietu atlasītājā</em>.</p>
      <p>Spiediet <keyseq><key>Super</key><key>Shift</key><key>Page Down</key></keyseq>, lai pārvietotu logu uz darbvietu, kas ir zem pašreizējās darbvietas <em>darbvietu atlasītājā</em>.</p>
    </item>
  </steps>

</page>
