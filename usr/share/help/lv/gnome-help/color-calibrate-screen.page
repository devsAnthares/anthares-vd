<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="lv">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ekrāna kalibrēšana ir svarīga, lai attēlotu precīzas krāsas.</desc>
  </info>

  <title>Kā lai es kalibrēju savu ekrānu?</title>

  <p>Jūs varat kalibrēt savu ekrānu, lai tas rādītu precīzākas krāsas. Tas it īpaši noder tad, ja esat saistīts ar digitālo fotografēšanu, dizainu vai mākslu.</p>

  <p>Lai to izdarītu, vajadzēs vai nu kolorimetru vai spektrofotometru. Abas ierīces tiek izmantotas ekrānu profilu veidošanai, bet tās darbojas dažādos veidos.</p>

  <steps>
    <item>
      <p>Pārliecinieties, ka kalibrējamā ierīce savienota ar datoru.</p>
    </item>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Sānu joslā nospiediet <gui>Ierīces</gui>.</p>
    </item>
    <item>
      <p>Sānu joslā spiediet <gui>Krāsa</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties savu ekrānu.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">Kalibrēt…</gui>, lai sāktu kalibrēšanu.</p>
    </item>
  </steps>

  <p>Ekrāni mainās visu laiku — izgaismojums TFT monitoros samazinās uz pusi katrus 18 mēnešus, un kļūs dzeltenāks, tam kļūstot vecākam. Tas nozīmē, ka jums jāpārkalibrē jūsu ekrāns tad, kad <gui>Krāsu</gui> panelī parādās [!] ikona.</p>

  <p>LED ekrāni arī mainās līdz ar laiku, bet daudz lēnāk nekā TFT.</p>

</page>
