<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="lv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neļaut citām personām izmantot jūsu datoru, kad dodaties prom.</desc>
  </info>

  <title>Automātiski bloķēt ekrānu</title>
  
  <p>Kad atstājiet savu datoru, jums vajadzētu <link xref="shell-exit#lock-screen">bloķēt ekrānu</link>, lai neļautu citiem cilvēkiem strādāt ar jūsu datoru un piekļūt jūsu datnēm. Ja dažkārt aizmirstat bloķēt savu ekrānu, jūs varētu vēlēties automātiski bloķēt sava datora ekrānu pēc noteikta laika. Tas palīdz turēt datoru drošībā arī tad, kad to neizmantojat.</p>

  <note><p>Kad ekrāns ir bloķēts, jūsu lietotnes un sistēmas procesi turpinās darboties, bet jums vajadzēs ievadīt paroli, lai to turpinātu izmantot.</p></note>
  
  <steps>
    <title>Lai iestatītu laiku, pirms ekrāns tiek bloķēts:</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Privātums</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Privātums</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Ekrāna bloķēšana</gui>.</p>
    </item>
    <item>
      <p>Pārliecinieties, ka <gui>Automātiskā ekrāna bloķēšana</gui> pārslēgts uz <gui>|</gui>, tad izvēlieties laiku no izkrītošā saraksta.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Lietotnes var jums parādīt paziņojumus, kas būs redzami uz bloķēta ekrāna. Tas noder, ja vēlaties redzēt, vai ir pienākušas jaunas vēstules, neatbloķējot ekrānu. Ja jūs uztrauc, ka citi cilvēki varētu šos paziņojumus redzēt, izslēdziet <gui>rādīt paziņojumus</gui>.</p>
  </note>

  <p>Kad ekrāns ir bloķēts un vēlaties to atbloķēt, spiediet <key>Esc</key>, vai velciet no ekrāna apakšas ar peli. Tad ievadiet savu paroli un spiediet <key>Enter</key> vai <gui>Atbloķēt</gui>. Varat arī sākt rakstīt paroli un bloķēšanas aizkars tiks automātiski pacelts.</p>

</page>
