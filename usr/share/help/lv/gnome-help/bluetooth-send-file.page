<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-send-file" xml:lang="lv">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koplietot datnes ar Bluetooth ierīcēm, tādām kā jūsu telefons.</desc>
  </info>

  <title>Sūtīt datnes uz Bluetooth ierīci</title>

  <p>Jūs varat nosūtīt datnes uz savienotajām Bluetooth ierīcēm, tādām kā daži mobilie telefoni vai citi datori. Daži ierīču tipi neatļauj datņu vai specifisku datņu tipu pārraidi. Jūs varat sūtīt datnes, izmantojot  Bluetooth iestatījumu logu.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Bluetooth</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Pārliecinieties, ka Bluetooth ir aktivēts — pārslēdziet virsraksta joslas slēdzi uz <gui>|</gui>.</p>
    </item>
    <item>
      <p><gui>Ierīču</gui> sarakstā izvēlieties ierīci, uz kuru sūtīt datnes.Ja vēlamā ierīce nav redzama sarakstā kā <gui>Savienota</gui>, ar to vajag <link xref="bluetooth-connect-device">savienoties</link>.</p>
      <p>Parādīsies ārējai ierīcei specifisks panelis.</p>
    </item>
    <item>
      <p>Spiediet <gui>Sūtīt datnes...</gui> un parādīsies datņu izvēlētājs.</p>
    </item>
    <item>
      <p>Izvēlieties datni, kuru vēlaties sūtīt, un klikšķiniet <gui>Izvēlēties</gui>.</p>
      <p>Lai nosūtītu vairāk par vienu datni mapē, pieturiet <key>Ctrl</key> taustiņu, izvēloties datnes.</p>
    </item>
    <item>
      <p>Saņemošās ierīces īpašniekam parasti ir jānospiež poga, lai saņemtu datni. <gui>Bluetooth datņu pārsūtīšanas</gui> dialoglodziņš parādīs progresa joslu. Kad pārsūtīšana ir pabeigta, spiediet <gui>Aizvērt</gui>.</p>
    </item>
  </steps>

</page>
