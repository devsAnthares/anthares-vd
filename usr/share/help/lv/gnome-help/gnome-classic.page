<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="lv">

  <info>
<!--    <link type="guide" xref="shell-overview#desktop" />-->

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ja vēlaties tradicionālāku darbvirsmas vidi, apsveriet pārslēgšanos uz klasisko GNOME.</desc>
  </info>

<title>Kas ir klasiskais GNOME?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>Klasiskais GNOME</em> ir tradicionālāka darbvirsmas vide. Lai arī <em>klasiskais GNOME</em> ir balstīts uz <em>GNOME 3</em> tehnoloģijām, tajā ir vairākas izmaiņas lietotāja saskarnē, piemēram, izvēlnes <gui>Lietotnes</gui> un <gui>Vietas</gui> augšējā joslā, un logu saraksts ekrāna apakšā.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>Klasiskais GNOME</em> ir tradicionālāka darbvirsmas vide. Lai arī <em>klasiskais GNOME</em> ir balstīts uz <em>GNOME 3</em> tehnoloģijām, tajā ir vairākas izmaiņas lietotāja saskarnē, piemēram, izvēlnes <gui xref="shell-introduction#activities">Lietotnes</gui> un <gui>Vietas</gui> augšējā joslā, un logu saraksts ekrāna apakšā.</p>
  </if:when>
</if:choose>

<p>Jūs varat izmantot izvēlni <gui>Lietotnes</gui> augšējā joslā, lai palaistu lietotnes. <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskats ir pieejams, izvēloties <gui>Aktivitāšu pārskatu</gui> vienumu izvēlnē.</p>

<p>Lai piekļūtu <em><gui>Aktivitāšu</gui> pārskatam</em>, varat arī spiest taustiņu <key xref="keyboard-key-super">Super</key>.</p>

<section id="gnome-classic-window-list">
<title>Logu saraksts</title>

<p>Ekrāna apakšā esošais logu saraksts dod pieeju visiem atvērtajiem logiem un lietotnēm, kā arī ļauj tos minimizēt un atjaunot.</p>

<p>Logu saraksta labajā pusē GNOME parāda pašreizējās darbvietas identifikatoru, piemēram, <gui>1</gui> pirmajai (augšējai) darbvietai. Identifikators arī attēlo kopējo pieejamo darbvietu skaitu. Lai pārslēgtos uz citu darbvietu, varat spiest uz identifikatora un izvēlēties vēlamo darbvietu no izvēlnes.</p>

<!-- <p>If an application or a system component wants to get your attention, it
 will display a blue icon at the right-hand side of the window list. Clicking
 the blue icon shows the <link xref="shell-notifications">message tray</link>,
 which lets you access all your notifications.</p> -->

</section>

<section id="gnome-classic-switch">
<title>Pārslēgties uz un no klasiskā GNOME</title>

<note if:test="!platform:gnome-classic" style="important">
<p>Klasiskais GNOME ir pieejams tikai uz sistēmām, kurās ir uzinstalēti noteikti GNOME čaulas paplašinājumi. Dažās Linux distribūcijās šie paplašinājumi varētu nebūt pieejami vai pēc noklusējuma uzinstalēti.</p>
</note>

  <steps>
    <title>Lai pārslēgtos no <em>GNOME</em> uz <em>klasisko GNOME</em>:</title>
    <item>
      <p>Saglabājiet atvērtos darbus un izrakstieties. Spiediet sistēmas lietotāju augšējās joslas labajā pusē, spiediet uz sava vārda un izvēlieties atbilstošo opciju.</p>
    </item>
    <item>
      <p>Sistēma prasīs jums apstiprinājumu. Spiediet <gui>Izrakstīties</gui>, lai apstiprinātu savu izvēli.</p>
    </item>
    <item>
      <p>Ierakstīšanās ekrānā izvēlieties no saraksta savu lietotāja vārdu.</p>
    </item>
    <item>
      <p>Ievadiet savu paroli paroles ievades kastē.</p>
    </item>
    <item>
      <p>Spiediet uz opciju ikonas, kas atrodas pa kreisi no <gui>Ierakstīties</gui> pogas un izvēlieties <gui>Klasiskais GNOME</gui>.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui>Ierakstīties</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Pārslēgties no <em>klasiskā GNOME</em> uz <em>GNOME</em>:</title>
    <item>
      <p>Saglabājiet atvērtos darbus un izrakstieties. Spiediet sistēmas lietotāju augšējās joslas labajā pusē, spiediet uz sava vārda un izvēlieties atbilstošo opciju.</p>
    </item>
    <item>
      <p>Sistēma prasīs jums apstiprinājumu. Spiediet <gui>Izrakstīties</gui>, lai apstiprinātu savu izvēli.</p>
    </item>
    <item>
      <p>Ierakstīšanās ekrānā izvēlieties no saraksta savu lietotāja vārdu.</p>
    </item>
    <item>
      <p>Ievadiet savu paroli paroles ievades kastē.</p>
    </item>
    <item>
      <p>Spiediet uz opciju ikonas, kas atrodas pa kreisi no <gui>Ierakstīties</gui> pogas un izvēlieties <gui>GNOME</gui>.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui>Ierakstīties</gui>.</p>
    </item>
  </steps>
  
</section>

</page>
