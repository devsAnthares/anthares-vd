<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="lv">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definējiet vai mainiet tastatūras saīsnes <gui>Tastatūra</gui> iestatījumos.</desc>
  </info>

  <title>Iestatīt tastatūras saīsnes</title>

<p>Lai izmainītu taustiņu vai taustiņus, kas veido tastatūras saīsni:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Iestatījumi</gui>.</p>
    </item>
    <item>
      <p>Sānu joslā nospiediet <gui>Ierīces</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the row for the desired action. The <gui>Set shortcut</gui>
      window will be shown.</p>
    </item>
    <item>
      <p>Turiet nospiestu vēlamo taustiņu kombināciju vai spiediet <key>Backspace</key>, lai atiestatītu, vai spiediet <key>Esc</key>, lai atceltu.</p>
    </item>
  </steps>


<section id="defined">
<title>Jau definētās saīsnes</title>
  <p>Šeit ir iepriekš konfigurētas saīsnes kas var tikt mainītas, sagrupētas šajās kategorijās:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Palaidēji</title>
  <tr>
	<td><p>Mājas mape</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> vai <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> vai <key>Pārlūks</key></p></td>
  </tr>
  <tr>
	<td><p>Palaist kalkulatoru</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> vai <key>Kalkulators</key></p></td>
  </tr>
  <tr>
	<td><p>Palaist e-pasta klientu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> vai <key>Pasts</key></p></td>
  </tr>
  <tr>
	<td><p>Palaist palīdzības pārlūku</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Palaist tīmekļa pārlūkprogrammu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> vai <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> vai <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Meklēšana</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> vai <key>Meklēšana</key></p></td>
  </tr>
  <tr>
	<td><p>Settings</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigācija</title>
  <tr>
	<td><p>Slēpt visus parastos logus</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pāriet uz darbvietu augšup</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pāriet uz darbvietu lejup</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor down</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the left</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the right</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz darbvietu lejup</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz darbvietu augšup</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key>
  <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window to last workspace</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz 1. darbvietu</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz 2. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz 3. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu uz 4. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt lietotnes</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt sistēmas vadīklas</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt sistēmas vadīklas tieši</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to last workspace</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgties uz 1. darbvietu</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgties uz 2. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārslēgties uz 3. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārslēgties uz 4. darbvietu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
        <td><p>Pārslēgties starp logiem</p></td>
        <td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārslēgties starp logiem tieši</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt lietotnes logu tieši</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt vienas lietotnes logus</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Ekrānattēli</title>
  <tr>
	<td><p>Ievietot loga attēlu starpliktuvē</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ievietot ekrāna daļas attēlu starpliktuvē</p></td>
	<td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ievietot ekrānattēlu starpliktuvē</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Record a short screencast</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of a window to Pictures</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of an area to Pictures</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot to Pictures</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Skaņa un multimediji</title>
  <tr>
	<td><p>Izgrūst</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Izgrūst)</p></td>
  </tr>
  <tr>
	<td><p>Palaist mediju atskaņotāju</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Audio medijs)</p></td>
  </tr>
  <tr>
	<td><p>Nākamais celiņš</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio nākamais)</p></td>
  </tr>  <tr>
	<td><p>Pauzēt atskaņošanu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Audio pauzēšana)</p></td>
  </tr>
  <tr>
	<td><p>Atskaņot (vai atskaņot/pauzēt)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Audio atskaņošana)</p></td>
  </tr>
  <tr>
	<td><p>Iepriekšējais celiņš</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio iepriekšējais)</p></td>
  </tr>
  <tr>
	<td><p>Apturēt atskaņošanu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Audio apturēšana)</p></td>
  </tr>
  <tr>
	<td><p>Samazināt skaļumu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Samazināt audio līmeni)</p></td>
  </tr>
  <tr>
	<td><p>Izslēgt skaņu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Audio klusināšana)</p></td>
  </tr>
  <tr>
	<td><p>Palielināt skaņu</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Palielināt audio līmeni)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sistēma</title>
  <tr>
        <td><p>Fokusēt aktīvo paziņojumu</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Noslēgt ekrānu</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Atteikties</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Atvērt lietotnes izvēlni</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restore the keyboard shortcuts</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Rādīt visas lietotnes</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Parādīt aktivitāšu pārskatu</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the notification list</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the overview</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Rādīt palaišanas komandrindu</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Rakstīšana</title>
  <tr>
  <td><p>Pārslēgties uz nākamo ievades avotu</p></td>
  <td><p><keyseq><key>Super</key><key>Atstarpe</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Pārslēgties uz iepriekšējo ievades avotu</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Universālā piekļuve</title>
  <tr>
	<td><p>Samazināt teksta izmēru</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Ieslēgt vai izslēgt augstu kontrastu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Palielināt teksta izmēru</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Ieslēgt vai izslēgt ekrāna tastatūru</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Ieslēgt vai izslēgt ekrāna lasītāju</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ieslēgt vai izslēgt tuvinājumu</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Tuvināt</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Tālināt</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Windows</title>
  <tr>
	<td><p>Aktivizēt loga izvēlni</p></td>
	<td><p><keyseq><key>Alt</key><key>Atstarpe</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Aizvērt logu</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Paslēpt logu</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pazemināt logu zem citiem logiem</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Maksimizēt logu</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maksimizēt logu horizontāli</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Maksimizēt logu vertikāli</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārvietot logu</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pacelt logu virs citiem logiem</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pacelt logu, ja to aizsedz, citādi pazemināt</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Mainīt loga izmēru</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Atjaunot loga izmēru</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt pilnekrāna režīmu</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt maksimizācijas stāvokli</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Pārslēgt, vai logam jābūt visās darbvietās vai vienā</p></td>
	<td><p>Atslēgts</p></td>
  </tr>
  <tr>
        <td><p>Izvietot kreisajā ekrāna pusē</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Izvietot labajā ekrāna pusē</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Pielāgotās saīsnes</title>

  <p>Izveidojiet savas lietotņu tastatūras saīsnes <gui>Tastatūra</gui> iestatījumos:</p>

  <steps>
    <item>
      <p>Click the <gui style="button">+</gui> button. The <gui>Add Custom
      Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Rakstiet <gui>Nosaukums</gui> laukā, lai identificētu saīsni, un <gui>Komanda</gui> laukā, lai palaistu lietotni. Piemēram, ja jūs vēlaties izveidot saīsni, lai palaistu <app>Rhythmbox</app>, jūs varētu to nosaukt <input>Mūzika</input> un izmantot <input>rhythmbox</input> komandu.</p>
    </item>
    <item>
      <p>Click the row that was just added. When the
      <gui>Set Custom Shortcut</gui> window opens, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Spiediet <gui>Pievienot</gui>.</p>
    </item>
  </steps>

  <p>Komandas nosaukumam ko jūs ievadīsiet vajadzētu būt derīgai sistēmas komandai. Jūs varat to pārbaudīt atverot Termināli un ievadot šo komandu tur. Komanda, kas atver lietotni nevar būt ar tādu pašu nosaukumu kā pati lietotne.</p>

  <p>Ja jūs vēlaties izmainīt komandu, kas saistīta ar pielāgoto tastatūras saīsni, spiediet uz saīsnes <em>nosaukuma</em>. Parādīsies <gui>Iestatīt pielāgotu saīsni</gui> dialoglodziņš, un jūs varēsiet mainīt komandu.</p>

</section>

</page>
