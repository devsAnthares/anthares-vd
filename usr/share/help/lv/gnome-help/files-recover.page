<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="lv">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Parasti dzēstās datnes tiek aizsūtītas uz miskasti, bet tās var atgūt.</desc>
  </info>

  <title>Atjaunot datni no miskastes</title>

  <p>Ja jūs izdzēšat datni, izmantojot datņu pārvaldnieku, datne parasti tiek ievietota <gui>Miskastē</gui>, un tai vajadzētu būt atjaunojamai.</p>

  <steps>
    <title>Lai atjaunotu datnes no miskastes:</title>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <app>Datnes</app>.</p>
    </item>
    <item>
      <p>Spiediet <app>Datnes</app>, lai atvērtu datņu pārvaldnieku.</p>
    </item>
    <item>
      <p>Sānu joslā spiediet <gui>Miskaste</gui>. Ja neredzat sānu joslu, augšējā joslā spiediet <gui>Datnes</gui> un izvēlieties <gui>Sānu josla</gui>.</p>
    </item>
    <item>
      <p>Ja izdzēstā datne atrodas tur, spiediet uz tās un izvēlieties <gui>Atjaunot</gui>. Datne tiks atjaunots mapē, no kuras tika izdzēsta.</p>
    </item>
  </steps>

  <p>Ja izdzēsāt datni, izmantojot <keyseq><key>Shift</key><key>Delete</key></keyseq> vai ar komandrindu, tā ir neatgriezeniski izdzēsta. Neatgriezeniski izdzēstas datnes nevar atjaunot no <gui>Miskastes</gui>.</p>

  <p>Ir vairāki rīki, kas dažreiz var atjaunot neatgriezeniski izdzēstas datnes, tomēr bieži tie nav ērti lietojami. Ja nejauši neatgriezeniski izdzēsāt datni, droši vien labāk būs prasīt padomu palīdzības forumos, lai mēģinātu to atjaunot.</p>

</page>
