<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-send-file" xml:lang="gl">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compartir ficheiros entre dispositivos Bluetooth, como o seu teléfono.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>Enviar ficheiros a un dispositivo Bluetooth</title>

  <p>Pode enviar ficheiros aos dispositivos Bluetooth conectados, tales como algúns teléfonos móbiles ou outros equipos. Algúns tipos de dispositivos non permiten a transferencia de ficheiros, ou tipos de ficheiros específicos. Pode enviar ficheiros usando a xanela de preferencias de Bluetooth.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Bluetooth</gui> no lateral dereito.</p>
    </item>
    <item>
      <p>Asegúrese que o Bluetooth está activado: o trocador da barra de título debería estar <gui>Desactivado</gui>.</p>
    </item>
    <item>
      <p>Na lista de <gui>Dispositivos</gui>, seleccione o dispositivo ao que quere enviar ficheiros. Se o dispositivo desexado non se mostra como <gui>Conectado</gui> na lista, debe <link xref="bluetooth-connect-device">conectarse</link> a él.</p>
      <p>Aparecerá un panel específico para o dispositivo externo.</p>
    </item>
    <item>
      <p>Prema <gui>Enviar ficheiros…</gui> e aparecerá un selector de ficheiros.</p>
    </item>
    <item>
      <p>Prema co botón dereito sobre o nome do ficheiro e seleccione <gui>Mover ao lixo</gui>.</p>
      <p>Para enviar máis dun ficheiro nun cartafol, manteña pulsado <key>Ctrl</key> ao seleccionar cada ficheiro.</p>
    </item>
    <item>
      <p>O propietario do dispositivo receptor normalmente ten que premer un botón para aceptar o ficheiro. Aparecerá un diálogo de <gui>Transferencia de ficheiros por Bluetooth</gui> que mostra unha barra de progreso. Prema <gui>Pechar</gui> cando se complete a transferencia.</p>
    </item>
  </steps>

</page>
