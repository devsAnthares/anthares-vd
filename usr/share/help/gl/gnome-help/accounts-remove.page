<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="gl">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eliminar acceso a un fornecedor de servizo en liña para os seus aplicativos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>Retirar unha conta</title>

  <p>Pode eliminar unha conta en liña que non desexa usar.</p>

  <note style="tip">
    <p>Algúns servizos en liña fornecen un token de autorización que GNOME almacena no lugar do seu contrasinal. Se eliminou unha conta terá que revocar o certificado no servizo en liña. Isto asegura que ningún outro aplicativo ou sitio pode conectarse ao servizo usando a autorización de GNOME.</p>

    <p>Como revocar a autorización dependerá do fornecedor do servizo. Comprobe as súas preferencias no sitio do fornecedor para autorizar ou conectar aplicativos ou sitios. Olle por un aplicativo chamado «GNOME» e elimíneo.</p>
  </note>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Contas en liña</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Contas en liñla</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Seleccione a conta que quere eliminar.</p>
    </item>
    <item>
      <p>Prema o botón <key>-</key> na parte inferior esquerda da xanela.</p>
    </item>
    <item>
      <p>Prema <gui>Quitar</gui> no diálogo de confirmación.</p>
    </item>
  </steps>

  <note style="tip">
    <p>No lugar de retirar a conta completamente, é posíbel <link xref="accounts-disable-service">restrinxir os servizos</link> aos que accede o seu escritorio.</p>
  </note>
</page>
