<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="gl">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>A <gui>pulsación ao pousarse</gui> (pulsación ao posicionar o punteiro) permítelle premer mantendo o rato quieto.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>Simular o clic ao pasar por enriba</title>

  <p>Pode premer ou arrastrar simplemente pousando o punteiro do seu rato sobre un control ou un obxecto na pantalla. É útil se ten dificultades para mover o rato e premer á vez. Este característica chámase <gui>clic ao enfocar</gui> ou pulsación ao pousarse.</p>

  <p>Cando a <gui>Clic ao enfocar</gui> está activada pode mover o punteiro do seu rato sobre un control, deixar o rato e agardar un pouco até que o botón se prema automaticamente.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Abra <gui>Asistencia de clic</gui> na sección <gui>Apuntar e premer</gui>.</p>
    </item>
    <item>
      <p>Troque <gui>Clic ao enfocar</gui> a <gui>I</gui>.</p>
    </item>
  </steps>

  <p>Abrirase a xanela <gui>Tipo de pulsación ao pousarse</gui>, que estará enriba de todas as demais xanelas. Pode usala para elixir o tipo de pulsación que deberá aplicarse cando coloque o rato sobre un botón. Por exemplo, se selecciona <gui>Pulsación secundaria</gui>, o rato simulará unha pulsación co botón dereito cando coloque o punteiro sobre un botón durante uns poucos segundos.</p>

  <p>Ao pasar o punteiro do rato sobre un botón e non moverse, pouco a pouco cambiará de cor. Unha vez totalmente cambiado de cor, o botón premerase.</p>

  <p>Use o deslizador <gui>Retardo</gui> para controlar canto tempo debe manter o rato apuntando até clicar.</p>

  <p>Non ten por que manter o rato perfectamente parado cando coloca o punteiro sobre un botón para premelo. O punteiro pódese mover un pouco e aínda así farase a pulsación pasado un tempo. En cambio se o move demasiado, non se producirá a pulsación.</p>

  <p>Axuste a opción <gui>límite de movemento</gui> para cambiar canto pode moverse o punteiro e aínda así consideralo como pousado.</p>

</page>
