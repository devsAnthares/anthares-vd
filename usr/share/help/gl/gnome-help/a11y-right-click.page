<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="gl">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Para realizar unha pulsación dereita, prema e manteña o botón esquerdo do rato.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>Simular o clic dereito do rato</title>

  <p>Pode facer que, no lugar de premer o botón dereito do rato, baste con manter pulsado o botón esquerdo do rato durante un tempo para realizar a mesma acción. Isto resulta útil se ten dificultades para mover os dedos dunha man de forma individual, ou se o seu dispositivo apuntador ten un só botón.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Abra <gui>Asistencia de clic</gui> na sección <gui>Apuntar e premer</gui>.</p>
    </item>
    <item>
      <p>Troque <gui>Clic secundario simulado</gui> a <gui>I</gui>.</p>
    </item>
  </steps>

  <p>Pode cambiar a duración da pulsación do botón esquerdo do rato antes de que se considere unha pulsación co botón dereito cambiando o <gui>atraso de aceptación</gui>.</p>

  <p>Para premer co botón dereito do rato usando unha unha pulsación secundaria simulada, manteña premido o botón esquerdo do rato onde normalmente premería co botón dereito, despois sólteo. O punteiro encherase con unha cor diferente mentres mantén premido o botón esquerdo. Cando cambie a súa cor por completo, solte o botón para facer a pulsación dereita.</p>

  <p>Algúns punteiros especiais, como os punteiros de redimensionado, non cambian a súa cor. Pode usar o clic secundario simulado como normalmente o fai, incluso se non ten un retorno visual do punteiro.</p>

  <p>Se usa as <link xref="mouse-mousekeys">Teclas do rato</link>, isto tamén lle permitirá facer clic dereito premendo e mantendo a tecla <key>5</key> no seu teclado numérico.</p>

  <note>
    <p>Na vista xeral de <gui>Actividades</gui> sempre pode realizar unha pulsación longa para executar unha pulsación co botón dereito, incluso se esta característica está desactivada. A pulsación longa funciona de forma lixeiramente diferente na vista xeral: non ten que liberar o botón para realizar a pulsación co botón para realizar a pulsación co botón dereito.</p>
  </note>

</page>
