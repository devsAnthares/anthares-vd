<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="gl">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision version="0.1" date="2013-02-23" status="review"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Explica o significado das iconas da parte dereita da barra superior.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>A icona de Bluetooth na barra superior</title>
<p>Esta sección explica o significado das iconas que se atopan na esquina superior dereita da pantalla. Máis especialmente, explícanse todas as variacións posíbeis destas iconas fornecidas pola inteface de GNOME.</p>

<if:choose>
<if:when test="!platform:gnome-classic">
<media type="image" src="figures/top-bar-icons.png" style="floatend">
  <p>Barra superior de GNOME Shell</p>
</media>
</if:when>
<if:when test="platform:gnome-classic">
<media type="image" src="figures/top-bar-icons-classic.png" width="395" height="70" style="floatend">
  <p>Barra superior de GNOME Shell</p>
</media>
</if:when>
</if:choose>

<links type="section"/>

<section id="universalicons">
<title>Acceso universal</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/preferences-desktop-accessibility-symbolic.svg"/></td>
      <td><p>Leva a un menú que se activa as preferencias de accesibilidade.</p></td>
    </tr>
    
  </table>
</section>


<section id="audioicons">
<title>Iconas de control de volume</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-high-symbolic.svg"/></td>
      <td><p>O volume está moi alto.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-medium-symbolic.svg"/></td>
      <td><p>O volume está pola metade.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-low-symbolic.svg"/></td>
      <td><p>O nome do ficheiro é demasiado largo</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-muted-symbolic.svg"/></td>
      <td><p>O volume está enmudecido.</p></td>
    </tr>
  </table>
</section>


<section id="bluetoothicons">
<title>Problemas de Bluetooth</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/></td>
      <td><p>O Bluetooth foi desactivado.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-disabled-symbolic.svg"/></td>
      <td><p>O Bluetooth desactivouse.</p></td>
    </tr>
  </table>
</section>

<section id="networkicons">
<title>Iconas do xestor de rede</title>
<p/>
<p><app>Conexión móbil</app></p>
 <table shade="rows">

    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-3g-symbolic.svg"/></td>
      <td><p>Conectado a unha rede 3G.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-4g-symbolic.svg"/></td>
      <td><p>Conectado a unha rede 4G.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-edge-symbolic.svg"/></td>
      <td><p>Conectar cunha rede EDGE.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-gprs-symbolic.svg"/></td>
      <td><p>Conectar cunha rede GPRS.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-umts-symbolic.svg"/></td>
      <td><p>Conectar cunha rede UMTS.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-connected-symbolic.svg"/></td>
      <td><p>Conectado a unha rede móbil.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-acquiring-symbolic.svg"/></td>
      <td><p>Obtendo unha conexión de rede móbil.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg"/></td>
      <td><p>Intensidade do sinal moi alta.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-good-symbolic.svg"/></td>
      <td><p>Intensidade do sinal alta.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-ok-symbolic.svg"/></td>
      <td><p>Intensidade do sinal media.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-weak-symbolic.svg"/></td>
      <td><p>Intensidade do sinal baixa.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-none-symbolic.svg"/></td>
      <td><p>Intensidade do sinal extremadamente baixa.</p></td>
    </tr></table>




<p><app>Conexións de rede de área local (LAN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-error-symbolic.svg"/></td>
      <td><p>Produciuse un erro ao atopar a rede.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-idle-symbolic.svg"/></td>
      <td><p>A rede está desactivada.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-no-route-symbolic.svg"/></td>
      <td><p>Non se atopou unha ruta para a rede.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-offline-symbolic.svg"/></td>
      <td><p>A rede está desconectada.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-receive-symbolic.svg"/></td>
      <td><p>A rede está recibindo datos.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-receive-symbolic.svg"/></td>
      <td><p>A rede está transmitindo e recibindo datos.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-symbolic.svg"/></td>
      <td><p>A rede está transmitindo datos.</p></td>
    </tr>
</table>



<p><app>Conexión a unha rede virtual privada (VPN)</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-acquiring-symbolic.svg"/></td>
      <td><p>Obtendo unha conexión de rede.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-symbolic.svg"/></td>
      <td><p>Conectado a unha rede VPN.</p></td>
    </tr>
</table>


<p><app>Conexión cableada</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-acquiring-symbolic.svg"/></td>
      <td><p>Obtendo unha conexión de rede.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-disconnected-symbolic.svg"/></td>
      <td><p>Desconectado da rede.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-symbolic.svg"/></td>
      <td><p>Conectado a unha rede con fíos.</p></td>
    </tr>
</table>


<p><app>Editar a conexión sen fíos</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-acquiring-symbolic.svg"/></td>
      <td><p>Obtendo unha rede sen fíos.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-encrypted-symbolic.svg"/></td>
      <td><p>A rede sen fíos está cifrada.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-connected-symbolic.svg"/></td>
      <td><p>Conectado a unha rede sen fíos.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg"/></td>
      <td><p>Intensidade do sinal moi alta.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-good-symbolic.svg"/></td>
      <td><p>Intensidade do sinal alta.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-ok-symbolic.svg"/></td>
      <td><p>Intensidade do sinal media.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-weak-symbolic.svg"/></td>
      <td><p>Intensidade do sinal baixa.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-none-symbolic.svg"/></td>
      <td><p>Intensidade do sinal moi baixa.</p></td>
    </tr>

  </table>
</section>

<section id="batteryicons">
<title>Xestionar contas de usuario</title>

 <table shade="rows">
   <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-symbolic.svg"/></td>
      <td><p>A batería está completamente cargada.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-symbolic.svg"/></td>
      <td><p>A batería está parcialmente baleira.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-symbolic.svg"/></td>
      <td><p>A batería está baixa.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-symbolic.svg"/></td>
      <td><p>Aviso: A batería está moi baixa.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-symbolic.svg"/></td>
      <td><p>A batería está extremadamente baixa.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-missing-symbolic.svg"/></td>
      <td><p>A batería está desconectada.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charged-symbolic.svg"/></td>
      <td><p>A batería está completamente cargada.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charging-symbolic.svg"/></td>
      <td><p>A batería está completa e cargándose.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-charging-symbolic.svg"/></td>
      <td><p>A batería está parcialmente chea e cargándose.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-charging-symbolic.svg"/></td>
      <td><p>A batería está baixa e cargándose.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-charging-symbolic.svg"/></td>
      <td><p>A batería está moi baixa e cargándose.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-charging-symbolic.svg"/></td>
      <td><p>A batería está baleira e cargándose.</p></td>
    </tr>
  </table>
</section>


</page>
