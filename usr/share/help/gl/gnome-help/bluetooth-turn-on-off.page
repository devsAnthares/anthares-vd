<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="gl">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar ou desactivar o «touchpad».</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

<title>Apagar ou acender o Bluetooth</title>

  <p>Pode activar o seu Bluetooth para conectarse a outros dispositivos Bluetooth, ou desactivalo para aforrar enerxía. Para activar Bluetoot:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Bluetooth</gui> no lateral dereito.</p>
    </item>
    <item>
      <p>Camie o trocador na parte superior a <gui>I</gui>.</p>
    </item>
  </steps>

  <p>Moitos portátiles teñen un interruptor físico ou unha combinación de teclas para acender ou apagar o Bluetooth. Busque por un interruptor no seu computador ou unha tecla no seu teclado. A tecla no teclado normalmente é accesíbel coa axuda dunha tecla <key>Fn</key>.</p>

  <p>Para desactivar o Bluetooth:</p>
  <steps>
    <item>
      <p>Abrir o <gui xref="shell-introduction#yourname">menú do sistema</gui> na parte dereita da barra superior.</p>
    </item>
    <item>
      <p>Seleccione <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Non en uso</gui>. A sección Bluetooth do menú expandirase.</p>
    </item>
    <item>
      <p>Seleccione <gui>Desactivar</gui>.</p>
    </item>
  </steps>

  <note><p>O seu computador é <link xref="bluetooth-visibility">visíbel</link> mentres o panel de <gui>Bluetooth</gui> está aberto.</p></note>

</page>
