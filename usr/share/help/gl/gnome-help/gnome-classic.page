<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="gl">

  <info>
<!--    <link type="guide" xref="shell-overview#desktop" />-->

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Considere cambiar a GNOME Clásico se prefire unha experiencia de escritorio máis tradicional.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

<title>Que é GNOME Clásico?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on <em>GNOME 3</em>
  technologies, it provides a number of changes to the user interface, such as
  the <gui>Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on <em>GNOME 3</em>
  technologies, it provides a number of changes to the user interface, such as
  the <gui xref="shell-introduction#activities">Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
</if:choose>

<p>You can use the <gui>Applications</gui> menu on the top bar to launch
 applications. The <gui xref="shell-introduction#activities">Activities</gui>
 overview is available by selecting the <gui>Activities
 Overview</gui> item from the menu.</p>

<p>To access the <em><gui>Activities</gui> overview</em>,
 you can also press the <key xref="keyboard-key-super">Super</key> key.</p>

<section id="gnome-classic-window-list">
<title>Lista de xanelas</title>

<p>A lista de xanelas na parte inferior da pantalla fornece acceso a todas as xanelas e aplicativos abertos e permítelle minimizar e restauralos de forma rápida.</p>

<p>At the right-hand side of the window list, GNOME displays a short
 identifier for the current workspace, such as <gui>1</gui> for the first
 (top) workspace. In addition, the identifier also displays the total number
 of available workspaces. To switch to a different workspace, you can click
 the identifier and select the workspace you want to use from the menu.</p>

<!-- <p>If an application or a system component wants to get your attention, it
 will display a blue icon at the right-hand side of the window list. Clicking
 the blue icon shows the <link xref="shell-notifications">message tray</link>,
 which lets you access all your notifications.</p> -->

</section>

<section id="gnome-classic-switch">
<title>Cambiar a ou desde GNOME Clásico</title>

<note if:test="!platform:gnome-classic" style="important">
<p>GNOME Clásico só está dispoñíbel en sistemas que teñen instaladas certas extensións de GNOME Shell. Algunhas distribucións Linux poderían non telas dispoñíbeis ou instaladas por omisión.</p>
</note>

  <steps>
    <title>Para cambiar de <em>GNOME</em> a <em>GNOME Clásico</em>:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar, click your name and then choose the right option.</p>
    </item>
    <item>
      <p>A confirmation message will appear. Select <gui>Log Out</gui> to
      confirm.</p>
    </item>
    <item>
      <p>Na pantalla de inicio de sesión, seleccione o seu nome de usuario da lista.</p>
    </item>
    <item>
      <p>Escriba o seu contrasinal na caixa de entrada de contrasinal.</p>
    </item>
    <item>
      <p>Click the options icon, which is displayed to the left of the <gui>Sign
      In</gui> button, and select <gui>GNOME Classic</gui>.</p>
    </item>
    <item>
      <p>Prema sobre o botón <gui>Iniciar sesión</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Para cambiar de <em>GNOME Cláisco</em> a <em>GNOME</em>:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar, click your name and then choose the right option.</p>
    </item>
    <item>
      <p>A confirmation message will appear. Select <gui>Log Out</gui> to
      confirm.</p>
    </item>
    <item>
      <p>Na pantalla de inicio de sesión, seleccione o seu nome de usuario da lista.</p>
    </item>
    <item>
      <p>Escriba o seu contrasinal na caixa de entrada de contrasinal.</p>
    </item>
    <item>
      <p>Click the options icon, which is displayed to the left of the <gui>Sign
      In</gui> button, and select <gui>GNOME</gui>.</p>
    </item>
    <item>
      <p>Prema sobre o botón <gui>Iniciar sesión</gui>.</p>
    </item>
  </steps>
  
</section>

</page>
