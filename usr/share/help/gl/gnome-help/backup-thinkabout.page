<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="gl">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Unha lista de cartafoles onde pode atopar documentos, ficheiros e configuracións que podería querer respaldar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2018</mal:years>
    </mal:credit>
  </info>

  <title>Onde podo atopar os ficheiros aos que quero facerlles copia de seguranza?</title>

  <p>Decidir que ficheiros respaldar e sabe onde están é o paso máis dificil para tentar levar a cabo unha copia de respaldo. A continuación móstranse as localizacións máis comúns de ficheiros e axustes importantes que pode querer respaldar.</p>

<list>
 <item>
  <p>Ficheiros persoais (documentos, música, fotos e vídeos)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs and need to be translated.  You can find the correct translations for your language here: http://translationproject.org/domain/xdg-user-dirs.html">Xeralmente están almacenados no seu cartafol (<file>/home/oseu_nome</file>). Poden estar en subcartafoles como Escritorio, Documentos, Imaxes, Música e Vídeos.</p>
  <p>Se o medio que usa para os seus respaldos ten espazo dabondo (se é un disco duro externo, por exemplo), considere respaldar completamente o seu cartafol persoal. pode averiguar canto espazo de disco duro usa o seu cartafol persoal empregando o <app>Analizador de uso de disco</app>.</p>
 </item>

 <item>
  <p>Ficheiros ocultos</p>
  <p>De forma predeterminada ocúltase calquera cartafol ou ficheiro que comece por punto (.). Para ver ficheiros ocultos, prema o botón <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">Opcións de visualización</span></media></gui> ou prema <keyseq><key>Ctrl</key><key>H</key></keyseq>. Pode copialos a unha localización de respaldo como calquera outro tipo de ficheiro.</p>
 </item>

 <item>
  <p>Preferencias persoais (preferencias de escritorio, temas e preferencias dos aplicativos)</p>
  <p>A maioría dos aplicativos almacenan as súas preferencias en cartafoles ocultos dentro do seu cartafol persoal (vexa máis arriba para obter máis información sobre os ficheiros ocultos).</p>
  <p>A maioría das preferencias dos seus aplicativos almacénanse en cartafoles ocultos <cmd>.config</cmd> e <cmd>.local</cmd> no seu cartafol persoal.</p>
 </item>

 <item>
  <p>Opcións do sistema</p>
  <p>As preferencias para as partes importantes do seu sistema non se almacenan no seu cartafol persoal. Hai certos lugares nos cales poden almacenarse, porén a maioría deles están no cartafol <file>/etc</file>. En xeral, non precisa facer unha copia de respaldo destes ficheiros no seu computador persoal. Porén, se se trata dun servidor, debería crear unha copia de respaldo dos ficheiros dos servizos que estean en execución.</p>
 </item>
</list>

</page>
