<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="dconf-profiles" xml:lang="gl">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-custom-defaults"/>
    <link type="seealso" xref="dconf"/>
    <revision version="0.1" date="2013-03-25" status="draft"/>
  
    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    
    <credit type="editor">
      <name>Jana Švárová</name>
      <email>jana.svarova@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>This page gives detailed information about profile selection.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  </info>

  <title>Seleccione un perfíl</title>
  
  <p>On startup, <sys>dconf</sys> consults the DCONF_PROFILE environment 
  variable. If set, <sys>dconf</sys> attempts to open the named profile and 
  aborts if that fails. If the environment variable is not set, 
  <sys>dconf</sys> attempts to open the profile named “user”. If that
  fails, it will fall back to an internal hard-wired configuration.</p>

  <p>Each line in a profile specifies one <sys>dconf</sys> database. The first 
  line indicates the database used to write changes, and the remaining lines 
  indicate read-only databases. Here is an example:</p>
<code>
user-db:user
system-db:local
system-db:site
</code>

<note style="important">
  <p>The <sys>dconf</sys> profile for a session is determined at login, so users
  will have to log out and log in to apply a new <sys>dconf</sys> user profile
  to their session.</p>
</note>

</page>
