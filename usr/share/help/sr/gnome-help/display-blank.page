<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="sr">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <credit type="author editor">
      <name>Петр Ковар</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Измените време затамњивања екрана да уштедите енергију.</desc>
  </info>

  <title>Подесите време затамњивања екрана</title>

  <p>Да уштедите напајање, можете да подесите време пре затамњивања екрана када је у мировању. Можете такође у потпуности да искључите затамњивање.</p>

  <steps>
    <title>Да подесите време затамњивања екрана:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Користите падајући списак <gui>Затамни екран</gui> у <gui>Уштеда ел. енергије</gui> да подесите време затамњивања екрана, или да га у потпуности искључите.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Када оставите рачунар да мирује, екран ће се сам закључати из безбедносних разлога. Да измените ово понашање, погледајте <link xref="session-screenlocks"/>.</p>
  </note>

</page>
