<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="sr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Обрисане датотеке бивају послате у смеће, али могу бити повраћене.</desc>
  </info>

  <title>Вратите датотеку из смећа</title>

  <p>Ако обришете датотеку управником датотека, она бива смештена у <gui>Смеће</gui>, и може бити могуће њено враћање.</p>

  <steps>
    <title>Да вратите датотеку из смећа:</title>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Датотеке</gui>.</p>
    </item>
    <item>
      <p>Притисните на <app>Датотеке</app> да отворите управника датотека.</p>
    </item>
    <item>
      <p>Притисните <gui>Смеће</gui> у бочној површи. Ако не видите бочну површ, притисните на <gui>Датотеке</gui> у горњој траци и изаберите <gui>Бочна површ</gui>.</p>
    </item>
    <item>
      <p>Ако је ваша обрисана датотека тамо, кликните на њу и изаберите <gui>Врати</gui>. Након овога датотека ће бити враћена у фасциклу из које је обрисана.</p>
    </item>
  </steps>

  <p>Ако обришете датотеку користећи <keyseq><key>Шифт</key><key>Обриши </key></keyseq>, или из линије наредби, датотека бива трајно обрисана. Датотеке које су трајно обрисане не могу бити враћене из <gui>Смећа</gui>.</p>

  <p>Постоји велики број доступних алата за опоравак који су понекад у стању да поврате датотека које су трајно обрисане. Међутим, ови алати уопште нису нимало лаки за коришћење. Ако сте случајно трајно обрисали датотеку, вероватно је најбоље да питате за савет на форуму да видите да ли је можете повратити.</p>

</page>
