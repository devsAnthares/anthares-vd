<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="sr">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Мапирајте Ваком таблицу према посебном монитору.</desc>
  </info>

  <title>Изаберите монитор</title>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click <gui>Devices</gui> in the sidebar.</p>
  </item>
  <item>
    <p>Click <gui>Wacom Tablet</gui> in the sidebar to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Ако таблица није откривена, од вас ће бити затражено да <gui>прикључите или упалите вашу Ваком таблицу</gui>. Кликните на везу <gui>Подешавања блутута</gui> да повежете бежичну таблицу.</p></note>
  </item>
  <item><p>Кликните <gui>Мапирај монитор…</gui></p></item>
  <item><p>Изаберите <gui>Мапирај на једном монитору</gui>.</p></item>
  <item><p>Поред <gui>Излаза</gui>, изаберите монитор за који желите да прима улаз са ваше графичке таблице.</p>
     <note style="tip"><p>Само монитори који су подешени моћи ће да се бирају.</p></note>
  </item>
  <item>
    <p>Switch <gui>Keep aspect ratio (letterbox)</gui> to <gui>ON</gui> to
    match the drawing area of the tablet to the proportions of the monitor.
      This setting, also called <em>force proportions</em>,
      “letterboxes” the drawing area on a tablet to correspond more
      directly to a display. For example, a 4∶3 tablet would be mapped so that
      the drawing area would correspond to a widescreen display.</p>
  </item>
  <item><p>Кликните <gui>Затвори</gui>.</p></item>
</steps>

</page>
