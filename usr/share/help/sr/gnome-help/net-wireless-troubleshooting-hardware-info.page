<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="sr">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Приложници викија Убунтуове документације</name>
    </credit>
    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Требаће вам детаљи као што је број модела вашег бежичног прилагођивача у наредним корацима решавања проблема.</desc>
  </info>

  <title>Решавање проблема бежичне мреже</title>
  <subtitle>Прикупите податке о вашим мрежним компонентама</subtitle>

  <p>У овом кораку, прикупите податке о вашем бежичном мрежном уређају. Начин решавања многих бежичних проблема зависи од марке и броја модела бежичног прилагођивача, тако да ћете морати да прибележите те детаље. Такође може бити од помоћи да имате неке од ствари које иду уз ваш рачунар, као што су дискови за инсталацију управљачких програма диска. Потражите следеће ствари, ако их још увек имате:</p>

  <list>
    <item>
      <p>Паковање и упутства за ваше бежичне уређаје (нарочито корисничко упутство вашег усмеривача)</p>
    </item>
    <item>
      <p>Диск који садржи управљачке програме за ваш бежични прилагођивач (чак и ако садржи само Виндоузове управљачке програме)</p>
    </item>
    <item>
      <p>Бројеве произвођача и модела вашег рачунара, бежичног прилагођивача и усмеривача. Овај податак се обично налази на доњој страни или на позадини уређаја.</p>
    </item>
    <item>
      <p>Било који број издања или ревизије који може бити одштампан на вашим бежичним мрежним уређајима или њиховим паковањима. Ово нарочито може бити корисно, зато тражите пажљиво.</p>
    </item>
    <item>
      <p>Било шта на диску управљачког програма што одређује сам уређај, издање његовог „угњежденог програма“, или компоненте (чипсет) које користи.</p>
    </item>
  </list>

  <p>Ако је могуће, покушајте да омогућите приступ некој другој радној интернет вези како бисте могли да преузмете софтвер и управљачке програме ако је потребно. (Прикључивање вашег рачунара директно у усмеривач помоћу мрежног кабла је један начин постизања овога, али га прикључите само када морате.)</p>

  <p>Када будете имали што је више могуће ових ствари, кликните <gui>Следеће</gui>.</p>

</page>
