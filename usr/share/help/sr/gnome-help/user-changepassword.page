<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="sr">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Држите ваш налог безбедним тако што ћете често мењати вашу лозинку у вашим подешавањима налога.</desc>
  </info>

  <title>Промените вашу лозинку</title>

  <p>Добра замисао је да промените вашу лозинку с времена на време, нарочито ако мислите да неко други зна вашу лозинку.</p>

  <p>Потребна су вам <link xref="user-admin-explain">Администраторска права</link> за уређивање корисничких налога који нису ваши.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Корисници</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Корисници</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Кликните на натпис <gui>·····</gui> поред <gui>лозинке</gui>. Ако мењате лозинку другог корисника, морате као прво да <gui>откључате</gui> панел.</p>
    </item>
    <item>
      <p>У одговарајућа поља прво унесите вашу тренутну лозинку а затим нову. Унесите још једном вашу нову лозинку у поље <gui>Потврди нову лозинку</gui>.</p>
      <p>Можете да притиснете иконицу за <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">створите лозинку</span></media></gui> да сами створите насумичну лозинку.</p>
    </item>
    <item>
      <p>Кликните <gui>Промени</gui>.</p>
    </item>
  </steps>

  <p>Уверите се да сте <link xref="user-goodpassword">изабрали добру лозинку</link>. Ово ће помоћи у очувању безбедности вашег корисничког налога.</p>

  <note>
    <p>Када освежите вашу лозинку пријављивања, ваша лозинка привеска пријављивања ће самостално бити освежена да би била иста као ваша нова лозинка пријављивања.</p>
  </note>

  <p>Ако заборавите вашу лозинку, било који корисник са администраторским овлашћењиам може да вам је измени.</p>

</page>
