<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="net-proxy" xml:lang="sr">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Батиста Миј-Матиас</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Посредник веб саобраћаја може бити коришћен за тајно приступање веб услугама, због управљања или безбедносних разлога.</desc>
  </info>

  <title>Одредите подешавања посредника</title>

<section id="what">
  <title>Шта је то посредник?</title>

  <p><em>Веб посредник</em> издваја веб сајтове које разгледате, прима захтеве од вашег веб прегледника да би довукао веб странице и њихове елементе, и пратећи политику одлучује да ли ће вам их проследити. Обично се користе у пословним и на јавним бежичним врућим тачкама да би одлучили које веб сајтове можете да прегледате, спречавајући вас да приступите интернету без пријављивања, или да би обавили безбедносне провере на веб сајтовима.</p>

</section>

<section id="change">
  <title>Измените начин посредовања</title>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Мрежа</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Мрежа</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Изаберите <gui>Мрежни посредник</gui> са списка на левој страни.</p>
    </item>
    <item>
      <p>Изаберите који начин посредовања желите да користите:</p>
      <terms>
        <item>
          <title><gui itst:context="proxy">Ништа</gui></title>
          <p>Програми ће користити непосредну везу да довуку садржај са веба.</p>
        </item>
        <item>
          <title><gui>Ручно</gui></title>
          <p>За сваки протокол посредовања, одредите адресу посредника и порт за протоколе. Протоколи су <gui>ХТТП</gui>, <gui>ХТТПС</gui>, <gui>ФТП</gui> и <gui>СОЦКС</gui>.</p>
        </item>
        <item>
          <title><gui>Самостално</gui></title>
          <p>Усмеравање адресе до изворишта, која садржи одговарајуће подешавање за ваш систем.</p>
        </item>
      </terms>
    </item>
  </steps>

  <p>Програми који користе мрежну везу ће користити ваша подешавања посредника.</p>

</section>

</page>
