<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="sr">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Савети за смањење потрошње напајања вашег рачунара.</desc>
  </info>

  <title>Користите мање напајања и побољшајте радни век батерије</title>

  <p>Рачунари могу да користе много енергије. Али коришћењем неких једноставних стратегија за очување енергије, можете да смањите ваш рачун ел. енергије и да помогнете окружењу.</p>

<section id="general">
  <title>Општи савети</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Обуставите ваш рачунар</link> када га не користите. Ово значајно умањује износ напајања које користи, а може бити пробуђен врло брзо.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Угасите</link> рачунар када га нећете користити дуже времена. Неки људи се плаше да често гашење рачунара може да га истроши брже, али то није тако.</p>
  </item>
  <item>
    <p>Користите панел <gui>напајања</gui> у <app>Подешавањима</app> да измените ваша подешавања напајања. Постоји велики број опција које ће вам помоћи да сачувате енергију: можете да подесите да се <link xref="display-blank">екран сам затамни</link> након одређеног времена, да умањите <link xref="display-brightness">осветљеност екрана</link>, и да рачунар <link xref="power-suspend">сам обустави рад</link> ако га не користите извесни временски период.</p>
  </item>
  <item>
    <p>Угасите све спољне уређаје (као што су штампачи и скенери) када их не користите.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Преносни рачунари, нетбукови, и остали уређаји са батеријама</title>

 <list>
   <item>
     <p>Смањите <link xref="display-brightness">осветљеност екрана</link>. Напајање екрана чини значајан део потрошње енергије преносног рачунара.</p>
     <p>Већина преносних рачунара има дугмиће на тастатури (или пречице тастатуре) које можете да користите да смањите осветљеност.</p>
   </item>
   <item>
     <p>Ако вам интернет веза није потребна неко време, искључите бежичну или блутут картицу. Ови уређаји раде тако што емитују радио таласе, који троше поприлично енергије.</p>
     <p>Неки рачунари имају физички прекидач који може бити коришћен за њихово гашење, док други имају пречице тастатуре које можете да користите уместо тога. Можете да га укључите поново када вам затреба.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Напреднији савети</title>

 <list>
   <item>
     <p>Смањите број задатака који се одвијају у позадини. Рачунари користе више енергије када имају више послова да одраде.</p>
     <p>Већина ваших радних програма ради веома мало када их не користите активно. Међутим, програми који учестано добављају податке са интернета или пуштају музику или филмове могу да утичу на потрошњу енергије.</p>
   </item>
 </list>

</section>

</page>
