<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-create" xml:lang="ko">

  <info>
    <link type="guide" xref="index#managing-archives"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email its:translate="no">majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>새 압축 파일에 파일 또는 폴더를 추가합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016.</mal:years>
    </mal:credit>
  </info>

  <title>새 압축 파일 만들기</title>

  <p>다음 과정을 따라 <app>압축 관리자</app>에서 새 압축 파일을 만드십시오:</p>

  <steps>
    <item>
      <p><guiseq><gui style="menu">압축 관리자</gui><gui style="menuitem">새 압축 파일</gui></guiseq>을 선택하십시오.</p>
    </item>
    <item>
      <p>새 압축 파일의 이름을 짓고 저장할 위치를 선택하신 후, <gui>만들기</gui>를 눌러 계속하십시오.</p>
      <note style="tip">
	<p><gui>기타 옵션</gui>을 눌러 암호를 설정하거나 새 압축 파일 분할 관련 옵션을 설정하여 더 작은 MB 단위크기의 개별 볼륨 파일로 쪼갤 수 있습니다.</p>
      </note>
    </item>
    <item>
      <p>도구 모음 단추 중 <gui>+</gui>를 눌러 원하는 파일 및 폴더를 추가하십시오. 추가하려는 파일 및 폴더 옆에 있는 상자를 표시하십시오.</p>
      <note style="tip">
	<p>모든 압축 파일 형식이 폴더를 지원하는건 아닙니다. 사용 중인 파일 형식에서 폴더를 지원하지 않는다면 경고가 뜹니다. 사용 중인 파일 형식에서 폴더를 지원하지 않는다면 폴더에 있는 파일을 추가하지만 폴더는 추가하지 않습니다.</p>
      </note>
    </item>
    <item>
      <p>파일 추가가 끝나면, 압축 파일 준비가 끝납니다. 따로 저장할 필요는 없습니다.</p>
    </item>
  </steps>

</page>
