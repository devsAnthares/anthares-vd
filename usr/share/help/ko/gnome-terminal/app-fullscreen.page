<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="app-fullscreen" xml:lang="ko">

  <info>
    <revision version="0.1" date="2013-02-17" status="candidate"/>
    <link type="guide" xref="index#appearance"/>
    <link type="seealso" xref="pref-menubar"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>전체 화면 모드를 활성화합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2018.</mal:years>
    </mal:credit>
  </info>

  <title>전체 화면 세션</title>

  <p>전체 화면 모드를 설정하여 <app>터미널</app>로 화면 전체를 활용할 수 있습니다.</p>

  <p>이 모드는 <app>터미널</app>을 오랫동안 다루면서 좁은 공간을 지닌 장치를 사용하려 하거나, 긴 터미널 출력 내용을 다루려 할 경우 요긴하게 쓸만합니다. 임시로 화면에 더 많은 <link xref="pref-scrolling">스크롤 행</link>을 보여주는 창 테두리를 숨깁니다.</p>

  <p>전체 화면 모드를 활성화하려면:</p>

  <steps>
    <item>
      <p><gui style="menu">보기</gui>를 선택하고 <gui style="menuitem">전체 화면</gui>을 선택하거나 <key>F11</key> 키를 누르십시오.</p>
    </item>
  </steps>

  <note style="important">
    <p>메뉴 모음을 숨기기 전에는 전체 화면 모드에서도 나타납니다.</p>
  </note>

  <note style="tip">
    <p><app>터미널</app> 탭을 하나 이상 열어두었다면, 탭 표시줄이 전체 화면 모드에서도 나타납니다.</p>
  </note>

  <p>전체 화면 모드를 비활성화하려면:</p>

  <steps>
    <item>
      <p><gui style="menu">보기</gui>를 선택하고 <gui style="menuitem">전체 화면</gui> 표시를 끄거나 터미널 화면에 마우스 커서를 놓고 오른쪽 단추를 눌러 <gui style="menuitem">전체 화면 나가기</gui>를 선택하십시오. 아니면, <key>F11</key> 키를 누르십시오.</p>
    </item>
  </steps>

</page>
