<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-shield" xml:lang="ko">

  <info>
    <link type="guide" xref="appearance"/>

    <revision pkgversion="3.22" date="2017-01-09" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email its:translate="no">matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email its:translate="no">jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email its:translate="no">pknbe@volny.cz</email>
      <years>2017</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>잠금 화면 보호기 배경을 변경하세요.</desc>
  </info>

  <title>잠금 화면 보호기 변경</title>

  <p><em>잠금 화면 보호기</em>는 시스템이 잠겼을 때 빠르게 아래로 내려가는 화면입니다. 잠금 화면 보호기의 배경은 <sys>org.gnome.desktop.screensaver.picture-uri</sys> GSettings 키에 의해 통제됩니다. <sys>GDM</sys>가 자체적인 <sys>dconf</sys> 프로필을 사용한 이후로, 당신은 프로필의 설정을 변경함으로써 기본 배경을 설정할 수 있습니다.</p>

<steps>
  <title>키 org.gnome.desktop.screensaver.picture-uri 설정</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>컴퓨터 전반적인 설정을 위해 <file>/etc/dconf/db/gdm.d/<var>01-screensaver</var></file> <sys>gdm</sys> 데이터베이스를 생성하세요:</p>
  <code>[org/gnome/desktop/screensaver]
picture-uri='file://<var>/opt/corp/background.jpg</var>'</code>
  <p><var>/opt/corp/background.jpg</var>를 당신이 잠금 화면 배경을 하기를 원하는 이미지 파일 경로로 대체하세요.</p>
  <p>지원되는 형식은 PNG, JPG, JPEG, 그리고 TGA입니다. 사진은 화면에 맞출 필요가 있다면 비율에 따라 맞추어 질 것입니다.</p>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item><p>시스템 전체 설정을 적용하기 전에 로그아웃했다가 다시 로그인해야 합니다.</p>
  </item>
</steps>

<p>화면을 잠그면, 새로운 잠금 화면 보호기가 배경에 보일 것입니다. 시간, 날짜 그리고 금주가 표시 될 것입니다.</p>

<section id="troubleshooting-background">
  <title>배경이 업데이트 되지 않는다면 어떻게 해야합니까?</title>

  <p>시스템 데이터베이스 업데이트를 총괄하는 <cmd its:translate="no">dconf update</cmd>가 작동하고 있는지 확인하세요.</p>

  <p>배경이 업데이트되지 않는 경우, <sys>GDM</sys>를 재시작하세요.</p>
</section>

</page>
