<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-favorite-applications" xml:lang="ko">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.8" date="2013-04-28" status="draft"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>활동 개요에서 기본 즐겨찾기를 사용자 정의하세요.</desc>

  </info>

  <title>기본 즐겨 찾는 애플리케이션</title>

  <p>즐겨 찾는 애플리케이션은 <link href="help:gnome-help/shell-terminology">dash</link>에서 볼 수 있습니다. 당신은 한 사용자에 대한 즐겨 찾는 애플리케이션을 설정하거나 모든 사용자에 대해 동일한 즐겨 찾기 애플리케이션을 설정하기 위해 <sys its:translate="no">dconf</sys>을 사용 할 수 있습니다. 두 경우 모두, 당신은 먼저 <file its:translate="no">/etc/dconf/profile</file>에 있는 <sys its:translate="no">dconf</sys> 프로필을 편집해야 합니다.</p>

<section id="per-user">
  <title>다른 사용자를 위한 다른 즐겨 찾기 애플리케이션 설정하기</title>

  <p>당신은 각 사용자마다 <file its:translate="no">~/.config/dconf/user</file>에 있는 사용자 데이터베이스를 수정하여 기본 즐겨 찾는 애플리케이션을 설정할 수 있습니다. 다음 예제는 <app>gedit</app>, <app>Terminal</app> 그리고 <app>Nautilus</app>를 사용자의 기본 즐겨찾기로 설정하기 위해 <sys its:translate="no">dconf</sys>를 사용했습니다. 예제 코드를 사용하여 그들이 원한다면 사용자는 나중에 목록을 수정할 수 있습니다.</p>

  <!--The code itself need not be translated but the comments withing the
  code have to be translated.-Aruna-->

  <listing>
    <title><file its:translate="no">/etc/dconf/profile</file>의 내용:</title>
      <code>
      #이 줄은 나중에 사용자가 기본 즐겨찾기를 변경하는 것을 허락합니다.
      user-db:user
      </code>
  </listing>

  <listing>
    <title><file its:translate="no">~/.config/dconf/user</file>의 내용:</title>
      <code>
      #gedit, terminal 그리고 nautilus를 기본 즐겨찾기로 설정합니다
      [org/gnome/shell]
      favorite-apps = [<input>'gedit.desktop'</input>, <input>'gnome-terminal.desktop'</input>, <input>'nautilus.desktop'</input>]
      </code>
  </listing>

  <note style="tip">
    <p>당신은 위의 설정을 사용자가 변경하지 못하도록 <link xref="dconf-lockdown">잠글</link> 수 있습니다.
.</p>
  </note>

</section>

<section id="all-users">
  <title>모든 사용자에게 즐겨 찾는 애플리케이션을 동일하게 설정</title>

  <p>모든 사용자에게 동일한 즐겨찾기를 적용시키기 위해서, 당신은 <link xref="dconf-keyfiles">dconf keyfiles</link>을 사용하는 시스템 데이터베이스 파일을 수정해야 합니다. 다음 코드는 <sys its:translate="no">dconf</sys> 프로필을 편집한 다음, first floor의 모든 사용자들에게 기본 즐겨 찾는 애플리케이션을 설정하기 위해 키파일을 생성합니다.</p>

  <!--The code itself need not be translated but the comments within the
  code have to be translated.-Aruna-->

  <listing>
    <title><file its:translate="no">/etc/dconf/profile</file>의 내용:</title>
      <code>
      user-db:user

      #이 줄은 first_floor라고 불리는 시스템 데이터베이스 파일을 정의합니다
      system-db:first_floor
      </code>
  </listing>

  <note style="info">
    <p><code>사용자</code> 데이터베이스 파일 설정은 <code>first_floor</code> 데이터베이스 파일 설정보다 우선권을 갖지만, <code>first_floor</code> 데이터베이스 파일의 <link xref="dconf-lockdown">잠금</link>은 <code>사용자</code>의 파일보다 우선시 될 것 입니다.</p>
  </note>

  <listing>
    <title><file its:translate="no">/etc/dconf/db/first_floor.d/00_floor1_settings</file>의 내용:</title>
      <code>
      # 예제는 gedit, terminal 그리고 nautilus를 기본 즐겨 찾기로 설정합니다
      # first floor의 모든 사용자에게

      [org/gnome/shell]
      favorite-apps = [<input>'gedit.desktop'</input>, <input>'gnome-terminal.desktop'</input>, <input>'nautilus.desktop'</input>]
      </code>
  </listing>

    <p><cmd>dconf update</cmd>을 실행시켜 시스템 데이터베이스에 당신의 변경 사항을 통합시키세요.</p>

</section>

</page>
