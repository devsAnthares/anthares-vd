<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-server-list" xml:lang="ko">

  <info>
    <link type="guide" xref="network"/>
    <revision pkgversion="3.8" date="2013-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
   <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
   <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@gvolny.cz</email>
      <years>2013</years>
    </credit>

    <desc>어떻게 다른 사용자가 다른 파일 공유에 쉽게 접근 할 수 있습니까?</desc>
  </info>

  <title>기본 서버 목록 구성</title>

  <p><app>Nautilus</app> (<app>Files</app> 애플리케이션) 는 XBEL 형식인 <file>~/.config/nautilus/servers</file> 파일에서 파일 공유 서버 목록을 저장합니다. 파일 공유를 사용자가 쉽게 접근할 수 있게 파일 공유 서버 목록을 추가합니다.</p>
  
  <note style="tip">
    <p><em>XBEL</em> (<em>XML Bookmark Exchange Language</em>)은 URIs (Uniform Resource Identifiers)를 공유할 수 있는 표준 XML입니다. GNOME에서 XBEL은 <app>Nautilus</app>과 같은 애플리케이션에서 데스크톱 북마크를 공유하는데에 쓰입니다.</p>
  </note>

  <example>
    <p><file>~/.config/nautilus/servers</file> 파일의 예제:</p>
    <code>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;xbel version="1.0"
      xmlns:bookmark="http://www.freedesktop.org/standards/desktop-bookmarks"
      xmlns:mime="http://www.freedesktop.org/standards/shared-mime-info"&gt;
   &lt;bookmark href="<input>ftp://ftp.gnome.org/</input>"&gt;
      &lt;title&gt;<input>GNOME FTP</input>&lt;/title&gt;
   &lt;/bookmark&gt;
&lt;/xbel&gt;
</code>
    <p>위의 예제에서 <app>Nautilus</app>는 URI <code>ftp://ftp.gnome.org/</code>를 사용하여 <em>GNOME FTP</em>라는 북마크를 생성합니다.</p>
  </example>

</page>
