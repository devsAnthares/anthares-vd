<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="ko">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.8" date="2013-08-08" status="review"/>
    <revision pkgversion="3.14" date="2014-06-18" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>

    <desc>화면은 자동으로 잠기므로 유휴 상태에서는 사용자가 패스워드를 입력해야 합니다.</desc>
  </info>

  <title>사용자가 유휴 상태일때 화면 잠금</title>

  <p>당신은 사용자가 일정 시간동안 유휴 상태일 때마다 화면을 자동으로 잠글 수 있습니다. 이 기능은 사용자가 공용 혹은 비보안적 위치에서 컴퓨터를 두고 갈 때 유용합니다.</p>

  <steps>
    <title>자동 화면 잠금 활성화</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p><sys>local</sys> 데이터베이스에 정보를 제공하기 위해 <file>/etc/dconf/db/local.d/00-screensaver</file>에 키 파일을 생성합니다.</p>
      <listing>
	<title><file> /etc/dconf/db/local.d/00-screensaver </file></title>
<code>
# dconf 경로를 지정하세요
[org/gnome/desktop/session]

# 화면 아웃 전까지의 시간(초)
idle-delay=uint32 180

# dconf 경로를 지정하세요
[org/gnome/desktop/screensaver]

# 화면 아웃 후 화면 잠금
lock-enabled=true

# 화면 잠금 전 화면 아웃 시간(초)
lock-delay=uint32 0
</code>
      </listing>
      <p>보여지는 것처럼 정수 키 값과 함께 <code>uint32</code>를 포함해야 합니다.</p>
    </item>
    <item>
      <p>사용자가 이 설정을 번복하는 것을 막기 위해, <file>/etc/dconf/db/local.d/locks/screensaver</file>에 다음 내용의 파일을 생성하세요:</p>
      <listing>
	<title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# 데스크톱 화면보호기 설정 잠금
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-enabled
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
