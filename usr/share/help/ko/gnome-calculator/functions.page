<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="function" xml:lang="ko">

    <info>
        <link type="guide" xref="index#equation"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>류창우</mal:name>
      <mal:email>cwryu@debian.org</mal:email>
      <mal:years>2007, 2008, 2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

	<title>함수</title>
    
    <p>함수 이름과 함수 인자를 입력하는 방식으로 함수를 사용할 수 있습니다. <app>계산기</app> 에서는 사용자 정의 함수도 지원합니다. 인자가 숫자 값이 아니거나 <link xref="variable">단일 변수</link>일 경우, 각 인자를 세미콜론으로 구분하여 작성하시고, 인자 목록을 괄호로 감싸십시오.</p>
    <example>
    <p>sin 30</p>
    <p>abs (5−9)</p>
    <p>fun (9;10)</p>
    </example>
    <p>새 함수를 추가하려면 아래에 주어진 형식대로 함수를 입력하십시오. 함수 이름과 인자는 대 소문자만 입력해야합니다. @ 다음 부분은 옵션으로 사용할 수 있는 함수의 설명입니다.</p>
    <example>
    <p>SimpleInterest (principal; rate; time) = principal * rate * time @ Simple Interest formula</p>
    </example>
    <p>f(x) 단추로 함수를 추가하고 입력할 수 있습니다.</p>
    <example>
    <p>f(x)를 누르십시오. 상자에 함수 이름을 입력하시고 인자 값으로 쓸 숫자를 입력한 다음 더하기 기호를 눌러 계산을 진행하십시오. 계산기 화면에 함수 수식을 입력하고 <key>Enter</key>키를 입력하십시오.</p>
    </example>
    <example>
    <p>f(x)를 누르십시오. 함수를 선택하고 해당 함수를 누르십시오.</p>
    </example>
    <p>다음 함수가 정의되어 있습니다.</p>
    <table>
    <tr>
    <td><p>abs</p></td>
    <td><p><link xref="absolute">절대값</link></p></td>
    </tr>
    <tr>
    <td><p>ceil</p></td>
    <td><p>올림</p></td>
    </tr>
    <tr>
    <td><p>cos</p></td>
    <td><p><link xref="trigonometry">코사인</link></p></td>
    </tr>
    <tr>
    <td><p>cosh</p></td>
    <td><p><link xref="trigonometry">하이퍼볼릭 코사인</link></p></td>
    </tr>
    <tr>
    <td><p>floor</p></td>
    <td><p>버림</p></td>
    </tr>
    <tr>
    <td><p>frac</p></td>
    <td><p>분수 부분</p></td>
    </tr>
    <tr>
    <td><p>int</p></td>
    <td><p>정수 부분</p></td>
    </tr>
    <tr>
    <td><p>ln</p></td>
    <td><p><link xref="logarithm">자연 로그</link></p></td>
    </tr>
    <tr>
    <td><p>log</p></td>
    <td><p><link xref="logarithm">로그</link></p></td>
    </tr>
    <tr>
    <td><p>not</p></td>
    <td><p><link xref="boolean">불리언 NOT</link></p></td>
    </tr>
    <tr>
    <td><p>ones</p></td>
    <td><p>1의 보수</p></td>
    </tr>
    <tr>
    <td><p>round</p></td>
    <td><p>반올림</p></td>
    </tr>
    <tr>
    <td><p>sgn</p></td>
    <td><p>부호함수</p></td>
    </tr>
    <tr>
    <td><p>sin</p></td>
    <td><p><link xref="trigonometry">사인</link></p></td>
    </tr>
    <tr>
    <td><p>sinh</p></td>
    <td><p><link xref="trigonometry">하이퍼볼릭 사인</link></p></td>
    </tr>
    <tr>
    <td><p>sqrt</p></td>
    <td><p><link xref="power">제곱근</link></p></td>
    </tr>
    <tr>
    <td><p>tan</p></td>
    <td><p><link xref="trigonometry">탄젠트</link></p></td>
    </tr>
    <tr>
    <td><p>tanh</p></td>
    <td><p><link xref="trigonometry">하이퍼볼릭 탄젠트</link></p></td>
    </tr>
    <tr>
    <td><p>twos</p></td>
    <td><p>2의 보수</p></td>
    </tr>
    </table>
</page>
