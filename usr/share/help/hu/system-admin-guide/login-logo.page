<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-logo" xml:lang="hu">

  <info>
    <link type="guide" xref="login#appearance"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2013</years>
    </credit>

    <desc><sys its:translate="no">GDM</sys> <sys its:translate="no">dconf</sys> profil szerkesztése egy kép megjelenítéséhez a bejelentkezési képernyőn.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs@fsf.hu</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Üdvözlő logó hozzáadása a bejelentkezési képernyőhöz</title>

  <p>A bejelentkezési képernyőn lévő üdvözlő logót az <sys>org.gnome.login-screen.logo</sys> GSettings kulcs vezérli. Mivel a <sys>GDM</sys> saját <sys>dconf</sys> profilt használ, hozzáadhat egy üdvözlő logót a profilban lévő beállítások megváltoztatásával.</p>
  
  <p>Amikor megfelelő fényképet választ logónak a bejelentkezési képernyőre, vegye figyelembe a fényképre vonatkozó alábbi követelményeket:</p>
  
  <list>
  <item><p>Az összes fontos formátum támogatott: ANI, BPM, GIF, ICNS, ICO, JPEG, JPEG 2000, PCX, PNM, PBM, PGM, PPM, GTIFF, RAS, TGA, TIFF, XBM, WBMP, XPM és SVG.</p></item>
  <item><p>A fénykép mérete a 48 képpontos magassághoz arányosan méreteződik. Így például ha egy 1920×1080 képpontos logót állít be, akkor az eredeti fénykép 85×48 képpont méretű bélyegképévé változik.</p></item>
  </list>
  
  <steps>
  <title>Az org.gnome.login-screen.logo kulcs beállítása</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
  <item><p>Hozzon létre egy <sys>gdm</sys> adatbázist a rendszerszintű beállításokhoz az <file>/etc/dconf/db/gdm.d/<var>01-logo</var></file> fájlban:</p>
  <code>[org/gnome/login-screen]
  logo='<var>/usr/share/pixmaps/logo/greeter-logo.png</var>'
  </code>
  <p>Cserélje ki az <var>/usr/share/pixmaps/logo/greeter-logo.png</var> útvonalat az üdvözlő logóként használni kívánt képfájlra mutató útvonalra.</p></item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <section id="login-logo-not-update">
  <title>Mi van, ha a logó nem frissül?</title>

  <p>Győződjön meg arról, hogy lefuttatta-e a <cmd>dconf update</cmd> parancsot a rendszer adatbázisainak frissítéséhez.</p>

  <p>Abban az esetben, ha a logó nem frissül, próbálja meg újraindítani a <sys>GDM</sys>-et.</p>
  </section>

</page>
