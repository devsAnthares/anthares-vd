<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-repair" xml:lang="hu">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Fájlrendszer sérüléseinek keresése és helyreállítása használható állapotba.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Sérült fájlrendszer javítása</title>

  <p>A fájlrendszerek váratlan áramkimaradás, rendszer-összeomlás és nem biztonságos meghajtó-eltávolítás miatt sérülhetnek. Ilyen események után ajánlott <em>javítani</em>, de legalábbis <em>ellenőrizni</em> a fájlrendszert a jövőbeli adatvesztés megelőzése érdekében.</p>
  <p>Néha javítás szükséges egy fájlrendszer csatolásához vagy módosításához. Még ha az <em>ellenőrzés</em> nem is jelez sérülést, a fájlrendszer kaphat egy belső „piszkos” jelzést, emiatt javítást igényelhet.</p>

<steps>
  <title>Fájlrendszer sérüléseinek keresése</title>
  <item>
    <p>Nyissa meg a <app>Lemezeket</app> a <gui>Tevékenységek</gui> áttekintésből.</p>
  </item>
  <item>
    <p>Válassza ki a fájlrendszert tartalmazó lemezt a tárolóeszközök bal oldali listájából. Ha több kötet is van a lemezen, akkor válassza ki a fájlrendszert tartalmazó kötetet.</p>
  </item>
  <item>
    <p>A <gui>Kötetek</gui> szakasz alatti eszköztáron kattintson a menü gombra, majd a <gui>Fájlrendszer ellenőrzése</gui> menüpontra.</p>
  </item>
  <item>
    <p>A fájlrendszeren tárolt adatok mennyiségétől függően az ellenőrzés hosszabb ideig is eltarthat. Erősítse meg a művelet megkezdését a felbukkanó ablakban.</p>
   <p>A művelet nem módosítja a fájlrendszert, de szükség szerint leválasztja. Legyen türelmes a fájlrendszer ellenőrzése során.</p>
  </item>
  <item>
    <p>Befejeződése után értesítést kap arról, hogy a fájlrendszer sérült-e. Egyes esetekben még ha a fájlrendszer nem is sérült, szükség lehet a javítására egy belső „piszkos” jelölés törléséhez.</p>
  </item>
</steps>

<note style="warning">
 <title>Adatvesztési veszély javításkor</title>
  <p>Ha a fájlrendszer szerkezete sérült, az kihathat a rajta tárolt fájlokra. Egyes esetekben ezek a fájlok nem állíthatók vissza érvényes formátumba, és törlésre vagy egy speciális könyvtárba áthelyezésre kerülnek. Ez a helyreállított részeket tartalmazó könyvtár általában a <em>lost+found</em> a fájlrendszer gyökerében.</p>
  <p>Ha az adatok túl értékesek a folyamat során való elvesztéshez, akkor ajánlott a kötetről egy lemezképet menteni a javítás megkezdése előtt.</p>
  <p>Ez a lemezkép ezután feldolgozható a megfelelő elemzőeszközökkel, mint például a <app>sleuthkit</app> a hiányzó fájlok és a javításkor helyre nem állított adatdarabok, valamint korábban eltávolított fájlok visszanyerése érdekében.</p>
</note>

<steps>
  <title>Fájlrendszer javítása</title>
  <item>
    <p>Nyissa meg a <app>Lemezeket</app> a <gui>Tevékenységek</gui> áttekintésből.</p>
  </item>
  <item>
    <p>Válassza ki a fájlrendszert tartalmazó lemezt a tárolóeszközök bal oldali listájából. Ha több kötet is van a lemezen, akkor válassza ki a fájlrendszert tartalmazó kötetet.</p>
  </item>
  <item>
    <p>A <gui>Kötetek</gui> szakasz alatti eszköztáron kattintson a menü gombra, majd a <gui>Fájlrendszer javítása</gui> menüpontra.</p>
  </item>
  <item>
    <p>A fájlrendszeren tárolt adatok mennyiségétől függően a javítás hosszabb ideig is eltarthat. Erősítse meg a művelet megkezdését a felbukkanó ablakban.</p>
   <p>A művelet szükség esetén leválasztja a fájlrendszert. A javítási művelet megpróbálja a fájlrendszert konzisztens állapotba állítani, és a sérült fájlokat egy speciális mappába helyezi át. A fájlrendszer javítása alatt legyen türelmes.</p>
  </item>
  <item>
    <p>Befejeződése után értesítést kap arról, hogy a fájlrendszer sikeresen javítható-e. Sikeres javítás esetén a megszokott módon tovább használható lesz.</p>
    <p>Ha a fájlrendszer nem javítható meg, akkor készítsen róla biztonsági másolat lemezképet, hogy később visszanyerhesse a fontos adatait. Ez a lemezkép csak olvasható módú csatolásával és a megfelelő elemzőeszközök, mint a <app>sleuthkit</app> használatával tehető meg.</p>
    <p>A kötet ismételt használatba vételéhez egy új fájlrendszerrel kell <link xref="disk-format">formázni</link>. Ekkor minden adat elvész.</p>
  </item>
</steps>

</page>
