<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="hu">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hogyan állíthat be képet, színt vagy színátmenetet az asztal vagy a zárolási képernyő háttereként.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Az asztal és a zárolási képernyő hátterének megváltoztatása</title>

  <p>A háttérként használt kép megváltoztatható, illetve egyszerű színre vagy színátmenetre cserélhető.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Click <gui>Background</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Background</gui> or <gui>Lock Screen</gui>.</p>
    </item>
    <item>
      <p>Három lehetőség jelenik meg fent:</p>
      <list>
        <item>
          <p>Válassza a <gui>Háttérképek</gui> elemet a GNOME által biztosított háttérképek egyikének használatához. Néhány háttérkép a nap folyamán változik. Ezen háttérképek esetén a jobb alsó sarokban megjelenik egy kis óra ikon.</p>
        </item>
        <item>
          <p>Válassza a <gui>Képek mappa</gui> elemet a <file>Képek</file> mappában található egyik saját képének használatához. A legtöbb fényképkezelő alkalmazás ebben a mappában tárolja a képeket. Ha olyan képet szeretne használni, amely nem a Képek mappában van, akkor vagy használja a <app>Fájlok</app> alkalmazást a képfájlra való jobb kattintással és a <gui>Beállítás háttérképként</gui> menüpont kiválasztásával, vagy a <app>Képmegjelenítő</app> alkalmazást a képfájl megnyitásával, a címsoron lévő menügombra kattintással, és a <gui>Beállítás háttérképként</gui> menüpont kiválasztásával.</p>
        </item>
        <item>
          <p>Válassza a <gui>Színek</gui> elemet egyszerű szín használatához.</p>
        </item>
      </list>
    </item>
    <item>
      <p>A beállítások azonnal alkalmazásra kerülnek.</p>
    </item>
    <item>
      <p>A teljes asztal megjelenítéséhez <link xref="shell-workspaces-switch">váltson egy üres munkaterületre</link>.</p>
    </item>
  </steps>

</page>
