<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="hu">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Telefon vagy internet stick használata a mobil széles sávú hálózathoz csatlakozásra.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Csatlakozás mobil széles sáv használatával</title>

  <p>Beállíthat kapcsolatot egy mobil (3G) hálózathoz a számítógép beépített 3G modemje, mobiltelefonja vagy internet stick használatával.</p>

  <steps>
    <item><p>Ha nincs beépített 3G modemje, akkor csatlakoztassa telefonját vagy az internet sticket a számítógép egyik USB portjához.</p>
    </item>
    <item>
    <p>Kattintson a <gui xref="shell-introduction#yourname">rendszer menüre</gui> a felső sáv jobb oldalán.</p>
  </item>
  <item>
    <p>Válassza ki a <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg" width="16" height="16"/> Mobil széles sáv ki</gui> menüpontot. A menü <gui>Mobil széles sáv</gui> kategóriája ki lesz bontva.</p>
      <note>
        <p>Ha a <gui>Mobil széles sáv</gui> nem jelenik meg a rendszerállapot menüben, akkor győződjön meg róla, hogy az eszköz nincs tömeges tároló módba állítva.</p>
      </note>
    </item>
    <item><p>Válassza a <gui>Kapcsolódás</gui> pontot. Ha először kapcsolódik, elindul a <gui>Mobil széles sávú kapcsolat beállítása</gui> varázsló. A nyitó képernyő megjeleníti a szükséges információk listáját. Kattintson a <gui style="button">Tovább</gui> gombra.</p></item>
    <item><p>Válassza ki a listából a szolgáltató országát. Nyomja meg a <gui>Tovább</gui> gombot.</p></item>
    <item><p>Válassza ki a listából a szolgáltatót. Nyomja meg a <gui>Tovább</gui> gombot.</p></item>
    <item><p>Válassza ki az előfizetést a csatlakozáshoz használt készüléknek megfelelően. Ez meghatározza a hozzáférési pont nevét. Nyomja meg a <gui>Tovább</gui> gombot.</p></item>
    <item><p>Erősítse meg a kiválasztott beállításokat az <gui style="button">Alkalmaz</gui> gomb megnyomásával. A varázsló bezáródik, és a <gui>Hálózat</gui> panel megjeleníti a kapcsolat tulajdonságait.</p></item>
  </steps>

    <note style="tip">
      <p>Egyes telefonok rendelkeznek egy <em>USB-internetmegosztás</em> nevű funkcióval, amely nem igényel beállítást a számítógép oldaláról. Amikor ezt a beállítást aktiválja a telefonon, a kapcsolat a rendszer menüben <gui>Ethernet csatlakozva</gui> néven, a hálózat panelen pedig <gui>USB ehternet</gui> néven jelenik meg.</p>
    </note>

</page>
