<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="hu">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ha nem talál egy frissen létrehozott vagy letöltött fájlt, akkor kövesse ezeket a tippeket.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Elveszett fájl megkeresése</title>

<p>Ha létrehozott vagy letöltött egy fájlt, de nem találja, akkor kövesse ezeket a tippeket.</p>

<list>
  <item><p>Ha nem emlékszik, hova mentette a fájlt, de van elképzelése a nevéről, akkor megpróbálhatja a <link xref="files-search">név alapján megkeresni a fájlt</link>.</p></item>

  <item><p>Ha letöltött egy fájlt, akkor a böngészője lehet, hogy automatikusan egy általános mappába mentette. Nézze meg az <file>Asztal</file> és a <file>Letöltések</file> mappát a saját mappájában.</p></item>

  <item><p>Lehet, hogy véletlenül törölte a fájlt. A fájlok törlésükkor a Kukába kerülnek, és addig ott is maradnak, amíg saját kezűleg ki nem üríti a Kukát. A törölt fájl visszaállításával kapcsolatban lásd: <link xref="files-recover"/>.</p></item>

  <item><p>Lehet, hogy úgy nevezte át a fájlt, hogy az rejtetté vált. A <file>.</file> karakterrel kezdődő, vagy <file>~</file> karakterrel végződő fájlok rejtettek a fájlkezelőben. Nyomja meg a nézetbeállítások gombot a <app>Fájlok</app> eszköztárán, és válassza a <gui>Rejtett fájlok megjelenítése</gui> menüpontot a megjelenítésükhöz. További információkért lásd: <link xref="files-hidden"/>.</p></item>
</list>

</page>
