<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="hu">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nyomja meg a <keyseq><key>Ctrl</key><key>S</key></keyseq> kombinációt több hasonló nevű fájl kijelöléséhez.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Fájlok kijelölése minta alapján</title>

  <p>Egy mappa fájljait fájlnévminta alapján is kiválaszthatja. Nyomja meg a <keyseq><key>Ctrl</key><key>S</key></keyseq> kombinációt a <gui>Találatok kijelölése</gui> ablak megnyitásához. Írja be a fájlok neveinek közös részét, valamint a helyettesítő karaktereket. Két helyettesítő karakter használható:</p>

  <list style="compact">
    <item><p>A <file>*</file> tetszőleges számú karakterre illeszkedik, még a nulla karakterre is.</p></item>
    <item><p>A <file>?</file> pontosan egy tetszőleges karakterre illeszkedik.</p></item>
  </list>

  <p>Például:</p>

  <list>
    <item><p>Ha van egy-egy <file>Számla</file> nevű OpenDocument szövegfájlja, PDF-fájlja és képe, akkor mindhárom kiválasztható a következő mintával:</p>
    <example><p><file>Számla.*</file></p></example></item>

    <item><p>Ha fényképeit például <file>Nyaralás-001.jpg</file>, <file>Nyaralás-002.jpg</file>, <file>Nyaralás-003.jpg</file> néven tárolja, akkor ezeket kijelölheti a következő mintával:</p>
    <example><p><file>Nyaralás-???.jpg</file></p></example></item>

    <item><p>Ha az előbbi fényképei vannak, de néhányat szerkesztett, és nevük végéhez hozzáadta a <file>-szerkesztett</file> utótagot, a szerkesztett fényképek ezzel választhatók ki:</p>
    <example><p><file>Nyaralás-???-szerkesztett.jpg</file></p></example></item>
  </list>

</page>
