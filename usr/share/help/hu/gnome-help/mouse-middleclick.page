<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="hu">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="mouse#tips"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A középső egérgomb használata alkalmazások és lapok megnyitására, és sok másra.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Kattintás a középső egérgombbal</title>

<p>Sok egér és néhány érintőtábla is rendelkezik középső egérgombbal. A görgővel rendelkező egereken általában közvetlenül a görgő lenyomásával végezhet középső kattintást. Ha nincs középső egérgombja, nyomja meg egyszerre a bal és jobb egérgombot a középső kattintáshoz.</p>

<p>A többujjas koppintást támogató érintőtáblákon három ujjal egyszerre koppintva végezhet középső kattintást. Ennek működéséhez engedélyeznie kell a <link xref="mouse-touchpad-click">koppintással kattintást</link> az érintőtábla beállításaiban.</p>

<p>Számos alkalmazás a középső kattintást használja bizonyos műveletek gyors végrehajtására.</p>

<list>
  <item><p>A <gui>Tevékenységek</gui> áttekintésben középső kattintással gyorsan új alkalmazásablakot nyithat. Ehhez kattintson a középső egérgombbal az alkalmazás ikonjára a bal oldali indítóban vagy az Alkalmazások áttekintésben. Az Alkalmazások áttekintés az indítópanel rács gombjának használatával jeleníthető meg.</p></item>

  <item><p>A legtöbb webböngésző lehetővé teszi a hivatkozások gyors megnyitását új lapokon a középső egérgombbal. Kattintson bármely hivatkozásra a középső egérgombbal, és az megnyílik egy új lapon.</p></item>

  <item><p>A fájlkezelőben a középső kattintás két szerepet tölt be. Ha egy mappán végez középső kattintást, akkor a mappa új lapon nyílik meg. Ez a népszerű webböngészők viselkedését utánozza. Ha egy fájlon végez középső kattintást, akkor a fájl a dupla kattintáshoz hasonlóan megnyílik.</p></item>
</list>

<p>Néhány speciális alkalmazás lehetővé teszi a középső egérgomb más feladatokra való használatát. Az alkalmazás súgójában keressen rá a <em>középső kattintás</em> vagy <em>középső egérgomb</em> kifejezésekre.</p>

</page>
