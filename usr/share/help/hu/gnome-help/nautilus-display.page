<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-display" xml:lang="hu">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-display"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A fájlkezelőben használt ikonfeliratok befolyásolása.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Fájlkezelő megjelenítési beállításai</title>

<p>Befolyásolhatja az ikonok alatti feliratok megjelenítését a fájlkezelőben. Válassza a <gui>Fájlok</gui> ikont a felső sávon, majd a <gui>Beállítások</gui> menüpontot, végül a <gui>Megjelenítés</gui> lapot.</p>

<section id="icon-captions">
  <title>Ikonfeliratok</title>
  <!-- TODO: update screenshot for 3.18 and above. -->
  <media type="image" src="figures/nautilus-icons.png" width="250" height="110" style="floatend floatright">
    <p>Ikonok a fájlkezelőben feliratokkal</p>
  </media>
  <p>Az ikonnézet használatakor extra információkat jeleníttethet meg a fájlokról és mappákról az ikonok alatti feliratokban. Ez akkor hasznos, ha például gyakran kell megjelenítenie a fájl tulajdonosát, vagy utolsó módosítási dátumát.</p>
  <p>Kinagyíthat egy mappát az eszköztár nézetbeállítások gombjára kattintva, és a csúszkával egy nagyítási szintet kiválasztva. A nagyítás során a fájlkezelő egyre több és több információt jelenít meg a feliratokban. Legfeljebb három fajta információt választhat a feliratokban történő megjelenítéshez. Az első a legtöbb nagyítási szinten megjelenik, az utolsó csak a nagyon nagy méreteknél.</p>
  <p>Az ikonfeliratokban megjeleníthető információk megegyeznek a listanézet oszlopaiban megjeleníthetőkkel. További részletekért lásd: <link xref="nautilus-list"/>.</p>
</section>

<section id="list-view">

  <title>Listanézet</title>

  <p>Amikor a fájlokat listaként jeleníti meg, <gui>Faként bejárhatja a mappákat</gui>.  Ekkor a fájllistában minden könyvtár előtt kiterjesztők jelennek meg, így egyszerre több mappa tartalma is megjeleníthető. Ez akkor hasznos, ha fontos a mappaszerkezet, például ha a zenefájljai előadónként egy mappában vannak, és albumonként egy almappában.</p>

</section>

</page>
