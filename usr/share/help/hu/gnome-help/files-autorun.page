<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="hu">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alkalmazások automatikus futtatása CD-ről, DVD-ről, fényképezőgépről, zenelejátszóról és más eszközökről és adathordozókról.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Alkalmazások megnyitása eszközökhöz vagy lemezekhez</title>

  <p>Beállítható, hogy egy alkalmazás automatikusan elinduljon egy eszköz csatlakoztatásakor, illetve lemez vagy médiakártya behelyezésekor. Digitális fényképezőgép csatlakoztatásakor például elindíttatható egy fényképkezelő alkalmazás. Ezt ki is kapcsolhatja, ekkor nem történik semmi eszköz csatlakoztatásakor.</p>

  <p>A különböző eszközök csatlakoztatásakor elindítandó alkalmazások kiválasztásához:</p>

<steps>
  <item>
    <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Részletek</gui> szót.</p>
  </item>
  <item>
    <p>Kattintson a <gui>Részletek</gui> elemre a panel megnyitásához.</p>
  </item>
  <item>
    <p>Kattintson a <gui>Külső adathordozók</gui> elemre.</p>
  </item>
  <item>
    <p>Keresse meg a kívánt eszközt vagy adathordozó-típust, majd válasszon egy alkalmazást vagy műveletet az adott adathordozó-típushoz. A különböző eszközök és adathordozó-típusok leírása alább látható.</p>
    <p>Alkalmazás elindítása helyett a <gui>Mappa megnyitása</gui> lehetőséggel beállíthatja azt is, hogy az eszköz megjelenjen a fájlkezelőben. Amikor ez történik, a rendszer rákérdez a teendőre, vagy nem történik automatikusan semmi.</p>
  </item>
  <item>
    <p>Ha nem látja a módosítani kívánt eszközt vagy adathordozót a listában (például Blu-ray lemezek vagy e-könyv olvasók esetén), akkor kattintson az <gui>Egyéb adathordozók</gui> gombra az ilyen eszközök részletesebb listájának megjelenítéséhez. Válassza ki az eszköz vagy adathordozó típusát a <gui>Típus</gui> legördülő mezőben, és az alkalmazást vagy műveletet a <gui>Művelet</gui> legördülő mezőben.</p>
  </item>
</steps>

  <note style="tip">
    <p>Ha nem szeretne automatikusan elindítani alkalmazásokat, függetlenül a csatlakoztatott eszköztől, akkor jelölje be a <gui>Soha ne kérdezzen vagy indítson programokat adathordozó behelyezésekor</gui> jelölőnégyzetet a <gui>Részletek</gui> ablak alján.</p>
  </note>

<section id="files-types-of-devices">
  <title>Eszköz- és adathordozó-típusok</title>
<terms>
  <item>
    <title>Hanglemezek</title>
    <p>Válassza ki kedvenc zenei alkalmazását vagy CD-hangbeolvasóját a hanglemezek kezeléséhez. Ha hang DVD-ket (DVD-A) használ, akkor válassza ki a megnyitásuk módját az <gui>Egyéb adathordozók</gui> alatt. Ha a hanglemezt a fájlkezelőben nyitja meg, akkor a számok WAV fájlokként jelennek meg, amelyeket bármely hanglejátszó alkalmazásban lejátszhat.</p>
  </item>
  <item>
    <title>Videolemezek</title>
    <p>Válassza ki a DVD-k kezelésére használandó kedvenc videoalkalmazását. Az <gui>Egyéb adathordozók</gui> gomb segítségével beállíthatja a Blu-ray, HD DVD, video CD (VCD) és super video CD (SVCD) megnyitásához használandó alkalmazást. Ha a DVD-k vagy más videolemezek nem működnek megfelelően, akkor nézze meg a <link xref="video-dvd"/> szakaszt.</p>
  </item>
  <item>
    <title>Üres lemezek</title>
    <p>Az <gui>Egyéb adathordozók</gui> gomb segítségével válasszon ki egy lemezíró alkalmazást az üres CD-k, DVD-k, Blu-ray lemezek és HD DVD-k megnyitásához.</p>
  </item>
  <item>
    <title>Fényképezőgépek és fényképek</title>
    <p>A <gui>Fényképek</gui> legördülő menüben válassza ki a digitális fényképezőgép csatlakoztatásakor vagy fényképezőgépből származó adathordozó, például CF, SD, MMC vagy MS kártya behelyezésekor. A fényképek tallózását a fájlkezelő használatával is egyszerűsítheti.</p>
    <p>Az <gui>Egyéb adathordozók</gui> alatt választhatja ki a (például fényképészeknél készített) Kodak picture CD-k megnyitásához használandó programot. Ezek normál adat CD-k, amelyek egy <file>Pictures</file> nevű mappában tartalmazzák a JPEG képeket.</p>
  </item>
  <item>
    <title>Zenelejátszók</title>
    <p>Válasszon egy alkalmazást a hordozható zenelejátszóján lévő zenegyűjtemény kezeléséhez, vagy választhatja a fájlok kezelését a fájlkezelővel is.</p>
    </item>
    <item>
      <title>E-könyvolvasók</title>
      <p>Az <gui>Egyéb adathordozók</gui> gomb segítségével válasszon egy alkalmazást az e-könyvolvasóján lévő könyvek kezeléséhez, vagy választhatja a fájlok kezelését a fájlkezelővel is.</p>
    </item>
    <item>
      <title>Szoftver</title>
      <p>Egyes lemezek és külső adathordozók olyan szoftvereket tartalmaznak, amelyeket automatikus futtatásra terveztek az adathordozók behelyezésekor. A <gui>Szoftver</gui> beállítás segítségével vezérelheti, hogy mi történjen automatikus futtatásra tervezett szoftvereket tartalmazó adathordozók behelyezésekor. A rendszer a szoftverek futtatása előtt mindig megerősítést fog kérni.</p>
      <note style="warning">
        <p>Soha ne futtasson nem megbízható adathordozóról származó szoftvert.</p>
      </note>
   </item>
</terms>

</section>

</page>
