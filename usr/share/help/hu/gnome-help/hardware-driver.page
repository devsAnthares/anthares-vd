<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="hu">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A hardver- vagy eszköz-illesztőprogram lehetővé teszi a számítógépnek a csatlakoztatott eszközök használatát.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Mi az az illesztőprogram?</title>

<p>Az eszközök a számítógép fizikai „alkatrészei”. Lehetnek <em>külsők</em>, mint a nyomtatók és a monitor, vagy <em>belsők</em>, mint a video- és hangkártya.</p>

<p>A számítógépnek ezen eszközök használatához tudnia kell, hogyan kommunikáljon velük. Ezt a feladatot az <em>eszköz-illesztőprogram</em> nevű szoftver végzi.</p>

<p>Egy eszköz számítógéphez csatlakoztatásakor az eszköz működtetéséhez a megfelelő illesztőprogramra van szüksége. Ha például csatlakoztat egy nyomtatót, de nem áll rendelkezésre a megfelelő illesztőprogram, akkor nem fogja tudni használni a nyomtatót. Általában a különböző eszköztípusokhoz különböző illesztőprogramok szükségesek, amelyek nem kompatibilisek más típusokkal.</p>

<p>Linux rendszereken a legtöbb eszköz illesztőprogramja alapesetben telepítve van, így minden eszköznek azonnal működnie kellene, amint csatlakoztatja. Egyes illesztőprogramokat azonban lehet, hogy Önnek kell telepítenie, de az is előfordulhat, hogy egyáltalán nem létezik megfelelő illesztőprogram.</p>

<p>Ezen kívül egyes illesztőprogramok befejezetlenek lehetnek, vagy részlegesen nem működnek. Például előfordulhat, hogy egy nyomtató nem képes kétoldalas nyomtatásra, de ettől eltekintve jól működik.</p>

</page>
