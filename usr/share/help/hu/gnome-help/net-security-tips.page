<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="net-security-tips" xml:lang="hu">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Általános tippek az internet használatához.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Maradjon biztonságban az interneten</title>

  <p>Az egyik lehetséges ok, amely miatt Linuxot használ, az a széles körben ismert biztonsága. A Linux többek közt azért van viszonylag biztonságban a vírusoktól és rosszindulatú programoktól, mert kevesen használják. A vírusokat népszerű, hatalmas felhasználói bázissal rendelkező operációs rendszerekre írják, mint például a Windows. A Linux nyílt forrása miatt is igen biztonságos, amely lehetővé teszi a szakembereknek az egyes disztribúciókban megtalálható biztonsági szolgáltatások módosítását és továbbfejlesztését.</p>

  <p>A Linux biztonságossá tételére irányuló erőfeszítések ellenére mindig előfordulhatnak sebezhetőségek. Átlagos internetfelhasználóként továbbra is ki van téve:</p>

  <list>
    <item>
      <p>Adathalászatnak – egyes weboldalak és e-mailek megtévesztéssel próbálnak bizalmas információkat kicsalni.</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Rosszindulatú levelek továbbításának</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Rosszindulatú alkalmazásoknak (vírusoknak)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Nem engedélyezett távoli/helyi hálózati hozzáférésnek</link></p>
    </item>
  </list>

  <p>Az online biztonság megőrzéséhez tartsa szem előtt a következő tippeket:</p>

  <list>
    <item>
      <p>Legyen óvatos az olyan e-mailekkel, mellékletekkel vagy hivatkozásokkal, amelyeket ismeretlenektől kapott.</p>
    </item>
    <item>
      <p>Ha egy weboldal ajánlata túl szép ahhoz, hogy igaz legyen, vagy látszólag fölöslegesen kér be érzékeny adatokat, gondolja meg kétszer is, milyen adatokat ad meg. Bizonyos személyes vagy banki adatok személyazonosság-tolvajok vagy más bűnözők kezébe kerülése súlyos következményekkel járhat.</p>
    </item>
    <item>
      <p>Legyen óvatos, amikor az alkalmazások <link xref="user-admin-explain">rendszergazdai hozzáférést kérnek</link>, különösen ha még nem használta az adott alkalmazást, vagy az interneten nem talál róla semmit. A rendszergazdai hozzáférés megadásával számítógépét a feltörés veszélyének teheti ki.</p>
    </item>
    <item>
      <p>Győződjön meg róla, hogy csak a szükséges távoli hozzáférést biztosító szolgáltatások futnak. Az SSH vagy VNC futtatása hasznos lehet, de számítógépét a behatolás veszélyének teheti ki, ha nem ügyel a biztonságukra. Fontolja meg egy <link xref="net-firewall-on-off">tűzfal</link> használatát a számítógép behatolás elleni védelméhez.</p>
    </item>
  </list>

</page>
