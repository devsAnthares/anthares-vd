<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="hu">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A <guiseq><gui>Beállítások</gui><gui>Színek</gui></guiseq> alatt keresse meg a színprofil képernyőhöz adására vonatkozó beállítást.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Hogyan társíthatok profilokat az eszközökhöz?</title>

  <p>Színprofil képernyőjéhez vagy nyomtatójához rendelésével az azok által megjelenített színeket pontosabbakká teheti.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Eszközök</gui> lehetőségre az oldalsávon.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Színek</gui> lehetőségre az oldalsávon a panel megnyitásához.</p>
    </item>
    <item>
      <p>Válassza ki az eszközt, amelyhez profilt szeretne társítani.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui>Profil hozzáadása</gui> gombot egy meglévő profil kiválasztásához, vagy új profilt importálhat.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Hozzáadás</gui> gombra a választás megerősítéséhez.</p>
    </item>
  </steps>

  <p>Minden eszközhöz több profil is társítható, de csak az egyik lehet <em>alapértelmezett</em> profil. Az alapértelmezett profil akkor kerül felhasználásra, amikor nincsenek a profil automatikus kiválasztásához használható extra információk. Ilyen automatikus kiválasztás történhet például akkor, ha létrehozásra került egy profil fényes papírhoz, egy másik pedig normál papírhoz.</p>

  <!--
  <figure>
    <desc>You can make a profile default by changing it with the radio button.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>
  -->

  <p>Ha csatlakoztatva van kalibrációs hardver, akkor a <gui>Kalibrálás</gui> gombbal létrehozhat új profilt.</p>

</page>
