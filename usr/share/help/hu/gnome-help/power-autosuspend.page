<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="hu">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Számítógép beállítása az automatikus felfüggesztéshez.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Automatikus felfüggesztés beállítása</title>

  <p>Beállíthatja számítógépét az automatikus felfüggesztésre üresjáratban. Különböző időtartamok adhatók meg akkumulátoros és a hálózatra csatlakoztatott működés esetére.</p>

  <steps>

    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Energiagazdálkodás</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Energiagazdálkodás</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>A <gui>Felfüggesztés és bekapcsológomb</gui> szakaszban kattintson az <gui>Automatikus felfüggesztés</gui> lehetőségre.</p>
    </item>
    <item>
      <p>Válassza az <gui>Akkumulátoron</gui> vagy a <gui>Csatlakoztatva</gui> lehetőséget, a kapcsolót állítsa <gui>BE</gui> állásba, és válasszon egy <gui>Késleltetést</gui>. Mindkét lehetőség beállítható.</p>

      <note style="tip">
        <p>Asztali számítógépen csak egy, <gui>Üresjáratban</gui> feliratú beállítás van.</p>
      </note>
    </item>

  </steps>

</page>
