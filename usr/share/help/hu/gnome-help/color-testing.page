<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="hu">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A mellékelt tesztprofilok használatával ellenőrizze, hogy profiljai helyesen kerülnek-e alkalmazásra a kijelzőjére.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Hogyan tesztelhetem a színkezelés megfelelő működését?</title>

  <p>A színprofil hatása néha enyhe, és nehezen látható, hogy változott valami.</p>

  <p>A GNOME számos tesztelésre használható profilt tartalmaz, amelyekkel tisztán láthatóvá válik, hogy a profilok alkalmazásra kerülnek:</p>

  <terms>
    <item>
      <title>Kék</title>
      <p>Ez kékre festi a képernyőt, és teszteli, hogy kalibrálási görbék elküldésre kerülnek-e a kijelzőre.</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Beállítások</gui> szót</p>
    </item>
    <item>
      <p>Válassza a <gui>Beállítások</gui> lehetőséget.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Eszközök</gui> lehetőségre az oldalsávon.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Színek</gui> lehetőségre az oldalsávon a panel megnyitásához.</p>
    </item>
    <item>
      <p>Válassza ki az eszközt, amelyhez profilt szeretne társítani. Hasznos lehet felírni, hogy épp melyik profilt használja.</p>
    </item>
    <item>
      <p>A <gui>Profil hozzáadása</gui> gomb megnyomásával válasszon egy tesztprofilt, amely általában a lista alján található.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Hozzáadás</gui> gombra a választás megerősítéséhez.</p>
    </item>
    <item>
      <p>A korábbi profilhoz való visszatéréshez válassza ki az eszközt a <gui>Színek</gui> panelen, majd válassza ki a tesztprofil kipróbálása előtt használt profilt, és nyomja meg a <gui>Bekapcsolás</gui> gombot az ismételt használatához.</p>
    </item>
  </steps>


  <p>Ezen profilok használatával tisztán láthatja, hogy melyik alkalmazás támogatja a színkezelést.</p>

</page>
