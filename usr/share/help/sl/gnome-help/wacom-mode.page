<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="sl">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Preklopite tablico med načinom tablice in miške.</desc>
  </info>

  <title>Set the Wacom tablet’s tracking mode</title>

<p><gui>Način sledenja</gui> določi kako je kazalec preslikan na zaslon.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Settings</gui>.</p>
  </item>
  <item>
    <p>Click <gui>Devices</gui> in the sidebar.</p>
  </item>
  <item>
    <p>Click <gui>Wacom Tablet</gui> in the sidebar to open the panel.</p>
  </item>
  <item>
    <p>Click the <gui>Tablet</gui> button in the header bar.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click the
    <gui>Bluetooth Settings</gui> link to connect a wireless tablet.</p></note>
  </item>
  <item><p>Poleg <gui>Načina sledenja</gui> izberite <gui>Tablica (absolutno)</gui> ali <gui>Drsna ploščica (relativno)</gui>.</p></item>
</steps>

<note style="info"><p>V <em>absolutnem</em> načinu se vsaka točka na tablici preslika na točko na zaslonu. Zgornji levi kot zaslona vedno ustreza isti točki na tablici.</p>
 <p>In <em>relative</em> mode, if you lift the pointer off the tablet and put it
 down in a different position, the cursor on the screen doesn’t move. This is
    the way a mouse operates.</p>
  </note>

</page>
