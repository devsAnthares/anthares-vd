<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="sl">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>

    <credit type="author">
      <name>Dokumentacijski projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use the <gui>Date &amp; Time Settings</gui> to alter the date or
    time.</desc>
  </info>

<title>Sprememba datuma in časa</title>

  <p>Če sta datum in čas prikazana v vrhnji vrstici nepravilna ali v napačni obliki, ju lahko spremenite: </p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Details</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Date &amp; Time</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If you have <gui>Automatic Date &amp; Time</gui> set to <gui>ON</gui>,
      your date and time should update automatically if you have an internet
      connection. To update your date and time manually, set this to
      <gui>OFF</gui>.</p>
    </item> 
    <item>
      <p>Click <gui>Date &amp; Time</gui>, then adjust the time and date.</p>
    </item>
    <item>
      <p>You can change how the hour is displayed by selecting
      <gui>24-hour</gui> or <gui>AM/PM</gui> for <gui>Time Format</gui>.</p>
    </item>
  </steps>

  <p>You may also wish to <link xref="clock-timezone">set the timezone
  manually</link>.</p>

</page>
