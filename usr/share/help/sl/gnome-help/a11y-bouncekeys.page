<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="sl">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Prezri hitro ponavljajoče se pritiske na isto tipko.</desc>
  </info>

  <title>Vklop odskočnih tipk</title>

  <p>Vklopite <em>odskočne tipke</em> za prezrtje pritiskov tipke, ki se hitro ponavljajo. Možnost je koristna za uporabnike, ki imajo težave s tresavico rok, zaradi katere je posamezna tipka pogosto pritisnjena večkrat zaporedno.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Typing Assist (AccessX)</gui> in the <gui>Typing</gui>
      section.</p>
    </item>
    <item>
      <p>Switch <gui>Bounce Keys</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Hiter vklop in izklop odskočnih tipk</title>
    <p>You can turn bounce keys on and off by clicking the
    <link xref="a11y-icon">accessibility icon</link> on the top bar and
    selecting <gui>Bounce Keys</gui>. The accessibility icon is visible when
    one or more settings have been enabled from the <gui>Universal Access</gui>
    panel.</p>
  </note>

  <p>Uporabite drsnik <gui>Zamik sprejema</gui> za spremembo kako dolgo odskočne tipke čakajo preden zaznajo naslednji pritisk tipke potem, ko ste tipko enkrat že pritisnili. Izberite <gui>Zapiskaj, ko je tipka zavrnjena</gui>, če želite, da računalnik predvaja zvok vsakič, ko prezre pritisk tipke, ker se je zgodil prehitro ali prepozno po predhodnem pritisku tipke.</p>

</page>
