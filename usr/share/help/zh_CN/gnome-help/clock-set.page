<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use the <gui>Date &amp; Time Settings</gui> to alter the date or
    time.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>更改日期和时间</title>

  <p>如果顶部面板上的日期时间显示不正确，您可以更改它：</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Details</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Date &amp; Time</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If you have <gui>Automatic Date &amp; Time</gui> set to <gui>ON</gui>,
      your date and time should update automatically if you have an internet
      connection. To update your date and time manually, set this to
      <gui>OFF</gui>.</p>
    </item> 
    <item>
      <p>Click <gui>Date &amp; Time</gui>, then adjust the time and date.</p>
    </item>
    <item>
      <p>You can change how the hour is displayed by selecting
      <gui>24-hour</gui> or <gui>AM/PM</gui> for <gui>Time Format</gui>.</p>
    </item>
  </steps>

  <p>You may also wish to <link xref="clock-timezone">set the timezone
  manually</link>.</p>

</page>
