<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>将用户界面和帮助文档切换到其他语言。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>更改系统语言</title>

  <p>您可以用数十种语言来显示桌面和程序，只要安装上相应的语言包即可。</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Region &amp; Language</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Language</gui>.</p>
    </item>
    <item>
      <p>Select your desired region and language. If your region and language
      are not listed, click
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui>
      at the bottom of the list to select from all available regions and
      languages.</p>
    </item>
    <item>
      <p>Click <gui style="button">Done</gui> to save.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">×</gui> to restart later.</p>
    </item>
  </steps>

  <p>Some translations may be incomplete, and certain applications may not
  support your language at all. Any untranslated text will appear in the
  language in which the software was originally developed, usually American
  English.</p>

  <p>在您的主文件夹中用一些特定文件夹，它们存放应用程序的文件，如音乐、图片和文档，这些文件夹一般使用您的语言来命名。当您重新登录后，会出来底部对话框，是否将文件名改成您的语言名称，如果您今后将一直使用新的语言，可以把文件夹名称更新一下。</p>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>
  </note>

</page>
