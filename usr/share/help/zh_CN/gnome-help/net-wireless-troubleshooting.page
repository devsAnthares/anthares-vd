<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu wiki 文档贡献者</name>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>无线网络故障排除</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>我们将通过以下步骤将您的计算机连接到 Internet：</p>

  <list style="numbered compact">
    <item>
      <p>执行最初的检查</p>
    </item>
    <item>
      <p>收集硬件相关信息</p>
    </item>
    <item>
      <p>检测硬件</p>
    </item>
    <item>
      <p>尝试创建一个到无线路由器的连接</p>
    </item>
    <item>
      <p>检测调制解调器和路由器</p>
    </item>
  </list>

  <p>要开始操作，请单击页面右上角的<em>下一步</em>链接。此链接及后续页面上的类似链接将带领您完成本指南中的各个步骤。</p>

  <note>
    <title>使用命令行</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
