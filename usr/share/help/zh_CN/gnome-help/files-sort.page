<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>文件按照名称、大小、类型和修改时间排列。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>排序文件和文件夹</title>

<p>您可以用不同的方式来排列文件夹中的文件，例如可以按照修改日期或文件大小。参阅下面常用的排列方法。<link xref="#ways"/> 更多详细信息，请参阅<link xref="nautilus-views"/>。</p>

<p>您可以排列文件的方法，取决于当前使用的<em>文件夹视图</em>。您可以点击工具栏上的列表或图标按钮来更改当前视图。</p>

<section id="icon-view">
  <title>图标视图</title>

  <p>To sort files in a different order, click the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choose <gui>By Name</gui>, <gui>By Size</gui>, <gui>By
  Type</gui>, <gui>By Modification Date</gui>, or <gui>By Access
  Date</gui>.</p>

  <p>例如，如果您选择了<gui>按名称</gui>，文件将会按名称的字母顺序来排列，更多详细信息，请参阅<link xref="#ways"/>。</p>

  <p>You can sort in the reverse order by selecting <gui>Reversed Order</gui>
  from the menu.</p>

</section>

<section id="list-view">
  <title>列表视图</title>

  <p>要用另外一种顺序排列文件，在文件管理器中点击对应的列标题。例如，点击<gui>类型</gui>将按照文件类型排序，再次点击这个列标题将反序排列。</p>
  <p>In list view, you can show columns with more attributes and sort on those
  columns. Click the view options button in the toolbar, pick <gui>Visible
  <!-- FIXME: Get a tooltip added for "View options" -->
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns. See <link xref="nautilus-list"/> for
  descriptions of available columns.</p>

</section>

<section id="ways">
  <title>排列文件的方法</title>

  <terms>
    <item>
      <title>名称</title>
      <p>按照文件名的字母顺序排列</p>
    </item>
    <item>
      <title>大小</title>
      <p>按照文件的大小顺序排列(占用的磁盘空间)，默认是从小到大。</p>
    </item>
    <item>
      <title>类型</title>
      <p>按照文件类型的字母顺序排列。相同类型的文件放在一组，然后按文件名排列。</p>
    </item>
    <item>
      <title>Last Modified</title>
      <p>按照文件最后修改的日期时间排列，默认较早的文件排在前面。</p>
    </item>
  </terms>

</section>

</page>
