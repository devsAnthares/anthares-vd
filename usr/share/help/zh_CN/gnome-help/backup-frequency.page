<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Learn how often you should backup your important files to make sure
    that they are safe.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>备份频率</title>

  <p>多久备份一次取决于要备份的数据的类型。例如，如果您运行的网络环境含有存储在服务器上的关键数据，则每晚备份一次都不多。</p>

  <p>另外，如果您是在家用计算机上备份数据，那么不需要随时备份。您只需要考虑下面这些因素：</p>

<list style="compact">
<item><p>您花在计算机上的时间量。</p></item>
<item><p>计算机上的数据变化的频率以及幅度。</p></item>
</list>

  <p>如果您要备份的数据是低优先级的，或者很少改变的如音乐、邮件和家庭照片，那么每周甚至每月备份一次就足够了。当然，如果您碰巧在处理税单，那就需要更多的备份次数。</p>

  <p>作为一个通用法则，备份的间隔时间，不应当超过您重新做出这项工作所花费的时间。例如，如果要花费一周的时间才能重做出丢失的文档，那么您至少每周备份一次。</p>

</page>
