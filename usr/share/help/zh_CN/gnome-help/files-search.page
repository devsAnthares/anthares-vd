<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="files-search" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Locate files based on file name and type.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>搜索文件</title>

  <p>You can search for files based on their name or file type directly
  within the file manager.</p>

  <links type="topic" style="linklist">
    <title>其他搜索程序</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>搜索</title>
    <item>
      <p>Open the <app>Files</app> application from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>如果您知道要找的文件存放位置，可以直接转到那个文件夹。</p>
    </item>
    <item>
      <p>Type a word or words that you know appear in the file name, and they
      will be shown in the search bar. For example, if you name all your
      invoices with the word “Invoice”, type <input>invoice</input>. Words are
      matched regardless of case.</p>
      <note>
        <p>Instead of typing words directly to bring up the search bar, you can
	click the magnifying glass in the toolbar, or press
        <keyseq><key>Ctrl</key><key>F</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>您可以通过存放位置和文件类型来缩小范围。</p>
      <list>
        <item>
          <p>点击 <gui>主文件夹</gui>将会指定仅搜索位于您<file>Home</file>文件夹，或者点<gui>全部</gui>将搜索所有位置。</p>
        </item>
        <item>
          <p>Click the <gui>+</gui> button and pick a <gui>File Type</gui> from
	  the drop-down list to narrow the search results based on file type.
	  Click the <gui>×</gui> button to remove this option and widen the
	  search results.</p>
        </item>
      </list>
    </item>
    <item>
      <p>您可以对搜索出来的文件，进行打开、复制、删除或其他操作，就像是在文件管理器的文件夹里操作一样。</p>
    </item>
    <item>
      <p>再次点击工具栏上的放大镜按钮退出搜索返回文件夹。</p>
    </item>
  </steps>

</page>
