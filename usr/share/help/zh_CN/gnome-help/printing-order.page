<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>逐份打印和反向顺序打印。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>用相反的顺序打印。</title>

  <section id="reverse">
    <title>逆序</title>

    <p>通常打印是从第一页开始一直到最后一页，因此打完最后一张放在了上面，如果需要您可以按相反顺序来打印。</p>

    <steps>
      <title>要反转顺序：</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
        dialog.</p>
      </item>
      <item>
        <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
        <gui>Reverse</gui>. The last page will be printed first, and so on.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>逐份打印</title>

  <p>If you are printing more than one copy of the document, the print-outs
  will be grouped by page number by default (that is, all of the copies of page
  one come out, then the copies of page two, and so on). <em>Collating</em>
  will make each copy come out with its pages grouped together in the right
  order instead.</p>

  <steps>
    <title>To collate:</title>
    <item>
     <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
     dialog.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
      <gui>Collate</gui>.</p>
    </item>
  </steps>

</section>

</page>
