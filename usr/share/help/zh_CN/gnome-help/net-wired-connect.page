<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>要设置有线网络连接，通常您只需插入网线。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>连接到有线网络(以太网)</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>)
  is displayed on the top bar with three dots while the connection is being
  established. The dots disappear when you are connected.</p>

  <p>如果未出现此现象，您应首先确保网线已插入。网线的一端应插入计算机上的矩形以太网(网络)端口，另一端应插入交换机、路由器、壁装插座或类似设备(具体取决于您的网络设置)。</p>

  <note>
    <p>You cannot plug one computer directly into another one with a network
    cable (at least, not without some extra setting-up). To connect two
    computers, you should plug them both into a network hub, router or
    switch.</p>
  </note>

  <p>如果您仍然不能连接，您的网络可能不支持自动配置(DHCP)。果真如此，您就需要<link xref="net-manual">手动配置</link>。</p>

</page>
