<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="ja">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tips such as “Do not let the battery charge get too low”.</desc>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>ラップトップのバッテリーを長く使う</title>

<p>ラップトップのバッテリーは年月が経過すれば、蓄電能力や容量も徐々に低下していきます。バッテリーの寿命を長持ちさせるコツがいくつかあります。もっとも、あまり大きな効果は期待しないでください。</p>

<list>
  <item>
    <p>バッテリー残量がなくなるまで使い切らないよう注意してください。ほとんどのバッテリーは残量が低下し過ぎないように保護機構を備えていますが、残量がごくわずかになってしまう<em>前に</em>充電してください。充電は、あるい程度放電した状態から行うのが効率的です。しかし、ほんのわずかしか放電していない状態で充電すると、バッテリーを劣化させます。</p>
  </item>
  <item>
    <p>熱はバッテリーの充電性能を劣化させます。必要以上にバッテリーが熱くならないようにしてください。</p>
  </item>
  <item>
    <p>Batteries age even if you leave them in storage. There is little
    advantage in buying a replacement battery at the same time as you get the
    original battery — always buy replacements when you need them.</p>
  </item>
</list>

<note>
  <p>ここで紹介した方法は、ラップトップのバッテリーとして最も利用されているリチウムイオン電池に当てはまります。他の種類のバッテリーには、異なる取扱いが必要になります。</p>
</note>

</page>
