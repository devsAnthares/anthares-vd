<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="ja">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Click, drag, or scroll using taps and gestures on your
    touchpad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Click, drag, or scroll with the touchpad</title>

  <p>ボタンを利用せずにタッチパッドを使って、クリックやダブルクリック、ドラッグ、スクロールができます。</p>

<section id="tap">
  <title>Tap to click</title>

  <p>You can tap your touchpad to click instead of using a button.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Mouse &amp; Touchpad</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure <gui>Touchpad</gui> is
      set to <gui>ON</gui>.</p>
      <note>
        <p>The <gui>Touchpad</gui> section only appears if your system has a
        touchpad.</p>
      </note>
    </item>
   <item>
      <p>Set <gui>Tap to click</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <list>
    <item>
      <p>クリックするにはタッチパッドをタップします。</p>
    </item>
    <item>
      <p>ダブルクリックするには2回タップします。</p>
    </item>
    <item>
      <p>To drag an item, double-tap but don’t lift your finger after the
      second tap. Drag the item where you want it, then lift your finger to
      drop.</p>
    </item>
    <item>
      <p>タッチパッドがマルチタップをサポートしているなら、いちどに2つの指でタップすることで右クリックすることができます。サポートしていない場合は右クリックボタンが必要です。マウスの副ボタンを使わず右クリックする方法については<link xref="a11y-right-click"/>を参照してください。</p>
    </item>
    <item>
      <p>タッチパッドがマルチタップをサポートしているなら、 3つの指を同時にタップして<link xref="mouse-middleclick">中クリック</link>ができます。</p>
    </item>
  </list>

  <note>
    <p>When tapping or dragging with multiple fingers, make sure your fingers
    are spread far enough apart. If your fingers are too close, your computer
    may think they’re a single finger.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Two finger scroll</title>

  <p>You can scroll using your touchpad using two fingers.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Mouse &amp; Touchpad</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure <gui>Touchpad</gui> is
      set to <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Set <gui>Two-finger Scrolling</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <p>When this is selected, tapping and dragging with one finger will work as
  normal, but if you drag two fingers across any part of the touchpad, it will
  scroll instead. Move your fingers between the top and bottom of your touchpad
  to scroll up and down, or move your fingers across the touchpad to scroll
  sideways. Be careful to space your fingers a bit apart. If your fingers are
  too close together, they just look like one big finger to your touchpad.</p>

  <note>
    <p>2つの指でのスクロールは、すべてのタッチパッドで機能しないかもしれません。</p>
  </note>

</section>

<section id="contentsticks">
  <title>Natural scrolling</title>

  <p>You can drag content as if sliding a physical piece of paper using the
  touchpad.</p>

  <steps>
   <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Mouse &amp; Touchpad</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure <gui>Touchpad</gui> is
      set to <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Set <gui>Natural Scrolling</gui> to <gui>ON</gui>.</p>
    </item>
  </steps>

  <note>
    <p>This feature is also known as <em>Reverse Scrolling</em>.</p>
  </note>

</section>

</page>
