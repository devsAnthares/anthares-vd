<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="cs">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak používat aplikace a celé uživatelské prostředí bez myši.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Ovládání klávesnicí</title>

  <p>Tato stránka se podrobněji zabývá ovládáním z klávesnice pro lidi, kteří nemohou používat myš nebo jiné ukazovací zařízení, nebo kteří chtějí využít svoji klávesnici na maximum. Na klávesové zkratky, které jsou použitelné pro všechny uživatele, se podívejte do kapitoly <link xref="shell-keyboard-shortcuts"/>.</p>

  <note style="tip">
    <p>Když nemůžete použít ukazovací zařízení, jako je myš, můžete ovládat ukazatel myši pomocí numerické klávesnice. Jak na to viz <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Pohyb v uživatelském rozhraní</title>
  <tr>
    <td><p><key>Tab</key> a</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>Přesunout zaměření klávesnice mezi různými ovládacími prvky. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> přesouvá mezi skupinami ovládacích prvků, jako třeba z postranního panelu do hlavního obsahu. Pomocí <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> může také opustit ovládací prvek, který používá klávesu <key>Tab</key> přímo, jako třeba textová oblast.</p>
      <p>Pro změnu zaměření v opačném pořadí držte zmáčknutý <key>Shift</key>.</p>
    </td>
  </tr>
  <tr>
    <td><p>kurzorové šipky</p></td>
    <td>
      <p>Přesunout výběr mezi položkami v rámci jednoho ovládacího prvku nebo mezi ovládacími prvky jedné skupiny. Použijte je například k zaměření tlačítek v nástrojové liště, výběru položek v seznamovém nebo ikonovém zobrazení nebo výběru skupinového přepínače ve skupině.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>kurzorové šipky</keyseq></p></td>
    <td><p>V seznamovém nebo ikonovém zobrazení přesunout zaměření klávesnice na jinou položku bez ovlivnění toho, které položky jsou vybrané.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>kurzorové šipky</keyseq></p></td>
    <td><p>V seznamovém nebo ikonovém zobrazení vybrat všechny položky počínaje právě vybranou až po nově změřenou.</p>
    <p>Položky ve stromovém zobrazení, které mají potomky, mohou být rozbalené nebo sbalené, takže své potomky zobrazují nebo skrývají: rozbalení provedete zmáčknutím <keyseq><key>Shift</key> <key>→</key></keyseq> a sbalení zmáčknutím <keyseq><key>Shift</key> <key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>mezerník</key></p></td>
    <td><p>Aktivovat zaměřenou položku, jako je tlačítko, zaškrtávací políčko nebo seznam položek.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>mezerník</key></keyseq></p></td>
    <td><p>V seznamovém nebo ikonovém zobrazení vybrat, případně vyřadit z výběru, zaměřenou položku, aniž by to ovlivnilo výběr ostatních položek.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Podržením klávesy <key>Alt</key> zviditelníte <em>horké klávesy</em>: podtržená písmena v položkách nabídky, v tlačítkách a v ostatních ovládacích prvcích. Zmáčknutím <key>Alt</key> plus podržené písmeno ovládací prvek aktivujete, stejně jako byste na něj klikli.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Opustit nabídku, vyskakovací okno, přepínač nebo dialogové ono.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Otevřít první nabídku v nabídkové liště okna. Pro pohyb v nabídkách použijte kurzorové šipky na klávesnici.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>Otevřít nabídku aplikace na horní liště.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> nebo</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>Zobrazit kontextovou nabídku pro aktuální výběr, stejně jako byste na něj klikli pravým tlačítkem.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>Ve správci souborů zobrazit kontextovou nabídku pro aktuální složku, jako byste klikli pravým tlačítkem na pozadí a ne na nějakou položku.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>a</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>V rozhraní s kartami přepnout na kartu vlevo nebo vpravo.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Ovládání uživatelského prostředí</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Cyklicky procházet okna v rámci stejné aplikace. Držte zmáčknutou klávesu <key>Alt</key> a mačkejte <key>F6</key> dokud se nezvýrazní okno, které chcete, a pak <key>Alt</key> pusťte. Je to podobné jako funkce <keyseq><key>Alt</key><key>`</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Cyklicky procházet všechna otevřená okna v rámci pracovní plochy.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Otevřít seznam upozornění.</link> Zmáčknutím <key>Esc</key> jej zavřete.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Ovládání oken</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Zavřít aktuální okno.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> nebo <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Obnovit maximalizované okno do jeho původní velikosti. Pro maximalizaci použijte <keyseq><key>Alt</key><key>F10</key></keyseq>. Jedná se o společnou klávesovou zkratku pro maximalizaci i obnovení velikosti.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Přesunout aktuální okno. Zmáčkněte <keyseq><key>Alt</key> <key>F7</key></keyseq> a pak použijte kurzorové šipky k přesunu okna. Zmáčknutím <key>Enter</key> přesun okna dokončete nebo jej klávesou <key>Esc</key> vraťte na původní místo.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Změnit velikost aktuálního okna. Zmáčkněte <keyseq><key>Alt</key> <key>F8</key></keyseq> a pak použijte kurzorové šipky k změně velikosti okna. Zmáčknutím <key>Enter</key> změnu velikosti okna dokončete nebo jej klávesou <key>Esc</key> vraťte na původní velikost.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> nebo <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p><link xref="shell-windows-maximize">Maximalizovat</link> okno. Zmáčknutím <keyseq><key>Alt</key><key>F10</key></keyseq> nebo <keyseq><key>Super</key><key>↓</key></keyseq> jej obnovíte do jeho původní velikosti.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Minimalizovat okno.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Maximalizovat okno ve svislém směru podél levé strany obrazovky. Opětovným zmáčknutím jej obnovíte do původní velikosti. Zmáčknutí <keyseq><key>Super</key> <key>→</key></keyseq> přepne strany.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Maximalizovat okno ve svislém směru podél pravé strany obrazovky. Opětovným zmáčknutím jej obnovíte do původní velikosti. Zmáčknutí <keyseq><key>Super</key> <key>←</key></keyseq> přepne strany.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>mezerník</key></keyseq></p></td>
    <td><p>Zobrazit nabídku okna, stejnou jako když kliknete pravým tlačítkem na záhlaví okna.</p></td>
  </tr>
</table>

</page>
