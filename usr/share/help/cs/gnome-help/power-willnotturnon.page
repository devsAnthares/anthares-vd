<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="cs">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Možnou příčinou je povysunutý napájecí kabel nebo problémy s hardwarem.</desc>
    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Můj počítač se nezapne</title>

<p>Existuje řada důvodů, proč se počítač nechce zapnout. Toto téma přináší stručný přehled některých možných příčin.</p>
	
<section id="nopower">
  <title>Počítač není připojen k napájení, má vybitou baterii nebo povysunutý napájecí kabel</title>
  <p>Ujistěte se, kabel napájení počítače je pořádně zastrčený ve zdířce v počítači a v elektrické zásuvce, a že jde elektřina. Rovněž se ujistěte, že je připojený a zapnutý monitor. Jestliže máte notebook, připojte nabíječku (pro případ, že by byly vybité baterie). Můžete také zkontrolovat, že baterie, pokud jsou výměnné, jsou správně zasunuté (podívejte se ze spodní strany notebooku).</p>
</section>

<section id="hardwareproblem">
  <title>Problémy s hardwarem počítače</title>
  <p>Některá součást v počítači může být poškozená nebo nefunkční. V takovém případě bude počítač potřebovat opravu. Mezi nejčastější závady patří vadný napájecí zdroj, nesprávně zasunuté součásti (např. paměťový modul nebo karta ve sběrnici), vadný disk a vadná základní deska.</p>
</section>

<section id="beeps">
  <title>Počítač zapípá a vypne se</title>
  <p>V případě, že počítač po zapnutí několikrát pípne a pak se vypne (nebo selže spuštění), může to signalizovat objevení nějakého problému. Tato pípnutí se nazývají <em>beep codes</em> a jejich počet a způsob říkají, o jaký problém se jedná. Bohužel každý výrobce používá jiný vzor pípání, takže je potřeba se podívat do příručky k základní desce nebo dát počítač na opravu.</p>
</section>

<section id="fans">
  <title>Ventilátor počítače běží, ale na obrazovce se nic neobjeví</title>
  <p>Nejprve zkontrolujte, že je váš monitor připojený k počítači a zapnutý.</p>
  <p>Problém může být způsoben také selháním hardwaru. Ventilátory se mohou po zmáčknutí vypínače napájení roztočit, ale u ostatních podstatných částí počítače může spuštění selhat. V takovém případě váš počítač potřebuje opravu.</p>
</section>

</page>
