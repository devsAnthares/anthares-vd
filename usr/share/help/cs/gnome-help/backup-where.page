<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="cs">

  <info>
    <link type="guide" xref="backup-why"/>
    <title type="sort">c</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rady, kde uchovávat zálohy a jaké typy úložišť používat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Kde uchovávat zálohy</title>

  <p>Kopie svých záloh bystě měli uchovávat někde mimo svůj počítač — například na externím disku. To zajistí, že když se počítač poškodí, záloha zůstane nedotčená. Z důvodu maximální bezpečnosti byste neměli zálohu uchovávat ani ve stejné budově, jako je počítač. Když by došlo k požáru nebo ke krádeži, mohli byste v případě uchování obou kopií na stejném místě o obě přijít.</p>

  <p>Důležité je také zvolit správné <em>zálohovací médium</em>. Zálohovat potřebujete na zařízení, které má dostatečnou kapacitu pro všechny vaše zálohované soubory.</p>

   <list style="compact">
    <title>Možná místní a vzdálená úložiště</title>
    <item>
      <p>Fleshdisk do USB (malá kapacita)</p>
    </item>
    <item>
      <p>Interní disk (velká kapacita)</p>
    </item>
    <item>
      <p>Externí pevný disk (obvykle velká kapacita)</p>
    </item>
    <item>
      <p>Síťový disk (velká kapacita)</p>
    </item>
    <item>
      <p>Souborový/zálohovací server (velká kapacita)</p>
    </item>
    <item>
     <p>Zapisovatelné CD nebo DVD (malá/střední kapacita)</p>
    </item>
    <item>
     <p>Zálohovací služba on-line (například <link href="http://aws.amazon.com/s3/">Amazon S3</link>, kapacita se odvíjí od ceny)</p>
    </item>
   </list>

  <p>Některé z těchto možností mají dostatečnou kapacitu pro zálohu všech souborů ve vašem systémů, také známou jako <em>kompletní záloha systému</em>.</p>
</page>
