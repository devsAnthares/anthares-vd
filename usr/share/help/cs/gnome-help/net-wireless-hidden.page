<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="cs">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Jak se připojit k bezdrátové síti, která není zobrazená v seznamu sítí.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Připojení ke skryté bezdrátové síti</title>

<p>Bezdrátovou síť je možné nastavit tak, aby byla „skrytá“. Skrytá bezdrátová síť se nezobrazuje v seznamu bezdrátových sítí zobrazených v nastavení <gui>Síť</gui>. Takže, když se k takové síti chcete připojit:</p>

<steps>
  <item>
    <p>Otevřete <gui xref="shell-introduction#yourname">systémovou nabídku</gui> vpravo na horní liště.</p>
  </item>
  <item>
    <p>Vyberte <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>Wi-Fi   Nepřipojeno</gui>. Oddíl Wi-Fi v nabídce se rozbalí.</p>
  </item>
  <item>
    <p>Klikněte na <gui>Nastavení Wi-Fi</gui>.</p>
  </item>
  <item><p>Klikněte na tlačítko <gui>Připojit se ke skryté síti…</gui></p></item>
 <item>
  <p>V okně, které se objeví, vyberte v rozbalovacím seznam <gui>Připojení</gui> některou z dříve použitých skrytých sítí nebo <gui>Nová</gui> pro novou.</p>
 </item>
 <item>
  <p>U nového připojení napište název sítě a zvolte typ bezdrátového zabezpečení v rozbalovacím seznamu <gui>Zabezpeční Wi-Fi</gui>.</p>
 </item>
 <item>
  <p>Zadejte heslo nebo jiné bezpečnostní údaje.</p>
 </item>
 <item>
  <p>Klikněte na <gui>Připojit</gui>.</p>
 </item>
</steps>

  <p>Musíte se podívat do nastavení bezdrátového přístupového bodu nebo směrovače, abyste zjistili název sítě. Pokud název sítě (SSID) nevíte, můžete použít také <em>BSSID</em> (Basic Service Set Identifier, což je adresa MAC přístupového bodu), která vypadá nějak jako <gui>02:EF:A1:B2:C3:04</gui> a můžete ji najít na štítku na spodní straně zařízení.</p>

  <p>Rovněž byste si měli zjistit nastavení zabezpečení bezdrátového přístupového bodu. Dívejte se po výrazech jako WEP nebo WPA.</p>

<note>
 <p>Možná vás napadne, že skrytí vaší bezdrátové sítě vylepší zabezpečení, protože když o ní lidé nebudou vědět, nemohou se k ní připojit. Ve skutečnosti tomu ale tak není. Takovou síť je sice o něco těžší najít, ale stále je vyhledatelná.</p>
</note>

</page>
