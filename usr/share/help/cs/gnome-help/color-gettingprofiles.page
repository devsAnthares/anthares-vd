<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="cs">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Profily barev jsou poskytovány výrobci a můžete si je sami vygenerovat.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Kde získám profily barev?</title>

  <p>Nejlepším způsobem, jak získat profil barev je sám si jej vygenerovat, i když to vyžaduje trochu námahy.</p>
  <p>Řada výrobců se snaží poskytnout profil barev pro svá zařízení, i když občas to řeší přibalením k <em>instalátoru ovladače</em>, který si budete muset stáhnout, rozbalit jej a profily barev v něm vyhledat.</p>
  <p>Někteří výrobci neposkytují pro hardware správné profily a takovýmto profilům je pak lépe se vyhnout. Dobrým vodítkem je stáhnout si profil a pokud je datum jeho vytvoření starší jak rok od data, kdy jste si zařízení koupili, obsahuje nejspíše obecně vygenerovaná data, která jsou nepoužitelná.</p>

  <p>Viz <link xref="color-why-calibrate"/> ohledně více informací, proč jsou výrobci poskytované profily často více než nepoužitelné.</p>

</page>
