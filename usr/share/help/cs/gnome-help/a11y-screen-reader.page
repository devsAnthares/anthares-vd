<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" id="a11y-screen-reader" xml:lang="cs">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Jak používat čtečku obrazovky <app>Orca</app> k hlasovému čtení uživatelského rozhraní.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Čtení obrazovky nahlas</title>

  <p>GNOME poskytuje čtečku obrazovky <app>Orca</app>, která umí říkat, co je na obrazovce v uživatelském rozhraní. V závislosti na tom, jak máte nainstalováno GNOME, můžete mít čtečku Orca nainstalovanou. Pokud ne, musíte ji nejprve nainstalovat.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Nainstalovat čtečku Orca</link></p>
  
  <p>Pro spuštění čtečky <app>Orca</app> pomocí klávesnice:</p>
  
  <steps>
    <item>
    <p>Zmáčkněte <keyseq><key>Super</key><key>Alt</key><key>S</key></keyseq>.</p>
    </item>
  </steps>
  
  <p>Nebo pro spuštění čtečky <app>Orca</app> pomocí myši a klávesnice:</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Univerzální přístup</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Univerzální přístup</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Čtečka obrazovky</gui> v části <gui>Zrak</gui>, abyste se přepnuli do dialogového okna <gui>Čtečka obrazovky</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Rychlé zapnutí a vypnutí Čtečky obrazovky</title>
    <p>Čtečku obrazovky můžete zapnout nebo vypnout kliknutím na <link xref="a11y-icon">ikonu zpřístupnění</link> na horní liště a následným výběrem položky <gui>Čtečka obrazovky</gui>.</p>
  </note>

  <p>Další informace můžete najít v <link href="help:orca">nápovědě ke čtečce Orca</link>.</p>
</page>
