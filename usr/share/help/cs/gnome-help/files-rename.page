<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="cs">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak změnit název souboru nebo složky.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Přejmenování souboru nebo složky</title>

  <p>Stejně jako jiné správce souborů, i <app>Soubory</app> můžete použít ke změně názvu souboru nebo složky.</p>

  <steps>
    <title>Když chcete přejmenovat soubor nebo složku:</title>
    <item><p>Klikněte na položku pravým tlačítkem a vyberte <gui>Přejmenovat</gui> nebo vyberte soubor a zmáčkněte <key>F2</key>.</p></item>
    <item><p>Napište nový název a zmáčkněte <key>Enter</key> nebo klikněte na <gui>Přejmenovat</gui>.</p></item>
  </steps>

  <p>Soubor můžete přejmenovat také v okně <link xref="nautilus-file-properties-basic">vlastností</link>.</p>

  <p>Když se pustíte do přejmenování souboru, bude označena jen jeho první část a přípona ne (tj. část za poslední <file>.</file>). Přípona normálně naznačuje, o jaký typ souboru se jedná (například <file>soubor.pdf</file> je dokument PDF), a normálně ji nepotřebujete měnit. Pokud potřebujete změnit i příponu, označte celý název a změňte jej.</p>

  <note style="tip">
    <p>Když přejmenujete nesprávný soubor nebo zadáte nesprávný název, můžete přejmenování vrátit zpět. Vrácení proveďte ihned kliknutím na tlačítko nabídky v nástrojové liště a volbou <gui>Vrátit přejmenování</gui>, nebo zmáčknutím <keyseq><key>Ctrl</key><key>Z</key></keyseq>. Soubor dostane svůj dřívější název.</p>
  </note>

  <section id="valid-chars">
    <title>Platné znaky pro názvy souborů</title>

    <p>V názvu souboru můžete použít libovolný znak, vyjma <file>/</file> (lomítko). Některá zařízení ale používají <em>souborové systémy</em>, které mají rozsáhlejší omezení na názvy souborů. Proto je dobrým zvykem vyhnout se v názvech souborů následujícím znakům: <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file> a <file>/</file>.</p>

    <note style="warning">
    <p>Pokud pojmenujete soubor s <file>.</file> (tečka) jako prvním znakem a zkusíte se na něj podívat ve správci souborů, zjistíte, že je <link xref="files-hidden">skrytý</link>.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Běžné problémy</title>

    <terms>
      <item>
        <title>Název souboru je již použit</title>
        <p>Nemůžete mít dva soubory nebo složky se stejným názvem v jedné složce. Když se pokusíte přejmenovat soubor na název, který již ve složce existuje, správce souborů vám to nedovolí.</p>
        <p>U názvů souborů a složek se rozlišuje velikost písmen, takže název <file>Soubor.txt</file> není to stejné co <file>SOUBOR.txt</file>. Použití názvů lišících se jen velikostí písmen, jako v tomto příkladu, je sice možné, ale nedoporučuje se.</p>
      </item>
      <item>
        <title>Název souboru je příliš dlouhý</title>
        <p>Na některých souborových systémech nemohou mít názvy souborů více než 255 znaků. Toto omezení zahrnuje jak vlastní název souboru, tak cestu k němu (například <file>/home/venda/Dokumenty/práce/rozjednané zakázky/…</file>), takže byste se měli vyhnout dlouhým názvům souborů a složek, pokud je to možné.</p>
      </item>
      <item>
        <title>Volba pro přejmenování je zašedlá</title>
        <p>Jestliže je <gui>Přejmenovat</gui> zašedlé, nemáte oprávnění soubor přejmenovat. Měli byste být obezřetní ohledně přejmenování takovýchto souboru, protože přejmenování některých chráněných souborů může způsobit nestabilitu systému. Více podrobností viz <link xref="nautilus-file-properties-permissions"/>.</p>
      </item>
    </terms>

  </section>

</page>
