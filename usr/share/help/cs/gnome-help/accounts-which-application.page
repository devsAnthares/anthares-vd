<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="cs">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aplikace mohou používat účty vytvořené v <app>Účtech on-line</app> a služby, které nabízí.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Služby a aplikace on-line</title>

  <p>Jakmile přidáte účet on-line, může použít kterákoliv aplikace kteroukoliv dostupnou službu, kterou jste <link xref="accounts-disable-service">nezakázali</link>. Různí poskytovatelé poskytují různé služby. Na této stránce je uveden seznam různých služeb a u nich některé aplikace, o kterých je známo, že je používají.</p>

  <terms>
    <item>
      <title>Kalendář</title>
      <p>Kalendářová služba umožňuje zobrazovat, přidávat a upravovat události v kalendáři on-line. Je používána aplikacemi jako <app>Kalendář</app>, <app>Evolution</app> a <app>California</app>.</p>
    </item>

    <item>
      <title>Diskuzní</title>
      <p>Diskuzní služba umožňuje pokec s vašimi kontakty na populárních platformách rychlé komunikace. Je používána aplikací <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Kontakty</title>
      <p>Adresářová služba umožňuje zobrazit si zveřejněné údaje vašich kontaktů na různých službách. Používají ji aplikace jako <app>Kontakty</app> a <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Dokumenty</title>
      <p>Dokumentová služba umožňuje zobrazit vaše dokumenty uložené on-line, jako třeba Google Docs. Své dokumenty si můžete prohlížet pomocí aplikace <app>Dokumenty</app>.</p>
    </item>

    <item>
      <title>Soubory</title>
      <p>Souborová služba přidává vzdálené úložiště souborů, stejně jako když nějaké přidáte pomocí funkce <link xref="nautilus-connect">Připojit k serveru</link> ve správci souborů. Ke vzdáleným souborům se můžete dostat ve správci souborů i v dialogových oknech pro otevření a uložení souboru v kterékoliv aplikaci.</p>
    </item>

    <item>
      <title>✉ (Mail/symbol obálky)</title>
      <p>Poštovní služba umožňuje odesílat a přijímat elektronickou poštu přes poskytovatele jako je Google. Je používána aplikací <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Fotografie</title>
      <p>Obrázkové služby umožňují zobrazení fotografií uložených on-line, jako jsou třeba ty nahrané na Facebook. Své fotografie si můžete prohlížet pomocí aplikace <app>Fotky</app>.</p>
    </item>

    <item>
      <title>Tiskárny</title>
      <p>Tisková služba umožňuje posílat poskytovateli kopie PDF z tiskového dialogového okna kteréhokoliv aplikace. Poskytovatel může poskytovat tiskové služby nebo může posloužit jen jako úložiště PDF, které si můžete stáhnout a vytisknout později.</p>
    </item>

    <item>
      <title>Odložené čtení</title>
      <p>Služba odloženého čtení umožňuje uložení webové stránky do externí služby, abyste si ji mohli později přečíst na jiném zařízení. Zatím žádná aplikace tuto službu nevyužívá.</p>
    </item>

  </terms>

</page>
