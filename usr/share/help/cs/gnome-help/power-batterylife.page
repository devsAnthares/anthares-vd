<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="cs">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tipy na snížení spotřeby vašeho počítače.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Snižte spotřebu a prodlužte výdrž baterie</title>

  <p>Počítač může mít celkem velkou spotřebu. Použitím pár jednoduchých postupů šetření energií můžete snížit svůj účet za elektřinu a pomoci životnímu prostředí.</p>

<section id="general">
  <title>Obecné rady</title>

<list>
  <item>
    <p>Když počítač zrovna nepoužíváte, <link xref="shell-exit#suspend">uspěte jej do paměti</link>. Tím významně snížíte odběr energie a přitom počítač můžete okamžitě probudit.</p>
  </item>
  <item>
    <p>Když počítač nebudete používat po delší dobu, tak jej <link xref="shell-exit#shutdown">vypněte</link>. Někdy to lidé nedělají z obavy, že pravidelným vypínáním počítač rychleji „opotřebují“, ale to není pravda.</p>
  </item>
  <item>
    <p>Použijte panel <gui>Napájení</gui> v <app>Nastaveních</app> ke změně nastavení napájení. K dispozici je řada voleb, které pomáhají šetřit energii: po určité době můžete <link xref="display-blank">automaticky zhasnout obrazovku</link>, <link xref="display-brightness">snížit jas obrazovky</link> a přimět počítač <link xref="power-suspend">automaticky se uspat</link>, když jej po nějakou dobu nepoužíváte.</p>
  </item>
  <item>
    <p>Vypněte externí zařízení (jako je tiskárna a skener), když je nepoužíváte.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Notebooky, tablety a další zařízení s bateriemi</title>

 <list>
   <item>
     <p>Snižte <link xref="display-brightness">jas displeje</link>. Napájení obrazovky se z významné části podílí na spotřebě energie notebooku.</p>
     <p>Většina notebooků má na klávesnici tlačítka (nebo kombinace kláves) sloužící k regulaci jasu.</p>
   </item>
   <item>
     <p>Když po nějakou dobu nepotřebujete internetové připojení, vypněte bezdrátovou kartu, případně Bluetooth. Tato zařízení pravidelně vysílají, i když po síti zrovna nekomunikujete, což také spotřebuje trochu energie.</p>
     <p>Některé počítače mají fyzický vypínač, kterým můžete bezdrátová zařízení vypnout, zatímco jiné k tomu používají klávesové zkratky. Až budete bezdrátové zařízení znovu potřebovat, můžete jej zase zapnout.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Pokročilejší rady</title>

 <list>
   <item>
     <p>Snižte počet úloh, které běží na pozadí. Čím více toho počítač dělá, tím má vyšší spotřebu.</p>
     <p>Většina běžících aplikací toho moc nedělá, pokud je zrovna aktivně nepoužíváte. Avšak aplikace, které často sbírají data z Internetu, přehrávají hudbu nebo filmy mohou spotřebu energie znatelně ovlivnit.</p>
   </item>
 </list>

</section>

</page>
