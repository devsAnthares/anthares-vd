<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="cs">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak používat volič pracovních ploch.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

 <title>Přepínání mezi pracovními plochami</title>

 <steps>  
 <title>Pomocí myši:</title>
 <item>
    <p if:test="!platform:gnome-classic">Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui>.</p>
  <p if:test="platform:gnome-classic">V levé horní části obrazovky klikněte na nabídku <gui xref="shell-introduction#activities">Aplikace</gui> a zvolte <gui>Přehled činností</gui>.</p>
 </item>
 <item>
  <p>Klikněte na pracovní plochu ve <link xref="shell-workspaces">voliči pracovních ploch</link> na pravé straně obrazovky, abyste viděli otevřená okna na této pracovní ploše.</p>
 </item>
 <item>
  <p>Kliknutím na některý z náhledů oken aktivujete pracovní plochu.</p>
 </item>
 </steps>
 
  <p if:test="platform:gnome-classic">Případně se můžete mezi pracovními plochami přepínat tak, že kliknete na ikonu pracovní plochy po pravé straně seznamu oken na spodní liště a v nabídce si vyberete pracovní plochu, kterou chcete používat.</p>

 <list>
 <title>Pomocí klávesnice:</title>  
  <item>
    <p>Zmáčkněte <keyseq><key xref="keyboard-key-super">Super</key> <key>Page Up</key></keyseq> nebo <keyseq><key>Ctrl</key><key>Alt</key><key>Up</key></keyseq> pro přesun na pracovní plochu zobrazenou nad aktuální pracovní plochou ve výběru pracovních ploch.</p>
  </item>
  <item>
    <p>Zmáčkněte <keyseq><key>Super</key><key>Page Down</key></keyseq> nebo <keyseq><key>Ctrl</key><key>Alt</key><key>Down</key></keyseq> pro přesun na pracovní plochu zobrazenou pod aktuální pracovní plochou ve výběru pracovních ploch.</p>
  </item>
 </list>

</page>
