<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="cs">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Musíte zrušit zaškrtnutí volby <gui>Zpřístupnit ostatním uživatelům</gui> v nastavení síťového připojení.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Ostatní uživatelé nemohou upravovat síťové připojení</title>

  <p>V případě, že můžete upravovat síťové připojení, ale ostatní uživatelé na počítači ne, je potřeba nastavit, aby připojení bylo <em>přístupné všem uživatelům</em>. To zajistí, že kdokoliv na počítači se bude moci <em>připojit</em> pomocí tohoto připojení.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Síť</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Síť</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V seznamu nalevo vyberte <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na tlačítko <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">nastavení</span></media> otevřete podrobnosti o připojení.</p>
    </item>
    <item>
      <p>V panelu na levé straně vyberte <gui>Identita</gui>.</p>
    </item>
    <item>
      <p>V dolní části panelu <gui>Identita</gui> zaškrtněte volbu <gui>Zpřístupnit ostatním uživatelům</gui>, aby i ostatní uživatelné mohli používat toto síťové připojení.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Použít</gui> změny uložte.</p>
    </item>
  </steps>

</page>
