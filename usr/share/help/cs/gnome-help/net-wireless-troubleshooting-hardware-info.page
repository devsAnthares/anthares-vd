<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="cs">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Přispěvatelé do wiki s dokumentací k Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>V jednotlivých částech kroků řešení problému budete možná potřebovat údaje, jako je číslo modelu vašeho bezdrátového adaptéru.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Řešení problémů s bezdrátovými sítěmi</title>
  <subtitle>Získání informací o vašem hardwaru</subtitle>

  <p>V tomto kroku sesbíráme informace o vašem bezdrátovém síťovém zařízení. Způsob, kterým můžete vyřešit většinu bezdrátových problémů závisí na typu a modelu bezdrátového adaptéru, takže si k tomu něco řekneme. Může se vám také hodit, když budete mít po ruce některé z věcí, které se dodávají s počítačem, jako je instalační disk s ovladači zařízení. Podívejte se, jestli ještě máte následující věci:</p>

  <list>
    <item>
      <p>Obal a návod k vašemu bezdrátovému zařízení (hlavně uživatelskou příručku k vašemu směrovači)</p>
    </item>
    <item>
      <p>Disk obsahující ovladače k vašemu bezdrátovému adaptéru (byť obsahuje jen ovladače pro Windows)</p>
    </item>
    <item>
      <p>Údaje o výrobci a číslu modelu vašeho počítače, bezdrátového adaptéru a přístupového bodu/směrovače. Tyto informace lze obvykle najít na štítku na spodní/zadní straně zařízení.</p>
    </item>
    <item>
      <p>Číslo verze/revize, které bývá vytištěné na bezdrátovém síťovém zařízení nebo jeho obalu. To je obzvlášť užitečné, takže pozorně hledejte.</p>
    </item>
    <item>
      <p>Cokoliv na disku s ovladači, co identifikuje jak zařízení samotné, tak jeho verzi firmwaru nebo použitých komponent (čipsetu).</p>
    </item>
  </list>

  <p>Pokud je to možné, zkuste získat přístup k nějakému jinému funkčnímu připojení k Internetu, abyste v případě potřeby mohli stáhnout software a ovladače. (Jednou z možností je zapojit počítač síťovým kabelem přímo do směrovače, ale to udělejte, až to bude potřeba.)</p>

  <p>Jakmile máte co nejvíce možných z těchto položek, klikněte na <gui>Následujicí</gui>.</p>

</page>
