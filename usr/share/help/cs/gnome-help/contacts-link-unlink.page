<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="cs">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zkombinovat údaje pro kontakt z více zdrojů.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Propojení a rozpojení kontaktů</title>

<section id="link-contacts">
  <title>Propojení kontaktů</title>

  <p>Můžete zkombinovat kontakty s místního adresáře a účtů on-line do jednoho společného záznamu v <app>Kontaktech</app>. Díky tomu můžete udržovat adresář přehledně se všemi údaji na jednom místě.</p>

  <steps>
    <item>
      <p>Zapněte <em>režim výběru</em> zmáčknutím tlačítka se zatrhnutím nad seznamem kontaktů.</p>
    </item>
    <item>
      <p>U každého kontaktu se objeví zaškrtávací políčko. U kontaktů, které chcete sloučit, políčko zaškrtněte.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Propojit</gui> vybrané kontakty propojíte.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Rozpojení kontaktů</title>

  <p>Pokud omylem propojíte kontakty, které propojené být nemají, můžete je zase rozpojit.</p>

  <steps>
    <item>
      <p>V seznamu kontaktů vyberte kontakt, který chcete rozpojit.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Upravit</gui> v pravém horním rohu aplikace <app>Kontakty</app>.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Propojené účty</gui>.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Rozpojit</gui> záznam v kontaktech rozpojíte.</p>
    </item>
    <item>
      <p>Po dokončení rozpojení okno zavřete.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Hotovo</gui> úpravy kontaktu dokončete.</p>
    </item>
  </steps>

</section>

</page>
