<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="cs">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak nastavit, kdy se mají pro soubory použít miniatury.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Předvolby náhledů ve správci souborů</title>

<p>Správce souborů vytváří miniatury s náhledy obrázků, videí a textových souborů. Miniatury s náhledy mohou být pomalé u velkých souborů nebo přes síť, takže máte možnost určit, kdy se mohou vytvářet. Klikněte na <gui>Soubory</gui> v horní liště, zvolte <gui>Předvolby</gui> a vyberte kartu <gui>Náhled</gui>.</p>

<terms>
  <item>
    <title><gui>Soubory</gui></title>
    <p>Ve výchozím nastavení budu mít miniatury vytvořené <gui>Jen místní soubory</gui>, které jsou na vašem počítači a připojených externích discích. Můžete tuto funkci nastavit také na <gui>Vždy</gui> nebo <gui>Nikdy</gui>. Správce souborů může <link xref="nautilus-connect">procházet soubory na jiných počítačích</link> přes místní síť nebo Internet. Pokud často procházíte soubory přes místní síť a ta má dostatečnou šířku pásma, můžete nastavit tvorbu miniatur na <gui>Vždy</gui>.</p>
    <p>Navíc můžete upravit nastavení <gui>Jen pro soubory menší než</gui>, abyste omezili velikost souborů, pro které se miniatury generují.</p>
  </item>
  <item>
    <title><gui>Složky</gui></title>
    <p>Když máte mezi <link xref="nautilus-list">sloupci seznamového zobrazení</link> nebo v <link xref="nautilus-display#icon-captions">popisech ikon</link> zobrazeny velikosti souborů, bude se u složek zobrazovat počet souborů a složek, které obsahuje. Spočítání položek ve složce může být pomalé, hlavně u rozsáhlých složek nebo přes síť. Proto můžete tuto funkci zapnout a vypnout, případně zapnout jen pro složky přímo na počítači nebo na místních externích discích.</p>
  </item>
</terms>
</page>
