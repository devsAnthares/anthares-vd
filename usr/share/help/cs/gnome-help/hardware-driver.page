<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="cs">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ovladače hardwaru/zařízení umožňují vašemu počítači používat zařízení, která jsou k němu připojená.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Co je to ovladač?</title>

<p>Zařízení jsou fyzické „části“ počítače. Mohou být <em>externí</em>, jako třeba tiskárna nebo monitor, nebo <em>interní</em>, jako třeba grafická karta nebo zvuková karta.</p>

<p>Aby systém v počítači dokázal tato zařízení používat, potřebuje vědět, jak s nimi komunikovat. Dělá to pomocí softwarových „částí“, nazývaných <em>ovladače zařízení</em>.</p>

<p>Když připojíte zařízení k počítači, musíte mít nainstalovaný správný ovladač, aby fungovalo. Například, když připojíte tiskárnu, ale nemáte k ní správný ovladač, nebudete ji moci používat. Normálně každý model zařízení používá jiný ovladač, který není zaměnitelný s ostatními.</p>

<p>V Linuxu je většina ovladačů ve výchozím stavu nainstalovaná, takže by mělo vše po připojení fungovat. Ale mohou nastat případy, kdy je ovladač potřeba nainstalovat ručně nebo kdy není k dispozici.</p>

<p>Navíc, některé existující ovladače jsou neúplné nebo částečně nefunkční. Například můžete zjistit, že některé tiskárně nejde tisknou oboustranně, ale vše ostatní funguje.</p>

</page>
