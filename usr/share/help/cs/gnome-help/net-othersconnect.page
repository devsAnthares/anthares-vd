<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="cs">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>U síťového připojení můžete uložit nastavení (včetně hesla), takže kdokoliv používá počítač, se bude moci k síti připojit.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Ostatní uživatelé se nemohou připojit k Internetu</title>

  <p>Když nastavíte síťové připojení, měli by všichni uživatelé počítače normálně mít možnost jej používat. Pokud informace o připojení nejsou sdílené, měli byste zkontrolovat nastavení připojení.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Síť</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Síť</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V seznamu nalevo vyberte <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na tlačítko <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">nastavení</span></media> otevřete podrobnosti o připojení.</p>
    </item>
    <item>
      <p>V panelu na levé straně vyberte <gui>Identita</gui>.</p>
    </item>
    <item>
      <p>V dolní části panelu <gui>Identita</gui> zaškrtněte volbu <gui>Zpřístupnit ostatním uživatelům</gui>, aby i ostatní uživatelné mohli používat toto síťové připojení.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Použít</gui> změny uložte.</p>
    </item>
  </steps>

  <p>Ostatní uživatelé tohoto počítače budou nyní moci používat toto připojení, aniž by museli zadávat nějaké údaje.</p>

  <note>
    <p>Kterýkoliv uživatel může toto nastavení změnit.</p>
  </note>

</page>
