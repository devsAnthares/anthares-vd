<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="cs">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Pár tipů k používání příručky k uživatelskému prostředí.</desc>
    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>O této příručce</title>
<p>Tato příručka je navržená, aby vás provedla funkcemi uživatelského prostředí, zodpověděla vaše otázky ohledně počítače a poskytla tipy, jak používat počítač efektivněji. Snažili jsem se udělat tuto příručku co nejjednodušší na použití:</p>

<list>
  <item><p>Tato příručka není dělená do souvislýck kapitol, ale je rozdělená do malých, tématicky orientovaných kapitol. To znamená, že se nemusíte prokousávat celou příručkou, abyste našli odpověď na svoji otázku.</p></item>
  <item><p>Související témata jsou spolu provázána. Odkazy „Další informace“ v dolní části stránky vedou přímo na související témata. Díky tomu snadno najdete podobná témata, které vám mohou pomoci provést požadovaný úkol.</p></item>
  <item><p>Součástí je zabudované hledání. Lišta v horní části prohlížeče nápovědy je <em>vyhledávací lišta</em> a související výsledky se začnou objevovat hned, jak začnete psát.</p></item>
  <item><p>Příručka je průběžně vylepšována. I když se vám snažíme poskytnout ucelenou sadu užitečných informací, je nám jasné, že zde nemůžeme zodpovědět všechny možné otázky. Přesto budeme přidávat nové informace, aby byla příručka ještě přínosnější.</p></item>
</list>

<p>Děkujeme, že jste si našli čas na přečtení nápovědy k uživatelskému prostředí. Přesto upřimně doufáme, že nápovědu nebudete nikdy potřebovat.</p>

<p>-- Dokumentační tým GNOME</p>
</page>
