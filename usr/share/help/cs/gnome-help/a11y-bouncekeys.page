<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="cs">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Jak ignorovat rychle se opakující zmáčknutí téže klávesy.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Zapnutí vícenásobných zmáčknutí kláves</title>

  <p><em>Vícenásobná zmáčknutí kláves</em> můžete zapnout, aby se ignorovala zmáčknutí, která se zopakují velmi rychle za sebou. Například, když máte třes rukou, způsobuje to více rychlých zmáčknutí klávesy po sobě, místo jednoho plynulého, takže byste v takovém případě měli funkci použít.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Univerzální přístup</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Psaní</gui> zmáčkněte <gui>Pomáhat s psaním</gui>.</p>
    </item>
    <item>
      <p>Přepněte vypínač <gui>Vícenásobná zmáčknutí kláves</gui> na <gui>❙</gui> (zapnuto).</p>
    </item>
  </steps>

  <note style="tip">
    <title>Rychlé zapnutí a vypnutí vícenásobných zmáčknutí kláves</title>
    <p>Vypnout nebo zapnout ignorování vícenásobných zmáčknutí kláves můžete kliknutím na <link xref="a11y-icon">ikonu zpřístupnění</link> na horní liště a následným výběrem položky <gui>Vícenásobná zmáčknutí kláves</gui>. Ikona zpřístupnění je viditelná, když je zapnuté aspoň jedno nastavení v panelu <gui>Univerzální přístup</gui>.</p>
  </note>

  <p>Použijte táhlo <gui>Zpoždění přijetí</gui> k změně, jak dlouho se má od prvního zmáčknutí čekat, než je považováno zmáčknutí za další samostatné. Vyberte <gui>Zvukové znamení při odmítnutí klávesy</gui>, pokud chcete, aby počítač vydal zvuk pokaždé, když klávesu ignoruje, protože byla zmáčknuta příliš krátce po předchozí.</p>

</page>
