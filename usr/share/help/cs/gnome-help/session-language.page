<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="cs">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak se přepnou na jiný jazyk pro uživatelské rozhraní a text nápovědy.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Změna jazyka, který používáte</title>

  <p>Aplikace a celé uživatelské prostředí můžete používat v tuctech národních jazyků, které jsem poskytovány skrze jazykové balíčky nainstalované v počítači.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Region a jazyk</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Region a jazyk</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Jazyk</gui>.</p>
    </item>
    <item>
      <p>Vyberte svůj požadovaný region a jazyk. Pokud váš region a jazyk nejsou na seznamu, klikněte na <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> ve spodní části seznamu, abyste si mohli vybrat ze všech dostupných regionů a jazyků.</p>
    </item>
    <item>
      <p>Kliknutím na <gui style="button">Hotovo</gui> nastavení uložíte.</p>
    </item>
    <item>
      <p>Na výzvu <gui>Aby se změny projevily, musí být sezení restartováno</gui> zareagujte kliknutím na <gui style="button">Restartovat nyní</gui> nebo kliknutím na <gui style="button">×</gui> odložte restart na později.</p>
    </item>
  </steps>

  <p>Některé překlady mohou být neúplné a některé aplikace nemusí podporovat váš jazyk vůbec. Nepřeložený text se pak objeví v jazyce, ve kterém byl software původně vyvíjen, obvykle americké angličtině.</p>

  <p>Existuje pár speciálních složek ve vaší domovské složce, do kterých mohou aplikace ukládat takové věci, jak jsou hudba, obrázky a dokumenty. Tyto složky používají standardní názvy ve vašem jazyce. Když se znovu přihlásíte, budete dotázáni, jestli chcete tyto složky přejmenovat na standardní názvy ve vašem jazyce. Pokud plánujete používat nový jazyk dlouhodobě, měli byste názvy složek nechat aktualizovat.</p>

  <note style="tip">
    <p>Pokud v systému existuje více uživatelských účtů, je v panelu <gui>Region a jazyk</gui> zvláštní část pro přihlašovací obrazovku. Kliknutím na tlačítko <gui>Přihlašovací obrazovka</gui> v pravém horním rohu se přepínáte mezi oběma nastaveními.</p>
  </note>

</page>
