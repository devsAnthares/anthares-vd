<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" id="processes" xml:lang="cs">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <desc>Které procesy můžete očekávat, že budou spuštěny v čistém sezení GNOME?</desc>
  </info>

  <title>Typické procesy</title>

  <p>Ve standardním sezení <app>GNOME</app> běží na pozadí systému programy nazývané <link href="man:daemon"><app>démoni</app></link>. Ve výchozím stavu můžete najít běžící tyto démony:</p>

   <terms>
     <item>
       <title>dbus-daemon</title>
       <p>Démon <app>dbus-daemon</app> poskytuje službu sběrnice zpráv, kterou mohou programy využít k vzájemné výměně zpráv. Je implementován pomocí knihovny D-Bus, která poskytuje komunikaci jedna ku jedné mezi dvěma aplikacemi.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:dbus-daemon">dbus-daemon</link>.</p>
     </item>
     <item>
       <title>gnome-keyring-daemon</title>
       <p>Ověřovací údaje, jako jsou jméno a heslo, pro různé programy a webové stránky se uchovávají v bezpečí pomocí démona <app>gnome-keyring-daemon</app>. Údaje jsou zapsané v zašifrovaném souboru nazývaném klíčenka a uloženém v domovské složce uživatele.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:gnome-keyring-daemon">gnome-keyring-daemon</link>.</p>
     </item>
     <item>
       <title>gnome-session</title>
       <p>Program <app>gnome-session</app> zodpovídá za běh pracovního prostředí GNOME, spolu s pomocí správce zobrazení, jako <app>GDM</app>, <app>LightDM</app> nebo <app>NODM</app>. Výchozí sezení pro uživatele je nastaveno správcem systému v průběhu instalace systému. <app>gnome-session</app> typicky načte poslední sezení, které na systému úspěšně fungovalo.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:gnome-session">gnome-session</link>.</p>
     </item>
     <item>
       <title>gnome-settings-daemon</title>
       <p>Démon <app>gnome-settings-daemon</app> se stará o nastavení pro sezení GNOME a pro všechny programy, které v sezení běží.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:gnome-settings-daemon">gnome-settings-daemon</link>.</p>
     </item>
     <item>
       <title>gnome-shell</title>
       <p><app>gnome-shell</app> poskytuje základní funkcionalitu uživatelského rozhraní GNOME, jako je spouštění programů, procházení složek, zobrazování souborů apod.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:gnome-shell">gnome-shell</link>.</p>
     </item>
     <item>
       <title>pulseaudio</title>
       <p><app>PulseAudio</app> je zvukový server pro systémy Linux, POSIX a Windows. Zpřístupňuje programům zvukový výstup skrz démona <app>pulseaudio</app>.</p>
       <p>Další informace najdete v manuálové stránce <link href="man:pulseaudio">pulseaudio</link>.</p>
     </item>
   </terms>

  <p>V závislosti na tom, co má uživatel nastavené, můžete, mimo mnoha jiných, vidět také některé z následujících procesů:</p>
   <list ui:expanded="false">
   <title>Další procesy</title>
     <item><p><app>at-spi2-dbus-launcher</app></p></item>
     <item><p><app>at-spi2-registryd</app></p></item>
     <item><p><app>gnome-screensaver</app></p></item>
     <item><p><app>gnome-shell-calendar-server</app></p></item>
     <item><p><app>goa-daemon</app></p></item>
     <item><p><app>gsd-printer</app></p></item>
     <item><p>různé procesy aplikace <app>Evolution</app></p></item>
     <item><p>různé procesy <app>GVFS</app></p></item>
   </list>

</page>
