<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom-user" xml:lang="cs">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  </info>

    <title>Přidání vlastních typů MIME jednotlivým uživatelům</title>
    <p>Když chcete přidat vlastní typ MIME pro jednotlivé uživatele a zaregistrovat pro tento typ MIME aplikaci, musíte vytvořit soubor se specifikací nového typu MIME ve složce <file>~/.local/share/mime/packages/</file> a soubor <file>.desktop</file> ve složce <file>~/.local/share/applications/</file>.</p>
    <steps>
      <title>Přidání vlastního typu MIME <code>application/x-newtype</code> jednotlivým uživatelům</title>
      <item>
        <p>Vytvořte soubor <file>~/.local/share/mime/packages/application-x-newtype.xml</file>:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>Ukázkový soubor <file>application-x-newtype.xml</file> výše definuje nový typ MIME <sys>application/x-newtype</sys> a přiřadí k němu soubory s příponou <file>.xyz</file>.</p>
      </item>
      <item>
        <p>Vytvořte nový soubor <file>.desktop</file> nazvaný například <file>mojeaplikace1.desktop</file> a umístěte jej do složky <file>~/.local/share/applications/</file>:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>Moje aplikace 1</var>
Exec=<var>mojeaplikace1</var></code>
      <p>Ukázkový soubor <file>mojeaplikace1.desktop</file> výše přiřazuje k typu MIME <code>application/x-newtype</code> aplikaci nazvanou <app>Moje aplikace 1</app>, která se spouští příkazem <cmd>mojeaplikace1</cmd>.</p>
      </item>
      <item>
        <p>Aktualizujte databázi MIME, aby se změny projevily:</p>
        <screen><output>$ </output><input>update-mime-database ~/.local/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Aktualizujte databázi aplikací:</p>
        <screen><output>$ </output><input>update-desktop-database ~/.local/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Abyste si mohli ověřit, že přidružení souborů <file>*.xyz</file> k typu MIME <sys>application/x-newtype</sys> bylo úspěšné, vytvořte nejprve prázdný soubor, například <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Spusťte příkaz <cmd>gio info</cmd>:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Když chcete ověřit, že soubor <file>mojeaplikace1.desktop</file> byl správně nastaven jako výchozí zaregistrovaná aplikace pro typ MIME <sys>application/x-newtype</sys>, spusťte příkaz <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Výchozí aplikace pro „application/x-newtype“: mojeaplikace1.desktop
Registrované aplikace:
	mojeaplikace1.desktop
Doporučené aplikace:
	mojeaplikace1.desktop</screen>
      </item>
    </steps>
</page>
