<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="cs">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  </info>

    <title>Přepsání výchozích registrovaných aplikací pro jednotlivé uživatele</title>
    <p>Soubor <file>/usr/share/applications/defaults.list</file> určuje, která aplikace je zaregistrovaná k otevírání konkrétních typů MIME jako výchozí. Abyste přepsali výchozí systémové přiřazení pro jednotlivé uživatele, musíte vytvořit soubor <file>~/.local/share/applications/mimeapps.list</file> se seznamem typů MIME, pro které chcete zaregistrované výchozí aplikace přepsat.</p>
    <steps>
      <title>Přepsání výchozích registrovaných aplikací pro jednotlivé uživatele</title>
      <item>
        <p>Podívejte se do souboru <file>/usr/share/applications/defaults.list</file> po typu MIME, pro který chcete změnit zaregistrovanou výchozí aplikaci. Například, následující ukázka souboru <file>vychozi.list</file> určuje zaregistrované výchozí aplikace pro typy MIME <code>text/html</code> a <code>application/xhtml+xml</code>.</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Výchozí aplikace (<app>Epiphany</app>) je definovaná zadáním příslušného souboru <file>.desktop</file> (<file>epiphany.desktop</file>). Výchozí systémové umístění pro soubory <file>.desktop</file> ostatních aplikací je <file>/usr/share/applications/</file>. Soubory <file>.desktop</file> jednotlivých uživatelů je možné uchovávat v <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Vytvořte soubor <file>~/.local/share/applications/mimeapps.list</file>. V něm určete typy MIME a k nim příslušné výchozí zaregistrované aplikace:</p>
        <code>[Default Applications]
text/html=<var>mojeaplikace1.desktop</var>
application/xhtml+xml=<var>mojeaplikace2.desktop</var>

[Added Associations]
text/html=<var>mojeaplikace1.desktop</var>;
application/xhtml+xml=<var>mojeaplikace2.desktop</var>;</code>
      <p>Tímto se nastaví zaregistrovaná výchozí aplikace pro typ MIME <code>text/html</code> na <code>mojeaplikace1.desktop</code> a zaregistrovaná výchozí aplikace pro typ MIME <code>application/xhtml+xml</code> na <code>mojeaplikace2.desktop</code>.</p>
        <p>Aby tato nastavení fungovala správně, je zapotřebí, aby oba soubory <file>mojealikace1.desktop</file> a <file>mojealikace2.desktop</file> byly umístěné ve složce <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Pro ověření, jestli se výchozí zaregistrované aplikace nastavily správně, můžete použít příkaz <cmd>gio mime</cmd>:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Výchozí aplikace pro „text/html“: mojeaplikace1.desktop
Registrované aplikace:
	mojeaplikace1.desktop
	epiphany.desktop
Doporučené aplikace:
	mojeaplikace1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
