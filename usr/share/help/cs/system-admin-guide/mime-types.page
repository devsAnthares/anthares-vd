<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="mime-types" xml:lang="cs">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Typy MIME se používají k identifikaci souborových formátů.</desc>
  </info>

    <title>Co jsou to typy MIME?</title>
    <p>V GNOME typy MIME (<em>Multipurpose Internet Mail Extensions</em> – víceúčelová rozšíření internetové pošty) složí k určení formátu souborů. Uživatelské prostředí GNOME používá typy MIME k:</p>
    <list>
      <item>
        <p>určení, která aplikace má jako výchozí otevírat ten který formát</p>
      </item>
      <item>
        <p>registraci ostatních aplikací, které také mohou otevírat daný formát</p>
      </item>
      <item>
        <p>poskytnutí textu popisujícího konkrétní typ souboru (například v dialogovém okně s vlastnostmi souboru v aplikaci <app>Soubory</app>)</p>
      </item>
      <item>
        <p>poskytnutí ikony reprezentující konkrétní souborový formát (například ve výpisu souborů v aplikaci <app>Soubory</app>)</p>
      </item>
    </list>
    <p>Názvy typů MIME mají následující formát:</p>
<screen>
<var>typ-média</var>/<var>identifikátor-podtypu</var>
</screen>
<p><sys>image/jpeg</sys> je příkladem typu MIME, kde <sys>image</sys> je typ média (zde obrázek) a <sys>jpeg</sys> je identifikátor podtypu (zde formát JPEG).</p>
    <p>GNOME se následovně drží specifikace <em>freedesktop.org Shared MIME Info</em> v těchto věcech:</p>
    <list>
    <item>
      <p>umístění souborů se specifikacemi typů MIME pro celý systém a jednotlivé uživatele</p>
    </item>
    <item>
      <p>způsobu registrace typů MIME, aby uživatelské prostředí znalo aplikace, které se mají použít pro otevírání konkrétních formátů</p>
    </item>
    <item>
      <p>způsobu uživatelské změny aplikací používaných pro otevírání jednotlivých formátů</p>
    </item>
    </list>
    <section id="mime-database">
      <title>Co je to databáze MIME?</title>
      <p>Databáze MIME je kolekce všech typů MIME specifikujích soubory, kterou GNOME používá k uchování informací o známých typech MIME.</p>
      <p>Nejdůležitější částí databáze MIME z pohledu správce systému je složka <file>/usr/share/mime/packages/</file>, ve které se uchovávají soubory s informacemi o známých typech MIME. Příkladem takového souboru je třeba <file>/usr/share/mime/packages/freedesktop.org.xml</file> s informace o standardních typech MIME dostupných v systému jako výchozí. Tento soubor pochází z balíčku <sys>shared-mime-info</sys>.</p>
    </section>
    <section id="mime-types-more-information">
    <title>Získání dalších informací</title>
    <p>Podrobné informace popisující systém typů MIME najdete ve <em>specifikaci Shared MIME Info od freedesktop.org</em>, která je k dispozici na webu freedesktop.org:</p>
    <list>
      <item>
        <p><link href="http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/"> http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/</link> (odkazovaný text je v angličtině)</p>
      </item>
    </list>
    </section>
</page>
