<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="it">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Il menù accesso universale è l'icona sulla barra superiore che ha l'aspetto di una persona.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Trovare il menù accesso universale</title>

  <p>The <em>universal access menu</em> is where you can turn on some of the
  accessibility settings. You can find this menu by clicking the icon which
  looks like a person surrounded by a circle on the top bar.</p>

  <figure>
    <desc>Il menù accesso universale è disponibile nella barra superiore.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>If you do not see the universal access menu, you can enable it from the
  <gui>Universal Access</gui> settings panel:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch <gui>Always Show Universal Access Menu</gui> to
      <gui>ON</gui>.</p>
    </item>
  </steps>

  <p>To access this menu using the keyboard rather than the mouse, press
  <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> to move the
  keyboard focus to the top bar. A white line will appear underneath the
  <gui>Activities</gui> button — this tells you which item on the top bar is
  selected. Use the arrow keys on the keyboard to move the white line under the
  universal access menu icon and then press <key>Enter</key> to open it. You
  can use the up and down arrow keys to select items in the menu. Press
  <key>Enter</key> to toggle the selected item.</p>

</page>
