<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="it">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rimuovere file e cartelle non più necessarie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Eliminare file e cartelle</title>

  <p>If you do not want a file or folder any more, you can delete it. When you
  delete an item it is moved to the <gui>Trash</gui> folder, where it is stored
  until you empty the trash. You can <link xref="files-recover">restore
  items</link> in the <gui>Trash</gui> folder to their original location if you
  decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>Per spostare un file nel cestino:</title>
    <item><p>Selezionare il file da mettere nel cestino facendoci clic una sola volta.</p></item>
    <item><p>Press <key>Delete</key> on your keyboard. Alternatively, drag the
    item to the <gui>Trash</gui> in the sidebar.</p></item>
  </steps>

  <p>The file will be moved to the trash, and you’ll be presented with an
  option to <gui>Undo</gui> the deletion. The <gui>Undo</gui> button will appear
  for a few seconds. If you select <gui>Undo</gui>, the file will be restored
  to its original location.</p>

  <p>Per eliminare definitivamente i file, e liberare spazio sul computer, è necessario svuotare il cestino. Per svuotare il cestino, fare clic con il pulsante destro del mouse sul <gui>Cestino</gui> nel riquadro laterale e selezionare <gui>Svuota cestino</gui>.</p>

  <section id="permanent">
    <title>Eliminare definitivamente un file</title>
    <p>È possibile eliminare immediatamente un file in maniera definitiva, senza doverlo spostare prima nel cestino.</p>

  <steps>
    <title>Per eliminare definitivamente un file:</title>
    <item><p>Selezionare l'elemento da eliminare.</p></item>
    <item><p>Premere e mantenere premuto il tasto <key>Maiusc</key>, quindi premere <key>Canc</key> sulla tastiera.</p></item>
    <item><p>Dato che l'azione non può essere annullata, verrà richiesta una conferma prima dell'eliminazione del file o della cartella.</p></item>
  </steps>

  <note><p>I file eliminati da un <link xref="files#removable">dispositivo rimovibile</link> potrebbero non essere visibili utilizzando altri sistemi operativi, come Windows o Mac OS. I file sono ancora presenti e saranno disponibili al successivo utilizzo del dispositivo in ambiente Linux.</p></note>

  </section>

</page>
