<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="it">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Enable or disable geolocation.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Control location services</title>

  <p>Geolocation, or location services, uses cell tower positioning, GPS, and
  nearby Wi-Fi access points to determine your current location for use in
  setting your timezone and by applications such as <app>Maps</app>. When
  enabled, it is possible for your location to be shared over the network with
  a great deal of precision.</p>

  <steps>
    <title>Turn off the geolocation features of your desktop</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Privacy</gui> to open the panel.</p>
    </item>
    <item>
     <p>Set the <gui>Location Services</gui> switch to <gui>OFF</gui>.</p>
     <p>To re-enable this feature, set the <gui>Location Services</gui> switch
     to <gui>ON</gui>.</p>
    </item>
  </steps>

</page>
