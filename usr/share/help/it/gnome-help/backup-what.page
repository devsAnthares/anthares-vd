<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="it">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up anything that you cannot bear to lose if something goes
    wrong.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Dati di cui eseguire il backup</title>

  <p>È utile effettuare il backup dei <link xref="backup-thinkabout">file più importanti</link> e di quelli difficili da ricreare. Di seguito vengono riportati alcuni esempi in ordine d'importanza:</p>

<terms>
 <item>
  <title>File personali</title>
   <p>Questo comprende documenti, fogli elettronici, email, appuntamenti sul calendario, dati finanziari, foto di famiglia o qualsiasi altro file personale che si ritiene insostituibile.</p>
 </item>

 <item>
  <title>Impostazioni personali</title>
   <p>Queste comprendono le modifiche effettuate ai colori, agli sfondi, alla risoluzione dello schermo e alle impostazioni del mouse; le impostazioni di applicazioni come <app>LibreOffice</app>,  il riproduttore musicale e il programma di posta elettronica. Creare di nuovo questi file, malgrado siano sostituibili, potrebbe richiedere del tempo.</p>
 </item>

 <item>
  <title>Impostazioni di sistema</title>
   <p>La maggior parte degli utenti non modifica le impostazioni di sistema definite in fase d'installazione del sistema operativo, ma nel caso in cui queste siano state modificate oppure si utilizzi il computer come un server, allora è utile eseguirne il backup.</p>
 </item>

 <item>
  <title>Software installato</title>
   <p>Di norma, dopo un serio problema al computer, è possibile ripristinare Il software in uso piuttosto velocemente reinstallandolo.</p>
 </item>
</terms>

  <p>In generale, eseguire il backup di file insostituibili o che richiederebbero un grande impiego di tempo per poter essere ripristinati senza l'ausilio di un backup. Nel caso i file siano facilmente sostituibili, invece, non è necessario occupare ulteriore spazio su disco conservandone i backup.</p>

</page>
