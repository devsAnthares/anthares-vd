<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="it">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Si stanno scaricando altri elementi, la connessione è scadente o la linea è congestionata.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>La connessione Internet sembra lenta</title>

  <p>Se la connessione a Internet sembra lenta, ciò può essere dovuto a diverse cause.</p>

  <p>Provare a chiudere il browser e a riaprirlo, a terminare la connessione e ristabilirla nuovamente (in tale maniera vengono ripristinati molti elementi che potrebbero causare il rallentamento).</p>

  <list>
    <item>
      <p><em style="strong">Linea congestionata</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">Scaricare molti elementi contemporaneamente</em></p>
      <p>Se coloro che usano la connessione Internet stanno scaricando diversi file contemporaneamente, o guardando video, la connessione Internet potrebbe non essere abbastanza veloce da soddisfare la richiesta. In questo caso, potrebbe sembrare più lenta.</p>
    </item>
    <item>
      <p><em style="strong">Connessione inaffidabile</em></p>
      <p>Alcune connessioni Internet sono semplicemente inaffidabili, specialmente quelle temporanee o in aree a elevata richiesta. Se ci si trova in un bar affollato o in un centro congressi, la connessione Internet potrebbe essere congestionata o semplicemente inaffidabile.</p>
    </item>
    <item>
      <p><em style="strong">Bassa qualità del segnale della connessione senza fili</em></p>
      <p>If you’re connected to the internet by wireless (wifi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">Connessione mobile a Internet lenta</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Il browser ha un problema</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
