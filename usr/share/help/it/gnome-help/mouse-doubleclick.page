<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="it">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controllare quanto rapidamente è necessario premere il pulsante del mouse una seconda volta per ottenere un doppio-clic.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Regolare la velocità del doppio-clic</title>

  <p>Double-clicking only happens when you press the mouse button twice
  quickly enough. If the second press is too long after the first, you’ll
  just get two separate clicks, not a double click. If you have difficulty
  pressing the mouse button quickly, you should increase the timeout.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Under <gui>Pointing &amp; Clicking</gui>, adjust the
      <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>Se il mouse effettua un doppio-clic invece che un clic singolo anche dopo aver aumentato l'intervallo di tempo a disposizione, ciò potrebbe indicare che il mouse sia difettoso. Provare a collegare un altro mouse al computer e verificare il corretto funzionamento. In alternativa, è possibile collegare il mouse a un altro computer per vedere se presenta il medesimo problema.</p>

  <note>
    <p>Questa impostazione ha effetti sul mouse, sul touchpad e su ogni altro dispositivo di puntamento.</p>
  </note>

</page>
