<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-where" xml:lang="it">

  <info>
    <link type="guide" xref="backup-why"/>
    <title type="sort">c</title>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dove salvare i backup e che tipo di dispositivo di archiviazione usare.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Dove salvare i backup</title>

  <p>You should store backup copies of your files somewhere separate from your
 computer — on an external hard disk, for example. That way, if the computer
 breaks, the backup will still be intact. For maximum security, you shouldn’t
 keep the backup in the same building as your computer. If there is a fire or
 theft, both copies of the data could be lost if they are kept together.</p>

  <p>È importante scegliere, inoltre, un appropriato <em>dispositivo di backup</em> in quanto sarà necessario salvare le proprie copie dei dati su un dispositivo che abbia una capacità di memoria sufficiente per tutti i file di cui si è eseguito il backup.</p>

   <list style="compact">
    <title>Opzioni di archiviazione locale e remota</title>
    <item>
      <p>Chiavetta USB (capacità limitata)</p>
    </item>
    <item>
      <p>Disco fisso interno (capacità elevata)</p>
    </item>
    <item>
      <p>Disco fisso esterno (generalmente capacità elevata)</p>
    </item>
    <item>
      <p>Unità di rete (capacità elevata)</p>
    </item>
    <item>
      <p>Server di file o backup (capacità elevata)</p>
    </item>
    <item>
     <p>CD o DVD scrivibili (capacità limitata o media)</p>
    </item>
    <item>
     <p>Online backup service
     (<link href="http://aws.amazon.com/s3/">Amazon S3</link>, for example;
     capacity depends on price)</p>
    </item>
   </list>

  <p>Alcune di queste opzioni dispongono di una capacità sufficiente a permettere di eseguire il backup di ogni file del sistema, ovvero di eseguire un <em>backup completo di sistema</em>.</p>
</page>
