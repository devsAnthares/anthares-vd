<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-menubar" xml:lang="es">

  <info>
    <revision version="0.1" date="2013-02-21" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref#global"/>
    <link type="seealso" xref="pref-keyboard-access"/>
    <link type="seealso" xref="app-fullscreen"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ocultar y restaurar la barra de menús.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodrigo Lledó</mal:name>
      <mal:email>rodhos92@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Javier F. Serrador</mal:name>
      <mal:email>serrador@gnome.org</mal:email>
      <mal:years>2001-2006</mal:years>
    </mal:credit>
  </info>

  <title>Visibilidad de la barra de menú</title>

  <p>Puede activar o desactivar la barra de menú si quiere. Esto puede ser útil si tiene limitado el espacio en la pantalla. Para ocultar la barra de menú:</p>

  <steps>
    <item>
      <p>Seleccione <gui style="menu">Ver</gui> y deseleccione <gui style="menuitem">Mostrar barra de menú</gui>.</p>
    </item>
  </steps>

  <p>Para restaurar la barra de menús:</p>

  <steps>
    <item>
      <p>Pulse con el botón derecho en la <app>Terminal</app> y elija <gui style="menuitem">Mostrar barra de menú</gui>.</p>
    </item>
  </steps>

  <p>Para activar o desactivar la barra de menú de manera predeterminada en todas las ventanas de la <app>Terminal</app> que abra:</p>

  <steps>
    <item>
      <p>Seleccione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferencias</gui> <gui style="tab">General</gui></guiseq>.</p>
    </item>
    <item>
      <p>Seleccione o deseleccione <gui>Mostrar la barra de menú en las terminales nuevas de manera predeterminada</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Puede <link xref="adv-keyboard-shortcuts">establecer un atajo de teclado</link> para mostrar y ocultar la barra de menú.</p>
  </note>

</page>
