<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="es">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar su equipo para que se suspenda automáticamente</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Configurar la suspensión automática</title>

  <p>Puede configurar su equipo para que se suspenda automáticamente cuando esté inactivo. Se pueden definir diferentes intervalos para cuando se trabaja con batería o enchufado.</p>

  <steps>

    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Energía</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Energía</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la sección <gui>Botón de apagar y suspender</gui>, pulse en <gui>Suspender automáticamente</gui>.</p>
    </item>
    <item>
      <p>Elija <gui>Usando batería</gui> o <gui>Enchufado</gui>, <gui>Active</gui> el interruptor, y elija un <gui>Retardo</gui>. Se pueden configurar ambas opciones.</p>

      <note style="tip">
        <p>En un equipo de escritorio, hay una opción etiquetada como <gui>Cuando esté inactivo</gui>.</p>
      </note>
    </item>

  </steps>

</page>
