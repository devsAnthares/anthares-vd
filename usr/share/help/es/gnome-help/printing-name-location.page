<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="es">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambiar el nombre o la ubicación de una impresora en las opciones de impresión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>
  
  <title>Cambiar el nombre o la ubicación de una impresora</title>

  <p>Puede cambiar el nombre o la ubicación de una impresora en la configuración de la impresora.</p>

  <note>
    <p>Necesita <link xref="user-admin-explain">privilegios de administrador</link> del sistema para cambiar el nombre o la ubicación de una impresora.</p>
  </note>

  <section id="printer-name-change">
    <title>Cambiar el nombre de la impresora</title>

  <p>Si quiere cambiar el nombre de una impresora, siga estos pasos:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Impresoras</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse el botón <gui>Desbloquear</gui> en la esquina superior derecha e introduzca su contraseña.</p>
    </item>
    <item>
      <p>Pulse sobre el nombre de su impresora y empiece a escribir el nuevo nombre de la impresora.</p>
    </item>
    <item>
      <p>Pulse <key>Intro</key> para guardar los cambios.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Cambiar la ubicación de la impresora</title>

  <p>Para cambiar la ubicación de su impresora:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Impresoras</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Pulse el botón <gui>Desbloquear</gui> en la esquina superior derecha e introduzca su contraseña.</p>
    </item>
    <item>
      <p>Pulse sobre la ubicación, y empiece a editarla.</p>
    </item>
    <item>
      <p>Pulse <key>Intro</key> para guardar los cambios.</p>
    </item>
  </steps>

  </section>

</page>
