<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="es">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar el inicio de sesión automático para cuando enciende el equipo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Iniciar sesión automáticamente</title>

  <p>Puede cambiar su configuración para que se inicie la sesión automáticamente con su cuenta cuando arranque el equipo:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Usuarios</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Usuarios</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Seleccione la cuenta de usuario con la que quiere iniciar la sesión automáticamente al inicio.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Desbloquear</gui> e introduzca su contraseña.</p>
    </item>
    <item>
      <p>Active la opción <gui>Inicio de sesión automático</gui> (<gui>Encendido</gui>).</p>
    </item>
  </steps>

  <p>La próxima vez que arranque su equipo, se iniciará la sesión automáticamente. Si tiene esta opción activada, no será necesario que escriba su contraseña para iniciar la sesión, lo que significa que si alguien arranca su equipo, podrá acceder a su cuenta y a sus datos personales, incluyendo sus archivos y su histórico de navegación.</p>

  <note>
    <p>Si su cuenta es de tipo <em>Estándar</em>, no podrá cambiar esta configuración; el administrador del sistema puede cambiar esto en su lugar.</p>
  </note>

</page>
