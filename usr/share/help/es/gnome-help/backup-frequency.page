<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="es">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aprenda con qué frecuencia debería respaldar sus archivos importantes para asegurarse de que están a buen recaudo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Frecuencia de las copias de respaldo</title>

  <p>La frecuencia con la que debe hacer copias de respaldo dependerá del tipo de datos que vaya a respaldar. Por ejemplo, si está trabajando en un entorno de red con datos críticos almacenados en sus servidores, es posible que ni siquiera resulte suficiente hacer una copia de respaldo todas las noches.</p>

  <p>Por otro lado, si está haciendo la copia de respaldo de los datos en su equipo personal entonces los respaldos cada hora son innecesarios. Puede que considere de ayuda los siguientes puntos al planificar un respaldo:</p>

<list style="compact">
<item><p>La cantidad de tiempo que pasa con su equipo.</p></item>
<item><p>Con qué frecuencia y en qué medida cambian los datos de su equipo.</p></item>
</list>

  <p>Si los datos que quiere respaldar son de poca importancia, o están sujetos a pocos cambios, como música, correos electrónicos y fotos de familia, hacer copias semanales o incluso mensuales puede ser suficiente. No obstante, si está en mitad de una auditoría fiscal, puede que sea necesario hacer copias más frecuentes.</p>

  <p>Como regla general, la cantidad de tiempo entre copias de respaldo no debe ser mayor que la cantidad de tiempo que podría estar parado rehaciendo cualquier trabajo perdido. Por ejemplo, si emplear una semana escribiendo documentos perdidos es demasiado, debería hacer una copia de respaldo al menos una vez por semana.</p>

</page>
