<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="es">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Use un micrófono analógico o USB y seleccione un dispositivo de entrada predeterminado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Usar un micrófono diferente</title>

  <p>Puede utilizar un micrófono externo para charlar con amigos, hablar con colegas del trabajo, hacer grabaciones de voz, o usar otras aplicaciones multimedia. Incluso si su equipo tiene un micrófono o una cámara web con micrófono, un micrófono separado podría proporcionar una mejor calidad de sonido.</p>

  <p>Si su micrófono tiene un conector circular, enchúfelo en el conector adecuado de su equipo. La mayoría de los equipos tienen dos conectores: uno para micrófonos y uno para los altavoces. Normalmente, este adaptador es de color rosa o tiene una imagen de un micrófono junto a él. Los micrófonos enchufados en el adaptador apropiado se usan de manera predeterminada. Si no es así, vea a continuación las instrucciones para la selección de un dispositivo de entrada predeterminado.</p>

  <p>Si tiene un micrófono USB, enchúfelo en cualquier puerto USB de su equipo. Los micrófonos USB actúan como dispositivos de sonido separados, y puede tener que especificar qué micrófono usar de manera predeterminada.</p>

  <steps>
    <title>Seleccione el dispositivo de entrada de sonido predeterminado</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Sonido</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Sonido</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la pestaña <gui>Entrada</gui>, seleccione el dispositivo que quiere usar. El indicador del nivel de entrada debería responder cuando hable.</p>
    </item>
  </steps>

  <p>Puede ajustar el volumen y apagar el micrófono desde este panel.</p>

</page>
