<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="es">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Puede que no tenga los códecs adecuados instalados, o el DVD podría ser de una región incorrecta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Por qué no puedo reproducir DVD?</title>

  <p>Si inserta un DVD en su equipo y no se reproduce, puede que no tenga instalados los <em>códecs</em> del DVD adecuados, o el DVD podría ser de una <em>región</em> diferente.</p>

<section id="codecs">
  <title>Instalar los códecs adecuados para reproducir DVD</title>

  <p>Para reproducir DVD, necesita tener los <em>códecs</em> adecuados instalados. Un códec es un software que permite a las aplicaciones leer un formato de vídeo o de sonido. Si su software reproductor de películas no puede encontrar los códecs adecuados, debe ofrecerle la posibilidad de instalarlos por usted. Si esto no sucede, tendrá que instalar los códecs manualmente; pida ayuda sobre cómo hacer esto usando los foros de ayuda de su distribución de Linux.</p>

  <p>Los DVD también están <em>protegidos contra copia</em> usando un sistema llamado CSS. Esto evita que se puedan copiar los DVD, pero también evita que se puedan reproducir a menos que tenga un software adicional para manejar la protección anticopia. Puede comprar un decodificador de DVD comercial que maneje la protección anticopia en <link href="http://fluendo.com/shop/product/oneplay-dvd-player/">Fluendo</link>. Funciona con Linux y su uso debería ser legal en todos los países.</p>

  </section>

<section id="region">
  <title>Comprobar la región del DVD</title>

  <p>Los DVD tienen un <em>código de región</em> que indica en qué región del mundo puede reproducir el DVD. Si la región del reproductor de DVD de su equipo no coincide con la región del DVD que intenta reproducir, no podrá reproducirlo. Por ejemplo, si tiene un reproductor DVD de la región 1, solo podrá reproducir DVD de Norteamérica.</p>

  <p>A menudo es posible cambiar la región de su reproductor de DVD, pero esto sólo se puede algunas veces antes de que se bloquee en una región permanentemente. Para cambiar la región del reproductor de DVD de su equipo, use <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>Puede encontrar <link href="http://es.wikipedia.org/wiki/C%C3%B3digos_regionales_DVD">más información sobre los códigos de región de DVD en la Wikipedia</link>.</p>

</section>

</page>
