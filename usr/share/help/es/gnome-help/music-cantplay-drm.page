<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="es">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Puede que no esté instalado el soporte para ese formato de archivo o que las canciones estén «protegidas contra copia».</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>No puedo reproducir las canciones que he comprado en una tienda de música en línea</title>

<p>Si descarga música de una tienda en línea, es posible que no pueda reproducirla en su equipo, especialmente si la compró desde un equipo Windows o Mac OS y luego la copió.</p>

<p>Esto se debe a que la música está en un formato que su equipo no reconoce. Para poder reproducir una canción necesita tener instalado el soporte para el formato de sonido adecuado, por ejemplo, si quiere reproducir archivos MP3, necesitará tener instalado el soporte para MP3. Si no tiene el soporte para un formato de archivos concreto, verá un mensaje indicándoselo cuando intente reproducir una canción. El mensaje además le dará instrucciones sobre cómo instalar el soporte para ese formato de forma que pueda reproducirlo.</p>

<p>Si tiene instalado el soporte para el formato de sonido de la canción, pero aún no la puede reproducir la canción podría tener <em>protección anticopia</em> (también conocido como <em>restricción DRM</em>). DRM es una manera de restringir quién puede reproducir una canción y en qué dispositivos. La empresa que le vendió la canción tiene el control, no usted. Si un archivo de música tiene restricciones DRM, probablemente no será capaz de reproducirlo, por lo general necesitará un software especial del proveedor de DRM para reproducir archivos restringidos, pero pocos de ellos son compatibles con Linux.</p>

<p>Puede aprender más sobre el DRM en la <link href="http://www.eff.org/issues/drm">Electronic Frontier Foundation</link>.</p>

</page>
