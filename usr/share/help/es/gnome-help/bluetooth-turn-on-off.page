<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="es">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activar o desactivar el dispositivo Bluetooth de su equipo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Encender o apagar el Bluetooth</title>

  <p>Puede activar su Bluetooth para usar dispositivos Bluetooth y enviar y recibir archivos, o apagarlo para ahorrar energía. Para activar el Bluetooth:</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Bluetooth</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Mueva el deslizador de la parte superior a <gui>Encendido</gui>.</p>
    </item>
  </steps>

  <p>Muchos portátiles tienen un interruptor físico o una combinación de teclas para encender y apagar el Bluetooth. Busque un interruptor en su equipo o una tecla en su teclado. La tecla suele ir asociada a la tecla <key>Fn</key>.</p>

  <p>Para apagar el Bluetooth:</p>
  <steps>
    <item>
      <p>Abra el <gui xref="shell-introduction#yourname">menú del sistema</gui> en la parte derecha de la barra superior.</p>
    </item>
    <item>
      <p>Seleccione <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> No está en uso</gui>. La sección Bluetooth del menú se expandirá.</p>
    </item>
    <item>
      <p>Seleccione <gui>Apagar</gui>.</p>
    </item>
  </steps>

  <note><p>El equipo es <link xref="bluetooth-visibility">visible</link> mientras el panel de <gui>Bluetooth</gui> esté abierto.</p></note>

</page>
