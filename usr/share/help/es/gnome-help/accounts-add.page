<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="es">

  <info>
    <link type="guide" xref="accounts" group="add"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.9.92" date="2012-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permitir a las aplicaciones acceder a sus cuentas en línea para fotos, contactos, calendarios y más.</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Añadir una cuenta</title>

  <p>Añadir una cuenta le ayudará a enlazar sus cuentas en línea con su escritorio GNOME. Por lo tanto, su programas de correo-e, chat y otras aplicaciones relacionadas estarán configurados.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Cuentas en línea</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Cuentas en línea</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Seleccione una cuenta de la lista de la derecha.</p>
    </item>
    <item>
      <p>Seleccione la cuenta que quiere añadir.</p>
    </item>
    <item>
      <p>Se abrirá una pequeña ventana o diálogo con un sitio web donde podrá introducir las credenciales de la cuenta en línea. Si está configurando una cuenta de Google, introduzca su nombre de usuario y contraseña de Google. Algunos proveedores le permite crear una cuenta desde el diálogo de inicio de sesión.</p>
    </item>
    <item>
      <p>Si ha introducido sus credenciales correctamente, se le pedirá que permita a GNOME acceder a su cuenta en línea. Autorice el acceso para continuar.</p>
    </item>
    <item>
      <p>Todos los servicios ofrecidos por el proveedor de la cuenta estarán activados de manera predeterminada. Puede <link xref="accounts-disable-service">desactivar</link> (<gui>Apagar</gui>) los servicios individualmente.</p>
    </item>
  </steps>

  <p>Después de haber añadido las cuentas, las aplicaciones puede usar esas cuentas para los servicios que haya permitido. Consulte la <link xref="accounts-disable-service"/> para obtener más información sobre cómo controlar qué servicios permitir.</p>

  <note style="tip">
    <p>Muchos proveedores de servicios en línea proporcionan un testigo de autorización que GNOME guarda en lugar su contraseña. Si elimina una cuenta, debe revocar también ese certificado en la cuenta en línea. Consulte la <link xref="accounts-remove"/> para obtener más información.</p>
  </note>

</page>
