<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-install-flash" xml:lang="es">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Puede necesitar instalar Flash para poder ver sitios web como Youtube, que muestra vídeos y páginas web interactivas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Instalar el complemento Flash</title>

  <p><app>Flash</app> es un <em>complemento</em> para su navegador web que le permite ver vídeos y usar páginas interactivas en algunos sitios web. Algunos de ellos no funcionarán sin Flash.</p>

  <p>Si no tiene Flash instalado, es probable que vea un mensaje que le dice esto cuando visite un sitio web que lo necesita. Flash está disponible como descarga libre (pero no es de código abierto) para la mayoría de navegadores web. La mayoría de las distribuciones GNU/Linux tienen una versión de Flash que puede instalar también a través de su instalador de software (gestor de paquetes).</p>

  <steps>
    <title>Si Flash está disponible en el instalador de software:</title>
    <item>
      <p>Abra la aplicación de instalación de software y busque <input>flash</input>.</p>
    </item>
    <item>
      <p>Busque el <gui>Complemento Adobe Flash</gui>, <gui>Adobe Flash Player</gui> o similar y pulse para instalarlo.</p>
    </item>
    <item>
      <p>Si tiene alguna ventana del navegador abierta, ciérrela y vuelva a abrirla. El navegador debe darse cuenta de que Flash está instalado cuando se abra de nuevo y ahora debería ser capaz de ver sitios web que usen Flash.</p>
    </item>
  </steps>

  <steps>
    <title>Si Flash <em>no está</em> disponible en el instalador de software:</title>
    <item>
      <p>Vaya al <link href="http://get.adobe.com/flashplayer">sitio web de descarga de Flash Player</link>. Su navegador y sistema operativo debería detectarse automáticamente.</p>
    </item>
    <item>
      <p>Pulse donde dice <gui>Seleccione una versión para descargar</gui> y elija el tipo de instalador de software que funciona para su distribución de GNU/Linux. Si no sabe cuál usar, elija la opción <file> tar.gz</file>.</p>
    </item>
    <item>
      <p>Lea las <link href="http://kb2.adobe.com/cps/153/tn_15380.html">instrucciones de instalación de Flash</link> para aprender cómo instalarlo para su navegador web</p>
    </item>
  </steps>

<section id="alternatives">
  <title>Alternativas a Flash de código abierto</title>

  <p>Existe un puñado de alternativas libres, de código abierto, disponibles para Flash. Estas tienden a trabajar mejor que el complemento Flash en algunos aspectos (por ejemplo, en la mejor manipulación de la reproducción de sonido), pero peor en otros (por ejemplo, por no ser capaz de mostrar algunas de las páginas en Flash más complicado en la web).</p>

  <p>Puede intentar utilizar uno de ellos si no está satisfecho con el reproductor de Flash, o si quiere utilizar tanto software de código abierto como sea posible en el equipo. Aquí están algunas de las opciones:</p>

  <list style="compact">
    <item>
      <p>LightSpark</p>
    </item>
    <item>
      <p>Gnash</p>
    </item>
  </list>

</section>

</page>
