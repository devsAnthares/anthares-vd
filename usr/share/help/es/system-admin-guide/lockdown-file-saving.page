<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-file-saving" xml:lang="es">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-10-14" status="candidate"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
        
    <desc>Evitar que el usuario guarde archivos en el disco.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2018</mal:years>
    </mal:credit>
  </info>
  
  <title>Desactivar el guardado de archivos</title>

  <p>Puede desactivar los diálogos <gui>Guardar</gui> y <gui>Guardar como</gui>. Esto puede ser útils si está dando acceso temporal a un usuario o si no quiere que el usuario guarde archivos en el equipo.</p>

  <note style="warning">
    <p>Esta característica solo funcionará en aplicaciones que lo soporten. No todas las aplicaciones GNOME y de terceras partes tienen activada esta característica. Estos cambios no tendrán efecto en aplicaciones que no lo soportan.</p>
  </note>

  <steps>
    <title>Desactivar el guardado de archivos</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Cree el archivo <file>/etc/dconf/db/local.d/00-filesaving</file> para proporcionar información a la base de datos <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-filesaving</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/lockdown]

# Prevent the user from saving files on disk
disable-save-to-disk=true
</code>
     </listing>
    </item>
    <item>
      <p>Para evitar que el usuario ignore estos ajustes, cree el archivo <file>/etc/dconf/db/local.d/locks/filesaving</file> con el siguiente contenido:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/filesaving</file></title>
<code>
# Lock file saving settings
/org/gnome/desktop/lockdown/disable-save-to-disk
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
