<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-printer-config" xml:lang="es">

  <info>
   <link type="guide" xref="network"/>
   <revision pkgversion="3.8" date="2013-03-19" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cómo preconfigurar una impresora Samba</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2018</mal:years>
    </mal:credit>
  </info>

  <title>Configurar una impresora</title>

  <p>Este apartado explica cómo hacer que impresoras de Windows en concreto esté disponibles en GNOME.</p>	

  <p>No hace falta preconfigurar nada para instalar impresoras Samba usando gnome-control-center. Para añadir una nueva impresora Samba a su servidor CUPS (Common UNIX Printing System o Sistema común de impresión de UNIX) simplemente siga los pasos expuestos a continuación.</p>

  <steps>
    <title>Configurar su impresora</title>
    <item>
      <p>Pulse en <em>su nombre</em> en la esquina superior derecha y en el menú desplegable elija <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse el icono <gui>Impresoras</gui>.</p>
    </item>
    <item>
      <p>Desbloquee el panel.<gui>Impresoras</gui> pulsando en el botón <gui>Desbloquear</gui>.</p>
    </item>
    <item>
      <p>Pulse el botón <gui>+</gui> debajo de la lista de impresoras.</p>
    </item>
    <item>
      <p>Si sus impresoras son públicas, espere a que aparezcan las impresoras disponibles en su red de Windows. En otro caso, escriba la dirección del servidor Samba y pulse la tecla <key>Intro</key></p>
    </item>
    <item>
      <p>Cuando se le solicite, identifíquese en los servidores Samba que necesiten autenticación para mostrar sus impresoras.</p>
    </item>
    <item>
      <p>Seleccione la impresora que instalar.</p>
    </item>
    <item>
      <p>Pulse el botón <gui>Añadir</gui>.</p>
    </item>
    <item>
      <p>De los controladores disponibles en su equipo, seleccione uno adecuado para su impresora.</p>
    </item>
    <item>
      <p>Pulse <key>Seleccionar</key>.</p>
    </item>
  </steps>

</page>
