<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="logout-automatic" xml:lang="es">

  <info>
    <link type="guide" xref="login#management"/>
<!--    <link type="seealso" xref="dconf-profiles" />-->
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="login-automatic"/>

    <revision pkgversion="3.12" date="2014-06-18" status="review"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Finalizar una sesión de usuario que no está en uso.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2018</mal:years>
    </mal:credit>
  </info>

  <title>Configurar cierre de sesión automático</title>

  <p>Las sesiones de usuario sin actividad por un intervalo específico de tiempo pueden ser finalizadas automáticamente. Puede configurar un comportamiento distinto basado en si la máquina está funcionando con la batería o conectada a la red configurando la <sys its:translate="no">dconf</sys>clave correspondiente y bloqueándola posteriormente.</p>

  <note style="warning">
    <p>Recuerde que los usuarios pueden perder datos no guardados si una sesión inactiva se cierra automáticamente.</p>
  </note>

  <steps>
    <title>Configurar cierre de sesión auomático para una máquina conectada a la red.</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Cree un base de datos <sys>local</sys> para opciones que apliquen a todo el sistema en <file>/etc/dconf/db/local.d/00-autologout</file>:</p>
      <listing>
        <code>
[org/gnome/settings-daemon/plugins/power]
# Set the timeout to 900 seconds when on mains power
sleep-inactive-ac-timeout=900
# Set action after timeout to be logout when on mains power
sleep-inactive-ac-type='logout'
</code>
      </listing>
    </item>
    <item>
      <p>Ignorar el ajuste del usuario y prevenir que lo cambie en <file>/etc/dconf/db/local.d/locks/autologout</file>:</p>
      <listing>
        <code>
# Lock automatic logout settings
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-timeout
/org/gnome/settings-daemon/plugins/power/sleep-inactive-ac-type
</code>
</listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

  <!-- There will shortly be a way to get key descriptions using gsettings, we
       should recommend this instead of listing the terms here. See
       https://bugzilla.gnome.org/show_bug.cgi?id=668232 -->
  <p>Las siguientes claves de GSettings son de interés:</p>
  <terms>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-timeout</code></title>
      <p>El número de segundos que el equipo necesita estar inactivo antes de pasar a reposo si está funcionando con el adaptador de corriente.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-ac-type</code></title>
      <p>Qué debería ocurrir cuando el tiempo de espera ha pasado si el equipo está usando el adaptador de corriente.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-timeout</code></title>
      <p>El número de segundos que el equipo necesita estar inactivo antes de pasar a reposo si está funcionando con la batería.</p>
    </item>
    <item>
      <title><code>org.gnome.settings-daemon.plugins.power.sleep-inactive-battery-type</code></title>
      <p>Qué debería ocurrir cuando el tiempo de espera ha pasado si el equipo está funcionando con la batería.</p>
    </item>
  </terms>

  <p>Puede ejecutar <cmd>gsettings range</cmd> en una clave para obtener una lista de valores que puede usar. Por ejemplo:</p>
  
  <screen>$ <input>gsettings range org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type</input>
enum
'blank' # blanks the screen
'suspend' # suspends the system
'shutdown' # starts a standard shutdown procedure
'hibernate' # hibernates the system
'interactive' # shows a pop-up query asking the user what to do
'nothing' # does nothing
'logout' # log out from the session</screen>

</page>
