<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-user" xml:lang="es">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <link type="seealso" xref="session-custom"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="draft"/>
    <revision pkgversion="3.8" date="2013-08-06" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Especificar la sesión predeterminada para un usuario.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2018</mal:years>
    </mal:credit>
  </info>

  <title>Configurar una sesión de usuario predeterminada</title>

  <p>The default session is retrieved from a program called
  <app>AccountsService</app>. <app>AccountsService</app> stores this information
  in the <file>/var/lib/AccountsService/users/</file> directory.</p>

<note style="note">
  <p>En GNOME 2, el archivo <file>.dmrc</file> en la carpeta personal del usuario, se usaba para crear sesiones predeterminadas. Este archivo <file>.dmrc</file> ya no está en uso.</p>
</note>

  <steps>
    <title>Especificar una sesión predeterminada para un usuario</title>
    <item>
      <p>Asegúrese de que tiene el paquete <sys>gnome-session-xsession</sys> instalado en su sistema.</p>
    </item>
    <item>
      <p>Acceda a la carpeta <file>/usr/share/xsessions</file> en la que puede encontrar archivos <file>.desktop</file> para cada una de las sesiones disponibles. Consulte los contenidos de los archivos <file>.desktop</file> para determinar que sesión quiere usar.</p>
    </item>
    <item>
      <p>Para especificar una sesión predeterminada para un usuario, actualice el <sys>servicio de cuenta</sys> del usuario en el archivo <file>/var/lib/AccountsService/users/<var>username</var></file>:</p>
<code>[User]
Language=
XSession=gnome-classic</code>
       <p>En este ejemplo se ha configurado <link href="help:gnome-help/gnome-classic">GNOME Classic</link> como sesión predeterminada, usando el archivo <file>/usr/share/xsessions/gnome-classic.desktop</file>.</p>
     </item>
  </steps>

  <p>Después de especificar una sesión predeterminada para el usuario, esa sesión será usada la próxima vez que el usuario inicie sesión, excepto si el usuario elige una sesión distinta en la pantalla de inicio de sesión.</p>

</page>
