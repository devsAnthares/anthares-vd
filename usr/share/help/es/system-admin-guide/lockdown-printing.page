<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-printing" xml:lang="es">
     
  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.11" date="2014-12-04" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Evitar que el usuario imprima documentos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2018</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar impresión</title>

  <p>Puede desactivar que se muestre el diálogo de impresión a los usuarios. Esto puede ser útil si está dando acceso temporal a un usuario o no quiere que el usuario imprima en las impresoras de red.</p>

  <note style="warning">
    <p>Esta característica solo funcionará en aplicaciones que lo soporten. No todas las aplicaciones GNOME y de terceras partes tienen activada esta característica. Estos cambios no tendrán efecto en aplicaciones que no lo soportan.</p>
  </note>

  <steps>
    <title>Desactivar impresión</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Cree el archivo de claves <file>/etc/dconf/db/local.d/00-printing</file> para proporcionar información a la base de datos <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-printing</file></title>
<code>
# Specify the dconf path
[org/gnome/desktop/lockdown]
 
# Prevent applications from printing
disable-printing=true
</code>
     </listing>
    </item>
    <item>
      <p>Para evitar que el usuario ignore estos ajustes, cree el archivo <file>/etc/dconf/db/local.d/locks/printing</file> con el siguiente contenido:</p>
      <listing>
        <title><file>/etc/dconf/db/local.db/locks/printing</file></title>
<code>
# Lock printing settings
/org/gnome/desktop/lockdown/disable-printing
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
