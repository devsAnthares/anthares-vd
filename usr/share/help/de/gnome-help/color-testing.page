<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="de">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verwenden Sie die zur Verfügung gestellten Testprofile, um zu überprüfen ob Ihre Profile korrekt auf Ihre Anzeige angewandt wurden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Wie kann ich überprüfen, ob die Farbverwaltung korrekt arbeitet?</title>

  <p>Die Auswirkungen eines Farbprofils sind oft nicht sofort sichtbar. Es kann schwierig sein festzustellen, ob sich überhaupt etwas geändert hat.</p>

  <p>In GNOME werden verschiedene Profile für Testzwecke mitgeliefert. Mit diesen ist deutlich sichtbar, wenn sie angewendet werden:</p>

  <terms>
    <item>
      <title>Blau</title>
      <p>Der gesamte Bildschirm wird blau eingefärbt und überprüft, ob die Kalibrierungskurven an die Anzeige gesendet wurden.</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Geräte</gui> in der Seitenleiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Farbe</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie das Gerät, für das Sie ein Profil hinzufügen wollen. Achten Sie darauf, welches Profil aktuell verwendet wird.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Profil hinzufügen</gui> und wählen Sie ein Test-Profil vom Ende der Liste aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Hinzufügen</gui>, um Ihre Auswahl zu bestätigen.</p>
    </item>
    <item>
      <p>Um zu Ihrem vorherigen Profil zurückzukehren, wählen Sie das Gerät im <gui>Farbe</gui>-Panel aus, wählen das Profil, das Sie vor der Aktivierung eines der Testprofile verwendet haben und klicken auf <gui>Aktivieren</gui>, um es wieder zu verwenden.</p>
    </item>
  </steps>


  <p>Mit diesen Profilen wird deutlich sichtbar, wenn eine Anwendung die Farbverwaltung unterstützt.</p>

</page>
