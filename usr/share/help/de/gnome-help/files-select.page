<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="de">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Drücken Sie <keyseq><key>Strg</key><key>S</key></keyseq>, um mehrere Dateien mit ähnlichen Namen auszuwählen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Dateien anhand eines Musters auswählen</title>

  <p>Sie können Dateien in einem Ordner mit Hilfe eines Musters auswählen. Drücken Sie <keyseq><key>Strg</key><key>S</key></keyseq>, um das Fenster <gui>Nach Muster auswählen</gui> aufzurufen. Geben Sie ein Muster ein, das die gemeinsamen Namensteile sowie Platzhalter enthält. Es gibt zwei Platzhalterzeichen:</p>

  <list style="compact">
    <item><p><file>*</file> entspricht einer beliebigen Anzahl beliebiger Zeichen, auch überhaupt keinem Zeichen.</p></item>
    <item><p><file>?</file> entspricht genau einem Zeichen.</p></item>
  </list>

  <p>Beispielsweise:</p>

  <list>
    <item><p>Wenn Sie eine OpenDocument-Textdatei, eine PDF-Datei oder ein Bild haben, die alle den gleichen Grundnamen <file>Rechnung</file> haben, wählen Sie alle drei mit dem Muster</p>
    <example><p><file>Rechnung.*</file></p></example></item>

    <item><p>Wenn Sie einige Fotos haben, die nach dem Schema <file>Urlaub-001.jpg</file>, <file>Urlaub-002.jpg</file>, <file>Urlaub-003.jpg</file> benannt sind, wählen Sie sie alle mit dem Muster</p>
    <example><p><file>Urlaub-???.jpg</file></p></example></item>

    <item><p>Wenn Sie Fotos nach dem vorigen Schema haben, aber einige davon bearbeitet und <file>-bearbeitet</file> an den Dateinamen angehängt haben, wählen Sie die bearbeiteten Fotos mit</p>
    <example><p><file>Urlaub-???-bearbeitet.jpg</file></p></example></item>
  </list>

</page>
