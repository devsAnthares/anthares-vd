<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einen weiteren Bildschirm einrichten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Einen weiteren Bildschirm mit Ihrem Rechner verbinden</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Einen weiteren Bildschirm einrichten</title>
  <p>Um einen zusätzlichen Bildschirm einzurichten, verbinden Sie ihn zunächst mit Ihrem Rechner. Falls Ihr System diesen nicht sofort erkennt oder wenn Sie die Einstellungen anpassen wollen:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Geräte</gui> in der Seitenleiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirme</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Platzieren Sie im Diagramm unter <gui>Bildschirmanordnung</gui> Ihre Bildschirme so, dass diese der tatsächlichen realen Anordnung entsprechen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Primärer Bildschirm</gui>, um den primären Bildschirm zu wählen.</p>

      <note>
        <p>Der primäre Bildschirm zeigt das <link xref="shell-introduction">obere Panel</link> und, sofern geöffnet, die <gui>Aktivitäten</gui>-Übersicht.</p>
      </note>
    </item>
    <item>
      <p>Wählen Sie die Auflösung oder Skalierung und die Ausrichtung.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Anwenden</gui>. Die geänderten Einstellungen werden für 20 Sekunden angewendet und dann wieder rückgängig gemacht. Auf diese Weise werden Ihre alten Einstellungen wiederhergestellt, falls Sie mit den neuen Einstellungen nichts sehen können. Wenn Sie mit den neuen Einstellungen zufrieden sind, klicken Sie auf <gui>beibehalten</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Darstellungsmodi</title>
    <p>Mit zwei Bildschirmen sind die folgenden Anzeigemodi verfügbar:</p>
    <list>
      <item><p><gui>Bildschirme verketten:</gui> Die Bildschirmkanten werden gekoppelt, so dass der Bildbereich nahtlos von einem zum anderen Bildschirm übergeht.</p></item>
      <item><p><gui>Bildschirm spiegeln:</gui> Der gleiche Inhalt wird auf zwei Bildschirmen mit der gleichen Auflösung und Ausrichtung angezeigt.</p></item>
      <item><p><gui>Einzelner Bildschirm:</gui> Nur ein Bildschirm wird eingerichtet und der andere ist ausgeschaltet. Zum Beispiel wäre ein externer Bildschirm, der mit einem angedockten Laptop verbunden ist, mit geschlossenem Deckel der einzige eingerichtete Bildschirm.</p></item>
    </list>

</section>

<section id="multiple">

  <title>Mehr als einen Bildschirm hinzufügen</title>
    <p>Bei mehr als zwei Bildschirmen ist <gui>Bildschirme verketten</gui> der einzige mögliche Modus.</p>
    <list>
      <item>
        <p>Wählen Sie im Auswahlmenü den Bildschirm zum Einrichten.</p>
      </item>
      <item>
        <p>Ordnen Sie die Bildschirme entsprechend der tatsächlichen physikalischen Positionen an.</p>
      </item>
      <item>
        <p>Jeder Bildschirm kann ein- und ausgeschaltet werden mit dem Schalter in den Stellungen <gui>EIN | AUS</gui>.</p>
      </item>
    </list>

</section>
</page>
