<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="de">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bei den meisten kabelgebundenen Netzwerkverbindungen genügt es, ein Netzwerkkabel einzustecken.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Mit einem Kabelnetzwerk (Ethernet) verbinden</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>Bei den meisten kabelgebundenen Netzwerkverbindungen genügt es, ein Netzwerkkabel einzustecken. Das Netzwerksymbol (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">Einstellungen</span></media>) im oberen Panel wird mit drei Punkten angezeigt, solange die Verbindung aufgebaut wird. Die Punkte verschwinden, sobald die Verbindung hergestellt ist.</p>

  <p>Falls dies nicht geschieht, sollten Sie zunächst sicherstellen, dass Ihr Netzwerkkabel eingesteckt ist. Ein Ende des Kabels gehört in die rechteckige Ethernet-(Netzwerk-)Buchse Ihres Rechners und das andere Ende in einen Switch, Router, eine Netzwerk-Wanddose oder Ähnliches (abhängig von der Ausstattung Ihres Netzwerks). Bei manchen Geräten zeigt ein Lämpchen neben der Ethernet-Buchse an, dass das Kabel eingesteckt und die Schnittstelle aktiv ist.</p>

  <note>
    <p>Sie können einen Rechner nicht direkt über ein Netzwerkkabel mit einem anderen verbinden, zumindest nicht ohne zusätzliche Einrichtungsschritte. Um zwei Rechner zu verbinden, müssen Sie beide an einen Netzwerk-Hub, Router oder Switch anschließen.</p>
  </note>

  <p>Falls noch immer kein Verbindungsaufbau möglich ist, könnte Ihr Netzwerk die automatische Einrichtung (DHCP) nicht unterstützen. In diesem Fall müssen Sie es <link xref="net-manual">manuell einrichten</link>.</p>

</page>
