<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="de">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Vergrößern Sie das Bild, damit Sie einfacher lesen können.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Einen Bildschirmbereich vergrößern</title>

  <p>Den Bildschirm zu vergrößern bedeutet nicht, die <link xref="a11y-font-size">Textgröße</link> zu erhöhen. Diese Funktion verhält sich vielmehr wie eine Lupe und erlaubt Ihnen, sich über Teile des Bildschirms zoomend zu bewegen.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Barrierefreiheit</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie im Abschnitt <gui>Sehen</gui> auf <gui>Vergrößerung</gui>.</p>
    </item>
    <item>
      <p>Schalten Sie <gui>Vergrößerung</gui> auf <gui>AN</gui> in der oberen rechten Ecke des Fensters <gui>Vergrößerungseinstellungen</gui>.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Sie können sich nun über den Bildbereich bewegen. Indem Sie Ihre Maus zu den Ecken des Bildschirms bewegen, bewegen Sie den vergrößerten Bereich in verschiedene Richtungen, was es Ihnen erlaubt, den Bereich Ihrer Wahl zu sehen.</p>

  <note style="tip">
    <p>Sie können die Vergrößerung schnell ein- und ausschalten, indem Sie auf das <link xref="a11y-icon">Barrierefreiheitssymbol</link> in der oberen Leiste klicken und dann <gui>Vergrößerung</gui> wählen.</p>
  </note>

  <p>Sie können den Vergrößerungsfaktor, die Mausverfolgung und die Position der vergrößerten Ansicht auf dem Bildschirm ändern. Passen Sie die Einstellungen im Einstellungsfenster <gui>Vergrößerungseinstellungen</gui> im Reiter <gui>Lupe</gui> an.</p>

  <p>Sie können ein Fadenkreuz aktivieren, welches Ihnen beim Finden des Zeigers von Maus oder Tastfeld hilft. Um es einzuschalten und die Länge, Farbe und Strichstärke anzupassen, klicken Sie im <gui>Vergrößerung</gui>-Einstellungsfenster auf den Reiter <gui>Fadenkreuz</gui>.</p>

  <p>Sie können die Videodarstellung umkehren oder zu <gui>Weiß auf schwarz</gui> wechseln sowie die Helligkeit, den Kontrast und die Graustufen-Optionen für die Lupe anpassen. Die richtige Kombination dieser Optionen ist nützlich für Personen mit Sehschwächen, jegliche Formen der Lichtempfindlichkeit (Lichtscheu, Photophobie) oder einfach für die Nutzung des Rechners unter ungünstigen Lichtverhältnissen. Wählen Sie <gui>Farbeffekte</gui> im Fenster <gui>Vergrößerung</gui>, um diese Optionen zu aktivieren und zu ändern.</p>

</page>
