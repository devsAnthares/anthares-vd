<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="de">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Der <gui>Überfahren-Klick</gui> (Verzögerter Klick) erlaubt Ihnen zu klicken, indem Sie die Maus unbewegt halten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Klicken durch Überfahren simulieren</title>

  <p>Sie können klicken und ziehen, indem Sie einfach mit dem Mauszeiger über ein Bedienungselement oder ein Objekt auf dem Bildschirm fahren. Das ist nützlich, wenn es Ihnen schwer fällt, die Maus zu bewegen und gleichzeitig die Maustaste zu drücken. Diese Funktion heißt <gui>Überfahren-Klick</gui> oder verzögerter Klick.</p>

  <p>Wenn der <gui>Überfahren-Klick</gui> aktiviert ist, können Sie Ihren Mauszeiger über einem Bedienelement platzieren, die Maus loslassen und einen Moment warten, bis das Bedienelement für Sie angeklickt wird.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Barrierefreiheit</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Klickassistent</gui> im Abschnitt <gui>Zeigen und Klicken</gui>.</p>
    </item>
    <item>
      <p>Stellen Sie den <gui>Überfahrklick</gui> auf <gui>AN</gui>.</p>
    </item>
  </steps>

  <p>Das Fenster <gui>Überfahren-Klick</gui>-Typ öffnet sich und bleibt über allen anderen Fenstern sichtbar. Sie können es benutzen, um auszuwählen, welche Art von Klick ausgelöst werden soll, wenn Sie über einen Knopf fahren. Wenn Sie zum Beispiel <gui>Kontextklick</gui> wählen, wird die Maus rechts klicken, sobald Sie die Maus für einige Sekunden über den Knopf fahren.</p>

  <p>Wenn Sie Ihren Mauszeiger über einen Knopf platzieren und nicht bewegen, wird er schrittweise seine Farbe ändern. Wenn er die Farbe vollständig geändert hat, wird der Knopf gedrückt.</p>

  <p>Ändern Sie die Einstellung der <gui>Verzögerung</gui>, um festzulegen, wie lange Sie den Mauszeiger ruhig halten müssen, bevor ein Klick ausgelöst wird.</p>

  <p>Sie müssen die Maus nicht vollkommen still halten, wenn Sie sie über einen Knopf fahren, um ihn zu klicken. Der Mauszeiger darf sich ein kleines bisschen bewegen und wird dennoch nach einer Weile klicken. Wenn er sich allerdings zu sehr bewegt, wird der Klick nicht ausgelöst.</p>

  <p>Passen Sie den <gui>Schwellenwert der Bewegung</gui> an, um einzustellen, wie sehr sich der Zeiger bewegen darf, damit er noch immer als ruhig positioniert eingestuft werden kann.</p>

</page>
