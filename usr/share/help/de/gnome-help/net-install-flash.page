<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-install-flash" xml:lang="de">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zum Betrachten von Webseiten wie YouTube ist es erforderlich, Flash zu installieren, welches die Anzeige von Videos und interaktiven Webinhalten ermöglicht.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Das Flash-Plugin installieren</title>

  <p><app>Flash</app> ist eine <em>Erweiterung</em> für Ihren Webbrowser zum Betrachten von Videos und interaktiven Inhalten auf einigen Webseiten. Einige Webseiten würden ohne Flash nicht funktionieren.</p>

  <p>Falls Sie Flash nicht installiert haben, wird wahrscheinlich beim Besuch einer Webseite eine Meldung angezeigt, wenn diese Seite Flash benötigt. Flash ist als freier (aber nicht quelloffener) Download für die meisten Webbrowser verfügbar. Die meisten Linux-Distributionen liefern eine Flash-Version, die Sie über die Softwareverwaltung (Paketverwaltung) installieren können.</p>

  <steps>
    <title>Falls Flash über die Softwareverwaltung verfügbar ist:</title>
    <item>
      <p>Öffnen Sie die Anwendung zur Softwareverwaltung und suchen Sie nach <input>flash</input>.</p>
    </item>
    <item>
      <p>Suchen Sie nach dem <gui>Adobe Flash Plugin</gui>, <gui>Adobe Flash Player</gui> oder Ähnlichem, und klicken Sie darauf, um es zu installieren.</p>
    </item>
    <item>
      <p>Falls irgendwelche Webbrowser-Fenster geöffnet sind, schließen Sie diese und öffnen Sie diese erneut. Der Webbrowser sollte nun feststellen, dass Flash installiert ist und Sie sollten nun in der Lage sein, auf Flash basierende Webseiten zu betrachten.</p>
    </item>
  </steps>

  <steps>
    <title>Falls Flash <em>nicht</em> über die Softwareverwaltung verfügbar ist:</title>
    <item>
      <p>Besuchen Sie die <link href="http://get.adobe.com/flashplayer">Download-Webseite für Flash Player</link>. Ihr Browser und Ihr Betriebssystem sollten automatisch erkannt werden.</p>
    </item>
    <item>
      <p>Klicken Sie auf<gui>Version auswählen</gui> und wählen Sie den Typ der Softwareinstallation, der für Ihre Linux-Distribution geeignet ist. Falls Sie unsicher sind, wählen Sie die Option <file>TAR.GZ für andere Linux-Konfigurationen</file>.</p>
    </item>
    <item>
      <p>In den <link href="http://kb2.adobe.com/cps/153/tn_15380.html">Installationsanweisungen für Flash</link> wird beschrieben, wie Sie Flash für Ihren Webbrowser installieren können.</p>
    </item>
  </steps>

<section id="alternatives">
  <title>Quelloffene Alternativen zu Flash</title>

  <p>Es gibt eine kleine Auswahl an quelloffenen Alternativen zu Flash. In einigen Fällen funktionieren diese besser als das Flash-Plugin, zum Beispiel bei der Audio-Wiedergabe. Andererseits können sie auch an der Komplexität einiger Flash-Webseiten scheitern.</p>

  <p>Falls Sie mit dem Flash-Player nicht wirklich zufrieden sind oder so viel quelloffene Software wie möglich auf Ihrem Rechner installieren wollen, sollten Sie diese Alternativen ausprobieren:</p>

  <list style="compact">
    <item>
      <p>LightSpark</p>
    </item>
    <item>
      <p>Gnash</p>
    </item>
  </list>

</section>

</page>
