<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Sprache für Benutzeroberfläche und Hilfetexte wechseln.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Zu verwendende Sprache ändern</title>

  <p>Sie können Ihre Arbeitsumgebung und Ihre Anwendungen in Dutzenden von Sprachen benutzen, vorausgesetzt die richtigen Sprachenpakete sind auf Ihrem Rechner installiert.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Region und Sprache</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Region und Sprache</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Sprache</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie die gewünschte Region und Sprache aus. Falls Ihre Region und Sprache nicht aufgelistet sind, klicken Sie auf <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> am unteren Rand der Liste, um aus allen verfügbaren Regionen und Sprachen zu wählen.</p>
    </item>
    <item>
      <p>Klicken Sie zum Speichern auf <gui style="button">Fertig</gui>.</p>
    </item>
    <item>
      <p>Sie sollten sich nun <gui>abmelden, damit die Änderungen wirksam werden</gui>. Klicken Sie hierzu auf <gui style="button">Jetzt neustarten</gui>, oder klicken Sie auf <gui style="button">X</gui>, um den Neustart später auszuführen.</p>
    </item>
  </steps>

  <p>Einige Übersetzungen könnten unvollständig sein, und bestimmte Anwendungen könnten überhaupt nicht in Ihrer Sprache verfügbar sein. Jeglicher nicht übersetzter Text wird in der Originalsprache angezeigt, üblicherweise amerikanisches Englisch.</p>

  <p>Es gibt einige spezielle Ordner in Ihrem persönlichen Ordner, wo Anwendungen Musik, Bilder und Dokumente speichern. Diese Ordner haben standardisierte Namen entsprechend Ihrer Sprache. Wenn Sie sich erneut anmelden, werden Sie gefragt, ob Sie die Ordner anhand der Vorgaben für Ihre gewählte Sprache umbenennen wollen. Falls Sie vorhaben, die neue Sprache ständig zu verwenden, sollten Sie auch die Ordnernamen aktualisieren.</p>

  <note style="tip">
    <p>Wenn auf Ihrem System mehrere Benutzerkonten vorhanden sind, dann gibt es eine separate Instanz des Panels <gui>Region und Sprache</gui> für den Anmeldebildschirm. Klicken Sie oben rechts auf den Knopf <gui>Anmeldebildschirm</gui>, um zwischen den beiden Instanzen umzuschalten.</p>
  </note>

</page>
