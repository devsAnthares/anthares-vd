<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Löschen Sie Benutzerkonten, die nicht mehr verwendet werden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Löschen eines Benutzerkontos</title>

  <p>Sie können <link xref="user-add">mehrere Benutzerkonten auf Ihrem Rechner anlegen</link>. Wenn jemand Ihren Rechner nicht mehr verwendet, können Sie dessen Benutzerkonto löschen.</p>

  <p>Sie brauchen <link xref="user-admin-explain">Systemverwalterrechte</link>, um Benutzerkonten zu löschen.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Benutzer</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Benutzer</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Entsperren</gui> in der rechten oberen Ecke und geben Sie Ihr Passwort ein.</p>
    </item>
    <item>
      <p>Wählen Sie den Benutzer, den Sie löschen möchten, und klicken Sie auf den Knopf <gui style="button">-</gui> unter der Kontenliste links, um dieses Konto zu löschen.</p>
    </item>
    <item>
      <p>Jeder Benutzer hat seinen eigenen persönlichen Ordner für Dateien und Einstellungen. Sie können den persönlichen Ordner des Benutzers entweder behalten oder löschen. Klicken Sie auf <gui>Dateien löschen</gui>, falls Sie sicher sind, dass diese in Zukunft nicht mehr verwendet werden, um somit Speicherplatz freizugeben. Die Dateien werden dauerhaft gelöscht und können nicht wiederhergestellt werden. Es könnte sinnvoll sein, die Dateien auf einem Speichermedium wie einem externen Datenträger oder einer CD zu sichern, bevor Sie diese löschen.</p>
    </item>
  </steps>

</page>
