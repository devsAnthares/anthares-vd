<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-location" xml:lang="de">
  <info>
    <link type="guide" xref="privacy" group="#last"/>

    <revision pkgversion="3.14" date="2014-10-13" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivieren oder Deaktivieren der Geoposition.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Ortungsdienste steuern</title>

  <p>Geoposition- oder Ortungsdienste nutzen die Funkzellen von Mobilfunknetzwerken, GPS oder WLAN-Zugangspunkte in der Nähe, um Ihre gegenwärtige Position zu ermitteln, die dann zum Festlegen Ihrer Zeitzone und von Anwendungen wie <app>Karten</app> verwendet werden kann. Wenn dies aktiviert ist, kann Ihr Standort mit hoher Genauigkeit über das Netzwerk freigegeben werden.</p>

  <steps>
    <title>Die Geoposition-Funktionsmerkmale Ihrer Arbeitsumgebung ausschalten</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Privatsphäre</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Privatsphäre</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
     <p>Stellen Sie den Schalter <gui>Ortungsdienste</gui> auf <gui>AUS</gui>.</p>
     <p>Um diese Funktion erneut zu aktivieren, stellen Sie den Schalter <gui>Ortungsdienste</gui> auf <gui>An</gui>.</p>
    </item>
  </steps>

</page>
