<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="de">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tipps, wie »Lassen Sie den Akku nicht zu stark entladen«.</desc>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Holen Sie das Optimum aus Ihrem Laptop-Akku</title>

<p>Wenn Laptop-Akkus älter werden, können sie Energie nicht mehr so gut speichern und ihre Kapazität verringert sich schrittweise. Es gibt einige Möglichkeiten, um die Lebensdauer des Akkus zu verlängern, wobei Sie keinen zu großen Unterschied erwarten sollten.</p>

<list>
  <item>
    <p>Lassen Sie nicht zu, dass der Akku sich vollständig entlädt. Laden Sie ihn wieder auf, <em>bevor</em> der Ladestand sehr niedrig ist, wobei die meisten Akkus über Schutzmechanismen verfügen, um den Akku nicht zu tief zu entladen. Wiederaufladen ist effizienter, wenn der Akku nur teilweise entladen ist, aber wenn der Akku nur ganz gering entladen ist, ist das Aufladen schlechter für den Akku.</p>
  </item>
  <item>
    <p>Hitze übt einen nachteiligen Effekt auf die Ladeeffizienz von Akkus aus. Lassen Sie den Akku nicht wärmer werden als nötig.</p>
  </item>
  <item>
    <p>Akkus altern auch dann, wenn Sie diese nicht verwenden. Es ist daher nicht sinnvoll, zusammen mit dem Originalakku einen Ersatzakku zu kaufen – beschaffen Sie dann Ersatz, wenn Sie ihn benötigen.</p>
  </item>
</list>

<note>
  <p>Diese Empfehlung bezieht sich besonders auf Lithium-Ionen-Akkus (Li-Ion), welches die meistverwendete Art von Akku ist. Andere Arten von Akkus verhalten sich möglicherweise anders.</p>
</note>

</page>
