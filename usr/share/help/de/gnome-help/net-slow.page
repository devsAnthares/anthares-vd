<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="de">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anderes wird gerade heruntergeladen, Sie könnten eine schlechte Verbindung haben oder es ist gerade eine Tageszeit mit erhöhtem Datenaufkommen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Das Internet scheint langsam zu sein</title>

  <p>Wenn Sie das Internet verwenden und es langsam zu sein scheint, gibt es eine Reihe von möglichen Gründen für die Verlangsamung.</p>

  <p>Schließen Sie Ihren Webbrowser und öffnen Sie ihn wieder, trennen Sie die Verbindung zum Internet und verbinden dann wieder neu. (Dadurch wird viel von dem, was der Grund für die langsame Internetverbindung sein könnte, in seinen ursprünglichen Zustand zurückversetzt.)</p>

  <list>
    <item>
      <p><em style="strong">Hauptverkehrszeiten im Internet</em></p>
      <p>Internet-Dienstanbieter richten Ihre Internetverbindungen meist so ein, dass sie von mehreren Haushalten geteilt werden. Selbst wenn Sie sich separat verbinden, über Ihre eigene Telefon- oder Kabelverbindung, kann es sein, dass die restliche Verbindungsstrecke zum Internet ab dem Verteiler geteilt ist. Wenn das der Fall ist und viele Ihrer Nachbarn das Internet zur gleichen Zeit verwenden wie Sie, könnten Sie eine Verlangsamung bemerken. Das tritt am ehesten zu den Zeiten auf, an denen Ihre Nachbarn aller Wahrscheinlichkeit nach im Internet unterwegs sind (zum Beispiel am Abend).</p>
    </item>
    <item>
      <p><em style="strong">Herunterladen mehrerer Dinge gleichzeitig</em></p>
      <p>Wenn Sie oder jemand anderer, der Ihre Internetverbindung mitbenutzt, mehrere Dateien auf einmal herunterlädt oder Videos ansieht, ist die Internetverbindung unter Umständen nicht schnell genug, um dieses Datenvolumen zu bewältigen. Sie wird sich dann langsamer anfühlen.</p>
    </item>
    <item>
      <p><em style="strong">Unzuverlässige Verbindung</em></p>
      <p>Manche Internetverbindungen sind einfach unzuverlässig, besonders temporäre oder an stark frequentierten Orten. Wenn Sie in einem Café oder einem Kongresszentrum arbeiten, ist die Internetverbindung vielleicht überlastet oder einfach unzuverlässig.</p>
    </item>
    <item>
      <p><em style="strong">Schwaches Funkverbindungssignal</em></p>
      <p>Wenn Sie kabellos (per Funknetzwerk) ins Internet gehen, sehen Sie beim Netzwerksymbol in der oberen Leiste nach, ob Sie einen guten Empfang haben. Wenn nicht, kann Ihnen die Internetverbindung langsam vorkommen, weil Sie keinen besonders guten Empfang haben.</p>
    </item>
    <item>
      <p><em style="strong">Verwendung einer langsameren Internetverbindung</em></p>
      <p>Wenn Sie eine mobile Internetverbindung haben und bemerken, dass sie langsam ist, haben Sie sich vielleicht an eine Stelle mit schlechtem Empfang bewegt. Wenn das passiert, wechselt die Internetverbindung automatisch von einer schnellen »mobilen Breitbandverbindung« wie 3G zu einer zuverlässigeren, aber langsameren Verbindung wie GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Der Webbrowser hat ein Problem</em></p>
      <p>Webbrowser stoßen manchmal auf Probleme, was sie langsamer macht. Dafür kommt eine Reihe von Gründen in Frage – Sie könnten eine Webseite besucht haben, mit der der Browser zu kämpfen hat, oder Sie hatten den Browser zum Beispiel sehr lange geöffnet. Versuchen Sie alle Browser-Fenster zu schließen und den Browser wieder zu öffnen, um zu sehen, ob das einen Unterschied ergibt.</p>
    </item>
  </list>

</page>
