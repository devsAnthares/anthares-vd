<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="de">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Schauen Sie unter <guiseq><gui>Einstellungen</gui><gui>Farbe</gui></guiseq>, um ihrem Bildschirm ein Farbprofil zuzuweisen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Wie ordne ich Profile Geräten zu?</title>

  <p>Sie können für Ihrer Anzeige oder Ihrem Drucker ein Farbprofil zuweisen, so dass die angezeigten Farben genauer dargestellt werden.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Geräte</gui> in der Seitenleiste.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Farbe</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie das Gerät aus, für welches Sie ein Profil hinzufügen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Profil hinzufügen</gui> und wählen Sie ein vorhandenes Profil aus oder importieren Sie ein neues Profil.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Hinzufügen</gui>, um Ihre Auswahl zu bestätigen.</p>
    </item>
  </steps>

  <p>Jedem Gerät können mehrere Profile zugeordnet werden, aber nur genau ein Profil lässt sich als <em>Standardprofil</em> festlegen. Dieses Standardprofil wird verwendet, wenn keine weiteren Informationen vorliegen, so dass das Profil automatisch gewählt wird. Ein Beispiel hierfür ist, dass zwei Profile vorliegen, eines für glänzendes Papier und eines für sonstiges Papier.</p>

  <!--
  <figure>
    <desc>You can make a profile default by changing it with the radio button.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>
  -->

  <p>Falls ein Kalibrierungsgerät angeschlossen ist, erstellen Sie mit dem Knopf <gui>Kalibrieren …</gui> ein neues Profil.</p>

</page>
