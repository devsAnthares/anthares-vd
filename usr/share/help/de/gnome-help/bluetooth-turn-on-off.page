<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="de">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktivieren oder deaktivieren von Bluetooth-Geräten auf Ihrem Rechner.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Bluetooth ein- oder ausschalten</title>

  <p>Sie können Bluetooth einschalten, um sich mit anderen Bluetooth-Geräten zu verbinden und Dateien zu senden und zu empfangen. So schalten Sie Bluetooth ein:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Bluetooth</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bluetooth</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie den Schalter oben auf <gui>AN</gui>.</p>
    </item>
  </steps>

  <p>Viele Laptops haben einen Gehäuse-Schalter oder eine Tastenkombination, um Bluetooth ein- oder auszuschalten. Schauen Sie nach, ob Ihr Rechner über einen Gehäuse-Schalter oder über einen Knopf auf Ihrer Tastatur verfügt. Die Tastenkombination ist meistens über die <key>Fn</key>-Taste erreichbar.</p>

  <p>So schalten Sie Bluetooth aus:</p>
  <steps>
    <item>
      <p>Öffnen Sie das <gui xref="shell-introduction#yourname">Systemmenü</gui> auf der rechten Seite der obersten Leiste.</p>
    </item>
    <item>
      <p>Wählen Sie <gui xref="shell-introduction#yourname">Bluetooth</gui>. Anschließend wird sich der Bluetooth-Bereich des Menüs erweitern.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Ausschalten</gui>.</p>
    </item>
  </steps>

  <note><p>Ihr Rechner ist solange <link xref="bluetooth-visibility">sichtbar</link>, wie das <gui>Bluetooth</gui>-Panel geöffnet ist.</p></note>

</page>
