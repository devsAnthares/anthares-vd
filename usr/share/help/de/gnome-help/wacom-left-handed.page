<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-left-handed" xml:lang="de">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Das Wacom-Tablett auf <gui>Linkshändige Ausrichtung</gui> umstellen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Das Tablet mit der linken Hand verwenden</title>

  <p>Einige Tabletts verfügen über physische Knöpfe an einer Seite. Das Tablett kann um 180 Grad gedreht werden, um die Position der Knöpfe den Erfordernissen linkshändiger Benutzer anzupassen. So ändern Sie die Orientierung für Linkshänder:</p>

<steps>
  <item>
    <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
  </item>
  <item>
    <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
  </item>
  <item>
    <p>Klicken Sie auf <gui>Geräte</gui> in der Seitenleiste.</p>
  </item>
  <item>
    <p>Klicken Sie auf <gui>Wacom-Tablett</gui> in der Seitenleiste, um das Panel zu öffnen.</p>
  </item>
  <item>
    <p>Klicken Sie auf den Knopf <gui>Tablett</gui> in der Kopfleiste.</p>
    <!-- TODO: document how to connect the tablet using Bluetooth/add link -->
    <note style="tip"><p>Falls kein Tablett erkannt wurde, dann werden Sie gebeten <gui>Bitte schließen Sie Ihr Wacom-Tablett an oder schalten Sie es ein</gui>. Klicken Sie auf <gui>Bluetooth-Einstellungen</gui>, um ein Funk-Tablett zu verbinden.</p></note>
  </item>
  <item><p>Stellen Sie <gui>Linkshändige Ausrichtung</gui> auf <gui>An</gui>.</p></item>
</steps>

</page>
