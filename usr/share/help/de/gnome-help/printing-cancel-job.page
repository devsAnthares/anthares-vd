<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="de">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einen ausstehenden Druckauftrag abbrechen und aus der Warteschlange entfernen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Einen Druckauftrag abbrechen, anhalten oder freigeben</title>

  <p>Sie können einen ausstehenden Druckauftrag abbrechen und in den Druckereinstellungen aus der Warteschlange entfernen.</p>

  <section id="cancel-print-job">
    <title>Einen Druckauftrag abbrechen</title>

  <p>Falls Sie unabsichtlich den Druck eines Dokuments gestartet haben, können Sie den Druck abbrechen, um nicht unnötig Tinte und Papier zu verbrauchen.</p>

  <steps>
    <title>So brechen Sie einen Druckauftrag ab:</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Drucker</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Drucker</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui>Aufträge anzeigen</gui> auf der rechten Seite des Dialogs <gui>Drucker</gui>.</p>
    </item>
    <item>
      <p>Brechen Sie den Druckauftrag ab, indem Sie auf den Stopp-Knopf klicken.</p>
    </item>
  </steps>

  <p>Falls dadurch der Druckvorgang entgegen aller Erwartungen nicht abgebrochen wird, versuchen Sie, die <gui>Abbrechen</gui>-Taste an Ihrem Drucker gedrückt zu halten.</p>

  <p>Als letzter Ausweg, insbesondere wenn es sich um einen großen Druckauftrag handelt, entfernen Sie das Papier aus dem Papiervorrat des Druckers. Der Drucker sollte dann erkennen, dass kein Papier mehr vorhanden ist und den Druckvorgang daraufhin abbrechen. Nun können Sie versuchen, den Druckvorgang erneut abzubrechen oder den Drucker aus- und wieder einzuschalten.</p>

  <note style="warning">
    <p>Seien Sie vorsichtig, um den Drucker beim Entnehmen des Papiers nicht zu beschädigen. Wenn Sie sehr kräftig am Papier ziehen müssten, um es zu entfernen, dann sollten Sie es doch lieber dort lassen, wo es ist.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Einen Druckauftrag anhalten und freigeben</title>

  <p>Wenn Sie Druckaufträge pausieren oder löschen wollen, können Sie zu den Druckaufträgen in den Druckereinstellungen gehen und den entsprechenden Knopf drücken.</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Drucker</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Drucker</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Aufträge anzeigen</gui> auf der rechten Seite des <gui>Drucker</gui>-Dialoges und pausieren oder löschen Sie die Aufträge nach Ihren Bedürfnissen.</p>
    </item>
  </steps>

  </section>

</page>
