<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="de">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Die Signalstärke könnte zu gering sein oder es ist kein sauberer Verbindungsaufbau möglich.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

<title>Warum wird meine WLAN-Verbindung dauernd getrennt?</title>

<p>Es kann passieren, dass Sie von einem Funknetzwerk getrennt werden, obwohl Sie die Verbindung aufrechterhalten wollten. Ihr Rechner wird normalerweise versuchen, die Verbindung wieder herzustellen, sobald dies passiert (das Netzwerksymbol in der oberen Leiste zeigt drei Punkte wenn es versucht, die Verbindung wieder herzustellen), aber es kann ärgerlich sein, besonders wenn Sie gerade zu der Zeit im Internet waren.</p>

<section id="signal">
 <title>Schwaches Funksignal</title>

 <p>Ein häufiger Grund für Verbindungsabbrüche bei einem Funknetzwerk ist ein schwaches Signal. Funknetzwerkverbindungen haben eine beschränkte Reichweite. Wenn Sie also zu weit weg von der WLAN-Basisstation sind, ist der Empfang eventuell nicht gut genug, um eine Verbindung aufrechterhalten zu können. Wände und andere Gegenstände zwischen Ihnen und der Basisstation können den Empfang auch schwächen.</p>

 <p>Das Netzwerksymbol im oberen Panel informiert Sie über die Stärke des Funksignals. Falls diese niedrig erscheint, versuchen Sie, näher an die Basisstation heranzukommen.</p>

</section>

<section id="network">
 <title>Die Netzwerkverbindung wurde nicht sauber erstellt</title>

 <p>Manchmal kommt es vor, dass wenn Sie sich mit einem Funknetzwerk verbinden, es erfolgreich gewesen zu sein scheint, Sie aber kurz danach getrennt werden. Das passiert normalerweise, weil Ihr Rechner die Verbindung nur teilweise herstellen konnte – er konnte zwar die Verbindung anbahnen, aber aus irgendeinem Grund nicht ganz fertigstellen und wurde dann getrennt.</p>

 <p>Ein möglicher Grund dafür ist, dass Sie die falsche Funknetzwerk-Passphrase eingegeben haben oder dass Ihrem Rechner die Verbindung zu diesem Netzwerk nicht erlaubt wurde (zum Beispiel weil das Netzwerk bei der Anmeldung einen Benutzernamen verlangt).</p>

</section>

<section id="hardware">
 <title>Unzuverlässige Funknetzwerkhardware/-treiber</title>

 <p>Manche Funknetzwerkgeräte können etwas unzuverlässig sein. Funknetzwerke sind recht kompliziert, so dass Netzwerkadapter und Basisstationen zuweilen auf kleinere Probleme stoßen und die Verbindung unterbrechen können. Dies ist ärgerlich, passiert aber mit vielen Geräten häufig. Falls Ihre Funknetzwerkverbindungen nur von Zeit zu Zeit unterbrochen werden, ist dies möglicherweise der einzige Grund. Falls das jedoch ziemlich oft passiert, sollten Sie sich nach einem anderen Gerät umschauen.</p>

</section>

<section id="busy">
 <title>Überlastete Funknetzwerke</title>

 <p>In Funknetzwerken an stark frequentierten Orten wie Universitäten oder in Cafés sind häufig zahlreiche Rechner zugleich angemeldet. Gelegentlich sind diese Netzwerke überlastet und können nicht mehr alle Rechner bedienen, die Verbindungsversuche unternehmen. In solchen Fällen werden einige Verbindungen getrennt.</p>

</section>

</page>
