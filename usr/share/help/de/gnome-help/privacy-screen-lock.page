<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="de">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hindern Sie andere Personen daran, Ihre Arbeitsumgebung zu verwenden, wenn Sie sich von Ihrem Rechner entfernen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Ihren Bildschirm automatisch sperren</title>
  
  <p>Wenn Sie Ihren Rechner verlassen, sollten Sie <link xref="shell-exit#lock-screen">den Bildschirm sperren</link>, um andere Personen daran zu hindern, Ihre Arbeitsumgebung zu benutzen und auf Ihre Dateien zuzugreifen. Sie können den Bildschirm automatisch nach einer festgelegten Zeit sperren lassen, falls Sie häufiger das Sperren vergessen. Somit wird Ihr Rechner besser gesichert, wenn Sie ihn nicht verwenden.</p>

  <note><p>Wenn Ihr Bildschirm gesperrt ist, werden Anwendungen und Systemprozesse weiter ausgeführt, aber Sie werden Ihr Passwort eingeben müssen, um Ihre Arbeit fortzusetzen.</p></note>
  
  <steps>
    <title>So legen Sie die Zeit bis zum automatischen Sperren des Bildschirms fest:</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Privatsphäre</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Privatsphäre</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Sperrbildschirm</gui>.</p>
    </item>
    <item>
      <p>Stellen Sie sicher, dass <gui>Automatische Sperrbildschirm</gui> <gui>AN</gui> ist und wählen Sie dann eine Zeit in der Auswahlliste.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Anwendungen können Benachrichtigungen ausgeben, die noch auf dem Sperrbildschirm angezeigt werden. Dies ist beispielsweise sinnvoll, um den Eingang einer E-Mail anzuzeigen, ohne dass der Bildschirm entsperrt werden muss. Wenn Sie nicht wollen, dass andere Benutzer diese Benachrichtigungen sehen, schalten Sie <gui>Benachrichtigungen anzeigen</gui> aus.</p>
  </note>

  <p>Wenn Ihr Bildschirm gesperrt ist und Sie ihn entsperren wollen, drücken Sie die <key>Esc</key>-Taste oder wischen Sie mit der Maus auf dem Bildschirm von unten nach oben. Geben Sie dann Ihr Passwort ein und drücken die <key>Eingabetaste</key> oder klicken auf <gui>Entsperren</gui>. Alternativ können Sie einfach mit der Eingabe Ihres Passworts beginnen, woraufhin der Sperrvorhang automatisch geöffnet wird.</p>

</page>
