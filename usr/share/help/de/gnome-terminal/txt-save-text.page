<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="txt-save-text" xml:lang="de">

  <info>
    <revision version="0.1" date="2013-02-17" status="candidate"/>
    <link type="guide" xref="index#textoutput"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Ausgaben im <app>Terminal</app> in eine Datei speichern.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jap@gnome.org</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008-2010, 2012-2013, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014, 2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernd Homuth</mal:name>
      <mal:email>dev@hmt.im</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

  <title>Inhalt speichern</title>

  <p>Sie können alle Ausgaben eines <app>Terminal</app>-Reiters oder -Fensters in eine Textdatei speichern. Dies kann nützlich sein, falls Sie die Terminalausgaben als Fehlerdiagnose-Information einreichen wollen.</p>

  <steps>
    <item>
      <p>Wählen Sie <gui style="menu">Datei</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui style="menuitem">Inhalt speichern …</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie Ihren gewünschten Ordner aus und geben Sie einen Dateinamen ein.</p>
    </item>
   <item>
      <p>Klicken Sie auf <gui style="button">Speichern</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Sie können bei Bedarf eine Dateiendung zum Dateinamen hinzufügen, zum Beispiel <input>.txt</input>. Dies kann nützlich sein, falls Sie die Datei in einem anderen Betriebssystem betrachten wollen.</p>
  </note>

</page>
