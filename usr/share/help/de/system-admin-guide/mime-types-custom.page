<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-custom" xml:lang="de">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

    <title>Einen benutzerdefinierten MIME-Typ für alle Benutzer hinzufügen</title>
    <p>Um einen benutzerdefinierten MIME-Typ für alle Benutzer des Systems hinzuzufügen und eine Vorgabeanwendung für diesen MIME-Typ zu registrieren, müssen Sie eine neue MIME-Typ-Spezifikationsdatei im Ordner <file>/usr/share/mime/packages/</file> und eine <file>.desktop</file>-Datei im Ordner <file>/usr/share/applications/</file> erstellen.</p>
    <steps>
      <title>Einen benutzerdefinierten MIME-Typ <sys>application/x-newtype</sys> für alle Benutzer hinzufügen</title>
      <item>
        <p>Legen Sie die Datei <file>/usr/share/mime/packages/application-x-newtype.xml</file> an:</p>
        <code mime="application/xml">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info"&gt;
  &lt;mime-type type="application/x-newtype"&gt;
    &lt;comment&gt;new mime type&lt;/comment&gt;
    &lt;glob pattern="*.xyz"/&gt;
  &lt;/mime-type&gt;
&lt;/mime-info&gt;</code>
      <p>Das Beispiel <file>application-x-newtype.xml</file> oben definiert einen neuen MIME-Typ <sys>application/x-newtype</sys> und verknüpft Dateinamen mit der Erweiterung <file>.xyz</file> mit diesem MIME-Typ.</p>
      </item>
      <item>
        <p>Legen Sie eine neue <file>.desktop</file>-Datei mit dem beispielhaften Namen <file>myapplication1.desktop</file> an und legen Sie diese im Ordner <file>/usr/share/applications/</file> ab:</p>
        <code>[Desktop Entry]
Type=Application
MimeType=application/x-newtype
Name=<var>My Application 1</var>
Exec=<var>myapplication1</var></code>
      <p>Die beispielhafte Datei <file>myapplication1.desktop</file> oben ordnet den MIME-Typ <sys>application/x-newtype</sys> einer Anwendung namens <app>My Application 1</app> zu, die mit dem Befehl <cmd>myapplication1</cmd> aufgerufen wird.</p>
      </item>
      <item>
        <p>Aktualisieren Sie (mit Systemverwalterrechten) die MIME-Datenbank, damit Ihre Änderungen wirksam werden:</p>
        <screen><output># </output><input>update-mime-database /usr/share/mime</input>
        </screen>
      </item>
      <item>
        <p>Aktualisieren Sie (mit Systemverwalterrechten) die Anwendungsdatenbank:</p>
        <screen><output># </output><input>update-desktop-database /usr/share/applications</input>
        </screen>
      </item>
      <item>
        <p>Um zu überprüfen, ob Sie <file>*.xyz</file>-Dateien den MIME-Typ <sys>application/x-newtype</sys> erfolgreich zugeordnet haben, legen Sie zunächst eine leere Datei an, zum Beispiel <file>test.xyz</file>:</p>
        <screen><output>$ </output><input>touch test.xyz</input></screen>
        <p>Führen Sie dann den Befehl <cmd>gio info</cmd> aus:</p>
        <screen><output>$ </output><input>gio info test.xyz | grep "standard::content-type"</input>
  standard::content-type: application/x-newtype</screen>
        </item>
        <item>
          <p>Um sicherzustellen, dass <file>myapplication1.desktop</file> korrekt als voreingestellte registrierte Anwendung für den MIME-Typ <sys>application/x-newtype</sys> eingerichtet wurde, führen Sie den Befehl <cmd>gio mime</cmd> aus:</p>
        <screen><output>$ </output><input>gio mime application/x-newtype</input>
Default application for “application/x-newtype”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
Recommended applications:
	myapplication1.desktop</screen>
      </item>
    </steps>
</page>
