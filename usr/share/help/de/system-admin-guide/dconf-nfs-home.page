<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-nfs-home" xml:lang="de">

  <info>
    <link type="guide" xref="setup"/>
    <link type="guide" xref="login#management"/>
    <revision version="0.1" date="2013-03-19" status="draft"/>
    <revision pkgversion="3.8" date="2013-05-09" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aktualisieren Sie die Systemkonfiguration für <sys>dconf</sys>, um Einstellungen in einem über <sys>NFS</sys> erreichbaren persönlichen Ordner zu speichern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Einstellungen über <sys>NFS</sys> speichern</title>

  <p>Damit <sys its:translate="no">dconf</sys> korrekt mit den persönlichen Ordnern im <sys>Network File System</sys> (<sys>NFS</sys>) umgehen kann, muss das <em>Schlüsseldatei-Backend</em> <sys its:translate="no">dconf</sys> verwendet werden.</p>

  <steps>
    <item>
      <p>Erstellen oder bearbeiten Sie die Datei <file its:translate="no">/etc/dconf/profile/user</file> auf jedem der Clients.</p>
    </item>
    <item>
      <p>Fügen Sie am Anfang dieser Datei folgende Zeile ein:</p>
      <code><input its:translate="no">service-db:keyfile/user</input></code>
    </item>
  </steps>
  <p>Das <sys its:translate="no">dconf</sys>-<em>Schlüsseldatei-Backend</em> wird erst bei der nächsten Anmeldung des Benutzers wirksam. Es schaut in der Schlüsseldatei, um zu ermitteln, ob diese aktualisiert worden ist, daher könnten Einstellungen nicht sofort aktualisiert werden.</p>
  <p>Wenn kein <em>Schlüsseldatei-Backend</em> verwendet wird, könnten die Benutzereinstellungen nicht korrekt ermittelt oder aktualisiert werden.</p>

</page>
