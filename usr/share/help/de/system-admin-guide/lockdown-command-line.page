<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-command-line" xml:lang="de">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.12" date="2014-06-18" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Benutzer am Zugriff auf die Befehlszeile hindern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Befehlszeilenzugriff deaktivieren</title>

  <p>Sie können Benutzer auf folgenden Wegen daran hindern, über virtuelle Terminals und Terminal-Anwendungen zuzugreifen:</p>

  <list>
    <item>
      <p>Benutzer daran hindern, mit <keyseq><key>Alt</key><key>F2</key></keyseq> eine Eingabeaufforderung zu öffnen.</p>
    </item>
    <item>
      <p>Wechsel in ein virtuelles Terminal (VT) deaktivieren.</p>
    </item>
    <item>
      <p>Entfernen Sie <app>Terminal</app> und alle anderen Terminal-Anwendungen aus der <gui>Aktivitäten</gui>-Übersicht. Sie müssen weiterhin verhindern, dass der Benutzer eine neue Terminal-Anwendung installiert.</p>
    </item>
  </list>

<section id="command-prompt">
  <title>Die Eingabeaufforderung deaktivieren</title>

  <steps>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <item>
      <p>Erstellen Sie eine <sys>local</sys>-Datenbank für systemweite Einstellungen in <file>/etc/dconf/db/local.d/00-lockdown</file>:</p>
      <code># Specify the dconf path
[org/gnome/desktop/lockdown]

# Disable the command prompt
disable-command-line=true</code>
    </item>
    <item>
      <p>So setzen Sie die Benutzereinstellung außer Kraft und verhindern, dass der Benutzer sie in <file>/etc/dconf/db/local.d/locks/lockdown</file> ändert:</p>
      <code># List the keys used to configure lockdown
/org/gnome/desktop/lockdown/disable-command-line</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>
</section>

<section id="virtual-terminal">
  <title>Wechsel in ein virtuelles Terminal deaktivieren</title>

  <p>Benutzer können normalerweise mit der Tastenkombination <keyseq><key>Strg</key><key>Alt</key><key><var>Funtionstaste</var></key></keyseq> (zum Beispiel <keyseq><key>Strg</key><key>Alt</key><key>F2</key></keyseq>) von der GNOME-Arbeitsumgebung in ein virtuelles Terminal wechseln.</p>

  <p>Falls auf dem Rechner das <em>X Window System</em> läuft, können Sie den Zugriff auf alle virtuellen Terminals durch Hinzufügen der Option <code>DontVTSwitch</code> zum Abschnitt <code>Serverflags</code> in der Datei <file>/etc/X11/xorg.conf.d</file> verhindern.</p>

  <steps>
    <item>
      <p>Erstellen Sie eine X-Konfigurationsdatei in <file>/etc/X11/xorg.conf.d</file>:</p>
      <code>Section "Serverflags"

Option "DontVTSwitch" "yes"

EndSection</code>
    </item>
    <item>
      <p>Ein Neustart von GDM ist erforderlich, damit die Änderungen wirksam werden.</p>
    </item>
  </steps>

</section>

<!-- TODO: add section for removing applications from the Activities overview. -->
</page>
