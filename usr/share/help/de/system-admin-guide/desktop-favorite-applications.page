<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-favorite-applications" xml:lang="de">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.8" date="2013-04-28" status="draft"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anpassen der vorgegebenen Standard-Anwendungen in der Aktivitäten-Übersicht.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Vorgegebene Standard-Anwendungen</title>

  <p>Favoriten sind jene Anwendungen, die im <link href="help:gnome-help/shell-terminology">Dash</link> sichtbar sind. Sie können <sys its:translate="no">dconf</sys> verwenden, um die Favoriten für einen einzelnen Benutzer oder gleiche Favoriten für alle Benutzer festzulegen. In beiden Fällen müssen Sie zuerst das <sys its:translate="no">dconf</sys>-Profil in <file its:translate="no">/etc/dconf/profile</file> bearbeiten.</p>

<section id="per-user">
  <title>Unterschiedliche Favoriten für verschiedene Benutzer festlegen</title>

  <p>Sie können die voreingestellten Favoriten für jeden Benutzer festlegen, indem Sie die jeweilige Benutzerdatenbank in <file its:translate="no">~/.config/dconf/user</file> bearbeiten. Das folgende Beispiel verwendet <sys its:translate="no">dconf</sys>, um <app>gedit</app>, <app>Terminal</app> und <app>Nautilus</app> als vorgegebene Favoriten für einen Benutzer festzulegen. Der Beispielcode ermöglicht Benutzern, später die Liste anzupassen, sofern gewünscht.</p>

  <!--The code itself need not be translated but the comments withing the
  code have to be translated.-Aruna-->

  <listing>
    <title>Inhalt von <file its:translate="no">/etc/dconf/profile</file>:</title>
      <code>
      #This line allows the user to change the default favorites later.
      user-db:user
      </code>
  </listing>

  <listing>
    <title>Inhalt von <file its:translate="no">~/.config/dconf/user</file>:</title>
      <code>
      #Set gedit, terminal and nautilus as default favorites
      [org/gnome/shell]
      favorite-apps = [<input>'gedit.desktop'</input>, <input>'gnome-terminal.desktop'</input>, <input>'nautilus.desktop'</input>]
      </code>
  </listing>

  <note style="tip">
    <p>Sie können die oben genannten Einstellungen auch <link xref="dconf-lockdown">sperren</link>, um Benutzer an deren Änderung zu hindern.</p>
  </note>

</section>

<section id="all-users">
  <title>Die gleichen Favoriten für alle Benutzer festlegen</title>

  <p>Damit alle Benutzer die gleichen Favoriten haben, müssen Sie die Systemdatenbankdateien mit Hilfe von <link xref="dconf-keyfiles">dconf-Schlüsseldateien</link> anpassen. Die folgenden Code-Schnipsel bearbeiten das Profil <sys its:translate="no">dconf</sys> und erstellen anschließend eine Schlüsseldatei, die voreingestellte Favoriten-Anwendungen für alle Mitarbeiter in der ersten Etage einer Abteilung festlegen.</p>

  <!--The code itself need not be translated but the comments within the
  code have to be translated.-Aruna-->

  <listing>
    <title>Inhalt von <file its:translate="no">/etc/dconf/profile</file>:</title>
      <code>
      user-db:user

      #This line defines a system database file called first_floor
      system-db:first_floor
      </code>
  </listing>

  <note style="info">
    <p>Einstellungen der Datenbank <code>user</code> (Benutzer) haben Vorrang vor Einstellungen der Datenbankdatei <code>first_floor</code>, aber <link xref="dconf-lockdown">angepasste Zugriffsrechte</link> der Datenbankdatei <code>first_floor</code> haben Vorrang vor jenen in <code>user</code>.</p>
  </note>

  <listing>
    <title>Inhalt von <file its:translate="no">/etc/dconf/db/first_floor.d/00_floor1_settings</file>:</title>
      <code>
      # Snippet sets gedit, terminal and nautilus as default favorites
      # for all users in the first floor

      [org/gnome/shell]
      favorite-apps = [<input>'gedit.desktop'</input>, <input>'gnome-terminal.desktop'</input>, <input>'nautilus.desktop'</input>]
      </code>
  </listing>

    <p>Binden Sie Ihre Änderungen in die Systemdatenbanken ein , indem Sie <cmd>dconf update</cmd> ausführen.</p>

</section>

</page>
