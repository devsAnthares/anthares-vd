<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="dconf" xml:lang="de">

  <info>
    <link type="guide" xref="setup"/>
    <revision pkgversion="3.8" date="2013-08-08" status="incomplete"/>
    <revision pkgversion="3.14" date="2014-06-17" status="incomplete"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="collaborator">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Was ist <sys>dconf</sys>? Wie kann es zum Bearbeiten von Konfigurationen verwendet werden?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Benutzer- und Systemeinstellungen mit dconf verwalten</title>

  <!-- TODO: improve wording throughout the page -->
  <p><sys>dconf</sys> ist eines der grundlegenden Konfigurationswerkzeuge in GNOME. Es soll Ihnen dabei helfen, die System- und Anwendungseinstellungen in GNOME zu verwalten.</p>

  <p>Wie mächtig <sys>dconf</sys> in GNOME ist, beweist die Tatsache, dass ein <sys>dconf</sys>-Schlüssel für fast jede Einstellung existiert, die ein Benutzer in der GNOME-Arbeitsumgebung selbst oder in jeder GNOME-Anwendung anklicken kann. Außerdem gibt es einige System- und Anwendungseinstellungen, die nicht direkt in der Benutzeroberfläche angepasst werden können, aber sehr wohl mit <sys>dconf</sys>.</p>

  <p>Für Sie als Systemverwalter bedeutet das, dass Ihnen Kenntnisse über die Benutzung von <sys>dconf</sys> dabei helfen werden, GNOME und GNOME-basierte Anwendungen so einzurichten, dass sie den Anforderungen Ihrer ganz bestimmten Umgebung am besten gerecht werden.</p>

  <!-- TODO: changes are written to the first database in the list, but
       multiple databases can be writable. -->
  <p>Ein <em>Profil</em> ist eine Liste von Konfigurationsdatenbanken. Die erste Datenbank in einem Profil ist jene, in die geschrieben wird, und die verbleibenden Datenbanken sind schreibgeschützt. Jede der Systemdatenbanken wird aus einem Schlüsseldatei-Ordner erzeugt. Jeder Schlüsseldatei-Ordner enthält eine oder mehrere Schlüsseldateien. Jede der Schlüsseldateien enthält mindestens einen dconf-Pfad sowie einen oder mehrere Schlüssel und deren zugehörige Werte.</p>

  <p>Schlüsselpaare, die in einem <sys>dconf</sys>-<em>Profil</em> gesetzt werden, setzen die Standardeinstellungen außer Kraft, außer wenn es ein Problem mit dem von Ihnen gesetzten Wert gibt.</p>

  <!-- TODO: databases -->
  <p/>

<section id="profile">
  <!-- TODO: explain the profile syntax (maybe new page) -->
  <title>Profile</title>
  <p>Üblicherweise wird Ihr <sys>dconf</sys>-Profil aus einer <em>Benutzerdatenbank</em> und mindestens einer Systemdatenbank bestehen. Im Profil muss pro Zeile eine Datenbank enthalten sein.</p>
  <p>Die erste Zeile in einem Profil benennt die Datenbank, in die die Änderungen geschrieben werden. Sie lautet üblicherweise <code>user-db:<input>Benutzer</input></code>, wobei <input>Benutzer</input> der Name der Benutzerdatenbank ist, welche Sie normalerweise in <file>~/.config/dconf</file> finden.</p>
  <example>
    <listing>
      <title>Beispielprofil</title>
      <code its:translate="no">user-db:user
system-db:<input>local</input>
system-db:<input>site</input></code>
    </listing>
  </example>
  <!--TODO: explain local and site -->
</section>

<!--section id="key-file">
  < TODO: link to dconf-keyfiles.page? >
  <p></p>

</section-->

</page>
