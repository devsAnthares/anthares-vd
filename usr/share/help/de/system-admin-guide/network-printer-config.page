<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="network-printer-config" xml:lang="de">

  <info>
   <link type="guide" xref="network"/>
   <revision pkgversion="3.8" date="2013-03-19" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wie kann ich meinen Samba-Drucker voreinrichten?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Einen Drucker einrichten</title>

  <p>Hier wird beschrieben, wie Drucker – speziell Windows-Drucker – in GNOME verfügbar gemacht werden können.</p>	

  <p>Es ist keine Vorkonfiguration notwendig, um Samba-Drucker im GNOME-Kontrollzentrum installieren zu können. Um einen neuen Samba-Drucker zu Ihrem CUPS-Server hinzuzufügen (Common UNIX Printing System), folgen Sie einfach den nachfolgend beschriebenen Schritten.</p>

  <steps>
    <title>Einrichten Ihres Druckers</title>
    <item>
      <p>Klicken Sie auf <em>Ihren Namen</em> in der oberen rechten Ecke und wählen Sie <gui>Einstellungen</gui> im Auswahlmenü.</p>
    </item>
    <item>
      <p>Klicken Sie auf das <gui>Drucker</gui>-Symbol.</p>
    </item>
    <item>
      <p>Entsperren Sie die <gui>Drucker</gui>-Einstellungen, indem Sie auf den <gui>Entsperren</gui>-Knopf klicken.</p>
    </item>
    <item>
      <p>Klicken Sie auf das <gui>+</gui> unterhalb der Liste der Drucker.</p>
    </item>
    <item>
      <p>Falls Ihre Drucker im Netzwerk verfügbar sind, warten Sie, bis die im Windows-Netzwerk eingerichteten Drucker erscheinen. Anderenfalls geben Sie die Adresse des Samba-Servers ein und drücken die <key>Eingabetaste</key>.</p>
    </item>
    <item>
      <p>Wenn Sie danach gefragt werden, legitimieren Sie sich an den Samba-Servern, die eine solche Legitimierung erfordern, um die verfügbaren Drucker anzuzeigen.</p>
    </item>
    <item>
      <p>Wählen Sie den zu installierenden Drucker aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Hinzufügen</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie aus den auf Ihrem Rechner verfügbaren Treibern den für Ihren Drucker geeigneten aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf <key>Auswählen</key>.</p>
    </item>
  </steps>

</page>
