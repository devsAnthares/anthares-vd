<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="session-custom" xml:lang="de">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eine benutzerdefinierte Sitzung durch Anlegen einer *.desktop-Datei erstellen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Eine benutzerdefinierte Sitzung erstellen</title>

 <p>Um Ihre eigene Sitzung mit benutzerdefinierter Konfiguration zu erstellen, gehen Sie wie folgt vor:</p>

<steps>
  <item><p>Legen Sie eine <file>.desktop</file>-Datei in <file>/etc/X11/sessions/<var>new-session</var>.desktop</file> an. Diese muss folgende Einträge enthalten:</p>
  <code>[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=<input>Custom session</input>
Comment=<input>This is our custom session</input>
Exec=<input>gnome-session --session=new-session</input></code>
   <p>Der <code>Exec</code>-Eintrag gibt den auszuführenden Befehl an, gegebenenfalls mit Argumenten. Sie können die benutzerdefinierte Sitzung mit dem Befehl <cmd>gnome-session --session=<var>neue_Sitzung</var></cmd> ausführen.</p>
   <p>Weitere Informationen zu den Parametern, die Sie mit <cmd>gnome-session</cmd> verwenden können, finden Sie in der <link href="man:gnome-session">Handbuchseite zu gnome-session</link>.</p>
  </item>
  <item><p>Erstellen Sie eine benutzerdefinierte Sitzungsdatei in <file>/usr/share/gnome-session/sessions/<var>new-session</var>.session</file>, wo Sie den Namen und die benötigten Komponenten für die Sitzung angeben können:</p>
   <code>[GNOME Session]
Name=<input>Custom Session</input>
RequiredComponents=<input>gnome-shell-classic;gnome-settings-daemon;</input></code>
  <p>Beachten Sie, dass für alles, was Sie in <code>RequiredComponents</code> angeben, eine zugehörige <file>.desktop</file>-Datei in <file>/usr/share/applications/</file> vorhanden sein muss.</p>
  </item>
</steps>

  <p>Nach dem Einrichten der benutzerdefinierten Sitzungsdateien sollte die neue Sitzung in der Sitzungsliste im GDM-Anmeldebildschirm angezeigt werden.</p>

  <section id="custom-session-issues">
  <title>Bekannte Probleme</title>
    <p>In Debian oder anderen auf Debian basierenden Systemen kann folgende Fehlermeldung ausgegeben werden:</p>

    <screen>Xsession: unable to launch ""
    Xsession --- "" not found; falling back to default
    session.</screen>

    <p>Sollte das bei Ihnen der Fall sein, nehmen Sie folgende Änderungen in der Datei <file>/etc/X11/Xsession.d/20x11-common_process-args</file> vor:</p>
    <steps>
    <item><p>Ändern Sie <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>"$1"</input>|| true)</code> zu <code>STARTUP_FULL_PATH=$(/usr/bin/which <input>$1</input> || true)</code></p></item>
    <item><p>Ändern Sie <code>STARTUP=<input>"$1"</input></code> zu <code>STARTUP=<input>$1</input></code></p></item>
    </steps>
  </section>

</page>
