<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-connect" xml:lang="mr">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Get on the internet — wirelessly.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>वायरलेस नेटवर्कशी जोडा</title>

<p>वायरलेस सक्रीय असलेले संगणक असल्यास, तुम्ही इंटरनेटकरिता प्रवेश प्राप्त करण्यासाठी वायरलेस नेटवर्कशी जोडणी करू शकता, नेटवर्कवरील शेअर्ड फाइल्स पाहू शकता, आणि पुढे.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#yourname">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>
      Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>Select Network</gui>.</p>
  </item>
  <item>
    <p>Click the name of the network you want, then click
    <gui>Connect</gui>.</p>
    <p>If the name of the network is not in the list, try clicking
    <gui>More</gui> to see if the network is further down the list. If you 
    still do not see the network, you may be out of range, or the network
    <link xref="net-wireless-hidden">might be hidden</link>.</p>
  </item>
  <item>
    <p>नेटवर्क पासवर्डसह (<link xref="net-wireless-wepwpa">एंक्रिप्शन कि</link>) सुरक्षित असल्यास, विनंती केल्यास पासवर्ड द्या आणि <gui>जोडणी करा</gui> क्लिक करा.</p>
    <p>If you do not know the key, it may be written on the underside of the
    wireless router or base station, or in its instruction manual, or you may
    have to ask the person who administers the wireless network.</p>
  </item>
  <item>
    <p>नेटवर्कशी जोडणी करण्याचा प्रयत्न करताना नेटवर्क चिन्ह अवतार बदलेल.</p>
  </item>
  <item>
    <p>If the connection is successful, the icon will change to a dot with
    several curved bars above it
    (<media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>). More bars
    indicate a stronger connection to the network. Fewer bars mean the
    connection is weaker and might not be very reliable.</p>
  </item>
</steps>

  <p>If the connection is not successful, you may be asked for your password
  again or it might just tell you that the connection has been disconnected.
  There are a number of things that could have caused this to happen. You could
  have entered the wrong password, the wireless signal could be too weak, or
  your computer’s wireless card might have a problem, for example. See
  <link xref="net-wireless-troubleshooting"/> for more help.</p>

  <p>वायरलेस नेटवर्कची मजबूत जोडणीचा अर्थ तुमच्याकडे वेगवान इंटरनेट जोडणी आहे, किंवा तुमच्याकडे वेगवान डाउनलोड गती आहे असे नाही. वायरेस जोडणी संगणकाला  <em>साधनाशी जोडणी करतो जे इंटरनेट जोडणी पुरवते</em> (जसे कि राउटर किंवा मोडेम), परंतु दोन जोडणी प्रत्यक्षरित्या वेगळे आहेत, म्हणून वेगळ्या गतींवर चालेल.</p>

</page>
