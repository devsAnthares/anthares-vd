<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="mr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>गरज नसलेले फाइल्स किंवा फोल्डर्स काढून टाका.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>फाइल्स आणि फोल्डर्स काढून टाका</title>

  <p>If you do not want a file or folder any more, you can delete it. When you
  delete an item it is moved to the <gui>Trash</gui> folder, where it is stored
  until you empty the trash. You can <link xref="files-recover">restore
  items</link> in the <gui>Trash</gui> folder to their original location if you
  decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>फाइलला कचरा कुंडीत पाठवण्यासाठी:</title>
    <item><p>इच्छित फाइल कचरा कुंडीत टाकायला निवडण्यासाठी त्यावर एकदा क्लिक करा.</p></item>
    <item><p>Press <key>Delete</key> on your keyboard. Alternatively, drag the
    item to the <gui>Trash</gui> in the sidebar.</p></item>
  </steps>

  <p>The file will be moved to the trash, and you’ll be presented with an
  option to <gui>Undo</gui> the deletion. The <gui>Undo</gui> button will appear
  for a few seconds. If you select <gui>Undo</gui>, the file will be restored
  to its original location.</p>

  <p>नेहमीकरिता फाइल्स नष्ट करण्यासाठी, आणि संगणकावरील डिस्कची जागा मोकळी करण्यासाठी, तुम्हाला कचरापेटी रिकामे करावे लागेल. कचरापेटी रिकामे करण्यासाठी, बाजूच्यापट्टीमध्ये <gui>कचरापेटी</gui> उजवी-क्लिक करा आणि <gui>कचरापेटी रिकामे करा</gui> निवडा.</p>

  <section id="permanent">
    <title>फाइल कायमची काढून टाका</title>
    <p>तुम्ही नेहमीकरिता फाइल नष्ट करू शकता, कचरापेटीकडे फाइलला न पाठवता.</p>

  <steps>
    <title>फाइल कायमची काढून टाकण्यासाठी:</title>
    <item><p>तुम्हाला जी वस्तु काढायची आहे ती निवडा.</p></item>
    <item><p><key>Shift</key> कि दाबा आणि दाबून ठेवा, त्यानंतर कळफलकावरील <key>Delete</key> कि दाबा.</p></item>
    <item><p>यास पूर्ववत् करणे अशक्य असल्यामुळे, तुम्हाला फाइल किंवा फोल्डर नष्ट करायचे आहे याच्या खात्रीकरिता विचारले जाईल.</p></item>
  </steps>

  <note><p><link xref="files#removable">काढून टाकण्याजोगी साधन </link> वरील नष्ट केलेल्या फाइल्स इतर कार्यप्रणालीवर, जसे कि Windows किंवा Mac OS वर दृश्यास्पद नसू शकते. फाइल्स अजूनही तेथेच आहेत, आणि साधनाला पुन्हा संगणकाशी जुळल्यास उपलब्ध होईल.</p></note>

  </section>

</page>
