<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="mr">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Check the <gui>Activities</gui> overview or other workspaces.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>हरवलेलं पटल शोधा</title>

  <p>A window on a different workspace, or hidden behind another window, is
  easily found using the <gui xref="shell-introduction#activities">Activities</gui>
  overview:</p>

  <list>
    <item>
      <p>Open the <gui>Activities</gui> overview. If the missing window is on
      the current
      <link xref="shell-windows#working-with-workspaces">workspace</link>, it
      will be shown here in thumbnail. Simply click the thumbnail to redisplay
      the window, or</p>
    </item>
    <item>
      <p>Click different workspaces in the
      <link xref="shell-workspaces">workspace selector</link> at the
      right-hand side of the screen to try to find your window, or</p>
    </item>
    <item>
      <p>डॅशमधील ॲप्लिकेशनवर उजवी-क्लिक द्या आणि त्यांचे खुले पटल दाखवले जाईल. वापरायचे असल्यास सूचीतील पटलावर क्लिक करा.</p>
    </item>
  </list>

  <p>पटल परिवर्तक वापरणे:</p>

  <list>
    <item>
      <p>Press
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      to display the <link xref="shell-windows-switching">window switcher</link>.
      Continue to hold down the <key>Super</key> key and press <key>Tab</key>
      to cycle through the open windows, or
      <keyseq><key>Shift</key><key>Tab</key> </keyseq> to cycle backwards.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>ॲप्लिकेशनकडे एकापेक्षा जास्त खुले पटल असल्यास, <key>Super</key> दाबून ठेवा आणि वापरण्याकरिता <key>`</key> (किंवा <key>Tab</key> वरील कि) दाबा.</p>
    </item>
  </list>

</page>
