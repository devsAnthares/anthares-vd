<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="mr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>संगणक सस्पेंड झाल्यानंतर आणि योग्यरित्या पुन्हा सुरू न झाल्यास काही वायरलेस साधनांमध्ये अडचण निर्माण होते.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>माझे संगणक वेक अप झाल्यानंतर वायरलेस नेटवर्क आढळत नाही</title>

  <p>If you have suspended your computer, you may find that your wireless
  internet connection does not work when you resume it again. This happens when
  the <link xref="hardware-driver">driver</link> for the wireless device does
  not fully support certain power saving features.
  Typically, the wireless connection fails to turn on properly when the
  computer is resumed.</p>

  <p>असे झाल्यास, वायरलेस बंद करा आणि पुन्हा सुरू करा:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Network</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Switch the wireless <gui>OFF</gui> and then <gui>ON</gui> again.</p>
    </item>
    <item>
      <p>If the wireless still does not work, switch <gui>ON</gui> the
      <gui>Airplane Mode</gui> and then switch it <gui>OFF</gui> again.</p>
    </item>
  </steps>

  <p>If this does not work, restarting your computer should make the wireless
  work again. If you are still having problems after that, connect to the
  internet using an Ethernet cable and update your computer.</p>

</page>
