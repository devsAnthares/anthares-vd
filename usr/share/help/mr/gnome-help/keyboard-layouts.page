<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="mr">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>
    <link type="guide" xref="keyboard-shortcuts-set"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>२०१२</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>२०१३</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>किबोर्ड शार्टकट्स समाविष्ट करा आणि बदल करत रहा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>पर्यायी कीबोर्ड मांडणी वापरा</title>

  <p>कळफलक विविध भाषांकरिता विविध मांडणीमध्ये उपलब्ध होतात. एका भाषाकरिता सुद्धा, सहसा एकापेक्षा जास्त कळफलक मांडणी असतात, जसे कि इंग्रजीकरिता ड्वोराक मांडणी. वेगळ्या मांडणीसह कळफलकाचे वापर करणे शक्य आहे, किजवरील छपाई केलेल्या अक्षरे आणि चिन्हांना उपेक्षित. एकापेक्षा जास्त भाषांचा वापर करायचे असल्यास हे उपयोगी ठरते.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Region &amp; Language</gui> in the sidebar to open the
      panel.</p>
    </item>
    <item>
      <p>Click the <gui>+</gui> button in the <gui>Input Sources</gui> section,
      select the language which is associated with the layout, then select a
      layout and press <gui>Add</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>

    <p>Some rarely used keyboard layout variants are not available by default
    when you click the <gui>+</gui> button. To make also those input sources
    available you can open a terminal window by pressing
    <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>
    and run this command:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>You can preview an image of any layout by selecting it in the list of
    <gui>Input Sources</gui> and clicking
    <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">preview</span></media></gui></p>
  </note>

  <p>Certain languages offer some extra configuration options. You can
  identify those languages because they have a
  <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">preview</span></media></gui>
  icon next to them. If you want to access these extra parameters, select the
  language from the <gui>Input Source</gui> list and a new
  <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">preferences</span></media></gui>
  button will give you access to the extra settings.</p>

  <p>When you use multiple layouts, you can choose to have all windows use the
  same layout or to set a different layout for each window. Using a different
  layout for each window is useful, for example, if you’re writing an article
  in another language in a word processor window. Your keyboard selection will
  be remembered for each window as you switch between windows. Press the
  <gui style="button">Options</gui> button to select how you want to manage
  multiple layouts.</p>

  <p>शीर्ष पट्टी सध्याच्या मांडणीकरिता छोटे आइडेंटिफायर दाखवेल, जसे कि मान इंग्रजीच्या मांडणीकरिता <gui>en</gui>. मांडणी निर्देशक क्लिक करा आणि मेन्युपासून वापरण्याजोगी मांडणीची निवड करा. निवडलेल्या भाषांमध्ये अगाऊ सेटिंग्ज असल्यास, त्यास उपलब्ध मांडणी खाली दाखवले जाईल. यामुळे सेटिंग्जचे जलद पूर्वावलोकन प्राप्त होते. संदर्भकरिता सध्याच्या कळफलक मांडणीसह तुम्ही प्रतिमा देखील उघडू शकता.</p>

  <p>The fastest way to change to another layout is by using the 
  <gui>Input Source</gui> <gui>Keyboard Shortcuts</gui>. These shortcuts open 
  the <gui>Input Source</gui> chooser where you can move forward and backward. 
  By default, you can switch to the next input source with 
  <keyseq><key xref="keyboard-key-super">Super</key><key>Space</key></keyseq>
  and to the previous one with
  <keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq>. You can
  change these shortcuts in the <gui>Keyboard</gui> settings.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
