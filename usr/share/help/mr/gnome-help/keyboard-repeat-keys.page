<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="mr">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>कि दाबून ठेवल्यानंतर कळफलकने अक्षरांची पुनरावृत्ती करू नये, किंवा रिपिट किजचा विलंब आणि वेग बदला.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Manage repeated key presses</title>

  <p>By default, when you hold down a key on your keyboard, the letter
  or symbol will be repeated until you release the key. If you have
  difficulty picking your finger back up quickly enough, you can
  disable this feature, or change how long it takes before key presses
  start repeating, or how quickly key presses repeat.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
     <p>Press <gui>Repeat Keys</gui> in the <gui>Typing</gui> section.</p>
    </item>
    <item>
      <p>Switch <gui>Repeat Keys</gui> to <gui>OFF</gui>.</p>
      <p>वैकल्पिकरित्या, पुनरावृत्ती सुरू करण्यासाठी कितीवेळ तुम्हाला कि दाबून ठेवावे लागेल त्याकरिता <gui>विलंब</gui> स्लाइडर सुस्थीत करा, आणि किती पटकन कि प्रेसेसची पुनरावृत्ती करायची त्याकरिता <gui>वेग</gui> स्लाइडर सुस्थीत करा.</p>
    </item>
  </steps>

</page>
