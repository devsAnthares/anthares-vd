<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="mr">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set an image, color, or gradient as your desktop background or lock
    screen background.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Change the desktop and lock screen backgrounds</title>

  <p>You can change the image used for your backgrounds or set it to be a
  solid color.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Background</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Background</gui> or <gui>Lock Screen</gui>.</p>
    </item>
    <item>
      <p>सगळ्यात वर तीन पर्याय दाखवलेले आहेत:</p>
      <list>
        <item>
          <p>Select <gui>Wallpapers</gui> to use one of the many professional
          background images that ship with GNOME. Some wallpapers change
          throughout the day. These wallpapers have a small clock icon in the
          bottom-right corner.</p>
        </item>
        <item>
          <p>Select <gui>Pictures</gui> to use one of your own photos from your
          <file>Pictures</file> folder. Most photo management applications
          store photos there. If you would like to use an image that is not in
          your Pictures folder, either use <app>Files</app> by right-clicking
          on the image file and selecting <gui>Set as Wallpaper</gui>, or
          <app>Image Viewer</app> by opening the image file, clicking the
          menu button in the titlebar and selecting <gui>Set as
          Wallpaper</gui>.</p>
        </item>
        <item>
          <p>Select <gui>Colors</gui> to just use a flat color.</p>
        </item>
      </list>
    </item>
    <item>
      <p>सेटिंग लगेच लागू होतात.</p>
    </item>
    <item>
      <p>संपूर्ण डेस्कटॉपच्या अवलोकनकरिता <link xref="shell-workspaces-switch">रिकाम्या कार्यक्षेत्राचा वापर करा</link>.</p>
    </item>
  </steps>

</page>
