<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="mr">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>VNCचा वापर करून इतर वापरकर्त्यांना तुमचे डेस्कटॉप पहाण्यास आणि संपर्क साधण्यास परवानगी द्या.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>तुमचा डेस्कटॉप शेअर करा</title>

  <p>तुम्ही डेस्कटॉप अवलोकन ॲप्लिकेशनच्या सहाय्याने इतर वापरकर्त्यांना दुसऱ्या संगणकापासून तुमचे डेस्कटॉपचे अवलोकन आणि नियंत्रणकरिता परवानगी देऊ शकता. इतरांना डेस्कॉपकरिता प्रवेशसाठी <gui>स्क्रीन शेअरिंग</gui>ला संरचीत करा आणि सुरक्षा प्राधन्यता सेट करा.</p>

  <note style="info package">
    <p><gui>स्क्रीन शेअरिंग</gui> दृष्यास्पद करण्याकरिता तुमच्याकडे <app>विनो</app> संकुल इंस्टॉल केलेले पाहिजे.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Vino इंस्टॉल करा</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>शेअरिंग</gui> <gui>बंद</gui> असल्यास, त्यास <gui>सुरू करा</gui>.</p>

      <note style="info"><p>If the text below <gui>Computer Name</gui> allows
      you to edit it, you can <link xref="sharing-displayname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p><gui>स्क्रीन शेअरिंग</gui> निवडा.</p>
    </item>
    <item>
      <p>To let others view your desktop, switch <gui>Screen Sharing</gui> to
      <gui>ON</gui>. This means that other people will be able to attempt to
      connect to your computer and view what’s on your screen.</p>
    </item>
    <item>
      <p>To let others interact with your desktop, switch
      <gui>Allow Remote Control</gui> to <gui>ON</gui>. This may allow the
      other person to move your mouse, run applications, and browse files on
      your computer, depending on the security settings which you are currently
      using.</p>

      <note style="tip">
        <p>This option is enabled by default when <gui>Screen Sharing</gui> is
        <gui>ON</gui>.</p>
      </note>

    </item>
  </steps>

  <section id="security">
  <title>सुरक्षा</title>

  <p>बदलण्यापूर्वी प्रत्येक सुरक्षा पर्यायची संपूर्ण व्याप्ति गृहीत धरणे खूप महत्वाचे आहे.</p>

  <terms>
    <item>
      <title>नवीन जोडण्यांनी प्रवेशकरिता विनंती केली पाहिजे</title>
      <p>If you want to be able to choose whether to allow someone to access
      your desktop, enable <gui>New connections must ask for access</gui>.  If
      you disable this option, you will not be asked whether you want to allow
      someone to connect to your computer.</p>
      <note style="tip">
        <p>हा पर्याय पुर्वनिर्धारितपणे लागू आहे.</p>
      </note>
    </item>
    <item>
      <title>Require a Password</title>
      <p>To require other people to use a password when connecting to your
      desktop, enable <gui>Require a Password</gui>. If you do not use this
      option, anyone can attempt to view your desktop.</p>
      <note style="tip">
        <p>हा पर्याय पूर्वनिर्धारितपणे बंद केले जाते, परंतु त्यास सुरू करा आणि सुरक्षित पासवर्ड सेट करा.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the <gui>ON | OFF</gui> switch next to each to
  choose where your desktop can be shared.</p>
  </section>

  <section id="disconnect">
  <title>Stop sharing your desktop</title>

  <p>To disconnect someone who is viewing your desktop:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p><gui>Screen Sharing</gui> will show as <gui>Active</gui>. Click on
      it.</p>
    </item>
    <item>
      <p>Toggle the switch at the top to <gui>OFF</gui>.</p>
    </item>
  </steps>

  </section>


</page>
