<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="mr">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer" group="#first"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>संगणकाशी जुळलेल्या छपाईयंत्राची मांडणी करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>स्थानिक प्रिंटर ठरवा</title>

  <p>Your system can recognize many types of printers automatically once
  they are connected. Most printers are connected with a USB cable that
  attaches to your computer.</p>

  <note style="tip">
    <p>You do not need to select whether you want to install a network or local
    printer now. They are listed in one window.</p>
  </note>

  <steps>
    <item>
      <p>प्रिंटर सुरु आहे याची खात्री करा.</p>
    </item>
    <item>
      <p>योग्य केबलसह छपाईयंत्राची प्रणालीशी जोडणी करा. प्रणाली ड्राइव्हर्सकरिता शोध घेतेवेळी तुम्हाला पडद्यावर कृती दिसतील, आणि तुम्हाला इंस्टॉल करण्यासाठी ओळख पटवण्यास विचारले जाईल.</p>
    </item>
    <item>
      <p>प्रणालीचे छपाईयंत्र इंस्टॉल करणे पूर्ण झाल्यानंतर संदेश आढळेल. चाचणी पृष्ठाची छपाई करण्यासाठी <gui>चाचणी पृष्ठाची छपाई करा</gui>, किंवा छपाईयंत्र मांडणीमध्ये अगाऊ बदल करण्यासाठी <gui>पर्याय</gui> निवडा.</p>
    </item>
  </steps>

  <p>If your printer was not set up automatically, you can add it in the
  printer settings:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p><gui>प्रिंटर्स</gui> क्लिक करा.</p>
    </item>
    <item>
      <p>उजव्या कोपऱ्यातील <key>कुलूपबंद अशक्य</key> बटन क्लिक करा आणि पासवर्ड द्या.</p>
    </item>
    <item>
      <p>Click the <gui>+</gui> button.</p>
    </item>
    <item>
      <p>पॉपअप पटलात, नविन छपाईयंत्राची निवड करा. <gui>समाविष्ट करा</gui> क्लिक करा.</p>
    </item>
  </steps>

  <p>If your printer does not appear in the Add a New Printer window, you may
  need to install print drivers.</p>

  <p>छपाईयंत्र इंस्टॉल केल्यानंतर, तुम्हाला <link xref="printing-setup-default-printer">पूर्वनिर्धारित छपाईयंत्र बदलायला</link> आवडेल.</p>

</page>
