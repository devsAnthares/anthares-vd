<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="mr">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu डॉक्युमेंटेशन विकीचे अंशदाते</name>
    </credit>
    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>सोपे नेटवर्क सेटिंग्ज योग्य आहे याची खात्री करा आणि पुढील त्रुटीनिवारण टप्प्यांकरिता तयारी करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>वायरलेस नेटवर्क समस्या निवारक</title>
  <subtitle>प्रारंभिक जोडणी तपास करा</subtitle>

  <p>In this step you will check some basic information about your wireless
  network connection. This is to make sure that your networking problem isn’t
  caused by a relatively simple issue, like the wireless connection being
  turned off, and to prepare for the next few troubleshooting steps.</p>

  <steps>
    <item>
      <p>लॅपटॉप <em>वायर्ड</em> इंटरनेट जोडणीशी जोडले नाही याची खात्री करा.</p>
    </item>
    <item>
      <p>तुमच्याकडे एक्सटर्नल वायरलेस अडऍप्टर असल्यास (जसे कि USB अडॅप्टर, किंवा PCMCIA कार्ड जे लॅपटॉपला जोडले जाते), ते संगणकातील योग्य स्लॉटमध्ये घट्टपणे बसले आहे याची खात्री करा.</p>
    </item>
    <item>
      <p>संगणकाच्या <em>आतमध्ये</em> वायरलेस कार्ड असल्यास, वायरलेस स्विच सुरू केली आहे याची खात्री करा (असल्यास). लॅपटॉप्सकडे सहसा वायरलेस स्विच असते ज्यास विविध कळफलक किजच्या जोडणीने दाबून टॉगल करणे शक्य आहे.</p>
    </item>
    <item>
      <p>Click the system status area on the top bar and select
      <gui>Wi-Fi</gui>, then select <gui>Wi-Fi Settings</gui>. Make sure that
      <gui>Wi-Fi</gui> is set to <gui>ON</gui>. You should also check that
      <link xref="net-wireless-airplane">Airplane Mode</link> is <em>not</em>
      switched on.</p>
    </item>
    <item>
      <p>Open the Terminal, type <cmd>nmcli device</cmd> and press
      <key>Enter</key>.</p>
      <p>This will display information about your network interfaces and
      connection status. Look down the list of information and see if there is
      an item related to the wireless network adapter. If the state is
      <code>connected</code>, it means that the adapter is working and connected
      to your wireless router.</p>
    </item>
  </steps>

  <p>वायरलेस राउटरशी जोडणी केली असल्यावरही, इंटरनेटकरिता प्रवेश नसल्यास, तुमचे राउटर योग्यरित्या कार्य करणार नाही, किंवा इंटरनेट सर्व्हिस प्रोव्हाइडर (ISP) कदाचित तांत्रिक अडचणी अनुभवत असावे. राउटर आणि ISP सेटअप गाइड्सचे पूर्वावलोकन करा आणि सेटिंग्ज अचूक आहे याची खात्री करा, किंवा सपोर्टकरिता ISP सह संपर्क करा.</p>

  <p>If the information from <cmd>nmcli device</cmd> did not indicate that you were
  connected to the network, click <gui>Next</gui> to proceed to the next
  portion of the troubleshooting guide.</p>

</page>
