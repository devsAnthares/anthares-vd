<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="mr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zoom in on your screen so that it is easier to see things.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>Magnify a screen area</title>

  <p><link xref="a11y-font-size">मजकूर आकार</link> वाढवणे स्क्रीन विस्तारित करण्यापेक्षा भिन्न आहे. हे गुणविशेष एका वर्धकाप्रमाणे आहे, जे तुम्हाला स्क्रीनचे विविध भाग मोठे करण्यास परवानगी देते.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press on <gui>Zoom</gui> in the <gui>Seeing</gui> section.</p>
    </item>
    <item>
      <p>Switch <gui>Zoom</gui> to <gui>ON</gui> in the top-right corner of the
      <gui>Zoom Options</gui> window.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>तुम्ही आत्ता स्क्रीन क्षेत्राच्या भवती हलवू शकता. स्क्रीनच्या किनाऱ्यांवरून माऊस हलवून, तुम्ही विस्तारीत क्षेत्रात विविध ठिकाणी हलवू शकता, ज्यामुळे पसंतीचा क्षेत्र पहाणे शक्य होते.</p>

  <note style="tip">
    <p>वैकल्पिकरित्या, शीर्ष पट्टीवर <link xref="a11y-icon">ॲक्सेसिबिलिटि चिन्ह</link> क्लिक करून आणि <gui>झू</gui> नीवडून पटकन झूम सुरू किंवा बंद करणे शक्य आहे.</p>
  </note>

  <p>You can change the magnification factor, the mouse tracking, and the
  position of the magnified view on the screen. Adjust these in the
  <gui>Magnifier</gui> tab of the <gui>Zoom Options</gui> window.</p>

  <p>You can activate crosshairs to help you find the mouse or touchpad
  pointer. Switch them on and adjust their length, color, and thickness in the
  <gui>Crosshairs</gui> tab of the <gui>Zoom</gui> settings window.</p>

  <p>You can switch to inverse video or <gui>White on black</gui>, and adjust
  brightness, contrast and greyscale options for the magnifier. The combination
  of these options is useful for people with low-vision, any degree of
  photophobia, or just for using the computer under adverse lighting
  conditions. Select the <gui>Color Effects</gui> tab in the <gui>Zoom</gui>
  settings window to enable and change these options.</p>

</page>
