<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="mr">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Remove access to an online service provider from your
    applications.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>खातं काढून टाका</title>

  <p>You can remove an online account which you no longer wish to use.</p>

  <note style="tip">
    <p>Many online services provide an authorization token which GNOME stores
    instead of your password. If you remove an account, you should also revoke
    that certificate in the online service. This will ensure that no other
    application or website can connect to that service using the authorization
    for GNOME.</p>

    <p>How to revoke the authorization depends on the service provider. Check
    your settings on the provider’s website for authorized or connected apps
    or sites. Look for an app called “GNOME” and remove it.</p>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Online Accounts</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Online Accounts</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the account which you wish to remove.</p>
    </item>
    <item>
      <p>Click the <gui>-</gui> button in the lower-left corner of the
      window.</p>
    </item>
    <item>
      <p>Click <gui>Remove</gui> in the confirmation dialog.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Instead of deleting the account completely, it is possible to
    <link xref="accounts-disable-service">restrict the services</link> accessed
    by your desktop.</p>
  </note>
</page>
