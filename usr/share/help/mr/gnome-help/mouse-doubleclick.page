<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="mr">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>दोनवेळा-क्लिककरिता दुसऱ्यावेळी माउस बटन दाबण्याकरिता किती पटकन दाबणे नियंत्रीत करा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>डबल-क्लिकच्या गतीत बदल करा</title>

  <p>Double-clicking only happens when you press the mouse button twice
  quickly enough. If the second press is too long after the first, you’ll
  just get two separate clicks, not a double click. If you have difficulty
  pressing the mouse button quickly, you should increase the timeout.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Under <gui>Pointing &amp; Clicking</gui>, adjust the
      <gui>Double-Click Delay</gui> slider to a value you find comfortable.</p>
    </item>
  </steps>

  <p>डबल-क्लिक वेळसमाप्ति वाढवल्यानंतरही, सिंगल-क्लिक आवश्यक असताना माउस डबल-क्लिक करत असल्यास, तुमचा माउस सदोषीत असू शकतो. संगणकाशी वेगळ्या माउसची जोडणी करून पहा आणि ते योग्यरित्या कार्य करते, त्याची तपासणी करा. वैकल्पिकरित्या, माउसला वेगळ्या संगणकाशी जोडणी करा आणि अडचणीचे निवारण झाले किंवा नाही याची खात्री करा.</p>

  <note>
    <p>ही सेटिंग दोन्ही माउस आणि टचपॅडला, तसेच इतर पॉइंटिंग साधनाला प्रभावीत करते.</p>
  </note>

</page>
