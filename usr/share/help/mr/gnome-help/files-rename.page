<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="mr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>फाइल किंवा फोल्डर नाव बदला.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>फाइल किंवा फोल्डर नामांतर करा</title>

  <p>As with other file managers, you can use <app>Files</app> to change the
  name of a file or folder.</p>

  <steps>
    <title>फाइल किंवा फोल्डर नामांतर करण्यासाठी:</title>
    <item><p>घटकावर उजवी-क्लिक द्या आणि <gui>पुन्हानाव द्या</gui> निवडा, किंवा फाइल निवडा आणि <key>F2</key> दाबा.</p></item>
    <item><p>Type the new name and press <key>Enter</key> or click
    <gui>Rename</gui>.</p></item>
  </steps>

  <p><link xref="nautilus-file-properties-basic">गुणधर्म</link> पटलातून फाइलला पुन्हा नाव देणे शक्य आहे.</p>

  <p>When you rename a file, only the first part of the name of the file is
  selected, not the file extension (the part after the last <file>.</file>).
  The extension normally denotes what type of file it is (for example,
  <file>file.pdf</file> is a PDF document), and you usually do not want to
  change that. If you need to change the extension as well, select the entire
  file name and change it.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the toolbar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>फाइल नावांसाठी वैध अक्षरे</title>

    <p>You can use any character except the <file>/</file> (slash) character in
    file names. Some devices, however, use a <em>file system</em> that has more
    restrictions on file names. Therefore, it is a best practice to avoid the
    following characters in your file names: <file>|</file>, <file>\</file>,
    <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>,
    <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>If you name a file with a <file>.</file> as the first character, the
    file will be <link xref="files-hidden">hidden</link> when you attempt to
    view it in the file manager.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>समान प्रश्ने</title>

    <terms>
      <item>
        <title>फाइल नाव आधीपासून वापरात आहे</title>
        <p>You cannot have two files or folders with the same name in the same
        folder. If you try to rename a file to a name that already exists in
        the folder you are working in, the file manager will not allow it.</p>
        <p>फाइल आणि फोल्डर नावे केस संवदेनशील आहे, म्हणूनच फाइल नाव <file>File.txt</file> हे <file>FILE.txt</file> प्रमाणे नाही. वेगळ्या फाइल नावांचा वापर करणे स्वीकार्य आहे, जरी ते शिफारसीय नाही.</p>
      </item>
      <item>
        <title>फाइल नाव अति लांब आहे</title>
        <p>On some file systems, file names can have no more than 255
        characters in their names.  This 255 character limit includes both the
        file name and the path to the file (for example,
        <file>/home/wanda/Documents/work/business-proposals/…</file>), so you
        should avoid long file and folder names where possible.</p>
      </item>
      <item>
        <title>नामांतराचा पर्याय बाहेर आलेला आहे</title>
        <p><gui>पुन्हा नाव देणे</gui> ग्रेए आउट केल्यानंतर, तुमच्याकडे फाइलला पुन्हनाव देण्याची परवानगी राहत नाही. उर्वरित फाइल्ससह सावधगिरी बाळगा, कारणा उर्वरित काही सुरक्षित फाइल्समुळे प्रणाली वापरण्याजोगी राहणार नाही. अधिक माहितीकरिता <link xref="nautilus-file-properties-permissions"/> पहा.</p>
      </item>
    </terms>

  </section>

</page>
