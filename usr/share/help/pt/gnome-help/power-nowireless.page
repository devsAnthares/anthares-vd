<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="pt">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alguns dispositivos sem fios têm problemas de manejo quando envia o seu computador a dormir e não acordam corretamente.</desc>
  </info>

  <title>Não tenho conexão sem fios à rede quando acordo minha computador</title>

  <p>Se tem suspendido o seu computador, pode encontrar que a sua conexão sem fios a Internet não funciona quando se acorda de novo. Isto sucede quando os <link xref="hardware-driver">controladores</link> do dispositivo sem fios, não são totalmente compatíveis com determinadas caraterísticas de poupança de energia. Normalmente, a conexão sem fios não se acende corretamente quando a computador se reativa.</p>

  <p>Se isto passa, tente apagar seu adaptador sem fios e o voltar a acender:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Rede</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Rede</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione <gui>sem fios</gui>.</p>
    </item>
    <item>
      <p><gui>Apague</gui> seu cartão sem fios e volte a <gui>acendê-la</gui>.</p>
    </item>
    <item>
      <p>Se a rede sem fios segue sem funcionar, <gui>ative</gui> o <gui>Modo avião</gui> e <gui>desative-o</gui> de novo.</p>
    </item>
  </steps>

  <p>Se isto não funciona, reiniciar a computador deveria fazer que a conexão sem fios funcione de novo. Se segue tendo problemas após isto, se ligue a Internet utilizando um cabo e atualize o seu computador.</p>

</page>
