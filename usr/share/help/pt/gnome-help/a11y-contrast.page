<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-contrast" xml:lang="pt">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Make windows and buttons on the screen more (or less) vivid, so
    they’re easier to see.</desc>
  </info>

  <title>Ajustar o contraste</title>

  <p>You can adjust the contrast of windows and buttons so that they’re easier
  to see. This is not the same as <!--display-dimscreen is a stub. Please add
  the link back when it's unstubbed.-Aruna<link xref="display-dimscreen">-->
  changing the brightness of the whole screen; only parts of the
  <em>user interface</em> will change.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Acesso universal</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Acesso universal</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Ative a opção <gui>Alto contraste</gui> (<gui>Acendido</gui>) na seção <gui>Visão</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ativar e desativar rapidamente o contraste alto</title>
    <p>Pode ativar e desativar o contraste alto clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Alto contraste</gui>.</p>
  </note>

</page>
