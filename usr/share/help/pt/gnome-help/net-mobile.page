<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="pt">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Use seu telefone ou a sua USB de Internet para ligar à rede de banda larga movel.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Ligar a uma rede de banda larga movel</title>

  <p>You can set up a connection to a cellular (3G) network with your
  computer’s built-in 3G modem, your mobile phone, or an Internet stick.</p>

  <steps>
    <item><p>Se não tem um modem 3G integrado, ligue seu telefone ou dispositivo de Internet a um porto USB da sua computador.</p>
    </item>
    <item>
    <p>Abra o <gui xref="shell-introduction#yourname">menu do sistema</gui> em parte-a direita da barra superior.</p>
  </item>
  <item>
    <p>Selecione <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg" width="16" height="16"/> Banda larga movel apagada</gui>. A seção <gui>Banda larga movel</gui>  do menu expandir-se-á.</p>
      <note>
        <p>Se <gui>Rede de banda larga movel</gui> não aparece no menu de estado do sistema, se assegure de que seu dispositivo não está conetado como dispositivo de armazenamento em massa.</p>
      </note>
    </item>
    <item><p>Selecione <gui>Ligar</gui>. Se está a ligar-se pela primeira vez, executar-se-á  o assistente para <gui>Configurar a rede de banda larga movel</gui>. A janela que se abre mostra uma lista de informação requerida. Carregue em <gui style="button">Seguinte</gui>.</p></item>
    <item><p>Choose your provider’s country or region from the list. Click
    <gui style="button">Next</gui>.</p></item>
    <item><p>Escolha seu provedor da lista. Carregue <gui style="button">Seguinte</gui>.</p></item>
    <item><p>Selecione um plano de acordo ao tipo de dispositivo que está a ligar. Isto determinará o nome do ponto de acesso. Carregue em <gui style="button">Seguinte</gui>.</p></item>
    <item><p>Confirme a configuração que tem elegido clicando <gui style="button">Aplicar</gui>. O asistentese fechará e o painel de <gui>Rede</gui> mostrará as propriedades da sua conexão.</p></item>
  </steps>

    <note style="tip">
      <p>Some phones have a setting called <em>USB tethering</em> that requires
      no setup on the computer. When the setting is activated on the phone, the
      connection will show up as <gui>Ethernet Connected</gui> in the system
      menu and <gui>USB ethernet</gui> in the network panel.</p>
    </note>

</page>
