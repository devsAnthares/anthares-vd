<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="pt">

  <info>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bulh</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A resolução do ecrã pode estar incorretamente ajustada.</desc>
  </info>

  <title>Por que parece que o que há em meu ecrã está difuso ou pixelado?</title>

  <p>The display resolution that is configured may not be the correct one for
  your screen. To solve this:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Try some of the <gui>Resolution</gui> options and select the one that
      makes the screen look better.</p>
    </item>
  </steps>

<section id="multihead">
  <title>Quando há vários monitores ligadas</title>

  <p>If you have two displays connected to the computer (for example, a normal
  monitor and a projector), the displays might have different optimal, or
  <link xref="look-resolution#native">native</link>, resolutions.</p>

  <p>Using <link xref="display-dual-monitors#modes">Mirror</link> mode, you can
  display the same thing on two screens. Both screens use the same resolution,
  which may not match the native resolution of either screen, so the sharpness
  of the image may suffer on both screens.</p>

  <p>Using <link xref="display-dual-monitors#modes">Join Displays</link> mode,
  the resolution of each screen can be set independently, so they can both be
  set to their native resolution.</p>

</section>

</page>
