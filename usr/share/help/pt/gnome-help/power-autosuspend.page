<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="pt">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configurar o seu computador para que se suspenda automaticamente</desc>
  </info>

  <title>Configurar a suspensão automática</title>

  <p>Pode configurar o seu computador para que se suspenda automaticamente quando esteja inativo. Podem-se definir diferentes intervalos para quando se trabalha com bateria ou ligado.</p>

  <steps>

    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> comece a escrever <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Energia</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Botão de apagar e suspender</gui>, carregue em <gui>Suspender automaticamente</gui>.</p>
    </item>
    <item>
      <p>Escolha <gui>Usando bateria</gui> ou <gui>Ligado</gui>, <gui>Ative</gui> o interruptor, e escolha um <gui>Atraso</gui>. Podem-se configurar ambas opções.</p>

      <note style="tip">
        <p>Numa computador de escritório, há uma opção etiquetada como <gui>Quando esteja inativo</gui>.</p>
      </note>
    </item>

  </steps>

</page>
