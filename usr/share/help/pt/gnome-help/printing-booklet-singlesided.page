<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-singlesided" xml:lang="pt">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Imprimir um folheto desde um PDF numa impressora duma sozinha cara.</desc>
  </info>

  <title>Imprimir um folheto numa impressora duma sozinha cara</title>

  <note>
    <p>Estas instruções são para imprimir um folheto a partir dum documento PDF.</p>
    <p>Se quer imprimir um folheto desde um documento de <app>LibreOffice</app>, terá que o exportar primeiro a PDF elegendo <guiseq><gui>Ficheiro</gui><gui>Exportar como PDF…</gui></guiseq>. Seu documento deve ter um número de páginas múltiplo de 4 (4, 8, 12, 16,...). É possível que tenha que acrescentar até 3 páginas em alvo.</p>
  </note>

  <p>Para imprimir:</p>

  <steps>
    <item>
      <p>Abra o diálogo de impressão. Isto se pode fazer normalmente mediante o elemento de menu <gui style="menuitem">Imprimir</gui> ou utilizando o atalho do teclado <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>Na lista desdobrável <gui>Orientação</gui>, assegure-se de que a opção <gui>Paisagem</gui> está selecionada</p>
      <p>Carregue <gui>Aceitar</gui> para voltar ao diálogo de impressão.</p>
    </item>
    <item>
      <p>Em <gui>Faixa e cópias</gui>, escolha <gui>Páginas</gui>.</p>
      <p>Escreva os números das páginas nesta ordem (n é o número total de páginas, múltiplo de 4):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>...até que tenha escrito todas as páginas.</p>
    <note>
      <p>Exemplos:</p>
      <p>Folheto de 4 páginas: escreva <input>4,1,2,3</input></p>
      <p>Folheto de 8 páginas: escreva <input>8,1,2,7,6,3,4,5</input></p>
      <p>Folheto de 12 páginas: escreva <input>12,1,2,11,10,3,4,9,8,5,6,7</input></p>
      <p>Folheto de 16 páginas: escreva <input>16,1,2,15,14,3,4,13,12,5,6,11,10,7,8,9</input></p>
      <p>Folheto de 20 páginas: escreva <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input></p>
     </note>
    </item>
    <item>
      <p>Selecione a flange <gui>Configuração de página</gui>.</p>
      <p>Em <gui>Distribuição</gui>, escolha <gui>Folheto</gui>.</p>
      <p>Em <gui>Caras</gui>, na lista desdobrável <gui>Incluir</gui>, escolha <gui>Caras frontais / páginas da direita</gui>.</p>
    </item>
    <item>
      <p>Carregue <gui>Imprimir</gui>.</p>
    </item>
    <item>
      <p>Quando todas as páginas estejam impressas, dê a volta às páginas e coloque na impressora.</p>
    </item>
    <item>
      <p>Abra o diálogo de impressão. Isto se pode fazer normalmente mediante o elemento de menu <gui style="menuitem">Imprimir</gui> ou utilizando o atalho do teclado <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Selecione a flange <gui>Configuração de página</gui>.</p>
      <p>Em <gui>Caras</gui>, na lista desdobrável <gui>Incluir</gui>, escolha <gui>Caras traseras / páginas da esquerda</gui>.</p>
    </item>
    <item>
      <p>Carregue <gui>Imprimir</gui>.</p>
    </item>
  </steps>

</page>
