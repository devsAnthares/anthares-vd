<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="pt">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ligue os altifalantes ou auriculares e selecione o dispositivo de saída de som predeterminado.</desc>
  </info>

  <title>Utilizar altifalantes ou auriculares diferentes</title>

  <p>Pode usar altifalantes externos ou auriculares com o seu computador. Os altifalantes normalmente ligam-se ou bem utilizando um conetor TRS circular (<em>jack</em>) ou com USB.</p>

  <p>Se seus altifalantes ou auriculares têm um conetor TRS, ligue na tomada apropriada da sua computador. A maioria das computadores têm dois conetores: um para microfone e outro para os altifalantes. Normalmente este conetor é de cor verde claro ou tem desenhada uma imagem de auriculares. Normalmente de maneira predeterminada usam-se altifalantes ou auriculares conetados a uma tomada TRS. Se não é assim, veja a seguir as instruções para selecionar o dispositivo predeterminado.</p>

  <p>Alguns computadores suportam a saída de vários canais para o som envolvente. Geralmente isto usa conetores TRS múltiplos, com um codigo de cor. Se não está seguro de que ligar na cada lugar, pode experimentar a saída de som na configuração do som.</p>

  <p>Se tem uns altifalantes ou uns auriculares USB, ou uns auriculares analogicos ligados num cartão de som USB, ligue em qualquer porto USB. Os altifalantes USB atuam como dispositivos de som separados, e pode ter que especificar que altifalantes usar de maneira predeterminada.</p>

  <steps>
    <title>Selecione o dispositivo primeiramente de som predeterminado</title>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Som</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Som</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na flange <gui>Saída</gui>, selecione o dispositivo que quer usar.</p>
    </item>
  </steps>

  <p>Use o botão <gui style="button">Provar altifalantes</gui> para verificar que os altifalantes funcionam e que estão ligados corretamente.</p>

</page>
