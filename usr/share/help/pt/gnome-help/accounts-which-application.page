<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="pt">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Milhe-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aplicações que podem usar as contas criadas em <app>Contas em linha</app> e os serviços que podem usar.</desc>

  </info>

  <title>Serviços e aplicações em linha</title>

  <p>Uma vez que tenha acrescentado uma conta em linha, qualquer aplicação pode usar essa conta para qualquer dos serviços disponíveis que não se tenham <link xref="accounts-disable-service">desativado</link>. Diferentes provedores oferecem diferentes serviços. Esta págia pronta os diferentes serviços e algumas de aplicações conhecidos que os usam.</p>

  <terms>
    <item>
      <title>Calendário</title>
      <p>O serviço de calendário permite-lhe ver, acrescentar e editar eventos num calendário em linha. Isto o usam aplicações tais como <app>Calendar</app>, <app>Evolution</app>, e <app>California</app>.</p>
    </item>

    <item>
      <title>Chat</title>
      <p>O serviço de chat permite-lhe chatear com seus contactos em populares plataformas de transportadora instantânea. Isto o usa o aplicação <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contatos</title>
      <p>O serviço de contactos permite-lhe ver os detalhes publicados de seus contactos em vários dispositivos. Isto o usam aplicações tais como <app>Contats</app> e <app>Evolution</app>.</p>
    </item>

    <item>
      <title>Documentos</title>
      <p>O serviço de documentos permite-lhe ver seus documentos em linha, como os que estão em Google docs. Pode ver os documentos utilizando o aplicação <app>Documentos</app>.</p>
    </item>

    <item>
      <title>Ficheiros</title>
      <p>O serviço Ficheiros acrescenta uma localização de ficheiro remota, como se tivesse acrescentado uma utilizando a funcionalidade de <link xref="nautilus-connect">Ligar ao servidor</link> no gestor de ficheiros. Pode aceder aos ficheiros remotos utilizando o gestor de ficheiros bem como os diálogos de abrir e guardar ficheiros de qualquer aplicação.</p>
    </item>

    <item>
      <title>E-mail</title>
      <p>O serviço de e-mail permite-lhe enviar e receber correios eletrónicos mediante um provedor de e-mail como Google. Isto o usa <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->

    <item>
      <title>Fotos</title>
      <p>O serviço de fotos permite-lhe ver suas fotos em linha, como as que tem publicadas em Facebook. Pode ver suas fotos utilizando o aplicação <app>Fotos</app>.</p>
    </item>

    <item>
      <title>Impressoras</title>
      <p>O serviço Impressoras permite-lhe enviar um ficheiro PDF a um provedor desde o diálogo de impressão de qualquer aplicação. Este provedor pode oferecer serviços de impressão ou simplesmente servir como armazenamento do PDF, que pode descarregar e imprimir mais tarde.</p>
    </item>

    <item>
      <title>Ler mais tarde</title>
      <p>O serviço de ler mais tarde permite-lhe guardar uma página site em dispositivos externos para poder lê-la mais tarde em outro dispositivo. Atualmente, nenhum aplicação usa este serviço.</p>
    </item>

  </terms>

</page>
