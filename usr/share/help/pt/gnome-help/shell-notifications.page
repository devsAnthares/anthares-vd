<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Marinha Zhurakhinskaya</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mensagens emergentes na parte superior do ecrã que lhe avisam de quando ocorrem certos eventos.</desc> </info>

<title>Notifications and the notification list</title>

<section id="what">
  <title>Que é uma notificação?</title>

  <p>Se um aplicação ou um componente do sistema quer chamar a sua atenção, mostrará uma notificação na parte superior do ecrã.</p>

  <p>For example, if you get a new chat message or a new email, you will get a
  notification informing you. Chat notifications are given special treatment,
  and are represented by the individual contacts who sent you the chat
  messages.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Outras notificações têm botões de opções selecionáveis. Para fechar uma destas notificações sem selecionar uma de suas opções, carregue no botão de fechar.</p>

  <p>Clicking the close button on some notifications dismisses them. Others,
  like Rhythmbox or your chat application, will stay hidden in the notification 
  list.</p>

</section>

<section id="notificationlist">

  <title>The notification list</title>

  <p>The notification list gives you a way to
  get back to your notifications when it is convenient for you. It appears when
  you click on the clock, or press
  <keyseq><key xref="keyboard-key-super">Super</key><key>V</key></keyseq>. The
  notification list contains all the notifications that you have not acted upon
  or that permanently reside in it.</p>

  <p>You can view a notification by clicking on it in the list. You can close
  the notification list by pressing
  <keyseq><key>Super</key><key>V</key></keyseq> again or <key>Esc</key>.</p>

  <p>Click the <gui>Clear List</gui> button to empty the list of
  notifications.</p>

</section>

<section id="hidenotifications">

  <title>Ocultar notificações</title>

  <p>If you’re working on something and do not want to be bothered, you can
  switch off notifications.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Notifications</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Switch <gui>Notification Popups</gui> to <gui>OFF</gui>.</p>
    </item>
  </steps>

  <p>When switched off, most notifications will not pop up at the top of the
  screen. <!--Very important notifications, such as when your battery is
  critically low, will still pop up.--> Notifications will still be available
  in the notification list when you display it (by clicking on the clock, or by
  pressing <keyseq><key>Super</key><key>V</key></keyseq>), and they will start
  popping up again when you switch the toggle back to <gui>ON</gui>.</p>

  <p>Também pode desativar ou voltar a ativar as notificações para aplicações individuais desde o painel <gui>Notificações</gui>.</p>


</section>

</page>
