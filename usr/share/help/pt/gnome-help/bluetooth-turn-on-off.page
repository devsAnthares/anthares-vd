<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-turn-on-off" xml:lang="pt">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbelh</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ativar ou desativar o dispositivo Bluetooth da sua computador.</desc>
  </info>

<title>Acender ou apagar o Bluetooth</title>

  <p>Pode ativar seu Bluetooth para usar dispositivos Bluetooth e enviar e receber ficheiros, ou apagá-lo para poupar energia. Para ativar o Bluetooth:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Carregue em <gui>Bluetooth</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Mova o deslizador da parte superior a <gui>Ignição</gui>.</p>
    </item>
  </steps>

  <p>Muitos portáteis têm um interruptor físico ou uma combinação de teclas para acender e apagar o Bluetooth. Procure um interruptor na sua computador ou uma tecla em seu teclado. A tecla costuma ir socia à tecla <key>Fn</key>.</p>

  <p>Para apagar o Bluetooth:</p>
  <steps>
    <item>
      <p>Abra o <gui xref="shell-introduction#yourname">menu do sistema</gui> em parte-a direita da barra superior.</p>
    </item>
    <item>
      <p>Selecione <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Não está em uso</gui>. A seção Bluetooth do menu expandir-se-á.</p>
    </item>
    <item>
      <p>Selecione <gui>Apagar</gui>.</p>
    </item>
  </steps>

  <note><p>A computador é <link xref="bluetooth-visibility">visível</link> enquanto o painel de <gui>Bluetooth</gui> esteja aberto.</p></note>

</page>
