<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="pt">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hilh</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Procure na <guiseq><gui>Configuração</gui><gui>Cor</gui></guiseq> a opção para anhadir um perfil de cor a seu ecrã.</desc>
  </info>

  <title>Como atribuir perfis aos dispositivos?</title>

  <p>Pode querer atribuir um perfil de cor a seu ecrã ou a sua impressora para que as cores que mostrem sejam mais precisos.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Selecione o dispositivo para o que quer anhadir um perfil.</p>
    </item>
    <item>
      <p>Carregue <gui>Anhadir perfil</gui> pode selecionar um perfil existente ou para importar um ficheiro novo.</p>
    </item>
    <item>
      <p>Carregue <gui>Anhadir</gui> para confirmar seu seleção.</p>
    </item>
  </steps>

  <p>A cada dispositivo pode ter atribuídos vários perfis, mas só um de eles pode ser o perfil <em>predeterminado</em>. O perfil predeterminado usa-se quando não há informação adicional que permita escolher o perfil automaticamente. Um exemplo de seleção automática seria se criasse-se um perfil para papel acetinado e outro para papel normal.</p>

  <!--
  <figure>
    <desc>You can make a profile default by changing it with the radio button.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>
  -->

  <p>Se o hardware de calibração está ligado, o botão <gui>Calibrar...</gui> criará um perfil novo.</p>

</page>
