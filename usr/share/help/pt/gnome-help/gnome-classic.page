<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="pt">

  <info>
<!--    <link type="guide" xref="shell-overview#desktop" />-->

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>

    <credit type="author">
      <name>Projeto de documentação de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Considere alterar para GNOME clássico se prefere uma experiência do escritório mais tradicional.</desc>
  </info>

<title>Que é GNOME clássico?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME clássico</em> é uma caraterística para aqueles utilizadores que preferem uma experiência do escritório mais tradicional. Ainda que <em>GNOME clássico</em> baseia-se em tecnologias de <em>GNOME 3</em> proporciona várias mudanças na interface do utilizador, como os menus <gui>Aplicações</gui> e <gui>Lugares</gui> na barra superior, e uma lista de janelas na parte inferior do ecrã.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME clássico</em> é uma caraterística para aqueles utilizadores que preferem uma experiência do escritório mais tradicional. Ainda que <em>GNOME clássico</em> baseia-se em tecnologias de <em>GNOME 3</em> proporciona várias mudanças na interface do utilizador, como os menus <gui xref="shell-introduction#activities">Aplicações</gui> e <gui>Lugares</gui> na barra superior, e uma lista de janelas na parte inferior do ecrã.</p>
  </if:when>
</if:choose>

<p>Pode usar o menu de <gui>Aplicações</gui> da barra superior para lançar aplicações. A vista de <gui xref="shell-introduction#activities">Atividades</gui> está disponível selecionando o elemento <gui>Vista de atividades</gui> no menu.</p>

<p>Para aceder à <em>vista de <gui>Atividades</gui></em>, também pode carregar a tecla <key xref="keyboard-key-super">Super</key>.</p>

<section id="gnome-classic-window-list">
<title>Lista de janelas</title>

<p>A lista de janelas na parte inferior do ecrã proporciona acesso a todas suas janelas e aplicações abertos e lhe permite as minimizar e as restaurar rapidamente.</p>

<p>À direita da lista de janelas, GNOME mostra um identificador curto para o área de trabalho atual, como <gui>1</gui> para a primeira área de trabalho. Além, o identificador também mostra o número total de áreas de trabalho disponíveis. Para alterar para um área de trabalho diferente, pode carregar o em identificador e selecionar o área de travado que quer usar no menu.</p>

<!-- <p>If an application or a system component wants to get your attention, it
 will display a blue icon at the right-hand side of the window list. Clicking
 the blue icon shows the <link xref="shell-notifications">message tray</link>,
 which lets you access all your notifications.</p> -->

</section>

<section id="gnome-classic-switch">
<title>Alterar para e de GNOME clássico</title>

<note if:test="!platform:gnome-classic" style="important">
<p>GNOME clássico só está disponível em sistemas com determinadas extensões de GNOME Shelh instaladas. Algumas distribuições de Linux podem não ter essas extensões disponíveis ou instaladas de maneira predeterminada.</p>
</note>

  <steps>
    <title>Para mudar de <em>GNOME</em> a <em>GNOME clássico</em>:</title>
    <item>
      <p>Guarde qualquer trabalho que tenha aberto e fechamento a sessão. Carregue sobre no menu do sistema à direita da barra superior, carregue sobre seu nome e selecione a opção apropriada.</p>
    </item>
    <item>
      <p>Aparecerá uma mensagem de confirmação. Selecione <gui>Fechar a sessão</gui> para confirmar.</p>
    </item>
    <item>
      <p>No ecrã de início de sessão, selecione seu nome de utilizador da lista.</p>
    </item>
    <item>
      <p>Introduza a sua palavra-passe na caixa primeiramente da palavra-passe.</p>
    </item>
    <item>
      <p>Carregue no ícone de opções, que aparece à esquerda do botão <gui>Iniciar sessão</gui> e escolha <gui>GNOME Clássico</gui>.</p>
    </item>
    <item>
      <p>Carregue o botão <gui>Iniciar sessão</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Para mudar de <em>GNOME clássico</em> a <em>GNOME</em>:</title>
    <item>
      <p>Guarde qualquer trabalho que tenha aberto e fechamento a sessão. Carregue sobre no menu do sistema à direita da barra superior, carregue sobre seu nome e selecione a opção apropriada.</p>
    </item>
    <item>
      <p>Aparecerá uma mensagem de confirmação. Selecione <gui>Fechar a sessão</gui> para confirmar.</p>
    </item>
    <item>
      <p>No ecrã de início de sessão, selecione seu nome de utilizador da lista.</p>
    </item>
    <item>
      <p>Introduza a sua palavra-passe na caixa primeiramente da palavra-passe.</p>
    </item>
    <item>
      <p>Carregue no ícone de opções, que aparece à esquerda do botão <gui>Iniciar sessão</gui> e escolha <gui>GNOME</gui>.</p>
    </item>
    <item>
      <p>Carregue o botão <gui>Iniciar sessão</gui>.</p>
    </item>
  </steps>
  
</section>

</page>
