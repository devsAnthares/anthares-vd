<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="vi">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Learn how often you should backup your important files to make sure
    that they are safe.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

<title>Tần suất sao lưu</title>

  <p>Tần suất sao lưu phụ thuộc vào loại dữ liệu cần sao lưu. Ví dụ bạn quản lý một môi trường mạng với nhiều dữ liệu quan trọng lưu trên máy chủ của bạn, thậm chí sao lưu hàng đêm cũng không đủ.</p>

  <p>Ngược lại, nếu bạn sao lưu dữ liệu trên máy cá nhân, sao lưu hàng giờ có thể không cần thiết. Bạn có thể cần xem xét những yếu tố sau khi hoạch định sao lưu:</p>

<list style="compact">
<item><p>Khoảng thời gian bạn dùng máy tính.</p></item>
<item><p>Bạn thay đổi bao nhiêu dữ liệu và có thường xuyên hay không.</p></item>
</list>

  <p>Nếu dữ liệu sao lưu có độ ưu tiên thấp, hoặc ít thay đổi như nhạc, email và ảnh gia đình, sao lưu hàng tuần hoặc thậm chí hàng tháng cũng đủ. Tuy nhiên, nếu bạn đang trong kỳ kiểm kê thuế, có thể cần sao lưu thường xuyên hơn.</p>

  <p>Nguyên tắc chung là lượng thời gian giữa mỗi lần sao lưu không hơn lượng thời gian bạn có thể dùng để tái tạo lại mất mát. Ví dụ, nếu một tuần để tạo lại tài liệu là quá lâu, bạn nên sao lưu ít nhất một lần một tuần.</p>

</page>
