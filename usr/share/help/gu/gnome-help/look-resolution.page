<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="gu">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-07-19" status="review"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>શોભા ત્યાગી</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>સ્ક્રીનનાં રિઝોલ્યુશન અને તેની દિશાને બદલો (ફેરવવાનું).</desc>
  </info>

  <title>Change the resolution or orientation of the screen</title>

  <p>You can change how big (or how detailed) things appear on the screen by
  changing the <em>screen resolution</em>. You can change which way up things
  appear (for example, if you have a rotating display) by changing the
  <em>rotation</em>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>જો તમારી પાસે ઘણા દર્શાવ હોય તો અને તેઓ મીરર થયેલ હોય તો, તમે દરેક દર્શાવ પર વિવિધ સુયોજનો હોઇ શકે છે. પૂર્વદર્શન વિસ્તારમાં દર્શાવને પસંદ કરો.</p>
    </item>
    <item>
      <p>Select the resolution or scale, and choose the orientation.</p>
    </item>
    <item>
      <p>Click <gui>Apply</gui>. The new settings will be applied for 20
      seconds before reverting back. That way, if you cannot see anything with
      the new settings, your old settings will be automatically restored. If
      you are happy with the new settings, click <gui>Keep Changes</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>રીઝોલ્યુશન</title>

  <p>The resolution is the number of pixels (dots on the screen) in each
  direction that can be displayed. Each resolution has an <em>aspect
  ratio</em>, the ratio of the width to the height. Wide-screen displays use a
  16∶9 aspect ratio, while traditional displays use 4∶3. If you choose a
  resolution that does not match the aspect ratio of your display, the screen
  will be letterboxed to avoid distortion, by adding black bars to the top and
  bottom or both sides of the screen.</p>

  <p>તમે રિઝોલ્યુશનને પસંદ કરી શકો છો જે તમે <gui>રિઝોલ્યુશન</gui> ડ્રોપ-ડાઉન યાદીમાંથી પસંદ કર્યુ છે. જો તમે એકને પસંદ કરો તો તમારી સ્ક્રીન માટે સાચુ નથી તે <link xref="look-display-fuzzy">ઝાંખુ અથવા પિક્સલેટ લાગી શકે છે</link>.</p>

</section>

<section id="native">
  <title>Native Resolution</title>

  <p>The <em>native resolution</em> of a laptop screen or LCD monitor is the
  one that works best: the pixels in the video signal will line up precisely
  with the pixels on the screen. When the screen is required to show other
  resolutions, interpolation is necessary to represent the pixels, causing a
  loss of image quality.</p>

</section>

<section id="scale">
  <title>Scale</title>

  <p>The scale setting increases the size of objects shown on the screen to
  match the density of your display, making them easier to read. Choose from
  <gui>100%</gui>, <gui>200%</gui>, or <gui>300%</gui>.</p>

</section>

<section id="orientation">
  <title>Orientation</title>

  <p>On some laptops and monitors, you can physically rotate the screen in many
  directions. Click <gui>Orientation</gui> in the panel and choose from
  <gui>Landscape</gui>, <gui>Portrait Right</gui>, <gui>Portrait Left</gui>, or
  <gui>Landscape (flipped)</gui>.</p>

</section>

</page>
