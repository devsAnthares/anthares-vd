<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="gu">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>મૂળભૂત ફાઇલ જાણકારીને જુઓ, પરવાનગીઓ સુયોજિત કરો, અને મૂળભૂત કાર્યક્રમોને પસંદ કરો.</desc>

  </info>

  <title>ફાઇલ ગુણધર્મો</title>

  <p>ફાઇલ અથવા ફોલ્ડર વિશે જાણકારીને જોવા માટે, તેની પર જમણી ક્લિક કરો અને <gui>ગુણધર્મો</gui> ને પસંદ કરો. તમે પણ ફાઇલને પસંદ કરી શકો છો અને <keyseq><key>Alt</key><key>Enter</key></keyseq> ને દબાવો.</p>

  <p>ફાઇલ ગુણધર્મો એ ફાઇલનાં પ્રકાર જેવી જાણકારીને તમને બતાવે છે, ફાઇલનું માપ, અને જ્યારે તમે તેને છેલ્લે બદલેલ હોય તો. જો તમારે આ જાણકારીની વારંવાર જરૂર હોય તો, તમે <link xref="nautilus-list">યાદી દૃશ્ય સ્તંભ</link> અથવા <link xref="nautilus-display#icon-captions">ચિહ્ન કૅપ્શન</link> માં તેને દર્શાવી શકો છો.</p>

  <p><gui>મૂળભૂત</gui> ટૅબ પર આપેલ જાણકારી એ નીચે વર્ણવેલ છે. ત્યાં પણ <gui><link xref="nautilus-file-properties-permissions">પરવાનગીઓ</link></gui> અને<gui><link xref="files-open#default">તેની સાથે ખોલો</link></gui> ટૅબ છે. અમુક ફાઇલોનાં પ્રકારો માટે, જેમ કે ઇમેજ અને વિડિયો, ત્યાં વધારાની ટૅબ હશે કે જે માપ, સમયગાળો, અને કોડેક જેવી જાણકારી પૂરી પાડે છે.</p>

<section id="basic">
 <title>મૂળભૂત ગુણધર્મો</title>
 <terms>
  <item>
    <title><gui>Name</gui></title>
    <p>તમે આ ક્ષેત્રને બદલીને ફાઇલનું નામ બદલી શકો છો. તમે પણ ગુણધર્મ વિન્ડોની બહાર પણ ફાઇલને બદલી શકો છો. <link xref="files-rename"/> જુઓ.</p>
  </item>
  <item>
    <title><gui>Type</gui></title>
    <p>This helps you identify the type of the file, such as PDF document,
    OpenDocument Text, or JPEG image. The file type determines which
    applications can open the file, among other things. For example, you
    can’t open a picture with a music player. See <link xref="files-open"/>
    for more information on this.</p>
    <p>કૌંસમાં ફાઇલનો <em>MIME પ્રકાર</em> બતાવેલ છે; MIME પ્રકાર એ મૂળભૂત રસ્તો છે કે જે કમ્પ્યૂટરો ફાઇલનાં સંદર્ભને વાપરે છે.</p>
  </item>

  <item>
    <title>સમાવિષ્ટો</title>
    <p>આ ક્ષેત્રને દર્શાવેલ છે જો તમે ફાઇલ કરતા ફોલ્ડરનાં ગુણધર્મોને જોઇ રહ્યા હોય. તે ફોલ્ડરમાં તમને વસ્તુઓની સંખ્યાને જોવા માટે મદદ કરે છે. જો ફોલ્ડર એ બીજા ફોલ્ડરોને સમાવે છે, દેરક આંતરિક ફોલ્ડરની એક વસ્તુ તરીકે ગણતરી થયેલ છે, જો તે આગળની વસ્તુઓને સમાવે તો પણ. દરેક ફાઇલ પણ એક વસ્તુ પ્રમાણે ગણતરી થયેલ છે. જો ફોલ્ડર ખાલી હોય, સમાવિષ્ટો એ <gui>કંઇપણ</gui> દર્શાવશે નહિં.</p>
  </item>

  <item>
    <title>માપ</title>
    <p>આ ક્ષેત્રને દર્શાવેલ છે જો તમે ફાઇલને જોઇ રહ્યા હોય (ફોલ્ડરને નહિં). ફાઇલનું માપ એ તમને કહે છે કે કેટલી ડિસ્ક જગ્યા તે લે છે. આ પણ સૂચક છે ફાઇલને ડાઉનલોડ કરવા માટે કેટલો સમય લાગશે અથવા ઇમેઇલમાં તેને મોકલો (મોટી ફાઇલોને મોકલવા/પ્રાપ્ત કરવા માટે લાંબો સમય લાગે છે).</p>
    <p>માપને બાઇટ, KB, MB, અથવા GB માં આપી શકાય છે; છેલ્લા ત્રણની સ્થિતિમાં, બાઇટમાં માપને પણ કૌંસમા આપેલ છે. ખાસ કરીને, 1 KB એ 1024 બાઇટ છે, 1 MB એ 1024 KB  છે અને વગેરે.</p>
  </item>

  <item>
    <title>Parent Folder</title>
    <p>The location of each file on your computer is given by its <em>absolute
    path</em>. This is a unique “address” of the file on your computer, made up
    of a list of the folders that you would need to go into to find the file.
    For example, if Jim had a file called <file>Resume.pdf</file> in his Home
    folder, its parent folder would be <file>/home/jim</file> and its location
    would be <file>/home/jim/Resume.pdf</file>.</p>
  </item>

  <item>
    <title>મુક્ત જગ્યા</title>
    <p>આ ફક્ત ફોલ્ડરો માટે દર્શાવેલ છે. તે ડિસ્ક જગ્યાની સંખ્યાને આપે છે કે જે ડિસ્ક પર ઉપલબ્ધ છે કે જે ફોલ્ડર તેની પર છે. ચકાસવા માટે આ ઉપયોગી છે જો હાર્ડ ડિસ્ક સંપૂર્ણ છે.</p>
  </item>

  <item>
    <title>પ્રવેશેલ</title>
    <p>તારીખ અને સમય જ્યારે ફાઇલ એ છેલ્લે ખોલેલ હતી.</p>
  </item>

  <item>
    <title>સુધારેલ</title>
    <p>તારીખ અને સમય જ્યારે ફાઇલ એ છેલ્લે બદલાયેલ અને સંગ્રહેલ હતી.</p>
  </item>
 </terms>
</section>

</page>
