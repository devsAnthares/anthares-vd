<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-disc-write" xml:lang="gu">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>

    <credit type="author">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>CD/DVD બર્નરની મદદથી ખાલી CD અથવા DVD માં ફાઇલો અને દસ્તાવેજોને મૂકો.</desc>
  </info>

  <title>CD અથવા DVD માં ફાઇલોને લખો</title>

  <p>તમે <gui>CD/DVD નિર્માતા</gui> ની મદદથી ખાલી ડિસ્કમાં ફાઇલોને મૂકી શકો છો. CD અથવા DVD ને બનાવવા માટે વિકલ્પ એ ફાઇલ સંચાલકમાં દેખાશે તમે તમારાં CD/DVD રાઇટરમાં CD ને સ્થિત કરો છો. ફાઇલ સંચાલક એ બીજા કમ્પ્યૂટરમાં તમને ફાઇલોનું પરિવહન કરવા દે છે અથવા ખાલી ડિસ્કમાં ફાઇલોને મૂકીને <link xref="backup-why">બેકઅપ</link> ચલાવો. CD અથવા DVD માં ફાઇલોને લખવા માટે:</p>

  <steps>
    <item>
      <p>તમારી CD/DVD લખી શકાય તેવી ડ્રાઇવમાં ખાલી ડિસ્કને સ્થિત કરો.</p></item>
    <item>
      <p><gui>ખાલી CD/DVD-R ડિસ્ક</gui> સૂચના કે જે સ્ક્રીનનાં તળિયે પોપ અપ કરે છે, <gui>CD/DVD નિર્માણકર્તા</gui> ને પસંદ કરો. <gui>CD/DVD નિર્માણકર્તા</gui> ફોલ્ડર વિન્ડો ખોલશે.</p>
      <p>(ફાઇલ સંચાલક બાજુપટ્ટીમાં <gui>ઉપકરણો</gui> હેઠળ તમે <gui>ખાલી CD/DVD-R ડિસ્ક</gui> પર પણ ક્લિક કરી શકો છો.)</p>
    </item>
    <item>
      <p><gui>ડિસ્ક નામ</gui> ક્ષેત્રમાં, ડિસ્ક માટે નામ લખો.</p>
    </item>
    <item>
      <p>વિન્ડોમાં ઇચ્છિત ફાઇલોની નકલ અથવા લાવો.</p>
    </item>
    <item>
      <p><gui>ડિસ્કમાં લખો</gui> પર ક્લિક કરો.</p>
    </item>
    <item>
      <p><gui>તેમાં લખવા માટે ડિસ્કને પસંદ કરો</gui> હેઠળ, ખાલી ડિસ્કને પસંદ કરો.</p>
      <p>(તમે તેને બદલે <gui>ઇમેજ ફાઇલ</gui> ને પસંદ કરી શકો છો. આ <em>ડિસ્ક ઇમેજ</em> માં ફાઇલને મૂકશે, કે જે તમારાં કમ્પ્યૂટર પર સંગ્રહશે. તમે પછીની તારીખે ખાલી ડિસ્કમાં તે ડિસ્ક ઇમેજને લખી શકો છો.)</p>
    </item>
    <item>
      <p><gui>ગુણધર્મો</gui> પર ક્લિક કરો જો તમે બર્નિંગ ઝડપને બરાબર કરવા માંગતા હોય, કામચલાઉ ફાઇલોનું સ્થાન, અને બીજા વિકલ્પો. મૂળભૂત વિકલ્પો બરાબર હોવા જોઇએ.</p>
    </item>
    <item>
      <p>રેકોર્ડીંગ શરૂ કરવા માટે <gui>લખો</gui> બટન પર ક્લિક કરો.</p>
      <p>જો <gui>ઘણી નકલોને બર્ન કરો</gui> પસંદ થયેલ છે, વધારાની ડિસ્ક માટે તમને પૂછશે.</p>
    </item>
    <item>
      <p>જ્યારે ડિસ્ક બર્નિંગ સમાપ્ત થાય ત્યારે, તે આપમેળે બહાર નીકળી જશે. બહાર નીકળવા માટે <gui>વધારે નકલોને બનાવો</gui> અથવા <gui>બંધ કરોClose</gui> ને પસંદ કરો.</p>
    </item>
  </steps>

<section id="problem">
  <title>If the disc wasn’t burned properly</title>

  <p>Sometimes the computer doesn’t record the data correctly, and you won’t be
  able to see the files you put onto the disc when you insert it into a
  computer.</p>

  <p>In this case, try burning the disc again but use a lower burning speed,
  for example, 12x rather than 48x. Burning at slower speeds is more reliable.
  You can choose the speed by clicking the <gui>Properties</gui> button in the
  <gui>CD/DVD Creator</gui> window.</p>

</section>

</page>
