<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="gu">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ફાઇલ સંચાલક સાથે ફાઇલોને સંચાલિત અને વ્યવસ્થિત કરો.</desc>
  </info>

<title>ફાઇલ અને ફોલ્ડર બ્રાઉઝ કરો</title>

<p>તમારાં કમ્પ્યૂટર પર ફાઇલોને સંચાલિત અને બ્રાઉઝ કરવા માટે <app>ફાઇલો</app> ફાઇલ સંચાલકને વાપરો. તમે સંગ્રહ ઉપકરણો પર ફાઇલોને સંચાલિત કરવા માટે તેને વાપરી શકો છો (જેમ કે બહારની હાર્ડ ડિસ્ક), <link xref="nautilus-connect">ફાઇલ સર્વર</link> પર, અને નેટવર્ક વહેંચણી પર.</p>

<p>To start the file manager, open <app>Files</app> in the
<gui xref="shell-introduction#activities">Activities</gui> overview. You can also search
for files and folders through the overview in the same way you would
<link xref="shell-apps-open">search for applications</link>.
</p>

<section id="files-view-folder-contents">
  <title>ફોલ્ડરનાં સમાવિષ્ટોની શોધ કરી રહ્યા છે</title>

<p>In the file manager, double-click any folder to view its contents, and
double-click or <link xref="mouse-middleclick">middle-click</link> any file to
open it with the default application for that file. Middle-click a folder to
open it in a new tab. You can also right-click a folder to open it in a new tab
or new window.</p>

<p>જ્યારે ફોલ્ડરમાં ફાઇલો મારફતે જોઇ રહ્યા હોય તો, તમે ખાતરી કરવા માટે જગ્યા પટ્ટીને દબાવીને તમે <link xref="files-preview">દરેક ફાઇલનું પૂર્વદર્શન</link> લઇ શકો છો જો તમારી પાસે તેને કાઢતા, નકલ કરતા, ખોલતા પહેલાં યોગ્ય ફાઇલ હોય.</p>

<p>The <em>path bar</em> above the list of files and folders shows you which
folder you’re viewing, including the parent folders of the current folder.
Click a parent folder in the path bar to go to that folder. Right-click any
folder in the path bar to open it in a new tab or window, or access its
properties.</p>

<p>If you want to quickly <link xref="files-search">search for a file</link>,
in or below the folder you are viewing, start typing its name. A <em>search
bar</em> will appear at the top of the window and only files which match your
search will be shown. Press <key>Esc</key> to cancel the search.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, click <gui>Files</gui> in the top bar and then select
<gui>Sidebar</gui>. You can add bookmarks to folders that you use often and
they will appear in the sidebar. Drag a folder to the sidebar, and drop it over
<gui>New bookmark</gui>, which appears dynamically, or click the window menu 
and then select <gui style="menuitem">Bookmark this Location</gui>.</p>

</section>

</page>
