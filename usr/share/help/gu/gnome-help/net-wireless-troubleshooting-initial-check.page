<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-initial-check" xml:lang="gu">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-info"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu દસ્તાવેજીકરણ વિકિ માટે ફાળકો</name>
    </credit>
    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ખાતરી કરો કે સાદા નેટવર્ક સુયોજનો યોગ્ય છે અને આગળનાં થોડા મુશ્કેલીનિવારણ તબક્કા માટે તૈયાર રહો.</desc>
  </info>

  <title>વાયરલેસ નેટવર્ક મુશ્કેલીનિવારક</title>
  <subtitle>પ્રારંભિક જોડાણ ચકાસણીને ચલાવો</subtitle>

  <p>In this step you will check some basic information about your wireless
  network connection. This is to make sure that your networking problem isn’t
  caused by a relatively simple issue, like the wireless connection being
  turned off, and to prepare for the next few troubleshooting steps.</p>

  <steps>
    <item>
      <p>ખાતરી કરો કે તમારું લેપટોપ <em>વાયર થયેલ</em> ઇન્ટરનેટ જોડાણ સાથે જોડાયેલ નથી.</p>
    </item>
    <item>
      <p>જો તમારી પાસે બહારનાં વાયરલેસ ઍડપ્ટર હોય તો (જેમ કે USB ઍડપ્ટર, અથવા PCMCI કાર્ડ કે જે તમારાં લેપટોપમાં પ્લગ થાય છે), ખાતરી કરો કે તે તમારાં કમ્પ્યૂટર પર યોગ્ય સ્લોટમાં દાખલ થયેલ છે.</p>
    </item>
    <item>
      <p>જો તમારું વાયરલેસ કાર્ડ એ તમારાં કમ્પ્યૂટરની <em>અંદર</em> હોય તો, ખાતરી કરો કે વાયરલેસ સ્વીચ ચાલુ થયેલ છે (જો તેમની પાસે એક હોય તો). વારંવાર લેપટોપ પાસે વાયરલેસ સ્વીચ હોય છે કે જે તમે કિબોર્ડ કીનાં સંયોજનને દબાવીને ટોગલ કરી શકો છો.</p>
    </item>
    <item>
      <p>Click the system status area on the top bar and select
      <gui>Wi-Fi</gui>, then select <gui>Wi-Fi Settings</gui>. Make sure that
      <gui>Wi-Fi</gui> is set to <gui>ON</gui>. You should also check that
      <link xref="net-wireless-airplane">Airplane Mode</link> is <em>not</em>
      switched on.</p>
    </item>
    <item>
      <p>Open the Terminal, type <cmd>nmcli device</cmd> and press
      <key>Enter</key>.</p>
      <p>This will display information about your network interfaces and
      connection status. Look down the list of information and see if there is
      an item related to the wireless network adapter. If the state is
      <code>connected</code>, it means that the adapter is working and connected
      to your wireless router.</p>
    </item>
  </steps>

  <p>જો તમે તમારાં વાયરલેસ રાઉટર સાથે જોડાયેલ હોય, પરંતુ તમે હજુ ઇન્ટરનેટને વાપરી શકતા ન હોય તો, તમારાં રાઉટરને યોગ્ય રીતે સુયોજિત કરી શકાતા નથી, અથવા તમારી Internet Service Provider (ISP) એ અમુક ટૅકનિકલ સમસયાને અનુભવી રહ્યા છે. સુયોજનો યોગ્ય છે તેની ખાતરી કરવા માટે તમારું રાઉટર અને ISP સુયોજન માર્ગદર્શિકાનું રિવ્યૂ કરો, અથવા આધાર માટે તમારાં ISP નો સંપર્ક કરો.</p>

  <p>If the information from <cmd>nmcli device</cmd> did not indicate that you were
  connected to the network, click <gui>Next</gui> to proceed to the next
  portion of the troubleshooting guide.</p>

</page>
