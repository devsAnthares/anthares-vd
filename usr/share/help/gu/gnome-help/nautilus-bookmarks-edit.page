<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="gu">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>સંચાલકમાં બુકમાર્કને ઉમેરો, કાઢો અને તેનું નામ બદલો.</desc>

  </info>

  <title>ફોલ્ડર બુકમાર્કમાં ફેરફાર કરો</title>

  <p>તમારા બુકમાર્ક સંચાલકની બાજુની પટ્ટીમાં યાદી થયેલ છે.</p>

  <steps>
    <title>બુકમાર્કને ઉમેરો:</title>
    <item>
      <p>ફોલ્ડર (અથવા સ્થાન) ને ખોલો કે જે તમે બુકમાર્ક કરવા માંગો છો.</p>
    </item>
    <item>
      <p>Click the window menu in the toolbar and pick <gui>Bookmark this
      Location</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>બુકમાર્ક કાઢી નાંખો:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Remove</gui> from the menu.</p>
    </item>
  </steps>

  <steps>
    <title>બુકમાર્કનું નામ બદલો:</title>
    <item>
      <p>Right-click on the bookmark in the sidebar and select
      <gui>Rename…</gui>.</p>
    </item>
    <item>
      <p><gui>નામ</gui> લખાણ બોક્સમાં, બુકમાર્ક માટે નવાં નામને ટાઇપ કરો.</p>
      <note>
        <p>Renaming a bookmark does not rename the folder. If you have
        bookmarks to two different folders in two different locations, but
        which each have the same name, the bookmarks will have the same name,
        and you won’t be able to tell them apart. In these cases, it is useful
        to give a bookmark a name other than the name of the folder it points
        to.</p>
      </note>
    </item>
  </steps>

</page>
