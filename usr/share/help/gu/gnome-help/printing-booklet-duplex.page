<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-booklet-duplex" xml:lang="gu">

  <info>
    <link type="guide" xref="printing-booklet"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author editor">
      <name>પેટર કોવર</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Print folded booklets (like a book or pamphlet) from a PDF using normal
    A4/Letter-size paper.</desc>
  </info>

  <title>Print a booklet on a double-sided printer</title>

  <p>You can make a folded booklet (like a small book or pamphlet) by printing
  pages of a document in a special order and changing a couple of printing
  options.</p>

  <p>આ સૂચનાઓ PDF દસ્તાવેજમાંથી બુકલેટને છાપવા માટે હોય છે.</p>

  <p>If you want to print a booklet from a <app>LibreOffice</app> document,
  first export it to a PDF by choosing <guiseq><gui>File</gui><gui>Export
  as PDF…</gui></guiseq>. Your document needs to have a multiple of 4 number of
  pages (4, 8, 12, 16,…). You may need to add up to 3 blank pages.</p>

  <p>બુકલેટને છાપવા માટે:</p>

  <steps>
    <item>
      <p>પ્રિન્ટ સંવાદને ખોલો. આ સામાન્ય રીતે મેનુમાં <gui style="menuitem">છાપો</gui> મારફતે પૂર્ણ કરી શકાય છે અથવા <keyseq><key>Ctrl</key><key>P</key></keyseq> કિબોર્ડ ટૂંકાણોને વાપરી રહ્યા છે.</p>
    </item>
    <item>
      <p>Click the <gui>Properties…</gui> button </p>
      <p>In the <gui>Orientation</gui> drop-down list, make sure that
      <gui>Landscape</gui> is selected.</p>
      <p>In the <gui>Duplex</gui> drop-down list, select <gui>Short Edge</gui>.
      </p>
      <p>Click <gui>OK</gui> to go back to the print dialog.</p>
    </item>
    <item>
      <p>Under <gui>Range and Copies</gui>, choose <gui>Pages</gui>.</p>
    </item>
    <item>
      <p>પાનાંઓની સંખ્યા આ ક્રમમાં લખો (n એ પાનાંઓની કુલ સંખ્યા છે, અને 4 નો ગુણક છે):</p>
      <p>n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…</p>
      <p>ઉદાહરણો:</p>
      <list>
        <item><p>4 પાનાં બુકલેટ: <input>4,1,2,3</input> ટાઇપ કરો</p></item>
        <item><p>8 પાનાં બુકલેટ: <input>8,1,2,7,6,3,4,5</input> ટાઇપ કરો</p></item>
        <item><p>20 પાનાં બુકલેટ: <input>20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11</input> ટાઇપ કરો</p></item>
      </list>
    </item>
    <item>
      <p>Choose the <gui>Page Layout</gui> tab.</p>
      <p>Under <gui>Layout</gui>, select <gui>Brochure</gui>.</p>
      <p>Under <gui>Page Sides</gui>, in the <gui>Include</gui> drop-down list,
      select <gui>All pages</gui>.</p>
    </item>
    <item>
      <p><gui>છાપો</gui> પર ક્લિક કરો.</p>
    </item>
  </steps>

</page>
