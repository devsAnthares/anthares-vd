<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="gu">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>
    <link type="guide" xref="keyboard-shortcuts-set"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>૨૦૧૨</years>
    </credit>
    <credit type="author">
       <name>જુલીટા ઇન્કા</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>જુઆન્જો મારીન</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
      <years>૨૦૧૩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>કિબોર્ડ લેઆઉટને ખોલો અને તેઓ વચ્ચે ફેરબદલ કરો.</desc>
  </info>

  <title>વૈકલ્પિક કિબોર્ડ લેઆઉટને વાપરો</title>

  <p>કિબોર્ડ વિવિધ ભાષાઓ માટે વિવિધ સેંકડોમાં આવે છે. દરેક ભાષા માટે પણ, ત્યાં વારંવાર ઘણાં કિબોર્ડ લેઆઉટ છે, જેમ કે અંગ્રેજી માટે Dvorak લેઆઉટ. તમે તમારાં કિબોર્ડ વર્તણૂકને બનાવી શકો છો જેમ કે વિવિધ લેઆઉટ સાથે કિબોર્ડ, કી પર છાપેલ સંકેતો અને અક્ષરોને બાદ કરતા. આ ઉપયોગી છે જો તમે ઘણી ભાષાઓ વચ્ચે વારંવાર બદલો તો.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Region &amp; Language</gui> in the sidebar to open the
      panel.</p>
    </item>
    <item>
      <p>Click the <gui>+</gui> button in the <gui>Input Sources</gui> section,
      select the language which is associated with the layout, then select a
      layout and press <gui>Add</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If there are multiple user accounts on your system, there is a separate
    instance of the <gui>Region &amp; Language</gui> panel for the login screen.
    Click the <gui>Login Screen</gui> button at the top right to toggle between
    the two instances.</p>

    <p>Some rarely used keyboard layout variants are not available by default
    when you click the <gui>+</gui> button. To make also those input sources
    available you can open a terminal window by pressing
    <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>
    and run this command:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>You can preview an image of any layout by selecting it in the list of
    <gui>Input Sources</gui> and clicking
    <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">preview</span></media></gui></p>
  </note>

  <p>Certain languages offer some extra configuration options. You can
  identify those languages because they have a
  <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">preview</span></media></gui>
  icon next to them. If you want to access these extra parameters, select the
  language from the <gui>Input Source</gui> list and a new
  <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">preferences</span></media></gui>
  button will give you access to the extra settings.</p>

  <p>When you use multiple layouts, you can choose to have all windows use the
  same layout or to set a different layout for each window. Using a different
  layout for each window is useful, for example, if you’re writing an article
  in another language in a word processor window. Your keyboard selection will
  be remembered for each window as you switch between windows. Press the
  <gui style="button">Options</gui> button to select how you want to manage
  multiple layouts.</p>

  <p>ટોચની પટ્ટી એ વર્તમાન લેઆઉટ માટે ટૂંકા ઓળખકર્તાને દર્શાવશે, જેમ કે મૂળભૂત અંગ્રેજી લેઆઉટ માટે <gui>en</gui>. લેઆઉટ સૂચક પર ક્લિક કરો અને લેઆઉટને પસંદ કરો જે તમે મેનુમાંથી વાપરવા માંગો છો. જો પસંદ થયેલ ભાષા પાસે કોઇપણ વધારાનાં સુયોજનો હોય તો, તેઓને ઉપલબ્ધ લેઆઉટની યાદી નીચે બતાવેલ હશે. આ તમને તમારાં સુયોજનોની ઝડપી ઝાંખીને આપે છે. તમે સંદર્ભ માટે વર્તમાન કિબોર્ડ લેઆઉટ સાથે ઇમેજને પણ ખોલી શકે છે.</p>

  <p>The fastest way to change to another layout is by using the 
  <gui>Input Source</gui> <gui>Keyboard Shortcuts</gui>. These shortcuts open 
  the <gui>Input Source</gui> chooser where you can move forward and backward. 
  By default, you can switch to the next input source with 
  <keyseq><key xref="keyboard-key-super">Super</key><key>Space</key></keyseq>
  and to the previous one with
  <keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq>. You can
  change these shortcuts in the <gui>Keyboard</gui> settings.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
