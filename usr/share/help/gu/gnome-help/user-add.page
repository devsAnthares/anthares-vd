<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="gu">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.8" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>નવાં વપરાશકર્તાઓને ઉમેરો કે બીજા લોકો કમ્પ્યૂટરમાં પ્રવેશી શકે છે.</desc>
  </info>

  <title>નવા વપરાશકર્તા ખાતાને ઉમેરો</title>

  <p>તમે તમારાં કમ્પ્યૂટરમાં ઘણા વપરાશકર્તા ખાતાને ઉમેરી શકો છો. તમારી ઘરમાં અથવા કંપનીમાં દરેક માણસ માટે એક ખાતાને આપો. દરેક વપરાશકર્તા પાસે તેનું પોતાનું ઘર ફોલ્ડર, દસ્તાવેજો, અને સુયોજનો છે.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to add user accounts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Press the <gui style="button">+</gui> button, below the list of
      accounts on the left, to add a new user account.</p>
    </item>
    <item>
      <p>If you want the new user to have
      <link xref="user-admin-explain">administrative access</link> to the
      computer, select <gui>Administrator</gui> for the account type.</p>
      <p>Administrators can do things like add and delete users, install software
      and drivers, and change the date and time.</p>
    </item>
    <item>
      <p>Enter the new user’s full name. The username will be filled in
      automatically based on the full name. If you do not like the proposed
      username, you can change it.</p>
    </item>
    <item>
      <p>You can choose to set a password for the new user, or let them set it
      themselves on their first login.</p>
      <p>If you choose to set the password now,
      you can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
    </item>
    <item>
      <p><gui>ઉમેરો</gui> પર ક્લિક કરો.</p>
    </item>
  </steps>

  <p>If you want to change the password after creating the account, select the
  account, <gui style="button">Unlock</gui> the panel and press the current
  password status.</p>

  <note>
    <p>In the <gui>Users</gui> panel, you can click the image next to the
    user’s name to the right to set an image for the account. This image will
    be shown in the login window. GNOME provides some stock photos you can use,
    or you can select your own or take a picture with your webcam.</p>
  </note>

</page>
