<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="gu">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>વધારે કઠીન પાસવર્ડ લાંબો સમય વાપરો.</desc>
  </info>

  <title>સુરક્ષિત પાસવર્ડને પસંદ કરો</title>

  <note style="important">
    <p>તમારી માટે યાદ રાખવા માટે તમારા પાસવર્ડને બનાવો, પરંતુ બીજાઓ માટે મુશ્કેલી છે ધારવા માટે (કમ્પ્યૂટર કાર્યક્રમોને સમાવી રહ્યા છે).</p>
  </note>

  <p>સારા પાસવર્ડને પસંદ કરવાનુ એ તમારાં કમ્પ્યૂટરને સલામત રાખશે. જો તમારો પાસવર્ડ એ ધારવા માટે સરળ હોય તો, કોઇક એને મેળવી શકે છે અને તમારી વ્યક્તિગત જાણકારીમાં પ્રવેશ મેળવી શકે છે.</p>

  <p>લોકો તમારાં પાસવર્ડને ધારવનો પ્રયત્ન કરવા માટે કમ્પ્યૂટરને પદ્દતિસર વાપરી શકે છે, અથવા એક કે જે માણસ માટે વાપરવાનું મુશ્કેલીભર્યુ હશે તે જાણવા માટે કમ્પ્યૂટર કાર્યક્રમ માટે એકદમ સરળ હોઇ શકે છે. અહિંયા સારા પાસવર્ડને પસંદ કરવા માટે અમુક મદદો અહિંયા છે:</p>
  
  <list>
    <item>
      <p>પાસવર્ડમાં મોટા અને નાનાં અક્ષરો,આંકડા, સંકેતો અને જગ્યાનાં મિશ્રણને વાપરો. આ તેને ધારવા માટે મુશ્કેલ બનાવે છે; ત્યાં ઘણા સંકેતો છે જેમાંથી પસંદ કરવાનુ હોય છે, વધારે શક્ય પાસવર્ડનો મતલબ એ છે કે કોઇક જ્યારે તમારાં પાસવર્ડને ધારવાનો પ્રયત્ન કરે ત્યારે ચકાસશે.</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>તમારાં પાસવર્ડને શક્ય તેટલો લાંબો રાખો. વધારે અક્ષરો કે જે તે સમાવે, તે લાંબો હોય તો માણસ અથવા કમ્પ્યૂટરને ધારવા માટે લાંબો સમય લાગી શકે છે.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>કોઇપણ સંજ્ઞાઓનો ઉપયોગ કરશો નહિં.</p>
    </item>
    <item>
      <p>પાસવર્ડને પસંદ કરો કે જેને ઝડપથી ટાઇપ કરી શકાય છે, કોઇપણ તેને સક્ષમ બનાવે તેની શક્યતાઓને ઘટાડવા માટે કે જે તમે ટાઇપ કરેલ છે જો તમને જોવા માટે તેઓ આવું કરે છે.</p>
      <note style="tip">
        <p>નીચે કોઇપણ જગ્યાએ તમારો પાસવર્ડ કદી લખો નહિં. તેઓ સરળતાથી મળી શકે છે!</p>
      </note>
    </item>
    <item>
      <p>વિવિધ વસ્તુઓ માટે વિવિધ પાસવર્ડને વાપરો.</p>
    </item>
    <item>
      <p>વિવિધ ખાતા માટે વિવિધ પાસવર્ડને વાપરો.</p>
      <p>જો તમે બધા તમારાં ખાતા માટે એજ પાસવર્ડ વાપરો તો, કોઇપણ તેને ધારે છે તે તરત જ બધા તમારા ખાતામાં પ્રવેશ મેળવવા સક્ષમ હશે.</p>
      <p>It can be difficult to remember lots of passwords, however. Though not
      as secure as using a different passwords for everything, it may be easier
      to use the same one for things that do not matter (like websites), and
      different ones for important things (like your online banking account and
      your email).</p>
   </item>
   <item>
     <p>નિયમિત રીતે તમારા પાસવર્ડને બદલો.</p>
   </item>
  </list>

</page>
