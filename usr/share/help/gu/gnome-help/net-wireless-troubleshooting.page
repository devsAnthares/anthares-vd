<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="gu">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Ubuntu દસ્તાવેજીકરણ વિકિ માટે ફાળકો</name>
    </credit>
    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  </info>

  <title>વાયરલેસ નેટવર્ક મુશ્કેલીનિવારક</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>ઇન્ટરનેટ સાથે જોડાયેલ તમારાં કમ્પ્યૂટરને મેળવવા માટે નીચેનાં તબક્કાઓ મારફતે અમે આગળ વધશો.</p>

  <list style="numbered compact">
    <item>
      <p>પ્રારંભિક ચકાસણી કરી રહ્યા છે</p>
    </item>
    <item>
      <p>તમારાં હાર્ડવેર વિશે જાણકારીને મેળવી રહ્યા છે</p>
    </item>
    <item>
      <p>તમારાં હાર્ડવેરને ચકાસી રહ્યા છે</p>
    </item>
    <item>
      <p>તમારાં વાયરલેસ રાઉટર સાથે જોડાણ બનાવવાનો પ્રયત્ન કરી રહ્યા છે</p>
    </item>
    <item>
      <p>તમારાં મોડેમ અને રાઉટરને ચકાસી રહ્યા છે</p>
    </item>
  </list>

  <p>શરૂ કરવા માટે, પાનાંની ટોચની જમણી બાજુ પર <em>આગળ</em> કડી પર ક્લિક કરો. આ કડી અને બીજા પાનાં પર તેનાં જેવા છે, માર્ગદર્શિકામાં દરેક પગલા મારફતે તમને લઇ જશે.</p>

  <note>
    <title>આદેશ વાક્યને વાપરી રહ્યા છે</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
