<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="el">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Χρήση μεγαλυτέρων, περισσότερο πολύπλοκων κωδικών πρόσβασης.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Επιλέξτε έναν ασφαλή κωδικό πρόσβασης</title>

  <note style="important">
    <p>Κάντε τους κωδικούς πρόσβασής σας πολύ εύκολους για να τους θυμάστε, αλλά πολύ δύσκολους για να τους μαντέψουν οι άλλοι (συμπεριλαμβανομένων και των προγραμμάτων υπολογιστή).</p>
  </note>

  <p>Η επιλογή ενός καλού κωδικού πρόσβασης θα βοηθήσει στην διατήρηση του υπολογιστή σας με ασφάλεια. Εάν ο κωδικός σας είναι εύκολος στην πρόβλεψη, κάποιος ενδέχεται να τον φανταστεί και να αποκτήσει πρόσβαση στις προσωπικές σας πληροφορίες.</p>

  <p>Κάποια άτομα μπορούν ακόμα να χρησιμοποιήσουν υπολογιστές για να προσπαθήσουν συστηματικά να μαντέψουν τον κωδικό πρόσβασής σας, έτσι ακόμα και κάποιος που θα ήταν δύσκολος για έναν άνθρωπο να μαντέψει, μπορεί να είναι πολύ εύκολος για να πρόγραμμα υπολογιστή να τον βρει. Εδώ είναι μερικές συμβουλές για επιλογή ενός καλού κωδικού πρόσβασης:</p>
  
  <list>
    <item>
      <p>Χρησιμοποιήστε ένα συνδυασμό κεφαλαίων και πεζών γραμμάτων, αριθμών, συμβόλων και κενών στον κωδικό πρόσβασης. Αυτό το κάνει πιο δύσκολο να το μαντέψετε· υπάρχουν περισσότερα σύμβολα από τα οποία θα επιλέξετε, που σημαίνει περισσότερους δυνατούς κωδικούς πρόσβασης που πρέπει κάποιος να ελέγξει όταν προσπαθεί να μαντέψει τους δικούς σας.</p>
      <note>
        <p>A good method for choosing a password is to take the first letter of
        each word in a phrase that you can remember. The phrase could be the
        name of a movie, a book, a song or an album. For example, “Flatland: A
        Romance of Many Dimensions” would become F:ARoMD or faromd or f:
        aromd.</p>
      </note>
    </item>
    <item>
      <p>Κάντε τον κωδικό πρόσβασής σας όσο μεγαλύτερο μπορείτε. Όσους περισσότερους χαρακτήρες περιέχει, τόσο περισσότερο θα πάρει για ένα άτομο ή υπολογιστή να τον μαντέψει.</p>
    </item>
    <item>
      <p>Do not use any words that appear in a standard dictionary in any
      language. Password crackers will try these first. The most common
      password is “password” — people can guess passwords like this very
      quickly!</p>
    </item>
    <item>
      <p>Do not use any personal information such as a date, license plate
      number, or any family member’s name.</p>
    </item>
    <item>
      <p>Μην χρησιμοποιείτε ουσιαστικά.</p>
    </item>
    <item>
      <p>Επιλέξτε έναν κωδικό πρόσβασης που να μπορεί να πληκτρολογηθεί γρήγορα, για να μειώσετε τις πιθανότητες σε κάποιον να δει τι πληκτρολογήσατε αν έτυχε να σας παρακολουθεί.</p>
      <note style="tip">
        <p>Ποτέ μη γράφετε κάπου τους κωδικούς πρόσβασής σας. Μπορούν εύκολα να βρεθούν!</p>
      </note>
    </item>
    <item>
      <p>Χρήση διαφορετικών κωδικών πρόσβασης για διαφορετικά πράγματα.</p>
    </item>
    <item>
      <p>Χρήση διαφορετικών κωδικών πρόσβασης για διαφορετικούς λογαριασμούς.</p>
      <p>Αν χρησιμοποιείτε τον ίδιο κωδικό πρόσβασης για όλους σας τους λογαριασμούς, οποιοσδήποτε που θα τον μαντέψει θα έχει άμεση πρόσβαση σε όλους τους λογαριασμούς σας αμέσως.</p>
      <p>Μπορεί να είναι δύσκολο να θυμάστε πολλούς κωδικούς πρόσβασης. Αν και όχι τόσο ασφαλές όσο η χρήση διαφορετικών κωδικών για καθετί, μπορεί να είναι ευκολότερο να χρησιμοποιήσετε τον ίδιο κωδικό για πράγματα που δεν πειράζουν (όπως ιστότοποι) και διαφορετικούς κωδικούς πρόσβασης για σημαντικά πράγματα (όπως τον διαδικτυακό σας τραπεζικό λογαριασμό και τον κωδικό αλληλογραφίας).</p>
   </item>
   <item>
     <p>Να αλλάζετε τους κωδικούς πρόσβασής σας συχνά.</p>
   </item>
  </list>

</page>
