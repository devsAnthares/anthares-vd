<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-file-properties-basic" xml:lang="el">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Δείτε βασικές πληροφορίες ενός αρχείου, ορίστε δικαιώματα και επιλέξτε τις προεπιλεγμένες εφαρμογές.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Ιδιότητες αρχείου</title>

  <p>Για να προβάλετε πληροφορίες για ένα αρχείο ή φάκελο, κάντε δεξί κλικ σε αυτό και επιλέξτε <gui>Ιδιότητες</gui>. Μπορείτε επίσης να επιλέξετε το αρχείο και να πατήσετε <keyseq><key>Alt</key><key>Enter</key></keyseq>.</p>

  <p>Το παράθυρο ιδιοτήτων αρχείου σας εμφανίζει πληροφορίες όπως τον τύπο του αρχείου, το μέγεθος του αρχείου και πότε τροποποιήθηκε τελευταία. Εάν χρειάζεστε αυτήν την πληροφορία συχνά, μπορείτε να την εμφανίσετε στο <link xref="nautilus-list">στήλες προβολής λίστας</link> ή <link xref="nautilus-display#icon-captions">τίτλοι εικονιδίου</link>.</p>

  <p>Οι πληροφορίες στην καρτέλα <gui>Βασικά</gui> εξηγούνται παρακάτω. Υπάρχουν επίσης οι καρτέλες <gui><link xref="nautilus-file-properties-permissions">Δικαιώματα</link></gui> και <gui><link xref="files-open#default">Άνοιγμα με</link></gui>. Για συγκεκριμένους τύπους αρχείων, όπως εικόνες και βίντεο, θα υπάρχει μια πρόσθετη καρτέλα που παρέχει πληροφορίες όπως τις διαστάσεις, διάρκεια και κωδικοποιητή-αποκωδικοποιητή.</p>

<section id="basic">
 <title>Βασικές ιδιότητες</title>
 <terms>
  <item>
    <title><gui>Όνομα</gui></title>
    <p>Μπορείτε να μετονομάσετε το αρχείο αλλάζοντας αυτό το πεδίο. Μπορείτε επίσης να μετονομάσετε ένα αρχείο έξω από το παράθυρο ιδιοτήτων. Δείτε <link xref="files-rename"/>.</p>
  </item>
  <item>
    <title><gui>Τύπος</gui></title>
    <p>This helps you identify the type of the file, such as PDF document,
    OpenDocument Text, or JPEG image. The file type determines which
    applications can open the file, among other things. For example, you
    can’t open a picture with a music player. See <link xref="files-open"/>
    for more information on this.</p>
    <p>Ο <em>τύπος MIME</em> του αρχείου προβάλλεται σε παρενθέσεις· ο τύπος MIME είναι ένας τυπικός τρόπος που οι υπολογιστές χρησιμοποιούν για να αναφερθούν σε αυτόν τον τύπο αρχείου.</p>
  </item>

  <item>
    <title>Περιεχόμενα</title>
    <p>Αυτό το πεδίο εμφανίζεται εάν κοιτάξετε στις ιδιότητες ενός φακέλου αντί για ένα αρχείο. Βοηθά να βλέπετε τον αριθμό των στοιχείων στον φάκελο. Εάν ο φάκελος περιλαμβάνει άλλους φακέλους, κάθε εσωτερικός φάκελος μετριέται ως ένα στοιχείο, ακόμα κι αν περιέχει παραπέρα στοιχεία. Κάθε φάκελος μετριέται επίσης ως ένα στοιχείο. Εάν ο φάκελος είναι κενός, τα περιεχόμενα θα εμφανίσουν <gui>τίποτα</gui>.</p>
  </item>

  <item>
    <title>Μέγεθος</title>
    <p>Αυτό το πεδίο εμφανίζεται εάν κοιτάξετε σε ένα αρχείο (όχι φάκελο). Το μέγεθος του αρχείου σας λέει πόσο χώρο δίσκου καταλαμβάνει. Αυτό είναι επίσης ένας δείκτης πόσο χρόνο θα πάρει να κάνετε λήψη ενός αρχείου ή να το στείλετε με ηλεκτρονική αλληλογγραφία (μεγάλα αρχεία παίρνουν περισσότερο χρόνο για αποστολή/λήψη).</p>
    <p>Τα μεγέθη μπορεί να δοθούν σε ψηφιολέξεις, KB, MB, ή GB· στην περίπτωση των τελευταίων τριών, το μέγεθος σε ψηφιολέξεις θα δοθεί επίσης σε παρενθέσεις. Τεχνικά, 1 KB είναι 1024 ψηφιολέξεις, 1 MB είναι 1024 KB κ.ο.κ.</p>
  </item>

  <item>
    <title>Γονικός φάκελος</title>
    <p>The location of each file on your computer is given by its <em>absolute
    path</em>. This is a unique “address” of the file on your computer, made up
    of a list of the folders that you would need to go into to find the file.
    For example, if Jim had a file called <file>Resume.pdf</file> in his Home
    folder, its parent folder would be <file>/home/jim</file> and its location
    would be <file>/home/jim/Resume.pdf</file>.</p>
  </item>

  <item>
    <title>Ελεύθερος χώρος</title>
    <p>Αυτό το πεδίο εμφανίζεται μόνο για φακέλους. Δίνει το ποσό του χώρου του δίσκου που είναι διαθέσιμο στον δίσκο που βρίσκεται ο φάκελος. Αυτό είναι χρήσιμο για να ελεγχθεί εάν ο σκληρός δίσκος είναι πλήρης.</p>
  </item>

  <item>
    <title>Προσπελάστηκε</title>
    <p>Η ημερομηνία και η ώρα που ανοίχτηκε το αρχείο για τελευταία φορά.</p>
  </item>

  <item>
    <title>Τροποποιημένο</title>
    <p>Η ημερομηνία και η ώρα κατά την οποία το αντικείμενο τροποποιήθηκε και αποθηκεύτηκε για τελευταία φορά.</p>
  </item>
 </terms>
</section>

</page>
