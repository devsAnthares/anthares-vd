<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="el">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>Ελέγξτε ποιος μπορεί να προβάλει και να επεξεργαστεί τα αρχεία και τους φακέλους σας.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>
  <title>Ορισμός δικαιωμάτων αρχείου</title>

  <p>Μπορείτε να χρησιμοποιήσετε τα δικαιώματα αρχείου για να ελέγξετε ποιος μπορεί να προβάλει και να επεξεργαστεί αρχεία που κατέχετε. Για προβολή και ορισμό των δικαιωμάτων για ένα αρχείο, κάντε δεξί κλικ σε αυτό και επιλέξτε <gui>Ιδιότητες</gui>, έπειτα επιλέξτε την καρτέλα <gui>Δικαιώματα</gui>.</p>

  <p>Δείτε <link xref="#files"/> και <link xref="#folders"/> παρακάτω για λεπτομέρειες στους τύπους των δικαιωμάτων που μπορείτε να ορίσετε.</p>

  <section id="files">
    <title>Αρχεία</title>

    <p>You can set the permissions for the file owner, the group owner,
    and all other users of the system. For your files, you are the owner,
    and you can give yourself read-only or read-and-write permission.
    Set a file to read-only if you don’t want to accidentally change it.</p>

    <p>Every user on your computer belongs to a group. On home computers,
    it is common for each user to have their own group, and group permissions
    are not often used. In corporate environments, groups are sometimes used
    for departments or projects. As well as having an owner, each file belongs
    to a group. You can set the file’s group and control the permissions for
    all users in that group. You can only set the file’s group to a group you
    belong to.</p>

    <p>You can also set the permissions for users other than the owner and
    those in the file’s group.</p>

    <p>Εάν το αρχείο είναι ένα πρόγραμμα, όπως μια δέσμη ενεργειών, πρέπει να επιλέξετε <gui>Να επιτρέπεται η εκτέλεση αρχείου ως προγράμματος</gui> για την εκτέλεση του. Ακόμα και με αυτήν την επιλογή σημειωμένη, ο διαχειριστής αρχείων μπορεί ακόμα να ανοίξει το αρχείο σε μια εφαρμογή ή να σας ζητήσει τι να κάνετε. Για περισσότερες πληροφορίες δείτε <link xref="nautilus-behavior#executable"/>.</p>
  </section>

  <section id="folders">
    <title>Φάκελοι</title>
    <p>Μπορείτε να ορίσετε δικαιώματα σε φακέλους για τον κάτοχο, ομάδα και άλλους χρήστες. Δείτε τις λεπτομέρειες των δικαιωμάτων αρχείου πιο πάνω για μια εξήγηση κατόχων, ομάδων και άλλων χρηστών.</p>
    <p>Τα δικαιώματα που μπορείτε να ορίσετε για έναν φάκελο είναι διαφορετικά από τα δικαιώματα που μπορείτε να ορίσετε για ένα αρχείο.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">Κανένα</gui></title>
        <p>Ο χρήστης δεν θα μπορεί ακόμα και να δει τι αρχεία είναι στον φάκελο.</p>
      </item>
      <item>
        <title><gui>Μόνο λίστα αρχείων</gui></title>
        <p>Οι χρήστες μπορούν να δουν ποια αρχεία είναι στον φάκελο, αλλά δεν θα μπορούν να ανοίξουν, να δημιουργήσουν ή να διαγράψουν αρχεία.</p>
      </item>
      <item>
        <title><gui>Προσπέλαση αρχείων</gui></title>
        <p>Ο χρήστης θα μπορεί να ανοίξει αρχεία στον φάκελο (με την προϋπόθεση ότι έχει δικαίωμα να το κάνει σε ένα συγκεκριμένο αρχείο), αλλά δεν θα μπορεί να δημιουργήσει νέα αρχεία ή να διαγράψει αρχεία.</p>
      </item>
      <item>
        <title><gui>Δημιουργία και διαγραφή αρχείων</gui></title>
        <p>Ο χρήστης θα έχει πλήρη πρόσβαση στον φάκελο, συμπεριλαμβανομένου του ανοίγματος, της δημιουργίας και διαγραφής αρχείων.</p>
      </item>
    </terms>

    <p>Μπορείτε επίσης γρήγορα να ορίσετε τα δικαιώματα ενός αρχείου για όλα τα αρχεία στον φάκελο κάνοντας κλικ στο <gui>Αλλαγή δικαιωμάτων για τα περιεχόμενα αρχεία</gui>. Χρησιμοποιήστε τις αναπτυσσόμενες λίστες για να ρυθμίσετε τα δικαιώματα των περιεχόμενων αρχείων ή φακέλων και πατήστε <gui>Αλλαγή</gui>. Τα δικαιώματα εφαρμόζονται σε αρχεία και φακέλους σε υποφακέλους επίσης, σε κάθε βάθος.</p>
  </section>

</page>
