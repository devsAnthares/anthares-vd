<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="el">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ορίστε μιας εικόνα, ένα χρώμα ή μια διαβάθμιση ως παρασκήνιο της επιφάνειας εργασίας σας ή το παρασκήνιο του κλειδώματος οθόνης.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αλλαγή παρασκηνίου της επιφάνειας εργασίας και της οθόνης κλειδώματος</title>

  <p>You can change the image used for your backgrounds or set it to be a
  solid color.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Background</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Background</gui> or <gui>Lock Screen</gui>.</p>
    </item>
    <item>
      <p>Υπάρχουν τρεις εμφανιζόμενες επιλογές στην κορυφή:</p>
      <list>
        <item>
          <p>Επιλέξτε <gui>Ταπετσαρίες</gui> για να χρησιμοποιήσετε μία από τις πολλές επαγγελματικές εικόνες παρασκηνίου που έρχονται με το GNOME. Μερικές ταπετσαρίες αλλάζουν κατά τη διάρκεια της ημέρας. Αυτές οι ταπετσαρίες έχουν ένα μικρό εικονίδιο ρολογιού στην κάτω δεξιά γωνία.</p>
        </item>
        <item>
          <p>Επιλέξτε <gui>Εικόνες</gui> για να χρησιμοποιήσετε μια προσωπική σας φωτογραφίας από τον φάκελο <file>Εικόνες</file>, καθώς οι περισσότερες εφαρμογές διαχείρισης φωτογραφιών τις αποθηκεύουν εκεί. Αν θέλετε να χρησιμοποιήσετε μια εικόνα που δεν είναι στον φάκελο, τότε χρησιμοποιήστε είτε την εφαρμογή <app>Αρχεία</app> για να κάνετε δεξί κλικ στην εικόνα που επιθυμείτε επιλέγοντας <gui>Ορισμός ως ταπετσαρία</gui>, ή ανοίγοντας την εικόνα με την <app>Εφαρμογή προβολής εικόνων</app>, κάνοντας κλικ στο κουμπί του μενού στην γραμμή τίτλου και επιλέγοντας <gui>Ορισμός ως ταπετσαρία</gui>.</p>
        </item>
        <item>
          <p>Επιλέξτε <gui>Χρώματα</gui> για να χρησιμοποιήσετε ένα ομοιόμορφο χρώμα.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Οι ρυθμίσεις εφαρμόζονται αμέσως.</p>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Εναλλαγή σε έναν κενό χώρο εργασίας</link> για να δείτε όλη την επιφάνεια εργασίας.</p>
    </item>
  </steps>

</page>
