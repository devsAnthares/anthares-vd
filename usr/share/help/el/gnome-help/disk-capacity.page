<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="el">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Χρησιμοποιήστε τον <gui>Αναλυτή χρήσης δίσκου</gui> ή την <gui>Παρακολούθηση συστήματος</gui> για να ελέγξετε τον χώρο και τη χωρητικότητα.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Ελέγξτε πόσος χώρος έχει απομείνει στον δίσκο</title>

  <p>Μπορείτε να ελέγξετε πόσος χώρος έχει απομείνει στον δίσκο με τον <app>Αναλυτή χρήσης δίσκου</app> ή την <app>Παρακολούθηση συστήματος</app>.</p>

<section id="disk-usage-analyzer">
<title>Έλεγχος με τον Αναλυτή χρήσης δίσκου</title>

  <p>Ελέγξετε τον ελεύθερο χώρο και τη χωρητικότητα του δίσκου χρησιμοποιώντας τον <app>Αναλυτή χρήσης δίσκου</app>:</p>

  <list>
    <item>
      <p>Ανοίξτε το <app>Αναλυτής χρήσης δίσκων</app> από την επισκόπηση <gui>Δραστηριότητες</gui>. Το παράθυρο θα εμφανίσει μια λίστα με θέσεις αρχείων μαζί με τη χρήση και τη χωρητικότητα του καθενός.</p>
    </item>
    <item>
      <p>Κάντε κλικ σε ένα αντικείμενο στη λίστα για να δείτε μια λεπτομερείς περίληψη της χρήσης του. Πατήστε το κουμπί του μενού και έπειτα το <gui>Σάρωση φακέλου…</gui> ή <gui>Σάρωση απομακρυσμένου φακέλου…</gui> για να σαρώσετε μια διαφορετική τοποθεσία.</p>
    </item>
  </list>
  <p>Οι πληροφορίες εμφανίζονται σύμφωνα με τον <gui>Φάκελο</gui>, το <gui>Μέγεθος</gui>, τα <gui>Περιεχόμενα</gui> και την <gui>Τελευταία τροποποίηση</gui> των δεδομένων. Δείτε περισσότερες λεπτομέρειες στο <link href="help:baobab"><app>Αναλυτής χρήσης δίσκου</app></link>.</p>

</section>

<section id="system-monitor">

<title>Έλεγχος με την «Παρακολούθηση συστήματος»</title>

  <p>Για να ελέγξετε τον ελεύθερο χώρο και τη χωρητικότητα του δίσκου με την <app>Παρακολούθηση συστήματος</app>:</p>

<steps>
 <item>
  <p>Ανοίξτε την εφαρμογή <app>Παρακολούθηση συστήματος</app> από την επισκόπηση <gui>Δραστηριότητες</gui>.</p>
 </item>
 <item>
  <p>Select the <gui>File Systems</gui> tab to view the system’s partitions and
  disk space usage.  The information is displayed according to <gui>Total</gui>,
  <gui>Free</gui>, <gui>Available</gui> and <gui>Used</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>Τι συμβαίνει εάν ο δίσκος είναι υπερβολικά γεμάτος;</title>

  <p>Εάν ο δίσκος είναι υπερβολικά γεμάτος θα πρέπει:</p>

 <list>
  <item>
   <p>Delete files that aren’t important or that you won’t use anymore.</p>
  </item>
  <item>
   <p>Make <link xref="backup-why">backups</link> of the important files that
   you won’t need for a while and delete them from the hard drive.</p>
  </item>
 </list>
</section>

</page>
