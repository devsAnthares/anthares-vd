<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="el">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Ενεργοποιήστε τα πλήκτρα ποντικιού για να ελέγξετε το ποντίκι με το αριθμητικό υποπληκτρολόγιο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Click and move the mouse pointer using the keypad</title>

  <p>Εάν δυσκολεύεστε χρησιμοποιώντας ένα ποντίκι ή μια άλλη συσκευή κατάδειξης, μπορείτε να ελέγξετε τον δείκτη ποντικιού χρησιμοποιώντας το αριθμητικό υποπληκτρολόγιο στο πληκτρολόγιό σας. Αυτή η λειτουργία λέγεται <em>πλήκτρα ποντικιού</em>.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      <p>You can access the <gui>Activities</gui> overview by pressing on it,
      by moving your mouse pointer against the top-left corner of the screen,
      by using <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq>
      followed by <key>Enter</key>, or by using the
      <key xref="keyboard-key-super">Super</key> key.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Universal Access</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Use the up and down arrow keys to select <gui>Mouse Keys</gui> in the
      <gui>Pointing &amp; Clicking</gui> section, then press <key>Enter</key>
      to switch <gui>Mouse Keys</gui> to <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Βεβαιωθείτε ότι το <key>Num Lock</key> είναι ανενεργό. Θα μπορείτε τώρα να μετακινήσετε τον δείκτη του ποντικιού σας χρησιμοποιώντας το υποπληκτρολόγιο.</p>
    </item>
  </steps>

  <p>Το υποπληκτρολόγιο είναι ένα σύνολο αριθμητικών πλήκτρων του πληκτρολογίου σας, συνήθως τακτοποιημένο σε ένα τετράγωνο πλέγμα. Αν έχετε ένα πληκτρολόγιο χωρίς υποπληκτρολόγιο (όπως ένα πληκτρολόγιο φορητού υπολογιστή), μπορεί να χρειαστείτε να κρατάτε πατημένο το πλήκτρο λειτουργίας (<key>Fn</key>) και να χρησιμοποιήσετε συγκεκριμένα άλλα πλήκτρα στο πληκτρολόγιό σας ως υποπληκτρολόγιο. Αν χρησιμοποιείτε αυτό το γνώρισμα συχνά σε έναν φορητό υπολογιστή, μπορείτε να αγοράσετε ένα εξωτερικό USB ή ένα αριθμητικό Bluetooth υποπληκτρολόγιο.</p>

  <p>Κάθε αριθμός στο υποπληκτρολόγιο αντιστοιχεί σε μια κατεύθυνση. Για παράδειγμα, πατώντας <key>8</key> θα μετακινήσει τον δείκτη προς τα πάνω και πατώντας <key>2</key> θα τον μετακινήσει προς τα κάτω. Πατήστε το πλήκτρο <key>5</key> για να κάνετε κλικ μια φορά με το ποντίκι, ή πατήστε το γρήγορα δυο φορές για διπλο κλικ.</p>

  <p>Τα περισσότερα πληκτρολόγια έχουν ένα ειδικό πλήκτρο που επιτρέπει το δεξιό κλικ, μερικές φορές λέγεται πλήκτρο <key xref="keyboard-key-menu">Μενού</key>. Σημειώστε, όμως, ότι αυτό το πλήκτρο αποκρίνεται εκεί που είναι η εστίαση του πληκτρολογίου, όχι εκεί που είναι ο δείκτης ποντικιού. Δείτε <link xref="a11y-right-click"/> για πληροφορίες πώς να κάνετε δεξί κλικ κρατώντας πατημένο το <key>5</key> ή το αριστερό πλήκτρο του ποντικιού.</p>

  <p>Εάν θέλετε να χρησιμοποιήσετε το υποπληκτρολόγιο για να πληκτρολογήσετε αριθμούς ενώ είναι ενεργά τα πλήκτρα ποντικιού, ενεργοποιήστε το <key>Num Lock</key>. Εν τούτοις το ποντίκι δεν μπορεί να ελεγχθεί με το υποπληκτρολόγιο όταν το <key>Num Lock</key> είναι ενεργό.</p>

  <note>
    <p>Τα κανονικά αριθμητικά πλήκτρα, σε μια γραμμή στην κορυφή του πληκτρολογίου, δεν ελέγχουν τον δείκτη ποντικιού. Μόνο τα αριθμητικά πλήκτρα του υποπληκτρολογίου μπορούν να χρησιμοποιηθούν.</p>
  </note>

</page>
