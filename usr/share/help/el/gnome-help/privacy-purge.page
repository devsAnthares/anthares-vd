<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="el">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ορίστε το πόσο συχνά τα προσωρινά αρχεία και τα απορρίμματά σας θα διαγράφονται από τον υπολογιστή σας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Εκκαθάριση απορριμμάτων &amp; προσωρινών αρχείων</title>

  <p>Ο καθαρισμός των απορριμμάτων και των προσωρινών αρχείων αφαιρεί ανεπιθύμητα και άχρηστα αρχεία από τον υπολογιστή σας και ελευθερώνει επίσης περισσότερο χώρο στον σκληρό σας δίσκο. Μπορείτε να αδειάσετε χειροκίνητα τα απορρίμματά σας και να καθαρίσετε τα προσωρινά αρχεία σας, αλλά μπορείτε επίσης να ορίσετε να το κάνει αυτό ο υπολογιστής σας αυτόματα για σας.</p>

  <p>Τα προσωρινά αρχεία δημιουργούνται αυτόματα από τις εφαρμογές στο παρασκήνιο. Μπορούν να αυξήσουν την επίδοση παρέχοντας ένα αντίγραφο των δεδομένων που λήφθηκαν ή υπολογίστηκαν.</p>

  <steps>
    <title>Αυτόματη εκκαθάριση των απορριμμάτων και προσωρινών αρχείων</title>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Ιδιωτικότητα</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Ιδιωτικότητα</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Επιλέξτε <gui>Καθαρισμός απορριμμάτων &amp; προσωρινών αρχείων</gui>.</p>
    </item>
    <item>
      <p>Ορίστε έναν ή και τους δύο διακόπτες <gui>Αυτόματη εκκαθάριση απορριμμάτων</gui> ή <gui>Αυτόματη εκκαθάριση προσωρινών αρχείων</gui> σε <gui>ΝΑΙ</gui>.</p>
    </item>
    <item>
      <p>Ορίστε πόσο συχνά θα θέλατε τα <em>Απορρίμματα</em> σας και τα <em>Προσωρινά αρχεία</em> να καθαρίζονται αλλάζοντας την τιμή <gui>Εκκαθάριση μετά από</gui>.</p>
    </item>
    <item>
      <p>Χρησιμοποιήστε τα κουμπιά <gui>Άδειασμα απορριμμάτων</gui> ή <gui>Εκκαθάριση προσωρινών αρχείων</gui> για να εκτελέσετε αυτές τις ενέργειες άμεσα.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Μπορείτε να διαγράψετε αρχεία μόνιμα και κατευθείαν χωρίς να χρησιμοποιήσετε τα απορρίμματα. Για περισσότερες πληροφορίες, δείτε <link xref="files-delete#permanent"/>.</p>
  </note>

</page>
