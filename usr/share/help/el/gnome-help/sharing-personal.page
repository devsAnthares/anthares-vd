<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-personal" xml:lang="el">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.1" date="2013-09-23" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Επιτρέψτε σε άλλα άτομα να προσπελάζουν τα αρχεία στον <file>Δημόσιο</file> φάκελό σας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Μοιραστείτε τα προσωπικά σας αρχεία</title>

  <p>Μπορείτε να επιτρέψετε την πρόσβαση στον <file>Δημόσιο</file> φάκελο στον <file>Προσωπικό φάκελο</file> σας από άλλον υπολογιστή στο δίκτυο. Ρυθμίστε το <gui>Κοινή χρήση προσωπικών αρχείων</gui> για να επιτρέψετε σε άλλους να προσπελάσουν τα περιεχόμενα του φακέλου.</p>

  <note style="info package">
    <p>Πρέπει να έχετε εγκατεστημένο το πακέτο <app>gnome-user-share</app> για να είναι ορατή η <gui>Κοινή χρήση προσωπικών αρχείων</gui>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-user-share" style="button">Εγκατάσταση gnome-user-share</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Κοινή χρήση</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Κοινή χρήση</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Αν η <gui>Κοινή χρήση</gui> είναι <gui>ΑΝΕΝΕΡΓΗ</gui>, αλλάξτε την σε <gui>ΕΝΕΡΓΗ</gui>.</p>

      <note style="info"><p>Αν το κείμενο κάτω από το <gui>όνομα υπολογιστή</gui> σας επιτρέπει να το επεξεργαστείτε, μπορείτε να <link xref="sharing-displayname">αλλάξετε</link> το όνομα που ο υπολογιστής σας εμφανίζετε στο δίκτυο.</p></note>
    </item>
    <item>
      <p>Επιλέξτε <gui>Κοινή χρήση προσωπικών αρχείων</gui>.</p>
    </item>
    <item>
      <p>Αλλάξτε το <gui>Κοινή χρήση προσωπικών αρχείων</gui> σε <gui>ΝΑΙ</gui>. Αυτό σημαίνει ότι άλλα άτομα, στο τρέχον δίκτυό σας, θα μπορούν να συνδεθούν στον υπολογιστή σας και να προσπελάσουν αρχεία στον <file>Δημόσιο</file> φάκελό σας.</p>
      <note style="info">
        <p>Ένα <em>URI</em> εμφανίζεται με το οποίο ο <file>Δημόσιος</file> φάκελό σας μπορεί να προσπελαστεί από άλλους υπολογιστές στο δίκτυο.</p>
      </note>
    </item>
  </steps>

  <section id="security">
  <title>Ασφάλεια</title>

  <terms>
    <item>
      <title>Απαιτείται κωδικός πρόσβασης</title>
      <p>Για να ζητήσετε τα άλλα άτομα να χρησιμοποιούν έναν κωδικό πρόσβασης όταν συνδέονται με τον <file>Δημόσιο</file> φάκελό σας, αλλάξτε το <gui>Απαιτείται κωδικός πρόσβασης</gui> σε <gui>ΕΝΕΡΓΟ</gui>. Αν δεν χρησιμοποιείτε αυτήν την επιλογή, οποιοσδήποτε μπορεί να προσπαθήσει να προβάλει τον <file>Δημόσιο</file> φάκελό σας.</p>
      <note style="tip">
        <p>Αυτή η επιλογή είναι ανενεργή από προεπιλογή, αλλά θα πρέπει να την ενεργοποιήσετε και να ορίσετε έναν ασφαλή κωδικό πρόσβασης.</p>
      </note>
    </item>
  </terms>
  </section>

  <section id="networks">
  <title>Δίκτυα</title>

  <p>Η ενότητα <gui>Δίκτυα</gui> εμφανίζει τα δίκτυα στα οποία είσαστε προς το παρόν συνδεμένοι. Χρησιμοποιήστε τον διακόπτη <gui>ΝΑΙ | ΟΧΙ</gui> δίπλα στο κάθε δίκτυο για να επιλέξετε που μπορούν να διαμοιραστούν τα προσωπικά σας αρχεία.</p>

  </section>

</page>
