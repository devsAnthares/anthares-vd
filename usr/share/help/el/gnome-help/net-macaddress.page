<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="net-macaddress" xml:lang="el">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ο μοναδικός ταυτοποιητής που εκχωρείται στο υλικό του δικτύου.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Τι είναι μια διεύθυνση MAC;</title>

  <p>Μια <em>διεύθυνση MAC</em> είναι ένας μοναδικός ταυτοποιητής που αποδίδεται από τον κατασκευαστή σε ένα κομμάτι του υλικού του δικτύου (όπως μια ασύρματη κάρτα ή μια κάρτα ethernet). Το MAC σημαίνει <em>Έλεγχος πρόσβασης μέσων</em> και κάθε ταυτοποιητής προορίζεται να είναι μοναδικός για μια συγκεκριμένη συσκευή.</p>

  <p>Μια διεύθυνση MAC αποτελείται από έξι σύνολα των δύο χαρακτήρων, που ο καθένας χωρίζεται από μια διπλή τελεία. <code>00:1B:44:11:3A:B7</code> είναι ένα παράδειγμα μιας διεύθυνσης MAC.</p>

  <p>Για να ταυτοποιήσετε τη διεύθυνση MAC του δικού σας υλικού δικτύου:</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Δίκτυο</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Δίκτυο</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Επιλέξτε τη συσκευή, <gui>Wi-Fi</gui> ή <gui>Ενσύμαρτη</gui>, από τον αριστερό πίνακα.</p>
      <p>Η διεύθυνση MAC της ενσύρματης συσκευής για εμφανιστεί στα δεξιά ως <gui>Διεύθυνση υλικού</gui>.</p>
      
      <p>Κάντε κλικ στο κουμπί <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">ρυθμίσεις</span></media> για να δείτε τη διεύθυνση MAC για την ασύρματη διεύθυνση ως <gui>Διεύθυνση υλικού</gui> στον πίνακα <gui>Λεπτομέρειες</gui>.</p>
    </item>
  </steps>

  <p>In practice, you may need to modify or “spoof” a MAC address. For example,
  some internet service providers may require that a specific MAC address be
  used to access their service. If the network card stops working, and you need
  to swap a new card in, the service won’t work anymore. In such cases, you
  would need to spoof the MAC address.</p>

</page>
