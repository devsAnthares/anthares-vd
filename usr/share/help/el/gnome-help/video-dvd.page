<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-dvd" xml:lang="el">

  <info>
    <link type="guide" xref="media#videos"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.12.1" date="2014-03-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>

    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μπορεί να μην έχετε τους σωστούς κωδικοποιητές-αποκωδικοποιητές εγκατεστημένους, ή το DVD μπορεί να είναι σε εσφαλμένη περιοχή.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Why won’t DVDs play?</title>

  <p>If you insert a DVD into your computer and it doesn’t play, you may not
  have the right DVD <em>codecs</em> installed, or the DVD might be from a
  different <em>region</em>.</p>

<section id="codecs">
  <title>Εγκατάσταση των σωστών κωδικοποιητών-αποκωδικοποιητών για την αναπαραγωγή του DVD</title>

  <p>In order to play DVDs, you need to have the right <em>codecs</em>
  installed.  A codec is a piece of software that allows applications to read a
  video or audio format. If your movie player software doesn’t find the right
  codecs, it may offer to install them for you. If not, you’ll have to install
  the codecs manually — ask for help on how to do this, for example on your
  Linux distribution’s support forums.</p>

  <p>Τα DVD <em>προστατεύονται από αντιγραφή</em> επίσης χρησιμοποιώντας ένα σύστημα που λέγεται CSS. Αυτό αποτρέπει την αντιγραφή DVD, αλλά επίσης αποτρέπει την αναπαραγωγή τους εκτός και έχετε πρόσθετο λογισμικό που χειρίζεται την προστασία αντιγραφής. Αυτό το λογισμικό διατίθεται για έναν αριθμό διανομών Linux, αλλά δεν μπορεί να χρησιμοποιηθεί νόμιμα σε όλες τις χώρες. Μπορείτε να αγοράσετε έναν εμπορικό αποκωδικοποιητή DVD που μπορεί να χειρίζεται προστασία αντιγραφής από <link href="http://fluendo.com/shop/product/oneplay-dvd-player/">Fluendo</link>. Δουλεύει με Linux και πρέπει να είναι νόμιμο για χρήση σε όλες τις χώρες.</p>

  </section>

<section id="region">
  <title>Έλεγχος περιοχής του DVD</title>

  <p>DVDs have a <em>region code</em>, which tells you in which region of the
  world they are allowed to be played. If the region of your computer’s DVD
  player does not match the region of the DVD you are trying to play, you won’t
  be able to play the DVD. For example, if you have a Region 1 DVD player, you
  will only be allowed to play DVDs from North America.</p>

  <p>It is often possible to change the region used by your DVD player, but it
  can only be done a few times before it locks into one region permanently. To
  change the DVD region of your computer’s DVD player, use <link href="http://linvdr.org/projects/regionset/">regionset</link>.</p>

  <p>Μπορείτε να βρείτε <link href="https://en.wikipedia.org/wiki/DVD_region_code">περισσότερες πληροφορίες για τους κώδικες περιοχής DVD στο βικιπαιδεία</link>.</p>

</section>

</page>
