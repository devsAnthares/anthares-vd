<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="el">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μια λίστα φακέλων όπου μπορείτε να βρείτε έγγραφα, αρχεία και ρυθμίσεις των οποίων μπορεί να θέλετε να δημιουργήσετε αντίγραφα ασφαλείας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Πού μπορώ να βρω τα αρχεία των οποίων θέλω να δημιουργήσω αντίγραφα ασφαλείας;</title>

  <p>Η απόφαση για ποια αρχεία θα δημιουργήσετε αντίγραφα ασφαλείας και ο εντοπισμός αυτών, είναι το πιο δύσκολο βήμα όταν προσπαθείτε να κάνετε ένα αντίγραφο ασφαλείας. Παρακάτω είναι καταχωρισμένες οι πιο κοινές τοποθεσίες σημαντικών αρχείων και ρυθμίσεων των οποίων μπορεί να θέλετε να δημιουργήσετε αντίγραφα ασφαλείας.</p>

<list>
 <item>
  <p>Προσωπικά αρχεία (έγγραφα, μουσική, φωτογραφίες και βίντεο)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs and need to be translated.  You can find the correct translations for your language here: http://translationproject.org/domain/xdg-user-dirs.html">Αυτά είναι συνήθως αποθηκευμένα στον προσωπικό σας φάκελο (<file>/home/your_name</file>). Μπορεί να είναι σε υποφακέλους όπως επιφάνεια εργασίας, έγγραφα, εικόνες, μουσική και βίντεο.</p>
  <p>Εάν το μέσο αντιγραφής έχει αρκετό χώρο (εάν είναι ένας εξωτερικός σκληρός δίσκος, για παράδειγμα), σκεφτείτε να αντιγράψετε όλο τον προσωπικό φάκελο. Μπορείτε να ψάξετε πόσο χώρο δίσκου καταλαμβάνει ο προσωπικός φάκελος χρησιμοποιώντας τον <app>Αναλυτή χρήσης δίσκου</app>.</p>
 </item>

 <item>
  <p>Κρυφά αρχεία</p>
  <p>Οποιοδήποτε αρχείο ή όνομα φακέλου που ξεκινά με μια τελεία (.) κρύβεται από προεπιλογή. Για να δείτε κρυφά αρχεία, κάντε κλικ στο κουμπί <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">Εμφάνιση επιλογών</span></media></gui> στην εργαλειοθήκη και έπειτα επιλέξτε <gui>Εμφάνιση κρυφών αρχείων</gui>, ή πατήστε <keyseq><key>Ctrl</key><key>H</key></keyseq>. Μπορείτε να τα αντιγράψετε σε μια τοποθεσία αντιγραφής όπως κάθε άλλο αρχείο.</p>
 </item>

 <item>
  <p>Προσωπικές ρυθμίσεις (προτιμήσεις επιφάνειας εργασίας, θέματα και ρυθμίσεις λογισμικού)</p>
  <p>Οι περισσότερες εφαρμογές αποθηκεύουν τις ρυθμίσεις τους σε κρυφούς φακέλους μέσα στον προσωπικό σας φάκελο (δείτε πιο πάνω για πληροφορίες για κρυφά αρχεία).</p>
  <p>Most of your application settings will be stored in the hidden folders
 <file>.config</file> and <file>.local</file> in your Home folder.</p>
 </item>

 <item>
  <p>Καθολικές ρυθμίσεις συστήματος</p>
  <p>Οι ρυθμίσεις για τα σημαντικά μέρη του συστήματος δεν αποθηκεύονται στον προσωπικό σας φάκελο. Υπάρχει ένας αριθμός θέσεων που θα μπορούσαν να αποθηκευτούν, αλλά οι περισσότερες αποθηκεύονται στον φάκελο <file>/etc</file>. Γενικά, δεν θα χρειαστείτε να αντιγράψετε αυτά τα αρχεία σε έναν προσωπικό υπολογιστή. Εάν έχετε όμως ένα διακομιστή, θα πρέπει να αντιγράψετε τα αρχεία για τις υπηρεσίες που τρέχουν.</p>
 </item>
</list>

</page>
