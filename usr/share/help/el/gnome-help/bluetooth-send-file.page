<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="bluetooth-send-file" xml:lang="el">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Κοινή χρήση αρχείων σε συσκευές bluetooth όπως το τηλέφωνο σας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Αποστολή αρχείων σε μια συσκευή Bluetooth</title>

  <p>Μπορείτε να στείλετε αρχεία σε συνδεμένες συσκευές Bluetooth, όπως κινητά τηλέφωνα ή υπολογιστές. Μερικοί τύποι συσκευών δεν επιτρέπουν τη μεταφορά αρχείων, ή συγκεκριμένων τύπων αρχείων. Μπορείτε να στείλετε αρχεία χρησιμοποιώντας το παράθυρο ρυθμίσεων του Bluetooth.</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Bluetooth</gui> για να ανοίξετε τον πίνακα.</p>
    </item>
    <item>
      <p>Βεβαιωθείτε ότι το Bluetooth είναι ενεργό: ο διακόπτης στη γραμμή τίτλου πρέπει να οριστεί σε <gui>Ενεργό</gui>.</p>
    </item>
    <item>
      <p>Στον κατάλογο <gui>Συσκευές</gui>, επιλέξτε τη συσκευή στην οποία θα στείλετε τα αρχεία. Αν η επιθυμητή συσκευή δεν εμφανίζεται ως <gui>Συνδεδεμένη</gui> στον κατάλογο, χρειάζεται να <link xref="bluetooth-connect-device">συνδεθείτε</link> σε αυτή.</p>
      <p>Εμφανίζεται ένας πίνακας για την εξωτερική συσκευή.</p>
    </item>
    <item>
      <p>Κάντε κλικ στο <gui>Αποστολή αρχείων…</gui> και θα εμφανιστεί ο επιλογέας αρχείου.</p>
    </item>
    <item>
      <p>Επιλέξτε το αρχείο που θέλετε να στείλετε και κάντε κλικ στο <gui>Επιλογή</gui>.</p>
      <p>Για να στείλετε περισσότερα από ένα αρχείο σε έναν φάκελο κρατήστε πατημένο το <key>Ctrl</key> καθώς επιλέγετε κάθε αρχείο.</p>
    </item>
    <item>
      <p>Ο κάτοχος της συσκευής λήψης συνήθως πρέπει να πατήσει ένα πλήκτρο για την αποδοχή του αρχείου. Ο διάλογος <gui>Μεταφορά αρχείου Bluetooth</gui> θα εμφανίσει τη γραμμή προόδου. Κάντε κλικ στο <gui>Κλείσιμο</gui> όταν ολοκληρωθεί η μεταφορά.</p>
    </item>
  </steps>

</page>
