<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="printing-paperjam" xml:lang="ro">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>How you clear a paper jam will depend on the make and model of
    printer that you have.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Clearing a paper jam</title>

  <p>Sometimes printers incorrectly feed sheets of paper and get jammed.</p>

  <p>The manual for your printer will usually provide detailed instructions on
  how to clear paper jams. Usually, you will need to open one of the printer’s
  panels to find the jam inside and then firmly (but carefully!) pull the
  jammed paper out of the printer’s feeding mechanism.</p>

  <p>Once the jam has been cleared you may need to press the printer’s
  <em>resume</em> button to start printing again. With some printers, you may
  even need to turn the printer off and then on again, and then start the print
  job again.</p>

</page>
