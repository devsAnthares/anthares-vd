<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="ro">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Apropie zone ale ecranului pentru a-l vizualiza mai ușor.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Șerbănescu</mal:name>
      <mal:email>daniel [at] serbanescu [dot] dk</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Mărirea unei zone de ecran</title>

  <p>Mărirea ecranului este diferită de mărirea <link xref="a11y-font-size">dimensiunii textului</link>. Această funcționalitate se aseamănă cu deținerea unei lupe, ce vă permite să navigați pe ecran apropiid părți ale ecranului.</p>

  <steps>
    <item>
      <p>Deschideți vederea de ansamblu <gui xref="shell-introduction#activities">Activități</gui> și tastați <gui>Acces universal</gui>.</p>
    </item>
    <item>
      <p>Apăsați pe <gui>Acces universal</gui> pentru a deschide panoul.</p>
    </item>
    <item>
      <p>Apăsați pe <gui>Zoom</gui> în secțiunea <gui>Văz</gui>.</p>
    </item>
    <item>
      <p>Comutați <gui>Zoom</gui> la <gui>ON</gui> în zona din dreapta sus a ferestrei <gui>Opțiuni zoom</gui>.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Universal Access</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Acum puteți naviga pe suprafața ecranului. Mutând mausul către marginile ecranului, veți muta zona mărită în direcții diferite, fapt ce vă permite vizualizarea zonei dorite.</p>

  <note style="tip">
    <p>Puteți comuta rapid zoom-ul apăsând clic pe <link xref="a11y-icon">iconița de accesibilitate</link> de pe bara de sus selectând <gui>Zoom</gui>.</p>
  </note>

  <p>Puteți schimba factorul de apropiere, urmărirea mausului, și poziția vizualizării apropiate pe ecran. Ajustați aceste configurări în tabul <gui>Lupă</gui> al feresterei <gui>Opțiuni zoom</gui>.</p>

  <p>Puteți activa ținta pentru a facilita găsirea cursorului mausului sau touchpadului. Porniți ținta și ajustați lungimea, culoarea și grosimea în tabul <gui>Țintă</gui> al feresterei de configurări <gui>Zoom</gui>.</p>

  <p>Puteți comuta inversarea videoului sau <gui>Alb pe negru</gui>, și ajusta opțiunile de luminozitate, contrast și scară de gri pentru lupă. Combinația acestor opțiuni sunt folositoare pentru oamenii cu văz slab, orice grad de fotofobie, sau doar folosirea calculatorului în condiții cu lumină nefavorabilă. Selectați tabul <gui>Efecte de culoare</gui> în fereastra de configurări <gui>Zoom</gui> pentru a activa și schimba aceste opțiuni.</p>

</page>
