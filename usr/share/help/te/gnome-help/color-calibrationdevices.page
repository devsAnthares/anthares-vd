<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="te">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>మేము పెద్ద మొత్తంలో కాలిబరేషన్ పరికరాలకు తోడ్పాటునిస్తాము.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>ఏ రంగు కొలిచే పరికరాలు తోడ్పాటునీయబడును?</title>

  <p>రంగు పరికరాలకు తోడ్పాటునిచ్చుటకు గ్నోమ్ ArgyII రంగు నిర్వహణ వ్యవస్థపై ఆధారపడును. కింది ప్రదర్శన కొలిచే పరికరాలకు తోడ్పాటునీయబడును:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (స్పెక్టోమీటర్)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (స్పెక్టోమీటర్)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 or LT (colorimeter)</p></item>
    <item><p>X-Rite i1 Display Pro (colorimeter)</p></item>
    <item><p>X-Rite ColorMunki Design లేదా Photo (స్పెక్టోమీటర్)</p></item>
    <item><p>X-Rite ColorMunki Create (colorimeter)</p></item>
    <item><p>X-Rite ColorMunki Display (colorimeter)</p></item>
    <item><p>Pantone Huey (colorimeter)</p></item>
    <item><p>MonacoOPTIX (colorimeter)</p></item>
    <item><p>ColorVision Spyder 2 మరియు 3 (colorimeter)</p></item>
    <item><p>Colorimètre HCFR (colorimeter)</p></item>
  </list>

  <note style="tip">
   <p>లైనక్స్ నందు Pantone Huey అనునది చవకైన మరియు బాగా తోడ్పాటునిచ్చు హార్డువేర్.</p>
  </note>

  <p>
    Thanks to Argyll there’s also a number of spot and strip reading
    reflective spectrometers supported to help you calibrating and
    characterizing your printers:
  </p>

  <list>
    <item><p>X-Rite DTP20 “Pulse” (“swipe” type reflective spectrometer)</p></item>
    <item><p>X-Rite DTP22 డిజిటల్ స్వాచ్‌బుక్ (స్పాట్ రకం రిఫ్లెక్టివ్ స్పెక్టోమీటర్)</p></item>
    <item><p>X-Rite DTP41 (స్టాప్ మరియు స్ట్రిప్ రీడింగ్ రిఫ్లెక్టివ్ స్పెక్టోమీటర్)</p></item>
    <item><p>X-Rite DTP41T (స్పాట్ మరియు స్ట్రిప్ రీడింగ్ రిఫ్లెక్టివ్ స్పెక్టోమీటర్)</p></item>
    <item><p>X-Rite DTP51 (స్పాట్ రీడింగ్ రిఫ్లెక్టివ్ స్పెక్టోమీటర్)</p></item>
  </list>

</page>
