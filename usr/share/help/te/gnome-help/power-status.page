<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="te">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display the status of the battery and connected devices.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Check the battery status</title>

  <steps>

    <title>Display the status of the battery and connected devices</title>

    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel. The status of known devices
      is displayed.</p>
    </item>

  </steps>

    <p>If an internal battery is detected, the <gui>Battery</gui> section
    displays the status of one or more laptop batteries. The indicator bar
    shows the percent charged, as well as time until fully charged if plugged
    in, and time remaining when running on battery power.</p>

    <p>The <gui>Devices</gui> section displays the status of connected
    devices.</p>

</page>
