<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="te">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>ఎలర్ట్ శబ్ధం మ్రోగించినప్పుడు విజువల్ ఎలర్ట్స్ తెరను లేదా విండోను ఫ్లాష్ చేయుటకు చేతనంచేయి.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>ఎలర్ట్ శబ్ధముల కొరకు తెరను ఫ్లాష్ చేయి</title>

  <p>కొన్ని రకాల సందేశాలు మరియు ఘటనల కొరకు మీ కంప్యూటర్ సాధారణ ఎలర్ట్ శబ్ధం మ్రోగించును. ఈ శబ్ధాలను మీరు వినలేక పోతే, శబ్ధం మ్రోగినప్పుడు మీరు మొత్తం తెరనందు లేదా ప్రస్తుత విండోనందు విజువల్ ఫ్లాగ్ పొందవచ్చు.</p>

  <p>This can also be useful if you’re in an environment where you need your
  computer to be silent, such as in a library. See <link xref="sound-alert"/>
  to learn how to mute the alert sound, then enable visual alerts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Universal Access</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Visual Alerts</gui> in the <gui>Hearing</gui> section.</p>
    </item>
    <item>
      <p>Switch <gui>Visual Alerts</gui> to <gui>ON</gui>.</p>
    </item>
    <item>
      <p> Select whether you want the entire screen or just your current window
      title to flash.</p>
    </item>
  </steps>

  <note style="tip">
    <p>పై పట్టీ నందలి <gui>విజువల్ ఎలర్ట్స్</gui> ఎంపికచేసి <link xref="a11y-icon">ఏక్సెసబిలిటి ప్రతిమ</link> పై నొక్కి మీరు త్వరగా విజువల్ ఎలర్ట్స్ ఆన్ మరియు ఆఫ్ చేయవచ్చు.</p>
  </note>

</page>
