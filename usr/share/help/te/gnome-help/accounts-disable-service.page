<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="te">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>బహుళ సేవలను ఏక్సెస్ చేయుటకు కొన్ని ఆన్‌లైన్ ఖాతాలు వుపయోగించవచ్చు(క్యాలెండర్ మరియు ఈమెయిల్ వలె). అనువర్తనములచేత వుపయోగించబడు ఈ సేవలను మీరు నియంత్రించవచ్చు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>ఒక ఖాతా ఏ ఆన్‌లైన్ సేవలను ఏక్సెస్ చేయాలో నియంత్రించుము</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, contacts and chat. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not chat if you have a different online account that you use
  for chat.</p>

  <p>ప్రతి ఆన్‌లైన్ ఖాతా చేత అందించబడు కొన్ని సేవలను మీరు అచేతనం చేయవచ్చు:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Online Accounts</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Online Accounts</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>A list of services that are available with this account will be
      shown under <gui>Use for</gui>. See <link xref="accounts-which-application"/>
      to see which applications access which services.</p>
    </item>
    <item>
      <p>Switch off any of the services that you do not want to use.</p>
    </item>
  </steps>

  <p>Once a service has been disabled for an account, applications on your
  computer will not be able to use the account to connect to that service any
  more.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
