<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="file-selection" xml:lang="pl">
  <info>
    <link type="guide" xref="index#dialogs"/>
    <desc>Używanie opcji <cmd>--file-selection</cmd>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>
  <title>Okno wyboru pliku</title>
    <p>Użyj opcji <cmd>--file-selection</cmd>, aby utworzyć okno wyboru pliku. <app>Zenity</app> zwraca wybrane pliki lub katalogi na standardowym wyjściu. Domyślny tryb okna wyboru pliku to otwieranie.</p>
    <p>Okno wyboru pliku obsługuje te opcje:</p>

    <terms>

      <item>
        <title><cmd>--filename</cmd>=<var>nazwa-pliku</var></title>
	<p>Określa plik lub katalog zaznaczony w oknie wyboru pliku po jego wyświetleniu.</p>
      </item>

      <item>
	<title><cmd>--multiple</cmd></title>
	<p>Umożliwia wybieranie wielu plików w oknie wyboru pliku.</p>
      </item>

      <item>
	<title><cmd>--directory</cmd></title>
	<p>Umożliwia wybieranie tylko katalogów w oknie wyboru pliku.</p>
      </item>

      <item>
	<title><cmd>--save</cmd></title>
	<p>Ustawia tryb zapisywania okna wyboru pliku.</p>
      </item>

      <item>
	<title><cmd>--separator</cmd>=<var>separator</var></title>
	<p>Określa ciąg używany do rozdzielania zwróconej listy nazw plików.</p>
      </item>

    </terms>

    <p>Ten przykładowy skrypt pokazuje, jak utworzyć okno wyboru pliku:</p>

<code>
#!/bin/sh

FILE=`zenity --file-selection --title="Wybór pliku"`

case $? in
         0)
                echo "Wybrano plik „$FILE”.";;
         1)
                echo "Nie wybrano pliku.";;
        -1)
                echo "Wystąpił nieoczekiwany błąd.";;
esac
</code>

    <figure>
      <title>Przykład okna wyboru pliku</title>
      <desc>Przykład okna wyboru pliku <app>Zenity</app></desc>
      <media type="image" mime="image/png" src="figures/zenity-fileselection-screenshot.png"/>
    </figure>
</page>
