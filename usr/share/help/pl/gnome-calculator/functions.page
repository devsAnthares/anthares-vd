<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="function" xml:lang="pl">

    <info>
        <link type="guide" xref="index#equation"/>
    
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

	<title>Funkcje</title>
    
    <p>Funkcje mogą być używane przez wstawienie nazwy funkcji z jej parametrami. <app>Kalkulator</app> obsługuje także funkcje określone przez użytkownika. Jeśli parametr nie jest liczbą lub <link xref="variable">pojedynczą zmienną</link>, to wprowadź każdy parametr oddzielony średnikiem i użyj nawiasów wokół listy parametrów.</p>
    <example>
    <p>sin 30</p>
    <p>abs (5−9)</p>
    <p>fun (9;10)</p>
    </example>
    <p>Aby dodać nowy typ funkcji, wprowadź funkcję w formacie podanym poniżej. Nazwa funkcji i parametry mogę zawierać tylko wielkie lub małe znaki. Część po znaku @ jest opcjonalnym opisem funkcji.</p>
    <example>
    <p>ProsteOdsetki (kapitał; stopa; okres) = kapitał * stopa * okres @ Prosta formuła odsetek</p>
    </example>
    <p>Funkcje mogą być także dodawane i wstawiane za pomocą przycisku f(x).</p>
    <example>
    <p>Kliknij przycisk f(x). Wprowadź nazwę funkcji w polu, wybierz liczbę parametrów i kliknij przycisk z plusem obok niego. W wyświetlaczu kalkulatora wprowadź wyrażenie funkcji i naciśnij klawisz <key>Enter</key>.</p>
    </example>
    <example>
    <p>Kliknij przycisk f(x). Wybierz funkcję i ją kliknij.</p>
    </example>
    <p>Poniższe funkcje są określone.</p>
    <table>
    <tr>
    <td><p>abs</p></td>
    <td><p><link xref="absolute">Wartość bezwzględna</link></p></td>
    </tr>
    <tr>
    <td><p>ceil</p></td>
    <td><p>Sufit</p></td>
    </tr>
    <tr>
    <td><p>cos</p></td>
    <td><p><link xref="trigonometry">Cosinus</link></p></td>
    </tr>
    <tr>
    <td><p>cosh</p></td>
    <td><p><link xref="trigonometry">Cosinus hiperboliczny</link></p></td>
    </tr>
    <tr>
    <td><p>floor</p></td>
    <td><p>Podłoga</p></td>
    </tr>
    <tr>
    <td><p>frac</p></td>
    <td><p>Część ułamkowa</p></td>
    </tr>
    <tr>
    <td><p>int</p></td>
    <td><p>Część całkowita</p></td>
    </tr>
    <tr>
    <td><p>ln</p></td>
    <td><p><link xref="logarithm">Logarytm naturalny</link></p></td>
    </tr>
    <tr>
    <td><p>log</p></td>
    <td><p><link xref="logarithm">Logarytm</link></p></td>
    </tr>
    <tr>
    <td><p>nie</p></td>
    <td><p><link xref="boolean">Zmienna NIE Boole’a</link></p></td>
    </tr>
    <tr>
    <td><p>ones</p></td>
    <td><p>Uzupełnienie do jedności</p></td>
    </tr>
    <tr>
    <td><p>round</p></td>
    <td><p>Zaokrąglanie</p></td>
    </tr>
    <tr>
    <td><p>sgn</p></td>
    <td><p>Signum</p></td>
    </tr>
    <tr>
    <td><p>sin</p></td>
    <td><p><link xref="trigonometry">Sinus</link></p></td>
    </tr>
    <tr>
    <td><p>sinh</p></td>
    <td><p><link xref="trigonometry">Sinus hiperboliczny</link></p></td>
    </tr>
    <tr>
    <td><p>sqrt</p></td>
    <td><p><link xref="power">Pierwiastek kwadratowy</link></p></td>
    </tr>
    <tr>
    <td><p>tan</p></td>
    <td><p><link xref="trigonometry">Tangens</link></p></td>
    </tr>
    <tr>
    <td><p>tanh</p></td>
    <td><p><link xref="trigonometry">Tangens hiperboliczny</link></p></td>
    </tr>
    <tr>
    <td><p>twos</p></td>
    <td><p>Uzupełnienie do dwóch</p></td>
    </tr>
    </table>
</page>
