<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="pl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Otwieranie plików za pomocą programu, który nie jest domyślny dla tego typu. Można także zmienić domyślny program.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

<title>Otwieranie plików w innych programach</title>

  <p>Po podwójnym kliknięciu (lub kliknięciu środkowym przyciskiem myszy) pliku w menedżerze plików zostanie on otwarty w domyślnym programie dla tego typu plików. Można otworzyć go w innym programie, wyszukać programów w Internecie lub ustawić domyślny program dla wszystkich plików tego samego typu.</p>

  <p>Aby otworzyć plik w programie innym niż domyślny, kliknij plik prawym przyciskiem myszy i wybierz program na górze menu. Jeśli nie ma tam danego programu, to wybierz <gui>Otwórz za pomocą innego programu</gui>. Domyślnie menedżer plików wyświetla tylko programy, o których wiadomo, że mogą obsłużyć ten plik. Aby przejrzeć wszystkie programy na komputerze, kliknij przycisk <gui>Wyświetl wszystkie programy</gui>.</p>

<p>Jeśli nadal nie można znaleźć danego programu, to można wyszukać więcej programów klikając przycisk <gui>Znajdź nowe programy</gui>. Menedżer plików wyszuka w Internecie pakiety zawierające programy, które mogą obsługiwać pliki tego typu.</p>

<section id="default">
  <title>Zmiana domyślnego programu</title>
  <p>Można zmienić domyślny program używany do otwierania plików danego typu. Umożliwia to otwieranie preferowanego programu po podwójnym kliknięciu pliku. Na przykład, można ustawić otwieranie ulubionego odtwarzacza muzyki po podwójnym kliknięciu pliku MP3.</p>

  <steps>
    <item><p>Wybierz plik typu, dla którego zmienić domyślny program. Na przykład, aby zmienić program używany do otwierania plików MP3, wybierz plik <file>.mp3</file>.</p></item>
    <item><p>Kliknij plik prawym przyciskiem myszy i wybierz <gui>Właściwości</gui>.</p></item>
    <item><p>Wybierz kartę <gui>Otwieranie</gui>.</p></item>
    <item><p>Wybierz program i kliknij przycisk <gui>Ustaw jako domyślny</gui>.</p>
    <p>Jeśli sekcja <gui>Inne programy</gui> zawiera program, którego chcesz czasem używać, ale nie ma być ustawiony jako domyślny, to wybierz go i kliknij przycisk <gui>Dodaj</gui>. Spowoduje to dodanie go to sekcji <gui>Zalecane programy</gui>. Będzie wtedy można używać tego programu przez kliknięcie pliku prawym przyciskiem myszy i wybranie go z listy.</p></item>
  </steps>

  <p>Powoduje to zmianę domyślnego programu nie tylko dla wybranego pliku, ale dla wszystkich plików tego samego typu.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
