<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="pl">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Automatyczne uruchamianie programów dla płyt CD/DVD, aparatów, odtwarzaczy muzyki i innych urządzeń i nośników.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>Otwieranie programów dla urządzeń i płyt</title>

  <p>Po podłączeniu urządzenia albo włożeniu płyty lub karty SD może być automatycznie uruchamiany program. Na przykład, menedżer zdjęć może być uruchamiany po podłączeniu aparatu cyfrowego. Można także to wyłączyć, aby nic się nie działo po podłączeniu.</p>

  <p>Aby zdecydować, które programy mają być uruchamiane po podłączeniu różnych urządzeń:</p>

<steps>
  <item>
    <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Informacje</gui>.</p>
  </item>
  <item>
    <p>Kliknij <gui>Informacje</gui>, aby otworzyć panel.</p>
  </item>
  <item>
    <p>Kliknij <gui>Nośniki wymienne</gui>.</p>
  </item>
  <item>
    <p>Znajdź wybrane urządzenie lub typ multimediów, a następnie wybierz program lub działanie dla niego. Poniżej znajduje się opis różnych typów urządzeń i multimediów.</p>
    <p>Zamiast uruchamiania programu można ustawić wyświetlanie urządzenia w menedżerze plików za pomocą opcji <gui>Otwarcie katalogu</gui>. Wtedy użytkownik będzie pytany, co zrobić.</p>
  </item>
  <item>
    <p>Jeśli na liście nie ma urządzenia lub typu multimediów do zmiany (np. płyt Blu-ray lub czytników e-booków), to kliknij <gui>Inny nośnik…</gui>, aby wyświetlić bardziej szczegółową listę urządzeń. Wybierz typ urządzenia lub multimediów z rozwijanej listy <gui>Typ</gui> oraz program lub działanie z rozwijanej listy <gui>Działanie</gui>.</p>
  </item>
</steps>

  <note style="tip">
    <p>Aby żadne programy nie były automatycznie otwierane po podłączeniu czegokolwiek, zaznacz opcję <gui>Bez pytania lub uruchamiania programów po wsunięciu nośnika</gui> na dole okna <gui>Informacji</gui>.</p>
  </note>

<section id="files-types-of-devices">
  <title>Typy urządzeń i nośników</title>
<terms>
  <item>
    <title>Płyty dźwiękowe</title>
    <p>Wybierz ulubiony odtwarzacz muzyki lub program do zgrywania płyt CD do obsługi płyt CD-Audio. Jeśli używane są dźwiękowe płyty DVD (DVD-A), to wybierz sposób ich obsługi w menu <gui>Inny nośnik…</gui>. Po otwarciu płyty dźwiękowej w menedżerze plików ścieżki będą wyświetlane jako pliki WAV, które można odtworzyć w każdym odtwarzaczu muzyki.</p>
  </item>
  <item>
    <title>Płyty z filmami</title>
    <p>Wybierz ulubiony odtwarzacz filmów do obsługi płyt DVD-Video. Użyj przycisku <gui>Inny nośnik…</gui>, aby ustawić program dla płyt Blu-ray, HD DVD, Video CD (VCD) i Super Video CD (SVCD). <link xref="video-dvd"/> zawiera przydatne informacje, jeśli płyty DVD lub inne płyty z filmami nie działają poprawnie.</p>
  </item>
  <item>
    <title>Puste płyty</title>
    <p>Użyj przycisku <gui>Inny nośnik…</gui>, aby wybrać program do nagrywania płyt dla pustych płyt CD, DVD, Blu-ray i HD DVD.</p>
  </item>
  <item>
    <title>Aparaty i zdjęcia</title>
    <p>Użyj rozwijanej listy <gui>Zdjęcia</gui>, aby wybrać program do zarządzania zdjęciami uruchamiany po podłączeniu aparatu cyfrowego lub włożeniu karty CF, SD, MMC lub MS z aparatu. Można także po prostu przeglądać zdjęcia za pomocą menedżera plików.</p>
    <p>W menu <gui>Inny nośnik…</gui> można wybrać program do otwierania płyt Picture CD firmy Kodak, które czasami można dostać w zakładach fotograficznych. Są to zwykłe płyty CD z plikami JPEG w katalogu o nazwie <file>Pictures</file>.</p>
  </item>
  <item>
    <title>Odtwarzacze muzyki</title>
    <p>Wybierz program do zarządzania kolekcją muzyki na przenośnym odtwarzaczu lub samodzielne zarządzanie plikami za pomocą menedżera plików.</p>
    </item>
    <item>
      <title>Czytniki e-booków</title>
      <p>Użyj przycisku <gui>Inny nośnik…</gui>, aby wybrać program do zarządzania książkami na czytniku e-booków lub samodzielne zarządzanie plikami za pomocą menedżera plików.</p>
    </item>
    <item>
      <title>Oprogramowanie</title>
      <p>Niektóre płyty i nośniki wymienne zawierają oprogramowanie, które ma być uruchamiane automatycznie po włożeniu. Użyj opcji <gui>Oprogramowanie</gui>, aby zdecydować, co robić w takim przypadku. Zawsze przed uruchomieniem oprogramowania użytkownik jest pytany o potwierdzenie.</p>
      <note style="warning">
        <p>Nigdy nie uruchamiaj programów z nośnika, któremu nie ufasz.</p>
      </note>
   </item>
</terms>

</section>

</page>
