<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="pl">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Wskazówki, takie jak „nie wyładowuj akumulatora za bardzo”.</desc>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

<title>Jak wydłużyć czas działania na zasilaniu z akumulatora</title>

<p>Kiedy akumulatory laptopów się starzeją, ich zdolność przechowywania ładunku elektrycznego się pogarsza, a pojemność z czasem zmniejsza. Można skorzystać z kilku sposobów na przedłużenie ich żywotności, ale nie należy się spodziewać dużej różnicy.</p>

<list>
  <item>
    <p>Nie wyładowuj zupełnie akumulatora. Zawsze podłącz go do ładowania <em>zanim</em> poziom spadnie bardzo nisko, chociaż większość akumulatorów ma wbudowane zabezpieczenia zapobiegające zbyt dużemu wyładowaniu. Ładowanie tylko częściowo rozładowanego akumulatora jest wydajniejsze, ale ładowanie tylko odrobinę rozładowanego akumulatora jest dla niego gorsze.</p>
  </item>
  <item>
    <p>Wysoka temperatura ma szkodliwy wpływ na wydajność ładowania akumulatora. Nie pozwalaj akumulatorowi na rozgrzanie się bardziej, niż potrzeba.</p>
  </item>
  <item>
    <p>Akumulatory starzeją się nawet, kiedy są odłączone od laptopa. Kupowanie zastępczego akumulatora w tym samym czasie, co oryginalny nie daje dużo korzyści — zawsze kupuj zamienniki wtedy, kiedy są potrzebne.</p>
  </item>
</list>

<note>
  <p>Ta porada dotyczy tylko akumulatorów litowo-jonowych (Li-Ion), które są najczęściej stosowane. Inne rodzaje akumulatorów mogą wymagać innego zachowania.</p>
</note>

</page>
