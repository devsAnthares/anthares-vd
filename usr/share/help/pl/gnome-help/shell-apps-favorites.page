<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="pl">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dodawanie (i usuwanie) często używanych programów na pasku ulubionych.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Przypinanie ulubionych programów do paska bocznego</title>

  <p>Aby dodać program do <link xref="shell-introduction#activities">paska ulubionych</link>:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> klikając <gui>Podgląd</gui> w górnym lewym rogu ekranu.</p>
      <p if:test="platform:gnome-classic">Kliknij menu <gui xref="shell-introduction#activities">Programy</gui> w górnym lewym rogu ekranu i wybierz element <gui>Ekran podglądu</gui> z menu.</p></item>
    <item>
      <p>Kliknij ostatni przycisk na pasku ulubionych i znajdź program do dodania.</p>
    </item>
    <item>
      <p>Kliknij ikonę programu prawym przyciskiem myszy i wybierz <gui>Dodaj do ulubionych</gui>.</p>
      <p>Można także kliknąć i przeciągnąć ikonę na pasek ulubionych.</p>
    </item>
  </steps>

  <p>Aby usunąć ikonę programu z paska ulubionych, kliknij ją prawym przyciskiem myszy i wybierz <gui>Usuń z ulubionych</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Ulubione programy są także wyświetlane w sekcji <gui>Ulubione</gui> menu <gui xref="shell-introduction#activities">Programy</gui>.</p>
  </note>

</page>
