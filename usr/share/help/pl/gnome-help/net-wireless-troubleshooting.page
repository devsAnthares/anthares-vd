<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="pl">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Współtwórcy wiki dokumentacji Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identyfikowanie i naprawianie problemów z połączeniami bezprzewodowymi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Rozwiązywanie problemów z siecią bezprzewodową</title>

  <p>To podręcznik rozwiązywania błędów pomagający zidentyfikować i naprawić problemy z siecią bezprzewodową. Jeśli z jakiegoś powodu nie można połączyć się z siecią bezprzewodową, to spróbuj skorzystać z zawartych tu wskazówek.</p>

  <p>Aby połączyć komputer z Internetem, przeprowadzimy te kroki:</p>

  <list style="numbered compact">
    <item>
      <p>Sprawdzanie połączenia</p>
    </item>
    <item>
      <p>Zbieranie informacji o sprzęcie</p>
    </item>
    <item>
      <p>Sprawdzanie sprzętu</p>
    </item>
    <item>
      <p>Próba utworzenia połączenia z routerem</p>
    </item>
    <item>
      <p>Sprawdzanie modemu i routera</p>
    </item>
  </list>

  <p>Aby rozpocząć, kliknij przycisk <em>Następna</em> w górnym prawym rogu strony. Ten przycisk, i podobne na kolejnych stronach, przeprowadzą użytkownika przez każdy krok podręcznika.</p>

  <note>
    <title>Używanie wiersza poleceń</title>
    <p>Niektóre wskazówki w tym podręczniku proszą o wpisanie poleceń w <em>wierszu poleceń</em> (terminalu). Program <app>Terminal</app> można znaleźć na <gui>ekranie podglądu</gui>.</p>
    <p>Jeśli nie używano wcześniej wiersza poleceń, nie ma obaw: ten podręcznik poprowadzi użytkownika przez każdy krok. Należy tylko pamiętać, że wielkość znaków poleceń ma znaczenie (więc należy wpisywać je <em>dokładnie</em> tak, jak są tu napisane), i po wpisaniu polecenia należy nacisnąć klawisz <key>Enter</key>, aby je wykonać.</p>
  </note>

</page>
