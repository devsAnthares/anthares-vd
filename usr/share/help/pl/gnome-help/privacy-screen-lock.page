<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="pl">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uniemożliwianie innym osobom używanie komputera, kiedy użytkownik jest nieobecny.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Automatyczne blokowanie ekranu</title>
  
  <p>Przed odejściem od komputera należy <link xref="shell-exit#lock-screen">zablokować ekran</link>, aby uniemożliwić innym osobom używanie komputera i plików użytkownika. Jeśli trudno pamiętać o blokowaniu ekranu, to można ustawić czas, po jakim ekran komputera jest blokowany automatycznie. Pomaga to zabezpieczyć komputer, kiedy nie jest używany.</p>

  <note><p>Kiedy ekran jest zablokowany, programy i procesy systemu nadal działają, ale trzeba wpisać hasło, aby znowu móc ich używać.</p></note>
  
  <steps>
    <title>Aby ustawić czas przed automatycznym zablokowaniem ekranu:</title>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Prywatność</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Prywatność</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Kliknij <gui>Blokada ekranu</gui>.</p>
    </item>
    <item>
      <p>Upewnij się, że <gui>Automatyczne blokowanie ekranu</gui> jest ustawione na <gui>|</gui> (włączone), a następnie wybierz czas z rozwijanej listy.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Programy mogą wyświetlać powiadomienia także na ekranie blokady. Jest to wygodne, na przykład, aby zobaczyć, czy jest nowa poczta bez odblokowywania ekranu. W razie obaw, że inne osoby mogą widzieć te powiadomienia, wyłącz <gui>Wyświetlanie powiadomień</gui>.</p>
  </note>

  <p>Kiedy ekran jest zablokowany i chcesz go odblokować, naciśnij klawisz <key>Esc</key> lub przeciągnij myszą z dołu do góry ekranu. Następnie wpisz hasło i naciśnij klawisz <key>Enter</key> lub kliknij przycisk <gui>Odblokuj</gui>. Można także od razu zacząć pisać hasło, a kurtyna blokady zostanie automatycznie podniesiona podczas pisania.</p>

</page>
