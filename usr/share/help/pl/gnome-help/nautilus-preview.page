<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="pl">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sterowanie wyświetlaniem miniatur plików.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

<title>Preferencje podglądu menedżera plików</title>

<p>Menedżer plików tworzy miniatury, aby móc wyświetlać podgląd obrazów, filmów i plików tekstowych. Tworzenie miniatur może być wolne w przypadku dużych plików lub przez sieć, więc można zmienić, kiedy są tworzone. Kliknij <gui>Pliki</gui> na górnym pasku, wybierz <gui>Preferencje</gui> i kartę <gui>Podgląd</gui>.</p>

<terms>
  <item>
    <title><gui>Pliki</gui></title>
    <p>Domyślnie wszystkie miniatury są tworzone tylko dla plików lokalnych, tych na komputerze i podłączonych dyskach zewnętrznych. Można ustawić tę funkcję na <gui>Zawsze</gui> lub <gui>Nigdy</gui>. Menedżer plików może <link xref="nautilus-connect">przeglądać pliki na innych komputerach</link> przez sieć lokalną lub Internet. Jeśli często przeglądane są pliki w sieci lokalnej lub sieć ma dużą wydajność, to można ustawić tworzenie miniatur na opcję <gui>Zawsze</gui>.</p>
    <p>Oprócz tego można użyć ustawienia <gui>Tylko dla plików mniejszych niż</gui>, aby ograniczyć rozmiar plików, dla których tworzone są miniatury.</p>
  </item>
  <item>
    <title><gui>Katalogi</gui></title>
    <p>Jeśli wyświetlane są rozmiary plików w <link xref="nautilus-list">kolumnach widoku listy</link> lub <link xref="nautilus-display#icon-captions">podpisach ikon</link>, to katalogi będą wyświetlane z liczbą plików i katalogów w nich zawartych. Obliczanie liczby elementów w katalogu może być wolne, zwłaszcza w przypadku bardzo dużych katalogów lub przez sieć. Można włączyć lub wyłączyć tę funkcję, albo włączyć ją tylko dla plików na komputerze i lokalnych dyskach zewnętrznych.</p>
  </item>
</terms>
</page>
