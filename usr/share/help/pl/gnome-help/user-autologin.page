<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Konfiguracja automatycznego logowania po włączeniu komputera.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Automatyczne logowanie</title>

  <p>Można zmienić ustawienia tak, aby użytkownik był automatycznie logowany po włączeniu komputera:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Zaznacz konto użytkownika, które ma być automatycznie logowane.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Odblokuj</gui> i wpisz swoje hasło.</p>
    </item>
    <item>
      <p>Przełącz <gui>Logowanie automatyczne</gui> na <gui>|</gui> (włączone).</p>
    </item>
  </steps>

  <p>Po następnym włączeniu komputera użytkownik zostanie automatycznie zalogowany. Jeśli włączono tę opcję, to nie będzie trzeba wpisywać hasła, aby się zalogować, co oznacza, że jeśli ktoś inny włączy komputer, to będzie miał dostęp do konta i prywatnych danych, w tym plików i historii przeglądarki.</p>

  <note>
    <p>Jeśli konto jest typu <em>Standardowe</em>, to nie można zmienić tego ustawienia. Skontaktuj się z administratorem komputera, który może zmienić to ustawienie za użytkownika.</p>
  </note>

</page>
