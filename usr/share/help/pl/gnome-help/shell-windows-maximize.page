<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="pl">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Podwójne kliknięcie lub przeciągnięcie paska tytułu, aby zmaksymalizować lub przywrócić okno.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Maksymalizowanie i cofanie maksymalizacji okna</title>

  <p>Można zmaksymalizować okno, aby zajmowało całe miejsc na pulpicie i cofnąć maksymalizację, aby przywrócić je do zwykłego rozmiaru. Można także zmaksymalizować okna pionowo po lewej i prawej stronie ekranu, aby wyświetlać dwa okna jednocześnie. <link xref="shell-windows-tiled"/> zawiera więcej informacji.</p>

  <p>Aby zmaksymalizować okno, chwyć pasek tytułu i przeciągnij go do góry ekranu lub kliknij go podwójnie. Aby zmaksymalizować okno za pomocą klawiatury, przytrzymaj klawisz <key xref="keyboard-key-super">Super</key> i naciśnij klawisz <key>↑</key>, albo naciśnij klawisze <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Można także zmaksymalizować okno klikając przycisk maksymalizacji na pasku tytułu.</p>

  <p>Aby przywrócić okno do stanu sprzed maksymalizacji, przeciągnij je od krawędzi ekranu. Jeśli okno jest całkowicie zmaksymalizowane, to można podwójnie kliknąć pasek tytułu, aby je przywrócić. Można także użyć tych samych skrótów klawiszowych, których użyto do maksymalizacji.</p>

  <note style="tip">
    <p>Przytrzymaj klawisz <key>Super</key> i przeciągnij okno w dowolnym miejscu, aby je przenieść.</p>
  </note>

</page>
