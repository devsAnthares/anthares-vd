<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="clock-calendar" xml:lang="pl">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wyświetlanie spotkań w kalendarzu na górze ekranu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

 <title>Spotkania w kalendarzu</title>

  <note>
    <p>Wymaga to użycia kalendarza <app>Evolution</app> lub ustawienia konta online obsługiwanego przez <gui>Kalendarz</gui>.</p>
    <p>Większość dystrybucji domyślnie ma zainstalowany program <app>Evolution</app>. Jeśli używana dystrybucja go nie ma, to należy go zainstalować za pomocą menedżera pakietów.</p>
 </note>

  <p>Aby wyświetlić spotkania:</p>
  <steps>
    <item>
      <p>Kliknij zegar na górnym pasku.</p>
    </item>
    <item>
      <p>Kliknij datę, dla której wyświetlić spotkania z kalendarza.</p>

    <note>
       <p>Mała kropka jest wyświetlana pod datami, które mają spotkania.</p>
    </note>

      <p>Istniejące spotkania są wyświetlane na lewo od kalendarza. Spotkania dodane w kalendarzu <app>Evolution</app> będą wyświetlane na liście spotkań zegara.</p>
    </item>
  </steps>

 <if:choose>
 <if:when test="!platform:gnome-classic">
 <media type="image" src="figures/shell-appts.png" width="500">
  <p>Zegar, kalendarz i spotkania</p>
 </media>
 </if:when>
 <if:when test="platform:gnome-classic">
 <media type="image" src="figures/shell-appts-classic.png" width="373" height="250">
  <p>Zegar, kalendarz i spotkania</p>
 </media>
 </if:when>
 </if:choose>

</page>
