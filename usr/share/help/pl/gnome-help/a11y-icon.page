<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-icon" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Menu ułatwień dostępu to ikona na górnym pasku wyglądająca jak człowiek.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Odnajdowanie menu ułatwień dostępu</title>

  <p><em>Menu ułatwień dostępu</em> to miejsce, w którym można włączać niektóre ustawienia ułatwień dostępu. Można je znaleźć klikając ikonę na górnym pasku wyglądającą jak człowiek w okręgu.</p>

  <figure>
    <desc>Menu ułatwień dostępu można znaleźć na górnym pasku.</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/universal-access-menu.png"/>
  </figure>

  <p>Jeśli menu ułatwień dostępu nie jest widoczne, to można je włączyć w panelu <gui>Ułatwienia dostępu</gui> ustawień:</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Zawsze widoczne menu ułatwień dostępu</gui> na <gui>|</gui> (włączone).</p>
    </item>
  </steps>

  <p>Aby użyć tego menu za pomocą klawiatury zamiast myszy, naciśnij klawisze <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq>, aby uaktywnić górny pasek. Pod przyciskiem <gui>Podgląd</gui> pojawi się biała linia — wskazuje ona na obecnie wybrany element. Użyj klawiszy strzałek na klawiaturze, aby przenieść białą linię pod ikonę menu ułatwień dostępu, a następnie naciśnij klawisz <key>Enter</key>, aby je otworzyć. Można używać klawiszy strzałek do góry i do dołu, aby wybierać elementy w menu. Naciśnij klawisz <key>Enter</key>, aby przełączyć wybrany element.</p>

</page>
