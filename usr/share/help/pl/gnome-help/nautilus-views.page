<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="pl">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Określanie domyślnej kolejności porządkowania i grupowania w menedżerze plików.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

<title>Preferencje widoków programu <app>Pliki</app></title>

<p>Można zmienić, jak pliki i katalogi są domyślnie grupowane i porządkowane. Wybierz <guiseq><gui style="menu">Pliki</gui> <gui style="menuitem">Preferencje</gui></guiseq> na górnym pasku, kiedy otwarty jest program <app>Pliki</app>, i wybierz kartę <gui style="tab">Widoki</gui>.</p>

<section id="default-view">
<title>Domyślny widok</title>
<terms>
  <item>
    <title><gui>Porządkowanie</gui></title>
    <p>Można zmienić domyślną kolejność porządkowania używaną w katalogach za pomocą rozwijanej listy <gui>Porządkowanie</gui> w preferencjach, aby porządkować według nazw, rozmiarów plików, ich typów, czasu ostatniej modyfikacji, czasu ostatniego otwarcia lub czasu przeniesienia do kosza.</p>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     view selector button that opens the view popover in the main window for     'View options'">Można zmienić jak <link xref="files-sort">pliki są porządkowane</link> w konkretnym katalogu klikając przycisk opcji widoku na pasku narzędziowym i wybierając <gui>Nazwa</gui>, <gui>Rozmiar</gui>, <gui>Typ</gui> lub <gui>Ostatnia modyfikacja</gui>, albo klikając nagłówki kolumn listy w widoku listy.</p>
  </item>
  <item>
    <title><gui>Katalogi przed plikami</gui></title>
    <p>Domyślnie menedżer plików nie wyświetla już wszystkich katalogów przed plikami. Aby to zmienić, włącz tę opcję.</p>
  </item>
</terms>
</section>

</page>
