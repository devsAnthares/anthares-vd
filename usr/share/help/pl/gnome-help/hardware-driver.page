<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="pl">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sterownik sprzętu/urządzenia umożliwia komputerowi używanie podłączonych do niego urządzeń.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

<title>Czym jest sterownik?</title>

<p>Urządzenia to fizyczne „części” komputera. Mogą być <em>zewnętrzne</em>, np. drukarki lub monitor, albo <em>wewnętrzne</em>, np. karty graficzne i dźwiękowe.</p>

<p>Aby komputer mógł używać tych urządzeń, musi wiedzieć, jak się z nimi komunikować. Robi to program o nazwie <em>sterownik urządzenia</em>.</p>

<p>Aby urządzenie działało po podłączeniu do komputera, musi być zainstalowany właściwy sterownik. Na przykład, nie będzie można używać drukarki, do której nie ma właściwego sterownika. Zwykle każdy model urządzenia używa sterownika niezgodnego z każdym innym modelem.</p>

<p>W systemie Linux sterowniki większości urządzeń są domyślnie zainstalowane, więc wszystko powinno działać po podłączeniu. Część sterowników może jednak być niedostępna lub trzeba je zainstalować ręcznie.</p>

<p>Oprócz tego, niektóre istniejące sterowniki są niepełne lub częściowo niefunkcjonalne. Na przykład, drukarka może nie móc drukować obustronnie, ale poza tym wszystko działa.</p>

</page>
