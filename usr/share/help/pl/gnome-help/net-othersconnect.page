<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="pl">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Można zapisać ustawienia (takie jak hasło) połączenia sieciowego, aby każdy użytkownik komputera mógł się z nim łączyć.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Inni użytkownicy nie mogą łączyć się z Internetem</title>

  <p>Po skonfigurowaniu połączenia sieciowego zwykle wszyscy pozostali użytkownicy na komputerze będą mogli go używać. Jeśli informacje o połączeniu nie są współdzielone, to należy sprawdzić jego ustawienia.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Sieć</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Sieć</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Wybierz <gui>Wi-Fi</gui> z listy po lewej.</p>
    </item>
    <item>
      <p>Kliknij przycisk <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">ustawienia</span></media>, aby otworzyć informacje o połączeniu.</p>
    </item>
    <item>
      <p>Wybierz <gui>Tożsamość</gui> z panelu po lewej.</p>
    </item>
    <item>
      <p>Na dole panelu <gui>Tożsamość</gui> zaznacz opcję <gui>Dostępne dla innych użytkowników</gui>, aby pozostali użytkownicy mogli używać połączenia sieciowego.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Zastosuj</gui>, aby zapisać zmiany.</p>
    </item>
  </steps>

  <p>Pozostali użytkownicy komputera będą teraz mogli używać tego połączenia bez wpisywania żadnych dodatkowych informacji.</p>

  <note>
    <p>Każdy użytkownik może zmienić to ustawienie.</p>
  </note>

</page>
