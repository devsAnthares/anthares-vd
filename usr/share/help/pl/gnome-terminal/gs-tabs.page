<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="gs-tabs" xml:lang="pl">

  <info>
    <link type="guide" xref="index#getting-started"/>
    <revision pkgversion="3.8" date="2013-02-17" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-07" status="candidate"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email its:translate="no">sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013-2014</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Włączanie, dodawanie, usuwanie i zmienianie kolejności kart <app>Terminala</app>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Używanie kart</title>

  <p>Pasek kart jest wyświetlany na górze okna <app>Terminala</app>, które ma otwartą więcej niż jedną kartę — wygląda on jak rząd przycisków. Kliknij kartę, aby na nią przełączyć. Można otworzyć wiele kart, aby wygodniej pracować w <app>Terminalu</app>. Umożliwia to wielozadaniowe uruchamianie programów, przeglądanie katalogów i modyfikowanie plików tekstowych w jednym oknie <app>Terminala</app>.</p>

  <section id="add-tab">
    <title>Otwieranie nowej karty</title>

    <p>Aby otworzyć nową kartę w obecnym oknie <app>Terminala</app>:</p>
    <steps>
      <item>
        <p>Naciśnij klawisze <keyseq><key>Shift</key><key>Ctrl</key><key>T</key></keyseq>.</p>
      </item>
    </steps>

  </section>

<!-- TODO: doesn't work, see bug 720693
  <section id="rename-tab">
    <title>Rename a tab</title>

    <p>Each tab has an automatically assigned title. You can rename the tabs
    individually:</p>

    <steps>
      <item>
        <p>Select <guiseq><gui style="menu">Terminal</gui>
        <gui style="menuitem">Set Title…</gui></guiseq>.</p>
      </item>
      <item>
        <p>Enter the desired <gui>Title</gui> that you wish to use for the tab.
        This will overwrite any titles that would be set by terminal commands.
        </p>
        <note>
          <p>It is not possible to set back the automatically set title once it
          has been set for a tab. To see the title, you need to allow terminal
          command titles to be shown in the <gui style="menuitem">Profile 
          Preferences</gui>.</p>
        </note>
      </item>
      <item>
        <p>Click <gui style="button">OK</gui>.</p>
      </item>
    </steps>
  </section>-->

  <section id="close-tab">
    <title>Zamykanie karty</title>

    <p>Aby zamknąć istniejącą kartę w obecnym oknie <app>Terminala</app>:</p>

    <steps>
      <item>
        <p>Wybierz <guiseq><gui style="menu">Plik</gui><gui style="menuitem">Zamknij kartę</gui></guiseq>.</p>
      </item>
    </steps>

    <p>Można także kliknąć przycisk <gui style="button">×</gui> w górnym prawym rogu karty lub kliknąć kartę prawym przyciskiem myszy i wybrać <gui style="menuitem">Zamknij terminal</gui>.</p>

  </section>

  <section id="reorder-tab">
    <title>Zmiana kolejności kart</title>

    <p>Aby zmienić kolejność kart w oknie:</p>
    <steps>
      <item>
        <p>Kliknij kartę i przytrzymaj lewy przycisk myszy.</p>
      </item>
      <item>
        <p>Przeciągnij kartę do wybranego miejsca między pozostałymi kartami.</p>
      </item>
      <item>
        <p>Zwolnij przycisk myszy.</p>
      </item>
    </steps>

    <p>Karta zostanie umieszczona w miejscu najbliższym do miejsca zwolnienia karty, zaraz obok pozostałych otwartych kart.</p>

    <p>Można także zmienić kolejność kart klikając kartę prawym przyciskiem myszy i wybierając <gui style="menuitem">Przesuń terminal w lewo</gui>, aby przenieść kartę w lewo lub <gui style="menuitem">Przesuń terminal w prawo</gui>, aby przenieść kartę w prawo. Przesunie to kartę o jedno miejsce.</p>

  </section>

  <section id="move-tab-another-window">
    <title>Przenoszenie karty do innego okna <app>Terminala</app></title>

    <p>Aby przenieść kartę z jednego okna do drugiego:</p>
    <steps>
      <item>
        <p>Kliknij kartę i przytrzymaj lewy przycisk myszy.</p>
      </item>
      <item>
        <p>Przeciągnij kartę do nowego okna.</p>
      </item>
      <item>
        <p>Umieść ją między pozostałymi kartami w nowym oknie.</p>
      </item>
      <item>
        <p>Zwolnij przycisk myszy.</p>
      </item>
    </steps>

    <note style="tip">
      <p>Można przenieść kartę z jednego okna do drugiego przeciągając ją do rogu <gui>Podgląd</gui> <gui>Powłoki GNOME</gui>. Wyświetli to wszystkie otwarte okna <app>Terminala</app>. Można zwolnić przytrzymywaną kartę nad wybranym oknem <app>Terminala</app>.</p>
    </note>
  </section>

  <section id="move-tab-create-window">
    <title>Przenoszenie karty, aby utworzyć nowe okno <app>Terminala</app></title>

    <p>Aby utworzyć nowe okno z istniejącej karty:</p>
    <steps>
      <item>
        <p>Kliknij kartę i przytrzymaj lewy przycisk myszy.</p>
      </item>
      <item>
        <p>Przeciągnij kartę z obecnego okna <app>Terminala</app>.</p>
      </item>
      <item>
        <p>Zwolnij przycisk myszy.</p>
      </item>
    </steps>
  </section>

</page>
