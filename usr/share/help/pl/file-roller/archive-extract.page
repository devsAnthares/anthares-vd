<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-extract" xml:lang="pl">

  <info>
    <link type="guide" xref="index#managing-archives"/>
    <link type="seealso" xref="archive-open"/>
    <link type="seealso" xref="password-protection"/>
    <link type="seealso" xref="archive-extract-advanced-options"/>
    <revision pkgversion="3.4" date="2011-12-12" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="review"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email its:translate="no">majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wypakowywanie plików i katalogów z archiwum.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2018</mal:years>
    </mal:credit>
  </info>

  <title>Rozpakowywanie archiwum</title>

  <p>Aby wypakować pliki z archiwum za pomocą <app>Menedżera archiwów</app>:</p>

  <steps>
    <item>
      <p><link xref="archive-open">Otwórz</link> archiwum.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui style="button">Rozpakuj</gui>.</p>
    </item>
    <item>
      <p>Wybierz miejsce rozpakowania archiwum w oknie wyboru plików. To będzie katalog docelowy. <link xref="archive-extract-advanced-options">Zaawansowane opcje rozpakowywania archiwów</link> umożliwiają więcej działań.</p>
    </item>
    <item>
      <p>Kliknij przycisk <gui>Rozpakuj</gui>.</p>
    </item>
    <item>
      <p>Jeśli archiwum jest chronione <link xref="password-protection">hasłem</link>, to <app>Menedżer archiwów</app> o nie poprosi. Wpisz hasło i kliknij przycisk <gui>OK</gui>.</p>
    </item>
    <item>
      <p>Otworzy się okno z paskiem postępu. Jeśli rozpakowywanie zakończy się pomyślnie, to można wybrać:</p>
      <list>
        <item>
          <p><gui>Zakończ</gui>, aby zamknąć <app>Menedżera archiwów</app>.</p>
        </item>
        <item>
          <p><gui>Wyświetl pliki</gui>, aby wyświetlić katalog docelowy w <app>Menedżerze plików</app>.</p>
        </item>
        <item>
          <p><gui>Zamknij</gui>, aby zamknąć to okno.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
