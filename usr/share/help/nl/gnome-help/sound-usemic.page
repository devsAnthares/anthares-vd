<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="nl">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Een analoge of USB-hoofdtelefoon gebruiken en een standaard invoerapparaat selecteren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Een andere microfoon gebruiken</title>

  <p>U kunt een externe microfoon gebruiken om met vrienden of collega's op het werk te praten, stemopnames te maken, of bij het gebruik van andere multimediatoepassingen. Zelfs als uw computer een ingebouwde microfoon of een webcam met een microfoon heeft, dan levert een aparte microfoon meestal toch een betere geluidskwaliteit op.</p>

  <p>Als uw microfoon een ronde stekker heeft, stop die dan in de daarvoor bestemde aansluiting op uw computer. De meeste computers hebben twee aansluitingen: één voor microfoons en één voor luidsprekers. Deze aansluiting is meestal lichtrood van kleur of heeft een afbeelding van een microfoon naast de aansluiting. Microfoons aangesloten op de daarvoor bestemde aansluiting zullen meestal standaard gebruikt worden. Als dat niet zo is, lees dan de instructies hieronder over het selecteren van een standaard invoerapparaat.</p>

  <p>Als u een USB-microfoon heeft, sluit die dan aan op één van de USB-poorten op uw computer. USB-microfoons worden behandeld als aparte audio-apparaten, u dient mogelijk op te geven welke microfoon standaard gebruikt moet worden.</p>

  <steps>
    <title>Standaard geluidsinvoerapparaat selecteren</title>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Geluid</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Geluid</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer in het tabblad <gui>Invoer</gui> het apparaat dat u wilt gebruiken. Als het goed is reageert de invoerniveau-indicator wanneer u spreekt.</p>
    </item>
  </steps>

  <p>Vanuit dit paneel kunt u het volume aanpassen en de microfoon uit zetten.</p>

</page>
