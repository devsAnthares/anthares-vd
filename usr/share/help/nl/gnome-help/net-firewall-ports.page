<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="reference" id="net-firewall-ports" xml:lang="nl">

  <info>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-firewall-on-off"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>U dient de juiste netwerkpoort op te geven om netwerktoegang voor een programma met uw firewall in- of uit te schakelen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Veelgebruikte netwerkpoorten</title>

  <p>Dit is een lijst met netwerkpoorten die veel gebruikt worden door toepassingen die netwerkdiensten aanbieden, zoals het delen van bestanden of bureaublad bekijken op afstand. U kunt uw firewall zo instellen dat de toegang tot deze toepassingen wordt <link xref="net-firewall-on-off">geblokkeerd of toegestaan</link>. Er worden duizenden poorten gebruikt, dus deze tabel is niet compleet.</p>

  <table shade="rows" frame="top">
    <thead>
      <tr>
	<td>
	  <p>Poort</p>
	</td>
	<td>
	  <p>Naam</p>
	</td>
	<td>
	  <p>Beschrijving</p>
	</td>
      </tr>
    </thead>
    <tbody>
      <tr>
	<td>
	  <p>5353/udp</p>
	</td>
	<td>
	  <p>mDNS, Avahi</p>
	</td>
	<td>
	  <p>Geeft systemen de mogelijkheid om elkaar te vinden, en beschrijft welke diensten zij aanbieden, zonder dat u zelf de informatie hoeft in te voeren.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/udp</p>
	</td>
	<td>
	  <p>Afdrukken</p>
	</td>
	<td>
	  <p>Hiermee kunt u printopdrachten naar een printer op het netwerk versturen.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/tcp</p>
	</td>
	<td>
	  <p>Afdrukken</p>
	</td>
	<td>
	  <p>Hiermee kunt u uw printer met andere mensen op het netwerk delen.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5298/tcp</p>
	</td>
	<td>
	  <p>Aanwezigheid</p>
	</td>
	<td>
	  <p>Hiermee wijzigt u uw chat-status, bedoeld voor andere mensen op het netwerk, zoals ‘online’ of ‘bezet’.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5900/tcp</p>
	</td>
	<td>
	  <p>Bureaublad op afstand</p>
	</td>
	<td>
	  <p>Hiermee kunt u uw bureaublad delen zodat andere mensen het kunnen bekijken of u op afstand kunnen helpen.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>3689/tcp</p>
	</td>
	<td>
	  <p>Muziek delen (DAAP)</p>
	</td>
	<td>
	  <p>Hiermee kunt u uw muziekcollectie met anderen op uw netwerk delen.</p>
	</td>
      </tr>
    </tbody>
  </table>

</page>
