<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="nl">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bij sommige online-accounts kunt u meerdere diensten gebruiken (zoals agenda en e-mail). U kunt zelf bepalen welke van deze diensten gebruikt kunnen worden door uw toepassingen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Bepaal tot welke online diensten een account toegang geeft</title>

  <p>Bij sommige soorten online-accountproviders heeft u toegang tot verschillende diensten met hetzelfde gebruikersaccount. Zo geeft een Google-account onder andere toegang tot agenda, e-mail, contacten en chatten. Het kan zijn dat u uw account wel voor sommige diensten, maar niet voor andere wilt gebruiken. U kunt bijvoorbeeld uw Google-account gebruiken voor e-mail maar niet voor chatten, als u een ander online-account gebruikt om te chatten.</p>

  <p>Voor elk online-account kunt u bepaalde diensten die geleverd worden uitschakelen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Online-accounts</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Online-accounts</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer uit de lijst aan de rechterkant het account dat u wilt wijzigen.</p>
    </item>
    <item>
      <p>Onder <gui>Gebruiken voor</gui> ziet u een lijst met diensten die beschikbaar zijn via dit account. Zie <link xref="accounts-which-application"/> om te lezen welke toepassingen toegang hebben tot welke diensten.</p>
    </item>
    <item>
      <p>Schakel de diensten uit die u niet wilt gebruiken.</p>
    </item>
  </steps>

  <p>Als een dienst eenmaal is uitgeschakeld voor een account, kunnen toepassingen op uw computer het account niet meer gebruiken om verbinding te maken met die dienst.</p>

  <p>Schakel een dienst die u eerder had uitgeschakeld weer in via het paneel <gui>Online-accounts</gui>.</p>

</page>
