<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="nl">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Night Light changes the color of your displays according to the time
    of day.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Adjust the color temperature of your screen</title>

  <p>A computer monitor emits blue light which contributes to sleeplessness and
  eye strain after dark. <gui>Night Light</gui> changes the color of your
  displays according to the time of day, making the color warmer in the
  evening. To enable <gui>Night Light</gui>:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Night Light</gui> to open the settings.</p>
    </item>
    <item>
      <p>Set the <gui>Night Light</gui> switch to <gui>ON</gui>. The screen
      color will follow the sunset and sunrise times for your location. Click
      the <gui>Manual</gui> button to set the times to a custom schedule.</p>
      <note>
        <p>The <link xref="shell-introduction">top bar</link> shows when
        <gui>Night Light</gui> is active. It can be temporarily disabled from
        the system menu.</p>
      </note>
    </item>
  </steps>

</page>
