<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-personal" xml:lang="nl">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.1" date="2013-09-23" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Geef anderen toegang tot uw bestanden in de map <file>Openbaar</file>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Uw persoonlijke bestanden delen</title>

  <p>U kunt een andere computer in het netwerk toestaan toegang te krijgen tot de map <file>Openbaar</file> in uw <file>Persoonlijke map</file>. Stel de voorkeuren voor <gui>Persoonlijke bestanden delen</gui> zo in dat anderen toegang krijgen tot de inhoud van de map.</p>

  <note style="info package">
    <p>Het <app>gnome-user-share</app>-pakket moet geïnstalleerd zijn voordat <gui>Persoonlijke bestanden delen</gui> zichtbaar is.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-user-share" style="button">gnome-user-share installeren</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Delen</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Delen</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Als <gui>Delen</gui> <gui>UIT</gui> staat, zet het dan <gui>AAN</gui>.</p>

      <note style="info"><p>Als u de tekst onder <gui>Computernaam</gui>kunt bewerken, dan kunt u de naam van uw computer zoals die getoond wordt in het netwerk <link xref="sharing-displayname">wijzigen</link></p></note>
    </item>
    <item>
      <p>Selecteer <gui>Persoonlijk bestand delen</gui>.</p>
    </item>
    <item>
      <p>Zet <gui>Delen van bestanden</gui> <gui>AAN</gui>. Dit betekent dat andere mensen verbinding mogen proberen te maken met uw computer en toegang krijgen tot uw map <file>Openbaar</file>.</p>
      <note style="info">
        <p>Er wordt een <em>URI</em> getoond waarmee andere computers in het netwerk toegang kunnen krijgen tot uw map <file>Openbaar</file>.</p>
      </note>
    </item>
  </steps>

  <section id="security">
  <title>Beveiliging</title>

  <terms>
    <item>
      <title>Wachtwoord vereisen</title>
      <p>U kunt degenen die toegang hebben tot uw map <file>Openbaar</file> vragen om een wachtwoord in te voeren , door <gui>Wachtwoord vereisen</gui> <gui>AAN</gui> te kiezen. Wanneer u deze optie niet gebruikt, kan iedereen proberen om uw map <file>Openbaar</file> te bekijken.</p>
      <note style="tip">
        <p>Deze optie is standaard uitgeschakeld, maar u kunt hem inschakelen en een veilig wachtwoord instellen.</p>
      </note>
    </item>
  </terms>
  </section>

  <section id="networks">
  <title>Netwerken</title>

  <p>De sectie <gui>Netwerken</gui> bevat een lijst met netwerken waarmee u verbonden bent. Gebruik de <gui>AAN | UIT</gui>-schakelaar naast elk ervan om te kiezen waar uw persoonlijke bestanden gedeeld kunnen worden.</p>

  </section>

</page>
