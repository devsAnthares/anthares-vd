<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="nl">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Weten wat uw IP-adres is kan helpen bij het oplossen van netwerkproblemen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Uw IP-adres achterhalen</title>

  <p>Weten wat uw IP-adres is kan u helpen bij het oplossen van problemen met uw internetverbinding. Het zal u misschien verbazen als u ziet dat u <em>twee</em> IP-adressen heeft: een IP-adres voor uw computer op het interne netwerk en een IP-adres voor uw computer op het internet.</p>

  <steps>
    <title>Uw interne (netwerk) IP-adres achterhalen:</title>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Netwerk</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Netwerk</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Kies de verbinding, <gui>Wifi</gui> of <gui>Bekabeld</gui>, in het linkerpaneel.</p>
      <p>Het IP-adres voor een bekabelde verbinding zal rechts getoond worden.</p>
      
      <p>Klik op de <media its:translate="no" type="image" src="figures/emblem-system.png"><span its:translate="yes">instellingen</span></media>-knop om uw IP-adres voor het draadloze netwerk te zien in het paneel <gui>Details</gui>.</p>
    </item>
  </steps>

  <steps>
  	<title>Uw externe (internet) IP-adres achterhalen:</title>
    <item>
      <p>Ga naar <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>De site zal uw externe IP-adres weergeven.</p>
    </item>
  </steps>

  <p>Afhankelijk van hoe uw computer verbinding maakt met het internet, kunnen deze adressen hetzelfde zijn.</p>

</page>
