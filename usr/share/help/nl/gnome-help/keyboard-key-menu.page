<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-menu" xml:lang="nl">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>
    <link type="seealso" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="new"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Juanjo Marin</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>De <key>Menu</key>-toets opent een contextmenu via het toetsenbord in plaats van een klik met de rechtermuisknop.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Wat is de <key>Menu</key>-toets?</title>

  <p>U vindt de <key>Menu</key>-toets, ook bekend als de <em>Toepassingen</em>-toets op sommige Windows-georiënteerde toetsenborden. Deze toets bevindt zich meestal rechtsonder op het toetsenbord, naast de <key>Ctrl</key>-toets, maar hij kan door toetsenbordfabrikanten op een andere locatie zijn geplaatst. Meestal staat er een afbeelding op van een cursor die boven een menu hangt: <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-menu.svg">
  <key>Menu</key> key icon</media>.</p>

  <p>De primaire functie van deze toets is het openen van een contextmenu via het toetsenbord in plaats van met een klik met de rechtermuisknop; dit is handig wanneer er geen muis of soortgelijk apparaat beschikbaar is, of wanneer er geen rechtermuisknop aanwezig is.</p>

  <p>Soms wordt de <key>Menu</key>-toets weggelaten omwille van de ruimte, met name op toetsenborden van draagbare en laptopcomputers. In dit geval bevindt zich op sommige toetsenborden een <key>Menu</key>-functietoets die geactiveerd kan worden in combinatie met de functietoets (<key>Fn</key>).</p>

  <p>Het <em>contextmenu</em> is een menu dat tevoorschijn komt wanneer u met rechts klikt. Het menu dat u ziet, als het er is, is afhankelijk van de context en functie van het gebied waarop u met rechts geklikt heeft. Wanneer u de <key>Menu</key>-toets gebruikt, dan wordt het contextmenu getoond voor het deel van het scherm waar uw cursor boven staat op het moment dat de toets wordt ingedrukt.</p>

</page>
