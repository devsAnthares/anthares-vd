<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="nl">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Maak een bestand onzichtbaar, zodat u het niet meer in bestandsbeheer ziet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Een bestand verbergen</title>

  <p>In de <app>Bestanden</app>-bestandsbeheerder kunt u naar wens bestanden verbergen of weer zichtbaar maken. Wanneer een bestand verborgen is, dan wordt het niet getoond in bestandsbeheer, maar het is nog wel aanwezig in de map.</p>

  <p>Om een bestand te verbergen <link xref="files-rename">hernoemt</link> u het zodat de naam met een <key>.</key> begint. Bijvoorbeeld, om het bestand met de naam <file>voorbeeld.txt</file> te verbergen moet u hernoemen naar <file>.voorbeeld.txt</file>.</p>

<note>
  <p>U kunt mappen op dezelfde manier verbergen als bestanden. Verberg een map door een <file>.</file> aan het begin van de mapnaam te plaatsen.</p>
</note>

<section id="show-hidden">
 <title>Alle verborgen bestanden weergeven</title>

  <p>Als u alle verborgen bestanden van een map wilt zien: ga naar die map, klik in de taakbalk op de knop Weergaveopties en kies <gui>Verborgen bestanden weergeven</gui>, of druk op <keyseq><key>Ctrl</key><key>H</key></keyseq>. U zult alle verborgen bestanden zien, samen met gewone bestanden die niet verborgen zijn.</p>

  <p>Om deze bestanden weer te verbergen: klik in de taakbalk op de knop Weergaveopties en kies <gui>Verborgen bestanden weergeven</gui>, of druk opnieuw op <keyseq><key>Ctrl</key><key>H</key></keyseq>.</p>

</section>

<section id="unhide">
 <title>Een bestand zichtbaar maken</title>

  <p>Om een bestand weer zichtbaar te maken: ga naar de map waarin zich het verborgen bestand bevindt, klik in de taakbalk op de knop Weergaveopties en kies <gui>Verborgen bestanden weergeven</gui>. Zoek vervolgens het verborgen bestand en hernoem het zodat het niet meer met een <key>.</key>begint. Bijvoorbeeld, om een bestand met de naam <file>.voorbeeld.txt</file> zichtbaar te maken, hernoemt u het naar <file>voorbeeld.txt</file>.</p>

  <p>Zodra u het bestand heeft hernoemd, kunt u in de taakbalk op de knop Weergaveopties klikken en <gui>Verborgen bestanden weergeven</gui> kiezen, of u drukt op <keyseq><key>Ctrl</key><key>H</key></keyseq> om de overige verborgen bestanden weer te verbergen.</p>

  <note><p>Normaal krijgt u verborgen bestanden in bestandsbeheer te zien totdat u bestandsbeheer sluit. Zie <link xref="nautilus-views"/> als u wilt weten hoe u de instellingen zo kunt wijzigen dat bestandsbeheer altijd verborgen bestanden toont.</p></note>

  <note><p>De meeste verborgen bestanden hebben een <file>.</file> aan het begin van hun naam. Andere kunnen daarentegen een <file>~</file> hebben aan het einde van hun naam. Dit zijn reservekopiebestanden. Zie <link xref="files-tilde"/> voor meer informatie hierover.</p></note>

</section>

</page>
