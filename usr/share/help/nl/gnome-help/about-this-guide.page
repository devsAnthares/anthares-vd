<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="about-this-guide" xml:lang="nl">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Enkele tips bij het gebruik van de bureaublad-handleiding.</desc>
    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

<title>Over deze handleiding</title>
<p>Deze handleiding leidt u rond door de functies van uw bureaublad, geeft antwoord op uw computer-gerelateerde vragen en geeft tips over hoe u uw computer doeltreffend kunt gebruiken. We hebben geprobeerd deze handleiding zo gemakkelijk mogelijk te maken:</p>

<list>
  <item><p>Deze handleiding is verdeeld in kleine taakgerichte onderwerpen  —  niet in hoofdstukken. Dit betekent dat u niet de hele handleiding hoeft door te spitten om antwoorden op uw vragen te krijgen.</p></item>
  <item><p>Verwante onderwerpen zijn aan elkaar gekoppeld. ‘Zie ook’-koppelingen onderaan sommige pagina's leiden u naar verwante onderwerpen. Dit maakt het gemakkelijk om soortelijke onderwerpen te vinden die u kunnen helpen om een bepaalde taak uit te voeren.</p></item>
  <item><p>Het bevat een ingebouwde zoekfunctie. Het invoervak bovenaan deze handleiding fungeert als <em>zoekbalk</em>, en er zullen relevante resultaten verschijnen zodra u begint te typen.</p></item>
  <item><p>Deze handleiding wordt doorlopend verbeterd. Ook al hebben we geprobeerd om u een volledige verzameling nuttige informatie te bieden, we weten dat we hier nooit al uw vragen kunnen beantwoorden.  We blijven echter meer informatie toevoegen om deze handleiding nog nuttiger te maken.</p></item>
</list>

<p>Wij danken u dat u de tijd heeft genomen om deze handleiding te lezen.</p>

<p>-- Het Gnome-documentatieteam</p>
</page>
