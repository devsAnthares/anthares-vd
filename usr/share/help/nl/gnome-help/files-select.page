<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="nl">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Druk op <keyseq><key>Ctrl</key><key>S</key></keyseq> om meerdere bestanden met vergelijkbare namen te selecteren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Selecteer bestanden met een patroon</title>

  <p>U kunt bestanden in een map selecteren met gebruikmaking van een filter voor de bestandsnaam. Druk op <keyseq><key>Ctrl</key><key>S</key></keyseq> om het venster <gui>Items selecteren die overeenkomen</gui>  te krijgen. Typ een filter in van algemene delen van de bestandsnamen plus jokertekens. Er zijn twee jokertekens beschikbaar:</p>

  <list style="compact">
    <item><p><file>*</file> komt overeen met een willekeurig aantal tekens, dus ook nul tekens.</p></item>
    <item><p><file>?</file> komt overeen met exact één willekeurig teken.</p></item>
  </list>

  <p>Bijvoorbeeld:</p>

  <list>
    <item><p>Als u een OpenDocument-tekstbestand, een PDF-bestand en een afbeelding heeft die allemaal dezelfde basisnaam <file>Factuur</file> hebben, selecteer ze dan alle drie met het filter.</p>
    <example><p><file>Factuur.*</file></p></example></item>

    <item><p>Als u een aantal foto's heeft die als volgt genoemd zijn <file>Vakantie-001.jpg</file>, <file>Vakantie-002.jpg</file>, <file>Vakantie-003.jpg</file>; kunt u deze selecteren met het patroon</p>
    <example><p><file>Vakantie-???.jpg</file></p></example></item>

    <item><p>Als u foto's heeft, maar u heeft sommige daarvan bewerkt en <file>-bewerkt</file> toegevoegd aan het einde van de bestandsnaam van de foto's die u bewerkt heeft, selecteer de bewerkte foto's dan met</p>
    <example><p><file>Vakantie-???-bewerkt.jpg</file></p></example></item>
  </list>

</page>
