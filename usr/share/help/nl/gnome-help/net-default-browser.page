<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="nl">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Om de standaard webbrowser te wijzigen, ga naar <gui>Details</gui> in de <gui>Instellingen</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Wijzigen in welke webbrowser websites geopend moeten worden</title>

  <p>Wanneer u in een toepassing op een link naar een webpagina klikt, dan zal automatisch een webbrowser met die pagina worden geopend. Als u echter meer dan één browser geïnstalleerd heeft, dan kan het zijn dat de pagina niet in de webbrowser van uw voorkeur wordt geopend. U kunt hier iets aan doen door de standaard webbrowser te wijzigen:</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Details</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Details</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Kies <gui>Standaardtoepassingen</gui> in de lijst aan de linkerkant van het venster.</p>
    </item>
    <item>
      <p>Kies in welke webbrowser u links wilt openen door de <gui>Web</gui>-optie te wijzigen.</p>
    </item>
  </steps>

  <p>Het kan zijn dat, wanneer u een andere webbrowser kiest om te openen, u een bericht krijgt dat het niet langer de standaard webbrowser is. Als dat het geval is, klik dan op de knop <gui>Annuleren</gui> (of iets dergelijks) zodat het niet zal pogen zichzelf weer als de standaard webbrowser in te stellen.</p>

</page>
