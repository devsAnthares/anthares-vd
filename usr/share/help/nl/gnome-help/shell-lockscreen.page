<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Het decoratieve en functionele vergrendelde scherm geeft nuttige informatie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Het vergrendelingsscherm</title>

  <p>Het vergrendelingsscherm betekent dat u kunt zien wat er gebeurt terwijl uw computer vergrendeld is, en u kunt een samenvatting krijgen van wat er gebeurd is terwijl u afwezig was. Het vergrendelingsschermgordijn laat een fraaie afbeelding op het scherm zien wanneer uw computer vergrendeld is en geeft nuttige informatie:</p>

  <list>
    <item><p>de naam van de aangemelde gebruiker</p></item>
    <item><p>datum en tijd, en bepaalde meldingen</p></item>
    <item><p>Batterij- en netwerkstatus</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>U kunt de computer ontgrendelen door het vergrendelde scherm gordijn met de cursor omhoog te halen, of door <key>Esc</key> of <key>Enter</key> in te drukken. Dit brengt het aanmeldscherm naar voren, waar u uw wachtwoord in kunt voeren om te ontgrendelen. U kunt ook beginnen met het intypen van uw wachtwoord waardoor het gordijn automatisch omhoog zal worden gehaald. U kunt ook van gebruiker wisselen als er meer gebruikers op de computer zijningesteld.</p>

</page>
