<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-add" xml:lang="nl">

  <info>
    <link type="guide" xref="accounts" group="add"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="draft"/>
    <revision pkgversion="3.9.92" date="2012-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Toestaan dat toepassingen toegang hebben tot uw accounts online voor foto's, contacten, agenda's en meer.</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>justin50@live.nl</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  </info>

  <title>Een account toevoegen</title>

  <p>Door het toevoegen van een account koppelt u uw online-accounts met uw Gnome-bureaublad. Zo worden uw e-mailprogramma, chatprogramma en andere gerelateerde toepassingen voor u ingesteld.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Online-accounts</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Online-accounts</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer uit de lijst aan de rechterkant een account.</p>
    </item>
    <item>
      <p>Selecteer het soort account dat u wilt toevoegen.</p>
    </item>
    <item>
      <p>Er wordt een klein websitescherm geopend waar u de gegevens van uw online-account kunt invoeren. Als u bijvoorbeeld een Google-account instelt, voer dan uw Google-gebruikersnaam en -wachtwoord in en meld u aan. Bij sommige providers kunt u een nieuw account aanmaken vanuit het dialoogvenster.</p>
    </item>
    <item>
      <p>Als u uw gegevens correct heeft ingevoerd, zult u worden gevraagd om Gnome toegang te geven tot uw online-account. Geef toegang om door te gaan.</p>
    </item>
    <item>
      <p>Alle diensten die worden aangeboden door een account-provider zullen standaard worden ingeschakeld. <link xref="accounts-disable-service">Schakel</link> individuele diensten <gui>UIT</gui> om ze uit te zetten.</p>
    </item>
  </steps>

  <p>Nadat u accounts heeft toegevoegd, kunnen toepassingen deze accounts gebruiken voor de diensten waarvoor u toestemming heeft gegeven. Zie <link xref="accounts-disable-service"/> om te weten te komen hoe u kunt bepalen welke diensten u wilt toestaan.</p>

  <note style="tip">
    <p>Veel online diensten verstrekken een autorisatiecode die door Gnome opgeslagen wordt in plaats van uw wachtwoord. Als u een account verwijdert, moet u ook dat certificaat intrekken in de online dienst. Zie <link xref="accounts-remove"/> voor meer informatie.</p>
  </note>

</page>
