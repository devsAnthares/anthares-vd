<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-lefthanded" xml:lang="he">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Reverse the left and right mouse buttons in the mouse settings.</desc>
  </info>

  <title>Use your mouse left-handed</title>

  <p>You can swap the behavior of the left and right buttons on your
  mouse or touchpad to make it more comfortable for left-handed use.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Devices</gui> in the sidebar.</p>
    </item>
    <item>
      <p>Click <gui>Mouse &amp; Touchpad</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> section, click to switch
      <gui>Primary button</gui> to <gui>Right</gui>.</p>
    </item>
  </steps>

  <note>
    <p>This setting will affect both your mouse and touchpad, as well as any
    other pointing device.</p>
  </note>

</page>
