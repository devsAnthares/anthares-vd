<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="sv">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vissa trådlösa enheter har problem med att hantera när datorn är i vänteläge och återstartar inte korrekt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Jag har inget trådlöst nätverk när jag väcker datorn</title>

  <p>Om du har försatt din dator i vänteläge kan du komma att upptäcka att din trådlösa internetanslutning inte fungerar när du startar den igen. Detta händer när <link xref="hardware-driver">drivrutinen</link> för den trådlösa enheten inte har fullständigt stöd för vissa strömsparsfunktioner. Typiskt kommer den trådlösa anslutningen att misslyckas att slås på när datorn återstartar.</p>

  <p>Om detta händer, prova att stänga av och slå på den trådlösa anslutningen igen:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Nätverk</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Nätverk</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj <gui>Trådlöst nätverk</gui>.</p>
    </item>
    <item>
      <p>Slå <gui>AV</gui> det trådlösa nätverket och slå sedan <gui>PÅ</gui> det igen.</p>
    </item>
    <item>
      <p>Om den trådlösa anslutningen fortfarande inte fungerar, slå <gui>PÅ</gui> <gui>Flygplansläge</gui> och slå sedan <gui>AV</gui> det igen.</p>
    </item>
  </steps>

  <p>Om detta inte fungerar så starta om din dator så borde den trådlösa anslutningen fungera igen. Om du fortfarande har problem efter det, anslut till internet via en nätverkskabel och uppdatera din dator.</p>

</page>
