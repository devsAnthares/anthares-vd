<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="sv">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Någon kontrollprogramvara för fläkten kan saknas, eller så är din dator varm.</desc>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Den bärbara datorns fläkt kör alltid</title>

<p>Om kylfläkten i din bärbara dator alltid kör kan det vara för att hårdvaran som kontrollerar kylsystemet i den bärbara datorn inte fungerar så bra i Linux. Vissa bärbara datorer behöver extra programvara för att kontrollera sina kylfläktar effektivt, men denna programvara kanske inte är installerad (eller tillgänglig under Linux överhuvudtaget) så fläktarna kör på full hastighet hela tiden.</p>

<p>Om detta är fallet kan du kanske ändra några inställningar eller installera extra programvara som tillåter full kontroll över fläkten. Till exempel kan <link href="http://vaio-utils.org/fan/">vaiofand</link> installeras för att kontrollera fläktarna på vissa bärbara Sony VAIO-datorer. Att installera denna programvara är en ganska teknisk process som i stor grad beror av din bärbara dators tillverkare och modell, så du kan behöva leta efter specifika råd för hur du gör det för din dator.</p>

<p>Det är också möjligt att din bärbara dator helt enkelt producerar mycket värme. Detta innebär inte nödvändigtvis att den håller på att överhettas; den kanske bara behöver köra fläktarna på högsta hastighet hela tiden för att bli kall nog. Om detta är fallet har du inte många alternativ än att låta fläkten köra på full hastighet hela tiden. Du kan ibland köpa extra kyltillbehör till din dator som kanske hjälper.</p>

</page>
