<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="sv">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Du kan ha låg signal eller så kanske nätverket inte låter dig ansluta ordentligt.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Varför kopplar mitt trådlösa nätverk ner hela tiden?</title>

<p>Du kan komma att upptäcka att du har blir frånkopplad från ett trådlöst nätverk även om du ville fortsätta vara ansluten. Din dator kommer normalt att försöka återansluta till nätverket igen så snart detta händer (nätverksikonen i systemraden kommer att visa tre punkter om den försöker återansluta), men det kan vara irriterande, speciellt om du använde internet just då.</p>

<section id="signal">
 <title>Svag trådlös signal</title>

 <p>En vanlig orsaka till att man blir frånkopplad från ett trådlöst nätverk är att du har låg signal. Trådlösa nätverk har en begränsad räckvidd, så om du är alltför långt borta från den trådlösa basstationen kanske du inte får tillräckligt stark signal för att upprätthålla en anslutning. Väggar och andra objekt mellan dig och basstationen kan också försvaga signalen.</p>

 <p>Nätverksikonen i systemraden visar hur stark din trådlösa signal är. Om signalen ser låg ut, prova att flytta närmare den trådlösa basstationen.</p>

</section>

<section id="network">
 <title>Nätverksanslutningen etablerades inte ordentligt</title>

 <p>Ibland när du ansluter till ett trådlöst nätverk kan det se ut som om du har lyckats ansluta först, men du blir frånkopplad strax efteråt. Detta händer vanligtvis för att din dator bara delvis lyckades ansluta till nätverket — den lyckades etablera en anslutning, men lyckades inte slutföra anslutningen av någon anledning och blev därför frånkopplad.</p>

 <p>Ett möjligt skäl till detta är att du skrivit in fel lösenordsfras för det trådlösa nätverket, eller att din dator inte tilläts använda nätverket (till exempel på grund av att nätverket kräver ett användarnamn för att logga in).</p>

</section>

<section id="hardware">
 <title>Otillförlitliga trådlös hårdvara/drivrutiner</title>

 <p>Viss trådlös nätverkshårdvara kan vara lite otillförlitlig. Trådlösa nätverk är komplicerade så trådlösa kort och basstationen stöter ibland på mindre problem och kan tapp anslutningar. Detta är irriterande men händer ganska regelbundet med många enheter. Om du blir frånkopplad från trådlösa nätverk ibland kan detta vara skälet. Om det händer regelbundet bör du överväga att skaffa annan hårdvara.</p>

</section>

<section id="busy">
 <title>Hektiska trådlösa nätverk</title>

 <p>Trådlösa nätverk på hektiska platser (exempelvis på universitet och caféer) har ofta många datorer som försöker anslut till dem samtidigt. Ibland kan dessa nätverk blir alltför upptagna och kanske inte kan hantera alla de datorer som försöker ansluta, så vissa blir nerkopplade.</p>

</section>

</page>
