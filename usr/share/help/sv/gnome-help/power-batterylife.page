<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="sv">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tips på hur du reducerar din dators strömförbrukning.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Använd mindre ström och förbättra batteridriftstiden</title>

  <p>Datorer kan använda mycket ström. Genom några enkla strömsparande strategier kan du reducera din elräkning och hjälpa miljön.</p>

<section id="general">
  <title>Allmänna tips</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Försätt datorn i vänteläge</link> när du inte använder den. Detta reducerar mängden ström den använder signifikant, och den kan väckas upp snabbt.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Stäng av</link> datorn när du inte kommer att använda den under längre period. Vissa människor oroar sig för att stänga av datorn regelbundet skulle orsaka att den slits snabbare, men detta är inte fallet.</p>
  </item>
  <item>
    <p>Använd <gui>Ström</gui>-panelen i <app>Inställningar</app> för att ändra dina ströminställningar. Det finns ett antal alternativ som kommer att hjälpa dig att spara ström: du kan <link xref="display-blank">automatiskt tömma skärmen</link> efter en viss tid, reducera <link xref="display-brightness">skärmens ljusstyrka</link> och få din dator att <link xref="power-autosuspend">automatiskt försättas i vänteläge</link> om du inte använt den under en viss tid.</p>
  </item>
  <item>
    <p>Stäng av externa enheter (som skrivare och bildläsare) när du inte använder dem.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Bärbara datorer, ultraportabla och andra enheter med batterier</title>

 <list>
   <item>
     <p>Reducera <link xref="display-brightness">skärmens ljusstyrka</link>. Strömförsörjningen för skärmen står för en signifikant del av en bärbar dators strömförbrukning.</p>
     <p>De flesta bärbara datorer har knappar på tangentbordet (eller ett kortkommando) som du kan använda för att reducera ljusstyrkan.</p>
   </item>
   <item>
     <p>Om du inte behöver en internetanslutning på ett litet tag, stäng av de trådlösa nätverks- eller Bluetooth-korten. Dessa enheter fungerar genom att sända ut radiovågor vilket kräver ganska mycket ström.</p>
     <p>Vissa datorer har en fysisk knapp som kan användas för att stänga av den, medan andra har en tangentbordsgenväg istället. Du kan starta den igen när du behöver den.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Fler avancerade tips</title>

 <list>
   <item>
     <p>Reducera antalet aktiviteter som kör i bakgrunden. Datorer använder mer ström när de har mycket arbete att göra.</p>
     <p>De flesta av dina körandes program gör väldigt lite när du inte använder dem aktivt. Program som ofta hämtar data från internet eller spelar musik eller film kan dock påverka din strömförbrukning.</p>
   </item>
 </list>

</section>

</page>
