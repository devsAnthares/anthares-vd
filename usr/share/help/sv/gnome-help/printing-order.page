<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sortera och omvänd utskriftsordningen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Skriv ut sidor i en annan ordning</title>

  <section id="reverse">
    <title>Omvänd</title>

    <p>Skrivare skriver vanligtvis ut första sidan först och sista sidan sist, så sidorna hamnar i omvänd ordning när du hämtar dem. Om det behövs kan du omvända denna utskriftsordning.</p>

    <steps>
      <title>För att omvända ordningen:</title>
      <item>
        <p>Tryck på <keyseq><key>Ctrl</key><key>P</key></keyseq> för att öppna utskriftsdialogen.</p>
      </item>
      <item>
        <p>I fliken <gui>Allmänt</gui>, under <gui>Kopior</gui>, kryssa i <gui>Omvänd</gui>. Den sista sidan kommer att skrivas ut först, och så vidare.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Sortering</title>

  <p>Om du skriver ut mer än en kopia av dokumentet, kommer utskrifterna att grupperas efter sidnummer som standard (det vill säga alla kopiorna av sida ett kommer ut, sedan kopiorna av sida två, och så vidare). <em>Sortering</em> kommer att få varje kopia att komma ut med dess sidor grupperade tillsammans i rätt ordning istället.</p>

  <steps>
    <title>För att sortera:</title>
    <item>
     <p>Tryck på <keyseq><key>Ctrl</key><key>P</key></keyseq> för att öppna utskriftsdialogen.</p>
    </item>
    <item>
      <p>I fliken <gui>Allmänt</gui>, under <gui>Kopior</gui>, kryssa i <gui>Sortera</gui>.</p>
    </item>
  </steps>

</section>

</page>
