<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="problem" id="bluetooth-problem-connecting" xml:lang="sv">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adaptern kan vara avstängd eller har kanske inte drivrutiner eller så är Bluetooth inaktiverat eller blockerat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Jag kan inte ansluta min Bluetooth-enhet</title>

  <p>Det finns ett flertal anledningar till varför du kanske inte kan ansluta till en Bluetooth-enhet, så som en telefon eller en hörlur.</p>

  <terms>
    <item>
      <title>Anslutning blockerad eller otillförlitlig</title>
      <p>Vissa Bluetooth-enheter blockerar anslutningar som standard eller kräver att du ändrar en inställning för att tillåta anslutningar. Försäkra att din enhet är inställd för att tillåta anslutningar.</p>
    </item>
    <item>
      <title>Bluetooth-hårdvara kändes inte igen</title>
      <p>Din Bluetooth-adapter kanske inte har känts igen av datorn. Detta skulle kunna vara för att <link xref="hardware-driver">drivrutiner</link> för adaptern inte installerats. Vissa Bluetooth-adaptrar saknar stöd under Linux, så du kan kanske inte få tag i rätt drivrutiner för dessa. Om det är fallet kommer du troligtvis att behöva skaffa en annan Bluetooth-adapter.</p>
    </item>
    <item>
      <title>Adaptern är inte påslagen</title>
        <p>Försäkra att din Bluetooth-adapter är påslagen. Öppna Bluetooth-panelen och kontrollera att den inte är <link xref="bluetooth-turn-on-off">inaktiverad</link>.</p>
    </item>
    <item>
      <title>Enhetens Bluetooth-anslutning är avslagen</title>
      <p>Kontrollera att Bluetooth är påslaget i enheten du försöker ansluta till och att den är <link xref="bluetooth-visibility">detekterbar eller synlig</link>. Om du till exempel försöker ansluta till en telefon, säkerställ att den inte är i flygplansläge.</p>
    </item>
    <item>
      <title>Ingen Bluetooth-adapter i din dator</title>
      <p>Många datorer har inte Bluetooth-adaptrar. Du kan köpa en adapter om du vill använda Bluetooth.</p>
    </item>
  </terms>

</page>
