<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-why-calibrate" xml:lang="sv">

  <info>
    <link type="guide" xref="color#calibration"/>
    <desc>Kalibrering är viktigt om du bryr dig om färgerna du visar eller skriver ut.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Varför måste jag själv kalibrera?</title>

  <p>Allmänna profiler är vanligtvis dåliga. När en tillverkare skapar en ny modell så tar de bara ett par produkter från produktionslinjen och gör ett genomsnitt:</p>

  <media its:translate="no" type="image" src="figures/color-average.png">
    <p its:translate="yes">Genomsnittliga profiler</p>
  </media>

  <p>Skärmpaneler skiljer sig ganska mycket från enhet till enhet, och förändras betydligt med åldern. Det är också svårare för skrivare, eftersom bara ett pappersbyte - papperstyp eller vikt - kan göra att karakteriseringstillståndet ändras och profilen ger fel färger.</p>

  <p>Det bästa sättet att se till att din profil är korrekt är att utföra kalibreringen själv, eller genom att låta ett utomstående företag skapa en profil åt dig baserat på just ditt karakteriseringstillstånd.</p>

</page>
