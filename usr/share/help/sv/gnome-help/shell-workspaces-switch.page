<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-switch" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Använd arbetsyteväxlaren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

 <title>Växla mellan arbetsytor</title>

 <steps>  
 <title>Via musen:</title>
 <item>
    <p if:test="!platform:gnome-classic">Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>.</p>
  <p if:test="platform:gnome-classic">Längst upp till vänster på skärmen, klicka på menyn <gui xref="shell-introduction#activities">Program</gui> och välj <gui>Aktivitetsöversikt</gui>.</p>
 </item>
 <item>
  <p>Klicka på en arbetsyta i <link xref="shell-workspaces">arbetsyteväxlaren</link> på höger sida av skärmen för att visa de öppna fönstren på den arbetsytan.</p>
 </item>
 <item>
  <p>Klicka på vilken fönsterminiatyrbild som helst för att aktivera den arbetsytan.</p>
 </item>
 </steps>
 
  <p if:test="platform:gnome-classic">Alternativt kan du växla mellan arbetsytor genom att klicka på arbetsytsnamnet på höger sida av fönsterlisten längst ner på bottenraden och välja den arbetsyta du önskar från menyn.</p>

 <list>
 <title>Via tangentbordet:</title>  
  <item>
    <p>Tryck <keyseq><key xref="keyboard-key-super">Super</key><key>Page Up</key></keyseq> eller <keyseq><key>Ctrl</key><key>Alt</key><key>Upp</key></keyseq> för att flytta till arbetsytan som visas ovanför den aktuella arbetsytan i arbetsyteväxlaren.</p>
  </item>
  <item>
    <p>Tryck <keyseq><key>Super</key><key>Page Down</key></keyseq> eller <keyseq><key>Ctrl</key><key>Alt</key><key>Ner</key></keyseq> för att flytta till arbetsytan som visas nedanför den aktuella arbetsytan i arbetsyteväxlaren.</p>
  </item>
 </list>

</page>
