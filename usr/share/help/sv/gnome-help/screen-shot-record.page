<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="sv">

  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.6.1" date="2012-11-10" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ta en bild eller spela in en video som visar vad som händer på skärmen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Skärmbilder och skärminspelningar</title>

  <p>Du kan ta en bild av din skärm (en <em>skärmbild</em>) eller spela in en video av vad som händer på skärmen (en <em>skärminspelning</em>). Detta är användbart om du vill visa någon hur något görs på datorn, till exempel. Skärmbilder och skärminspelningar är normala bild- och videofiler, så du kan e-posta dem och dela dem på nätet.</p>

<section id="screenshot">
  <title>Ta en skärmbild</title>

  <steps>
    <item>
      <p>Öppna <app>Skärmbild</app> från översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui>.</p>
    </item>
    <item>
      <p>I fönstret <app>Skärmbild</app>, välj huruvida hela skärmen, det aktuella fönstret eller ett område på skärmen ska avbildas. Sätt en fördröjning om du behöver välja ett fönster eller på annat sätt arrangera skrivbordet för skärmbilden. Välj sedan effekterna du vill ha.</p>
    </item>
    <item>
       <p>Klicka på <gui>Ta skärmbild</gui>.</p>
       <p>Om du valde <gui>Välj området att fånga</gui> kommer markören att ändras till ett hårkors. Klicka och dra ett område som du önskar avbilda.</p>
    </item>
    <item>
      <p>I fönstret <gui>Spara skärmbild</gui>, mata in ett filnamn och välj en mapp, klicka sedan på <gui>Spara</gui>.</p>
      <p>Alternativt, importera skärmbilden direkt i ett bildredigeringsprogram utan att spara det först. Klicka på <gui>Kopiera till urklipp</gui> och klistra sedan in bilden i det andra programmet eller dra miniatyrskärmbilden till programmet.</p>
    </item>
  </steps>

  <section id="keyboard-shortcuts">
    <title>Tangentbordsgenvägar</title>

    <p>Ta snabbt en skärmbild av skrivbordet, ett fönster eller ett område när som helst via dessa globala tangentbordsgenvägar:</p>

    <list style="compact">
      <item>
        <p><key>Prt Scrn</key> för att ta en skärmbild av skrivbordet.</p>
      </item>
      <item>
        <p>Tryck <keyseq><key>Alt</key><key>uppåtpil</key></keyseq> för att ta en skärmbild av ett fönster.</p>
      </item>
      <item>
        <p><keyseq><key>Skift</key><key>Prt Scrn</key></keyseq> för att ta en skärmbild av ett område som du väljer.</p>
      </item>
    </list>

    <p>När du använder en tangentbordsgenväg kommer bilden automatiskt att sparas i din mapp <file>Bilder</file> med ett filnamn som börjar med <file>Skärmbild</file> och inkluderar datum och tid då den togs.</p>
    <note style="note">
      <p>Om du inte har en mapp <file>Bilder</file>, kommer bilderna att sparas i din hemmapp istället.</p>
    </note>
    <p>Du kan också hålla ner <key>Ctrl</key> med någon av ovanstående genvägar för att kopiera skärmbilden till urklipp istället för att spara den.</p>
  </section>

</section>

<section id="screencast">
  <title>Gör en skärminspelning</title>

  <p>Du kan göra en videoinspelning av vad som händer på din skärm:</p>

  <steps>
    <item>
      <p>Tryck <keyseq><key>Ctrl</key><key>Alt</key><key>Skift</key><key>R</key></keyseq> för att börja spela in det som finns på din skärm.</p>
      <p>En röd cirkel visas i övre högra hörnet av skärmen när inspelningen pågår.</p>
    </item>
    <item>
      <p>När du är klar tryck <keyseq><key>Ctrl</key><key>Alt</key><key>Skift</key><key>R</key></keyseq> igen för att avsluta inspelningen.</p>
    </item>
    <item>
      <p>Videon sparas automatiskt i din mapp <file>Videor</file> med ett filnamn som börjar med <file>Skärminspelning</file> och inkluderar datum och tid när den spelades in.</p>
    </item>
  </steps>

  <note style="note">
    <p>Om du inte har en mapp <file>Videor</file>, kommer videorna att sparas i din hemmapp istället.</p>
  </note>

</section>

</page>
