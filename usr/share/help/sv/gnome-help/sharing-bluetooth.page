<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="sv">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Låt filer skickas till din dator över Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Styr delning över Bluetooth</title>

  <p>Du kan aktivera <gui>Bluetooth</gui>-delning för att ta emot filer över Bluetooth som sparas i mappen <file>Hämtningar</file></p>

  <steps>
    <title>Tillåt filer att delas till din mapp <file>Hämtningar</file></title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Bluetooth</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Säkerställ att <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui> är påslaget</link>.</p>
    </item>
    <item>
      <p>Bluetooth-aktiverade enheter kan skicka filer till din mapp <file>Hämtningar</file> endast när <gui>Bluetooth</gui>-panelen är öppen.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Du kan <link xref="sharing-displayname">ändra</link>namnet som din dator visar för andra enheter.</p>
  </note>

</page>
