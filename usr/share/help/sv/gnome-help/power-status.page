<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="sv">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Visa status för batteriet och anslutna enheter.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Kontrollera batteristatus</title>

  <steps>

    <title>Visa status för batteriet och anslutna enheter</title>

    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Ström</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ström</gui> för att öppna panelen. Status för kända enheter visas.</p>
    </item>

  </steps>

    <p>Om ett internt batteri upptäcks visar avsnittet <gui>Batteri</gui> status för en eller flera batterier för bärbar dator. Indikatorraden visar laddad procentsats, så väl som tiden till fullständig laddning om ansluten, och tid som kvarstår vid batteridrift.</p>

    <p>Avsnittet <gui>Enheter</gui> visar status för anslutna enheter.</p>

</page>
