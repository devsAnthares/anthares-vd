<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="a11y task" id="a11y-visualalert" xml:lang="sv">

  <info>
    <link type="guide" xref="a11y#sound"/>
    <link type="seealso" xref="sound-alert"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Aktivera visuella larm för att få skärmen eller fönstret att blinka när ett larmljud spelas.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Blinka skärmen vid larmljud</title>

  <p>Din dator kommer att spela ett enkelt larmljud för vissa typer av meddelande och händelser. Om du har svårt att höra dessa ljud kan du antingen låta hela skärmen eller det aktuella fönstret blinka när ett larmljud spelas.</p>

  <p>Detta kan också vara användbart om du är i en miljö där din dator måste vara tyst, som på ett bibliotek. Se <link xref="sound-alert"/> för att lära dig hur du tystar larmljuden, och aktivera sedan visuella larm.</p>

  <steps>
    <item>
      <p>Öppna <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Hjälpmedel</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Hjälpmedel</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Tryck på <gui>Visuella larm</gui> i avsnittet <gui>Höra</gui>.</p>
    </item>
    <item>
      <p>Slå om <gui>Visuella larm</gui> till <gui>PÅ</gui>.</p>
    </item>
    <item>
      <p>Välj om du vill att hela skärmen eller bara det aktuella fönstrets namnlist ska blinka.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Du kan också aktivera eller inaktivera visuella larm genom att klicka på <link xref="a11y-icon">hjälpmedelsikonen</link> i systemraden och välja <gui>Visuella larm</gui>.</p>
  </note>

</page>
