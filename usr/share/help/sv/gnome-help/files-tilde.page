<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="sv">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Detta är säkerhetskopior. De är dolda som standard.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Vad är en fil med en <file>~</file> i slutet på namnet?</title>

  <p>Filer med ett <file>~</file> i slutet på deras namn (till exempel <file>example.txt~</file>) är automatiskt skapade säkerhetskopior av dokument som redigerats i textredigeraren <app>gedit</app> eller andra program. Det är säkert att ta bort dem, men det är inte farligt att låta dem vara kvar på din dator.</p>

  <p>Dessa filer döljs som standard. Om du ser dem är det för att du antingen valt <gui>Visa dolda filer</gui> (i menyn visningsalternativ i verktygsraden <app>Filer</app>) eller tryckt på <keyseq><key>Ctrl</key><key>H</key></keyseq>. Du kan dölja dem igen genom att repetera endera av dessa steg.</p>

  <p>Dessa filer behandlas på samma sätt som normala dolda filer. Se <link xref="files-hidden"/> för råd om hur du hanterar dolda filer.</p>

</page>
