<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-ipodtransfer" xml:lang="sv">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Använd en mediaspelare för att kopiera låtarna och koppla säkert från iPod:en efteråt.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Låtar visas inte på min iPod när jag kopierar över dem</title>

<p>När du kopplar en iPod till din dator kommer den att visas i ditt musikspelarprogram och också i filhanteraren (programmet <app>Filer</app> i översiktsvyn <gui>Aktiviteter</gui>). Du måste kopiera låtar till din iPod via musikspelaren — om du kopierar dem via filhanteraren så kommer det inte att fungera eftersom låtarna inte kommer att hamna på rätt ställe. IPod:ar har en speciell plats för att spara låtar som musikspelarprogrammet känner till hur man når, men som filhanteraren inte känner till.</p>

<p>Du måste också vänta på att låtarna ska sluta kopieras till iPod:en innan du kopplar från den. Innan du kopplar från iPod:en, försäkra dig om att du valt att göra en <link xref="files-removedrive">säker borttagning av den</link>. Detta kommer att säkerställa att alla låtarna har kopierats korrekt.</p>

<p>Ytterligare ett skäl till varför låtar kanske inte syns på din iPod kan vara att musikspelarprogrammet du använder inte har stöd för att konvertera låtarna från ett ljudformat till ett annat. Om du kopierar en låt som är sparade i ett ljudformat som inte har stöd för din iPod (till exempel en Ogg Vorbis-fil (.oga)) kommer musikspelaren att försöka konvertera den till ett format som iPod:en förstår, exempelvis MP3. Om det lämpliga konverteringsprogrammet (också känt som kodek eller kodare) inte är installerat kommer musikspelaren inte att kunna konvertera och kommer därför inte att kopiera låten. Leta i programinstalleraren efter en lämplig kodek.</p>

</page>
