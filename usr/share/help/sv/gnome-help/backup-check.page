<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="sv">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifiera att din säkerhetskopiering lyckades.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Kontrollera din säkerhetskopia</title>

  <p>Efter att du har säkerhetskopierat dina filer, bör du försäkra dig om att säkerhetskopieringen lyckades. Om den inte fungerade korrekt så kan du förlora viktig data då några filer kan saknas från säkerhetskopian.</p>

   <p>När du använder <app>Filer</app> för att kopiera eller flytta filer, så kommer datorn att kontrollera att all data överfördes korrekt. Men om du överför data som är mycket viktig för dig så kanske du vill utföra ytterligare kontroller för att bekräfta att dina data har överförts korrekt.</p>

  <p>Du kan göra en extra kontroll genom att titta genom de kopierade filerna och mapparna på destinationsmediet. Genom att kontrollera att filerna och mapparna är de du överförde faktiskt finns i säkerhetskopian så kan du få större förtroende för att processen lyckades.</p>

  <note style="tip"><p>Om du upptäcker att du regelbundet tar säkerhetskopior av stora mängder data kan det vara enklare att använda ett särskilt program för säkerhetskopiering, så som <app>Déjà Dup</app>. Ett sådan program är mer kraftfullt och mer pålitligt än att bara kopiera och klistra in filer.</p></note>

</page>
