<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="sv">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Tips så som ”Låt inte batteriet bli för lågt”.</desc>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Få ut det mesta ur din bärbara dators batteri</title>

<p>Efterhand som batterier åldras blir de sämre på att spara laddning och deras kapacitet minskar gradvis. Det finns ett par tekniker som du kan använda för att förlänga deras användbara livslängd, även om du inte bör förvänta dig en stor skillnad.</p>

<list>
  <item>
    <p>Låt inte batteriet laddas ur helt. Ladda alltid <em>innan</em> batteriet blir väldigt lågt, även om de flesta batteri har inbyggda skyddsmekanismer för att förhindra att batteriet blir allt för lågt. Att ladda när det bara är delvis urladdat är mer effektivt, men att ladda när det bara är lite urladdat är värre för batteriet.</p>
  </item>
  <item>
    <p>Värme har en skadlig effekt på batteriets laddningseffektivitet. Låt inte batteriet blir varmare än nödvändigt.</p>
  </item>
  <item>
    <p>Batterier åldras även om du låter dem ligga. Det är bara en liten vinst att köpa ett ersättningsbatteri samtidigt som du köper originalbatteriet — köp alltid ersättningsbatterier när du behöver dem.</p>
  </item>
</list>

<note>
  <p>Detta råd gäller speciellt för litiumjonbatterier (Li-Ion) vilket är den vanligaste typen. Andra typer av batteriet kan dra fördel av annan behandling.</p>
</note>

</page>
