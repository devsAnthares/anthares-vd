<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-differentsize" xml:lang="sv">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Skriv ut ett dokument på en annan pappersstorlek eller orientering.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra pappers storlek vid utskrift</title>

  <p>Om du vill ändra pappersstorlek för ditt dokument (till exempel, skriva ut en PDF i US Letter-storlek på ett A4-papper) kan du ändra utskriftsformatet för dokumentet.</p>

  <steps>
    <item>
      <p>Öppna utskriftsdialogen genom att trycka på <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Välj fliken <gui>Sidinställningar</gui>.</p>
    </item>
    <item>
      <p>Under kolumnen <gui>Papper</gui>, välj din <gui>Pappersstorlek</gui> från rullgardinsmenyn.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skriv ut</gui> för att skriva ut ditt dokument.</p>
    </item>
  </steps>

  <p>Du kan också använda rullgardinsmeny <gui>Orientering</gui> för att välja en annan orientering:</p>

  <list>
    <item><p><gui>Stående</gui></p></item>
    <item><p><gui>Liggande</gui></p></item>
    <item><p><gui>Omvänd stående</gui></p></item>
    <item><p><gui>Omvänd liggande</gui></p></item>
  </list>

</page>
