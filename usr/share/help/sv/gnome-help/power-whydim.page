<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="sv">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-28" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skärmen tonas ner när datorn är oanvänd för att spara ström.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Varför tonas min skärm ner efter ett tag?</title>

  <p>Om det är möjligt att ställa in ljusstyrkan på din skärm kommer datorn att tona ner den när den är oanvänd för att spara ström. När du börjar använda datorn igen kommer skärmen att bli ljusare igen.</p>

  <p>För att förhindra skärmen från att tona ner:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ström</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Slå om <gui>Dämpa skärmen när inaktiv</gui> till <gui>AV</gui> i avsnittet <gui>Strömspar</gui>.</p>
    </item>
  </steps>

</page>
