<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-autologin" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="shell-exit"/>

    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ställ in automatisk inloggning när du startar din dator.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Logga in automatiskt</title>

  <p>Du kan ändra dina inställningar så att du loggas in automatiskt på ditt konto när du startar din dator:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Användare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Användare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj användarkontot du vill logga in som automatiskt vid uppstart.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Lås upp</gui> och mata in ditt lösenord.</p>
    </item>
    <item>
      <p>Slå om <gui>Automatiskt inlogging</gui> till <gui>PÅ</gui>.</p>
    </item>
  </steps>

  <p>När du startar din dator nästa gång kommer du att loggas in automatiskt. Om du har detta alternativ aktiverat behöver du inte skriva in ditt lösenord för att logga in på ditt konto vilket innebär att om någon annan startar din dator så kommer de att ha åtkomst till ditt konto och dina personliga data inklusive dina filer och webbläsarhistorik.</p>

  <note>
    <p>Om din kontotyp är <em>Standard</em> så kan du inte ändra denna inställning. Kontakta systemadministratören som kan ändra denna inställning åt dig.</p>
  </note>

</page>
