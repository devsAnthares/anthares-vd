<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="a11y task" id="a11y" xml:lang="sv">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc><link xref="a11y#vision">Se</link>, <link xref="a11y#sound">höra</link>, <link xref="a11y#mobility">mobilitet</link>, <link xref="a11y-braille">punktskrift</link>, <link xref="a11y-mag">skärmförstorare</link>…</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hjälpmedel</title>

  <p>GNOME-skrivbordsmiljön inkluderar hjälpmedelsteknik för att stödja användare med olika typer av funktionshinder och speciella behov och för att interagera med vanliga hjälpmedelsenheter. En hjälpmedelsmeny kan läggas till i systemraden och lätt ge tillgång till många av hjälpmedelsfunktionerna.</p>

  <section id="vision">
    <title>Synnedsättningar</title>

    <links type="topic" groups="blind" style="linklist">
      <title>Blindhet</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>Nedsatt syn</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>Färgblindhet</title>
    </links>
    <links type="topic" style="linklist">
      <title>Övriga ämnen</title>
    </links>
  </section>

  <section id="sound">
    <title>Hörselnedsättningar</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>Rörelsehinder</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>Musförflyttning</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>Klicka och dra</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>Användning av tangentbord</title>
    </links>
    <links type="topic" style="linklist">
      <title>Övriga ämnen</title>
    </links>
  </section>
</page>
