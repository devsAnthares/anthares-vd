<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-encoding" xml:lang="sv">
  <info>
    <link type="guide" xref="index#preferences"/>
    <revision version="0.1" date="2013-03-02" status="candidate"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändra till en annan teckenuppsättning.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Teckenkodning</title>

  <p><em>Teckenkodningen</em> är som standard vanligtvis UTF-8. Du kan vilja byta teckenkodning i <app>Terminal</app> om du:</p>

  <list>
    <item>
      <p>arbetar med fil- eller katalognamn som använder tecken som inte finns tillgängliga i din standardkodning.</p>
    </item>
    <item>
      <p>använder en extern hårddisk som använder en annan kodning än ditt system.</p>
    </item>
    <item>
      <p>ansluter till en fjärrdator som använder en annan kodning.</p>
    </item>
  </list>

  <section id="change-encoding">
    <title>Ändra teckenkodningen</title>

    <steps>
      <item>
        <p>Välj <guiseq><gui style="menu">Terminal</gui> <gui style="menuitem">Ställ in teckenkodning</gui></guiseq>.</p>
      </item>
      <item>
        <p>Välj den önskade teckenkodningen.</p>
      </item>
    </steps>

  </section>

</page>
