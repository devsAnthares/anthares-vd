<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-menubar" xml:lang="sv">

  <info>
    <revision version="0.1" date="2013-02-21" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref#global"/>
    <link type="seealso" xref="pref-keyboard-access"/>
    <link type="seealso" xref="app-fullscreen"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dölj och återställ menyraden.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  </info>

  <title>Synlighet för menyrad</title>

  <p>Du kan aktivera eller inaktivera menyraden efter vad du vill. Detta kan vara användbart om du har begränsat skärmutrymme. För att dölja menyraden:</p>

  <steps>
    <item>
      <p>Välj <gui style="menu">Visa</gui> och avmarkera <gui style="menuitem">Visa menyrad</gui>.</p>
    </item>
  </steps>

  <p>För att återställa menyraden:</p>

  <steps>
    <item>
      <p>Högerklicka i <app>Terminal</app> och välj <gui style="menuitem">Visa menyrad</gui>.</p>
    </item>
  </steps>

  <p>För att aktivera eller inaktivera menyraden som standard i alla <app>Terminal</app>-fönster som du öppnar:</p>

  <steps>
    <item>
      <p>Välj <guiseq><gui style="menu">Redigera</gui> <gui style="menuitem"> Inställningar</gui> <gui style="tab">Allmänt</gui></guiseq>.</p>
    </item>
    <item>
      <p>Markera eller avmarkera <gui>Visa menyrad som standard i nya terminalfönster</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Du kan <link xref="adv-keyboard-shortcuts">ställa in en tangentbordsgenväg</link> för att visa och dölja menyraden.</p>
  </note>

</page>
