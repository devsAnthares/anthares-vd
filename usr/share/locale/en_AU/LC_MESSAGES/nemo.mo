��    F      L  a   |              .     >   N  *   �  1   �      �  ,     o   8     �  i   �       Q   (     z     �     �     �     �     �     �  2   �     !	     ?	     K	     S	     `	  "   l	     �	     �	  /   �	     �	  2   �	     !
  
   5
  (   @
      i
     �
  
   �
  
   �
     �
     �
     �
     �
      �
  -     5   >  8   t  &   �  )   �  +   �  #   *  �   N  #   �  '   �  	        )     -     :     H  F   V  G   �     �     �     �               $     )     2     9  �  E     �  .     >   D  *   �  1   �      �  ,     o   .     �  i   �       Q        p     �     �     �     �     �     �  2   �  #        ;     G     O     f  "   x     �     �  /   �     �  2   �     -  
   A  (   L      u     �  
   �  
   �     �     �     �     �      �  -     5   J  8   �  &   �  )   �  +   
  #   6  �   Z  #   �  '     	   +     5     9     F     T  F   b  G   �     �     �                    0     5     >     E            ;   C      E      9   +          -   %   	   7           "   A       ?      0   @       6   ,   (      4   $             >   .       #   3   <         *                                               :   &      
            5           B   D          1                        2   )               !       /                 '      8         F      =    

Add connect to server mount 

Browse the file system with the file manager "%s" could not be found. Perhaps it has recently been deleted. --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Add Desklets Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. C_onnect Can't load the supported server method list.
Please check your gvfs installation. Cannot find the autorun program Change Desktop _Background Command Comment Connect to Server Connecting... Continue Create the initial window with the given geometry. Delete all items in the Trash Description Desktop E_mpty Trash Empty Trash Error starting autorun program: %s FTP (with login) GEOMETRY Nemo could not create the required folder "%s". No bookmarks defined Only create windows for explicitly specified URIs. Operation cancelled Pass_word: Perform a quick set of self-check tests. Please verify your user details. Print but do not open the URI Public FTP Quit Nemo. SSH Secure WebDAV (HTTPS) Server Details Sh_are: Show the version of the program. Sorry, could not change the group of "%s": %s Sorry, could not display all the contents of "%s": %s The desktop view encountered an error while starting up. The desktop view encountered an error. The folder "%s" cannot be opened on "%s". The folder contents could not be displayed. The server at "%s" cannot be found. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There was an error displaying help. There was an error displaying help: 
%s Try Again URL User Details WebDAV (HTTP) Windows share You do not have the permissions necessary to change the group of "%s". You do not have the permissions necessary to view the contents of "%s". [URI...] _Domain name: _Folder: _Port: _Remember this password _Run _Server: _Type: _User name: Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-01-26 18:23+0000
Last-Translator: Laywah <hansen.ross@live.com.au>
Language-Team: English (Australia) <en_AU@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 

Add connect to server mount 

Browse the file system with the file manager "%s" could not be found. Perhaps it has recently been deleted. --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Add Desklets Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. C_onnect Can't load the supported server method list.
Please check your gvfs installation. Cannot find the autorun program Change Desktop _Background Command Comment Connect to Server Connecting... Continue Create the initial window with the given geometry. Delete all items in the Rubbish Bin Description Desktop E_mpty the Rubbish Bin Empty Rubbish Bin Error starting autorun program: %s FTP (with login) GEOMETRY Nemo could not create the required folder "%s". No bookmarks defined Only create windows for explicitly specified URIs. Operation cancelled Pass_word: Perform a quick set of self-check tests. Please verify your user details. Print but do not open the URI Public FTP Quit Nemo. SSH Secure WebDAV (HTTPS) Server Details Sh_are: Show the version of the program. Sorry, could not change the group of "%s": %s Sorry, could not display all the contents of "%s": %s The desktop view encountered an error while starting up. The desktop view encountered an error. The folder "%s" cannot be opened on "%s". The folder contents could not be displayed. The server at "%s" cannot be found. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There was an error displaying help. There was an error displaying help: 
%s Try Again URL User Details WebDAV (HTTP) Windows share You do not have the permissions necessary to change the group of "%s". You do not have the permissions necessary to view the contents of "%s". [URI...] _Domain name: _Folder: _Port: _Remember this password _Run _Server: _Type: _User name: 