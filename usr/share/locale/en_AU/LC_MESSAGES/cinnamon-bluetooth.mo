��            )         �     �     �     �  	   �     �  (        4     D  
   a  *   l     �  K   �     �     
     &     )     E     L     h  %   o     �     �     �     �     �  
   �     �     �     �       y       �     �     �  	   �     �  (   �            
   5  *   @     k  K   �     �     �     �     �                <  %   C     i     w     �     �     �  
   �     �     �     �     �                                                    	                                                 
                                   Address Always grant access Authorization request from %s Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection Device %s wants access to the service '%s' Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found Paired Pairing confirmation for %s Reject Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-02-08 10:57+0000
Last-Translator: Brian B <Unknown>
Language-Team: English (Australia) <en_AU@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Address Always grant access Authorisation request from %s Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection Device %s wants access to the service '%s' Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adaptors found Paired Pairing confirmation for %s Reject Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 