��    0      �  C         (  K   )  &   u  (   �  S   �  "     $   <  $   a  &   �     �  >   �     �  9   �     1  7   =  8   u  #   �     �     �               ,     ?     K     S     Y     k     �     �     �     �     �  #   �  #   �  -   	  &   7  m   ^     �     �     �     �     �  $   	  	   )	  �   3	  �   �	  �   N
     �
  �    _   �  '   �  7     F   S  1   �  3   �  .      /   /     _  R   e     �  S   �       ;   '  @   c  (   �  %   �  $   �          4     N     i     �     �     �  #   �     �     �  "   �       &     $   ;      `  +   �  &   �  �   �     \     p     x     �     �  %   �     �  �   �  �   �  �   ]     =     *   '      +       	                          $   .                 )         !   (       
             -      "                  /           ,              0                                                  &   %             #          Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot determine user's home directory Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Lock screen Log out Mouse Mouse Preferences Play (or play/pause) Search Select Sound File Slow Keys Alert Sound Sticky Keys Alert Sync text/plain and text/* handlers The file %s is not a valid wav file The sound file for this event does not exist. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Do not show this message again Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2004-12-13 19:02+0200
Last-Translator: Zuza Software Foundation <info@translate.org.za>
Language-Team: Zulu <translate-discuss-zu@lists.sourceforge.net>
Language: zu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Ihlulekile ukudala indlela yamakheli "%s".
Lokhu kuyadingeka ukuvumela izikhombi ezishintshayo. Ihlulekile ukuthola ikheli lomsebenzisi Ihlulekile ukufaka ihele lomsindo %s njenge sibonelo %s Ihlulekile ukulalisa umashini.
Bhekisisa ukuba umashini uhlelwe kahle. Ingabe ufuna ukukhanyisa amankinombho angasheshi? Ingabe ufuna ukukhanyisa amankinombho anamathelayo? Ingabe ufuna ukucisha amankinombho angasheshi? Ingabe ufuna ukucisha amankinombho anmathelayo? Khipa Iphutha lenzekile uma usazama ukusebenza (%s)
elixhunyanyiswe kwisihlushulelo (%s) Isiqwema Inkinombho GConf %s lihlelelwe ukubhala %s kepha ukubhala okulindelekile bekuyi %s
 Isibaya sasekhaya Ukuphetha inkinombho (%s) inesenzo sayo esichaziwe kaningi
 Ukuphetha inkinombho (%s) inokuphethao kwayo okuchaziwe kaningi
 Ukuphetha inkinombho (%s) iyasebenziswa
 Ukuphetha inkinombho (%s) ayiphelele
 Ukuphetha inkinombho (%s) akufanele
 Indawo yokushaya uma ubhala Ngenisa usizo lwesiyaluzi Ngenisa isiyaluzi solwembu Vala ubuso besiga-nyezi Phuma Isingoso Okuthandekayo kwesingoso Dlala (noma dlala/misa isikhashana) Hlola Khetha ihele lomsindo Amankinombho angasheshi isexwayiso Umsindo Ukwexwayiswa ngamankinombho angasheshi Sync text/plain and text/* abaphathi Ihele %s asiyilo ihele elifanele Ihele lomsindo lwalomcimbi alikho sanhlobo. Kube khona iphutha elibonisa usizo: %s Kube khona iphutha makuqalwa isigcini sobuso besiga-nyezi:

%s

Ukusebenza kwesigcini sobuso besiga-nyezi ngekhe kusebenze kulesiqephu. Ukunqamula ukubhala Ivolumu Ivolumu ephantsi Ukucisha ivolumu okwesikhashana Inyathelo lwevolumu Inyathelo lwevolumu njengamaphesenti. Ivolumu ephezulu Uqeda ukubambela phantsi inkinombho Shift imizuzwana 8. Le yindlela emfishane yemniningo engasheshi yamankinombho, leyo ethinta indlela indawo yokushaya uma ubhala esebenza ngayo. Uqeda ukupotshozela inkinombho Shift 5 emugqeni. Le yindlela emfishane yemniningo yamankinombho anamathelayo, lawo athinta indlela indawo yakho yokushaya uma ubhala esebenza ngayo. Uqeda ukupotsoza amankinombho amabili kanye, noma upotshoze inkinombho Shift izihlanhlo 5 emugqeni. Lokhu kucisha imniningwane yamankinombho anamathelayo, athintana nendlela indawo yakho yokushaya uma ubhala isebenza ngayo. _Unga khombisi lomyalezo futhi 