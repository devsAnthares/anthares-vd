��          D      l       �   &   �   �   �   �   <  �   �  �  m  &   '  �   N  �     �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:32+0000
Last-Translator: Zuza Software Foundation <Unknown>
Language-Team: Zulu <translate-discuss-zu@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:18+0000
X-Generator: Launchpad (build 18688)
 Kube khona iphutha elibonisa usizo: %s Uqeda ukubambela phantsi inkinombho Shift imizuzwana 8. Le yindlela emfishane yemniningo engasheshi yamankinombho, leyo ethinta indlela indawo yokushaya uma ubhala esebenza ngayo. Uqeda ukupotshozela inkinombho Shift 5 emugqeni. Le yindlela emfishane yemniningo yamankinombho anamathelayo, lawo athinta indlela indawo yakho yokushaya uma ubhala esebenza ngayo. Uqeda ukupotsoza amankinombho amabili kanye, noma upotshoze inkinombho Shift izihlanhlo 5 emugqeni. Lokhu kucisha imniningwane yamankinombho anamathelayo, athintana nendlela indawo yakho yokushaya uma ubhala isebenza ngayo. 