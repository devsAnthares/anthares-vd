��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  @   ^  /   �  >   �  '     %   6     \     w     �  N   �  #   �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: kristjan <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto (http://www.transifex.com/projects/p/freedesktop/language/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 Aŭtentigo bezonatas por ŝanĝi la agordojn de la ensalutekrano Aŭtentigo bezonatas por ŝanĝi uzantodatumojn Aŭtentigo bezonatas por ŝanĝi viajn proprajn uzantodatumojn Ŝanĝi la agordojn de la ensalutekrano Ŝanĝi viajn proprajn uzantodatumojn Enŝalti sencimigan kodon Administri uzantokontojn Eligi versioinformon kaj eliri Provizas D-Bus-interfacon por petado kaj manipuli
informojn de la uzantokonto. Anstataŭigi ekzistantan instancon 