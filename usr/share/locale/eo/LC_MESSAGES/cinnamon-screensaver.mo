��          L      |       �   `   �      
     %  )   B     l  )  �  \   �          0  %   P  %   v                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-11-26 00:08+0000
Last-Translator: Jon Stevenson <jon.stevenson@gmail.com>
Language-Team: Esperanto <ubuntu-l10n-eo@lists.launchpad.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: eo
 La ekrankurteno estas aktiva dum %d sekundo.
 La ekrankurteno estas aktiva dum %d sekundoj.
 Le ekrankurteno estas aktiva
 Le ekrankurteno estas neaktiva
 La ekrankurteno ne estas aktiva nun.
 La majuskla baskulo estas enŝaltita. 