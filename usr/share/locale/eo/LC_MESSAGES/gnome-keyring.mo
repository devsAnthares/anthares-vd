��    E      D  a   l      �  
   �  I   �  U   F  E   �  I   �  H   ,  9   u     �     �     �  "   �  .        E     e     q     z     �     �     �     �     �  (   �  4   	  (   7	  '   `	  /   �	     �	  
   �	  X   �	     #
     ,
     5
     ;
     H
     U
     j
  (   �
     �
     �
  $   �
     �
  	   �
                    (     ?     M     S     p     w       #   �  !   �     �  F   �      #     D     K     Z     m     �     �     �     �     �     �     �  (    
   -  C   8  W   |  E   �  L     L   g  9   �  #   �  "        5  *   J  1   u  '   �     �     �     �  
   �  	   �     �            *   -  A   X  3   �  0   �  5   �     5     ;  O   G  	   �     �     �     �     �     �     �  +        -     6  )   D     n     z     �     �     �     �     �     �  #   �     �  
   �     	  #         ;     \  I   c  "   �  	   �     �     �  *        /     H     e     �     �     �     �     =          	                  ;              A   3   '                          (          !   9   *       E   .   C               )                B   2   &              +   /       %   "   0   
      4                           <          1                        #   $   ,          D             >   :       8          6      5       ?   -   @   7                  An application wants access to the certificate “%s”, but it is locked An application wants access to the certificate/key storage “%s”, but it is locked An application wants access to the keyring “%s”, but it is locked An application wants access to the private key “%s”, but it is locked An application wants access to the public key “%s”, but it is locked An application wants access to “%s”, but it is locked Certificate and Key Storage Change Keyring Password Change Password Change password for secure storage Choose a new password for the “%s” keyring Choose password for new keyring Common Name Continue Country Country of Citizenship Country of Residence DSA Date of Birth Enter password to unlock Enter password to unlock the certificate Enter password to unlock the certificate/key storage Enter password to unlock the private key Enter password to unlock the public key Enter the old password for the “%s” keyring Gender Given Name In order to prepare “%s” for storage of certificates or keys, a password is required Initials Locality Login MD2 with RSA MD5 with RSA New Keyring Password New Password Required New password required for secure storage Organization Organizational Unit Original password for secure storage Place of Birth Pseudonym RSA SHA1 with DSA SHA1 with RSA Secret Storage Service Serial Number State Store passwords unencrypted? Street Surname Telephone Number The original password was incorrect The unlock password was incorrect Title To change the password for “%s”, the original password is required Type a new password for “%s” Unlock Unlock Keyring Unlock certificate Unlock certificate/key storage Unlock password for: %s Unlock private key Unlock public key Unnamed Unnamed Certificate User ID can't create `%s': %s
 Project-Id-Version: gnome-keyring
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-keyring&keywords=I18N+L10N&component=general
POT-Creation-Date: 2017-02-26 07:38+0000
PO-Revision-Date: 2017-06-11 03:51+0200
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <gnome-eo-list@gnome.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
            Aplikaĵo volas akiri la atestilon “%s”, sed ĝi estas ŝlosita Aplikaĵo volas akiri memorejon de atestiloj/ŝlosiloj “%s”, sed ĝi estas ŝlosita Aplikaĵo volas akiri la ŝlosilaron “%s”, sed ĝi estas ŝlosita Aplikaĵo volas akiri la privatan ŝlosilon “%s”, sed ĝi estas ŝlosita Aplikaĵo volas akiri la publikan ŝlosilon “%s”, sed ĝi estas ŝlosita Aplikaĵo volas akiri al “%s”, sed ĝi estas ŝlosita Memorejo de atestiloj kaj ŝlosiloj Ŝanĝi pasvorton de la ŝlosilaro Ŝanĝi la pasvorton Ŝanĝi la pasvorton de la sekura memorejo Elektu novan pasvorton por la ŝlosilaro “%s” Elekti pasvorton por la nova ŝlosilaro Komuna nomo Daŭrigi Lando Civitlando Loĝlando DSA Naskiĝdato Enigi pasvorton por malŝlosi Enigi pasvorton por malŝlosi la atestilon Enigi pasvorton por malŝlosi la memorejon de atestiloj/ŝlosiloj Enigi pasvorton por malŝlosi la privatan ŝlosilon Enigi pasvorton por malŝlosi publikan ŝlosilon Enigu la malnovan pasvorton de la ŝlosilaro “%s” Sekso Donita nomo Pasvorto bezonatas por pretigi “%s” por memorejo de atestiloj aŭ ŝlosiloj Inicialoj Urbo Ensaluti MD2 kun RSA MD5 kun RSA Pasvorto por nova ŝlosilaro Nova pasvorto estas bezonata Nova pasvorto bezonatas por sekura memorejo Organizo Organiza unuo Originala pasvorton de la sekura memorejo Naskiĝloko Pseŭdonimo RSA SHA1 kun DSA SHA1 kun RSA Servo pri sekreta memorejo Seria numero Ŝtato Ĉu memori neĉifritajn pasvortojn? Strato Familinomo Telefonnumero La origina pasvorto estas nekorekta Malŝlospasvorto estas nekorekta Titolo La originala pasvorto estas bezonata por ŝanĝi la pasvorton de “%s” Tajpu novan pasvorton por “%s” Malŝlosi Malŝlosi ŝlosilaron Malŝlosi atestilon Malŝlosi memorejon de atestiloj/ŝlosiloj Malŝlospasvorto por: %s Malŝlosi privatan ŝlosilon Malŝlosi publikan ŝlosilon Sennome Nenomata atestilo Uzant-ID ne eblas krei `%s': %s
 