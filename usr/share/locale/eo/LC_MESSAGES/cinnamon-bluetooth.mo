��          �   %   �      @     A  	   I     S  (   i     �     �  
   �  K   �          (     D     G     c  %   j     �     �     �     �     �  
   �     �     �     �     �  �       �     �     �  1   �     �     �       G   #     k     ~     �     �     �  (   �     �     �       
   &     1  	   6     @     R     V     ^                                                                                             
                   	              Address Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found Paired Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-05-07 13:33+0000
Last-Translator: Jon Stevenson <jon.stevenson@gmail.com>
Language-Team: Esperanto <eo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adreso Bludento Bludento estas malŝaltita Bludento estas malŝaltita per aparatara ŝaltilo Foliumi dosierojn... Ŝanĝi la agordojn de Bludento Konekto Se vi forigas la aparaton, vi devos agordi ĝin denove antaŭ uzi ĝin. Agordoj de klavaro Agordoj de muso kaj tuŝplato Ne Ne trovis Bludento-aparaton Parigite Ĉu forigi '%s' de la listo de aparatoj? Forigi aparaton Sendi dosierojn... Agordi novan aparaton Sonagordoj Tipo Videbleco Videbleco de '%s' Jes paĝo 1 paĝo 2 