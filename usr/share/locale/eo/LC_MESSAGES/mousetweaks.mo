��    $      <  5   \      0  A   1     s     �     �  0   �     �     �  
   �     �       
        *  
   6     A     T     [     h     t     �     �     �  7   �     �          !  	   0     :     C     J     O     U     d     q     x     �  &  �  J   �     �          $  .   =     l     y  
        �     �     �     �     �     �     �  	   	  
   	     	  %   3	     Y	     g	  .   v	  
   �	     �	     �	  
   �	  	   �	     �	     �	     �	     
     
     
     ,
     5
                                    	                                           "                        #                                  $                  !                  
       Applet to select different dwell-click types.
Part of Mousetweaks Area to lock the pointer Button Style Click Type Window Control your desktop without using mouse buttons Double Click Drag Drag Click Dwell Click Applet Enable dwell click Horizontal Hover Click Icons only Keyboard modifier: Locked Mouse button Orientation Pointer Capture Applet Pointer Capture Preferences Right Click Secondary Click Selecting Button 0 will capture the pointer immediately Single Click Size of the Capture Area Text and Icons Text only Vertical _About _Alt _Help _Mouse button: _Preferences _Shift _Width: pixels Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&keywords=I18N+L10N&component=general
PO-Revision-Date: 2011-04-20 19:01+0200
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <ubuntu-l10n-eo@lists.launchpad.net>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-04-20 16:52+0000
X-Generator: Launchpad (build 12847)
Plural-Forms: nplurals=2; plural=(n != 1)
 Aplikaĵeto por elekti diversajn prokrast-klaktipojn.
Parto de Mousetweaks Areo por ŝlosi  la montrilon Butona stilo Fenestro de la klaktipoj Regi vian labortablon sen uzo de musaj butonoj Duobla klako Treni Tren-klako Prokrast-klaka aplikaĵeto Enŝalti prokrast-klakon Horizontale Pertuŝa klako Nur piktogramoj Klavara modifilo: Ŝlosite Musbutono Orientiĝo Montril-kapta aplikaĵeto Agordoj pri kaptado de la musmontrilo Dekstra klako Duaranga klako Premado de butono 0 tuj kaptas la musmontrilon Sola klako Grando de la kapta areo Teksto kaj piktogramoj Nur teksto Vertikale _Pri _Alt _Helpo _Musbutono: _Agordoj _Majuskliga klavo _Larĝo: bilderoj 