��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  %  �  -   �
  	   �
  /   �
          #  �   1     �     �  3   �          $  2   A     t      �  @   �  +   �  b     "   s  "   �     �     �     �  #        (  (   8     a  '   p     �  (   �  &   �     �               #     0     K     b     p     ~     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2017-12-23 17:31+0000
PO-Revision-Date: 2018-02-23 22:10+0100
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <gnome-eo-list@gnome.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
X-Project-Style: gnome
 Defaŭlta grupo, atribuita je fenestra kreado Indikilo: Teni kaj administri apartan grupon per fenestro Klavararanĝo Klavararanĝo Klarararanĝoj "%s"
Kopirajto &#169; X.Org-fondaĵo kaj XKeyboardConfig-kontribuantoj
Por permesilado vidu al metadatumoj de la pakaĵo Klavarmodelo Klavaropcioj Ŝarĝi ekzotaj, malofte uzitaj aranĝoj kaj opcioj Ŝarĝi kromajn agordoaĵojn Antaŭmontri klavararanĝojn Konservi/restarigi indikiloj kune kun aranĝgrupoj Kromaj grupoj Montri flagojn en la aplikaĵeto Montri flagojn en la aplikaĵeto por indiki la aktualan aranĝon Montri aranĝnomojn anstataŭ ol grupnomojn Montri aranĝnomojn anstataŭ ol grupnomojn (nur por versioj de XFree subtenantaj pluraj aranĝoj) La klavar-antaŭrigardo, X-deŝovo La klavar-antaŭrigardo, Y-deŝovo La klavar-antaŭrigardo, alto La klavar-antaŭrigardo, larĝo La fonkoloro La fonkoloro por la aranĝoindikilo La tiparfamilio La tipara familio por la aranĝoindikilo La tipargrando La tipara grando por la aranĝoindikilo La malfona koloro La malfona koloro por la aranĝoindikilo Okazis eraro dum ŝargado de bildo: %s Nekonate XKB-prepara eraro klavararanĝo klavarmodelo aranĝo "%s" aranĝoj "%s" modelo "%s", %s kaj %s neniu aranĝo neniuj opcioj opcio "%s" opcioj "%s" preferences-desktop-keyboard 