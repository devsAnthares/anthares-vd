��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S    w     }     �  8   �     �  	          W   !  i   y     �  )   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-12-25 03:05+0200
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <gnome-eo-list@gnome.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Publikaj dosieroj de %s Publikaj dosieroj de %s sur %s Lanĉi personan dosierkunhavigon se ĝi estas enŝaltite Persona dosierkunhavigo Kunhavigo Kunhavigo-agordoj Enŝalti personan dosierkunhavigon por kunhavigi enhavojn de tiu dosierujo per la reto. Kiam peti pasvortojn. Eblaj valoroj estas "never" (neniam), "on_write" (por skribi) kaj "always" (ĉiam). Kiam postuli pasvortojn kunhavigi;dosieroj;http;reto;kopii;sendi; 