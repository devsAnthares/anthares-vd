��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �          $     ?     K     Z     i     {     �     �     �      �     �  C   �     B	  
   \	     g	  '   �	  M   �	  �   �	  +   �
  n   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop 2.3.6.1
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-10-15 14:33+0200
Last-Translator: Carmen Bianca BAKKER <carmen@carmenbianca.eu>
Language-Team: Esperanto <gnome-eo-list@gnome.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.1.1
X-Project-Style: gnome
 %R %R:%S %a %R %a %R:%S %a %-e-a de %b_%R %a %-e-a de %b_%R:%S %a %-e-a de %b_%l:%M %p %a %-e-a de %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %-e-a de %b_%R %-e-a de %b_%R:%S %-e-a de %b_%l:%M %p %-e-a de %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d ne subtenas la eligon %s CRTC %d ne subtenas rotacion=%d CRTC %d: provreĝimo %dx%d@%dHz kun eligo ĉe %dx%d@%dHz (paso %d)
 Provreĝimoj por CRTC %d
 Nedifinita ne eblas kloni al eligo %s ne eblis atribui CRTC-ojn al eligoj:
%s neniu reĝimo de la elektitaj reĝimoj estas kongrua al la eblaj reĝimoj:
%s eligo %s ne havas la samajn parametrojn kiel alia klonita eligo:
ekztistanta reĝimo = %d, nova reĝimo = %d
ekztistantaj koordinatoj = (%d, %d), novaj koordinatoj = (%d, %d)
ekztistanta rotacio = %d, nova rotacio = %d eligo %s ne subtenas la reĝimon %dx%d@%dHz bezonata virtuala grando ne kongruas al havebla grando: petita=(%d, %d), minimumo=(%d, %d), maksimumo=(%d, %d) 