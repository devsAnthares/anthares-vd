��    H      \  a   �            !  
   <     G     `          �  1   �  	   �  
   �     �  �   �     l     r          �     �     �     �     �     �     �     �     �  	   	          !     &  
   /     :     A     O     X     ]     f     ~     �     �     �     �     �     �     �     �     �     �     
	     	     %	     6	     >	     ^	     d	     t	     {	  	   �	     �	     �	     �	     �	  �   �	     S
     m
  &   u
     �
     �
  	   �
      �
     �
     �
     �
     	  �       �  
   �     �  +        :     H  8   P  	   �  
   �     �  �   �     9  	   A     K     [     m     v     �     �     �     �     �     �     �            
   &  
   1     <     D     R     _     d  "   q  !   �     �  "   �     �     �     �  	                  ,     4  
   T     _     r  
   �  -   �     �     �     �     �     �                &     :  �   B     �     �  2   �     0     E  
   b  /   m     �     �     �     �     '   <                                                $              /   (              *   .   8   =      D   ?          >   :   !      +       @   ;             9   2          	                ,   #   B   
          G               0   4   A       )      5   E   6       "      C   F                 1   %             H      -   7           &      3        troubleshooting purposes. %1$s: %2$s Add applets to the panel Add or remove users and groups Administrator Applets Are you sure you want to remove workspace "%s"?

 Bluetooth Brightness Cancel Cinnamon is currently running without video hardware acceleration and, as a result, you may observe much higher than normal CPU usage.

 Close Configure... Connect to... Control Center Copy Disabled Eject Error Execution of '%s' failed: Failed to launch '%s' Failed to unmount '%s' File System Hide Text High Contrast Home Keyboard Large Text Loaded Looking Glass Maximize Menu Minimize Move to a new workspace Move to left workspace Move to monitor %d Move to right workspace No OK Open Out of date Panel Panel settings Paste Please enter a command: Remove Remove this desklet Restart Cinnamon Restore Restore all settings to default Retry Screen Keyboard Search Settings Show Text Sound Settings System Information System Settings Themes There could be a problem with your drivers or some other issue.  For the best experience, it is recommended that you only use this mode for Universal Access Settings Unknown Use the arrow keys to shift workspaces Users and Groups Visible on all workspaces WORKSPACE Wrong password, please try again Yes Zoom connecting... disconnecting... Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-26 02:18+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Limburgian <li@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
  probleemoplossing. %1$s: %2$s Applets aan het paneel toevoege Gebroekers en groepe toevoege of verwijdere Administrator Applets Wit ger zeker det ger wirkplaats "%s" wilt verwijdere?

 Bluetooth Helderheid Annulere Cinnamon lùp momenteel zônger video hardware versjnelling en, es resultaat, kindj ger ein veul hoger processorgebroek (CPU) zeen den normaal.

 Sjloete Insjtelle Verbinje mit... Configurasiesjerm Kopiëre Oetgesjakeld Oetwerpe Fout Oetveure van '%s'is mislùk: Sjtarte van '%s' is mislùk Oetwerpe van '%s' mislùk Bestjandssysteem Teks verbérge Koog contras Perseunlike besténj Toetsebord Grote Teks Gelaaje Vergrootglaas Maximalizere Menu Minimalizere Verplaatste noa een nuu werkplaats Verplaatste noa linker werkplaats Verplaatste noa beeldsjerm %d Verplaatste noa rechter werkplaats Nèe OK Aöpene Veraajerd Paneel Paneel insjtellinge Plékke Veur estebleef ein commando in: Verwijdere Desklet verwijdere Cinnamon hersjtarte Hersjtelle Alle insjtellinge hersjtelle noa de standaard Opnuu probere Sjermtoetsebord Zeuke Insjtellinge Teks loate zeen Geluid Insjtellinge Systeeminformasie Systeeminsjtellinge Thema's D'r kòs ein probleem zeen mit eur drivers of ein angere kwestie. Veur de beste ervaring is het aangeroaje det ger deze modus allein gebroek veur Universele Toegang Insjtellinge Onbekind Gebroek de pieltoetse om werkruumtes te verplaatse Gebroekers en groepe Zichbaar op alle werkplaatse Werkruumte Verkeerd wachwoard, probeer het estebleef opnuu Joa Zoome verbinje... verbinjine verbraeke... 