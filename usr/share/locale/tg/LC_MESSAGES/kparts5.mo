��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  v  v     �  g   �  ,   e     �  =   �  
   �     �            ,   )     V  U   n     �     �  B   �     =	     X	     n	  !   �	     �	     �	  $   �	  m   �	  d   g
  �   �
     T                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2013-06-04 17:36+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik Language
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=2; plural=n != 1;
 &Ҷустуҷӯ <qt>Оё воқеъан мехоҳед дар интернет ҷустуҷӯ кунед<b>%1</b>?</qt> Открыть &в программе «%1» Кушодани '%1'? Имя файла, предложенное сервером. Ном: %1 Намуд: %1 &Кушодан &Кушодан бо... &Открыть в программе «%1» &Кушодан бо... В будущем поступать с файлами этого типа также Қабул кардан Пӯшидани Ҳуҷҷат Оё воқеъан мехоҳед '%1'-ро иҷро кунед?  victor.ibragimov@gmail.com Иҷро кардан Ҷойнавишти файл? Ҷустуҷӯи интернет Victor Ibragimov Рад кардан Нигоҳ доштан ҳамчун Не удаётся найти программу менеджера загрузки (%1) в пути $PATH. Ҳуҷҷат "%1" дигаргун карда шуд.
Оё мехоҳед нигоҳаш доред? Попробуйте переустановить программу.

Интеграция с Konqueror будет выключена. Бесарлавҳа 