��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  `   &
     �
  s   �
  )     )   9    c  #   e  %   �  ^   �  L     >   [  l   �       J   '  �   r  _   �  �   U  A     A   `  9   �  1   �       P   ,     }  N   �     �  L   �     K  P   i  V   �       #   "  )   F  #   p  3   �     �     �       #                    
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: Tajik Gnome
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2013-12-19 14:38+0000
PO-Revision-Date: 2014-01-28 22:32+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: 
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.3
 Гурӯҳи пешфарзе, ки ҳангоми эҷоди равзана таъйин шуд Индикатор: Нигоҳ доштан ва идора кардани гурӯҳи алоҳида барои ҳар равзана Тарҳбандии клавиатура Тарҳбандии клавиатура Тарҳбандии клавиатура "%s"
Ҳуқуқи муаллиф &#169; Ташкилоти X.Org ва саҳмгузорони XKeyboardConfig
Барои иҷозатдиҳӣ ба бастаи метаиттилоот муроҷиат намоед Намунаи клавиатура Имконоти клавиатура Боргирии тарҳбандиҳои ғайриоддӣ, камчин ва имконот Бор кардани объектҳои танзимотии иловагӣ Пешнамоиши тарҳбандии клавиатура Захира/барқарор кардани индикаторҳо бо гурӯҳҳои тарҳбандӣ Гурӯҳҳои иловагӣ Намоиш додани байрақчаҳо дар зербарнома Намоиш додани байрақчаҳо дар зербарнома барои намоиши тарҳбандии ҷорӣ Намоиш додани номҳои тарҳбандӣ ба ҷои номҳои гурӯҳӣ Намоиш додани номҳои тарҳбандӣ ба ҷои номҳои гурӯҳӣ (танҳо барои версияҳои XFree бо дастгирии якчанд тарҳбандӣ) Пешнамоиши клавиатура, тағйирёбии X Пешнамоиши клавиатура, тағйирёбии Y Пешнамоиши клавиатура, баландӣ Пешнамоиши клавиатура, бар Ранги пасзамина Ранги пасзамина барои индикатори тарҳбандӣ Гурӯҳи шрифт Гурӯҳи шрифтҳо барои индикатори тарҳбандӣ Андозаи шрифт Андозаи шрифт барои индикатори тарҳбандӣ Ранги пешзамина Ранги пешзамина барои индикатори тарҳбандӣ Ҳангоми боркунии тасвир хатогӣ ба вуҷуд омад: %s Номаълум Хатои омодасозии XKB Тарҳбандии клавиатура намунаи клавиатура тарҳбандии "%s" тарҳбандии "%s" намунаи "%s", %s ва %s тарҳбандӣ нест имконот нест имкони "%s" имкони "%s" 