��          �   %   �      @     A  	   I     S  (   i     �     �  
   �  K   �          (     D     G     c  %   j     �     �     �     �     �  
   �     �     �     �     �  �       �  	   �  %   �  U   �  *   ?  3   j     �  �   �  '   �  6   �     �  5   �       I   *  &   t  &   �  3   �     �     	     	  *   ;	     f	     k	     |	                                                                                             
                   	              Address Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found Paired Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2014-01-14 06:14+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: tg
 Суроға Bluetooth Bluetooth ғайрифаъол аст Bluetooth бо калиди сахтафзор ғайрифаъол карда шуд Тамошо кардани файлҳо.. Танзимоти хусусиятҳои Bluetooth Пайвастшавӣ Агар шумо дастгоҳро тоза кунед, ба шумо лозим мешавад, ки пеш аз истифодабарии навбатӣ онро аз нав танзим кунед. Танзимоти клавиатура Танзимоти муш ва панели ламсӣ Не Ягон адаптери Bluetooth ёфт нашуд Ҷуфтдор '%s'-ро аз рӯйхати дастгоҳҳо тоза мекунед? Тоза кардани дастгоҳ Фиристодани файлҳо... Танзим кардани дастгоҳи нав Танзимоти садо Навъ Қобилияти намоиш Қобилияти намоиши “%s” Ҳа саҳифаи 1 саҳифаи 2 