��          �      l      �     �  #   �       1   &  3   X     �     �     �     �     �     �     �     �     �     �          (     A     _     x  �  �     X  @   p      �  v   �  v   I  ;   �  I   �  
   F     Q     `  
   i     t     �  ?   �  =   �  F     8   d  F   �  8   �  '   	                         
                        	                                                 @REALNAME@'s media A module named %s is already loaded Action Failed Add a directory to the list of shared directories Add a network interface Rygel should serve files on Add network interface Add shared directory Album Albums All Artist Artists Bad URI: %s Bad current tag value. Bad new tag value. Can't add containers in %s Can't create items in %s Can't remove containers in %s Can't remove items in %s [Plugin] group not found Project-Id-Version: rygel master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=Rygel&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-01-13 12:35+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.3
 Медиаи @REALNAME@ Модул бо номи %s аллакай бор шудааст Амал қатъ шудааст Илова кардани директория ба рӯйхати директорияҳои мубодилашуда Илова кардани интерфейси шабака барои иҷро кардани файлҳо бо Rygel Илова кардани интерфейси шабака Илова кардани директорияи мубодилашуда Албом Албомҳо Ҳама Ҳофиз Ҳофизон URI-и нодуруст: %s Қимати барчаспи ҷорӣ нодуруст аст. Қимати барчаспи нав нодуруст аст. Дарбаргирандаҳо ба %s илова намешаванд Объектҳо дар %s эҷод намешаванд Дарбаргирандаҳо дар %s тоза намешаванд Объектҳо дар %s тоза намешаванд Гурӯҳи [Plugin] ёфт нашуд 