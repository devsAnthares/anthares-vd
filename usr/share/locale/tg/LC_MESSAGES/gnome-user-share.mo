��          �   %   �      `     a     s     �     �  c   �  ,     '   ;     c  	   �     �     �     �     �     �     �  (   �  [     Q   `     �  8   �  -        3  ,   M     z     �  2   �  �  �      v  ,   �     �  8   �  �     K   �  d   >	  Y   �	     �	  ,   
     D
     Y
  "   q
  #   �
  6   �
  P   �
  �   @  �   �  2   �  y   �  ]   7  /   �  G   �  G     /   U     �                                                 	                         
                                                  %s's public files %s's public files on %s Cancel File reception complete If this is true, Bluetooth devices can send files to the user's Downloads directory when logged in. Launch Bluetooth ObexPush sharing if enabled Launch Personal File Sharing if enabled May be shared over the network Open File Personal File Sharing Preferences Receive Reveal File Sharing Sharing Settings When to accept files sent over Bluetooth When to accept files sent over Bluetooth. Possible values are "always", "bonded" and "ask". When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords Whether Bluetooth clients can send files using ObexPush. Whether to notify about newly received files. You have been sent a file You have been sent a file "%s" via Bluetooth You received "%s" via Bluetooth You received a file share;files;bluetooth;obex;http;network;copy;send; Project-Id-Version: Tajik Gnome
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-03-14 11:35+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: 
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
 Файлҳои оммавии %s Файлҳои оммавии %s's дар %s Бекор кардан Қабулкунии файл ба анҷом расид Агар "true" бошад, дастгоҳҳои Bluetooth метавонанд ҳангоми воридшавӣ файлҳоро ба директорияи корбарии "Боргириҳо" фиристонанд. Агар фаъол бошад, Bluetooth ObexPush-ро оғоз кунед Агар фаъол бошад, мубодилаи файлҳои шахсиро оғоз кунед Метавонад тавассути шабака мубодила карда шавад Кушодани файл Мубодилаи файлҳои шахсӣ Хусусиятҳо Қабул кардан Ошкор кардани файл Дастрасии муштарак Танзимоти дастрасии муштарак Шарти қабулкунии файлҳои ирсолшуда аз Bluetooth Шарти қабулкунии файлҳои ирсолшуда аз Bluetooth. Қиматҳои имконпазир: "ҳамеша", "шартӣ" ва "пурсиш". Шарти дархосткунии паролҳо. Қиматҳои имконпазир: "ҳеҷ гоҳ", "ҳангоми навишт" ва "ҳамеша". Шарти дархосткунии паролҳо Шартҳои муштариёни Bluetooth барои фиристодани файлҳо тавассути ObexPush. Шартҳои огоҳкунӣ дар бораи файлҳои қабулшудаи нав. Шумо файлеро ирсол кардед Шумо "%s"-ро тавассути Bluetooth ирсол кардед Шумо "%s"-ро тавассути Bluetooth қабул кардед Шумо файлеро қабул кардед мубодила кардан;файлҳо;bluetooth;obex;http;шабака;нусха бардоштан;фиристодан; 