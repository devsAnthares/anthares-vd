��          �      l      �     �     �           3     T     l     �     �  3   �  ;   �  5   +  <   a  w   �  @     9   W  3   �  K   �  5     I   G     �  �  �  -   \  H   �  3   �  F     4   N  3   �  A   �  7   �  ]   1	  x   �	  c   
  v   l
  �   �
  u   �  q   H  g   �  �   "  u   �  �   2  E   �                                                                          	      
                  Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2015-03-27 09:15+0000
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh (http://www.transifex.com/freedesktop/cups-pk-helper/language/kk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kk
Plural-Forms: nplurals=1; plural=0;
 Класты қосу/өшіру/түзету Жергілікті принтерді қосу/өшіру/түзету Принтерді қосу/өшіру/түзету Қашықтағы принтерді қосу/өшіру/түзету Принтер баптауларын өзгерту Принтерді іске қосу/сөндіру Қолжетерлік құрылғылар тізімін алу Сервер баптауларын алу/орнату Класты қосу/өшіру/түзету үшін привилегиялар керек. Жергілікті принтерді қосу/өшіру/түзету үшін привилегиялар керек. Принтерді қосу/өшіру/түзету үшін привилегиялар керек. Қашықтағы принтерді қосу/өшіру/түзету үшін привилегиялар керек. Принтер баптауларын өзгерту үшін привилегиялар керек. Бұл тек Принтерлер жүйелік баптаулар панелінен шақырылғанда керек болады. Принтерді не класты іске қосу/сөндіру үшін привилегиялар керек. Қолжетерлік құрылғылар тізімін алу үшін привилегиялар керек. Сервер баптауларын алу/орнату үшін привилегиялар керек. Басқа пайдаланушы тапсырмасын қайта іске қосу/үзу/түзету үшін привилегиялар керек. Тапсырманы қайта іске қосу/үзу/түзету үшін привилегиялар керек. Принтерді не класты үнсіз келісім бойынша принтері ету үшін привилегиялар керек. Тапсырманы қайта іске қосу/үзу/түзету 