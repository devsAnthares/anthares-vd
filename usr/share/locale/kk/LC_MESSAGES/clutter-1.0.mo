��    D      <  a   \      �     �  
   �     �  
   �       
        !     (     .     <     K     S     \     l     r     �     �     �     �     �  
   �     �     �     �     �  	   �     �               %     3     ?  
   L     W     \     a     h     t     y     �     �     �     �     �  
   �     �     �     �     �     �     �     �  3   �  0   0     a     f     l     p     y     �     �     �     �     �     �     �     �  �  �     �
     �
  
   �
     �
               +     >     E     ]     }     �     �     �  '   �     �     �          1     8     H     \     m     �  '   �     �  A   �  L         m      �     �     �      �       
             +     :  !   K     m     ~     �     �     �     �     �                #     0  
   9     D  :   \  6   �     �  
   �     �     �  #   �          ,     3     @  +   [  '   �     �     �                             )          
      .   1              2       7   %   D      (   B                "         *   !         ;   A       &       5          0                         C      ?      <          4                 /          3          $                +   8              ,   	   @          :   #   =   -   6   9   '                 >        Actions Allocation Alpha Attributes Background color Brightness Center Color Column Number Column spacing Content Contrast Cursor Position Delay Device Manager Duration Editable Enabled Expand Filename Font Color Height Held Homogeneous Horizontal Alignment Line wrap Make X calls synchronous Make all warnings fatal Mapped Margin Bottom Margin Left Margin Right Margin Top Mode Name Object Orientation Path Pixel Format Playing Position Progress Property Name Realized Row Number Row spacing Size Source Spacing State Text Text Direction The amount of space between two consecutive columns The amount of space between two consecutive rows Tint Title URI Vertical Vertical Alignment Visible Width Window X coordinate X display to use X screen to use Y coordinate default:LTR Project-Id-Version: clutter master
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=clutter&keywords=I18N+L10N&component=general
POT-Creation-Date: 2017-05-12 11:29+0100
PO-Revision-Date: 2016-09-11 18:36+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.9
 Әрекеттер Бөлінуі Альфа Атрибуттар Фон түсі Жарықтылығы Ортасында Түс Баған нөмірі Бағандар аралығы Құрамасы Контраст Курсор орны Кідіріс Құрылғылар менеджері Ұзақтығы Түзетуге болады Іске қосулы Ашу Файл аты Қаріп түсі Биіктігі Тұрып қалған Біркелкі Горизонталды туралау Жолды тасымалдау X сервер шақыруларын синхронды қылу Барлық ескертулерді фаталды деп белгілеу Сәйкестелген Астыңғы шет өрісі Сол жақ шет өрісі Оң жақ шет өрісі Үстіңгі шет өрісі Режимі Атауы Объект Бағдары Орналасу Пиксельдер пішімі Ойналуда Орны Барысы Қасиетінің атауы Іске асырылған Жол нөмірі Жолдар аралығы Өлшемі Қайнар көзі Аралық Күйі Мәтін Мәтін бағыты Екі қатар баған арасындағы орын Екі қатар жол арасындағы орын Реңі Үндеу URI Тігінен Вертикалды туралау Көрінетін Ені Терезе X координатасы Қолданылатын X дисплейі Қолданылатын X экраны Y координатасы default:LTR 