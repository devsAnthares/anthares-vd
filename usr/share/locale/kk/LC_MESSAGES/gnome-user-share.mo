��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     ,  2   L  _     *   �     
  .     �   F  �   �  >   k  M   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-04 22:01+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk@li.org>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.4
 %s-ң жеке файлдары %s-ң жеке файлдары, %s жерінде Іске қосулы болса, жеке файлдармен бөлісуді жөнелту Жеке файлдармен бөлісу Бөлісу Ортақ қылудың баптаулары Бұл буманың құрамасымен желі арқылы бөлісу үшін Жеке файлдармен бөлісуді іске қосыңыз. Парольдерді қашан талап ету керек. Мүмкін мәндері "never", "on_write", және "always". Парольдерді қашан талап ету керек бөлісу;файлдар;http;желі;көшіріп алу;жіберу; 