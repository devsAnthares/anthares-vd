��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )    B  p   G
     �
  E   �
  #     #   6  �   Z  !   1  '   S  w   {  C   �  F   7  x   ~     �  8     p   P  U   �  �     I   �  I     G   N  =   �     �  8   �       F   ;     �  @   �  &   �  O     ,   R       +   �  #   �  !   �       !        4     F     ^                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-09 22:24+0000
PO-Revision-Date: 2014-11-14 09:01+0600
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.6.9
 Үнсіз келісім тобы, терезе жасалған кезде оған тағайындалады Индикатор: Әр терезе үшін бөлек топтарды қолдану Пернетақта жаймасы Пернетақта жаймасы Пернетақта жаймасы "%s"
Copyright &#169; X.Org Foundation және XKeyboardConfig үлес қосушылары
Лицензия жөнінде десте метаақпаратын қараңыз Пернетақта моделі Пернетақта опциялары Экзотикалық, сирек қолданылатын жаймалар және опцияларды жүктеу Қосымша баптаулар нәрселерін жүктеу Пернетақта жаймаларын алдын-ала қарау Индикаторларды жаймалар топтарымен бірге сақтау/қалпына келтіру Екіншілік топтар Апплетте жалаушаларды көрсету Апплетте ағымдағы жайманы көрсету үшін жалаушаларды қолдану Топтар аттары орнына жаймалар аттарын көрсету Топтар аттары орнына жаймалар аттарын көрсету (XFree нұсқасы бірнеше жайманы қолдаса ғана) Пернетақтаны алдын-ала қарау, X шегінісі Пернетақтаны алдын-ала қарау, Y шегінісі Пернетақтаны алдын-ала қарау, биіктігі Пернетақтаны алдын-ала қарау, ені Фон түсі Жайма индикаторы үшін фон түсі Қаріптер отбасы Жайма индикаторы үшін қаріптер отбасы Қаріп өлшемі Жайма индикаторы үшін қаріп өлшемі Алдыңғы көрініс түсі Жайма индикаторы үшін алдыңғы көрініс түсі Суретті жүктеу қатесі: %s Белгісіз XKB инициализация қатесі пернетақта жаймасы пернетақта моделі жайма "%s" модель "%s", %s және %s жайма жоқ опциялар жоқ опция "%s" 