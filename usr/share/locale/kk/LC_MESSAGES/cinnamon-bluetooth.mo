��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 x  %     �	  $   �	  &   �	  	   �	     
  #   &
  J   J
     �
     �
     �
     �
  6   �
  Q        m  4   �  9   �  �   �  )   �     �  !   �  7   �       3   %  
   Y     d  )   w  +   �  f   �  [   4     �  H   �     �  6   	  "   @  (   c  ,   �     �  A   �          $     3     K     P     `     v     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-09 17:48+0000
Last-Translator: Murat Káribaı <d2vsd1@mail.ru>
Language-Team: Kazakh <kk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Мекенжайы Әрқашан рұқсат беру Авторлауға сұрау ... %s Bluetooth Bluetooth баптаулары Bluetooth сөндірулі тұр Bluetooth құрылғылық қосқышымен сөндірілген Шолу... Болдырмау Bluetooth баптаулары Байланыс %s құрылғы '%s' қызметін сұрайды %s құрылғы осы компьютермен қосылуды сұрайды Сәйкес келмейді Құрылғылар табуында қателік Осы бір рет кіруіне рұқсат беру Құрылғыны өшірсеңіз, қайта қолдану үшін оны жаңадан баптау керек болады. Пернетақта баптаулары Сәйкес келеді Тышқан баптаулары Тышқан және Тачпад баптаулары Жоқ Bluetooth адаптерлері табылмады Жақсы Жұпталған %s мен жұптасуды растау %s мен жұптастыру сұрауы Өтініш, PIN-код '%s' және құрылғы сәйкес келетінін растаңыз Өтініш, құрылғыда көрсетілген PIN-код-ты енгізіңіз. Бас тарту '%s' құрылғылар тізімінен өшіру керек пе? Құрылғыны өшіру ... құрылғыға файлдарды жіберу Файлдарды жіберу... Жаңа құрылғыны орнату ... жаңа құрылғыны баптау Дыбыс баптаулары Сұранған құрылғы табылмады, '%s' қате Түрі Көрінуі “%s” көрінуі Иә қосылу... ажыратылу... құрылғы өшірулі бет 1 бет 2 