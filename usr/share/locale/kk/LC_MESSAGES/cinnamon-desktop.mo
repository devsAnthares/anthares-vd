��          �   %   �      0  	   1     ;  $   Z  D     <   �                    >     X  %   r  '   �  )   �  '   �  :     +   M  I   y  F   �  �   
  *   �  t     i   x  9   �  �    	   �  M   �  H   �  a   C	  y   �	     
     .
  !   ?
  K   a
  8   �
  Z   �
  \   A  N   �  O   �  s   =  b   �  �     x   �  )  9  @   c  �   �  �   u  j   ,                           
         	                                                                                           %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-09-15 12:34+0600
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kk
Plural-Forms: nplurals=1; plural=0
X-Poedit-Country: KAZAKHSTAN
X-Poedit-Language: Kazakh
 %A, %e %b CRT %d контроллері %s шығысын басқара алмайды CRT %d контроллері айналдыру=%s қолдамайды CRTC %d: %dx%d@%dГц режимін көру, %dx%d@%dГц шығысымен (%d-талабы)
 Терминал табылмады, жұмыс істемеу мүмкін болса да, xterm қолданылады Ноутбук Белгісіз RANDR кеңейтілуі жоқ CRT %d контроллері үшін режимдерді тексеру
 %s шығысына клондау мүмкін емес шығыстар үшін CRT контроллерларын орнату сәтсіз:
%s CRT %d контроллері жөнінде ақпаратты алу мүмкін емес %d шығысы жөнінде ақпаратты алу мүмкін емес экран шеттерінің шегін анықтау мүмкін емес экран ресурстарына қатынау мүмкін емес (CRTC, шығыстар, режимдер) CRT %d контроллері жөнінде ақпаратты орнату мүмкін емес сақталып тұрған экран баптауларының ешқайсысы да белсенді болып тұрған баптауға сәйкес емес таңдалған режимдердің ешқайсысы да мүмкін режиммен үйлеспейді:
%s %s шығысының баптаулары басқа клондалған баптауларынан өзгеше:
режим = %d, жаңа режим = %d
координаттар = (%d, %d), жаңа координаттар = (%d, %d)
айналдыру = %s, жаңа айналдыру = %s %s шығысы %dx%d@%dГц режимін қолдамайды CRTC %d үшін сұралған орналасуы не өлшемі рұқсат етілген шегінен асып тұр: орналасуы=(%d, %d), өлшемі=(%d, %d), максимум=(%d, %d) сұралған виртуалды өлшемі қолжетерлік өлшемге сыймайды: сұралған=(%d, %d), минимум=(%d, %d), максимум=(%d, %d) экранның мүмкін өлшемдер шегін алу кезінде қате орын алды 