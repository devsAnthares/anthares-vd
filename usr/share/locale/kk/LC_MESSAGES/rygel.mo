��    2      �  C   <      H     I  1   W     �     �     �     �     �     �     �     �  !   �           .     >     D     V     f     s     �     �     �     �  	   �     �     �     �     �  	   �       6        C     \     t     z     �     �     �  !   �     �     �     �     �                      5   9     o  $   u  �  �  *   k	  B   �	  .   �	  $   
     -
     :
     M
  
   \
     g
  (   x
  J   �
  G   �
  &   4  
   [     f     �     �     �     �     �     �  '        0  $   @     e     �     �     �     �  F   �  0     &   K     r     x     �     �     �  1   �                >     O     j     {     �  <   �  �   �     f  1   l           )          2                          	         (                    &                          *             %   0   1               ,   !   
       /             -   .      "   $   #                             +            '    Action Failed Add a directory to the list of shared directories Add network interface Add shared directory Album Albums All Artist Artists Display version number Failed to start Rygel service: %s Failed to stop Rygel service: %s Files & Folders Genre Illegal MIME-type Invalid Channel Invalid Name Invalid argument Invalid range Missing filter Music Network Interfaces Networks: No such container Not implemented Not supported Pictures Playlists Port Remove a directory from the list of shared directories Remove network interface Remove shared directory Rygel Rygel Preferences Select folders Title %d Titles UPnP class “%s” not supported UPnP/DLNA Preferences UPnP/DLNA Services Unknown Unknown error. Videos Year Years _Share media through DLNA mediaserver;mediarenderer;share;audio;video;pictures; rygel upnp:createClass value not supported Project-Id-Version: rygel master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=rygel&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-02-21 13:20+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.11
 Әрекет сәтсіз аяқталды Буманы бөлісу бумалар тізіміне қосу Желілік интерфейсті қосу Бөлісу бумасын қосу Альбом Альбомдар Барлығы Әртіс Әртістер Нұсқа нөмірін көрсету Rygel қызметін іске қосу сәтсіз аяқталды: %s Rygel қызметін тоқтату сәтсіз аяқталды: %s Файлдар және бумалар Жанры MIME түрі жарамсыз Арнасы жарамсыз Аты жарамсыз Аргумент қате Аумақ қате Сүзгі жоқ Музыка Желілік интерфейстер Желілер: Ондай контейнер жоқ Іске асырылмаған Қолдау жоқ Суреттер Ойнату тізімдері Порт Буманы бөлісу бумалар тізімінен өшіру Желілік интерфейсті өшіру Бөлісу бумасын өшіру Rygel Rygel баптаулары Бумаларды таңдау Атау %d Атаулар UPnP "%s" класы үшін қолдау жоқ UPnP/DLNA баптаулары UPnP/DLNA қызметтері Белгісіз Белгісіз қате. Видеолар Жыл Жылдар DLNA арқылы мульмимедиямен бөлі_су mediaserver;mediarenderer;share;audio;video;pictures;медиасервер;медиаөңдеуші;бөлісу;аудио;видео;суреттер; rygel upnp:createClass мәніне қолдау жоқ 