��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �     �       	             (     8     K     T  M   `  '   �  a   �  K   8	     �	  8   �	  Z   �	  x   1
  @  �
  @   �  �   ,                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: 
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-23 21:40+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.1.1
 %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRT %d контроллері %s шығысын басқара алмайды CRT %d rotation=%d қолдамайды CRTC %d: %dx%d@%dГц режимін көру, %dx%d@%dГц шығысымен (%d-талабы)
 CRT %d контроллері үшін режимдерді тексеру
 Анықталмаған %s шығысына клондау мүмкін емес шығыстар үшін CRT контроллерларын орнату сәтсіз:
%s таңдалған режимдердің ешқайсысы да мүмкін режиммен үйлеспейді:
%s %s шығысының баптаулары басқа клондалған баптауларынан өзгеше:
бар режимі = %d, жаңа режим = %d
бар координаттар = (%d, %d), жаңа координаттар = (%d, %d)
бар айналдыру = %d, жаңа айналдыру = %d %s шығысы %dx%d@%dГц режимін қолдамайды сұралған виртуалды өлшемі қолжетерлік өлшемге сыймайды: сұралған=(%d, %d), минимум=(%d, %d), максимум=(%d, %d) 