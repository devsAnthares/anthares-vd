��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 r  %     �	     �	     �	  	   �	     �	     �	  6   
     Q
  	   i
  )   s
  	   �
  0   �
  8   �
       )        H  Q   b     �     �     �  .   �     !  !   $     F     I  $   R      w  D   �  6   �       *        H     _     {     �     �     �  A   �     $     ,     9     R     U     e     x     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-29 16:59+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Garantir sempre le accesso Autorisation requeste per%s Bluetooth Configurationes de Bluetooth Le Bluetooth es disactivate Le Bluetooth es disactivate per le commutator hardware Navigar tra le files... Cancellar Configurar le preparationes del Bluetooth Connexion Le dispositivo%s desira acceder al servicio '%s' Le dispositivi %s desira accopularsi con iste computator Non concorda Error in le navigation sur le dispositivo Garantir un vice solmente Si tu remove le dispositivo, tu habera configurar lo de novo ante le uso proxime. Configurationes del claviero Concorda Configurationes del mus Configurationes del mus e del pannello tactile No Necun adaptator Bluetooth trovate OK Copulate Confirmation de accopulamento pro %s Requesta de accopulamento per %s Per favor confirma si le PIN '%s' concorda con illo del dispositivo. Per favor insere le PIN mentionate sur le dispositivo. Rejectar Remover '%s' ex le lista del dispositivos? Remover le dispositivo Inviar files al dispositivo Inviar files... Configurar un nove dispositivo Configurar un nove dispositivo Configurationes del sono Impossibile navigar sur le dispositivo requeste, le error es '%s' Inserer Visibilitate Visibilitate de “%s” Si in connexion... in disconnexion... hardware disactivate pagina 1 pagina 2 