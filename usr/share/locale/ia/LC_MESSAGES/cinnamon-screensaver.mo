��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  ,        ;  ;   G     �  (   �  "   �  E   �  !   .     P  M   _  f   �          0  +   P  .   |  	   �     �  *   �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-06 08:26+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Causa le exito del salva schermo sin fortiar Controlo... Disactivar le salva schermo si active (disvelar le schermo) Contrasigno non correcte Message a monstrar in le schermo blocate Per favor insere tu contrasigno... Querer le intervallo de tempore que le salva schermo ha essite active Querer le stato del salva-schermo Cambiar usator Indica al processo salva schermo currente de blocar le schermo immediatemente Le salva schermo ha essite active pro %d secunda.
 Le salva schermo ha essite active pro %d secundas.
 Le salva-schermo es active
 Le salva-schermo non es active
 Le salva-schermo non es actualmente active
 Activar le salva schermo (obscurar le schermo) Disblocar Version de iste application Tu ha active le clave Fixar le majusculas. 