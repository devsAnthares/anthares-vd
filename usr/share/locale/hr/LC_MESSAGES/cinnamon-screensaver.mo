��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  ,   M     z  G   �     �  6   �       -   7     e     �  C   �  �   �     Y     t  &   �  (   �  
   �     �  !   	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-01 20:04+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Uzrokuje pravilan izlazak iz čuvara zaslona Provjeravanje... Ako je čuvar zaslona aktivan, deaktiviraj ga (obnovi sliku na zaslonu) Neispravna lozinka Poruka koja će biti prikazana na zaključanom zaslonu Upišite svoju lozinku... Određivanje duljine trajanja čuvara zaslona Upit stanja čuvara zaslona Zamijeni korisnika Govori pokrenutom procesu čuvara zaslona da odmah zaključa zaslon Čuvar zaslona je bio aktivan %d sekundu.
 Čuvar zaslona je bio aktivan %d sekunde.
 Čuvar zaslona je bio aktivan %d sekundi.
 Čuvar zaslona je aktivan
 Čuvar zaslona nije aktivan
 Čuvar zaslona trenutno nije aktivan.
 Pokreni čuvara zaslona (zatamni zaslon) Otključaj Inačica ove aplikacije Imate uključenu Caps Lock tipku. 