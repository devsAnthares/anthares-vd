��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 u  %     �	     �	     �	  	   �	     �	     �	  /   
     5
     K
     T
     m
  '   y
  ,   �
     �
     �
     �
  L        _     s          �     �  #   �     �     �     �     �  5     !   G     i     o     �     �     �     �     �     �  <        K  
   Q     \     p     s     �     �  
   �  
   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-17 14:42+0000
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresa Uvijek dopusti pristup %s je zatražio ovjeru Bluetooth Bluetooth postavke Bluetooth je isključen Bluetooth je isključen hardverskim prekidačem Pregledaj datoteke... Odustani Uredi Bluetooth postavke Povezivanje Uređaj %s želi pristupiti '%s' usluzi Uređaj %s se želi uparit s ovim računalom Ne podudara se Greška pregledavanja uređaja Dopusti samo ovaj put Ako uklonite uređaj, prije ponovnog korištenja morate ga ponovo postaviti. Postavke tipkovnice Podudara se Postavke miša Postavke miša i touchpada Ne Nema pronađenih Bluetooth uređaja U redu Upareno Potvrda uparivanja za %s Zahtjev uparivanja za %s Potvrdite podudara li se PIN '%s' s onim na uređaju. Upišite PIN naveden na uređaju. Odbij Ukloni '%s' s popisa uređaja? Ukloni uređaj Pošalji datoteke na uređaj... Pošalji datoteke... Postavi novi uređaj Postavi novi uređaj... Postavke zvuka Zatraženi uređaj se ne može pregledavati, greška je '%s' Vrsta Vidljivost Vidljivost “%s” Da povezivanje... odspajanje... hardver onemogućen stranica 1 stranica 2 