��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9       J     R
     _
     o
     �
  	   �
     �
     �
     �
     �
  #   �
  "   �
  V     O   ^     �     �     �     �  (   �     %  ,   D  )   q  +   �  -   �  K   �  .   A  S   p  W   �  �     /   �  }     x   �  B                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-09-25 15:31+0100
Last-Translator: Launchpad Translations Administrators <rosetta@launchpad.net>
Language-Team: Croatian <trebelnik2@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Poedit 1.5.4
X-Launchpad-Export-Date: 2008-05-28 06:03+0000
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %e %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ne može pokrenuti izlaz %s CRTC %d ne podržava zakretanje=%s CRTC %d: isprobavanje načina prikaza %dx%d@%dHz s izlazom od %dx%d@%dHz (prošlo %d)
 Nemoguće je pronaći terminal, koristi se xterm, čak iako možda neće raditi Prijenosno računalo Zrcalni zasloni Nepoznat RANDR proširenje nije dostupno Isprobavanje načina prikaza za CRTC %d
 nemoguće kloniranje izlaza %s nemoguće pridruživanje CRTC-a na izlaz:
%s nemoguće dobivanje informacija o CRTC %d nemoguće dobivanje informacije o izlazu %d nemoguće dobivanje raspona veličine zaslona nemoguće dobavljanje resursa zaslona (CRTC-ovi, izlazni uređaji, načini) nemoguće postavljanje podešavanja za CRTC %d nijedna od spremljenih podešavanja zaslona se ne podudara s aktivnim podešavanjem nijedan od odabranih načina prikaza nije kompatibilan s mogućim načinima prikaza:
%s izlaz %s nema iste parametre kao drugi klonirani izlaz:
trenutni način = %d, novi način = %d
trenutne koordinate = (%d, %d), nove koordinate = (%d, %d)
trenutno zakretanje = %s, novo zakretanje = %s izlaz %s ne podržava način prikaza %dx%d@%dHz zahtjevan položaj/veličina za CRTC %d je izvan dopuštene granice: položaj=(%d, %d), veličina=(%d, %d), maksimum=(%d, %d) potrebna virtualna veličina ne odgovara dostupnoj veličini: potrebno=(%d, %d), minimalno=(%d, %d), maksimalno=(%d, %d) neobrađena X greška prilikom dobivanja raspona veličine zaslona 