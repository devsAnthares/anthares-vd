��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     E     _  5        �  
   �     �  U   �  [   F     �  -   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-04-08 19:12+0200
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-09-15 12:11+0000
X-Generator: Poedit 1.8.7.1
 Javni podaci korisnika %s Javni podaci korisnika %s na %s Pokreni dijeljenje osobnih datoteka ako je omogućeno Dijeljenje osobnih datoteka Dijeljenje Postavke dijeljenja Uključi dijeljenje osobnih datoteka kako bi dijelili sadržaj ove mape putem mreže. Kada upitati za lozinku. Moguće vrijednosti su "nikad", "prilikom zapisivanja" i "uvijek". Kada zahtijevati lozinku dijeli;datoteka;http;mreža;kopiraj;pošalji; 