��          �            x     y     �     �  l   �            )   4     ^     j     �  Q   �     �       
        )  3  <     p     �     �  m   �            )   ,     V     b     y  Q   �     �       
        !        
                                                          	             %s's public files Always File Sharing Preferences If this is true, the Public directory in the users home directory will be shared when the user is logged in. Never Personal File Sharing Preferences for sharing of personal files Share Files Share Public directory Share Public files on network When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords When writing files _Password: _Require password: Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-02-04 20:13-0400
Last-Translator: Adam Weinberger <adamw@gnome.org>
Language-Team: Canadian English <adamw@gnome.org>
Language: en_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s's public files Always File Sharing Preferences If this is true, the Public directory in the user's home directory will be shared when the user is logged in. Never Personal File Sharing Preferences for sharing of personal files Share Files Share Public directory Share Public files on network When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords When writing files _Password: _Require password: 