��          �            h     i     o     �  .   �     �     �  B   �       /         P  O   b  !   �     �     �  �  �     �     �     �  /   �     �     �  D   �     =  0   D     u  P   �  "   �     �                        	                                                         
    Color Color management settings Colorspace:  Create a color profile for the selected device Default Disabled Each device needs an up to date color profile to be color managed. Layout No devices supporting color management detected Show help options This device does not have a profile suitable for whole-screen color correction. This device is not color managed. Unknown _Import Project-Id-Version: cinnamon-control-center
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-11-03 19:07+0000
Last-Translator: Corbin <RavetcoFX@gmail.com>
Language-Team: Canadian English <adamw@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Colour Colour management settings Colourspace:  Create a colour profile for the selected device Default Disabled Each device needs an up to date colour profile to be colour managed. Layout No devices supporting colour management detected Show help options This device does not have a profile suitable for whole-screen colour correction. This device is not colour managed. Unknown _Import 