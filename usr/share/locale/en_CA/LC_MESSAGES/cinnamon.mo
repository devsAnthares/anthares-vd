��          �      l      �  
   �     �       A     +   T  U   �     �     �     �     �     �          *     6     ;     J     Q     a     i  (   z  �  �  
   a     l     �  B   �  ,   �  W        Z     a     p     y          �     �     �     �     �     �     �     �  0   �                          	                  
                                                      %1$s: %2$s Add or remove users and groups Cancel Click the button to select a new background color for this applet Click the button to select a new text color Click the eyedropper, then click a color anywhere on your screen to select that color Color Control Center Disabled Eject Execution of '%s' failed: Failed to launch '%s' File System Home Panel settings Search System Settings Unknown Users and Groups color, profile, display, printer, output Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-26 02:18+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: English (Canada) <en_CA@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
 %1$s: %2$s Add or remove users and groups Cancel Click the button to select a new background colour for this applet Click the button to select a new text colour Click the eyedropper, then click a colour anywhere on your screen to select that colour Colour Control Centre Disabled Eject Execution of '%s' failed: Failed to launch '%s' File System Home Panel settings Search System Settings Unknown Users and Groups colour, color, profile, display, printer, output 