��          L      |       �   `   �      
     %  )   B     l  �  �    �  @   �  I   �  T   /  5   �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: gu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-08-26 12:24+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: gu_IN <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 સ્ક્રીનસેવર %d સેકંડ માટે સક્રિય કરી દેવામાં આવ્યુ છે.
 સ્ક્રીનસેવર %d સેકંડો માટે સક્રિય કરી દેવામાં આવ્યુ છે.
 સ્ક્રીનસેવર એ સક્રિય છે
 સ્ક્રીનસેવર એ નિષ્ક્રિય છે
 સ્ક્રીનસેવર એ હમણાં સક્રિય નથી.
 તમારી Caps Lock કી ચાલુ છે. 