��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S    w  )   {  3   �  �   �  2   l     �  .   �  �   �  �   �  B   q  [   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share.master.gu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-07-11 22:31+0200
Last-Translator: વિશાલ ભલાણી <vishalbhalani89@gmail.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n!=1);
 %s ની જાહેર ફાઈલો %s ની %s પર જાહેર ફાઈલો વ્યક્તિગત ફાઇલ વહેંચણીને પ્રકાશિત કરો જો સક્રિય હોય તો ખાનગી ફાઈલ વહેંચણી ભાગીદારી ભાગીદારી સુયોજનો આ ફોલ્ડર સમાવિષ્ટો નેટવર્ક પર શેર કરવા માટે અંગત ફાઈલ શેરિંગ ચાલુ કરો. પાસવર્ડો માટે ક્યારે પૂછવું. "ક્યારેય નહિં", "લખવા પર", અને "હંમેશા" શક્ય કિંમતો છે. ક્યારે પાસવર્ડો જરૂરી છે વહેંચવુ;ફાઇલો;http;નેટવર્ક;નકલ;મોકલો; 