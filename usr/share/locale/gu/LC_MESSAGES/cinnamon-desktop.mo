��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  @        a	     n	     ~	     �	     �	     �	     �	     �	  \   �	  G   "
  �   j
  �        �  /   �       E   +  h   q  S   �  ]   .  T   �  b   �  r   D  �   �  t   L  �   �  �   c  �    ^   �  �   �  �   �  �   �             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: gu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-18 12:05+0530
Last-Translator: 
Language-Team: gu_IN <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n!=1);




X-Generator: Lokalize 1.0
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d એ આઉટપુટ %s ને ડ્રાઇવ કરી શકતુ નથી CRTC %d રોટેશનને આધાર આપતુ નથી=%s CRTC %d: %dx%d@%dHz પર આઉટપુટ સાથે સ્થિતિ %dx%d@%dHz પ્રયત્ન કરી રહ્યા છે (%d ને પસાર કરો)
 ટર્મિનલ ને શોધી શકાતુ નથી, xterm ને વાપરી રહ્યા છે, જો તે કામ ન કરી શકતુ હોય લેપટોપ મીરર થયેલ દર્શાવો અજ્ઞાત RANDR એક્સટેન્સન એ હાલમાં નથી CRTC %d માટે સ્થિતિઓનો પ્રયત્ન કરી રહ્યા છે
 આઉટપુટ %s માટે ક્લોન કરી શકતા નથી આઉટપુટ માટે CRTCs ને સોંપી શક્યા નહિં:
%s CRTC %d વિશે જાણકારી મેળવી શકાતી નથી આઉટપુટ %d વિશે જાણકારી મેળવી શકાતી નથી સ્ક્રીન માપો નાં વિસ્તારને મેળવી શકાતુ નથી સ્ક્રીન સ્ત્રોતોને મેળવી શકાતુ નથી (CRTCs, આઉટપુટો, સ્થિતિઓ) CRTC %d માટે રૂપરેખાંકન ને સુયોજિત કરી શકાતુ નથી સંગ્રહ થયેલ રૂપરેખાંકનો એ સક્રિય રૂપરેખાંકન ને બંધબેસતા નથી પસંદ થયેલ સ્થિતિઓમાંથી કોઇપણ શક્ય સ્થિતિઓ સાથે સુસંગત ન હતી:
%s આઉટપુટ %s પાસે બીજી ક્લોન થયેલ આઉટપુટ તરીકે સરખા પરિમાણો નથી:
હાલની સ્થિતિ = %d, નવી સ્થિતિ = %d
હાલનો નિયામક = (%d, %d), નવો નિયામક = (%d, %d)
હાલનું રોટેશન = %s, નવુ રોટેશન = %s આઉટપુટ %s સ્થિતિ %dx%d@%dHz ને આધાર આપતુ નથી CRTC %d માટે સૂચેલ સ્થાન/માપ એ પરવાનગી આપેલ મર્યાદા કરતા બહાર છે: સ્થાન=(%d, %d), માપ=(%d, %d), મહત્તમ=(%d, %d) જરૂરિયાત વર્ચ્યુઅલ માપ એ ઉપલ્બધ માપ ને બંધબેસતુ નથી: સૂચેલ=(%d, %d), ન્યૂનત્તમ=(%d, %d), મહત્તમ=(%d, %d) ન સંભાળાયેલ X બૂલ જ્યારે સ્ક્રીન માપોનો વિસ્તાર મેળવી રહ્યા છે 