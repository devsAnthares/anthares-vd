��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     =  J     �	     �	     �	     �	     �	     �	     �	     �	     �	  l   �	  ;   b
  �   �
  n   )     �  ?   �     �  -     O   5  Z   �  m   �  \   N  o   �  c     �     M     �   `  �   0  �  �  ]   �  
  �  �      �   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-07 15:21+0700
Last-Translator: Theppitak Karoonboonyanan <thep@linux.thai.net>
Language-Team: Thai <thai-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
 %A %d %B, %R %A %d %B, %R:%S %A %d %B, %l:%M %p %A %d %B, %l:%M:%S %p %A %e %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ไม่สามารถสั่งการอุปกรณ์แสดงผล %s ได้ CTRC %d ไม่รองรับมุมหมุน=%s CRTC %d: กำลังลองใช้โหมด %dx%d@%dHz โดยแสดงผลที่ %dx%d@%dHz (รอบที่ %d)
 ไม่พบเทอร์มินัล จะใช้ xterm แม้อาจไม่ได้ผล แล็ปท็อป จอแสดงผลแสดงเหมือนกัน ไม่ทราบ ไม่มีส่วนขยาย RANDR กำลังลองโหมดต่างๆ สำหรับ CRTC %d
 ไม่สามารถผลิตซ้ำอุปกรณ์แสดงผล %s ไม่สามารถกำหนด CRTC ให้กับอุปกรณ์แสดงผล:
%s ไม่สามารถอ่านข้อมูลเกี่ยวกับ CRTC %d ไม่สามารถอ่านข้อมูลเกี่ยวกับเอาต์พุต %d ไม่สามารถอ่านค่าช่วงของขนาดหน้าจอ ไม่สามารถอ่านค่าทรัพยากรของหน้าจอ (CRTC, เอาต์พุต, โหมด) ไม่สามารถกำหนดค่าสำหรับ CRTC %d ไม่มีค่าตั้งดิสเพลย์ที่บันทึกไว้รายการใดที่ตรงกับค่าตั้งที่ใช้งานอยู่ โหมดที่เลือกไว้นั้น ไม่มีโหมดใดเข้ากันได้กับโหมดที่เป็นไปได้:
%s อุปกรณ์แสดงผล %s มีพารามิเตอร์ไม่ตรงกับอุปกรณ์แสดงผลอีกอันที่ผลิตซ้ำออกไป:
โหมดเดิม = %d, โหมดใหม่ = %d
พิกัดเดิม = (%d, %d), พิกัดใหม่ = (%d, %d)
มุมหมุนเดิม = %s, มุมหมุนใหม่ = %s อุปกรณ์แสดงผล %s ไม่รองรับโหมด %dx%d@%dHz ตำแหน่ง/ขนาดสำหรับ CRTC %d ที่ร้องขอ มีค่าอยู่นอกขีดจำกัดที่กำหนด: ตำแหน่ง=(%d, %d), ขนาด=(%d, %d), ค่าสูงสุด=(%d, %d) ขนาดเสมือนที่ต้องการไม่เข้ากับขนาดที่มี: ที่ร้องขอ=(%d, %d), ค่าต่ำสุด=(%d, %d), ค่าสูงสุด=(%d, %d) เกิดข้อผิดพลาดของ X ที่ไม่ได้เตรียมรับมือไว้ขณะอ่านค่าช่วงของขนาดหน้าจอ 