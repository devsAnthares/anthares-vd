��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  
  r  �   }  '     �   -  6     i   8  E   �  �   �  `   p	     �	  �   �	  n   �
  U   ,  R   �  d   �  �   :     �  3   �  2                       	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver 0.0.21
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-03-26 15:36+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 ทำให้โปรแกรมรักษาหน้าจอจบการทำงานอย่างนุ่มนวล กำลังตรวจสอบ... ถ้าโปรแกรมรักษาหน้าจอทำงานอยู่ ให้หยุดทำงาน (เลิกการทำให้หน้าจอว่างเปล่า) รหัสผ่านไม่ถูกต้อง ข้อความที่ต้องการแสดงผลในหน้าจอล็อก โปรดป้อนรหัสผ่านของคุณ... สอบถามระยะเวลาที่โปรแกรมรักษาหน้าจอได้ทำงานมา สอบถามสถานะของโปรแกรมรักษาหน้าจอ สลับผู้ใช้ บอกกระบวนการของโปรแกรมรักษาหน้าจอที่กำลังทำงานอยู่ให้ล็อกหน้าจอทันที โปรแกรมรักษาหน้าจอทำงานมาแล้ว %d วินาที
 โปรแกรมรักษาหน้าจอกำลังทำงาน
 โปรแกรมรักษาหน้าจอกำลังหลับ
 โปรแกรมรักษาหน้าจอไม่ได้ทำงานอยู่
 เปิดโปรแกรมรักษาหน้าจอ (ทำให้หน้าจอว่างเปล่า) ปลดล็อก รุ่นของโปรแกรมนี้ ปุ่ม Caps Lock ถูกกดอยู่ 