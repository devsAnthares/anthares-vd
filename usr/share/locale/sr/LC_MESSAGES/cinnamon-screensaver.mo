��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  E   �     ?  o   W     �  ]   �  )   C  [   m  7   �  !   	  t   #	  �   �	  .   L
  .   {
  D   �
  D   �
     4  0   E  0   v                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-01-05 15:28+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: српски <gnome-sr@googlegroups.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: sr
 Доводи до лепог изласка чувара екрана Проверавам... Ако је чувар екрана покренут гаси га (поништава празан екран) Нетачна лозинка Порука која ће бити приказана на закључаном екрану Унесите вашу лозинку... Пропитује колико је дуго чувар екрана био активан Пропитује стање чувара екрана Промени корисника Говори покренутом процесу чувара екрана да одмах закључа екран Чувар екрана је радио %d секунду.
 Чувар екрана је радио %d секунде.
 Чувар екрана је радио %d секунди.
 Чувар екрана је покренут
 Чувар екрана је искључен
 Чувар екрана је тренутно заустављен.
 Укључује чувара екрана (празан екран) Откључај Приказује издање програма Укључили сте велика слова. 