��    	      d      �       �      �      �      �      �      	  &     1   D     v  C       �     �     �  '   �  5     K   H  N   �     �                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-10-30 11:20+0100
Last-Translator: Марко М. Костић <marko.m.kostic@gmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
X-Generator: Poedit 1.8.8
 Звучни ЦД Блу-реј ДВД Дигитална телевизија Нисам успео да прикачим „%s“. Није убачен носач података у уређај „%s“. Проверите да ли је диск присутан у уређају. Видео ЦД 