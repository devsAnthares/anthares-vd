��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  C  �                 	   &     0     ?     Q     c  	   x     �     �     �     �     �     �     �  @   �  <   '	  w   d	  9   �	     
  :   +
  F   f
  w   �
  N  %  ?   t  �   �                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-09-29 11:37+0200
Last-Translator: Марко М. Костић <marko.m.kostic@gmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
X-Generator: Poedit 2.0.6
 %R %R:%S %A, %R %A, %R:%S %A, %e. %b, %R %A, %e. %b, %R:%S %A, %e. %b, %l:%M %A, %e. %b, %l:%M:%S %A, %l:%M %A, %l:%M:%S %A, %e. %b, %R %A, %e. %b, %R:%S %A, %e. %b, %l:%M %A, %e. %b, %l:%M:%S %l:%M %l:%M:%S ЦРТЦ „%d“ не може добије излаз „%s“ ЦРТЦ „%d“ не подржава ротацију =%d ЦРТЦ „%d“: испробавам режим %dx%d@%dHz без излаза на %dx%d@%dHz (%d. покушај)
 Испробавам режиме за ЦРТЦ „%d“
 Неодређено не могу да клонирам у излаз „%s“ не могу да придружим ЦРТЦ-е излазима:
%s ни један од изабраних режима није сагласан са могућим режимима:
%s излаз „%s“ нема исте параметре као други клонирани уређај:
постојећи режим = %d, нови режим = %d
постојеће координате = (%d, %d), нове координате = (%d, %d)
постојећа ротација = %d, нова ротација = %d излаз „%s“ не подржава режим %dx%d@%dHz захтевана виртуелна величина је изван доступне: захтевана=(%d, %d), најмања=(%d, %d), највећа=(%d, %d) 