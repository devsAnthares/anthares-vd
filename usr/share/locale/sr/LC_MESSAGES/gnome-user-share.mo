��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  b  w  7   �  V     X   i  *   �     �  !   �  �     �   �  ,   ~  d   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-22 15:32+0200
Last-Translator: Марко М. Костић (Marko M. Kostić) <marko.m.kostic@gmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
X-Generator: Poedit 1.8.4
 Јавне датотеке корисника „%s“ Јавне датотеке корисника „%s“ на рачунару „%s“ Покрени дељење личних датотека ако је омогућено Делење личних датотека Дељење Подешавања дељења Укључите дељење личних датотека да бисте поделили садржај ове фасцикле преко мреже. Одређује када се захтева унос лозинке. Могуће вредности су „never“ (никада), „on_write“ (при упису) и „always“ (увек). Када да се траже лозинке дели;дељење;датотеке;хттп;мрежа;умножи;копирај;пошаљи; 