��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	  &   �	  $   
     +
  #   8
  "   \
  H   
  &   �
     �
  4   �
     1  D   :  N        �  I   �  '   *  �   R  #   �     	       6   1     h  I   m     �     �  )   �  4   �  _   /  *   �  
   �  >   �       2         Q  "   r  %   �     �  ]   �  
   5     @      S     t     y     �  $   �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-31 04:10+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <gnom@prevod.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: sr
 Адреса Увек дозволи приступ %s захтева овлашћење Блутут Подешавања Блутута Блутут је искључен Блутут је искључен физичким прекидачем Прегледај датотеке... Откажи Измените подешавања Блутута Веза Уређај %s жели да приступи услузи „%s“ Уређај %s жели да се упари са овим рачунаром Није исти Десила се грешка при прегледању уређаја Дозволи само овај пут Уколико уклоните уређај, мораћете поново да га подесите пре следећег коришћења. Поставке тастатуре Исти је Поставке миша Поставке миша и додирне табле Не Нису пронађени примопредајници Блутута У реду Упарен Потврда упаривања са %s Захтев за упаривањем за „%s“ Потврдите да ли је ПИН „%s“ исти као онај на уређају. Унесите ПИН са уређаја. Одбиј Да уклоним „%s“ са списка уређаја? Уклони уређај Пошаљи датотеке на уређај... Пошаљи датотеке... Подеси нови уређај Подеси нови уређај... Поставке звука Жељени уређај се не може прегледати, грешка је „%s“ Врста Видљивост Видљивост за „%s“ Да повезујем се... прекидам везу... физички је искључен страна 1 страна 2 