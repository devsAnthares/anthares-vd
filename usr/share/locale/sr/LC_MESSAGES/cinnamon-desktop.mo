��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     %
     4
     F
     X
  
   l
     w
     z
     �
     �
  @   �
  =   �
  w     w   �     �           ,  7   =  9   u  :   �  F   �  R   1  I   �  M   �  g     Z   �  �   �  w   m  N  �  ?   4  �   t  �   =  �   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-05 10:39+0200
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <gnom@prevod.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
 %A, %e. %B, %R %A, %e. %B, %R:%S %A, %e. %B, %l:%M %A, %e. %B, %l:%M:S %A, %e. %B %R %R:%S %l:%M %l:%M:%S ЦРТЦ „%d“ не може добије излаз „%s“ ЦРТЦ „%d“ не подржава ротацију = %s ЦРТЦ „%d“: испробавам режим %dx%d@%dHz без излаза на %dx%d@%dHz (%d. покушај)
 Не могу да нађем терминал, користим икстерм, чак и уколико не ради Лаптоп Екрани у огледалу Непознат Није присутно РАНДР проширење Испробавам режиме за ЦРТЦ „%d“
 не могу да клонирам у излаз „%s“ не могу да придружим ЦРТЦ-е излазима:
%s не могу да добијем податке о ЦРТЦ „%d“ екрану не могу да добијем податке о излазу „%d“ не могу да добијем опсег величина за екран не могу да добијем ресурсе за екран (ЦРТЦ, излази, режими) не могу да поставим подешавања за ЦРТЦ „%d“ екран ни једно од сачуваних подешавања екрана се не поклапа са текућим подешавањем ни један од изабраних режима није сагласан са могућим режимима:
%s излаз „%s“ нема исте параметре као други клонирани уређај:
постојећи режим = %d, нови режим = %d
постојеће координате = (%d, %d), нове координате = (%d, %d)
постојећа ротација = %s, нова ротација = %s излаз „%s“ не подржава режим %dx%d@%dHz захтеван положај/величина за ЦРТЦ %d је изван дозвољеног ограничења: положај=(%d, %d), величина=(%d, %d), највише=(%d, %d) захтевана виртуелна величина је изван доступне: захтевана=(%d, %d), најмања=(%d, %d), највећа=(%d, %d) неутврђена грешка у X серверу приликом добављања опсега величине екрана 