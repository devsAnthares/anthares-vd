��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     
     
     %
  	   ;
     E
     H
     N
     T
  $   ]
     �
  R   �
  B   �
     6     P  
   b     m     �  !   �  &   �  (   �  -     %   ?  <   e  *   �  O   �  I     �   g  +   Z  k   �  ]   �  3   P                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: lt
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-07 20:34+0300
Last-Translator: Aurimas Černius <aurisc4@gmail.com>
Language-Team: Lithuanian <gnome-lt@lists.akl.lt>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.6.1
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %H:%M %H:%M:%S CRTC %d negali valdyti išvesties %s CRTC %d nepalaiko pasukimo=%s CRTC %d: bandoma %dx%d@%dHz veiksena su išvestimi esant %dx%d@%dHz (%d bandymas)
 Nepavyko rasti terminalo, naudojamas xterm, nors jis gali neveikti Nešiojamasis kompiuteris Dubliuoti ekranai Nežinomas nėra RANDR plėtinio Bandomos CRTC %d veiksenos
 nepavyko klonuoti į išvestį %s nepavyko priskirti CRTC išvestims:
%s nepavyko gauti informacijos apie CRTC %d nepavyko gauti informacijos apie išvestį %d nepavyko gauti ekrano dydžių ruožo nepavyko gauti ekrano resursų (CRTC, išvedimų, veiksenų) nepavyko nustatyti CRTC %d konfigūracijos jokia iš įrašytų ekranų konfigūracijų neatitiko aktyvios konfigūracijos nei viena pasirinktų veiksenų nesuderinama su galimomis veiksenomis:
%s išvestis %s neturi tų pačių parametrų kaip kita klonuota išvestis:
dabartinė veiksena = %d, naujoji veiksena = %d
dabartinės koordinatės = (%d, %d), naujosios koordinatės = (%d, %d)
dabartinis pasukimas = %s, naujasis pasukimas = %s išvestis %s nepalaiko %dx%d@%dHz veiksenos prašoma pozicija/dydis CRTC %d už leistinos ribos: pozicija=(%d, %d), dydis=(%d, %d), maksimumas=(%d, %d) prašomas virtualus dydis netelpa: prašoma=(%d, %d), minimalus=(%d, %d), maksimalus=(%d, %d) neapdorota X klaida gaunant ekrano dydžių ruožą 