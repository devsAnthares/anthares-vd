��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  W  r  =   �       L        b  ,   {     �  4   �  &   �     #  @   8  �   y      	     	  '   >	  0   f	  	   �	     �	     �	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-06-22 20:38+0000
Last-Translator: Moo <hazap@hotmail.com>
Language-Team: Lithuanian <komp_lt@konferencijos.lt>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: lt
 Padaro, kad ekrano užsklanda baigtų savo darbą sklandžiai Tikrinama... Jei ekrano užsklanda aktyvi, ją deaktyvuoti (išjungti ekrano užtemdymą) Neteisingas slaptažodis Žinutė, kuri bus rodoma užrakinimo ekrane Įveskite savo slaptažodį... Užklausti, kiek laiko ekrano užsklanda buvo aktyvi Užklausti ekrano užsklandos būseną Perjungti naudotoją Nurodo ekrano užsklandos procesui nedelsiant užrakinti ekraną Ekrano užsklanda buvo aktyvi %d sekundę.
 Ekrano užsklanda buvo aktyvi %d sekundes.
 Ekrano užsklanda buvo aktyvi %d sekundžių.
 Ekrano užsklanda yra aktyvi
 Ekrano užsklanda yra neaktyvi
 Šiuo metu ekrano užsklanda neaktyvi.
 Įjungti ekrano užsklandą (užtemdyti ekraną) Atrakinti Šios programos versija Įjungtas Caps Lock klavišas. 