��          �   %   �      @     A  /   U     �     �  6   �  N   �  E   ?  @   �     �     �      �  !     %   ?  )   e      �  c   �  -     D   B     �     �  !   �  B   �  @   &     g  �  �     `  0   i     �     �  !   �  ?   �  .   	  (   E	     n	      �	  #   �	  "   �	  %   �	  (   
  ^   C
  X   �
  6   �
  F   2  \   y  $   �     �  G     -   ^     �                                                               	              
                                                 @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian @info:shell command-line optionCreate global database @info:shell command-line optionDisable incremental update, re-read everything @info:shell command-line optionPerform menu generation test run only @info:shell command-line optionTrack menu id for debug purposes Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2014-11-11 00:26+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 Autorius Autorinės teisės 1999-2014, KDE programuotojai David Faure Waldo Bastian Sukurti globalią duomenų bazę. Išjungti papildymų atnaujinimus, iš naujo viską perskaityti Atlikti meniu generavimo testą tik paleidimui Išriktavimo tikslais nustatyti meniu id Nepavyko paleisti naršyklės Nepavyko paleisti pašto kliento Nepavyko paleisti terminalo kliento Nepavyko paleisti naršyklės:

%1 Nepavyko paleisti pašto kliento:

%1 Nepavyko paleisti terminalo kliento:

%1 rch@richard.eu.org, dgvirtual@akl.lt, gintautas@miselis.lt, stikonas@gmail.com, liudas@akmc.lt Klaida, paleidžiant %1. Arba KLauncher nebeveikia arba jam nepavyko paleisti programos. Funkcija turi būti kviečiama iš pagrindinės gijos. Nepavyksta pasiekti KLauncher per D-Bus. Klaida kreipiantis į %1:
%2
 Ričardas Čepas, Donatas Glodenis, Gintautas Miselis, Andrius Štikonas, Liudas Ališauskas Nėra tarnybos, įgyvendinančios %1 Pateikta tarnyba negalioja Paslauga „%1“ neteikia bibliotekų arba trūksta bibliotekos rakto. Atstatomas sistemos konfigūracijos krepšys. KBuildSycoca 