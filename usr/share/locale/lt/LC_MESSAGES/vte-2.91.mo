��          L      |       �   /   �      �   =   �   +   5     a  �  i  V   A  9   �  D   �  *        B                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: lt
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-11 18:19+0300
Last-Translator: Aurimas Černius <aurisc4@gmail.com>
Language-Team: Lietuvių <gnome-lt@lists.akl.lt>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Gtranslator 2.91.7
 Įvyko klaida (%s) keičiant duomenis gautus iš antrinio proceso, nutraukiame darbą. Įvyko klaida skaitant duomenis iš antrinio proceso: %s. GNUTLS nėra įjungta; duomenys bus rašomi į diską nešifruojant! Nepavyko perversti simbolių iš %s į %s. ĮSPĖJIMAS 