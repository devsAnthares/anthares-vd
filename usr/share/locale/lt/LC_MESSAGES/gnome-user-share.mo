��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  %  w     �     �  5   �          !     -  \   C  _   �        /                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share 1.0
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-06 22:20+0300
Last-Translator: Aurimas Černius <aurisc4@gmail.com>
Language-Team: Lietuvių <gnome-lt@lists.akl.lt>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Gtranslator 2.91.7
 Viešieji failai (%s) Viešieji failai (%s, %s) Paleisti asmeninių failų viešinimą, jei įjungtas Asmeninių failų bendrinimas Dalinimasis Dalinimosi nustatymai Įjungti dalinimąsi asmeniniais failais, kad šio aplanko turiniu būtų dalinamasi tinkle. Kada prašoma slaptažodžių. Galimos reikšmės: „never“, „on_write“ ir „always“. Kada reikia slaptažodžių dalintis;failai;http;tinklas;kopijuoti;siųsti; 