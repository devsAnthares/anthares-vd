��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 s  %     �	     �	  +   �	  	   �	     �	     
  -   
     J
  
   `
  !   k
     �
  .   �
  8   �
  
             *  V   A     �     �     �  *   �     �     �       	        $     @  F   \  *   �     �  -   �               :     L     h     �  <   �     �  	   �     �     �     �            
   8  
   C                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-22 09:19+0000
Last-Translator: Moo <hazap@hotmail.com>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresas Visada leisti prieigą Prieigos teisių suteikimo užklausa iš %s Bluetooth Bluetooth nustatymai Bluetooth yra išjungtas Bluetooth yra išjungtas aparatiniu jungikliu Naršyti po failus... Atsisakyti Keisti „Bluetooth“ nustatymus Prisijungimas Įrenginys %s nori prieigos prie "%s" tarnybos Įrenginys %s nori būti suporuotas su šiuo kompiuteriu Neatitinka Klaida, naršant įrenginį Leisti tik šį kartą Jei pašalinsite šį įrenginį, kitą kartą jį vėl turėsite nustatyti iš naujo. Klaviatūros nustatymai Atitinka Pelės nustatymai Pelės ir jutiklinio kilimėlio nustatymai Ne Nėra Bluetooth adapterių Gerai Suporuota %s suporavimo patvirtinimas Suporavimo užklausa iš %s Prašome patvirtinti ar PIN "%s", atitinka tą, kuris yra įrenginyje. Prašome įvesti įrenginyje nurodyta PIN. Atmesti Pašalinti „%s“ iš įrenginių sąrašo? Pašalinti įrenginį Siųsti failus į įrenginį... Siųsti failus... Nustatyti naują įrenginį Nustatyti naują įrenginį... Garso nustatymai Užklaustas įrenginys negali būti naršomas, klaida - "%s" Tipas Matomumas „%s“ matomumas Taip jungiamasi... atsijungiama... aparatinė įranga išjungta 1 puslapis 2 puslapis 