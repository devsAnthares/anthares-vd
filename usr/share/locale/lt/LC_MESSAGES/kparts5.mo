��          �   %   �      `     a  :   i     �     �  =   �          *     G     [     t     �  6   �     �     �  #   �           >     F     T     d     �     �  ;   �  V   �  G   %     m  �  v  	   Q  0   [     �     �  .   �     �  	   �     �     �            #   )     M     U  (   i  ^   �     �     �     		  \   	     y	     �	  9   �	  V   �	  I    
     j
                                                  
                                                                   	       &Search <qt>Do you want to search the Internet for <b>%1</b>?</qt> @action:inmenuOpen &with %1 @infoOpen '%1'? @info:whatsthisThis is the file name suggested by the server @label File nameName: %1 @label Type of fileType: %1 @label:button&Open @label:button&Open with @label:button&Open with %1 @label:button&Open with... @label:checkboxRemember action for files of this type Accept Close Document Do you really want to execute '%1'? EMAIL OF TRANSLATORSYour emails Execute Execute File? Internet Search NAME OF TRANSLATORSYour names Reject Save As The Download Manager (%1) could not be found in your $PATH  The document "%1" has been modified.
Do you want to save your changes or discard them? Try to reinstall it  

The integration with Konqueror will be disabled. Untitled Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2014-03-16 20:46+0200
Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 1.5
 &Ieškoti <qt>Ar norite ieškoti Internete <b>%1</b>?</qt> Atverti &su %1 Atverti „%1“? Tai yra serverio pasiūlytas failo pavadinimas Pavadinimas: %1 Tipas: %1 &Atverti &Atverti su &Atverti su %1 &Atverti su... Prisiminti šio tipo veiksmo failus Sutinku Užverti dokumentą Ar Jūs tikrai norite vykdyti „%1“?  rch@richard.eu.org, dgvirtual@akl.lt, gintautas@miselis.lt, stikonas@gmail.com, liudas@akmc.lt Vykdyti Vykdyti failą? Interneto paieška Ričardas Čepas, Donatas Glodenis, Gintautas Miselis, Andrius Štikonas, Liudas Ališauskas Atmetu Įrašyti kaip Atsisiuntimo tvarkyklė (%1) nerasta Jūsų kelyje $PATH. Dokumentas „%1“ buvo pakeistas.
Ar norite jį įrašyti, ar -- atmesti pakeitimus? Pamėginkite įdiegti iš naujo 

Integracija su Konqueror bus išjungta. Be pavadinimo 