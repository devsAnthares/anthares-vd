��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  E  �  1   �
     �
  )        7     Q  �   k     �       ?   "  +   b  '   �  >   �     �       <     1   [  j   �  !   �  !        <     [     x  &   �     �  )   �     �  (   �       0   2  )   c  	   �     �     �     �  5   �          -     ?  ,   Q     ~     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-07-28 21:05+0000
PO-Revision-Date: 2016-08-15 13:33+0300
Last-Translator: Aurimas Černius <aurisc4@gmail.com>
Language-Team: Lietuvių <gnome-lt@lists.akl.lt>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Gtranslator 2.91.7
 Standartinė grupė, priskiriama naujiems langams Indikatorius: Valdyti grupes kiekvienam langui atskirai Klaviatūros išdėstymas Klaviatūros išdėstymas Klaviatūros išdėstymas "%s"
Autorinės teisės &#169; X.Org Foundation ir XKeyboardConfig kontributoriai
Licencija paketo metaduomenyse Klaviatūros modelis Klaviatūros parinktys Įkelti egzotiškus, retai naudojamus išdėstymus ir parinktis Įkelti papildomus konfigūravimo elementus Peržiūrėti klaviatūros išdėstymas Įrašymo/skaitymo indikatoriai kartu su išdėstymo grupėmis Antrinės grupės Rodyti savybes įtaise Rodyti savybes, rodančias pasirinktą išdėstymą, įtaise Rodyti išdėstymų vardus vietoje grupių vardų Rodyti išdėstymų vardus vietoje grupių vardų (tik XFree versijoms, palaikančioms kelis išdėstymus) Klaviatūros vaizdas, X poslinkis Klaviatūros vaizdas, Y poslinkis Klaviatūros vaizdas, aukštis Klaviatūros vaizdas, plotis Fono spalva Išdėstymų indikatoriaus fono spalva Šrifto šeima Šrifto šeima išsidėstymo indikatoriui Šrifto dydis Šrifto dydis išsidėstymo indikatoriui Priekinio plano spalva Priekinio plano spalva išdėstymų indikatoriui Įvyko klaida įkeliant paveikslėlį: %s Nežinoma XKB inicializavimo klaida klaviatūros išdėstymas klaviatūros modelis išdėstymas "%s" išdėstymai "%s" išdėstymai "%s" modelis "%s", %s ir %s nėra išdėstymo nėra parinkčių parinktis "%s" parinktys "%s" parinktys "%s" preferences-desktop-keyboard 