��    t      �  �   \      �	     �	  	   �	     �	     �	     
     	
     
     !
     '
     /
     B
     K
     W
     d
  	   m
     w
     �
  
   �
     �
     �
     �
  
   �
     �
     �
     �
  	   �
     �
  
   �
                    #     2     ?     E     I  ;   R  -   �  #   �     �     �       
   .     9     @  
   [     f     u     �     �     �     �     �     �  	              ;     U     l  
   ~     �     �     �     �     �     �     �               /     5  
   T     _  )   l     �     �     �     �     �     �     �            '   0     X     d     r     �     �     �     �     �  C   �     %  s   5      �     �     �     �          +     ;      L     m  -   {  /   �     �  	   �  !   �           )     J     X     l     |  �  �     z     �     �     �     �  	   �     �  
   �     �     �                     4     <     H     \     q     �     �     �     �     �     �     �     �  
             !  $   .  
   S     ^     t  
   �  	   �     �  	   �     �     �     �     �     �  	     
        %     C     W     k  )   �     �  &   �     �  
     4        N  ^   f     �  
   �     �     �          "     A     ^     t     �      �     �      �       \        q     �  $   �  !   �     �               2  
   F     Q     ]  '   y  5   �     �     �  %   �     %     8     S     m     �  R   �     �  �   	  (   �  !   �  $   �          '     E     W  &   u     �  ,   �  8   �  
          .   '  %   V  ,   |     �     �     �     �     4   G   o                c   #          I              _   K      B   :   "   W           p   3   N          P       L   *   R   !                  +              &   %          ?       m   >   X   $       l   5   6   	   A      n       
   E   d      D      Y   9       @       M   '      -           [                 <              a   U   t   g   V   b   h   1   S          f   ^         k       i   s       7   =       2   T              C       J   e      8   (   Q   r   0           j           /   \   ]   .      ;   q                  ,      H   O              Z   F             )   `    %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2016-11-22 23:23+0200
Last-Translator: Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
Language-Team: Lithuanian <kde-i18n-lt@kde.org>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);
X-Generator: Lokalize 2.0
 %1 &vadovas &Apie %1 &Tikrasis dydis &Pridėti žymelę &Atgal &Užverti &Konfigūruoti %1... &Kopijuoti Pa&remti &Keisti žymeles... &Ieškoti... &Pirmas puslapis &Talpinti į puslapį &Pirmyn &Eiti į... &Eiti į eilutę... &Eiti į puslapį... P&askutinis puslapis &Siųsti paštu... Per&kelti į šiukšlinę &Naujas &Kitas puslapis At&verti... &Padėti Ankstesnis &puslapis S&pausdinti... &Išjungti &Rodyti iš naujo Pa&keisti... &Pranešti apie programos klaidą... Į&rašyti Į&rašyti nustatymus &Rašybos tikrinimas... Atš&aukti &Aukštyn &Mastelis... &Tolesnis &Ankstesnis Paleidžiant rodyti patarimus Ar žinojote?
 Konfigūruoti Dienos patarimas Apie &KDE Iš&valyti Patikrinti rašybą dokumente Išvalyti sąrašą Užverti dokumentą Konfigūruoti &pranešimus... Konfigūruoti &sparčiuosius klavišus... Konfigūruoti į&rankines Kopijuoti pažymėjimą į iškarpinę Sukurti naują dokumentą Iškirp&ti Iškirpti pažymėjimą, perkeliant į mainų sritį &Panaikinti žymėjimą rch@richard.eu.org, dgvirtual@akl.lt, gintautas@miselis.lt, stikonas@gmail.com, liudas@akmc.lt Nustatyti automatiškai Numatytoji Rodyti &visame ekrane Ieškoti &kito Ieškoti &ankstesnio Talpinti į puslapio &aukštį Talpinti į puslapio &plotį Eiti dokumente &atgal Eiti dokumente &pirmyn Peršokti į pirmą puslapį Peršokti į paskutinį puslapį Peršokti į kitą puslapį Peršokti į ankstesnį puslapį Eiti aukštyn Ričardas Čepas, Donatas Glodenis, Gintautas Miselis, Andrius Štikonas, Liudas Ališauskas Nėra įrašų Atverti &paskutinį naudotą Atverti neseniai atvertą dokumentą Atverti egzistuojantį dokumentą Įklijuoti iškarpinės turinį &Spausdinimo peržiūra Spausdinti dokumentą Išjungti programą At&statyti G&rąžinti Rodyti dokumentą iš naujo Grąžinti paskutinį atliktą veiksmą Atmesti neišsaugotus pakeitimus padarytus dokumentui Įrašyti &kaip... Įrašyti dokumentą Įrašyti dokumentą nauju pavadinimu Pažymėti &viską Pasirinkite didinimo lygį Siųsti dokumentą paštu Rodyti &meniu juostą Rodyti į&rankių juostą Rodyti meniu juostą<p>Vėl parodo meniu juostą po to, kai ji buvo paslėpta.</p> Rodyti &būsenos juostą Rodyti būsenos juostą<p>Parodo būsenos juostą, t.y., juostą, esančią lango apačioje, ir naudojamą būsenos informacijai pateikti.</p> Rodyti dokumento spausdinimo peržiūrą Rodyti arba slėpti meniu juostą Rodyti arba slėpti būsenos juostą Rodyti arba slėpti įrankinę Pakeisti programos &kalbą... &Dienos patarimas Atšaukti paskutinį veiksmą Žiūrėti dokumentą jo tikru dydžiu Kas &tai yra? Jums neleidžiama išsaugoti konfigūracijos Busite prašomi nustatyti tapatybę prieš išsaugojimą Pa&didinti Su&mažinti Padidinti, kad tilptų puslapio aukštis lange Padidinti, kad tilptų puslapis lange Padidinti, kad tilptų puslapio plotis lange &Atgal &Pirmyn &Namų puslapis &Pagalba 