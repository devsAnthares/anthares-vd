��            )   �      �     �  
   �     �  �   �  `   I     �     �     �     �     �     �     �     �  	   	               %     ,  	   @     J     M     Y     _     s  	   z     �     �  	   �    �     �  
   �     �  �   �  d   f  	   �     �     �     �     �     �          *     6     C  	   L     V     _     r     ~     �     �     �     �     �  	   �     �  
   �        
                                                                                                                              	     troubleshooting purposes. %1$s: %2$s About... Cinnamon is currently running without video hardware acceleration and, as a result, you may observe much higher than normal CPU usage.

 Cinnamon started successfully, but one or more applets, desklets or extensions failed to load.

 Configure... Copy Disabled Eject Error Execution time (ms):  Failed to launch '%s' File System Hide Text Home Initializing Loaded Loaded successfully Main Menu No Out of date Paste Remove this desklet Search Show Text Unknown Yes _New Menu Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-26 02:18+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Serbian Ijekavian Latin <sr@ijekavianlatin@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
  ispravljanje grešaka. %1$s: %2$s O... Cinnamon trenutno radi bez hardverskog video ubrzanja i kao posljedicu možete primjetiti mnogo veću upotrebu procesora nego obično.

 Cinnamon je pokrenut uspješno, ali jedan ili više apleta, deskleta ili proširenja nije učitan.

 Podesi... Kopiraj Deaktiviran Izbaci Greška Trajanje izvršavanja  Neuspjelo pokretanje  '%s' Fajl sistem Sakrij tekst Početak Pripremam Učitano Uspješno učitano Glavni Meni Ne Zastario Nalijepi Ukloni ovaj desklet Traži Prikaži tekst Nepoznato Da _Novi Meni 