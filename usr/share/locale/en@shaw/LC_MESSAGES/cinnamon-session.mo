��    )      d  ;   �      �     �     �  5   �       (     )   ;  %   e  '   �     �     �     �     �     �     �          
     &  '   5  P   ]     �     �     �     �        +   /     [     y     �     �  E   �     �  &   �       X   ;  $   �  
   �     �     �  
   �     �  �  �  H   �	  Q   "
  �   t
        j   9  h   �  p     y   ~  B   �     ;  1   H     z  3   }  !   �     �  J   �  1   4  |   f  �   �     �  S   �  [   >  I   �  Y   �  �   >  Y   �     %  1   A  2   s  �   �     Z  p   k  V   �  �   3  S        p     �     �     �     �         
       	                   %             !          #                                                              $              "               (          &      )                   '                - the Cinnamon session manager A program is still running: Application does not accept documents on command line Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Enable debugging code FILE Hibernate Anyway ID Icon '%s' not found Lock Screen Log out Log out of this system now? Not responding Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Session management options: Show session management options Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Suspend Anyway Switch User Anyway Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Waiting for programs to finish.  Interrupting these programs may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: metacity
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-session&component=general
PO-Revision-Date: 2017-09-06 11:17+0000
Last-Translator: Thomas Thurman <Unknown>
Language-Team: Shavian <ubuntu-l10n-en-shaw@launchpad.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
  - 𐑞 ·𐑜𐑯𐑴𐑥 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑼 𐑩 𐑐𐑮𐑴𐑜𐑮𐑨𐑥 𐑦𐑟 𐑕𐑑𐑦𐑤 𐑮𐑳𐑯𐑦𐑙: 𐑩𐑐𐑤𐑦𐑒𐑱𐑕𐑩𐑯 𐑛𐑳𐑟 𐑯𐑪𐑑 𐑨𐑒𐑕𐑧𐑐𐑑 𐑛𐑪𐑒𐑿𐑥𐑩𐑯𐑑𐑕 𐑪𐑯 𐑒𐑩𐑥𐑭𐑯𐑛 𐑤𐑲𐑯 𐑒𐑨𐑯𐑕𐑩𐑤 𐑒𐑫𐑛 𐑯𐑪𐑑 𐑒𐑩𐑯𐑧𐑒𐑑 𐑑 𐑞 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑼 𐑒𐑫𐑛 𐑯𐑪𐑑 𐑒𐑮𐑦𐑱𐑑 ICE 𐑤𐑦𐑕𐑩𐑯𐑦𐑙 𐑕𐑪𐑒𐑩𐑑: %s 𐑛𐑦𐑕𐑱𐑚𐑩𐑤 𐑒𐑩𐑯𐑧𐑒𐑖𐑩𐑯 𐑑 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑼 𐑛𐑵 𐑯𐑪𐑑 𐑤𐑴𐑛 𐑿𐑟𐑼-𐑕𐑐𐑧𐑕𐑦𐑓𐑲𐑛 𐑩𐑐𐑤𐑦𐑒𐑱𐑕𐑩𐑯𐑟 𐑦𐑯𐑱𐑚𐑩𐑤 𐑛𐑰𐑚𐑳𐑜𐑦𐑙 𐑒𐑴𐑛 𐑓𐑲𐑤 𐑣𐑲𐑚𐑼𐑯𐑱𐑑 𐑧𐑯𐑦𐑢𐑱 ID 𐑲𐑒𐑪𐑯 '%s' 𐑯𐑪𐑑 𐑓𐑬𐑯𐑛 𐑤𐑪𐑒 𐑕𐑒𐑮𐑰𐑯 𐑤𐑪𐑜 𐑬𐑑 𐑤𐑪𐑜 𐑬𐑑 𐑝 𐑞𐑦𐑕 𐑕𐑦𐑕𐑑𐑩𐑥 𐑯𐑬? 𐑯𐑪𐑑 𐑮𐑰𐑕𐑐𐑪𐑯𐑛𐑦𐑙 𐑐𐑮𐑴𐑜𐑮𐑨𐑥 𐑒𐑷𐑤𐑛 𐑢𐑦𐑞 𐑒𐑩𐑯𐑓𐑤𐑦𐑒𐑑𐑦𐑙 𐑪𐑐𐑖𐑩𐑯𐑟 𐑮𐑩𐑓𐑘𐑵𐑟𐑦𐑙 𐑯𐑿 𐑒𐑤𐑲𐑩𐑯𐑑 𐑒𐑩𐑯𐑧𐑒𐑖𐑩𐑯 𐑚𐑦𐑒𐑪𐑟 𐑞 𐑕𐑧𐑖𐑩𐑯 𐑦𐑟 𐑒𐑳𐑮𐑩𐑯𐑑𐑤𐑦 𐑚𐑰𐑦𐑙 𐑖𐑳𐑑 𐑛𐑬𐑯
 𐑕_𐑳𐑕𐑐𐑧𐑯𐑛 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑥𐑩𐑯𐑑 𐑪𐑐𐑖𐑩𐑯𐑟: 𐑖𐑴 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑥𐑩𐑯𐑑 𐑪𐑐𐑖𐑩𐑯𐑟 𐑖𐑳𐑑 𐑛𐑬𐑯 𐑞𐑦𐑕 𐑕𐑦𐑕𐑑𐑩𐑥 𐑯𐑬? 𐑕𐑳𐑥 𐑐𐑮𐑴𐑜𐑮𐑨𐑥𐑟 𐑸 𐑕𐑑𐑦𐑤 𐑮𐑳𐑯𐑦𐑙: 𐑕𐑐𐑧𐑕𐑦𐑓𐑲 𐑓𐑲𐑤 𐑒𐑩𐑯𐑑𐑱𐑯𐑦𐑙 𐑕𐑱𐑝𐑛 𐑒𐑩𐑯𐑓𐑦𐑜𐑘𐑼𐑱𐑖𐑩𐑯 𐑕𐑐𐑧𐑕𐑦𐑓𐑲 𐑕𐑧𐑖𐑩𐑯 𐑥𐑨𐑯𐑩𐑡𐑥𐑩𐑯𐑑 ID 𐑕𐑑𐑸𐑑𐑦𐑙 %s 𐑕𐑩𐑕𐑐𐑧𐑯𐑛 𐑧𐑯𐑦𐑢𐑱 𐑕𐑢𐑦𐑗 𐑿𐑟𐑼 𐑧𐑯𐑦𐑢𐑱 𐑳𐑯𐑱𐑚𐑩𐑤 𐑑 𐑕𐑑𐑸𐑑 𐑤𐑪𐑜𐑦𐑯 𐑕𐑧𐑖𐑩𐑯 (𐑯 𐑳𐑯𐑱𐑚𐑩𐑤 𐑑 𐑒𐑩𐑯𐑧𐑒𐑑 𐑑 𐑞 X 𐑕𐑻𐑝𐑼) 𐑳𐑯𐑴𐑯 𐑩𐑯𐑮𐑧𐑒𐑩𐑜𐑯𐑲𐑟𐑛 𐑛𐑧𐑕𐑒𐑑𐑪𐑐 𐑓𐑲𐑤 𐑝𐑻𐑠𐑩𐑯 '%s' 𐑩𐑯𐑮𐑧𐑒𐑩𐑜𐑯𐑲𐑟𐑛 𐑤𐑷𐑯𐑗 𐑪𐑐𐑖𐑩𐑯: %d 𐑢𐑱𐑑𐑦𐑙 𐑓𐑹 𐑐𐑮𐑴𐑜𐑮𐑨𐑥𐑟 𐑑 𐑓𐑦𐑯𐑦𐑖.  𐑦𐑯𐑑𐑻𐑳𐑐𐑑𐑦𐑙 𐑞𐑰𐑟 𐑐𐑮𐑴𐑜𐑮𐑨𐑥𐑟 𐑥𐑱 𐑒𐑷𐑟 𐑿 𐑑 𐑤𐑵𐑟 𐑢𐑻𐑒. 𐑿 𐑸 𐑒𐑳𐑮𐑩𐑯𐑑𐑤𐑦 𐑤𐑪𐑜𐑛 𐑦𐑯 𐑨𐑟 "%s". _𐑣𐑲𐑚𐑼𐑯𐑱𐑑 _𐑤𐑪𐑜 𐑬𐑑 _𐑮𐑰𐑕𐑑𐑸𐑑 _𐑖𐑳𐑑 𐑛𐑬𐑯 _𐑕𐑢𐑦𐑗 𐑿𐑟𐑼 