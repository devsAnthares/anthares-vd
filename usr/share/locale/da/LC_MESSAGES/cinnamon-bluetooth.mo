��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                   %     �	     �	     �	  	   �	     �	     
  ,    
     M
  	   `
  #   j
     �
  ,   �
  .   �
     �
          !  O   7     �     �     �  "   �     �     �     �               $  I   @  ,   �     �  #   �     �     �               %     <  *   M     x  	   }     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-09-23 17:00+0000
Last-Translator: scootergrisen <scootergrisen@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Tillad altid adgang Anmodning om godkendelse fra %s Bluetooth Indstillinger for Bluetooth Bluetooth er deaktiveret Bluetooth er deaktiveret med hardwarekontakt Gennemse filer … Annullér Konfigurér Bluetooth-indstillinger Forbindelse Enheden %s ønsker adgang til tjenesten "%s" Enheden %s ønsker at parre med denne computer Matcher ikke Fejl under gennemsyn af enhed Tillad kun denne gang Hvis du fjerner enheden, skal den sættes op igen, før den bruges næste gang. Tastaturindstillinger Matcher Museindstillinger Indstillinger for mus og pegeplade Nej Ingen Bluetooth-adaptere fundet OK Parret Parringsbekræftelse for %s Anmodning om parring for %s Bekræft venligst, at PIN-koden "%s" stemmer overens med den på enheden. Indtast venligst PIN-koden vist på enheden. Afvis Fjern "%s" fra listen over enheder? Fjern enhed Send filer til enhed … Send filer … Opsæt ny enhed Opsæt en ny enhed … Lydindstillinger Enheden kan ikke gennemses, fejlen er "%s" Type Synlighed Synlighed af "%s" Ja forbinder … afbryder forbindelsen … hardwaredeaktiveret side 1 side 2 