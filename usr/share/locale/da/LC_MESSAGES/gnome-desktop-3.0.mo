��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �            
   .     9     G     W     k     t      �  &   �  O   �     	     7	     D	  )   a	  G   �	  �   �	  2   �
  z   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-13 03:09+0000
PO-Revision-Date: 2018-08-30 19:24+0200
Last-Translator: Ask Hjorth Larsen <asklarsen@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Danish
X-Poedit-Country: DENMARK
X-Poedit-SourceCharset: utf-8
 %R %R:%S %a %R %a %R:%S %a %-e. %b_%R %a %-e. %b_%R:%S %a %-e. %b_%l:%M %p %a %-e. %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %-e. %b_%R %-e. %b_%R:%S %e. %b_%l:%M %p %-e. %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d kan ikke drive output %s CRTC %d understøtter ikke rotation=%d CRTC %d: prøver tilstand %dx%d@%dHz med output på %dx%d@%dHz (gennemløb %d)
 Prøver tilstande for CRTC %d
 Ikke angivet kan ikke klone til output %s kunne ikke tildele CTRC'er til output:
%s ingen af de valgte tilstande var kompatible med de mulige tilstande:
%s output %s har ikke de samme parametre som et andet klonet output:
eksisterende tilstand = %d, ny tilstand = %d
eksisterende koordinater = (%d, %d), nye koordinater = (%d, %d)
eksisterende rotation = %d, ny rotation = %d output %s understøtter ikke tilstanden %dx%d@%dHz anmodet virtuel størrelse passer ikke til tilgængelige størrelse: Anmodet=(%d, %d), minimum=(%d, %d), maksimum=(%d, %d) 