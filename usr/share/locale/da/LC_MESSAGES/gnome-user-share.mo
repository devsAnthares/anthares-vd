��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     c     y  (   �     �     �     �  I   �  e   <      �  +   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-09-17 18:27+0200
Last-Translator: Kenneth Nielsen <k.nielsen81@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Language: da_DK
X-Source-Language: C
 %s's offentlige filer %ss offentlige filer på %s Start personlig fildeling hvis aktiveret Personlig fildeling Deling Indstillinger for deling Aktivér personlig fildeling, for at dele mappens indhold på netværket. Hvornår skal der spørges om adgangskode. Valgmuligheder: “never”, “on_write”, “always”. Hvornår der kræves adgangskode del;deling;filer;http;netværk;kopier;send; 