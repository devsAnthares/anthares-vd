��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  &   *     Q     Z     w      �     �     �     �     �  "   �          -     C  
   K  
   V      a     �  !   �     �     �     �  
   �  |   �     t	  #   �	     �	  	   �	     �	  *   �	     
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-03-18 17:12+0100
Last-Translator: Ask Hjorth Larsen <asklarsen@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Danish
X-Poedit-Country: DENMARK
X-Poedit-SourceCharset: utf-8
 - Musetilgængelighedsdæmon til GNOME Knapstil Knapstil for kliktypevindue. Målene for kliktypevinduet Orienteringen af kliktypevinduet Stilen af kliktypevinduet Dobbeltklik Træk Aktivér hvileklik Aktivér simuleret sekundært klik Kunne ikke vise hjælp Skjul kliktypevinduet Vandret Svæveklik Kun ikoner Ignorér små markørbevægelser Orientering Orienteringen af kliktypevinduet. Sekundært klik Brug aktiv hviletilstand Afslut mousetweaks Enkeltklik Størrelse og placering af kliktypevinduet. Formatet er en standardstreng for angivelse af vinduesmål for X-vinduessystemet Start mousetweaks som dæmon Start mousetweaks i logind-tilstand Tekst og ikoner Kun tekst Ventetid før hvileklik Ventetid før et simuleret sekundært klik Lodret 