��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
     
     '
     *
     0
     9
      E
  &   f
  O   �
  Q   �
     /     7     G  !   N     p     �  )   �  '   �  )   �  /   (  E   X  +   �  P   �  G     �   c  2   ?  �   r  z   �  D   r                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-16 00:42+0200
Last-Translator: Ask Hjorth Larsen <asklarsen@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
X-Poedit-Country: DENMARK
X-Poedit-Language: Danish
X-Poedit-SourceCharset: utf-8
 %A %e. %B, %R %A %e. %B, %R:%S %A %e. %B, %l:%M %p %A %e. %B, %l:%M:%S %p %A d. %e. %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d kan ikke drive output %s CRTC %d understøtter ikke rotation=%s CRTC %d: prøver tilstand %dx%d@%dHz med output på %dx%d@%dHz (gennemløb %d)
 Kan ikke finde et terminalprogram. Bruger xterm, selvom om det måske ikke virker Bærbar Klonede skærme Ukendt RANDR-udvidelsen er ikke tilstede Prøver tilstande for CRTC %d
 kan ikke klone til output %s kunne ikke tildele CTRC'er til output:
%s kunne ikke hente information om CRTC %d kunne ikke hente information om uddata %d kunne ikke hente tabeller til skærmstørrelser kunne ikke hente skærmresourcerne (CRTC'er, uddataene, tilstandende) kunne ikke sætte indstillinger til CRTC %d ingen af de gemte skærmindstillinger stemmer overens med nuværende indstilling ingen af de valgte tilstande var kompatible med de mulige tilstande:
%s output %s har ikke de samme parametre som et andet klonet output:
eksisterende tilstand = %d, ny tilstand = %d
eksisterende koordinater = (%d, %d), nye koordinater = (%d, %d)
eksisterende rotation = %s, ny rotation = %s output %s understøtter ikke tilstanden %dx%d@%dHz anmodet placering/størrelse til CRTC %d er uden for de tilladte grænser: Position=(%d, %d), størrelse=(%d, %d), maksimum=(%d, %d) anmodet virtuel størrelse passer ikke til tilgængelige størrelse: Anmodet=(%d, %d), minimum=(%d, %d), maksimum=(%d, %d) uhåndterbar X-fejl under hentning af tabeller til skærmstørrelser 