��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  /   '     W  P   h     �  '   �  $   �  6     "   Q     t  P   �  [   �     .     F  +   `  +   �     �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 17:01+0000
Last-Translator: scootergrisen <scootergrisen@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: da
 Får pauseskærmen til at afslutte kontrolleret Kontrollerer … Deaktivér pauseskærmen, hvis den er aktiv (så skærmen ikke længere er sort) Forkert adgangskode Besked der skal vises på låseskærmen Indtast venligst din adgangskode … Spørg til, hvor længe pauseskærmen har været aktiv Spørg til pauseskærmens tilstand Skift bruger Fortæller den kørende pauseskærmproces, at skærmen skal låses med det samme Pauseskærmen har været aktiv i %d sekund.
 Pauseskærmen har været aktiv i %d sekunder.
 Pauseskærmen er aktiv
 Pauseskærmen er inaktiv
 Pauseskærmen er ikke aktiv i øjeblikket.
 Slå pauseskærmen til (gør skærmen sort) Lås op Programmets version Caps-Lock-tasten er slået til. 