��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �      �     �     �  8   �  A   +  	   m  -   w  &   �  /   �  %   �  !   "     D     ^     u  $   y     �     �     �  -   �     	          -     >     E     ]     y  $   �     �  -   �     �  I   �     0     ?     R     l     z     �  &   �     �  )   �  4   �     /     >  #   T  0   x      �  
   �     �     �     
     !  ^   @  Z   �  H   �     C  "   J     m     �  d   �  `   �  3   ]  V   �  
   �     �       	   
               =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-04 17:09+0000
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
  - Cinnamons sessionshåndtering Et program kører stadigvæk: AUTOSTART_KAT Programmet accepterer ikke dokumenter på kommandolinjen Kan ikke sende dokument-URI'er til en 'Type=Link'-skrivebordspost Annullér Kunne ikke forbinde til sessionshåndteringen Kunne ikke oprette ICE-lyttesoklen: %s Deaktivér forbindelsen til sessionshåndtering Indlæs ikke brugerangivne programmer Spørg ikke om brugerbekræftelse Aktivér fejlfindingskode Afsluttede med kode %d FIL Filen er ikke en gyldig .desktop-fil Gå i dvaletilstand alligevel ID Ikon "%s" kunne ikke findes Ignorerer eventuelle eksisterende blokeringer Dræbt af signalet %d Lås skærmen Log ud alligevel Log ud Log af dette system nu? Ikke et opstartbart element Svarer ikke Tilsidesæt standard-autostartmapper Sluk Program blev kaldt med selvmodsigende tilvalg Genstart Nægter ny klientforbindelse, fordi sessionen er ved at blive lukket ned
 Husket program Genstart alligevel Genstart dette system nu? SESSIONS_NAVN _Hviletilstand Session Indstillinger for sessionshåndtering: Session som skal bruges Vis indstillinger for sessionshåndtering Vis dialogen uheldig hval for at gennemføre en test Sluk alligevel Sluk dette system nu? Nogle programmer kører stadigvæk: Angiv fil som indeholder den gemte konfiguration Angiv ID for sessionshåndtering Starter %s Stoppet af signalet %d Gå i hviletilstand alligevel Skift bruger alligevel Dette program blokerer log ud. Dette system vil automatisk genstarte om %d sekund. Dette system vil genstarte om %d sekunder. Dette system vil automatisk slukkes om %d sekund. Dette system vil slukkes om %d sekunder. Kunne ikke starte loginsessionen (og kunne ikke forbinde til X-serveren) Ukendt Ukendt version "%s" af desktop-fil Ukendt opstartstilvalg: %d Programversion Venter på, at programmerne afslutter. Hvis du afbryder programmerne, kan det føre til tab af data. Venter på, at programmet afslutter. Hvis du afbryder programmet, kan det føre til tab af data. Du er på nuværende tidspunkt logget ind som "%s". Du vil automatisk blive logget ud om %d sekund. Du vil blive logget ud om %d sekunder. _Annullér _Dvaletilstand _Log ud _Genstart _Sluk S_kift bruger 