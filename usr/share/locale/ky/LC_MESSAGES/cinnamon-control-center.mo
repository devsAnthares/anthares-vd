��    l      |  �   �      0	     1	     9	     A	     N	     e	     x	     �	  I   �	     �	     �	  &   �	     $
  /   C
     s
     z
  	   �
     �
     �
     �
     �
     �
          (     H     g     �     �     �     �     �     �  	   �  
   �     �           !  (   B     k     �     �     �     �  	   �     �     �     �     �  
   �                    $     +     3  
   8     C     \     t     �     �     �  	   �     �     �     �     �     �          9     F     T     e  
   v     �     �     �     �     �     �     �     �     �  	     '        A  N   Y     �     �     �     �     �     �                &     2     @  	   H     R     ^     b     f     k     q     y       	   �  /  �  
   �     �     �     �     �     �  	     }        �  -   �  L   �  ,     S   ?     �  "   �     �     �  6   �  .     J   F  H   �  L   �  J   '  P   r  %   �  &   �       
   0     ;     [  2   y     �     �  $   �  :   �  F   ,  S   s  *   �  &   �       6   8     o     |  
   �     �     �     �     �  .   �               -     <     M     Z  J   h  7   �     �     �          (     >  +   Y  E   �  "   �     �  3   �  9   1  "   k     �     �  !   �     �  )   �  5   "     X  
   e  $   p     �  0   �  ,   �  ,      "   -  L   P  4   �  �   �     t  ?   �     �     �     �  %      0   .   !   _      �      �      �   *   �      �      !     !     !     !     0!     >!  
   N!  
   Y!     -              5   1          3   	   )           4   A              i   \   <   ,      f   _   Z       [   Q   K   T       U                     G   l                 '       9   L   D      B           =       M       g   #   h   /   ]   .   d                  J   (   `   j                 >       a   0   C   e      b              c                      X       k              
       +      O   P       W   *       %      2   N   7   !   ^   E   "      :          R       ?   Y         F   V   ;   @   8      I   S   H         $   &   6                         %d Mb/s %d x %d %d x %d (%s) %i day ago %i days ago %i month %i months %i week %i weeks %i year %i years %s
Run '%s --help' to see a full list of available command line options.
 %s VPN - System Settings 802.1x supplicant configuration failed 802.1x supplicant disconnected 802.1x supplicant took too long to authenticate Ad-hoc Add new connection All files Arabic Authentication required Available Profiles Available Profiles for Cameras Available Profiles for Displays Available Profiles for Printers Available Profiles for Scanners Available Profiles for Webcams Bluetooth connection failed British English Cable unplugged China Chinese (simplified) Colorspace:  Configuration failed Connected Connecting Connection failed Could not detect displays Could not get screen information Could not save the monitor configuration Create virtual device DHCP client error DHCP client failed DHCP client failed to start Default Default:  Device Disabled Disconnecting English Enterprise Firmware missing France French German Germany Help IP Address IP configuration expired IP configuration failed IPv4 Address IPv6 Address Infrastructure Less than 1 week Line busy Mirrored Displays Modem initialization failed Modem not found Monitor Network registration denied Network registration timed out No dial tone Not connected Other profile… PIN check failed PPP failed PPP service disconnected PPP service failed to start Proxy Quit Remove Device Russian SIM Card not inserted SIM Pin required SIM Puk required SIM wrong Secrets were required, but not provided Select ICC Profile File Select a monitor to change its properties; drag it to rearrange its placement. Select a region Set for all users Spain Spanish Status unknown Status unknown (missing) Supported ICC profiles Test profile:  Unavailable United States Unknown Unmanaged Unspecified WEP WPA WPA2 Wired _Import never today yesterday Project-Id-Version: cinnamon-control-center master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-control-center&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-08-26 10:56+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Kirghiz <cinnamon-i18n@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
Language: ky
 %d Мб/с %d x %d %d x %d (%s) %i күн мурун %i ай %i жума %i жыл %s
Бардык параметрлердин толук тизмесин көрүү үчүн '%s --help' аткарыңыз.
 %s VPN - Системалык параметрлер 802.1x клиентин конфигурациялоо ишке ашпады 802.1x клиенти ажыратылган 802.1x клиенти аутентификацядан өтө узак өтүүдө Ad-hoc Жаңы туташуу кошуу Бардык файл Арабча Аутентификация талап кылынат Мүмкүн болгон профайлдар Камералар үчүн мүмкүн болгон профайлдар Экрандар үчүн мүмкүн болгон профайлдар Принтерлер үчүн мүмкүн болгон профайлдар Сканерлер үчүн мүмкүн болгон профайлдар Вебкамералар үчүн мүмкүн болгон профайлдар Bluetooth туташуу катасы Британ анлис тилинде Кабель суурулган Кытай Кытайча (жөнөкөй) Түс мейкиндиги:  Конфигурциялоо ишке ашпады Туташкан Туташуу Туташуу ишке ашпады Мониторлорду аныктоого болбоду Экран жөнүндө маалымат алууга болбоду Монитордун конфигурациясын сактоого болбоду Виртуалдык түзүм түзүү DHCP клиентинин катасы DHCP клиент катасы DHCP клиенти ишин баштай албады Абалкы Абалкы:  Түзүм Токтотулган Ажыратуу Англисче Ишкана Фирмалык программасы жок Франция Французча Немисче Германия Жардам IP дарек IP конфигурациясынын убакыты өтүп кеткен IP конфигурациялоо ишке ашпады IPv4 дарек IPv6 дарек Инфраструктура 1 жумадан аз Канал бош эмес Чагылдырылган экрандар Модемди инициализациялоо ишке ашпады Модем табылган жок Монитор Тармак каттоодон баш тартты Тармактын каттоо убакыты бүттү Каналда сигнал жок Туташкан эмес Башка профиль… PIN текшерүү катасы PPP катасы PPP кызматы ажыратылган PPP кызматы ишин баштай албады Прокси Чыгуу Түзүмдү алып таштоо Орусча SIM картасы орнотулган эмес SIM картанын Pin коду керек SIM картанын Puk коду керек Туура эмес SIM карта Сырсөз талап кылынат, бирок берилген эмес ICC профайлынын файлын тандоо Монитордун касиеттерин өзгөртүү үчүн, аны тандаңыз; орунуналмаштыруу үчүн - жылдырыңыз. Регион тандоо Бардык колдонуучулар үчүн орнотуу Испания Испанча Абалы белгисиз Абалы белгисиз  (жок) Колдоого ээ ICC профайлдары Текшерүү профили:  Жеткиликтүү эмес Кошмо Штаттары Белгисиз Башкарууга мүмкүн эмес Аныкталбаган WEP WPA WPA2 Кабелдик _Импорт эч качан бүгүн кечээ 