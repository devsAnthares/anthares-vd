��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  w        �	     �	     �	     �	     �	     �	     �	     �	  9   �	  6   6
  q   m
  t   �
     T  3   c     �  !   �  7   �  7     >   :  C   y  M   �  X     r   d  M   �  �   %  w   �  \  2  >   �  �   �  �   �  �   `             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 2.13.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-24 18:49+0600
Last-Translator: Timur Zhamakeev <ztimur@gmail.com>
Language-Team: Kirghiz <gnome-i18n@gnome.org>
Language: ky
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.4
Plural-Forms: nplurals=2; plural=n != 1;
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p  %d  CRTC %s чыгуусун башкара албайт %d CRTC  тегеретүүсү=%s  колдобойт %d CRTC:  %dx%d@%dГц режимин текшерүү; чыгуусу %dx%d@%dГц (%d-жолку аракет)
 Туура териминал табылбады, xterm колдонулууда, ал иштебеши мүмкүн Ноутбук Чагылыштырылган дисплейлер Белгисиз RANDR кеңейтмеси жок %d CRTC үчүн режимдерди текшерүү
 %s чыгуусуна клондоого болбоду Чыгуу үчүн CRTC аныктоого болбоду:
%s %d CRTC жөнүндө маалымат алууга болбоду %d чыгуусу жөнүндө маалымат алууга болбоду экрандын өлчөмдөрүнүн диапазонун алуудагы ката экрандын ресурстарына жетүүгө болбоду (CRTC, чыгуусу, режимдери) %d CRTC үчүн параметрлерди орнотууга болбоду дисплейдин сакталган конфигурацияларынын ичинен бири да учурдагыга дал келбейт тандалган бир дагы режим, уруксат болгон режимге туура келбейт:
%s %s чыгуусу, клондонгон башка чыгуунун параметрлерине туура келбейт:
учурдагы режим = %d, жаңы режим = %d
учурдагы координата = (%d, %d), жаңы координата = (%d, %d)
учурдагы тегеретүү = %s, жаңы тегеретүү = %s %s чыгуусу %dx%d@%dHz режимин колдобойт %d CRTC үчүн талап кылынган абалы же өлчөмү белгиленген чектен чыгып кетти: абалы=(%d, %d), өлчөмү=(%d, %d), максимуму=(%d, %d) талап кылынган виртулдык өлчөм, учурдагы жерге батпады:талып кылынгыны=(%d, %d), минимуму=(%d, %d), максимуму=(%d, %d) экрандын өлчөмдөрүнүн диапазонун алууда X системасынын иштетилбеген катасы 