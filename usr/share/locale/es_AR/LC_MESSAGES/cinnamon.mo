��    3      �  G   L      h     i  
   �     �     �  1   �     �  I   �  �   :  `   �     $     1     @     E     N     T     Z     t     �     �  	   �     �     �  5   �     �            	   "     ,     /     4     @     F     d      r     �     �     �     �  "   �       	             2  �   B     �     �  	   �      �     	  i   	  �  �	     2  
   N     Y  "   g  E   �     �  X   �  �   2  n   �     -     ;     M     T     `     i     o     �     �     �     �     �  	   �  ?   �     >     F     N     a     q     t     z     �  (   �     �  (   �     �     �  '     2   9  .   l     �     �     �     �  �   �     u     �     �  6   �     �  z   �     %         -   /                         	                                     #   &   .   '              !   ,   
             (                 2          $                       3         )                1   +   "   0                        *         troubleshooting purposes. %1$s: %2$s About... Add or remove users and groups Are you sure you want to remove workspace "%s"?

 Cancel Check your system log and the Cinnamon LookingGlass log for any issues.   Cinnamon is currently running without video hardware acceleration and, as a result, you may observe much higher than normal CPU usage.

 Cinnamon started successfully, but one or more applets, desklets or extensions failed to load.

 Configure... Control Center Copy Disabled Eject Error Execution of '%s' failed: Execution time (ms):  Failed to launch '%s' File System Hide Text Home Initializing Invalid overview options: Incorrect number of corners Keyboard Loaded Loaded successfully Main Menu No Open Out of date Paste Please contact the developer. Print version Problems during Cinnamon startup Remove Remove this desklet Restore System Configuration Restore the default menu layout Running in software rendering mode Search Show Text System Information System Settings There could be a problem with your drivers or some other issue.  For the best experience, it is recommended that you only use this mode for Unknown Users and Groups WORKSPACE Wrong password, please try again Yes You can disable the offending extension(s) in Cinnamon Settings to prevent this message from recurring.   Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-01 12:45+0000
Last-Translator: Germán <dev.germanfr@gmail.com>
Language-Team: Spanish (Argentina) <es_AR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
  la solución de problemas. %1$s: %2$s Acerca de.... Agregar o quitar usuarios y grupos ¿Usted está seguro de querer eliminar el espacio de trabajo "%s"?

 Cancelar Compruebe el registro del sistema y el de Cinnamon LookingGlass por si hubo problemas.   Cinnamon se está ejecutando sin aceleración de video por hardware, y como resultado, puede haber un consumo mayor del CPU de lo normal.

 Cinnamon ha iniciado correctamente, pero una o más applets, desklets o extensiones no se han podido cargar.

 Configurar... Centro de control Copiar Desactivado Expulsar Error Falló la ejecución de '%s': Tiempo de ejecución (ms):  No se pudo iniciar '%s' Sistema de Archivos Ocultar texto Carpeta Personal Iniciando Resumen opciones inválidas: número incorrecto de las esquinas Teclado Cargado Cargado con éxito Menú principal No Abrir Desactualizado Pegar Por favor, contacte con el desarrollador Mostrar versión Ocurrió un problema al iniciar Cinnamon Eliminar Eliminar este desklet Restaurar la configuración del sistema Restaurar la distribución de menú predeterminada Ejecutando en modo de renderizado por software Buscar Mostrar texto Información del sistema Configuración del sistema Puede haber un problema con los controladores o algún otro problema. Para una mejor experiencia, se recomienda que sólo utilice este modo para Desconocido Usuarios y Grupos Área de trabajo Contraseña incorrecta, por favor intentelo nuevamente Si Puede desactivar las extensiones problemáticas en la configuración de Cinnamon para evitar que este mensaje se repita.   