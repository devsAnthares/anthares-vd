��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  �   8
     �
  �   �
  (   b  "   �  �   �     �  "   �  �   �  @   c  M   �  e   �     X  <   r  �   �  h   :  �   �  A   }  A   �  <     ?   >  +   ~  c   �       W   .     �  Q   �  %   �  ]     R   v     �  <   �  "     %   ?  /   e  %   �     �     �  /   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: mr
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-02-22 23:49+0000
PO-Revision-Date: 2012-08-16 14:27+0530
Last-Translator: Sandeep Shedmake <sshedmak@redhat.com>
Language-Team: Marathi <fedora-trans-mr@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.4
Language: mr
 चौकट निर्माण केल्यावर मुलभूत गटाची, नेमणूक केली जाते सूचक: प्रत्येक चौकटीकरीता स्वतंत्र गट ठेवा व नियंत्रीत करा किबोर्ड मांडणी कळफलक मांडणी कळफलक मांडणी "%s"
सर्वहक्काधीकर &#169; X.Org Foundation व XKeyboardConfig सहमभागी
संकुल मेटामाहिती करीता परवाना पहा कळफलक मॉडेल कळफलक पर्याय एक्जॉटीक, क्वचीत वापरलेली मांडणी व पर्याय लोड करा अगाऊ संरचना घटके लोड करा किबोर्ड मांडणीचे पुनरावलोकन मांडणी गटासह सूचक संचयन/पुनःसंचयन करा दुय्यम गट ऐपलेट मधील बाबी दर्शवा सद्य मांडणी सूचीत करण्यासाठी ऐपलेटमधील बाबी दर्शवा गटातील नावं ऐवजी मांडणीतील नावं दर्शवा गटातील नावं ऐवजी मांडणीतील नावं दर्शवा (फक्त XFree बहु मांडणी समर्थीत आवृत्तीं करीता) कळफलक पूर्वदृश्य, X ऑफसेट कळफलक पूर्वदृश्य, Y ऑफसेट कळफलक पूर्वदृश्य, उंची कळफलक पूर्वदृश्य, रुंदी पार्श्वभूमी रंग लेआउट इंडिकेटरकरीता पार्श्वभूमी रंग फाँट फॅमिलि लेआउट इंडिकेटरकरीता फाँट फॅमिलि फाँट आकार लेआउट इंडिकेटरकरीता फाँट आकार पृष्ठभूमी रंग लेआउट इंडिकेटरकरीता पृष्ठभूमी रंग प्रतिमा दाखल करतेवेळी त्रुटी: %s अपरिचीत XKB सुरू करतेवेळी त्रुटी कळफलक मांडणी कळफलक प्रारूप मांडणी "%s" मांडणी "%s" प्रारूप "%s", %s व %s मांडणी नाही पर्याय नाही पर्याय "%s" पर्याय "%s" 