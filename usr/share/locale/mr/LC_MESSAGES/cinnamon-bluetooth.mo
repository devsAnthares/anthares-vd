��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 ~  %     �	  N   �	  >   
     B
  7   ^
  8   �
  w   �
  5   G     }  T   �     �  y   �  �   s     �  A     0   P  �   �  1   W     �  (   �  <   �       W        g     {  2   �  N   �  |   
  _   �     �  ]     )   _  K   �  (   �  <   �  D   ;  +   �  b   �          "  1   >     p  &   w  6   �  B   �          *                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-04-21 14:25+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Marathi <mr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 पत्ता नेहमी प्रवेश करण्याची मंजूरी %s पासून अधिकृतता विनंती ब्ल्युटूथ ब्ल्युटूथ सेटिंग्स् Bluetooth अकार्यान्वीत आहे हार्डवेअर स्विचतर्फे ब्ल्युटूथ बंद केले आहे फाइल्स् तपासणी करा... रद्द करा ब्ल्युटूथ सेटिंग्स् संरचीत करा जोडणी डिव्हाइस '%s' मध्ये सेवा प्रवेश '%s' करू इच्छित आहे डिव्हाइस%s चे या संगणकावर जोडण्यासाठी इच्छित आहे जुळत नाही डिव्हाइस वाचताना त्रुटी या वेळी फक्त मंजूर उपकरन काढून टाकल्यास, पुढच्या वापरणीपूर्व तुम्हाला त्यांस पुनः सेट करावे लागेल. किबोर्ड सेटिंग्स् जुळवणी माऊस सेटिंग्स् माऊस व टचपॅड सेटिंग्स् नाही ब्ल्युटूथ अडॅप्टर्स् आढळले नाही ठीक आहे जोडी %s साठी जोडणी पुष्टी %s करीता जोडी बनवण्याची विनंती पिन '%s' मध्ये साधनावर एक जुळते की नाही खात्री करा डिव्हाइसवर उल्लेख पिन प्रविष्ट करा. नकार द्या साधनाच्या सूचीतून '%s' काढून टाकायचे? साधन काढून टाका डिव्हाइसमध्ये फायली पाठवा ... फाइल्स् पाठवा... नविन साधनची मांडणी करा नवीन डिव्हाइस सेट अप करा ... साऊंड सेटिंग्स् डिव्हाइस वाचले जाऊ शकत नाही, त्रुटी '%s' प्रकार दर्शनियता “%s” ची दर्शनक्षमता हो जोडले जात आहे... डिस्कनेक्ट करत आहे ... हार्डवेअर अक्षम केले आहे पृष्ठ 1 पृष्ठ 2 