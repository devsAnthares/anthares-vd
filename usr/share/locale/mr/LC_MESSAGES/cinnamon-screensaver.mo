��          L      |       �   `   �      
     %  )   B     l    �  �   �  6   K  3   �  P   �  T                                            The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: mr
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-11-22 18:12+0000
Last-Translator: Rahul Chawre <rahul.chawre@gmail.com>
Language-Team: Marathi <fedora-trans-mr@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: mr
 %d सेकंदकरीता सक्रीनसेवर सुरू आहे.
 %d सेकंदकरीता सक्रीनसेवर सुरू आहे.
 सक्रीनसेवर सुरू आहे
 सक्रीनसेवर बंद आहे
 सक्रीनसेवर सध्या सक्रीय नाही.
 Caps Lock कळ कार्यन्वीत केले गेले आहे. 