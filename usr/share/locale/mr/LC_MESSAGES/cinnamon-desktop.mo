��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     o  J     �	     �	     �	     �	  	    
     

     
     
     
  Z   (
  S   �
  �   �
  �   d     �  .        A  ,   W  R   �  R   �  U   *  g   �  t   �  o   ]  �   �  g   S  �   �  �   f  �   �  j   �  �   ]  �   0  �   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: mr
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-12 17:19+0530
Last-Translator: Sandeep Shedmake <sshedmak@redhat.com>
Language-Team: Marathi <maajhe-sanganak@freelists.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mr
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Lokalize 1.4
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d आऊटपुट %s ला ड्राइव्ह करणे अशक्य CRTC %d rotation=%s करीता समर्थन पुरवत नाही CRTC %d: मोड %dx%d@%dHz वापरून पाहत आहे, %dx%d@%dHz (पास %d) येथील आऊटपुटसह
 टर्मिनल आढळले नाही, xterm चा वापर करत आहे, जरी ते कार्य करत नसेल लॅपटॉप मिरर्ड डिस्पलेज् अपरिचीत RANDR वाढ आढळले नाही CRTC %d करीता मोडस् वापरून पहात आहे
 आऊटपुट %s करीता क्लोन करणे अशक्य CRTC करीता आऊटपुट लागू करणे अशक्य:
%s CRTC %d विषयी माहिती प्राप्त करणे शक्य नाही आउटपुट %d विषयी माहिती प्राप्त करण्यास अशक्य पडदा आकारचे क्षेत्र प्राप्त करू शकले नाही पडदा स्त्रोत (CRTCs, आऊटपुट, पद्धती) प्राप्त करणे अशक्य CRTC %d करीता संयोजना निश्चित करू शकले नाही साठवलेले प्रदर्शन संयोजना पैकी सक्रीय संयोजनाशी जुळू शकले नाही संभाव्य मोडस्सह:
%s कोणतेहि निवडलेले मोडस् सहत्व नाही आऊटपुट %s कडे घटके इतर क्लोन आऊटपुट सारखे नाही:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s आऊटपुट %s मोड %dx%d@%dHz करीता समर्थन पुरवत नाही CRTC %d करीता विनंतीकृत ठिकाण/आकार स्वीकार्य मर्यादा पलीकडे आहे: position(%d, %d), size=(%d, %d), maximum=(%d, %d) आवश्यक आभासी आकार उपलब्ध आकारात घट्ट बसत नाही: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) पडदा आकाराचे क्षेत्र प्राप्त करतेवेळी न हाताळलेली X त्रुटी आढळली 