��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 g  %  	   �	  ?   �	  ;   �	     
  .   )
  &   X
  e   
  ;   �
     !  T   ;     �  U   �  �   �  #   }  c   �  >     �   D  1   �       %   :  @   `     �  O   �     �       9   /  9   i  �   �  ]   2  !   �  [   �  "     B   1  %   t  3   �  6   �  (     �   .     �     �  %   �  	     -     9   I  >   �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-06-15 14:24+0000
Last-Translator: Panwar <Unknown>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 पता हमेशा पहुँच प्रदान करें %s से प्रमाणीकरण अनुरोध ब्लूटूथ ब्लूटूथ सेटिंग्स ब्लूटूथ बंद है हार्डवेयर स्विच द्वारा ब्लूटूथ बंद है फ़ाइलें ब्राउज़ करें... रद्द करें ब्लूटूथ सेटिंग्स विन्यस्त करें कनेक्शन डिवाइस %s सेवा '%s' तक पहुँच चाहता है डिवाइस %s इस कंप्यूटर के साथ युग्मित होना चाहता है मेल नहीं खाता डिवाइस ब्राउज़ करने में त्रुटि हो गयी केवल इस बार ही पहुँच दें यदि आप इस डिवाइस को हटाते हैं, तो अगली बार इसे फिर सेट करना होगा। कुंजीपटल सेटिंग्स मेल खाता है माउस सेटिंग्स माउस व टचपैड की सेटिंग्स नहीं कोई ब्लूटूथ अडैप्टर नहीं मिला ठीक है युग्मित हैं %s के लिए युग्मन पुष्टि %s के लिए युग्मन अनुरोध कृपया पुष्टि करें कि '%s' PIN डिवाइस से मेल खाता है या नहीं। कृपया डिवाइस पर वर्णित PIN दर्ज करें। अस्‍वीकारें क्या '%s' को डिवाइस सूची से हटाया जाए ? डिवाइस हटाएँ डिवाइस को फ़ाइलें भेजें... फ़ाइलें भेजें नया डिवाइस सेट करें नया डिवाइस सेट करें... ध्वनि सेटिंग्स अनुरोधित डिवाइस को ब्राउज़ नहीं किया जा सका, त्रुटि '%s' है प्रकार दृश्यता “%s” की दृश्यता हाँ कनेक्ट हो रहा है... डिस्कनेक्ट हो रहा है... हार्डवेयर निष्क्रिय है पृष्ठ 1 पृष्ठ 2 