��            )   �      �     �     �     �     �  c   �  ,   N  '   {     �  +   �  +   �  %     	   @     J     `     l     t     �     �  (   �  [   �  Q        p  8   �  -   �     �  ,        8     X  2   l  �  �  2   i  C   �     �  8   �  �   3	  j   
  �   �
  K     h   T  a   �  q        �  )   �     �     �     
     '  %   @  t   f  �   �  �   �  3   Q  �   �  �   !  :   �  \   �  ;   O  F   �  2   �                                  	                      
                                                                                 %s's public files %s's public files on %s Cancel File reception complete If this is true, Bluetooth devices can send files to the user's Downloads directory when logged in. Launch Bluetooth ObexPush sharing if enabled Launch Personal File Sharing if enabled May be shared over the network May be shared over the network or Bluetooth May be used to receive files over Bluetooth May be used to share or receive files Open File Personal File Sharing Preferences Receive Reveal File Sharing Sharing Settings When to accept files sent over Bluetooth When to accept files sent over Bluetooth. Possible values are "always", "bonded" and "ask". When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords Whether Bluetooth clients can send files using ObexPush. Whether to notify about newly received files. You have been sent a file You have been sent a file "%s" via Bluetooth You received "%s" via Bluetooth You received a file share;files;bluetooth;obex;http;network;copy;send; Project-Id-Version: gnome-user-share.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-09-21 09:14+0630
Last-Translator: rajesh <rajesh>
Language-Team: Hindi <kde-i18n-doc@kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);


 %s की सार्वजनिक फाइल %s की सार्वजनिक फाइल %s पर है रद्द करें फ़ाइल रिसेप्शन पूर्ण यदि यह सही है, ब्लूटूथ युक्ति उपयोक्ता के डाउनलोड निर्देशिका में भेज सकता है जब लॉगिन हो. Bluetooth ObexPush लॉन्च करें यदि सक्रिय किया गया है निजी फ़ाइल साझा लॉन्च करें यदि सक्रिय किया गया है संजाल पर साझा किया जा सकता है संजाल या ब्लूटूथ पर साझा किया जा सकता है ब्लूटूथ पर फ़ाइल साझा किया जा सकता है शायद फ़ाइल साझा या प्राप्त करना चाह सकता है फ़ाइल खोलें निजी फ़ाइल साझा वरीयताएँ प्राप्त फाइल खोलें साझेदारी सेटिंग्स साझा कब ब्लूटूथ पर भेजे फाइलों को स्वीकार करना है ब्लूटूथ पर भेजे फाइलों को कब स्वीकार करना है. संभावित मान हैं "हमेशा", "बंधित", और "पूछें". कब कूटशब्द के लिए पूछना है. संभावित मान हैं "never", "on_write", और "always". कब कूटशब्द जरूरी है क्या ब्लूटूथ क्लाइंट को ObexPush के प्रयोग से फाइलें भेज सकता है. क्या हाल में प्राप्त फाइलों के बारे में अधिसूचित करना है. आपको कोई फाइल भेजा गया आपको कोई फाइल "%s" ब्लूटूथ से भेजा गया आपने "%s" ब्लूटूथ से पाया आपने कोई फाइल प्राप्त किया share;files;bluetooth;obex;http;network;copy;send; 