��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  �   z      �  �        �  o   �  O   j  �   �  b   ;	  +   �	  �   �	  �   �
  <   ?  ?   |  M   �  p   
     {  ?   �  -   �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-01-07 07:03+0000
Last-Translator: Panwar <Unknown>
Language-Team: Hindi <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: hi
 सुनिश्चित करता है कि स्क्रीनसेवर करीने से बंद हो जांच रहा है... यदि स्क्रीनसेवर सक्रिय है तो इसे निष्क्रिय करें (स्क्रीन दृश्यमान करें) गलत कूटशब्द लॉक स्क्रीन में प्रदर्शित होने वाला संदेश कृपया अपना कूटशब्द दर्ज करें... अनुरोध करे कि स्क्रीनसेवर कितने समय से सक्रिय है स्क्रीनसेवर की स्थिति का अनुरोध करें प्रयोक्ता बदलें स्क्रीन को तुरंत लॉक करने के लिए सक्रिय स्क्रीनसेवर को निर्देशित करता है स्क्रीनसेवर %d सेकेंड से सक्रिय है
 स्क्रीनसेवर %d सेकेंड से सक्रिय है
 स्क्रीनसेवर सक्रिय है
 स्क्रीनसेवर असक्रिय है
 स्क्रीनसेवर अभी असक्रिय है ।
 स्क्रीनसेवर चालू करें (स्क्रीन रिक्त करें) खोलें इस अनुप्रयोग का संस्करण कैप्स लॉक चालू है 