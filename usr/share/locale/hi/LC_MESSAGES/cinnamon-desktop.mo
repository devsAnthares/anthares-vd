��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     t  J     �	     �	     �	     �	  	   
     
     
     
     !
  X   -
  U   �
  �   �
  �   a     E  2   X     �  ?   �  T   �  W   3  a   �  S   �  a   A  a   �  o     V   u  �   �  �   �  �    n   �  t   ,  i   �  �                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-21 14:08+0530
Last-Translator: rajesh <rajeshkajha@yahoo.com>
Language-Team: Hindi <hindi.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
net>
Language: hi
Plural-Forms: nplurals=2; plural=(n!=1);






X-Generator: Lokalize 1.2
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d आउटपुट %s ड्राइव नहीं कर सकता है CRTC %d घुमाव का समर्थन नहीं करता है=%s CRTC %d: %dx%d@%dHz with output at %dx%d@%dHz (pass %d) अवस्था के लिए कोशिश कर रहा है
 कोई टर्मिनल नहीं ढूँढ़ नहीं सकता है, xterm का प्रयोग कर रहा है, बावजूद कि यह काम नहीं करता है लैपटॉप मिरर किया प्रदर्शन अज्ञात RANDR विस्तार मौजूद नहीं है CRTC %d के लिए अवस्था कोशिश कर रहा है
 आउटपुट %s में क्लोन नहीं कर सकता है CRTC को आउटपुट में नियत नहीं कर सकता है:
%s CRTC %d के बारे में सूचना नहीं पा सका %d आउटपुट के बारे में सूचना नहीं पा सका स्क्रीन आकार का परिसर पा नहीं सकता है स्क्रीन संसाधन नहीं पा सका (CRTC, आउटपुट, विधि) CRTC %d के लिए विन्यास सेट नहीं कर सका सक्रिय विन्यास से मेल खाता कोई सहेजा प्रदर्शन विन्यास सहेजा नहीं था कोई भी चुना हुआ नोड संगत नहीं था संभावित विधि के साथ:
%s आउटपुट %s के पास वही पैरामीटर नहीं है जैसा कि क्लोन किया गया आउटपुट:
मौजूदा अवस्था = %d, नयी अवस्था = %d
मौजूदा निर्देशांक = (%d, %d), नया निर्देशांक = (%d, %d)
मौजूदा घुमाव = %s, नए घुमाव = %s आउटपुट %s अवस्था %dx%d@%dHz का समर्थन नहीं करता है requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) अनियंत्रित X त्रुटि जब स्क्रीन आकार का दायरा पा रहा है 