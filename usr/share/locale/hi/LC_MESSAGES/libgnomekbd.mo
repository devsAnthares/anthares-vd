��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )    B  Y   J
     �
  j   �
  %     1   D  z   v  %   �  +     �   C  L   �  _     �   r     �  9     �   O  Z   �  �   G  D     T   c  J   �  M     %   Q  Y   w  %   �  Y   �     Q  S   q  )   �  ]   �  H   M     �  /   �  1   �  %     )   1     [  &   {  )   �  /   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-02-22 23:49+0000
PO-Revision-Date: 2012-09-21 08:04+0530
Last-Translator: chandankumar <chandankumar.093047@gmail.com>
Language-Team: Hindi <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);




X-Generator: Lokalize 1.4
Language: hi
 मूलभूत समूह विंडो निर्माण पर नियत सूचक:  प्रति विंडो अलग समूह रखे व प्रबंधित करें कुंजीपट ख़ाका कुंजीपटल अभिन्यास Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata कुंजीपटल मॉडल कुंजीपटल विकल्प अनोखा और कभी कभी प्रयुक्त लेआउट व विकल्प लोड करें अतिरिक्त विन्यास मद लोड करें कुंजीपटल लेआउट का पूर्वावलोकन करें  लेआउट समूह के साथ संकेतक सहेजें/पुनर्भंडारित करें सहायक समूह ध्वज में एप्लेट देखें वर्तमान अभिन्यास को सूचित करने हेतु एप्लेट में ध्वज दिखाएँ समूह नाम के बजाय लेआउट नाम दिखायें समूह नाम के बजाय लेआउट नाम दिखायें (सिर्फ XFree समर्थित बहुल लेआउट के संस्करण के लिये) कुंजीपटल पूर्वावलोकन, X offset कुंजीपटल पूर्वावलोकन ऊँचाई, Y offset कुंजीपटल पूर्वावलोकन ऊँचाई कुंजीपटल पूर्वावलोकन चौड़ाई पृष्ठभूमि रंग लेआउट संकेतक के लिए पृष्ठभूमि रंग फ़ॉन्ट परिवार लेआउट संकेतक के लिए फ़ॉन्ट परिवार फ़ॉन्ट आकार लेआउट संकेतक के लिए फ़ॉन्ट आकार अग्रभूमि का रंग लेआउट संकेतक के लिए अग्रभूमि का रंग छवि लोड करने में त्रुटि थी: %s अज्ञात XKB आरंभीकरण त्रुटि कुंजीपटल अभिन्यास कुंजीपटल मॉडल लेआउट "%s" लेआउट "%s" मॉडल "%s", %s और %s कोई लेआउट नहीं कोई विकल्प नहीं विकल्प "%s" विकल्प "%s" 