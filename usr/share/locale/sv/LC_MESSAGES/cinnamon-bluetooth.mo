��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 q  %     �	     �	  "   �	  	   �	     �	     
  ,   
     H
     `
  $   g
  
   �
  /   �
  -   �
     �
            S   9     �     �     �  &   �     �      �       
              =  E   Z  3   �     �  (   �               ,     <     R     k  6   ~     �  	   �     �     �     �     �     �                          /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-05-27 10:57+0000
Last-Translator: Jonatan Nyberg <Unknown>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adress Tillåt alltid åtkomst Auktoriseringsförfrågan från %s Bluetooth Inställningar för Bluetooth Bluetooth är inaktiverat Bluetooth är inaktiverat med hårdvaruknapp Bläddra efter filer... Avbryt Konfigurera Bluetooth-inställningar Anslutning Enheten %s vill ha åtkomst till tjänsten '%s' Enheten %s vill para ihop sig med denna dator Matchar inte Fel vid avläsning av enhet Tillåt endast denna gång Om du tar bort enheten så måste du ställa in den igen innan du kan använda den. Tangentbordsinställningar Matchar Musinställningar Inställningar för mus och styrplatta Nej Inga Bluetooth-adaptrar hittades OK Ihopparade Ihopparningsbekräftelse för %s Ihopparningsbegäran för %s Var god och bekräfta att PIN '%s' matchar den som visas på enheten. Var god och ange PIN-koden som visades på enheten. Avvisa Ta bort "%s" från listan över enheter? Ta bort enhet Sänd Filer till Enhet... Skicka filer... Ställ in en Ny Enhet Ställ in en Ny Enhet... Ljudinställningar Den begärda enheten kan inte avläsas, felet är '%s' Typ Synlighet Synlighet för “%s” Ja ansluter... kopplar från... hårdvaran inaktiverad sida 1 sida 2 