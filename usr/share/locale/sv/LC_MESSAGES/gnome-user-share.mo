��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     2     D  *   ]     �     �     �  V   �  y   !     �  (   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-07 14:57+0200
Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.3
 %.0sPublika filer %.0sPublika filer på %s Starta personlig filutdelning om aktiverat Utdelning av personliga filer Delning Inställningar för delning Slå på personlig fildelning för att dela innehållet i denna mapp över nätverket. När lösenord ska efterfrågas. Möjliga värden är "never" (aldrig), "on_write" (vid skrivning) och "always" (alltid). När lösenord ska krävas dela;filer;http;nätverk;kopiera;skicka; 