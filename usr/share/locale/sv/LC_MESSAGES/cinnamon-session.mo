��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �     �     �     �  3   �  B        K  *   R  (   }  ,   �  '   �  '   �     #     <     R  %   V     |     �     �  !   �     �     �     �     
          0     J  3   V  	   �  '   �  	   �  T   �          .     ?     Z     g     t     |     �  #   �  &   �     �       !   (  -   J     x  
   �     �     �     �  %   �  {     �   �  J        `  #   g     �     �  k   �  h   !  *   �  e   �       
   #  	   .  
   8  
   C     N     =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-11-17 03:07+0000
Last-Translator: kristian <Unknown>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: sv
  Cinnamon's sessionshanterare Ett program körs fortfarande: AUTOSTART_DIR Programmet tar inte emot dokument på kommandoraden Kan inte skicka dokument-URI:er till en "Type=Link"-skrivbordspost Avbryt Kunde inte ansluta till sessionshanteraren Kunde inte skapa lyssnande ICE-uttag: %s Inaktivera anslutning till sessionshanterare Läs inte in användarspecifika program Fråga inte efter användarbekräftelse Aktivera felsökningskod Avslutades med kod %d FIL Filen är inte en giltig .desktop-fil Försätt i viloläge ändå ID Ikonen "%s" hittades inte Ignorera allt som förhindrar det Dödad av signal %d Lås skärmen Logga ut ändå Logga ut Logga ut ur detta system nu? Inte ett startbart objekt Svarar inte Åsidosätt standardkataloger för automatisk start Stäng av Program anropat med motsägande flaggor Starta om Avvisar ny klientanslutning på grund av att sessionen är på väg att stängas av
 Ihågkomna program Starta om ändå Starta om detta system nu? SESSION_NAME Vä_nteläge Session Flaggor för sessionshantering: Session att använda Visa flaggor för sessionshantering Visa den "felande valen" för testning Stäng av ändå Stäng av detta system nu? Några program körs fortfarande: Ange fil som innehåller sparad konfiguration Ange sessionshanterings-id Startar %s Stoppad av signal %d Försätt i vänteläge ändå Växla användare ändå Detta program blockerar utloggningen. Detta system kommer om %d sekund att automatiskt startas om. Detta system kommer om %d sekunder att automatiskt startas om. Det här systemet kommer att automatiskt stängas av om %d sekund. Det här systemet kommer att automatiskt stängas av om %d sekunder. Kunde inte starta inloggningssession (och kan inte ansluta till X-servern) Okänt Okänd version i skrivbordsfil "%s" Okänt startalternativ: %d Programversion Väntar på att programmen ska köras färdigt.  Avbryta dessa program kan innebära att du förlorar data. Väntar på att programmet ska köras färdigt.  Avbryta programmet kan innebära att du förlorar data. Du är för närvarande inloggad som "%s". Du kommer automatiskt att loggas ut om %d sekund. Du kommer automatiskt att loggas ut om %d sekunder. _Avbryt Vilolä_ge _Logga ut Starta _om St_äng av _Växla användare 