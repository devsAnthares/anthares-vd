��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  *        =  B   M     �  $   �  "   �  ;   �  &   (     O  V   ^  _   �          1  2   O  )   �     �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-03 16:21+0000
Last-Translator: kristian <Unknown>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: sv
 Gör att skärmsläckaren avslutas elegant Kontrollerar... Om skärmsläckaren är aktiv, inaktivera den (väck upp skärmen) Felaktigt lösenord Meddelande att visa på låsskärmen Var god och ange ditt lösenord... Fråga om tidsperioden som skärmsläckaren har varit aktiv Fråga om status för skärmsläckaren Byt användare Berättar för den körande skärmsläckarprocessen att skärmen ska låsas omedelbart Skärmsläckaren har varit aktiv i %d sekund.
 Skärmsläckaren har varit aktiv i %d sekunder.
 Skärmsläckaren är aktiv
 Skärmsläckaren är inaktiv
 Skärmsläckaren är för närvarande inte aktiv.
 Slå på skärmsläckaren (töm skärmen) Lås upp Programversion Du har CapsLock-tangenten på. 