��          L      |       �   /   �      �   =   �   +   5     a  �  i  4         @  A   a  ,   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-23 22:35+0200
Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
 Fel (%s) vid konvertering av data för barn, kastar. Fel vid läsning från barn: %s. GnuTLS inte aktiverat; data kommer skrivas till disk okrypterade! Kan inte konvertera tecken från %s till %s. VARNING 