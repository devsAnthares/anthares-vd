��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  M   X  2   �  <   �  /        F     e     ~  (   �  \   �                    	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Daniel Nylander <po@danielnylander.se>
Language-Team: Swedish (http://www.transifex.com/projects/p/freedesktop/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Autentisering krävs för att ändra konfigurationen för inloggningsskärmen Autentisering krävs för att ändra användardata Autentisering krävs för att ändra ditt egna användardata Ändra konfigurationen för inloggningsskärmen Ändra ditt egna användardata Aktivera felsökningskod Hantera användarkonton Skriv ut versionsinformation och avsluta Tillhandahåller D-Bus-gränssnitt för frågor och ändra information för användarkonton. Ersätt befintlig instans 