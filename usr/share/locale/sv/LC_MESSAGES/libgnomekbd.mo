��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j    �  /   �
  
   �
  .   �
     �
       �        �     �  :   �  "     "   (  ;   K     �     �  ?   �  (   �  k   "  (   �  (   �  #   �  #        (  &   9     `  '   r     �  (   �     �  '   �  5        F     M     `     s     �     �     �     �     �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-07-28 21:05+0000
PO-Revision-Date: 2016-08-01 12:23+0200
Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.8
 Standardgrupp, tilldelade för fönsterskapande Indikator: Behåll och hantera separat grupp per fönster Tangentbordslayout Tangentbordslayout Tangentbordslayouten "%s"
Copyright &#169; X.Org Foundation och XKeyboardConfig-bidragsgivarna
Se paketets metadata för licensiering Tangentbordsmodell Tangentbordsalternativ Läs in exotiska, sällan använda layouter och alternativ Läs in extra konfigurationsobjekt Förhandsvisa tangentbordslayouter Spara/återställ indikatorer tillsammans med layoutgrupper Sekundära grupper Visa flaggor i panelprogrammet Visa flaggor i panelprogrammet för att indikera aktuell layout Visa layoutnamn istället för gruppnamn Visa layoutnamn istället för gruppnamn (endast för versioner av XFree som har stöd för flera layouter) Tangentbordsförhandsvisning, X-position Tangentbordsförhandsvisning, Y-position Tangentbordsförhandsvisning, höjd Tangentbordsförhandsvisning, bredd Bakgrundsfärgen Bakgrundsfärgen för layoutindikatorn Typsnittsfamiljen Typsnittsfamiljen för layoutindikatorn Typsnittsstorleken Typsnittsstorleken för layoutindikatorn Förgrundsfärgen Förgrundsfärgen för layoutindikatorn Det inträffade ett fel vid inläsning av en bild: %s Okänd XKB-initieringsfel tangentbordslayout tangentbordsmodell layout "%s" layouter "%s" modell "%s", %s och %s ingen layout inga alternativ alternativ "%s" alternativ "%s" preferences-desktop-keyboard 