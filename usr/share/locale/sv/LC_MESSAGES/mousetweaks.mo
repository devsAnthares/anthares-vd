��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  -  �      �  	   �  "        %     B     b     {     �     �  !   �     �     �     �            !   !     C  $   O     t      �     �  
   �  l   �  -   5	  (   c	     �	     �	  (   �	  4   �	  	   
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-02-19 13:36+0100
Last-Translator: Daniel Nylander <po@danielnylander.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 - GNOME-mustillgänglighetsdemon Knappstil Knappstil för klicktypsfönstret. Klicktypsfönstrets geometri Klicktypsfönstrets orientering Klicktypsfönstrets stil Dubbelklick Dra Aktivera uppehållsklick Aktivera simulerat sekundärklick Kunde inte visa hjälp Dölj klicktypsfönstret Horisontellt Uppehållsklick Endast ikoner Ignorera mindre muspekarrörelser Orientering Orientering för klicktypsfönstret. Sekundärklick Ställ in aktivt uppehållsläge Stäng av musjusteringar Enkelklick Storlek och position för klicktypsfönstret. Formatet är en standardsträng för X Window System-geometri. Starta musjusteringar som en bakgrundsprocess Starta musjusteringar i inloggningsläge Text och ikoner Endast text Tid att vänta innan ett uppehållsklick Tid att vänta innan en simulerad sekundärklickning Vertikalt 