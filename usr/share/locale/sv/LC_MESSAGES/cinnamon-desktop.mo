��             +         �     �     �     �     �  	                  "     +     7  $   V  D   {  <   �     �               :     T  %   n  '   �  )   �  '   �  :     +   I  I   u  F   �  �     *   �  t   �  i   t  9   �  2       K	     X	     h	     {	  	   �	     �	     �	     �	     �	  #   �	  %   �	  I   
  M   M
     �
     �
     �
     �
      �
  .   
  -   9  0   g  5   �  <   �  2     O   >  C   �  �   �  1   �  �   �  w   X  ?   �             
   	                                                                                                                                %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-02 11:36+0100
Last-Translator: Daniel Nylander <po@danielnylander.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
 %A %e %B, %R %A %e %B, %R.%S %A %e %B, %I.%M %p %A %e %B, %I.%M.%S %p %A, %B %e %R %R.%S %l.%M %p %l.%M.%S %p CRTC %d kan inte driva utgången %s CRTC %d saknar stöd för rotation=%s CRTC %d: provar läget %dx%d@%d Hz med utmatning i %dx%d@%d Hz (pass %d)
 Kan inte hitta en terminal, använder xterm även om det kanske inte fungerar Bärbar dator Okänd RANDR-utökningen finns inte Provar lägen för CRTC %d
 kan inte klona till utgången %s kunde inte tilldela CRTC:er till utgångar:
%s kunde inte få tag på information om CRTC %d kunde inte få tag på information om utgång %d kunde inte få tag på information om skärmstorlekar kunde inte få skärmresurserna (CRTC:er, utgångar, lägen) kunde inte ställa in konfigurationen för CRTC %d ingen av de sparade skärmkonfigurationerna matchade den aktiva konfigurationen inga av de valda lägena var kompatibla med de möjliga lägena:
%s utgången %s har inte samma parametrar som en annan klonad utgång:
befintligt läge = %d, nytt läge = %d
befintliga koordinater = (%d, %d), nya koordinater = (%d, %d)
befintlig rotation = %s, ny rotation = %s utgången %s saknar stöd för läget %dx%d@%d Hz begärd position/storlek för CRTC %d är utanför den tillåtna gränsen: position=(%d, %d), storlek=(%d, %d), maximum=(%d, %d) nödvändig virtuell storlek passar inte tillgänglig storlek: nödvändig=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) ohanterat X-fel när information om skärmstorlekarna hämtades 