��    "      ,  /   <      �  
   �       ,   #     P  �   W     �     �     �     �          	          )     ?  	   K     U     Z  	   c     m     p     s          �     �     �     �  	   �  �   �     P     X     a      j     �  �  �  
   .  0   9  /   j     �  ^   �  	               	   $  	   .     8     ?     ]     y     �     �     �     �     �     �  
   �     �     �     �     �     	  
   	  �   	     �	  	   �	     �	  &   �	     �	                                                                                                          "             	                !            
           %1$s: %2$s Add or remove users and groups Allows Nemo to connect to an OwnCloud server Cancel Cinnamon is currently running without video hardware acceleration and, as a result, you may observe much higher than normal CPU usage.

 Close Control Center Copy Disabled Eject Error Execution of '%s' failed: Failed to launch '%s' File System Hide Text Home Keyboard Main Menu No OK Out of date Paste Print version Remove '%s' Remove this desklet Search Show Text There could be a problem with your drivers or some other issue.  For the best experience, it is recommended that you only use this mode for Unknown Version: Website: Wrong password, please try again Yes Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-10-18 20:33+0000
Last-Translator: Richard Schwartson <Unknown>
Language-Team: Faroese <fo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
 %1$s: %2$s Legg afturat ella tak burtur brúkarr og bólkar Loyv Nemo at fáa samband við OwnCloud servara Ógilda Cinnamon brúkarí løtuni ikki video hardware akseleration og tí kann CPU brukið økjast.

 Lat aftur Stýris-miðdepil Avrita Ógildað Kasta út Feilur Inning av '%s' miseydnaðist: Miseydnaðist at byrja '%s' Fílu kervið Krógva tekst Heim Knappaborð Høvuðsvalmynd Nei OK Útgingið Set inn Prenta útgávu Tak burtur '%s' Tak burtur hesa desklet Leita Vís tekst Tað er møguligt, at trupulleikar eru við dreivarum ella onkrum øðrum. Fyri at fáa tað besta burturúr, verður viðmælt bert at brúka støðu fyri Ókent Útgáva: Heimasíða: Skeivt loyniorð, vinarliga royn aftur Ja 