��    %      D  5   l      @     A     I  	   ]     g     z  (   �     �     �     �  
   �  *   �     #     2     G     Y     a     p     �     �     �     �     �  -   �     �  %        '     ?     M     \  
   a     l     �     �     �     �     �  t  �     )     0  	   L     V     s  0   �     �  	   �  )   �  
   �  +   	     5     D     d     �     �     �     �  &   �     �     �     		  5   (	     ^	  (   f	     �	     �	     �	     �	  
   �	     �	     �	     
     
  
   *
  
   5
           #                                        %          !                  	                                           "                                  
   $                    Address Always grant access Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants to pair with this computer Does not match Grant this time only Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing request for %s Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Send Files to Device... Send Files... Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-12-03 22:47+0000
Last-Translator: Hasan Salehov <Unknown>
Language-Team: Azerbaijani <az@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Ünvan Hər zaman bağlantı yarat Bluetooth Bluetooth Tənzimləmələri Bluetooth bağlıdır Bluetooth avadanlıq tərəfindən bağlanıldı Fayllara Bax... Ləğv et Bluetooth tənzimləmələrini quraşdır Bağlantı %s qurğusu  kompyuterə qoşulmaq istəyir Uyğunlaşmır Bu səfərlik  bağlantı yarat Klaviatura Tənzimləmələri Uyğunlaşır Siçan Tənzimləmələri Siçan və Taçbar Ayarları Yox Heç bir Bluetooth cihazı tapılmadı Oldu Birləşdirilmiş %s üçün birləşmə istəyi Lütfən, qurğuda göstərilən PIN kodu daxil edin. İmtina Cihazlar siyahısından '%s' silinsinmi? Qurğuya Fayl Göndər... Fayl Göndər... Səs Tənzimləmələri Növ Görünmə “%s” üçün görünmə Hə bağlanılır... bağlantı kəsilir... səhifə 1 səhifə 2 