��          D      l       �   &   �   �   �   �   <  �   �  �  m  5   :  �   p  �     �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:01+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Azerbaijani <translation-team-az@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Yardımın göstərilməsi sırasında xəta oldu: %s Az öncə Shift düyməsinə basıb 8 saniyə saxladınız.  Bu Yavaş Düymələr xassəsini fəallaşdırar, bu da klaviaturanızın işləmə tərzini dəyişdirər. Az öncə Shift düyməsinə arxa arxaya 5 dəfə basdınız.  Bu Yapışqan Düymələr xassəsini fəallaşdırar, bu da klaviaturanızın işləmə tərzini dəyişdirər. Az öncə iki düyməyə bərabər ya da Shift düyməsinə arxa arxaya 5 dəfə basdınız.  Bu Yapışqan Düymələr xassəsini fəallaşdırar, bu da klaviaturanızın işləmə tərzini dəyişdirər. 