��    Z      �     �      �     �     �     �     �     �                "  	   )     3     :     R     e     �     �     �     �     	     	     	     %	      :	      [	     |	     �	  	   �	  
   �	     �	  .   �	     �	     
  	   
     #
     *
     =
     Q
     e
     x
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
  	   �
     �
     �
     �
  
                  *     ;     C     [     k     }     �     �     �     �     �     �     �  _        h     t     �     �     �     �     �     �     �     �     �     �     �     �  #   �  !     (   >     g     �     �  	   �  �  �     a     i     q     ~     �     �     �     �     �  	   �     �  "     3   *  2   ^  5   �  5   �  7   �     5     L     Y     ^     |     �     �     �     �     �     �  1   �           4     8  
   F     Q     X     `  
   h     s     {     �     �  
   �     �     �     �     �     �     �     �     �               $     2     A     R     X     r  +   �     �     �     �     �     �     �     �     �  ]     
   b     m          �     �     �     �     �     �  	   �     �     �     �  
   �     �     �          (  	   /     9     A     @   !   5   F   I           .       +   P       "   R   C          V         ?                   Y   0   3   -      2   
                         *                %             	       X      B       (       <   N   A   E         #                      D          :                   T   7           &             )          K       >       1          W   O   M   ;   /       9   Z   =   L   ,   S   U   J   6   '             8         4   Q   $           H       G            %d Mb/s %d x %d %d x %d (%s) %i day ago %i days ago %i month %i months %i week %i weeks %i year %i years %s VPN All files Arabic Authentication required Available Profiles Available Profiles for Cameras Available Profiles for Displays Available Profiles for Printers Available Profiles for Scanners Available Profiles for Webcams British English Calibration China Chinese (simplified) Colorspace fallbackDefault CMYK Colorspace fallbackDefault Gray Colorspace fallbackDefault RGB Colorspace:  Connected Connecting Connection failed Create a color profile for the selected device Create virtual device DNS Default:  Device Device kindCamera Device kindPrinter Device kindScanner Device kindWebcam Disabled Disconnecting English France French German Germany IP Address IPv4 Address IPv6 Address Last used Layout Less than 1 week Monitor No profile Not connected Not specified Other profile… Russian Select ICC Profile File Select a region Set for all users Signal strengthExcellent Signal strengthGood Signal strengthOk Signal strengthWeak Spain Spanish Supported ICC profiles Test profile:  The measuring instrument is not detected. Please check it is turned on and correctly connected. Unavailable Uncalibrated United States Unknown Unspecified WEP WPA WPA2 Wi-Fi Network_Forget Wifi securityNone _Cancel _Close _Forget _Import display panel, rotation180 Degrees display panel, rotationClockwise display panel, rotationCounterclockwise display panel, rotationNormal never today yesterday Project-Id-Version: cinnamon-control-center.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-12-23 16:12+0000
Last-Translator: Nicat Məmmədov <n1c4t97@gmail.com>
Language-Team: Azerbaijani <translation-team-az@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
 %d Mb/s %d x %d %d x %d (%s) %i gün əvvəl %i gün əvvəl %i ay %i ay %i hәftә %i hәftә %i il %i il %s VPN Bütün fayllar Ərəbcə Təsdiqlənmə lazımdır İstifadə oluna bilən profillər Kameralar üçün istifadə oluna bilən profillər Ekranlar üçün istifadə oluna bilən profillər Printerlər üçün istifadə oluna bilən profillər Skaynerlər üçün istifadə oluna bilən profillər Veb-kameralar üçün istifadə oluna bilən profillər Britanya İngiliscəsi Kalibrləmə Çin Çincə (sadələşdirilmiş) Varsayılan CMYK Varsayılan Boz Varsayılan RGB Rəng qutusu:  Bağlanıldı Bağlanılır Bağlantı alınmadı Seçilmiş avadanlıq üçün rəng profili yarat Virtual cihaz yarat DNS Varsayılan:  Avadanlıq Kamera Printer Skayner Veb-kamera Bağlı Bağlantı kəsilir İngiliscə Fransa Fransızca Almanca Almanya IP Ünvanı IPv4 Ünvanı IPv6 Ünvanı Son istifadə Düzülüş 1 həftədən az Ekran Profil yoxdur Bağlanmayıb Bildirilməyib Başqa profil... Rusca ICC Profil Faylını Seç Bölgə seçin Bütün istifadəçilər üçün quraşdır Əla Yaxşı OK Zəif İspanya İspanca Dəstəklənən ICC profilləri Test profili:  Ölçü cihazı tapılmadı. Lütfən, doğru qoşulduğunu və yanılı olduğunu yoxlayın. Əlçatmaz Kalibrlənməmiş ABŞ Naməlum Müəyyənləşdirilməmiş WEP WPA WPA2 _Unut Heç biri _İmtina _Bağla _Unut _İdxal Et 180 Dərəcə Saat istiqamətində Saat istiqamətinin əksinə Normal heç vaxt bu gün dünən 