��    $      <  5   \      0     1     7     I  	   N     X  	   ]     g     �     �     �     �     �     �     �     �     �  	   �  	         
       	             8  	   =     G     M     _  	   q  	   {     �     �     �     �     �     �  a  �     8     >     T     ]     d     m  '   �     �     �     �     �     �  	   �     �     �               "     .     5     =  &   Q     x     }     �     �     �     �  
   �     �  #   �     �     �                                   #                                                      !   
                                                                           "       $         	       %H:%M %a, %b %d / %H:%M Calm Clear Sky Dust Duststorm Duststorm in the vicinity East East - NorthEast East - Southeast Invalid Moderate rain Moderate snow North North - NorthEast North - Northwest Northeast Northwest Rain Sand Sandstorm Sandstorm in the vicinity Snow Snowstorm South South - Southeast South - Southwest Southeast Southwest Unknown Unknown observation time Variable West West - Northwest West - Southwest Project-Id-Version: gnome-applets 1.2.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2002-05-14 12:10MET
Last-Translator: Pablo Saratxaga <pablo@mandrakesoft.com>
Language-Team: Walon <linux-wa@chanae.alphanet.ch>
Language: wa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
 %H:%M %a %d di %b / %H e %M Påjhule Vûcir Poussire Timpesse di poussire Timpesse di poussire dins l' vijhnaedje Esse Esse - Nôresse Esse - Sudesse Nén valåbe Plovinaedje Nivtaedje Nôr Nôr - Nôresse Nôr - Nôrouwesse Nôresse Nôrouwesse Plouve Såvlon Timpesse di såvlon Timpesse di såvlon dins l' vijhnaedje Nive Timpesse di nive Sud Sud - Sudesse Sud - Sudouwesse Sudesse Sudouwesse Nén cnoxhou Eure d' observåcion nén cnoxhowe Variåve Ouwesse Ouwesse - Nôrouwesse Ouwesse - Sudouwesse 