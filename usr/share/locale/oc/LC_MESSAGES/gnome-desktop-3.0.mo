��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     f     i     o     u     ~     �     �     �     �     �     �     �  +   �  -      M   N     �     �  &   �  1   �  G   "	  �   j	  3   P
  x   �
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: oc
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-13 17:00+0000
PO-Revision-Date: 2015-05-16 08:46+0200
Last-Translator: Cédric Valmary (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Tot en òc (totenoc.eu)
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 %R %R:%S %a %R %a %R:%S %a %e %b, %R %a %e %b, %R:%S %a %e %b, %l:%M %p %a %e %b, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p lo CRTC %d pòt pas pilotar una sortida  %s lo CRTC %d pren pas en carga la rotacion = %d CRTC %d : tèst del mòde %dx%d@%dHz amb una sortida a %dx%d@%dHz (passa %d)
 Tèsts dels mòdes pel CRTC %d
 Pas precisat impossible de clonar sus la sortida %s impossible d'assignar de CRTC a las sortidas :
%s cap dels mòdes causits es pas compatible amb los mòdes possibles :
%s La sortida %s possedís pas los meteisses paramètres que l'autra sortida clòna :
mòde actual = %d, mòde novèl = %d
coordenadas actualas = (%d, %d), coordenadas novèlas = (%d, %d)
rotacion actuala = %d, novèla rotacion = %d la sortida %s pren pas en carga lo mòde %dx%d@%dHz la talha virtuala demandada es pas adaptada a la talha disponibla : demanda=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 