��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     t     �  5   �     �  	         
  k   #  �   �       4   0                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-10-31 09:25+0200
Last-Translator: Cédric Valmary (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Tot en òc (totenoc.eu)
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Fichièrs publics de %s Fichièrs publics de %s sus %s Avia lo partiment de fichièrs personals s'es activat Parteja de fichièrs personals Partiment Paramètres de partiment Activar lo Partiment de fichièrs personals per partejar lo contengut d'aqueste dorsièr a travèrs la ret. Quora demandar de senhals. Las valors possiblas son : « never » (pas jamai), « on_write » (a l'escritura) e « always » (totjorn). Quora exigir los senhals partejar;fichièrs;http;ret;copiar;mandadís;mandar; 