��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     �	  	   �	     
     
  6   8
     o
     �
  ,   �
  	   �
  ,   �
  H   �
     @  2   O     �  G   �     �  
          *   &     Q  &   U  	   |     �     �     �  G   �  *        =  2   E     x     �     �     �     �     �  :   
     E     J     V     p     t     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-09-29 19:26+0000
Last-Translator: Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adreça Totjorn acordar l'accès Demanda d'autorizacion de %s Bluetooth Paramètres del Bluetooth Lo Bluetooth es desactivat Lo Bluetooth es desactivat per un interruptor material Percórrer los fichièrs... Anullar Configuracion dels paramètres del Bluetooth Connexion Lo periferic %s vòl accedir al servici '%s' Lo periferic %s a efectuat una demanda d'apairatge amb aqueste ordenador Correspond pas Una error s'es produita al moment de la navigacion Autorizar aqueste còp solament Se levatz lo periferic, vos lo cal reconfigurar per un usatge ulterior. Paramètres del clavièr Correspond Paramètres de la mirga Paramètres de la mirga e del pavat tactil Non Cap d'adaptador Bluetooth pas detectat D'acòrdi Apariat Confirmacion d'apairatge per %s Requèsta de ligason per %s Confirmatz que lo PIN '%s' correspond a lo qu'es afichat sul periferic. Entratz lo còdi PIN afichat sul periferic Refusar Suprimir « %s » de la lista dels periferics ? Suprimir un periferic Mandar de fichièrs ... Mandar los fichièrs... Configurar un periferic novèl Apondre un periferic ... Paramètres del son La navigacion es pas possibla per aqueste periferic : '%s' Tipe Visibilitat Visibilitat de « %s » Òc Connexion en cors... Desconnexion en cors... material desactivat pagina 1 pagina 2 