��          �            h  )   i     �  E   �  &   �  8     "   E  D   h  `   �          )  )   F  *   p     �     �  �  �  7        �  =   �  -     F   ;  '   �  Z   �  t        z  !   �  1   �  +   �       1   7                                 	                              
                 Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Message to be displayed in lock screen Query the length of time the screensaver has been active Query the state of the screensaver Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Version of this application You have the Caps Lock key on. Project-Id-Version: oc
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-01 17:23+0000
Last-Translator: Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Occitan <ubuntu-l10n-oci@lists.ubuntu.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Demanda a l'estalviador d'ecran de quitar corrèctament Verificacion en cors... Desactiva l'estalviador d'ecran s'es actiu (reaficha l'ecran) Messatge d'afichar sus l'ecran de verrolhatge Indica la durada pendent la quala l'estalviador d'ecran es estat actiu Indica l'estat de l'estalviador d'ecran Demanda a l'estalviador d'ecran en cors de foncionament de varrolhar l'ecran immediatament L'estalviador d'ecran es estat actiu pendent %d segonda.
 L'estalviador d'ecran es estat actiu pendent %d segondas.
 L'estalviador d'ecran es actiu
 L'estalviador d'ecran es inactiu
 Actualament, l'estalviador d'ecran es pas actiu.
 Avia l'estalviador d'ecran (escafa l'ecran) Version d'aquesta aplicacion Vòstre verrolhatge de las majusculas es activat. 