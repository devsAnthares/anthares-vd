��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  B   !
     d
  2   q
     �
     �
  �   �
          �  4   �  3   �  *     D   <     �  #   �  D   �  8   �  �   5  %   �  %   �            0     Q  5   i     �  6   �     �  1        3  7   M  6   �  
   �     �     �     �  .        @     [     q  $   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: oc
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-08 09:21+0000
PO-Revision-Date: 2011-08-28 07:36+0000
Last-Translator: Cédric VALMARY (Tot en òc) <cvalmary@yahoo.fr>
Language-Team: Occitan <ubuntu-l10n-oci@lists.ubuntu.com>
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2015-05-20 17:00+0000
X-Generator: Launchpad (build 17508)
 Grop per defaut, atribuit al moment de la creacion d'una fenèstra Indicador : Gerís e manten de gropes distinctes per fenèstra Agençament del clavièr Disposicion del clavièr Agençament de clavièr « %s »
Copyright &#169; X.Org Foundacion e contributors XKeyboardConfig
Los detalhs de licéncia se tròban dins las metadonadas del paquet. Modèl del clavièr Opcions del clavièr Cargar los agençaments e opcions utilizats rarament Cargar los objèctes de configuracion suplementaris Previsualizar los agençaments de clavièr Enregistra e restablís los indicadors amb los gropes d'agençaments Gropes segondaris Aficha las bandièras dins l'applet Aficha las bandièras dins l'applet per indicar l'agençament actual Aficha los noms d'agençament al luòc dels noms de grop Aficha los noms d'agençament al luòc dels noms de grop (unicament per las versions de XFree que prenon en carga los agençaments multiples) L'apercebut del clavièr, decalatge X L'apercebut del clavièr, decalatge Y L'apercebut del clavièr, nautor L'apercebut del clavièr, largor La color de rèire plan La color de rèire plan per l'indicador d'agençament La talha de la poliça La poliça de caractèrs per l'indicador d'agençament La talha de poliça La talha de poliça per l'indicador d'agençament La color de primièr plan La color de primièr plan per l'indicador d'agençament I a agut una error al moment de cargar un imatge : %s Desconegut Error d'inicializacion de XKB agençament de clavièr modèl del clavièr agençament « %s » agençaments « %s » modèl « %s », %s e %s pas cap d'agençament pas cap d'opcions opcion « %s » opcions « %s » 