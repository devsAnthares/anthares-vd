��    +      t  ;   �      �     �     �  	   �     �     �  (        1     A     H  
   e  *   p  *   �     �     �     �  K         L     ^     f     u     �     �     �     �     �     �  %   �               )     7     I     `     o  
   t          �     �     �     �     �     �  u  �     O  Z   b     �  '   �  <   �  c   5	  $   �	     �	  J   �	  $   
  `   D
  m   �
       N   /  ?   ~  �   �  4   �     �  '   �  T   �     G  <   ]     �     �  E   �     �  [        l  6   �     �  3   �  6     '   I     q     �  -   �     �  -   �  K     3   R     �     �           '                      *   +      )   #   (                "                                             !             
                       $             	                     &   %               Address Always grant access Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-04-29 17:33+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Lao <lo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 ທີ່ຢູ່ ອະນູຍາດການເຂົ້າເຖິງໃນທູກໆຄັເ້ງ ບລູທູດ ຕັ້ງຄ່າບລູທູດ ບລູທູດຖືກປິດການນຳໃຊ້ ບລູທູດຖືກປິດການນຳໃຊ້ໂດຍຮາດແວສະວິດ ເອີ້ນຫາແຟ້ມ... ຍົກເລີກ ກຳໜົດຄ່າ ການຕັ້ງຄ່າ ບລູທູດ ການເຊື່ອມຕໍ່ ອຸປະກອນ %s ຕ້ອງການເຂົ້າໃຊ້ບໍລິການ '%s' ອຸປະກອນ %s ຕ້ອງການຈັບຄຸ່ກັບຄອມພິວເຕີນີ້ ບໍ່ກົງກັນ ຂໍ້ຜິດພາດໃນການຊອກຫາອູປະກອນ ອະນຸຍາດຄັ້ງນີ້ຄັ້ງດຽວ ຖ້າເຈົ້າລຶບອູປະກອນ, ເຈົ້າຕ້ອງຕັ້ງຄ່າອີກເທື່ອກ່ອນການໃຊ້ໃນເທື່ອຕໍ່ໄປ. ການຕັ້ງຄ່າ ແປ້ນພິມ ກົງກັນ ຕັ້ງຄ່າເມົາສ໌ ການຕັ້ງຄ່າເມົ້າສ໌ແລະແຜງສຳຜັດ ບໍ່ແມ່ນ ບໍ່ພົບອະແດບເຕີບລູທູດ ຕົກລົງ Paired ກຳລັງຈຈັບຄຸເພື່ອຢືນຢັນ %s ປະຕິເສດ ລຶບ '%s' ອອກຈາກລາຍການຂອງອູປະກອນບໍ່? ລຶບອູປະກອນ ສົ່ງແຟ້ມໄປອູປະກອນ... ສົ່ງແຟ້ມ... ຕັ້ງຄ່າອຸປະກອນໃໝ່ ຕັ້ງຄ່າອູປະກອນໃໝ່... ການຕັ້ງຄ່າສຽງ ປະເພດ ການເຫັນໄກ ການເຫັນໄກຂອງ “%s” ແມ່ນ ກຳລັງເຊື່ອມຕໍ່... ກຳລັງຍົກເລີກການເຊື່ອມຕໍ່... ປິດໃຊ້ຮາດແວຣ໌ແລ້ວ ໜ້າ 1 ໜ້າ 2 