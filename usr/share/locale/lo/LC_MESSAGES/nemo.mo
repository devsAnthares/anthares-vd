��          \      �       �   *   �   1   �       &     G  i   O  /   �     �  �  �  ]   �  a   �  I   T     �    �  _   �  '                                          --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. Actions Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. Nemo could not create the required folder "%s". No actions found Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-07-21 17:08+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Lao <lo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 --check ບໍ່ສາມາດນໍາໃຊ້ມີທາງເລືອກອື່ນ. --geometry ບໍ່ສາມາດນໍາໃຊ້ຫຼາຍກ່ວາຫນຶ່ງ URI. --quit ບໍ່ສາມາດຖືກນໍາໃຊ້ກັບ URIs. ການກະທຳ ກ່ອນທີ່ຈະໃຊ້ Nemo, ກະລຸນາສ້າງໂຟເດີດັ່ງຕໍ່ໄປນີ້, ຫຼືກໍານົດການອະນຸຍາດດັ່ງກ່າວທີ່ Nemo ສາມາດສ້າງມັນ. Nemo ບໍ່ສາມາດສ້າງໂຟນເດີທີ່ຕ້ອງການ "%s". ບໍ່ພົບການກະທຳ 