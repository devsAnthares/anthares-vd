��          �      �        	   	       $   2  D   W  <   �     �     �     �       %   3  '   Y  )   �  '   �  :   �  +     I   :  F   �  �   �  *   �  t   �  i   9  9   �  I  �  	   '  P   1  S   �  u   �  �   L	     �	  -   �	  9   
  O   W
  b   �
  T   
  O   _  Y   �  W   	  H   a  �   �  �   I  �  �  u   �  �   �  �   �  Y   �                                         
                                                	               %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.gnome-2-32.lo
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-11-17 15:59+0700
Last-Translator: Anousak Souphavanh <anousak@gmail.com>
Language-Team: Lao <lo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
X-Generator: KBabel 1.11.4
 %A, %B %e CRTC %d ບໍ່ສາມາດຜົນອອກສະເເດງອອກ %s CRTC %d ບໍ່ສາມາດສະນົບສະໜູນມຸນວຽນ=%s CRTC %d: ທົດລອງເເບບ %dx%d@%dHz ກັບຜົນອອກທີ່ %dx%d@%dHz (ພ່ານ %d)
 ບໍ່ສາມາດຫາຈໍ, ໄຊ້ Xterm ເເທນ, ເຖີງມັນຈະບໍ່ທໍາງານກໍ່ຕາມ ເເລບທອບ ບໍ່ມີ RAND ໂປຣເເກຣມ ທົດລອງເເບບສະເພາະ CRTC %d
 ບໍ່ສາມາດລອອກເເບບໃສ່ ຜົນອອກ %s ບໍ່ສາມາດເເຕ່ງຕັ້ງໃຫ້ CRTCs ຕໍ່ຜົນອອກ:
%s ບໍ່ສາມາດ ເອົາຂໍ້ມູນກ່ຽວກັບ CRTC %d ບໍ່ສາມາດ ເອົາຂໍ້ມູນກ່ຽວກັບ %d ພິດພາດ ກັບ X ໃນເວລາປັບປູງຂະນານຈໍ  ບໍ່ສາມາດເອົາອຸປະກອນຈໍ (CRTCs, outputs, modes) ບໍ່ສາມາດ ຕັ້ງຄ່າສະເພາະ CRTC %d ບໍ່ມີຕັ້ງຄ່າທີ່ຖຶກບັນທຶກ ທີ່ກົງກັບ ຕັ້ງຄ່າທີ່ທໍາງານຢູ່ ບໍ່ມີເເບບໃດທີຖຶກເລືອຈະເຂົ້າກັບເເບບທີ່ເປັນໄປໃດ້:
%s ຜົນອອກ %s ບໍ່ມີຕົວປ່ຽນເສີມທີ່ເທົ່າກັບຜົນອອກທີ່ທຶກລອອກເເບ:
ເເບບມີເເລ້ວ = %d, ເເບບໄໝ່ = %d
ຕົວປະສານມີເເລ້ວ= (%d, %d), ຕົວປະສາບໄໝ່ = (%d, %d)
ການມຸນວຽນມີເເລ້ວ = %s, ການມຸນວຽນໄໝ່= %s ຜົນສະເເດງອອກ %s ບໍ່ສາມາດສະນົບສະໜູນເເບບ %dx%d@%dHz ຮ້ອງຂໍ ທີ່/ຂະນານ ສະເພາະ CRTC %d ເເມ່ນ ທີ່ໃດ້is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) ຂະໜາດທີ້ຈໍາເປັນບໍ່ສາມາດເຂົ້າກັບຂະໜາດທີ່ມີ: ຖຶກຮ້ອງຂໍ=(%d, %d), ຕໍາສຸດ=(%d, %d), ສູງສຸດ=(%d, %d) ພິດພາດ ກັບ X ໃນເວລາປັບປູງຂະນານຈໍ  