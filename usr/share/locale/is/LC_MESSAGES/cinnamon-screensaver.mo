��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  0   B  	   s  @   }     �  ,   �     �  <     '   X     �  5   �  `   �     )     ?  +   W  '   �     �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver_cinnamon-screensaver-is
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-02-06 16:01+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Veldur því að skjáhvílan hættir með sæmd Athuga... Ef skjáhvíla er virk, afvirkja hana þá (ekki tæma skjáinn) Rangt lykilorð Skilaboð sem á að birta á læsingarskjá Settu inn lykilorðið þitt... Senda fyrirspurn um tíma sem skjáhvílan hefur verið virk Senda fyrirspurn um stöðu skjáhvílu Skipta um notanda Skipa núverandi skjáhvílu að læsa skjánum strax Skjáhvílan hefur verið virk í %d sekúndu.
 Skjáhvílan hefur verið virk í %d sekúndur.
 Skjáhvílan er virk
 Skjáhvílan er óvirk
 Skjáhvílan er ekki virk í augnablikinu.
 Kveikja á skjáhvílu (tæma skjáinn) Aflæsa Útgáfa þessa forrits Þú ert að nota HÁSTAFI. 