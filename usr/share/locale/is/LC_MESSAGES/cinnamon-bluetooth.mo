��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	  "   
  	   *
     4
     I
  :   ^
     �
     �
     �
     �
  2   �
  )        ,     ;     W  e   k     �     �     �           &  !   *     L     T     [     y  F   �  /   �     
  /        @     Q     j     z     �     �  9   �     �  
   �            
        %     2     I     R                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: cinnamon-bluetooth_cinnamon-bluetooth-is
Report-Msgid-Bugs-To: Sveinn í Felli <sveinki@nett.is>
PO-Revision-Date: 2015-10-21 13:50+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Vistfang Veita alltaf aðgang Beðið er um auðkenningu frá %s Bluetooth Stillingar Bluetooth Bluetooth er óvirkt Bluetooth hefur verið gert óvirkt með vélbúnaðarrofa Skoða skrár... Hætta við Stilla Bluetooth Tenging Tækið %s vill fá aðgang að þjónustunni '%s' Tækið %s vill parast við þessa tölvu Samsvarar ekki Villa við að skoða tæki Leyfa aðeins núna Ef þú fjarlægir tækið, verður þú að setja það upp áður en þú getur notað það aftur. Lyklaborðsstillingar Samsvaranir Stillingar músar Stillingar músa og snertiplatta Nei Enginn Bluetooth búnaður fannst Í lagi Parað Staðfesting pörunar við %s Beiðni um pörun frá %s Staðfestu hvort PIN-númerið '%s' samsvarar því sem er á tækinu. Settu inn PIN-númerið sem birt er á tækinu. Hafna Viltu fjarlægja '%s‘ af listanum yfir tæki? Fjarlægja tæki Senda skrár á tæki... Senda skrár... Setja upp nýtt tæki Setja upp nýtt tæki... Hljóðstillingar Ekki er hægt að skoða gögn á tækinu, villan er '%s' Gerð Sýnileiki Sýnileiki "%s" Já tengist... aftengist... vélbúnaður óvirkur síða 1 síða 2 