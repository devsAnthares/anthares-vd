��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �     �     �     �  ;   �  C        \  $   h  ,   �  "   �  ,   �     
           5     N  #   T     x     �     �     �     �     �     �  	   �  !     !   *     L  7   X     �  )   �  
   �  _   �     .     <     L     f     s     {     �     �     �  -   �     �     
  #   %  /   I  %   y     �     �     �     �  $   �  t     q   �  Q   �     D  *   M  .   x     �  e   �  g   %      �  m   �          )  
   :     E  	   Q     [     =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-24 17:10+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <gnome@techattack.nu>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: is
  - Cinnamon setustjórinn Forrit er ennþá í gangi: AUTOSTART_DIR Þetta forrit tekur ekki á móti skrám á skipanalínunni Get ekki látið veffang ganga til 'Type=Link‘ skjáborðsfærslu Hætta við Ekki tókst að tengjast setustjóra Gat ekki búið til ICE hlustunarsökkul: %s Afvirkja tengingu við setustjóra Ekki hlaða inn forritum völdum að notanda Ekki spyrja notandann Virkja aflúsunarham Hætti með kóðanum %d SKRÁ Skráin er ekki gild .desktop skrá Leggjast samt í dvala ID Táknmynd ,%s' fannst ekki Hunsa allt sem gæti truflað Drepið með merkinu %d Læsa skjá Skrá samt út Skrá út Skrá út úr þessu kerfi núna? Þetta er ekki ræsanlegt atriði Svarar ekki Ekki nota sjálfgefna möppu fyrir sjálfvirka ræsingu Slökkva Kallað á forrit með röngum stillingum Endurræsa Leyfi biðlara ekki að tengjast vegna þess að það er verið að slökkva á þessari setu
 Munað forrit Endurræsa samt Endurræsa kerfið núna? SESSION_NAME _Hvíla Seta Valkostir setustýringar: Setan sem á að nota Sýna valkosti setustýringar Birta 'bilaða bilunargluggann' til prófunar Slökkva samt Slökkva á kerfinu núna? Nokkur forrit eru ennþá í gangi: Veldu skrá sem inniheldur vistaðar stillingar Tilgreinið auðkenni setustjórnunar Ræsi %s Stöðvað með merkinu %d Svæfa samt Skipta samt um notanda Þetta forrit hindrar útskráningu. Tölvan verður endurræst sjálfvirkt eftir %d sekúndu. This system will be automatically restarted in %d seconds. Tölvan slekkur sjálfvirkt á sér eftir %d sekúndu. This system will be automatically shut down in %d seconds. Tekst ekki að ræsa innskráningarsetu (og tekst ekki að tengjast X-þjóninum) Óþekkt Óþekkt .desktop skrá, útgáfa ‚%s‘ Þessi ræsivalmöguleiki er ekki þekktur: %d Útgáfa þessa forrits Leyfi forritunum að klára fyrst. Það að trufla þessi forrit við vinnu gæti valdið gagnatapi. Bíð eftir að forritið ljúki sér af. Ef forritið er truflað gætirðu tapað einhverjum gögnum. Þú ert skráður inn sem "%s". Þú verður skráður sjálfvirkt út eftir %d sekúndu. You will be automatically logged out in %d seconds. _Hætta við _Leggja í dvala S_krá út _Endurræsa _Slökkva _Skipta um notanda 