��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     n     q     w     }     �     �     �     �     �     �     �     �  @   �  <   8  X   u  1   �      	     	  *   ,	  @   W	  �   �	  0   Z
  c   �
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-02-08 07:58+0000
PO-Revision-Date: 2015-01-29 15:48+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %R %R:%S %a %R %a %R:%S %a %e. %b, %R %a %e. %b, %R:%S %a %e. %b, %l:%M %a %e. %b, %l:%M:%S %p %a %l:%M %a %l:%M:%S %p %H:%M %l:%M:%S %p CRTC skjástýringin %d getur ekki meðhöndlað úttakið á %s CRTC skjástýringin %d styður ekki snúninginn rotation=%d CRTC skjástýringin %d: prófa upplausn %dx%d@%dHz með úttak á %dx%d@%dHz (pass %d)
 Prófa upplausnir fyrir CRTC skjástýringuna %d
 Óskilgreint get ekki klónað úttakið %s gat ekki úthlutað CRTC-um á úttök:
%s enginn af völdum hömum var samhæfður við mögulega hami:
%s úttakið %s hefur ekki sömu viðföng og annað klónað úttak:
núverandi hamur = %d, nýr hamur = %d
núverandi hnit = (%d, %d), ný hnit = (%d, %d)
núverandi stefna = %d, ný stefna = %d úttakið %s styður ekki upplausnina %dx%d@%dHz umbeðin sýndarstærð passar ekki: umbeðin stærð=(%d, %d), lágmark=(%d, %d), hámark=(%d, %d) 