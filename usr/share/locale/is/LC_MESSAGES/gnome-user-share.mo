��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     S     f  +        �     �     �  V   �  �   2      �  '   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: 
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-02 16:55+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 Almenningsgögn %s Almenningsgögn %s á %s Opna einkaskráadeilingu ef þetta er virkt Deiling einkagagna Deiling Stillingar deilingar Kveiktu á deilingu einkagagna til að deila innihaldi þessarar möppu um netkerfið. Þegar spyrja þarf um lykilorð. Möguleikar eru „never“ (aldrei), „on_write“ (þegar skrifað er) og „always“ (alltaf). Hvenær skal krefjast lykilorðs deila;skrár;http;netkerfi;afrit;senda; 