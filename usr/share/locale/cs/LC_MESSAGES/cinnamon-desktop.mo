��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
  	   ,
     6
     <
     E
     R
  #   ^
     �
  Q   �
  Q   �
     F     O  	   c  $   m     �     �  &   �  !   �  %     )   ;  8   e  "   �  N   �  J     �   [  (   ?  r   h  z   �  E   V                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-16 20:13+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.0
 %A, %e. %B, %k:%M %A, %e. %B, %k:%M:%S %A, %e. %B, %l:%M %p %A, %e. %B, %l:%M:%S %p %A, %B %e %k:%M %k:%M:%S %A, %l:%M %p %l:%M:%S %p CRTC %d nemůže řídit výstup %s CRTC %d nepodporuje rotation=%s CRTC %d: zkouší se režim %d×%d@%dHz s výstupem na %d×%d@%dHz (průchod %d)
 Nelze nalézt terminál, používá se xterm, přestože možná nebude funkční Notebook Zrcadlené displeje Neznámý Rozšíření RANDR není přítomno Zkouší se režimy CRTC %d
 nelze klonovat na výstup %s k výstupům nelze přiřadit CRTC:
%s nelze získat informace o CRTC %d nelze získat informace o výstupu %d nelze získat rozsah velikostí obrazovky nelze získat zdroje obrazovky (CRTC, výstupy, režimy) nelze nastavit konfiguraci CRTC %d žádná z uložených konfigurací displeje neodpovídá aktivní konfiguraci žádný z vybraných režimů nebyl kompatibilní s možnými režimy:
%s výstup %s nemá parametry stejné jako další klonovaný výstup:
existující režim = %d, nový režim = %d
existující souřadnice = (%d, %d), nové souřadnice = (%d, %d)
existující otočení = %s, nové otočení = %s výstup %s nepodporuje režim %dx%d@%dHz požadovaná poloha/velikost CRTC %d je mimo povolený limit: poloha=(%d, %d), velikost=(%d, %d), maximum=(%d, %d) požadovaná virtuální velikost přesahuje dostupnou velikost: požadováno=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) nezpracovaná chyba X při získávání rozsahu velikostí obrazovky 