��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  K   Y  ;   �  @   �  ,   "     O     l     �  -   �  Z   �     )               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <translation-team-cs@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pro změnu nastavení přihlašovací obrazovky je vyžadována autentizace Pro změnu údajů o uživateli je vyžadována autentizace Pro změnu svých vlastních údajů je vyžadována autentizace Změnit nastavení přihlašovací obrazovky Změnit své vlastní údaje Povolit ladicí kód Spravovat uživatelské účty Zobrazit informace o verzi a ukončit program Poskytuje rozhraní D-Bus pro zobrazování a změny
informací o uživatelských účtech Nahradit současnou instanci 