��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h    �     �     �     �     �               5     L     h     w     �     �     �     �  
   �     �  #   �  "   	  Q   ;	     �	  	   �	     �	  &   �	  J   �	  �   C
  (   '  z   P                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-09-04 11:00+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Gtranslator 2.91.6
X-Project-Style: gnome
 %k∶%M %k∶%M∶%S %A, %k∶%M %A, %k∶%M∶%S %A, %-e. %B_%k∶%M %A, %-e. %B_%k∶%M∶%S %A, %-e. %B_%l∶%M %p %A, %-e. %B_%l∶%M∶%S %p %A, %l∶%M %p %A %l∶%M∶%S %p %e. %B_%k∶%M %-e. %B_%k∶%M∶%S %-e. %B_%l∶%M %p %-e. %B_%l∶%M∶%S %p %l∶%M %p %l∶%M∶%S %p CRTC %d nemůže řídit výstup %s CRTC %d nepodporuje otočení = %d CRTC %d: zkouší se režim %d×%d@%dHz s výstupem na %d×%d@%dHz (průchod %d)
 Zkouší se režimy CRTC %d
 Neurčeno nelze klonovat na výstup %s k výstupům nelze přiřadit CRTC:
%s žádný z vybraných režimů nebyl kompatibilní s možnými režimy:
%s výstup %s nemá parametry stejné jako další klonovaný výstup:
stávající režim = %d, nový režim = %d
stávající souřadnice = (%d, %d), nové souřadnice = (%d, %d)
stávající otočení = %d, nové otočení = %d výstup %s nepodporuje režim %dx%d@%dHz požadovaná virtuální velikost přesahuje dostupnou velikost: požadováno=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 