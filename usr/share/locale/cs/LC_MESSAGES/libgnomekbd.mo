��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j    �  5   �
     �
  9   �
     !     9  �   Q     �     �  ?     '   B     j  B   �     �     �  ?     4   A  u   v  "   �  "        2     O     m  %   {     �  %   �     �  '   �       '     ,   E  	   r     |     �     �  2   �     �     	       ,   (     U     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2016-08-15 10:36+0000
PO-Revision-Date: 2016-08-21 10:36+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: čeština <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Gtranslator 2.91.7
 Výchozí skupina, přiřazená při vytvoření okna Indikátor: U každého okna udržovat a spravovat oddělenou skupinu Rozložení klávesnice Rozložení klávesnice Rozložení klávesnice "%s"
Copyright &#169; X.Org Foundation a přispívající k XKeyboardConfig
Licencování viz metadata balíčku Model klávesnice Možnosti klávesnice Načíst exotická, zřídka užívaná rozložení a možnosti Nahrát dodatečné položky nastavení Náhled rozložení klávesnice Ukládat/obnovovat indikátory společně se skupinami rozložení Sekundární skupiny Zobrazovat v appletu vlaječky Ukazovat aktuální rozložení zobrazením vlaječek v appletu Zobrazovat názvy rozložení namísto názvu skupin Zobrazí názvy rozložení namísto názvů skupin (pouze u verzí XFree podporujících vícenásobná rozložení) Náhled klávesnice, umístění X Náhled klávesnice, umístění Y Náhled klávesnice, výška Náhled klávesnice, šířka Barva pozadí Barva pozadí indikátoru rozložení Rodina písma Rodina písma indikátoru rozložení Velikost písma Velikost písma indikátoru rozložení Barva popředí Barva popředí indikátoru rozložení Nastala chyba při načítání obrázku: %s Neznámé Chyba při spouštění XKB rozložení klávesnice model klávesnice rozložení "%s" rozložení "%s" rozložení "%s" model "%s", %s a %s bez rozložení bez možností možnost "%s" možnosti "%s" možností "%s" preferences-desktop-keyboard 