��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  -   x     �  )   �     �     �          8  	   A     K  1   k     �     �  
   �     �     �  "   	  	   *	     4	     T	  0   e	     �	     �	  x   �	      @
  ,   a
     �
  
   �
  -   �
  @   �
                                                        
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&keywords=I18N+L10N&component=general
PO-Revision-Date: 2012-03-15 18:16+0100
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 – Démon pro zpřístupnění myši v GNOME Styl tlačítka Styl tlačítka pro okno typů kliknutí. Geometrie okna typů kliknutí Orientace okna typů kliknutí Styl okna typů kliknutí Dvojklik Táhnutí Zapnout kliknutí posečkáním Zapnout simulované kliknutí pravým tlačítkem Selhalo zobrazení nápovědy Skrýt okno typů kliknutí. Vodorovně Kliknutí posečkáním Pouze ikony Ignorovat nepatrné pohyby kurzoru Orientace Orientace okna typů kliknutí. Druhé kliknutí Nastavit aktivní režim kliknutí posečkáním Vypnout mousetweaks Kliknutí levým tlačítkem Velikost a umístění okna typů kliknutí. Formát je v podobě standardního řetězce geometrie v systému X Window. Spustit mousetweaks jako démona Spustit mousetweaks v režimu přihlášení Text a ikony Pouze text Doba čekání před kliknutím posečkáním Doba čekání před simulovaným kliknutím pravým tlačítkem Svisle 