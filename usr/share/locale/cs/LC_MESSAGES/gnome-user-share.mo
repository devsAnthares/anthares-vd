��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  6  w     �  %   �  4   �     )  	   F     P  \   e  W   �       I   /                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-06 08:59+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Poedit-Language: Czech
X-Poedit-SourceCharset: utf-8
X-Generator: Gtranslator 2.91.6
 Veřejné soubory uživatele %s Veřejné soubory uživatele %s na %s Je-li povoleno, spustit Sdílení osobních souborů Sdílení osobních souborů Sdílení Nastavení sdílení Zapněte sdílení osobních souborů, abyste mohli sdílet obsah této složky přes síť. Kdy se ptát na hesla. Možné hodnoty jsou „never“, „on_write“ a „always“. Kdy vyžadovat hesla sdílet;sdílení;http;síť;kopírovat;kopírování;poslat;posílání; 