��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       v     �     �     �     �  /   �  9   �     !                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-10-16 13:01+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Gtranslator 2.91.6
 Zvukové CD Blu-ray DVD Digitální televize Nelze připojit %s. V mechanice zařízení „%s“ není médium. Zkontrolujte prosím, zda je v mechanice přítomen disk. Video CD 