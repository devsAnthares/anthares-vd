��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 n  %     �	     �	     �	  	   �	     �	     �	  *   
     .
     D
     K
     ^
  5   g
  6   �
     �
  #   �
       X        w  	   �     �     �     �  %   �     �     �     �       <   ;     x  
   �  (   �     �  !   �     �          +     I  <   Z     �     �      �     �     �     �  &   �                          /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-09-06 16:40+0000
Last-Translator: Pavel Borecki <Unknown>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresa Vždy povolit přístup Žádost o autorizaci od %s Bluetooth Nastavení Bluetooth Bluetooth je vypnuto Bluetooth je vypnuto fyzickým vypínačem Procházet soubory… Storno Nastavit Bluetooth Spojení Zařízení %s chce přistupovat ke službě „%s“ Zařízení %s se chce spárovat s tímto počítačem Nesouhlasí Chyba při procházení zařízení Umožnit pouze pro jednou Pokud zařízení odeberete, bude třeba ho před příštím použitím znovu nastavit. Nastavení klávesnice Souhlasí Nastavení myši Nastavení myši a touchpadu Ne Nenalezen žádný adaptér Bluetooth OK Spárováno Potvrzení spárování s %s Požadavek na spárování s %s Potvrďte zda je PIN „%s“ shodný s tím na zařízení. Opište PIN ze zařízení. Odmítnout Odebrat „%s“ ze seznamu zařízení? Odebrat zařízení Odeslat soubory do zařízení… Poslat soubory… Nastavit nové zařízení Nastavit nové zařízení… Nastavení zvuku Požadované zařízení nelze procházet, chyba je „%s“ Druh Viditelnost Viditelnost zařízení „%s“ Ano připojování… odpojování… vypnuto na fyzické (hardware) úrovni strana 1 strana 2 