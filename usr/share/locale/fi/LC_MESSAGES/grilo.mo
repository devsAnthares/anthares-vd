��          �      \      �  '   �     �  "        8     U     h     v     �     �     �     �     �  !   �           >     Q  %   p  (   �  #   �  �  �  8   �     �  &   �          2     H     W     k  $   �     �  &   �  &   �  %   	  +   /     [  #   r  (   �  %   �  0   �                          
                                                                  	       All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Data not available Grilo Options Invalid URL %s Invalid path %s Invalid plugin file %s Operation was cancelled Plugin not found: “%s” Plugin “%s” already exists Plugin “%s” is already loaded Plugin “%s” not available Show Grilo Options Some keys could not be written Source with id “%s” was not found The requested resource was not found: %s “%s” is not a valid plugin file Project-Id-Version: grilo master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/grilo/issues
PO-Revision-Date: 2018-09-02 11:56+0300
Language-Team: Finnish <lokalisointi-lista@googlegroups.com>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>
X-Generator: Poedit 2.0.6
 Kaikki määritetyt liitännäispolut ovat virheellisiä Tunnistautuminen vaaditaan: %s Välityspalvelimeen ei saada yhteyttä Palvelimeen ei saada yhteyttä Tietoja ei saatavilla Grilo-valinnat Virheellinen URL %s Virheellinen polku %s Virheellinen liitännäistiedosto %s Toimenpide peruttiin Liitännäistä ei löytynyt: “%s” Liitännäinen “%s” on jo olemassa Liitännäinen “%s” on jo ladattu Liitännäinen “%s” ei käytettävissä Näytä Grilo-valinnat Joitain avaimia ei voitu kirjoittaa Lähdettä id:llä “%s” ei löytynyt Pyydettyä resurssia ei löytynyt: %s “%s” ei ole kelvollinen liitännäistiedosto 