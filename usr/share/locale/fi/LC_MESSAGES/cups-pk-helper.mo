��          \      �       �      �      �            $     <  /   V      �  �  �  +   �     �  *   �     
  /   "  C   R  !   �                                       Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=cups-pk-helper
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2012-05-21 19:17+0000
Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>
Language-Team: Finnish (http://www.transifex.com/projects/p/freedesktop/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Lisää/poista/muokkaa paikallinen tulostin Lisää/poista/muokkaa tulostin Lisää, poista tai muokkaa etätulostinta Muuta tulostinasetuksia Käynnistä uudelleen, peru tai muokkaa työtä Käynnistä uudelleen, peru tai muokkaa toisen käyttäjän työtä Aseta tulostin oletustulostimeksi 