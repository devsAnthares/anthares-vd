��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  )   $     N  D   ^     �  '   �     �  A   �     6     V  X   k  o   �  $   4  (   Y  :   �  :   �     �     	  *    	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-04-13 18:15+0000
Last-Translator: Elias Ojala <Unknown>
Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: fi
 Sammuttaa näytönsäästäjän sulavasti Tarkistetaan… Sulje näytönsäästäjä, jos se on aktiivisena (palauta näyttö) Väärä salasana Lukitusnäytöllä näytettävä viesti Kirjoita salasanasi... Näytä, kuinka pitkään näytönsäästäjä on ollut päällä Kysy näytönsäästäjän tila Vaihda käyttäjää Käskee käynnissä olevan näytönsäästäjäprosessin lukita näyttö välittömästi Näytönsäästäjä on ollut käynnissä %d sekunnin.
 Näytönsäästäjä on ollut käynnissä %d sekuntia.
 Näytönsäästäjä on käynnissä
 Näytönsäästäjä ei ole käynnissä
 Näytönsäästäjä ei ole tällä hetkellä aktiivinen.
 Laita näytönsäästäjä päälle (tyhjentää näytön) Avaa lukitus Tämän sovelluksen versio Näppäimistössä on Caps Lock päällä. 