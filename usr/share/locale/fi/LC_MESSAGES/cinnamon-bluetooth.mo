��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 ~  %     �	     �	     �	  	   �	     �	     �	  9   
     I
     ]
     b
     |
  '   �
  9   �
     �
     �
       Z   *     �  	   �     �  !   �     �  !   �     �               +  =   H  $   �     �      �     �  !   �               0     D  0   T     �  	   �     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-12-03 01:51+0000
Last-Translator: Ari Ervasti <ari.ervasti87@gmail.com>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Osoite Myönnä pääsy aina Valtuutus pyyntö %s Bluetooth Bluetooth-asetukset Bluetooth ei ole päällä Bluetooth ei ole käytössä laitekytkimen asennon vuoksi Selaa tiedostoja... Peru Muuta Bluetooth-asetuksia Yhteys Laite %s haluaa pääsyn palveluun '%s' Laite %s haluaa liittyä tämän tietokoneen laitepariksi Ei täsmää Virhe selattaessa laitetta Myönnä vain tämän kerran Jos laite poistetaan, sen asetukset tulee tehdä uudelleen ennen seuraavaa käyttökertaa. Näppäimistön asetukset Täsmää Hiiriasetukset Hiiren ja kosketuslevyn asetukset Ei Bluetooth-sovittimia ei löytynyt OK Kytketty pariksi Laiteparin vahvistus %s Parituspyyntö laitteelta %s Varmista sopiiko PIN '%s' täsmällisesti laitteessa olevaan. Anna PIN, joka mainitaan laitteessa. Hylkää Poistetaanko '%s' laitelistasta? Poista laite Lähetä tiedostot laitteeseen... Lähetä tiedostoja... Asenna uusi laite... Aseta uusi laite... Ääniasetukset Pyydettyä laitetta ei voi selata, virhe on '%s' Tyyppi Näkyvyys Näkyvyys (%s) Kyllä luodaan yhteyttä... katkaistaan yhteys... laitteisto poissa käytöstä sivu 1 sivu 2 