��          L      |       �   /   �      �   =   �   +   5     a  �  i  5        D  F   d  A   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-17 21:03+0300
Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>
Language-Team: suomi <gnome-fi-laatu@lists.sourceforge.net>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
 Virhe (%s) datan muuntamisessa lapselle, putoaa pois. Virhe lapselta lukemisessa: %s. GnuTLS ei ole käytössä; tiedot kirjoitetaan levylle ilman salausta! Merkkien muuntaminen merkistöstä %s merkistöön %s ei onnistu. VAROITUS 