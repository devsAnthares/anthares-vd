��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     u  J     �	     �	     �	     �	     
     
     #
     ,
     5
      A
     b
  L   |
  H   �
  
          
   /  !   :     \     |  -   �     �      �  )     =   2  &   p  Q   �  R   �  �   <  %     s   1  p   �  B                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-14 19:17+0300
Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>
Language-Team: Finnish <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 %A %d.%m., %H.%M %A %d.%m., %H.%M.%S %A %d.%m., %I.%M %p %A %d.%m., %I.%M.%S %p %A, %e. %Bta %H.%M %H.%M.%S %l:%M %p %l:%M:%S %p CRTC %d ei voi ajaa ulostuloa %s CRTC %d ei tue kiertoa %s CRTC %d: yritetään tilaa %d×%d@%d Hz ulostulolla %d×%d@%d Hz (vaihe %d)
 Päätettä ei löytynyt, käytetään xtermiä vaikka se ei ehkä toimi Kannettava Peilatut näytöt Tuntematon RANDR-laajennos ei ole saatavilla Yritetään tiloja CRTC %d:lle
 ei voi kloonata ulostuloon %s ei voitu sijoittaa CRTC:itä ulostuloihin:
%s Tietoja CRTC:stä %d ei saatu. Tietoja ulostulosta %d ei saatu. näytön kokoluetteloa ei saatu noudettua Näyttöresurssien tietoja ei saatu (CRTCt, ulostuloa, tilat) Asetuksia CRTC:lle %d ei voitu asettaa yksikään tallennetuista näyttöasetuksista ei vastaa aktiivista määrittelyä mikään valituista tiloista ei ollut yhteensopiva mahdollisten tilojen kanssa:
%s ulostulolla %s ei ole samat parametrit kuin kloonatulla ulostulolla:
nykyinen tila = %d, uusi tila = %d
nykyiset koordinaatit = (%d, %d), uudet koordinaatit = (%d, %d)
nykyinen kierto = %s, uusi kierto = %s ulostulo %s ei tue tilaa %d×%d@%d Hz pyydetty sijainti tai koko CRTC:lle %d ei ole sallituissa rajoissa: sijainti=(%d,%d), koko=(%d,%d), maksimi=(%d,%d) vaadittu virtuaalinen koko ei mahdu saatavilla olevaan kokoon: pyydetty=(%d,%d), minimi=(%d,%d), maksimi=(%d,%d) käsittelemätön X-virhe noudettaessa kelvollisia näytön kokoja 