��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  "   k  2   �  >   �        	        !  f   2  �   �     #  L   ?                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-05 13:50+0300
Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>
Language-Team: suomi <gnome-fi-laatu@lists.sourceforge.net>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.6
 Käyttäjän %s julkiset tiedostot Käyttäjän %s julkiset tiedostot palvelimella %s Käynnistä henkilökohtainen tiedostojenjako, jos käytössä Omien tiedostojen jako Jakaminen Jakamisasetukset Ota henkilökohtainen tiedostojenjako käyttöön jakaaksesi tämän kansion sisällön verkon kautta. Milloin kysytään salasanoja. Mahdolliset arvot ovat: ”never” (ei koskaan), ”on_write” (kirjoitettaessa) ja ”always” (aina). Milloin vaaditaan salasanat share;files;http;network;copy;send;jako;jakaminen;verkko;lähetys;kopiointi; 