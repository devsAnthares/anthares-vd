��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  B   W  7   �  =   �  $        5  %   U     {     �  I   �  "                   	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Timo Jyrinki <timo.jyrinki@gmail.com>
Language-Team: Finnish (http://www.transifex.com/projects/p/freedesktop/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Kirjautumisnäkymän asetusten muuttaminen vaatii tunnistautumisen Käyttäjätietojen muuttaminen vaatii tunnistautumisen Omien käyttäjätietojen muuttaminen vaatii tunnistautumisen Muuta kirjautumisnäkymän asetuksia Muuta omia käyttäjätietojasi Ota käyttöön virheenjäljityskoodi Hallitse käyttäjätilejä Näytä versiotiedot ja lopeta Tarjoaa D-Bus-rajapinnan käyttäjätilitietojen kyselyyn
ja käsittelyyn Korvaa käynnissä oleva instanssi 