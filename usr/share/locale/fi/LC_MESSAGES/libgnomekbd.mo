��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j    �  (   �
  	   �
  3   �
     �
       �   -     �     �  5   �     %  #   <  5   `     �     �  ?   �  .     f   3  &   �  &   �  #   �  "        /     ;     Z     f     �     �     �     �     �  
   �               ,  $   B     g     �     �  ,   �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2017-04-27 15:27+0000
PO-Revision-Date: 2017-09-07 14:54+0300
Last-Translator: Jir Grönroos <jiri.gronroos+l10n@iki.fi>
Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
 Oletusryhmä, asetettu ikkunaa luotaessa Ilmaisin: Säilytä ja hallitse erillisiä ryhmiä ikkunoille Näppäimistöasettelu Näppäimistöasettelu Näppäinasettelu ”%s”
Tekijänoikeudet &#169; X.Org Foundation ja XKeyboardConfig-avustajat
Katso lisensointiehdot paketin metatiedoista Näppäimistön malli Näppäimistön lisävalinnat Lataa harvemmin käytettyjä asetteluita ja valintoja Lataa lisäasetteluita Esikatsele näppäimistöasetteluja Tallenna ja palauta ilmaisimet asetteluryhmien mukana Toissijaiset ryhmät Näytä liput sovelmassa Näytä sovelmassa lippu ilmaisemaan tämänhetkistä asettelua Näytä asettelujen nimet ryhmänimien sijasta Näytä asettelujen nimet ryhmänimien sijasta (vain XFree-versiolla, jotka tukevat useita asetteluja) Näppäimistön esikatselu, X-sijainti Näppäimistön esikatselu, Y-sijainti Näppäimistön esikatselun korkeus Näppäimistön esikatselun leveys Taustaväri Asetteluilmaisimen taustaväri Fonttiperhe Asetteluilmaisimen fonttiperhe Fontin koko Asetteluilmaisimen fontin koko Edustaväri Asetteluilmaisimen edustaväri Virhe ladattaessa kuvaa: %s Tuntematon XKB-alustusvirhe näppäimistön asettelu näppäimistön malli asettelu ”%s” asettelut ”%s” malli ”%s”, %s ja %s ei asettelua ei lisävalintoja lisävalinta ”%s” lisävalinnat ”%s” preferences-desktop-keyboard 