��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �          "  
   1     <     J     [     o     x      �     �  L   �     	     ,	     ?	  -   ]	  R   �	  �   �	  %   �
  p   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-11-03 12:02+0200
Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>
Language-Team: suomi <gnome-fi-laatu@lists.sourceforge.net>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
 %R %R:%S %a %R %a %R:%S %a %-e. %b_%R %a %-e. %b_%R:%S %a %-e. %b_%l:%M %p %a %-e. %b_%I.%M.%S %p %a %I.%M %p %a %I.%M.%S %p %-e. %b_%R %-e. %b_%R:%S %b %-e._%I.%M %p %-e. %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d ei voi ajaa ulostuloa %s CRTC %d ei tue kiertoa=%d CRTC %d: yritetään tilaa %d×%d@%d Hz ulostulolla %d×%d@%d Hz (vaihe %d)
 Yritetään tiloja CRTC %d:lle
 Määrittämätön ei voi kloonata ulostuloon %s ei voitu sijoittaa CRTC:itä ulostuloihin:
%s mikään valituista tiloista ei ollut yhteensopiva mahdollisten tilojen kanssa:
%s ulostulolla %s ei ole samat parametrit kuin kloonatulla ulostulolla:
nykyinen tila = %d, uusi tila = %d
nykyiset koordinaatit = (%d, %d), uudet koordinaatit = (%d, %d)
nykyinen kierto = %d, uusi kierto = %d ulostulo %s ei tue tilaa %d×%d@%d Hz vaadittu virtuaalinen koko ei mahdu saatavilla olevaan kokoon: pyydetty=(%d,%d), minimi=(%d,%d), maksimi=(%d,%d) 