��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  W   8  I   �  1   �  ,     f   9  p   �  \     �   n  |   �  T   u     �  �   �  G   b  G   �  (   �  (     K  D  (   �  4   �  �   �  `   �        B   ,  �   o  %     Q   .  �   �  i   $  �   �  M   �  M   �  B   *  H   m  0   �  Y   �  3   A  [   u  2   �  \     ,   a  T   �  Z   �  �   >  o   �  P   5     �  ,   �  O   �  (     (   B  )   k  +   �     �  +   �  5                       !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master.te
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2009-09-17 19:07+0530
Last-Translator: Hari Krishna <hari@swecha.net>
Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);


X-Generator: KBabel 1.11.4
 ఎక్కువ ప్లగ్ఇన్ల ను చేతనంచేయుము ప్లగ్ఇన్ల ను చేతనంచేయుము(_p): ప్లగ్ఇన్ జతచేయుము డైలాగ్ ను మూయుము ఎంపికచేసిన ప్లగ్ఇన్ ను ఆకృతీకరించుము ఎంపికచేసిన ప్లగ్ఇన్ ను క్రియాహీనం చేయుము ప్లగ్ఇన్ ప్రాముఖ్యతను తగ్గించుము అప్రమేయ సమూహం, విండో సృష్టీకరణనందు అప్పగించబడింది సంస్థాపించిన ప్లగ్ఇన్లను చేతనం/అచేతనం చేయుము ప్లగ్ఇన్ ప్రాముఖ్యత ను పెంచుము సూచిక: ప్రత్యేక సమూహాన్ని విండోకు నిర్వహించు మరియు ఉంచు కీబోర్డు సూచిక ప్లగ్ఇన్లు కీబోర్డు సూచిక ప్లగ్ఇన్లు కీబోర్డు నమూనా కీబోర్డు నమూనా కీబోర్డు నమూనా "%s"
కాపీరైట్ &#169; X.Org వ్యవస్థాపన మరియు Xకీబోర్డుఆకృతీకరణ సహాయకులు
లైసెన్సింగ్ కొరకు ప్యాకేజి మెటాడాటా ను చూడండి కీబోర్డు నమూనా కీబోర్డు ఐచ్చికాలు అసాధారణమైన, అరుదుగా వుపయోగించు నమూనాలను మరియు ఐచ్చికాలను లోడుచేయుము అదనపు ఆకృతీకరణ అంశములను లోడుచేయుము ఏ వివరణలేదు. కీబోర్డు నమూనా ఉపదర్శనం  నమూనా సమూహాలతో సంయుక్తంగా దాయు/తిరిగినిల్వఉంచు సూచికలు రెండవ సమూహాలు ఆప్లెట్ నందు ప్లాగ్సను చూపుము ప్రస్తుత నమూనాను సూచించుటకు ఆప్లెట్ నందు ప్లాగ్స్ ను చూపుము సమూహనామాల బదులుగా నమూనానామాలను చూపుము సమూహనామాల బదులుగా నమూనానామాలను చూపుము (XFree మద్దతిచ్చు బహుళ నమూనాల యొక్క వర్షన్‌ల కొరకు మాత్రమే) కీబోర్డు ఉపదర్శనం, X ఆఫ్‌సెట్ కీబోర్డు ఉపదర్శనం, Y ఆఫ్‌సెట్ కీబోర్డు ఉపదర్శనం, ఎత్తు కీబోర్డు ఉపదర్శనం, వెడల్పు పూర్వ రంగ వర్ణాలు  కూర్పు సూచిక కు పూర్వరంగ వర్ణాలు  అక్షరశైలి కుటుంబం   కూర్పు సూచిక కు అక్షరశైలి కుటుంబం అక్షరశైలి పరిమాణం  కూర్పు సూచిక కు అక్షరశైలి పరిమాణం  దృశ్యరంగం వర్ణం  కూర్పు సూచిక కుదృశ్యరంగం వర్ణం క్రియాశీల ప్లగ్ఇన్ల యొక్క జాబితా చేతనపరిచిన కీబోర్డు సూచిక ప్లగ్ఇన్ల యొక్క జాబితా చిత్రమును నింపుటకు అక్కడ ఒక్క దోషం ఉంది: %s సహాయపు దస్త్రాన్ని తెరువలేదు తెలియని XKB సిద్దీకరణ దోషం అందుబాటులో ఉన్న ప్లగ్ఇన్లు(_A): కీబోర్డు నమూనా కీబోర్డు నమూనా నమూనా "%s" నమూనా "%s" నమూనా "%s", %s మరియు %s ఏ నమూనాలేదు ఏ ఐచ్చికాలులేవు ఐచ్చికం "%s" ఐచ్చికం "%s" 