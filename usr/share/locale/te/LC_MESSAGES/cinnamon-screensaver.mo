��          L      |       �   `   �      
     %  )   B     l  ;  �  �   �  T   �  Z   �  k   C  -   �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master.te
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-08-26 12:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: te
 తెరకాపరి %d సెకను క్రియాశీలముగా ఉంది.
 తెరకాపరి %d సెకన్లు క్రియాశీలముగా ఉంది.
 తెరకాపరి క్రియాశీలముగా ఉన్నది
 తెరకాపరి అక్రియాత్మకముగా ఉన్నది
 తెరకాపరి ప్రస్తుతం క్రియాశీలముగా లేదు.
 Caps Lock key ఆన్‌లో ఉంది 