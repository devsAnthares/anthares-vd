��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  �        �	     �	     �	     �	     �	     �	     �	     
  Z   
  H   o
  �   �
  �   a  !   +  I   M     �  .   �  S   �  n   0  p   �  h     y   y  p   �  i   d  e   �  �   4  �   �  �  �  Y   l  �   �  �   �  �   �             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master.te
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-20 23:43+0530
Last-Translator: Praveen Illa <mail2ipn@gmail.com>
Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: te
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Gtranslator 2.91.5
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d %s అవుట్ పుట్ ని ఇవ్వలేకపోతుంది  CRTC %d %s పరిభ్రమమును సహకరించదు CRTC %d:  %dx%d@%dHz మోడ్ ను  %dx%d@%dHz అవుట్ పుట్ వద్ద ప్రయత్నిస్తున్నాము (పాస్ %d)
 xపదమును వుపయోగించి, టెర్మినల్‌ను కనుగొనలేదు, వుపయోగించినా పనిచేయక పోవచ్చు ల్యాప్‌టాప్ ప్రతిబింబించిన ప్రదర్శనలు తెలియదు RANDR పొడిగింపు లేదు CRTC %d కి రీతులను ప్రయత్నిస్తునాం
 %s అవుట్‌పుట్‌కి క్లోన్ చేయలేకపోతున్నాము అవుట్‌పుట్లను CRTCల స్థానమివ్వలేనున్నాము:
%s CRTC %d గురించి సమాచారమును పొందలేక పోయింది అవుట్పుట్ %d గురించి సమాచారము పొందలేక పోయింది తెర పరిమాణముల విస్తృతిని పొందలేక పోయింది తెర మూలములు (CRTCs, outputs, modes)ను పొందలేక పోయింది CRTC %d కొరకు ఆకృతీకరణను అమర్చలేక పోయింది దాయబడిన ప్రదర్శన ఆకృతీకరణలు క్రియాశీల ఆకృతీకరణతో సరిపోలలేదు ఎంపిక చేసిన రీతులలో సంభవించే రీతులకు అనుసంధానమైన రీతులు లభించలేదు:
%s %s అవుట్‌పుట్‌కి ఇంకో క్లోన్ అయిన అవుట్‌పుట్‌లా అదే విధమైన పరామితిలు లేవు:
ప్రస్తుత రీతి = %d, కొత్త రీతి =%d
ప్రస్తుతసమన్వయం  = (%d,%d), కొత్త సమన్వయం = (%d,%d)
ప్రస్తుత భ్రమణం=%s, కొత్త భ్రమణం = %s అవుట్ పుట్ %s %dx%d@%dHz రీతిని సహకరించదు CRTC %d కొరకు అభ్యర్దించిన స్థానము/పరిమాణం అనుమతించిన పరిమితికి బయటవుంది: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) కావలిసిన వర్చ్యువల్ పరిమాణము అందుబాటులో వున్న పరిమాణముకు సరిపోదు: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) తెర పరిమాణముల యొక్క విస్తృతిని పొందుచున్నప్పుడు సంభాలించని X దోషము 