��    ?        Y         p     q  .   �  >   �  *   �  1   (      Z  ,   {  o   �  i        �  Q   �     �     �                 2   -     `     ~     �     �     �     �  /   �  2   �     	  
   +	  (   6	     _	  
   }	     �	     �	     �	      �	  -   �	  -   
  3   2
  5   f
  8   �
  &   �
  +   �
     (  J   H     �  %   �  �   �  F   ^  #   �     �     �     �     �  F   �  G   =     �     �     �     �     �     �     �     �  �  �  (   q  C   �  7   �  B     C   Y  .   �  ;   �  q     �   z       m     0   �     �     �     �     �  ;   �  ,   %     R     ^     l     �  	   �  F   �  =   �     0     H  @   W  #   �     �     �     �  	   �        9   )  9   c  @   �  D   �  F   #  ,   j  7   �     �  \   �     I  "   e  �   �  G   -  ,   u     �     �     �     �  N   �  T   2     �     �     �     �     �     �     �     �     #                  /   6      7          !                +             1   2       <       &          ;      8   *   5   "            )      .       $   9   (   :   =              ,                                            0                      >   ?   3       %          4       
   '                   -           	    

Add connect to server mount 

Browse the file system with the file manager "%s" could not be found. Perhaps it has recently been deleted. --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. C_onnect Can't load the supported server method list.
Please check your gvfs installation. Cannot find the autorun program Command Comment Connect to Server Connecting... Create the initial window with the given geometry. Delete all items in the Trash Description Desktop E_mpty Trash Empty Trash GEOMETRY Nemo could not create the required folder "%s". Only create windows for explicitly specified URIs. Operation cancelled Pass_word: Perform a quick set of self-check tests. Print but do not open the URI Quit Nemo. Secure WebDAV (HTTPS) Server Details Sh_are: Show the version of the program. Sorry, could not change the group of "%s": %s Sorry, could not change the owner of "%s": %s Sorry, could not change the permissions of "%s": %s Sorry, could not display all the contents of "%s": %s The desktop view encountered an error while starting up. The desktop view encountered an error. The folder contents could not be displayed. The group could not be changed. The name "%s" is already used in this folder. Please use a different name. The owner could not be changed. The permissions could not be changed. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There is no "%s" in this folder. Perhaps it was just moved or deleted? There was an error displaying help. URL User Details WebDAV (HTTP) Windows share You do not have the permissions necessary to change the group of "%s". You do not have the permissions necessary to view the contents of "%s". [URI...] _Domain name: _Port: _Remember this password _Run _Server: _Type: _User name: Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-02-18 15:53+0000
Last-Translator: Ueliton <Unknown>
Language-Team: Filipino <fil@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 

Magdagdag ng koneksyon sa mount server 

Tingnan ang kayarian ng file, na may mga tagapamahala ng mga file "%s" hindi mahanap. Siguro ito ay natanggal kamakailan. --tseke hindi maaaring gamitin kasama ng iba pang mga pagpipilian. --heometrya hindi maaaring gamitin kasama ng higit pa sa isang URI. --paghinto hindi maaaring magamit sa mga URIs. <big><b>Error sa mga awtomatikong programa gawain</b></big> <big><b>Media na ito ay naglalaman software upang awtomatikong magsimula. Gusto mo bang patakbuhin ito?</b></big> Bago tumakbo ang mga Nemo, mangyaring lumikha ng mga sumusunod na lalagyan, o itakda ang mga pahintulot sa paraan na ang Nemo maaaring lumikha nito. I_kabit Hindi ma-load ang pamamaraan ng mga suportadong listahan server.
Mangyaring suriin ang iyong istraktura gvfs. Hindi mahanap ang gawain iskema mga awtomatikong Magmando Puna Kumonekta sa Server Sa pagkonekta... Lumikha ng inisyal na bintana na may ibinigay na heometrya. Tanggalin ang lahat ng mga item sa Basurahan Deskripsyon Pook ng Likha A_lisan ng laman ang Trash Alisan ng laman ang Trash HEOMETRYA Nemo ay hindi maaaring lumikha ng mga kinakailangang na lalagyan "%s". Lumikha lamang ng mga bintana sa sa mga URI tahasang tinukoy. Kinansela ang operasyon Kontra_senyas: Magsagawa ng mabilis na hanay ng mga pagsubok na sarili pagpili. I-print ngunit hindi buksan ang URI Lumabas mula Nemo. Insurance WebDAV (HTTPS) Mga Detalye ng Server Pa_litan: Ipakita ang bersyon ng programa. Paumanhin, hindi maaaring baguhin ang pangkat ng "%s": %s Paumanhin, hindi maaaring baguhin ang may-ari ng "%s": %s Paumanhin, hindi maaaring baguhin ang mga pahintulot ng "%s": %s Paumanhin, hindi maaaring ipakita ang lahat ng nilalaman ng "%s": %s Ang tanawin ng work area nakatagpo ng error sa panahon ng pagsisimula. Ang tanawin ng work area nakaranas ng error. Ang mga nilalaman ng lalagyan hindi maaaring matingnan. Ang grupo ay hindi mababago. Ang pangalan na "%s" ay ginagamit na sa folder na ito. Mangyaring gumamit ng ibang pangalan. Ang may-ari hindi mababago. Hindi mababago ang mga pahintulot. Tatakbo software ang direkta mula sa media "%s". Hindi ka dapat patakbuhin ang software na hindi mo pinagkakatiwalaan.

Kung may pagdududa, pindutin ang Kanselahin. Walang "%s" sa folder na ito. Marahil ito ay inilipat lang o natanggal? Nagkaroon ng error sa pagpapakita ng tulong. URL Mga detalye ng User WebDAV (HTTP) Pagbabahagi sa mga Windows Wala kang mga kinakailangang mga pahintulot upang baguhin ang pangkat ng "%s". Wala kang mga kinakailangang mga pahintulot upang tingnan ang mga nilalaman ng "%s". [URI...] _Pangalan ng domain: _Pinto: _Tandaan ang kontrasenyas _ Gawin ang operasyon _Server: _Uri: _Gumagamit: 