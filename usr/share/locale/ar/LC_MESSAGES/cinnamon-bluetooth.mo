��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     
     #
     4
      T
  H   u
     �
  
   �
  *   �
       >     E   ]     �  !   �     �  �   �  (   �     �     �  1   �       '   	  
   1  
   <  %   G  !   m  �   �  M        d  >   k     �  0   �     �          -     M  I   g  
   �     �     �     �     �  !         "  
   A  
   L                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-04-09 18:30+0000
Last-Translator: انور الاسكندرانى <Unknown>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ar_EG
 العنوان منح الوصول دائما %s طلب التفويض من البلوتوث إعدادات البلوتوث تم تعطيل البلوتوث تم تعطيل البلوتوث عن طريق تبديل الأجهزة تصفح الملفات... إلغاء تكوين إعدادات البلوتوث الإتصال الجهاز %s يريد الوصول إلى الخدمة '%s' الجهاز %s يريد الإقتران بهذا الكمبيوتر غير متطابق خطأ في تصفح الجهاز منح هذة المرة فقط إذا قمت بإزالة الجهاز، فسيتعين عليك إعداده مرة أخرى قبل الاستخدام التالي. إعدادات لوحة المفاتيح متطابق إعدادات الماوس إعدادات الفأرة ولوحة اللمس لا لا توجد محولات بلوتوث موافق مقترن %s تأكيد الإقتران إلى %s طلب الاقتران إلى الرجاء تأكيد ما إذا كان رقم التعريف '%s' يتطابق مع الرقم الموجود على الجهاز. يرجى إدخال رقم التعريف المذكور على الجهاز. رفض هل تريد إزالة '%s' من قائمة الأجهزة؟ إزالة الجهاز إرسال الملفات إلى الجهاز... إرسال الملفات... إعداد جهاز جديد إعداد جهاز جديد... إعدادات الصوت لا يمكن تصفح الجهاز المطلوب، الخطأ هو '%s' النوع مرئي “%s” مرئي إلى نعم جاري الإتصال... جاري قطع الإتصال... تم تعطيل الأجهزة صفحة 1 صفحة 2 