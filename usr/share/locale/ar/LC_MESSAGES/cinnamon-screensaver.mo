��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  8  r  =   �     �  r     '   u  ;   �  >   �  \     6   u     �  Q   �  �  	     �
  &   �
  4   �
  :        W      m  '   �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-09 18:31+0000
Last-Translator: انور الاسكندرانى <Unknown>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 && n % 100 <= 99 ? 4 : 5;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ar_EG
 يؤدي إلى شاشة التوقف للخروج بأمان جاري التحقق... إذا كانت شاشة التوقف نشطة، قم بإلغاء تنشيطها (الشاشة غير فارغة) كلمة المرور غير صحيحة الرسالة ليتم عرضها في شاشة القفل يرجى إدخال كلمة المرور الخاصة بك... الاستعلام عن المدة التي كانت فيها شاشة التوقف نشطة الاستعلام عن حالة شاشة التوقف تبديل المستخدم أخبر تشغيل شاشة التوقف لقفل الشاشة على الفور شاشة التوقف كانت نشطة لمدة %d ثانية.
 شاشة التوقف كانت نشطة لمدة %d ثانية.
 شاشة التوقف كانت نشطة لمدة %d ثواني.
 شاشة التوقف كانت نشطة لمدة %d ثواني.
 شاشة التوقف كانت نشطة لمدة %d ثواني.
 شاشة التوقف كانت نشطة لمدة %d ثواني.
 شاشة التوقف نشطة
 شاشة التوقف غير نشطة
 شاشة التوقف ليست نشطة حاليا.
 تشغيل شاشة التوقف (الشاشة فارغة) إلغاء القفل إصدار هذا التطبيق مفتاح Caps Lock لديك يعمل. 