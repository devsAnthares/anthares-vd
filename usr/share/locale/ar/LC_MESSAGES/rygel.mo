��    H      \  a   �         #   !     E     S     h     n     u     y     �     �  D   �  ,   �  '     !   >     `  '   {  !   �     �  .   �  '        ;  %   S     y  #   �  2   �     �  !   	      *	     K	     \	     b	     t	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     0
     ?
     V
     i
     �
      �
  	   �
  	   �
  !   �
  "   �
       /   '      W     x     �     �     �     �     �  `   �  "   N     q     �  *   �     �     �     �        	          �  )  7   �     �          4     C     V     _     l  8   }  j   �  O   !  J   q  0   �     �  :     '   B  -   j  ;   �  @   �       <   ,  $   i      �  B   �     �  %     )   8     b  
   z     �     �     �     �     �          )     B     [  !   h     �     �  !   �     �     �  "     %   &  <   L     �     �  :   �  D   �     7  _   >  3   �     �  7   �  
   )     4  #   N     r     �  %        ,     E  N   Z  #   �  
   �  ,   �  
     
     #            3              '      $   %            :   0   C   /          5   "                  ?   ;   A   	      
       1   -                         >   @                    9      )           E                       G      H   .   6   (   *       4   D                  =      7   !       #         &   2   ,      8   +          F   B   <                     A module named %s is already loaded Action Failed Add shared directory Album Albums All Artist Artists Comments not allowed in XML Enable sharing of media, such as photos, videos and music, with DLNA Error fetching object '%s' from database: %s Error removing object from database: %s Failed to add item with ID %s: %s Failed to browse '%s': %s
 Failed to create preferences dialog: %s Failed to destroy object '%s': %s Failed to get file info for %s Failed to get log level from configuration: %s Failed to get original URI for '%s': %s Failed to link %s to %s Failed to query content type for '%s' Failed to remove entry: %s Failed to roll back transaction: %s Failed to save configuration data to file '%s': %s Failed to search in '%s': %s Failed to start Rygel service: %s Failed to stop Rygel service: %s GStreamer Player Genre Invalid Arguments Invalid Channel Invalid Name Invalid Range '%s' Invalid Request Invalid URI '%s' Invalid argument Invalid range Music New plugin '%s' available No subtitle available No such file transfer No such object No thumbnail available No value available No value available for '%s' No value available for '%s/%s' No writable URI for %s available Not Found Not found Object creation in %s not allowed Object removal from %s not allowed Pictures Pushing data to non-empty item '%s' not allowed Removal of object %s not allowed Remove shared directory Requested item '%s' not found Rygel Rygel Preferences Seek mode not supported Select folders Select the network interface that DLNA media will be shared on, or share media on all interfaces Successfully destroyed object '%s' UPnP/DLNA Preferences UPnP/DLNA Services URI '%s' invalid for importing contents to Value of '%s' out of range Videos XML node '%s' not found. Year _Network: _Share media through DLNA Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-22 14:42+0300
Last-Translator: os_ <syriatalks@hotmail.com>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Lokalize 1.4
 ثمة وحدة مسماة %s ومحملة بالفعل فشل الإجراء أضف دليلا مشتركا الألبوم الألبومات الكل الفنان الفنانين من غير المسموح بالتعليقات في XML فعّل مشاركة الوسائط، كالفيديو والصور والموسيقى, بواسطة DLNA خطأ أثناء جلب الكائن '%s' من قاعدة البيانات: %s خطأ أثناء إزالة كائن من قاعدة البيانات: %s فشل إضافة عنصر بالهوية %s: %s فشل تصفح '%s': %s
 فشل إنشاء مربع حوار التفضيلات: %s فشل تدمير الكائن '%s': %s فشل جلب معلومات الملف ل %s فشل جلب مستوى السجل من التضبيط: %s قشل الحصول على الرابط الأصلي ل '%s': %s فشل ربط %s ب %s فشل الاستعلام عن نوع المحتوى ل '%s' فشل إزالة المدخلة: %s فشل عكس العملية: %s فشل حفظ بيانات التضبيط في الملف '%s': %s قشل البحث في '%s': %s فشل بدء خدمة رايجل: %s فشل إيقاف خدمة رايجل: %s مشغل جستريمر النوع معطيات غير صالحة قناة غير صالحة اسم غير صالح مجال غير صالح '%s' طلب غير صالح رابط غير صالح '%s' معطى غير صالح مجال غير صالح موسيقى مقبس جديد متوفر '%s' لا ترجمة متوفرة   لا يوجد كائن مماثل لا مصغَّر متوفر لا قيمة متوفرة لا قيمة متوفرة ل '%s' لا قيمة متوفرة ل '%s/%s' لا رابطا قابلا للكتابة متوفرا ل %s لم يعثر عليه لم يعثر عليه إنشاء الكائنات في %s غير مسموح به  من غير المسموح به إزالة الكائنات من %s صور دفع البيانات إلى العنصر الغير الفارغ '%s' غير مسموح به إزالة الكائن %s غير مسموح بها أزل دليلا مشتركا العنصر المطلوب '%s' لم يعثر عليه رايجل تفضيلات رايجل وضع السعي غير مدعوم حدد مجلدات حدد واجهة شبكة التي ستُشارك بها وسائط DLNA, أو شارك الوسائط بكل الواجهات دُمر الكائن '%s' بنجاح تفضيلات UPnP/DLNA خدمات UPnP/DLNA الرابط '%s' غير صالح لاستيراد المحتويات إليه قيمة '%s' خارج المجال فيديو لم يعثر على عقدة ال XML '%s'. السنة _شبكة: شارك الوسائط عبر DLNA 