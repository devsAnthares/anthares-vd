��          �   %   �      0     1     >     N     a  	   w     �     �     �     �  <   �     �     �     �       '   +  )   S  '   }  :   �  +   �  I     t   V  i   �  9   5  �  o     E     S     d     x  	   �     �     �     �     �  j   �     "     8     R  $   _  -   �  5   �  0   �  T   	  (   n	  ^   �	  �   �	  �   �
  R   7                                                                  	                                    
                         %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD.ar
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-05 07:17+0200
Last-Translator: Khaled Hosny <khaledhosny@eglug.org>
Language-Team: Arabic <doc@arabeyes.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Virtaal 0.7.0
X-Project-Style: gnome
 %A %e %B، %R %A %e %B، %R:%S %A %e %B، %l:%M %p %A %e %B، %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p تعذّر العثور على طرفية، سأستعمل xterm، حتى وإن كان قد لا يعمل حاسوب محمول شاشات متطابقة مجهولة امتداد RANDR غير موجود تعذّر جلب معلومات عن CRTC %d تعذّر جلب معلومات عن الخرْج %d تعذّر جلب مدى أحجام الشاشة تعذّرت معرفة موارد الشاشة (CRTC، الخرج، الأوضاع) تعذّر ضبط إعدادات CRTC %d لم يطابق أي من تضبيطات العرض المحفوظة التضبيط النشط الموضع\الحجم المطلوب ل‍ CRTC %d خارج المدى المسموح: الموضع=(%d، %d)، الحجم=(%d، %d)، الأقصي=(%d، %d) الحجم الافتراضي المطلوب لا يطابق الحجم المتاح: المطلوب=(%d، %d)، الأدنى=(%d، %d)، الأقصى=(%d، %d) حدث خطأ X غير معروف أثناء جلب مدى أحجام الشاشة 