��          L      |       �   `   �      
     %  )   B     l  ~  �  V   
  2   a  8   �  I   �  )                                            The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-03-05 11:15+0000
Last-Translator: majidnavabi <majidnavabi65@gmail.com>
Language-Team: Persian <translate@ifsug.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: IRAN, ISLAMIC REPUBLIC OF
Language: fa
X-Poedit-Language: Persian
X-Poedit-SourceCharset: utf-8
 محافظ صفحه‌نمایش به مدت %Id ثانیه فعال بوده است.
 محافظ صفحه‌نمایش فعال است.
 محافظ صفحه‌نمایش غیرفعال است.
 محافظ صفحه‌نمایش در حال حاظر فعال نیست.
 دکمه Caps Lock شما روشن است 