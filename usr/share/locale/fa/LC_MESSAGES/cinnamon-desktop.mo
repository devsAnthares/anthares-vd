��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  <        ]	     o	     �	     �	     �	     �	  
   �	     �	  X   �	  Z   4
  �   �
  �        �  $   �     �  %     W   5  B   �  R   �  U   #  C   y  I   �  �     Q   �  �   �  }   �  .     H   /  �   x  �   Z  p                	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-06 20:48+0330
Last-Translator: Arash Mousavi <mousavi.arash@gmail.com>
Language-Team: Persian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
X-Poedit-SourceCharset: UTF-8
 %A %Od %B %OH:%OM %A %Od %B %OH:%OM:%OS %A %Od %B %OH:%OM %A Od% %B %OH:%OM:%OS %OH:%OM %OH:%OM:%OS %OI:%OM %p %OI:%OM:%OS %p کنترلر سی‌آر‌تی %d نمی‌تواند خروجی %s را بگرداند کنترلر سی‌آر‌تی %d از چرخاندن=%s پشتیبانی نمی‌کند کنترلر سی‌آر‌تی %d: در حال امتحان کردن حالت %dx%d@%dHz با خروجی در %dx%d@%dHz (گذر %d )
 پایانه‌ای یافت نمی‌شود، با استفاده از مقلد پایانه در سیستم پنجرهٔ ایکس، حتی اگر ممکن است کار نکند لپ‌تاپ نمایشگرهای آینه‌ای ناشناخته پسوند RANDR موجود نیست. در حال آزمودن حالت‌ها برای کنترلر سی‌آر‌تی %d  
 کپی‌برابر به خروجی %s انجام نمی‌شود. کنترلرهای سی‌آر‌تی به خروجی‌ها منتسب نشد:
%s اطلاعات درباره‌ی کنترلر سی‌آر‌تی %d گرفته نشد اطلاعات درباره‌ی خروجی %d بدست نیامد. گسترهٔ اندازه‌های صفحه نمایش گرفته نشد. منابع صفحه نمایش گرفته نشد(کنترل کننده‌ی سی‌آر‌تی، خروجی‌ها، حالت‌ها) پیکر‌بندی برای کنترلر سی‌آر‌تی %d تنظیم نشد هیچ‌کدام از پیکربندی‌های ذخیره شده برای صفحهٔ نمایش با پیکربندی فعال فعلی مطابقت نیافت. هیچ‌کدام از حالت‌های انتخاب شده با حالت‌های ممکن، سازگار نبودند: 
%s خروجی %s دارای پارامتر‌های همانند با خروجی دیگری (که کپی‌برابر است) نیست: 
حالت موجود = %d ، حالت جدید= %d
مختصات موجود = (%d, %d), مختصات جدید = (%d, %d)
چرخش موجود =  %s, چرخش جدید = %s خروجی %s از حالت %dx%d@%dHz پشتیبانی نمی‌کند. موقعیت/اندازه‌ی درخواست شده برای کنترل‌کننده‌ی سی‌آر‌تی %d در خارج از حد مجاز است: موقعیت=(%d, %d) ، , اندازه=(%d, %d), بیشینه=(%d, %d) اندازه‌ی مجازی مورد نیاز با اندازه‌ی در دسترس جور نمی‌شود: درخواست شده= (%d, %d) ، کمینه= (%d, %d)، بیشینه= (%d, %d) خطای تصدی نشدهٔ ایکس هنگام گرفتن گسترهٔ اندازه‌ی صفحه‌نمایش 