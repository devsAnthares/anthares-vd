��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  #   [  +     d   �  4     #   E  (   i  �   �  �   >  )   �  j   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-13 12:12+0430
Last-Translator: Arash Mousavi <mousavi.arash@gmail.com>
Language-Team: Persian
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.4
 پرونده‌های عمومی %s پرونده‌های عمومی %s در %s اجرای به اشتراک گذاری شخصی پرونده‌ها در صورت فعال بودن به اشتراک‌گذاری شخصی پرونده به‌اشتراک‌گذاری‌ تنظیمات اشتراک‌گذاری برای اشتراک‌گذاری محتویات این پوشه بر روی شبکه «اشتراک‌گذاری پرونده‌های شخصی» را روشن کنید. چه زمانی برای گذر‌واژه سوال شود. مقادیر ممکن عبارتند از «never»، «on_write»، و «always» زمان لازم بودن گذرواژه اشتراک;پرونده‌ها;http;شبکه;رونوشت;ارسال;share;files;http;network;copy;send; 