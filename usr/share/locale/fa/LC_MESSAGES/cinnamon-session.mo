��  :   0      S   �          $  4      �     �     �  5   �  7   %     ]  (   d  )   �  %   �  '   �  "        (     >  !   C     e     v     y      �     �     �     �     �     �       '     	   :  '   D     l  P   s     �     �     �     	     	     	     5	     D	  &   d	     �	     �	      �	  +   �	     
     "
     .
     =
      P
  E   q
     �
  &   �
  X   �
  X   ?  $   �  
   �     �     �  
   �     �  �  �  ;   �     �  I   �  j   *     �  3   �  D   �  <     M   X  9   �  >   �       A   ,  :   n  
   �     �  L   �     !  ;   :     v  <   �  >   �       J        h  W   |     �  s   �  "   g  C   �  0   �     �       *     %   C  F   i  F   �  A   �  7   9  A   q  O   �  7        ;  +   S  :     M   �  {        �  ;   �  �   �  �   �  T   q     �     �     �          .              0                          )                    %       1      +      .   9      /      ,                 7              
      8   4            	                 "   '         3                         :       -   !               $   (      #   5   &         2               6      *          E  D  P  \  h  t  �  �  �  G     ����[     ����o     �����     �����            �����            �����            ����  <          ���� A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session 2.10.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:18+0000
Last-Translator: Arash <Unknown>
Language-Team: Persian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: fa_IR
X-Poedit-SourceCharset: utf-8
 یک برنامه همچنان در حال اجرا است: AUTOSTART_DIR برنامه سند‌ها را در خط فرمان نمی‌پذیرد. نشانی سند به یک ورودی میزکار «نوع=پیوند» گذر داده نمی‌شود. انصراف اتصال به مدیر نشست ممکن نیست ایجاد سوکت گوش‌کننده‌ی ICE ممکن نشد: %s از کار انداختن اتصال به مدیر نشست برنامه‌های مشخص شده توسط کاربر اجرا نشوند برای تصدیق هویت کاربر هشدار نده کد اشکال‌زدایی به کار انداخته شود پرونده پرونده یک پرونده‌ی .desktop معتبر نیست. در هر حال خواب زمستانی انجام شود شناسه نقشک «%s» پیدا نشد هرگونه مهارکننده‌های خروج را نادیده بگیر قفل کردن صفحه در هر حال خروج از سیستم انجام شود خروج از سیستم هم‌اکنون از این سیستم خارج شویم؟ این، یک مورد قابل راه‌اندازی نیست نا پاسخگو شاخه‌های استاندارد آغاز خودکار، لغو شود خاموش کردن برنامه با گزینه‌های ناسازگار فراخوانده شده است راه‌اندازی مجدد چون سیستم در حال خاموش شدن است درخواست اتصال کارگیر رَد می‌شود
 بخاطر سپردن برنامه در هر حال سیستم مجددا راه‌اندازی شود سیستم هم‌اکنون خاموش شود؟ SESSION_NAME _تعلیق گزینه‌های مدیریت نشست: نشستی که استفاده شود گزینه‌های مدیریت نشست نمایش داده شوند نمایش بالن محاوره شکست جهت آزمایش کردن در هر حال خاموش کردن سیستم صورت گیرد این سیستم هم‌اکنون خاموش شود؟ چند برنامه همچنان در حال اجرا هستند: مشخص کردن پرونده‌ی شامل پیکربندی ذخیره شده شناسه مدیریت نشست را مشخص کنید در حال آغاز %s در هر حال سیستم معلق شود در هر حال، تعویض کاربر انجام شود این برنامه جلوی خروج از سیستم را گرفته است. ناتوان در آغاز نشست ورود به سیستم (و ناتوان در اتصال به کارگزار ایکس) ناشناخته نسخه‌ی پرونده میزکار ناشناس «%s» در حال انتظار برای برنامه‌ها که به پایان برسند. وقفه انداختن در این برنامه‌ها ممکن است باعث شود که کار شما از دست برود. در حال انتظار برای برنامه که به پایان برسد. وقفه انداختن در برنامه‌ها ممکن است باعث شود که کار شما از دست برود. هم‌اکنون شما با عنوان «%s» وارد سیستم شده‌اید. _خواب زمستانی _خروج از سیستم _راه‌اندازی مجدد خامو_ش کردن _تعویض کاربر I Exited with code %d Killed by signal %d Stopped by signal %d Unrecognized launch option: %d با کد %d خارج شد با سیگنال %d کشته شد با سیگنال %d متوقف شد گزینه‌ی راه‌اندازی ناشناخته‌: %d 