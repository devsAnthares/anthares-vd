��          �   %   �      @     A  	   I     S  (   i     �     �  
   �  K   �          (     D     G     c  %   j     �     �     �     �     �  
   �     �     �     �     �  �       �     �  "   �  N   �       ,   1  
   ^  �   i  "   �  ,        E  4   L     �  2   �     �  !   �     �          -     4     L     k     r     ~                                                                                             
                   	              Address Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found Paired Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-11-01 19:33+0000
Last-Translator: Babak Alizadeh <alizadeh.babak@gmail.com>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 آدرس بلوتوث بلوتوث غیر فعال شد. بلوتوث از طریق کلید سخت‌افزاری قطع شده است نمایش پرونده ها... پیکربندی تنظیمات بلوتوث اتصال اگر دستگاه را حذف کنید، برای استفاده مجدد باید ابتدا آن را دوباره تنظیم کنید. تنظیمات صفحه‌کلید تنظیمات موس و صفحه‌لمسی خیر هیچ آداپتور بلوتوثی پیدا نشد همگام شد آیا %s از لیست وسایل حذف شود؟ حذف دستگاه ارسال پرونده‌ها... نصب دستگاه جدید تنظیمات صدا نوع قابلیت روئیت قابلیت روئیت «%s» آری صفحه ۱ صفحه ۲ 