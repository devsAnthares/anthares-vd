��          L      |       �   /   �      �   =   �   +   5     a  �  i  `   �  -   K  v   y  K   �  
   <                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GNUTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-09-13 14:55+0430
Last-Translator: Arash Mousavi <mousavi.arash@gmail.com>
Language-Team: Persian
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.9
 خطا (%s) در هنگام تبدیل داده برای فرزند. درحال رها کردن. خطا در خواندن از فرزند: %s. GNUTLS فعال نیست؛ اطلاعات بدون رمزنگاری بر روی دیسک ذخیره خواهند شد! نمی‌توان نویسه‌ها را از %s به %s تبدیل کرد. هشدار 