��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  1   
     3
  8   ?
     x
     �
  �   �
     ;     O  >   c  *   �  '   �  >   �     4     H  G   f  6   �  k   �  (   Q  (   z  '   �  &   �     �  #   �     "  #   .     R  %   `     �  ,   �  -   �  	   �                6  A   J     �     �  
   �  6   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: lv
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-09-24 15:23+0300
PO-Revision-Date: 2012-09-24 15:30+0300
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 1.4
 Noklusējuma grupa, ko piesaista loga veidošanai Indikators: Uzturēt un pārvaldīt atsevišķas grupas katram logam Tastatūras izkārtojums Tastatūras izkārtojums Tastatūras izkārtojums “%s”
Autortiesības &#169; X.Org Fonds un XKeyboardConfig komanda
Licences informācija pieejama pakotnes metadatos Tastatūras modelis Tastatūras opcijas Ielādēt eksotiskus, reti izmantotus izkārtojumus un opcijas Ielādēt papildu konfigurācijas vienumus Priekšskatīt tastatūras izkārtojums Saglabāt / atjaunot indikatorus kopā ar izkārtojuma grupām Sekundārās grupas Rādīt karogus sīklietotnē Rādīt karogus sīklietotnē, lai norādītu pašreizējo izkārtojumu Grupu nosaukumu vietā rādīt izkārtojumu nosaukumus Grupu nosaukumu vietā rādīt izkārtojumu nosaukumus (XFree versijām, kas uztur vairākus izkārtojumus) Tastatūras priekšskatījums, X nobīde Tastatūras priekšskatījums, Y nobīde Tastatūras priekšskatījums, augstums Tastatūras priekšskatījums, platums Fona krāsa Izkārtojuma indikatora fona krāsa Fontu saime Izkārtojuma indikatora fontu saime Fonta izmērs Izkārtojuma indikatora fonta izmērs Priekšplāna krāsa Izkārtojuma indikatora priekšplāna krāsa Gadījās kļūda, ielādējot attēlu — %s Nezināms XKB inicializācijas kļūda tastatūras izkārtojums tastatūras modelis izkārtojums “%s” izkārtojumi “%s” izkārtojumu “%s” modelis “%s”, %s un %s nav izkārtojuma nav opciju iespēja “%s” iespējas “%s“ iespēju “%s” 