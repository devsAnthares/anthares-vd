��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S    w     �     �  =   �      �          $  T   @  y   �       ,   &                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-02-06 17:06+0200
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 2.0
 %s publiskās datnes %s publiskās datnes uz %s Palaist personisko datņu koplietošanu (ja tā ir ieslēgta) Personiskā datņu koplietošana Koplietošana Koplietošanas iestatījumi Ieslēgt personīgo datņu koplietošanu, lai koplietotu šīs mapes saturu tīklā. Kad prasīt paroles. Iespējamās vērtības ir “never” (nekad), “on_write” (rakstot) un “always” (vienmēr). Kad pieprasīt paroles koplietot;datnes;http;tīkls;kopēt;sūtīt; 