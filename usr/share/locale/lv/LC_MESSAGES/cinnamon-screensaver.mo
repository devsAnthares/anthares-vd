��          L      |       �   `   �      
     %  )   B     l  ,  �  �   �     M      l  )   �  '   �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: lv
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-01-25 13:54+0000
Last-Translator: Jānis Marks Gailis <jm-gailis@fai-vianet.fr>
Language-Team: Latvian <lata-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Ekrānsaudzētājs ir bijis aktīvs %d sekundi.
 Ekrānsaudzētājs ir bijis aktīvs %d sekundes.
 Ekrānsaudzētājs ir bijis aktīvs %d sekundes.
 Ekrānsaudzētājs ir aktīvs
 Ekrānsaudzētājs ir neaktīvs
 Ekrānsaudzētājs pašlaik nav aktīvs.
 Jums ir ieslēgts Caps Lock taustiņš. 