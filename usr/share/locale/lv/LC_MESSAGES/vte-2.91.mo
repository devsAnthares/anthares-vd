��          L      |       �   /   �      �   =   �   +   5     a  �  i  /   )     Y  ?   v  0   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: lv
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-31 21:06+0200
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 2.0
 Kļūda (%s) konvertējot datus bērnam, pamet. Kļūda lasot no bērna: %s. GnuTLS nav ieslēgti; dati diskā tiks ierakstīti nešifrēti! Neiespējami pārveidot rakstzīmes no %s uz %s. BRĪDINĀJUMS 