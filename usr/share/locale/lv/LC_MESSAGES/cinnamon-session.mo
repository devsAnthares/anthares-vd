��    ?        Y         p     q     �  5   �  7   �     	  (     )   9  %   c  '   �  "   �     �     �     �  !        %     6     9      M     n     �     �     �     �     �     �  '   �  	     '        @  P   G     �     �     �     �     �     �     		     	  &   8	     _	     p	      �	  +   �	     �	     �	     
     
     &
      9
  E   Z
     �
  &   �
     �
     �
  X   
  X   c  $   �  
   �     �     �  
   �     	  �       �     �  -      ?   .     n  )   u  /   �  .   �  -   �  $   ,     Q     o     �      �     �     �     �  "   �     �          +     @  !   N     p     �  4   �     �  0   �     �  ?        L     a     u     �     �     �     �  $   �  $        (     8  %   O  .   u  !   �  
   �     �     �     �  "     <   =  	   z  ,   �  "   �     �  h   �  h   V  .   �     �     �       	        $     "       4   -   .       :   0   	           9       !                     7                 #   8   6             ;                               +          /   >      5             $             2       ,   ?       (              *       <                       &   '              =         
   3      )   %      1          A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: lv
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:17+0000
Last-Translator: Rūdolfs Mazurs <Unknown>
Language-Team: Latvian <lata-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: lv
 Programma joprojām darbojas: AUTOSTARTA_DIR Lietotne nepieņem dokumentus no komandrindas Nevar padot dokumenta URI darbvirsmas ierakstam “Type=Link” Atcelt Nevar savienoties ar sesiju pārvaldnieku Nevar izveidot ICE klausīšanās ligzdu — %s Deaktivēt savienojumu ar sesiju pārvaldnieku Neielādēt lietotāja norādītās lietotnes Neprasīt lietotāja apstiprinājumu Aktivēt atkļūdošanas kodu Izgāja ar kodu %d DATNE Datne nav derīga .desktop datne Tomēr sastindzināt ID Ikona “%s” nav atrasta Ignorējot jebkādus šķēršļus Pārtraukts ar signālu %d Noslēgt ekrānu Tomēr izrakstīties Izrakstīties Izrakstīties no sistēmas tagad? Nav palaižams vienums Neatbild Aizstāt standarta automātiskā starta direktorijas Izslēgt Programma izsaukta ar konfliktējošām opcijām Pārstartēt Atsaka jauniem klientu savienojumiem, jo sesija tiek izslēgta
 Iegaumētā lietotne Tomēr pārstartēt Izslēgt datoru tagad? SESIJAS_NOSAUKUMS Iesna_udināt Sesiju pārvaldības opcijas: Sesija, ko izmantot Rādīt sesiju pārvaldības opcijas Rādīt neveiksmju vali testēšanai Tomēr izslēgt Izslēgt datoru tagad? Dažas programmas joprojām darbojas: Norādiet datni, kur saglabāta konfigurācija Norādiet sesiju pārvaldības ID Palaiž %s Apturēts ar signālu %d Tomēr iesnaudināt Tomēr nomainīt lietotāju Programma bloķē izrakstīšanos. Nevar ierakstīties sesijā (nevar savienoties ar X serveri) Nezināms Neatpazīta .desktop datnes versija “%s” Neatpazīta palaišanas opcija: %d Šīs programmas versija Gaida programmu darbības beigas. Programmu pārtraukšana var novest pie darba rezultātu zaudēšanas. Gaida programmu darbības beigas. Programmu pārtraukšana var novest pie darba rezultātu zaudēšanas. Šobrīd jūs esat ierakstījies kā “%s”. S_astindzināt _Izrakstīties Pā_rstartēt Iz_slēgt _Nomainīt lietotāju 