��          �   %   �      @     A  	   I     S  (   i     �     �  
   �  K   �          (     D     G     c  %   j     �     �     �     �     �  
   �     �     �     �     �  �       �  	   �     �  /   �     �  #   �       q   (     �  '   �     �     �  	   �  "        +     ;     M     e     z  
        �     �  
   �  
   �                                                                                             
                   	              Address Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Configure Bluetooth settings Connection If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found Paired Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-05-03 19:42+0000
Last-Translator: Juris Daikteris <juris.daikteris@gmail.com>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adrese Bluetooth Bluetooth ir atslēgts Bluetooth ir deaktivēts ar aparatūras slēdzi Pārlūkot datnes... Konfigurēt Bluetooth iestatījumus Savienojums Ja izņemsiet ierīci no saraksta, nākošajā reizē jums tā būs jāiestata vēlreiz, lai to varētu izmantot. Tastatūras iestatījumi Peles un skārienpaliktņa iestatījumi Nē Nav atrasti Bluetooth adapteri Sapārots Izņemt '%s' no ierīču saraksta? Izņemt ierīci Sūtīt datnes... Iestatīt jaunu ierīci Skaņas iestatījumi Tips Redzamība “%s” redzamība Jā 1. lappuse 2. lappuse 