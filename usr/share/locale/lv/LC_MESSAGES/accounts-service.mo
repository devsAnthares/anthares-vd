��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  R     =   �  H     -   Y  %   �     �     �  '   �  Z         l               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian (http://www.transifex.com/projects/p/freedesktop/language/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Nepieciešama autentifikācija, lai mainītu pieteikšanās ekrāna konfigurāciju Nepieciešama autentifikācija, lai mainītu lietotāja datus Nepieciešama autentifikācija, lai mainītu pats savus lietotāja datus Mainīt pieteikšanās ekrāna konfigurāciju Mainīt pašam savus lietotāja datus Aktivēt atkļūdošanas kodu Pārvaldīt lietotāju kontus Izvadīt versijas informāciju un iziet Nodrošina D-Bus saskarnes vaicājumiem un lietotāju konta informācijas manipulēšanai. Aizstāt pastāvošās instances 