��    D      <  a   \      �  .   �                (     :     L     ^     p     �  *   �  1   �      �       ,     o   G     �     �     �     �     �          +     @  )   N     x  	   �  2   �  %   �     �  "   	     =	     W	     t	     �	     �	     �	  ?   �	  X   �	  P   J
  /   �
     �
     �
  2   �
  (   2  @   [  
   �  
   �     �     �      �  �   �  ,   r  '   �     �     �  #   �  %        -  
   6  
   A     L  
   Q  0   \     �     �     �     �  �  �  2   �     �  	   �     �     �     �          "     ;  0   I  ;   z     �     �  @   �  y   !     �     �     �     �  1   �     "     6     I  .   V     �     �  *   �  .   �       ;   ,     h     �     �     �     �     �  Y   �  n   :  g   �  0     (   B  "   k  0   �  *   �  a   �     L     Y     j     n     �  �   �  !   H  "   j     �     �  0   �     �           	          -     6  9   J  	   �  	   �  	   �  	   �     <   ,      >       '              :   +   	          C   @   D   B          =                           6   4   %   #      
      !      -   &   ?   $                ;   (       .                        7   /       5             "           *   1          9                            2                   3   A                0      8   )                   

Browse the file system with the file manager  (another copy)  (copy) %s (%'dnd copy)%s %s (%'drd copy)%s %s (%'dst copy)%s %s (%'dth copy)%s %s (another copy)%s %s (copy)%s --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <b>Date</b> <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Accessed date By Access Date By Modification Date By Trashed Date Cannot find the autorun program Copy '%s' to '%s' Copying "%B" to "%B" Copying Files Copying file %'d of %'d (in "%B") to "%B" Copying file %'d of %'d to "%B" Copyright Create the initial window with the given geometry. Date when file was moved to the Trash Don't show this message again. Error starting autorun program: %s Error while copying "%B". Error while copying to "%B". Error while copying. FTP (with login) GEOMETRY Modified date Move or copy files previously selected by a Cut or Copy command Move or copy files previously selected by a Cut or Copy command into the selected folder Move or copy files previously selected by a Cut or Copy command into this folder Nemo could not create the required folder "%s". Nemo's main menu is now hidden No bookmarks defined Only create windows for explicitly specified URIs. Perform a quick set of self-check tests. Preparing to copy %'d file (%S) Preparing to copy %'d files (%S) Public FTP Quit Nemo. SSH Secure WebDAV (HTTPS) Show the version of the program. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There was an error copying the file into %F. There was an error displaying help: 
%s WebDAV (HTTP) Windows share You cannot copy a file over itself. You cannot copy a folder into itself. [URI...] _Copy Here _Redo Copy _Run _Undo Copy _View executable text files when they are opened nd copy) rd copy) st copy) th copy) Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-01-25 14:02+0000
Last-Translator: Jānis Marks Gailis <jm-gailis@fai-vianet.fr>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:10+0000
X-Generator: Launchpad (build 18688)
 

Pārlūkot failu sistēmu ar failu pārvaldnieku  (vēl viena kopija)  (kopija) %s (%'d. kopija)%s %s (%'d. kopija)%s %s (%'d. kopija)%s %s (%'d. kopija)%s %s (vēl viena kopija)%s %s (kopija)%s --check nevar tikt izmantots ar citām opcijām. --geometry nevar tikt izmantots ar vairāk nekā vienu URI. --quit nevar lietot ar URI. <b>Datums</b> <big><b>Kļūda, automātiski palaižot programmatūru</b></big> <big><b>Šajā datu nesējā ir programmatūra, kuru paredzēts palaist automātiski. Vai vēlaties to palaist?</b></big> Piekļuves datums Pēc piekļuves datuma Pēc izmaiņu datuma Pēc izmešanas datuma Nevar atrast automātiskās palaišanas programmu Kopēt '%s' uz '%s' Kopē "%B" uz "%B" Kopē failus Kopē %'d. no %'d failiem (mapē "%B") uz "%B" Kopē %'d. failu no %'d uz "%B" Autortiesības Izveidot sākuma logu ar doto ģeometriju. Datums, kad fails tika pārvietots uz miskasti Šo ziņojumu vairs nerādit. Kļūda, palaižot automātiskās palaišanas programmu: %s Kļūda, kopējot "%B". Kļūda, kopējot uz "%B". Kļūda kopējot. FTP (ar pieteikšanos) ĢEOMETRIJA Izmaiņu datums Pārvietot vai kopēt failus, kas iepriekš izvēlēti ar 'Izgriezt' vai 'Kopēt' komandu Pārvietot vai kopēt failus, kas iepriekš izvēlēti ar 'Izgriezt' vai 'Kopēt' komandu, izvēlētajā mapē Pārvietot vai kopēt failus, kas iepriekš izvēlēti ar 'Izgriezt' vai 'Kopēt' komandu, šajā mapē Nemo nevarēja izveidot nepieciešamo mapi "%s". Galvenais "Nemo" logs tagad ir paslēpts Nav izveidota neviena grāmatzīme Izveidot logus tikai speciāli norādītiem URI. Izpildīt ātru pašpārbaudes testu kopu. Gatavojas kopēt %'d failu (%S) Gatavojas kopēt %'d failus (%S) Gatavojas kopēt %'d failus (%S) Publisks FTP Iziet no "Nemo". SSH Drošs WebDAV (HTTPS) Rādīt programmas versiju. Programmatūra tiks palaista tieši no datu nesēja "%s". Jums nekad nevajadzētu darbināt programmatūru, kurai neuzticaties.

Ja šaubāties, izvēlieties "Atcelt". Kļūda, kopējot failu mapē %F. Kļūda, parādot palīdzību: 
%s WebDAV (HTTP) Windows koplietojums Failu nevar kopēt mapē, kurā tas jau atrodas. Mapi nevar kopēt pašu sevī. [URI...] _Kopēt šeit A_tatsaukt kopēšanu _Palaist _Atsaukt kopēšanu _Skatīt izpildāmos teksta failus, kad tie tiek atvērti . kopija) . kopija) . kopija) . kopija) 