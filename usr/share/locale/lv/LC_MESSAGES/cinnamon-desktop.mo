��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
  	   &
     0
     3
     9
     B
  )   N
  +   x
  W   �
  E   �
     B     O  	   a  "   k  '   �     �  4   �  *     ,   -  +   Z  =   �  +   �  W   �  V   H  �   �  '     �   �  �   ,  =   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: lv
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-23 20:49+0300
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 1.4
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRT %d kontrolieris nevar vadīt izeju %s CRT %d kontrolieris neatbalsta rotāciju=%s CRT %d kontrolieris — mēģina režīmu %dx%d@%dHz ar izeju pie %dx%d@%dHz (pass %d)
 Nevar atrast termināli; lieto xterm, lai gan tas varētu nestrādāt Klēpjdators Dublēti displeji Nezināms RANDR paplašinājums nav pieejams Mēģina režīmus CRT %d kontrolierim
 nevar klonēt izeju %s nevarēja piešķirt CRT kontrolieri pie izejām:
%s neizdevās atrast informāciju par CRTC %d neizdevās atrast informāciju par izvadi %d neizdevās iegūt ekrāna izmēru diapazonu neizdevās iegūt ekrāna resursus (CRTC, izvadus, režīmus) neizdevās iestatīt CRTC %d konfigurāciju neviena no saglabātajām ekrāna konfigurācijām neatbilst aktīvajai konfigurācijai neviens no izvēlētajiem režīmiem nebija savietojams ar iespējamiem režīmiem:
%s izvadei %s nav tie paši parametri kā citai klonētajai izvadei:
esošais režīms = %d, jaunais režīms = %d
esošās koordinātas = (%d, %d), jaunās koordinātas = (%d, %d)
esošā rotācija = %s, jaunā rotācija = %s izeja %s neatbalsta režīmu %dx%d@%dHz pieprasītā CRTC %d pozīcija/izmērs ir ārpus pieļaujamās robežas — pozīcija=(%d, %d), izmērs=(%d, %d), maksimums=(%d, %d) pieprasītais virtuālais izmērs neatbilst pieejamajam izmēram — pieprasīts=(%d, %d), minimums=(%d, %d), maksimums=(%d, %d) neapstrādāta X kļūda, iegūstot ekrāna izmēru diapazonu 