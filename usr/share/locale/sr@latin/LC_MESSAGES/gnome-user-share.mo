��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  ]  w  !   �  7   �  3   /     c     |     �  Z   �  �   �     {  <   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-22 15:32+0200
Last-Translator: Marko M. Kostić (Marko M. Kostić) <marko.m.kostic@gmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
X-Generator: Poedit 1.8.4
 Javne datoteke korisnika „%s“ Javne datoteke korisnika „%s“ na računaru „%s“ Pokreni deljenje ličnih datoteka ako je omogućeno Delenje ličnih datoteka Deljenje Podešavanja deljenja Uključite deljenje ličnih datoteka da biste podelili sadržaj ove fascikle preko mreže. Određuje kada se zahteva unos lozinke. Moguće vrednosti su „never“ (nikada), „on_write“ (pri upisu) i „always“ (uvek). Kada da se traže lozinke deli;deljenje;datoteke;http;mreža;umnoži;kopiraj;pošalji; 