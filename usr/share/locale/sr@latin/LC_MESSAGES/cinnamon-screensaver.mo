��          L      |       �   `   �      
     %  )   B     l  }  �  q   	     {     �  '   �  (   �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-08-26 12:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Serbian <gnom@prevod.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 :    n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: Serbian (sr)
 Čuvar ekrana je aktivan %d sekundu.
 Čuvar ekrana je aktivan %d sekunde.
 Čuvar ekrana je aktivan %d sekundi.
 Čuvar ekrana je pokrenut
 Čuvar ekrana je isključen
 Čuvar ekrana je trenutno zaustavljen.
 Uključen je taster za sva velika slova. 