��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  �        �	     �	     
      
     4
     7
     =
     C
  ,   L
  (   y
  T   �
  D   �
     <     C     U     ^  $   ~  $   �  )   �  1   �  ,   $  +   Q  ;   }  7   �  R   �  E   D  �   �  -   ]  �   �  i     L   x             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-05 10:39+0200
Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <gnom@prevod.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
 %A, %e. %B, %R %A, %e. %B, %R:%S %A, %e. %B, %l:%M %A, %e. %B, %l:%M:S %R %R:%S %l:%M %l:%M:%S CRTC „%d“ ne može dobije izlaz „%s“ CRTC „%d“ ne podržava rotaciju = %s CRTC „%d“: isprobavam režim %dx%d@%dHz bez izlaza na %dx%d@%dHz (%d. pokušaj)
 Ne mogu da nađem terminal, koristim iksterm, čak i ukoliko ne radi Laptop Ekrani u ogledalu Nepoznat Nije prisutno RANDR proširenje Isprobavam režime za CRTC „%d“
 ne mogu da kloniram u izlaz „%s“ ne mogu da pridružim CRTC-e izlazima:
%s ne mogu da dobijem podatke o CRTC „%d“ ekranu ne mogu da dobijem podatke o izlazu „%d“ ne mogu da dobijem opseg veličina za ekran ne mogu da dobijem resurse za ekran (CRTC, izlazi, režimi) ne mogu da postavim podešavanja za CRTC „%d“ ekran ni jedno od sačuvanih podešavanja ekrana se ne poklapa sa tekućim podešavanjem ni jedan od izabranih režima nije saglasan sa mogućim režimima:
%s izlaz „%s“ nema iste parametre kao drugi klonirani uređaj:
postojeći režim = %d, novi režim = %d
postojeće koordinate = (%d, %d), nove koordinate = (%d, %d)
postojeća rotacija = %s, nova rotacija = %s izlaz „%s“ ne podržava režim %dx%d@%dHz zahtevan položaj/veličina za CRTC %d je izvan dozvoljenog ograničenja: položaj=(%d, %d), veličina=(%d, %d), najviše=(%d, %d) zahtevana virtuelna veličina je izvan dostupne: zahtevana=(%d, %d), najmanja=(%d, %d), najveća=(%d, %d) neutvrđena greška u X serveru prilikom dobavljanja opsega veličine ekrana 