��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     �	     �	     �	     �	     
     *
     :
     A
     T
      [
     |
  	   �
     �
     �
  E   �
          $     +     8     Q     U     n  	   u          �  7   �      �     �  #        )     6     O     _     o     �  2   �     �     �     �     �     �     �       	     	   )                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-11 08:48+0000
Last-Translator: tomoe_musashi <hkg.musashi@gmail.com>
Language-Team: Chinese (Hong Kong) <zh_HK@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 位址 總是允許存取 來自 %s 的授權請求 藍牙 藍牙設定 藍牙已停用 藍牙已被硬件開關停用 瀏覽檔案... 取消 配置藍牙設定 連線 %s 裝置想存取「%s」服務 %s 裝置想與此電腦配對 不相符 瀏覽裝置時出現錯誤 僅允許這次 如果你移除裝置，在下次使用時你需要再次設定它。 鍵盤設定 相符 滑鼠設定 滑鼠與觸控板設定 否 找不到藍牙配接器 確定 已配對 %s 的配對確認 %s 的配對請求 請確認 PIN 碼「%s」是否與裝置上的相符。 請輸入裝置上的 PIN 碼。 拒絕 從裝置列表中移除「%s」？ 移除裝置 傳送檔案至裝置... 傳送檔案... 設定新裝置 設定新裝置... 聲音設定 請求的裝置無法被瀏覽，錯誤為「%s」 類型 是否可見 「%s」是否可見 是 連線中... 中斷連線中... 硬件已停用 第 1 頁 第 2 頁 