��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  !   4     V  E   c     �     �     �  '   �          5  *   E  )   p     �     �  (   �  '   �     %     2     E                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver 2.91.93
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-08 02:16+0000
Last-Translator: tomoe_musashi <hkg.musashi@gmail.com>
Language-Team: Chinese (Hong Kong) <community@linuxhall.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
 令熒幕保護程式正常結束 檢查中... 如果熒幕保護程式為作用中，立即將其停用 (不變黑) 密碼錯誤 顯示於鎖定畫面的訊息 請輸入你的密碼... 查詢熒幕保護程式的作用時間 查詢熒幕保護程式狀態 切換使用者 要求熒幕保護程式立刻鎖定畫面 熒幕保護程式已作用了 %d 秒。
 熒幕保護程式作用中
 熒幕保護程式未作用
 熒幕保護程式目前並未作用。
 啟動熒幕保護程式 (畫面變黑) 解除鎖定 本程式的版本 你已開啟 Caps Lock 鍵。 