��    &      L  5   |      P  K   Q  &   �  (   �  S   �  "   A  $   d  &   �     �  >   �     �  9   �  7   4  8   l  #   �     �     �                    '     .     @     P     V  #   h  #   �  -   �  &   �  m        s     �     �     �     �  $   �  	   �     �  S  �  U   N	  -   �	  3   �	  V   
  0   ]
  ,   �
  ,   �
     �
  H   �
     9  F   >  C   �  =   �  $     "   ,     O     n     |     �     �     �     �     �     �  -   �  &     '   4  "   \  `        �     �     �            &     
   B     M                                             $                 &      	      #          %      !                               
                                       "                  Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot determine user's home directory Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Sticky Keys? Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Mouse Mouse Preferences Search Select Sound File Slow Keys Alert Sound Sticky Keys Alert Sync text/plain and text/* handlers The file %s is not a valid wav file The sound file for this event does not exist. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up _Do not show this message again Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2004-02-23 12:12+0730
Last-Translator: Hasbullah Bin Pit <sebol@my-penguin.org>
Language-Team: Projek Gabai <gabai-penyumbang@lists.sourceforge.org>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Tak dapat mencipta direktori "%s".
Ini diperlukan untuk membolehkan penukaran kursor. Tak dapat menentukan direktori rumah pengguna Tak dapat memuatkan fail bunyi %s sebagai contoh %s Tak dapat meletakkan mesin kepada tidur.
Pastikan mesin dikonfigurasikan dengan betul. Adakah anda ingin mengaktifkan Kekunci Perlahan? Adakah anda akan mengaktifkan Kekunci Lekat? Adakah anda ingin mempasifkan Kekunci Lekat? Lenting Ralat bila cuba melaksanakan (%s)
dimana ianya dipautkan ke kekunci (%s) Font Kekunci GConf %s ditetapkan ke jenis %s tapi ia menjangkakan jenis %s
 Ikatan Kekunci (%s) imempunyai aksi yang ditakrifkan beberapa kali
 Ikatan Kekunci (%s) mempunyai ikatan ditakrifkan banyak kali
 Ikatan Kekunci (%s) telah digunakan
 Ikatan kekunci (%s) tidak selesai
 Ikatan Kekunci (%s) tidak sah
 Papan Kekunci Tetikus Keutamaan Tetikus Cari Pilih Fail Bunyi Amaran Kekunci Perlahan Bunyi Amaran Kekunci Lekat Sinkronisasi pengendali text/plain and text/* Fail %s adalah fail wav yang tidak sah Fail bunyi untuk acara ini tidak wujud. Terdapat ralat memapar bantuan: %s Terdapat ralat memulakan screensaver:

%s

Fungsi screensaver tidak akan bekerja pada sessi ini. Hentian Menaip Volum Volum turun Volume mute Langkah volum Langkah volum sebagai peratusan volum. Volum naik _Jangan papar mesej ini lagi 