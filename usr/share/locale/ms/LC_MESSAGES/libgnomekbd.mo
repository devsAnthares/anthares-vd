��    0      �  C         (     )     ?  
   P     [     l     �     �  *   �      �          (  
   E  )   P     z     �     �     �     �     �     �       3   ,     `     q  2   �     �  -   �        (        9  &   G     n  -   �     �  .   �  '   �     #     <     U     i     y     �     �     �  	   �  
   �     �  �  �     �
     �
     �
     �
          '     >  4   Z  (   �     �     �  	   �  9   �     6      S     t     �     �     �  !   �     �  C   �     C     U  +   o     �  .   �     �  '   �       "        B  +   W     �  .   �     �      �     	           2     F     Y     e     {     �     �     �                   %   &              '       )   	                .   /                     *   !   ,                                 "          +                $      #         
          -                           (       0              Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Failed to init GConf: %s
 Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator Test (%s) Keyboard Indicator plugins Keyboard layout Keyboard model Keyboard options Load extra configuration items No description. Save/restore indicators together with layout groups Secondary groups Show flags in the applet Suppress the "X sysconfig changed" warning message The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s modmap file list no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2010-04-21 21:03+0000
PO-Revision-Date: 2010-04-23 20:49+0800
Last-Translator: Ahmed Noor Kader Mustajir Md Eusoff <sir.ade@gmail.com>
Language-Team: Malay <ms@li.org>
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Aktifkan lagi plugin lain Plugin _aktif: Tambah Plugin Tutup dialog Konfigurasi plugin dipilih Matikan plugin dipilih Menurunkan keutamaan plugin Default kumpulan, ditugaskan pada kejadian tetingkap Mengaktifkan / mematikan plugin dipasang Gagal untuk init GConf: %s
 Meningkatkan keutamaan plugin Penunjuk: Menjaga dan menguruskan kumpulan berasingan per tetingkap Penunjuk Plugin Papankekunci Ujian Penunjuk Papankekunci (%s) Penunjuk plugin Papankekunci Layout Papankekunci Model papankekunci Opysen papankekunci Muatkan item tambahan konfigurasi Tiada deskripsi. Simpan / mengembalikan penunjuk bersama-sama dengan kumpulan layout Kumpulan sekunder Tunjukkan flags di applet Menekan  "X sysconfig changed" mesej amaran Warna latarbelakang Warna latar belakang untuk penunjuk tata letak The font family Keluarga font untuk penunjuk tata letak Saiz fon Saiz fon untuk penunjuk tata letak The foreground color Warna latar depan untuk penunjuk tata letak Senarai plugin yang aktif Senara plugin penunjuk papankekunci dihidupkan Ada ralat pemuatan foto: %s Tidak dapat membuka fail bantuan Ralat inisialisasi XKB _Plugin Tersedia: layout papankekunci model papankekunci layout "%s" model "%s", %s dan %s senarai fail modmap tiada layout tiada opysen opsyen "%s" 