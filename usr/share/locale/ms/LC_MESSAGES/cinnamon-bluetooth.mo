��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 y  %     �	     �	     �	  	   �	     �	     �	  *   

     5
     C
     I
  	   c
  $   m
  /   �
     �
     �
     �
  [        b     x     ~     �     �  !   �     �     �     �       C   !  +   e     �      �     �     �     �     �             5   .     d  
   j     u     �     �     �     �  	   �  	   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-12-01 09:48+0000
Last-Translator: Fazwan Fazil <takaizu91@gmail.com>
Language-Team: Malay <ms@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Alamat Sentiasa berikan akses Meminta pengesahan dari %s Bluetooth Tetapan Bluetooth Bluetooth dilumpuhkan Bluetooth dilumpuhkan oleh suis perkakasan Layar Fail... Batal Konfigur tetapan Bluetooh Sambungan Peranti %s mahu akses ke servis '%s' Peranti %s mahu berpasangan dengan komputer ini Tidak sepadan Ralat melayari peranti Benarkan untuk kali ini sahaja Jika anda buang peranti tersebut, anda perlu tetapkan ia semula untuk kegunaan akan datang. Tetapan Papan Kekunci Padan Tetapan Tetikus Tetapan Pad Sentuh dan Tetikus Tidak Tiada penyesuai Bluetooth ditemui OK Berpasangan Pengesahan pasangan untuk %s Permintaan perpasangan untuk %s Sila pastikan samada PIN '%s' padan dengan PIN di peranti tersebut. Sila masukkan PIN seperti di dalam peranti. Tolak Buang '%s' dari senarai peranti? Buang Peranti Hantar Fail ke Peranti... Hantar Fail... Persediaan Peranti Baru Tetapkan Peranti Baru... Tetapan Bunyi Peranti yang diminta tidak dapat dilayari, ralat '%s' Jenis Ketampakan Kebolehtampakan "%s" Ya menyambung... memutuskan... perkakasan dinyahaktif halaman 1 halaman 2 