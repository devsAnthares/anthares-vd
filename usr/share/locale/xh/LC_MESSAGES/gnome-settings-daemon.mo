��    0      �  C         (  K   )  &   u  (   �  S   �  "     $   <  $   a  &   �     �  >   �     �  9   �     1  7   =  8   u  #   �     �     �               ,     ?     K     S     Y     k     �     �     �     �     �  #   �  #   �  -   	  &   7  m   ^     �     �     �     �     �  $   	  	   )	  �   3	  �   �	  �   N
     �
  a    X   r  :   �  9     \   @  2   �  0   �  5     3   7     k  F   r     �  c   �     $  O   C  Y   �  8   �  4   &  4   [  
   �     �     �     �     �     �     �          1     :     Q     p  #   w  1   �  '   �  "   �  %     f   >     �     �     �     �     �  '   �     "  �   /  �   �  �   ^  )        *   '      +       	                          $   .                 )         !   (       
             -      "                  /           ,              0                                                  &   %             #          Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot determine user's home directory Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Lock screen Log out Mouse Mouse Preferences Play (or play/pause) Search Select Sound File Slow Keys Alert Sound Sticky Keys Alert Sync text/plain and text/* handlers The file %s is not a valid wav file The sound file for this event does not exist. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Do not show this message again Project-Id-Version: gnome-control-center
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-03-23 09:53+0200
Last-Translator: Canonical Ltd <translations@canonical.com>
Language-Team: Xhosa <xh-translate@ubuntu.com>
Language: xh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
 Akukwazeki ukwenza uvimba weefayili "%s".
Oku kuyafuneka ukuvumela ukutshintsha iikhesa. Akukwazeki ukumisa uvimba weefayili wasekhaya womsebenzisi Akukwazekanga ukufaka ifayili yesandi %s njengesampuli %s Akukwazekanga ukubeka umatshini kwimo yokulala.
Qinisekisa ukuba imatshini imiselwe kakuhle. Uyafuna ukwenza amaQhosha aCothayo ukuba asebenze? Ufuna ukuwenza asebenze amaQhosha aNcamathelayo? Uyafuna ukwenza ukuba amaQhosha aCothayo angasebenzi? Ufuna ukuwenza angasebenzi amaQhosha aNcamathelayo? Khupha Impazamo xa bekuzanywa ukusebenzisa i- (%s)
edityaniswe kwiqhosha (%s) Ifonti Iqhosha le-GConf %s limiselwe ukuba lichwetheze %s kodwa umchwethezo walo olindelekileyo ibingu %s
 Isiqulathi seefayili sasekhaya IKhowudi exhomekekileyo yeQhosa (%s) inesenzo sayo esichazwe amaxesha amaninzi
 IKhowudi exhomekekileyo yeQhosha(%s) inekhowudi exhomekekileyo echazwe amaxesha amaninzi
 IKhowudi exhomekekileyo yeQhosha (%s) sele isetyenziswa
 IKhowudi exhomekekileyo yeQhosha (%s) ayiphelelanga
 IKhowudi exhomekekileyo yeQhosha (%s) ayiphelelanga
 I-keyboard Isikhangeli soncedo sokundulula Isikhangeli sewebhu sokundulula Tshixa iskrini Phuma Imawusi ULuhlu lokuKhetha lweMawusi Dlala (okanye dlala/nqumama) Khangela Khetha iFayili yeSandi IsiLumkiso samaQhosha aCothayo Isandi IsiLumkiso samaQhosha aNcamathelayo Umbhalo owenzeka text/plain kunye text/* izibambi Ifayili %s asiyofayili i-wav elungileyo Ifayili yesandi yesi sehlo ayikho. Kubekho impazamo ekuvezeni uncedo: %s Kubekho impazamo ukuqalisa i-screensaver:

%s

Ukusebenza kwe-Screensaver akuzukusebenza kule seshoni. Unqanyulo lokuChwetheza Isandi Isandi masithotywe Ukuthula kwesandi Inqanaba lesandi Inqanaba lesandi njengepesenti yesandi. Nyusa isandi Ucinezele iqhosha u-Shift imizuzu eyi-8. Le yindlela enqumlayo yophawu lwamaQhosha aCothayo, echaphazela indlela esebenza ngayo i-keyboard yakho. Uvele wacinezela iqhosha i-Shift ka-5 kuluhlu. Le yindlela enqumlayo yophawu lwamaQhosha aNcamathelayo, echaphazela indlela esebenza ngayo i-keyboard yakho. Uvele wacinezela amaqhosha amabini kwakanye, okanye wacinezela iqhosha u-Shift ka-5 kuluhlu. Oku kuvala uphawu lwamaQhosha aNcamathelayo, okuchaphazela indlela esebenza ngayo i-keyboard yakho. _Ungaphinde uwubonise lo myalezo kwakhona 