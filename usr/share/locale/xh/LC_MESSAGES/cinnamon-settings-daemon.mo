��          D      l       �   &   �   �   �   �   <  �   �  �  m  %     �   +  �   �  �   Z                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:21+0000
Last-Translator: Canonical Ltd <Unknown>
Language-Team: Xhosa <xh-translate@ubuntu.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Kubekho impazamo ekuvezeni uncedo: %s Ucinezele iqhosha u-Shift imizuzu eyi-8. Le yindlela enqumlayo yophawu lwamaQhosha aCothayo, echaphazela indlela esebenza ngayo i-keyboard yakho. Uvele wacinezela iqhosha i-Shift ka-5 kuluhlu. Le yindlela enqumlayo yophawu lwamaQhosha aNcamathelayo, echaphazela indlela esebenza ngayo i-keyboard yakho. Uvele wacinezela amaqhosha amabini kwakanye, okanye wacinezela iqhosha u-Shift ka-5 kuluhlu. Oku kuvala uphawu lwamaQhosha aNcamathelayo, okuchaphazela indlela esebenza ngayo i-keyboard yakho. 