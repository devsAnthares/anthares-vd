��    x      �  �   �      (
     )
     /
     6
  	   >
     H
  
   Q
  	   \
     f
     o
     x
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
            	             ,     @     V     g     }     �     �     �     �     �     �     �     �  	   �     �            
   #     .     2     7  
   E     P  
   `     k     s  
   �  
   �     �     �     �     �     �     �     �     �  	   �  	   	          $     -     ;     J     O  	   T     ^     o     u  	   z     �     �     �  	   �  	   �  
   �     �     �      �          =     ]     j     r     z     �     �     �     �     �  *   �     )     2     ?     R     o     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  [  �     O     U     \  	   d     n  
   w  	   �     �     �     �     �  	   �     �     �     �     �     �     �     �     �          
       %     (   ?     h  !   ~     �     �     �     �     �     �               1     >  ;   T     �     �     �  %   �     �  '     0   ;     l     �     �      �     �  &   �                3     T     k     |  )   �  &   �      �     �          "     >     W  	   d  >   n  1   �     �  %   �          /     <  .   \  	   �  	   �     �     �  $   �     �          #     3  "   J  G   m  P   �  P     Q   W  /   �     �     �  D     8   T  +   �  Z   �  6     H   K  �   �          0  5   J  8   �  9   �     �       !        A     ^     b     f     k     o     �     �     �     �     �     �     �         o   t       B   2   r       L   ]   i       M   C       W   )   9   7   `                  ?   s   m   %   &   Q       a   "   u   D      Y   ,                       E      +       P       @             >   j   R   g               K      v   c   H   x         '   \      .          0             ^   T       !          n           3   A   -       4       V   1   G   [   $                 l   #               U           e   *      F   d   k   _   5   q      f          O   w   (          X          	   6   :   8   I   
   N   S       p                 h           Z       J           =   ;   b      /          <    %.0fm %.1f K %.1f km %.1f km/h %.1f m/s %.1f miles %.1f mmHg %.1f mph %.1f ℃ %.1f ℉ %.2f hPa %.2f inHg %.2f kPa %.2f mb %.3f atm %.f%% %0.1f knots %H:%M %a, %b %d / %H:%M %d K %d ℃ %d ℉ %s / %s Beaufort scale Broken clouds Calm Clear Sky DEFAULT_CODE DEFAULT_COORDINATES DEFAULT_DISTANCE_UNIT DEFAULT_LOCATION DEFAULT_PRESSURE_UNIT DEFAULT_RADAR DEFAULT_SPEED_UNIT DEFAULT_TEMP_UNIT DEFAULT_ZONE Default Display radar map Distance unit Drizzle Dust Duststorm East East - NorthEast East - Southeast Few clouds Fog Hail Heavy drizzle Heavy rain Heavy sandstorm Heavy snow Invalid Light drizzle Light rain Light snow Mist Moderate drizzle Moderate rain Moderate snow Nearby city North North - NorthEast North - Northwest Northeast Northwest Not used anymore Overcast Pressure unit Radar location Rain Sand Sandstorm Scattered clouds Smoke Snow Snowstorm South South - Southeast South - Southwest Southeast Southwest Speed unit Temperature unit The unit to use for pressure. The unit to use for temperature. The unit to use for visibility. The unit to use for wind speed. Thunderstorm Tornado Unknown Unknown observation time Unknown precipitation Update interval Update the data automatically Url for the radar map Use metric units Use metric units instead of english units. Variable Volcanic ash Weather for a city Weather location information Weather location information. West West - Northwest West - Southwest Zone location atm hPa inHg kPa km km/h knots m m/s mb mmHg mph Project-Id-Version: si
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-01-29 14:21+0530
Last-Translator: Danishka Navin <danishka@gmail.com>
Language-Team: Sinhala <en@li.org>
Language: si
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 %.0fm %.1f K %.1f km %.1f km/h %.1f m/s %.1f miles %.1f mmHg %.1f mph %.1f ℃ %.1f ℉ %.2f hPa %.2f inHg %.2f kPa %.2f mb %.3f atm %.f%% %0.1f knots %H:%M %a, %b %d / %H:%M %d K %d ℃ %d ℉ %s / %s බෝපට් පරිමාණය බිඳුනු වලාකුළු සන්සුන් පැහැදිළිඅහස DEFAULT_CODE DEFAULT_COORDINATES DEFAULT_DISTANCE_UNIT DEFAULT_LOCATION DEFAULT_PRESSURE_UNIT DEFAULT_RADAR DEFAULT_SPEED_UNIT DEFAULT_TEMP_UNIT DEFAULT_ZONE පෙරනිමි රේඩාර් සිතියම දක්වන්න දුර ඒකකය පොද වැස්ස දූවිලි දූවිලි කුණාටු නැගෙනහිර නැගෙනහිර - ඊසාන නැගෙනහිර - ගිනිකොන මද වලාකුළු ඝන මීදුම හේල් තද පොද වැස්ස තද වැස්ස තද වැලි කුණාටු තද හිම සාවද්‍ය මද පොද වැස්ස මද වැස්ස මද හිම මීදුම තරමක් පොද වැස්ස තරමක් තද වැස්ස තරමක් මද හිම ලගම නගරය උතුර උතුර - ඊසාන උතුර - වයඹ ඊසාන වයඹ තවදුරටත් භාවිතා නොකරයි වලාකුළින් අඳුරුවූ පීඩන ඒකකය රේඩාර් ස්ථානය වැස්ස වැලි වැලි කුණාටු විසිරුණු වලාකුළු දුම හිම හිම කුණාටු දකුණ දකුණ - ගිනිකොන දකුණ - නිරිත ගිනිකොන නිරිත වේග ඒකකය උෂ්ණත්ව ඒකකය පීඩනය සඳහා භාවිතා කරන ඒකකය. උෂ්ණත්වය සඳහා භාවිතා කරන ඒකකය. දෘෂෘතාවය සඳහා භාවිතා කරන ඒකකය. සුළං වේගය සඳහා භාවිතා කරන ඒකකය. අකුණු සහිත කුණාටු සුළි කුණාටු නොදන්නා නොහඳුනන නිරීක්ෂන වේලාවක් නොකිවහැකි වර්ෂා පතනය යාවත්කාල විරාමය දත්ත ස්වයංක්‍රීයව යාවත්කාල කරන්න රේඩාර් සිතියම සඳහා Url මෙට්‍රික් ඒකක භාවිතා කරන්න ඉංග්‍රීසි ඒකක වෙනුවට මෙට්‍රික් ඒකක භාවිතා කරන්න. විචල්‍යය යමහල් අළු නගරයක් සඳහා කාළගුණය කාළගුණ ස්ථාන තොරතුරු කාළගුණ ස්ථාන තොරතුරු. බටහිර බටහිර - වයඹ බටහිර - නිරිත කලාප ස්ථාන atm hPa inHg kPa කිලෝ මිටර km/h knots mb m/s mb mmHg mph 