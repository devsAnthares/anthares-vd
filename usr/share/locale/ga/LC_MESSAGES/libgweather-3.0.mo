��    [      �     �      �     �     �     �  	   �     �  
   �  	   �                    !  	   *     4     =     E     N     T     `     f     x     }     �     �     �     �     �     �     �  	   �     �     �     �     	     	     3	     A	     T	     f	     s	     {	     �	     �	     �	     �	  
   �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	  	   
  	   
     
     
     ,
     ;
     @
     Q
     W
     \
     b
     t
  	   �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
                                              !     %     (     +     0     4  �  E               #  	   +     5  
   >  	   I     S     \     e     n  	   w     �     �     �     �     �     �     �     �     �     �     �     �     �     	               #     0     =     Q     S     d     i     w     |     ~     �     �     �     �     �     �     �     �  
   �     �                 
   &     1     M     i     x     �  
   �     �  
   �     �     �     �  	   �     �     �          $     2  	   B     L     Y     k     s     �     �     �     �     �     �     �     �  
   �     �     �     �     �     �     �  	   �             ,   F   Y       U      :               ?               '      1       +   =                  *      <      7           &              B          8       D   2             )   %          V   S                  4   $       O      X   W   N      	      .   -   H             0              [       
   Z   5            Q           "   C   L       M   (   ;   K   >      #      J   G       6          !   A         9      /   R      E   T       @              P   3   I    %.0fm %.1f K %.1f km %.1f km/h %.1f m/s %.1f miles %.1f mmHg %.1f mph %.1f °C %.1f °F %.2f hPa %.2f inHg %.2f kPa %.2f mb %.3f atm %.f%% %0.1f knots %H:%M %a, %b %d / %H:%M %d K %d °C %d °F %s / %s Beaufort force %.1f Beaufort scale Broken clouds C Calm Clear Sky DEFAULT_CODE DEFAULT_COORDINATES DEFAULT_DISTANCE_UNIT DEFAULT_LOCATION DEFAULT_PRESSURE_UNIT DEFAULT_RADAR DEFAULT_SPEED_UNIT DEFAULT_TEMP_UNIT DEFAULT_ZONE Default Dust East East - NorthEast East - Southeast F Few clouds Fog Haze Heavy rain Invalid K Mist North North - NorthEast North - Northwest Northeast Northwest Overcast Pressure unit Radar location Rain Scattered clouds Smoke Snow South South - Southeast South - Southwest Southeast Southwest Thunderstorm Unknown Variable Weather for a city West West - Northwest West - Southwest Zone location atm hPa inHg kPa km km/h knots m m/s mb mi mmHg mph timezoneUnknown Project-Id-Version: libgweather.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-08-06 21:59-0600
Last-Translator: Seán de Búrca <leftmostcat@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
 %.0fm %.1f K %.1f km %.1f km/u %.1f m/s %.1f míle %.1f mmHg %.1f msu %.1f °C %.1f °F %.2f hPa %.2f inHg %.2f cPa %.2f mb %.3f atm %.f%% %0.1f muirmhíle %H:%M %a %d %b / %H:%M %d K %d °C %d °F %s / %s fórsa Beaufort %.1f scála Beaufort Scamaill bhriste C Ciúin Spéir Ghlan DEFAULT_CODE DEFAULT_COORDINATES m DEFAULT_LOCATION mmHg DEFAULT_RADAR km/h C DEFAULT_ZONE Réamhshocrú Deannach Oirthear Oirthear - Oirthuaisceart Oirthear - Oirdheisceart F Cúpla scamall Ceo Smúiteán Báisteach trom Neamhbhailí K Ceochán Tuaisceart Tuaisceart - Oirthuaisceart Tuaisceart - Iarthuaisceart Oirthuaisceart Iarthuaisceart Modartha Aonad brú Suíomh radair Báisteach Scamaill scaipthe Deatach Sneachta Deisceart Deisceart - Oirdheisceart Deisceart - Iardheisceart Oirdheisceart Iardheisceart Stoirm thoirní Anaithnid Athraitheach Aimsir le cathair Iarthar Iarthar - Iarthuaisceart Iarthar - Iardheisceart Suíomh creasa atm hPa inHg kPa km km/h muirmhíle m m/s mb mi mmHg msu Anaithnid 