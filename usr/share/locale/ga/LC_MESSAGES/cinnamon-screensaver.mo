��          L      |       �   `   �      
     %  )   B     l  5  �  �   �  *   q  0   �  :   �  &                                            The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-08-26 12:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ga
 Tá an spárálaí scáileáin gníomhach le %d soicind.
 Tá an spárálaí scáileáin gníomhach le %d shoicind.
 Tá an spárálaí scáileáin gníomhach le %d shoicind.
 Tá an spárálaí scáileáin gníomhach
 Tá an spárálaí scáileáin neamhghníomhach
 Níl an spárálaí scáileáin gníomhach faoi láthair.
 Tá an eochair Caps Lock brúite agat. 