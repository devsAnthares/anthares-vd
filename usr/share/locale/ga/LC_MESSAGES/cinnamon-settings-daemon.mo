��          |      �             !     ?  
   K     V     ]  1   l  &   �  /   �     �          +    K      b     �     �     �     �  ;   �  &     7   ,     d     �  (   �                       
                                	    Don't show any warnings again Empty Trash Examine... Ignore Low Disk Space The volume "%s" has only %s disk space remaining. There was an error displaying help: %s This computer has only %s disk space remaining. Universal Access Preferences _Keep This Configuration _Restore Previous Configuration Project-Id-Version: cinnamon-settings-daemon.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:10+0000
Last-Translator: Seán de Búrca <Unknown>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Ná taispeáin aon rabhadh arís Folmhaigh Bruscar Scrúdaigh... Déan Neamhaird Air Spás Diosca Gann Níl ach %2$s spáis diosca fágtha ag an imleabhar "%1$s". Bhí earraid ag taispeáin cabhair: %s Níl ach %s spáis diosca fágtha ag an ríomhaire seo. Sainroghanna Rochtana Uilíche _Coinnigh an Chumraíocht Seo _Athchóirigh an Chumraíocht Roimhe Seo 