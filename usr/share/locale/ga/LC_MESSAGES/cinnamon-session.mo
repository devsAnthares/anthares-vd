��             +         �     �     �  '   �          *  !   /     Q     b     e     y     �     �     �  '   �     �     �          %     @     ^     j     y     �  &   �     �  $   �  
   �     
       
        '    4     N     m  7   v     �     �  '   �     �               1     M  &   [     �  2   �     �  !   �  +   �     $	  #   ?	     c	     p	  "   �	  	   �	  $   �	     �	  #   �	  
   "
     -
  	   =
     G
     N
                       
                                                                                     	                                    A program is still running: Cancel Do not load user-specified applications Enable debugging code FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Lock Screen Log out Log out of this system now? Not responding Override standard autostart directories S_uspend Session management options: Show session management options Shut down this system now? Specify session management ID Starting %s Suspend Anyway Switch User Anyway Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:18+0000
Last-Translator: Seán de Búrca <Unknown>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ga
 Tá feidhmchlár á rith fós: Cealaigh Ná luchtaigh feidhmchláir sonraithe ag an úsáideoir Cumasaigh cód dífhabhtaithe COMHAD Ní comhad .desktop bailí é an comhad Geimhrigh Mar Sin Féin CA Deilbhín '%s' gan aimsiú Cuir Scáileáin Faoi Ghlas Logáil amach Logáil amach as an gcóras seo anois? Gan fhreagairt Sáraigh comhadlanna uath-thosaithe caighdeánacha _Cuir ar Fionraí Roghanna bainistíochta seisiún: Taispeáin roghanna bainistíochta seisiún Múch an córas seo anois? Sonraigh CA bainistíochta seisiún %s á Thosú Cuir ar Fionraí Mar Sin Féin Aistrigh Úsáideoir Mar Sin Féin Anaithnid Leagan comhaid deisce anaithnid '%s' Rogha tosaithe anaithnid: %d Tá tú logáilte isteach mar "%s". _Geimhrigh _Lógáil Amach _Atosaigh _Múch _Switch User 