��    7      �  I   �      �     �     �     �  I   �            	   1     ;     B     R     ^     d     y      �  (   �     �  	   �     �     �     �       
        &     -     4     ;     C     H     U     b     t  
   |     �     �     �     �     �     �     �     �     �     �                    -     :     H     P     \     `     d     i     o  8  w     �	     �	     �	  a   �	     /
     6
     N
     Z
     b
     w
     �
     �
  '   �
  +   �
  8   �
     *     8     H     O     ^     u     }  
   �     �     �     �     �     �     �     �  	   �     �  
             *     B     T     Z     b     v     �     �  
   �  	   �     �     �     �  	     
        #     '     +     0     <     5   %   4                    	      
                     /   ,             '   3          7             -   "   2           .                            *      0   +             6       (      $       &   #          1                    )                 !    %d Mb/s %d x %d %d x %d (%s) %s
Run '%s --help' to see a full list of available command line options.
 %s VPN - System Settings All files Arabic British English Calibration China Chinese (simplified) Could not detect displays Could not get screen information Could not save the monitor configuration Default Default:  Device Disabled Enable verbose mode English Enterprise France French German Germany Help IPv4 Address IPv6 Address Mirrored Displays Monitor No profile Not specified Other profile… Panel to display Proxy Quit Russian Select a region Set for all users Show help options Show the overview Spain Spanish Status unknown Uncalibrated United States Unknown Unspecified WEP WPA WPA2 Wired _Import Project-Id-Version: cinnamon-control-center.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-08-26 10:56+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
Language: ga
 %d Mb/s %d x %d %d x %d (%s) %s
Rith '%s --help' chun liosta iomlán roghanna líne na n-orduithe atá le fáil a fheiceáil.
 VPN %s - Socruithe an Chórais Gach comhad Araibis Béarla na Breataine Calabrú An tSín Sínis (shimplithe) Níorbh fhéidir taispeáintí a bhrath Níorbh fhéidir eolas scáileáin a fháil Níorbh fhéidir cumraíocht an scáileáin a shábháil Réamhshocrú Réamhshocrú:  Gléas Díchumasaithe Cumasaigh mód foclach Béarla Fiontar An Fhrainc Fraincis Géarmáinis An Ghéarmáin Cabhair Seoladh IPv4 Seoladh IPv6 Taispeáintí Scáthánaithe Monatóir Gan phróifíl Gan sonrú Próifíl eile… Painéal le taispeáint Seachfhreastalaí Scoir Rúisis Roghnaigh réigiún Socraigh do gach úsáideoir Taispeáin roghanna cabhrach Taispeáin an foramharc An Spáinn Spáinnis Stádas anaithnid Gan chalabrú Stáit Aontaithe Anaithnid Gan socrú WEP WPA WPA2 Sreangaithe _Iompórtáil 