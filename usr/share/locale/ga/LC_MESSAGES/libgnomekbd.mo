��    4      �  G   \      x  x   y  8   �     +     A  
   R     ]     n     �     �  *   �      �          *  
   G  )   R     |     �     �     �     �  z   �     s     �  ,   �     �     �  3   �     #     4  7   M  (   �  a   �  2   	     C	     b	     �	     �	     �	  .   �	  '   
     ,
     E
     ^
     r
     �
     �
     �
     �
  	   �
  
   �
     �
  �  �
  �   �  4   �  #   �     �     	           -  +   N  #   z  6   �  .   �       #      
   D  9   O  !   �  &   �  "   �  (   �       g   8     �     �  J   �  '        ?  K   O     �  (   �  S   �  >   /  �   n  9   �  .   0  .   _  $   �  '   �      �  7   �  +   4  %   `     �     �     �     �  i   �     L     e     {     �  B   �                           #       .      	   
       $      *         4         ,      -          3          /   )            !                     +             0   %   2           1          &      (                      "         '                       A collection of scripts to run whenever the keyboard state is reloaded. Useful for re-applying xmodmap based adjustments A list of modmap files available in the $HOME directory. Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Failed to init GConf: %s
 Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator Test (%s) Keyboard Indicator plugins Keyboard Update Handlers Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) Suppress the "X sysconfig changed" warning message The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s modmap file list no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-08-20 07:38-0600
PO-Revision-Date: 2009-08-20 08:06-0600
Last-Translator: Seán de Búrca <leftmostcat@gmail.com>
Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>
Language: ga
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=5; plural=n==1 ? 0 : (n%10==1 || n%10==2) ? 1 : (n%10>=3 && n%10<= 6) ? 2 : ((n%10>=7 && n%10<=9) || n==10) ? 3 : 4;
 Bailiúchán scripteanna le rith aon uair a athluchtaíodh staid an mhéarchláir. Úsáideach le haghaidh athchur i bfheidhm coigeartuithe xmodmap-bhunaithe Liosta comhaid modmap le fáil sa chomhadlann $HOME. Gníomhachtaigh tuilleadh breiseán _Breiseáin gníomhacha: Cuir Breiseán Leis Dún an dialóg Cumraigh an breiseán roghnaithe Díghníomhachtaigh an breiseán roghnaithe Laghdaigh tosaíocht an bhreiseáin Grúpa réamhshocraithe, sanntar ag cruthú fuinneoige Cumasaigh/díchumasaigh breiseáin suiteáilte Theip ar thúsú GConf: %s
 Méadaigh tosaíocht an bhreiseáin Táscaire: Coinnigh agus bainistigh grúpa ar leith do gach fuinneog Breiseáin Táscaire Méarchláir Tástáil Táscaire Mhéarchláir (%s) Breiseáin Táscaire Mhéarchláir Láimhseálaithe Nuashonrú Méarchláir Leagan amach méarchláir Leagan amach méarchláir "%s"
Cóipcheart &#169; Rannpháirtithe X.Org Foundation agus XKeyboardConfig Samhail mhéarchláir Roghanna méarchláir Luchtaigh leaganacha amach agus roghanna coimhthíoch úsáidte go hannamh Luchtaigh míreanna cumraíochta breise Gan chur síos. Sábháil/athchóirigh táscairí in éineacht le grúpaí leaganacha amach Grúpaí tánaisteacha Taispeáin brataí san fheidhmchláirín Taispeáin brataí san fheidhmchláirín chun an leagan amach reatha a chur in iúl Taispeáin ainmneacha leagain amach in ionad ainmneacha grúpa Taispeáin ainmneacha leagain amach in ionad ainmneacha grúpa (do leaganacha XFree a thacaíonn le leaganacha amach iomadúla amháin) Ceil an teachtaireachta rabhaidh "Athraíodh sysconfig X" Réamhamharc an Mhéarchláir, fritháireamh X Réamhamharc an Mhéarchláir, fritháireamh Y Réamhamharc an Mhéarchláir, airde Réamhamharc an Mhéarchláir, leithead Liosta na mbreiseán gníomhacha Liosta na mbreiseán Táscaire Mhéarchláir cumasaithe Tharla earráid agus íomhá á luchtú: %s Ní féidir comhad cabhrach a oscailt Earrád túsaithe XKB Breiseáin le _fáil: leagan amach méarchláir samhail mhéarchláir leagan amach "%s" leaganacha amach "%s" leaganacha amach "%s" leaganacha amach "%s" leaganacha amach "%s" samhail "%s", %s agus %s liosta comhaid modmap gan leagan amach gan roghanna rogha "%s" roghanna "%s" roghanna "%s" roghanna "%s" roghanna "%s" 