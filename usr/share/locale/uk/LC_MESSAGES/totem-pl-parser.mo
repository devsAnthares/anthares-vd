��    	      d      �       �      �      �      �      �      	  "     1   @     r    {     }     �     �  %   �  .   �  7   �  K   *     v                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device '%s'. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem&keywords=I18N+L10N&component=playlist parser
PO-Revision-Date: 2016-03-10 16:19+0300
Last-Translator: Mykola Tkach <Stuartlittle1970@gmail.com>
Language-Team: Ukrainian <uk@li.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Звуковий CD Blu-ray DVD Цифрове телебачення Не вдається підключити %s. Відсутній носій у пристрої '%s'. Перевірте, що диск вставлений у пристрій. Відео CD 