��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  {   �  l   �  y   l  ;   �  9   "  @   \  8   �  R   �  �   )  4   �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Для зміни налаштувань вікна вітання потрібно пройти розпізнавання Для зміни даних користувача потрібно пройти розпізнавання Для зміни даних вашого користувача потрібно пройти розпізнавання Зміна налаштувань вікна вітання Зміна даних вашого користувача Увімкнути діагностичні можливості Керування обліковими записами Показати дані щодо версії і завершити роботу Надає інтерфейси D-Bus для отримання та внесення змін
до даних облікових записів користувачів. Замінити поточний екземпляр 