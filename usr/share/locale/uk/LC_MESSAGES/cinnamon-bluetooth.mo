��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 z  %     �	  *   �	  +   �	  	   
  "   
     1
  +   L
     x
     �
  1   �
     �
  S   �
  Z   @     �  3   �  3   �  �     -   �     �  !     L   1     ~  ?   �     �     �  6   �  1        B  W   �       =   -  !   k  5   �      �  2   �  (     #   @  o   d     �     �     �               *  %   E     k     ~                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-09-19 09:52+0000
Last-Translator: Oliver Star <ewqstar@gmail.com>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Адреса Завжди надавати доступ Запит авторизації від %s Bluetooth Налаштування Bluetooth Bluetooth вимкнено Bluetooth вимкнено апаратно Огляд файлів… Скасувати Змінити налаштування Bluetooth З’єднання Пристрій %s воліє отримати доступ до служби '%s' Пристрій %s хоче встановити пару з цим комп'ютером Не збігається Помилка перегляду  пристрою Дозволити тільки цього разу Якщо Ви вилучите пристрій, Вам доведеться встановити його знову перед наступним використанням Налаштування клавіатури Збігається Налаштування миші Налаштування миші та сенсорного пристрою Ні Не знайдено жодного адаптера Bluetooth OK Зпаровані Підтвердження з'єднання для %s Запит на утворення пари з %s Будь ласка, підтвердіть, чи збігається PIN-код '%s' з PIN-кодом на пристрої. Будь ласка, введіть PIN-код, вказаний на пристрої. Відхилити Вилучити '%s' з переліку пристроїв? Вилучити пристрій Відправити файли на пристрій Надіслати файли… Налаштувати новий пристрій Додати новий пристрій Налаштування звуку Запитуваний пристрій не може бути переглянутий, помилка в '%s' Тип Видимість Видимість з “%s” Так з'єднання ... від’єднання... обладнання вимкнено сторінка 1 сторінка 2 