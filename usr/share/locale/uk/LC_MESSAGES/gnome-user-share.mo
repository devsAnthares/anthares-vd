��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     B  $   _  I   �  B   �       +   *  �   V  u   �  &   `  Z   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-03-04 15:06+0300
Last-Translator: Daniel Korostil <ted.korostiled@gmail.com>
Language-Team: linux.org.ua
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Спільні файли %s Спільні файли %s на %s Увімкнути спільний доступ, якщо можливо Спільний доступ до особистих файлів Оприлюднення Параметри оприлюднення Увімкнути особисте оприлюднення файлів для поширення вмісту цієї теки у мережі. Коли запитувати пароль. Можливі значення «never», «on_write», та «always». Коли вимагати пароль оприлюднити;файли;http;мережа;копіювати;надіслати; 