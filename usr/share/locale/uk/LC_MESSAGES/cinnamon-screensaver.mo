��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  p   b     �  z   �     d  k   �  3   �  �   "  7   �  %   �  �   	  �   �	  1   �
  5   �
  J   �
  R   7     �  &   �  ,   �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-19 11:09+0000
Last-Translator: Oliver Star <ewqstar@gmail.com>
Language-Team: translation@linux.org.ua
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: uk
 Призводить до коректного завершення роботи зберігача екрану Перевірка… Якщо зберігач екрану активний, деактивувати його (відновити екран) Невірний пароль Повідомлення, яке буде відображатися на екрані блокування Будь ласка, введіть пароль... Запитати інтервал часу, протягом якого було активне зберігання екрану Запитує стан зберігача екрану Змінити користувача Вказує запущеному процесу зберігання екрану негайно заблокувати екран Збереження екрану активне протягом %d секунди.
 Збереження екрану активне протягом %d секунд.
 Збереження екрану активне протягом %d секунд.
 Збереження екрану активне
 Збереження екрану неактивне
 Збереження екрану наразі не активовано.
 Увімкнути збереження екрану (погасити екран) Розблокувати Версія цієї програми Натиснуто клавішу Caps Lock. 