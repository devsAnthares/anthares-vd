��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     	
     
     &
     9
  	   O
     Y
     \
     b
     k
  =   w
  5   �
  p   �
  �   \     �  !        0  :   C  7   ~  3   �  F   �  [   1  N   �  S   �  j   0  ^   �  i   �  m   d  S  �  ;   &  �   b  �   ,  w   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-26 09:05+0300
Last-Translator: Re. <ted.korostiled@gmail.com>
Language-Team: linux.org.ua
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d неможливо керувати виводом %s CRTC %d не підтримує обертання=%s CRTC %d: випробування режиму %dx%d@%dHz з виводом на %dx%d@%dHz (пропуск %d)
 Не вдалось знайти термінал, який використовує програма xterm, хоча й вона може не працювати Ноутбук Дзеркальні покази Невідомий Розширення RANDR не підтримується Випробування режимів для CRTC %d
 неможливо клонувати вивід %s неможливо призначити CRTC для виводів:
%s не вдалось отримати інформацію про контролер CRTC %d не вдалось отримати інформацію про вихід %d не вдалось отримати допустимі розміри екрана не вдалось отримати доступ до ресурсів (CRTC, виходи, режими) не вдалось встановити конфігурацію контролера CRTC %d жодна із збережених конфігурацій не збігається з наявною жоден з вибраних режимів несумісний з можливими режимами:
%s вивід %s не підтримує однакові параметри як інший клонований вивід:
наявний режим = %d, новий режим = %d
наявні координати = (%d, %d), нові координати = (%d, %d)
наявне обертання = %s, нове обертання = %s вивід %s не підтримує режим %dx%d@%dHz потрібна програма та розмір CRTC %d виходить за рамки дозволених меж: позиція=(%d, %d), розмір=(%d, %d), максимально=(%d, %d) потрібний віртуальний розмір не вміщується у доступний розмір: потрібно=(%d, %d), мінімум=(%d, %d), максимум=(%d, %d) при спробі отримання допустимих розмірів екрана виникла помилка 