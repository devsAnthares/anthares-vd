��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  0   "  #   S     w     �  6   �  0   �  2     ]   H  I   �  4   �     %  g   9  8   �  =   �  '     '   @  �   h  !   9  '   [  o   �  M   �     A  8   X  p   �       3   "  w   V  S   �  �   "  4   �  4   �  3   1  3   e     �  @   �     �  N        [  H   u     �  F   �  2     N   P  @   �  <   �       !   .  !   P  '   r  !   �  M   �     
     *     H  M   h                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-04 17:31+0200
PO-Revision-Date: 2011-03-04 17:32+0300
Last-Translator: Korostil Daniel <ted.korostiled@gmail.com>
Language-Team: translation@linux.org.ua
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.6.1
 Увімкнути більше додатків Увімкнені _додатки: Додати додаток Закрити вікно Налаштувати вибраний додаток Вимкнути вибраний додаток Зменшити пріоритет додатка Типова група, що призначається при створенні вікна Увімкнути/вимкнути встановлені додатки Збільшити пріоритет додатка Індикатор: Зберігати та підтримувати різні групи для окремих вікон Додатки індикатора клавіатури Додатки до індикатора клавіатури Розкладка клавіатури Розкладка клавіатури Розкладка клавіатури «%s»
Copyright &#169; X.Org Foundation та учасники розробки XKeyboardConfig
Ліцензію викладено в метаданих пакунка Модель клавіатури Параметри клавіатури Завантажити екзотичні, рідко вживані розкладки та параметри Завантажити додаткові пункти налаштувань Немає опису. Перегляд розкладки клавіатури Зберігати/відновлювати індикатори разом з групами розкладок Другорядні групи Показувати прапори в аплеті Показувати прапори у аплеті для індикації теперішньої розкладки Показувати назви розкладок замість назв груп Показувати назви замість назв груп (лише для версій XFree, які підтримують багато розкладок) Перегляд клавіатури, зсув з X Перегляд клавіатури, зсув з Y Перегляд клавіатури, висота Перегляд клавіатури, ширина Колір тла Колір тла для індикатора розкладки Гарнітура шрифту Гарнітура шрифту для індикатора розкладки Розмір шрифту Розмір шрифту для індикатора розкладки Колір тексту Колір тексту для індикатора розкладки Список увімкнутих додатків Список додатків для індикатора клавіатури Помилка завантаження зображення: %s Не вдалось відкрити файл довідки Невідомо Помилка запуску XKB _Доступні додатки: розкладка клавіатури модель клавіатури розкладка «%s» розкладки «%s» розкладок «%s» модель «%s», %s та %s немає розкладки немає параметрів параметр «%s» параметри «%s» параметрів «%s» 