��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  0   `  -   �  9   �     �  !        4     G     ]  >   y     �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese (Taiwan) (http://www.transifex.com/projects/p/freedesktop/language/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
 若要變更登入畫面組態需要通過認證 若要變更使用者資料需要通過認證 若要變更您自身的使用者資料需要通過認證 變更登入畫面組態 變更您自身的使用者資料 啟用除錯模式 管理使用者帳戶 輸出版本資訊並離開 提供 D-Bus 界面以查詢與處理
使用者帳戶資訊。 替換現有實體 