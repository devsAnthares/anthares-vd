��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     J  J     �	     �	     �	     �	  
   �	     �	     
     
     
  #   
     @
  O   ^
  L   �
     �
               "     <     \  '   {     �     �  !   �  2        7  <   V  9   �  �   �  *   �  r   �  r   9  <   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 2.91.4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-05 16:09+0800
Last-Translator: Chao-Hsiung Liao <j_h_liau@yahoo.com.tw>
Language-Team: Chinese/Traditional <zh-l10n@lists.linux.org.tw>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 %m月%e日(%A), %R %m月%e日(%A), %R:%S %m月%e日(%A), %p %l:%M %m月%e日(%A), %p %l:%M:%S %A，%B %e %R %R:%S %p %l:%M %p %l:%M:%S CRTC %d 不能驅動輸出裝置 %s CRTC %d 不支援 rotation=%s CRTC %d：正在嘗試模式 %dx%d@%dHz 於輸出裝置 %dx%d@%dHz (通過 %d)
 找不到終端機，將使用 xterm 代替，不管系統中有沒有 xterm 筆記型電腦 鏡射顯示器 不明 沒有 RANDR 擴充功能 正在嘗試 CRTC %d 的模式
 不能複製到輸出裝置 %s 無法指派 CRTC 給輸出裝置：
%s 無法取得 CRTC %d 的資訊 無法取得輸出 %d 的資訊 無法取得螢幕大小的範圍 無法取得螢幕資源 (CTRCs、輸出、模式) 無法設定 CRTC %d 的組態 已儲存的顯示組態沒有一個符合使用中的組態 選取的模式沒有一個和可用的模式相容：
%s 輸出裝置 %s 沒有和另一個複製輸出裝置同樣的參數：
現有的模式 = %d，新的模式 = %d
現有的坐標 = (%d, %d)，新的坐標 = (%d, %d)
現有的旋轉 = %s，新的旋轉 = %s 輸出裝置 %s 不支援模式 %dx%d@%dHz 對 CTRC %d 要求的位置/大小超出允許的限制：位置 =(%d, %d)，大小 =(%d, %d)，最大值=(%d, %d) 要求的虛擬大小無法符合可用的大小：要求的 =(%d, %d)，最小值 =(%d, %d)，最大值 =(%d, %d) 在取得螢幕大小的範圍發生無法處理的 X 錯誤 