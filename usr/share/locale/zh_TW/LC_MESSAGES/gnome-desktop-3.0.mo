��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �  
   �     �     �     �     �  
   �                     )     ;     O  
   W  #   b     �  O   �     �  	   	     	  '   =	  9   e	  �   �	  *   m
  r   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop 2.91.4
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-19 20:07+0000
PO-Revision-Date: 2018-08-21 21:26+0800
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese/Traditional <zh-l10n@lists.linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
 %R %R:%S %A %R %A %R∶%S %b%-e日%A_%R %b%-e日%A_%R:%S %b%-e日%A_%p%l:%M %b%-e日%A_%p%l:%M:%S %A %p%l:%M %A %p%l:%M:%S %b%-e日_%R %b%-e日_%R:%S %b%-e日_%p %l:%M %b%-e日_%p%l:%M:%S %p%l:%M %p%l:%M:%S CRTC %d 不能驅動輸出裝置 %s CRTC %d 不支援 rotation=%d CRTC %d：正在嘗試模式 %dx%d@%dHz 於輸出裝置 %dx%d@%dHz (通過 %d)
 正在嘗試 CRTC %d 的模式
 未指定 不能複製到輸出裝置 %s 無法指派 CRTC 給輸出裝置：
%s 選取的模式沒有一個和可用的模式相容：
%s 輸出裝置 %s 沒有和另一個複製輸出裝置同樣的參數：
現有的模式 = %d，新的模式 = %d
現有的坐標 = (%d, %d)，新的坐標 = (%d, %d)
現有的旋轉 = %d，新的旋轉 = %d 輸出裝置 %s 不支援模式 %dx%d@%dHz 要求的虛擬大小無法符合可用的大小：要求的 =(%d, %d)，最小值 =(%d, %d)，最大值 =(%d, %d) 