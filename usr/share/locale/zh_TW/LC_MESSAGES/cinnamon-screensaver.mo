��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  !   5     W  ?   d     �  '   �     �  *   �           ?  *   O  &   z     �     �  %   �  *        ,     9     R                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver 2.91.93
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-02-26 16:07+0000
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese (Taiwan) <zh-l10n@lists.linux.org.tw>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
 令螢幕保護程式正常結束 檢查中... 如果螢幕保護程式已經啟動則回復 (螢幕不轉黑) 密碼不正確 想要在鎖定畫面中顯示的訊息 請輸入您的密碼... 查詢螢幕保護程式已啟動的時間 查詢螢幕保護程式狀態 切換使用者 要求螢幕保護程式立刻鎖定畫面 螢幕保護程式已使用 %d 秒。
 螢幕保護程式使用中
 螢幕保護程式未使用
 螢幕保護程式目前未使用。
 開啟螢幕保護程式（螢幕轉黑） 解除鎖定 此應用程式的版本 您已開啟 Caps Lock 鍵。 