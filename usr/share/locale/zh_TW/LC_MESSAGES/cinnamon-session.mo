��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �  !   �     �     �  -   �  5   (     ^     e  "   �  *   �  *   �     �          #     =  %   D     j     w     z  !   �     �     �     �     �     �          !  !   .     P  !   ]       L   �     �     �  !        $  
   1     <     I     e     ~  '   �     �  !   �     �  $        >     Z     j     ~     �  !   �  (   �  (   �  E        [  )   b      �     �  W   �  N     &   j  "   �  
   �  
   �  
   �     �     �     �     =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session 3.1.92
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-02-26 15:50+0000
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese (Taiwan) <zh-l10n@lists.linux.org.tw>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
  - Cinnamon 作業階段管理員 一個程式仍在執行中： AUTOSTART_DIR 應用程式不接受以命令列開啟文件 不能傳送文件 URI 至「Type=Link」桌面項目 取消 無法連接作業階段總管 無法建立 ICE 監聽插槽：%s 停用到作業階段管理程式的連線 不要載入使用者指定的應用程式 無需使用者確認 啟用除錯碼 程式以代碼 %d 結束 檔案 檔案不是有效的 .desktop 檔案 強制休眠 ID 找不到圖示「%s」 忽略任何現有的限制因素 以訊號 %d 終止 鎖定螢幕 強制登出 登出 是否立刻登出系統？ 不是可以啟動的項目 沒有回應 覆蓋標準的自動啟動目錄 關閉電源 以相衝突的選項呼叫程式 重新開機 因為目前此作業階段即將關閉電腦，拒絕新的客戶端連線
 記住的應用程式 強制重新啟動 是否立刻重新啟動系統？ SESSION_NAME 暫停(_U) 作業階段 作業階段管理選項： 要使用的作業階段 顯示作業階段管理選項 顯示嚴重錯誤對話盒以供測試 強制關閉電腦 是否立刻關閉這個系統？ 有些程式仍在執行中： 指定含有已儲存組態的檔案 指定作業階段管理 ID 準備啟動 %s 以訊號 %d 停止 強制暫停 強制切換使用者 這個程式正在阻擋登出。 本系統將在 %d 秒後自動重啟。 本系統將在 %d 秒後自動關機。 無法啟動登入作業階段（並且無法連線至 X 伺服器） 不明 無法辨識的桌面檔案版本「%s」 無法辨識的啟動選項：%d 此應用程式版本 正在等待程式結束。中斷這些程式可能造成您進行中的工作遺失。 正在等待程式結束。中斷這些程式可能造成您的工作遺失。 您目前以「%s」的身分登入。 您將在 %d 秒後自動登出。 取消(_C) 休眠(_H) 登出(_L) 重新啟動(_R) 關閉電腦(_S) 切換使用者(_S) 