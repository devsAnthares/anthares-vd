��          �      �           	     !     A      [     |     �     �     �  3   �  ;     5   S  <   �  w   �  @   >  9     3   �  K   �  5   9  I   o     �  /   �        �  $       &   3     Z  #   x     �     �     �     �  /   	  ;   8	  2   t	  8   �	  l   �	  4   M
  0   �
  4   �
  P   �
  5   9  K   o      �  ;   �  $                                        
                                                   	                Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=cups-pk-helper
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2012-02-14 08:27+0000
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese (Taiwan) (http://www.transifex.net/projects/p/freedesktop/language/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0
 加入/移除/編輯類別 加入/移除/編輯本地端印表機 加入/移除/編輯印表機 加入/移除/編輯遠端印表機 變更印表機設定值 啟用/停用印表機 取得可用裝置的清單 取得/設定伺服器設定值 需要特權才能加入/移除/編輯類別。 需要特權才能加入/移除/編輯本地端印表機。 需要特權才能加入/移除/編輯印表機。 需要特權才能加入/移除/編輯遠端印表機。 需要特權才能變更印表機設定值。這應該只有「印表機」系統設定值面板會需要。 需要特權才能啟用/停用印表機或類別。 需要特權才能取得可用裝置的清單。 需要特權才能取得/設定伺服器設定值。 需要特權才能重新啟動/取消/編輯另一位使用者擁有的工作。 需要特權才能重新啟動/取消/編輯工作。 需要特權才能設定印表機、設定類別、設為預設印表機。 重新啟動/取消/編輯工作 重新啟動/取消/編輯另一位使用者擁有的工作 將印表機設定為預設印表機 