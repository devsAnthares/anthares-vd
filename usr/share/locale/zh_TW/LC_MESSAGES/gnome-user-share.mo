��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     O     b  $   {     �     �     �  H   �  s        �  F   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share 2.28.0
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-25 14:49+0800
Last-Translator: Chao-Hsiung Liao <j_h_liau@yahoo.com.tw>
Language-Team: Chinese (traditional)
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.4
 %s 的公用檔案 %s 的公用檔案於 %s 若啟用則執行個人檔案分享 個人檔案分享 分享 分享設定值 開啟個人檔案分享以透過網路分享這個資料夾的內容。 何時會詢問密碼。可能的數值是「never」(永不)、「on_write」(寫入時)和「always」(永遠)。 何時需要密碼 share;files;http;network;copy;send;分享;檔案;網路;複製;傳送; 