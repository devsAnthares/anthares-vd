��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  +   m     �  I   �     �  ,         /  <   P  '   �     �  Q   �  [        x  !   �  (   �  ,   �     	     	     4	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: eu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-02-18 14:07+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <itzulpena@euskalgnu.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Pantaila-babeslea ongi amaitzea eragiten du Egiaztatzen... Pantaila-babeslea aktibo egonez gero, desaktibatu (kendu pantaila beltza) Pasahitz okerra Blokeatutako pantailan bistaratuko den mezua Mesedez, sartu zure pasahitza... Kontsultatu zenbat denbora egon den pantaila-babeslea aktibo Kontsultatu pantaila-babeslearen egoera Aldatu erabiltzailea Pantaila berehala blokeatzeko esaten dio exekutatzen ari den pantaila-babesleari. Pantaila-babeslea segundo %d egon da aktibo.
 Pantaila-babeslea %d segundo egon da aktibo.
 Pantaila-babeslea aktibo dago
 Pantaila-babeslea ez dago aktibo
 Pantaila-babeslea ez dago unean aktibo.
 Aktibatu pantaila-babeslea (belztu pantaila) Desblokeatu Aplikazio honen bertsioa Blok.maius. aktibatuta dago 