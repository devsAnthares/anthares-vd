��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �          #  	   3     =     I     Y     l     u  %   �  #   �  K   �     	     4	     B	  ,   _	  K   �	  �   �	  +   �
  �   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: eu_to_be_translate
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-11-03 17:56+0100
Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@zundan.com>
Language-Team: Basque <librezale@librezale.eus>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %R %R:%S %a, %R %a, %R∶%S %a, %b %e_%R %a, %b %e_%R:%S %a, %b %-e_%l:%M %p %a- %b %-e_%l:%M:%S %p %a, %l:%M %p %a, %l:%M:%S %p %b %-e_%R %b %e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p %d. CRTCk ezin du %s irteera bideratu %d CRTCk ez du biraketa=%d onartzen %d. CRTC: %dx%d@%dHz modua saiatzen %dx%d@%dHz irteerarekin (%d. pasaldia)
 %d. CRTCren moduak saiatzen
 Zehaztu gabea ezin da %s irteerara klonatu ezin izan da CRTCrik esleitu irteeretara:
%s hautatutako moduetariko bat ere ez ba modu erabilgarriekin bateragarria:
%s %s irteerak ez dauka klonatutako beste irteeren parametro berdinak:
dagoen modua = %d, modu berria = %d
dauden koordenatuak = (%d, %d), koordenatu berriak = (%d, %d)
dagoen biraketa = %d, biraketa berria = %d %s irteerak ez du %dx%d@%dHz modua onartzen eskatutako tamaina birtuala ez zaio tamaina erabilgarriari doitzen: eskatutakoa=(%d, %d), gutxienekoa=(%d, %d), gehienezkoa=(%d, %d) 