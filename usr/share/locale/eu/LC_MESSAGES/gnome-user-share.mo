��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     �  %     9   @  !   z     �     �  Z   �  o        �  0   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-09-29 00:13+0200
Last-Translator: Inaki Larranaga Murgoitio <dooteo@zundan.com>
Language-Team: Basque <librezale@librezale.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
 %s(r)en fitxategi publikoak %s(r)en fitxategi publikoak (%s-(e)n) Abiarazi fitxategi pertsonalak partekatzea gaituta badago Fitxategi pertsonalak partekatzea Partekatzea Partekatzearen ezarpenak Aktibatu fitxategi pertsonalak partekatzea karpeta honen edukia sarean zehar partekatzeko. Noiz eskatu pasahitza. Balio erabilgarriak: "never" (inoiz ere ez), "on_write" (idaztean), eta "always" (beti). Noiz eskatu pasahitza partekatu;fitxategiak;http;sarea;kopiatu;bidali; 