��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >     -     E     W     f     �     �  !   �  /   �  %     !   8     Z  /   f     �     �     �     �  �   �     �     �  >   �  +   �     &      4  5   U     �  !   �  <   �  ,     e   0  (   �  (   �     �           )  7   B     z  3   �     �  ,   �     �  8        Q  4   j  +   �  #   �  	   �     �          )     >     N     k     �     �     �                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: eu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2011-04-01 12:11+0200
Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>
Language-Team: Basque <itzulpena@euskalgnu.org>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);

X-Generator: KBabel 1.11.4
 Aktibatu plugin gehiago _Plugin aktiboak: Gehitu plugina Itxi elkarrizketa-koadroa Konfiguratu hautatutako plugina Desaktibatu hautatutako plugina Gutxiagotu pluginaren lehentasuna Talde lehenetsia (leihoa sortzean esleitutakoa) Gaitu/desgaitu instalatutako pluginak Handiagotu pluginaren lehentasuna Adierazlea: Mantendu eta kudeatu taldeak leihoko bereiztuta Teklatu-adierazlearen pluginak Teklatu-adierazlearen pluginak Teklatuaren diseinua Teklatuaren diseinua Teklatuaren "%s" diseinua
Copyright-a &#169; X.Org Fundazioa eta XKeyboardConfig-en laguntzaileak
Lizentziari buruz jakiteko ikusi paketearen metadatuak Teklatu modeloa Teklatuaren aukerak Kargatu diseinu eta aukera exotikoak (oso gutxi erabilitakoak) Kargatu konfigurazioko elementu gehigarriak Azalpenik ez. Aurreikusi teklatuaren diseinuak Gorde/Leheneratu adierazleak diseinu-taldeekin batera Bigarren mailako taldeak Erakutsi banderak miniaplikazioan Erakutsi banderak miniaplikazioan uneko diseinua adierazteko Erakutsi diseinuen izenak talde-izenen ordez Erakutsi diseinuen izenak taldeen izenen ordez (hainbat diseinu onartzen duen XFree bertsioan soilik) Teklatuaren aurrebista, X desplazamendua Teklatuaren aurrebista, Y desplazamendua Teklatuaren aurrebista, altuera Teklatuaren aurrebista, zabalera Atzeko planoaren kolorea Atzeko planoaren kolorea diseinuaren adierazlearentzako Letra-tipoen familia Letra-tipoen familia diseinuaren adierazlearentzako Letra-tamaina Letra-tamaina diseinuaren adierazlearentzako Aurreko planoaren kolorea Aurreko planoaren kolorea diseinuaren adierazlearentzako Plugin aktiboen zerrenda Gaitutako 'Teklatu-adierazlearen plugin'-en zerrenda Errorea gertatu da irudi bat kargatzean: %s Ezin da laguntzako fitxategia ireki Ezezaguna Errorea XKB hasieratzean Plugin _erabilgarriak: teklatuaren diseinua teklatu modeloa "%s" diseinua "%s" diseinuak "%s" modeloa, %s eta %s diseinurik ez aukerarik ez "%s" aukera "%s" aukerak 