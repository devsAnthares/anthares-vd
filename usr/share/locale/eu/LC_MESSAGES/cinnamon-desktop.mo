��          �   %   �      p     q     ~     �     �  	   �     �  $   �  D     <   J     �     �     �     �     �  %   �  '     )   F  '   p  :   �  +   �  I   �  F   I  �   �  *   ^  t   �  i   �  9   h  �  �     *	     7	     G	     Z	  	   p	  #   z	  $   �	  J   �	  R   
     a
  	   n
     x
      �
     �
  +   �
  2   �
  3   ,  .   `  C   �  .   �  \     K   _  �   �  +   |  �   �  �   )  =   �                                   	                       
                                                                 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: eu_to_be_translate
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-01-27 22:13+0100
Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>
Language-Team: Basque <itzulpena@euskalgnu.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %d CRTC ezin du %s irteera bideratu %d CRTCek ez du biraketa=%s onartzen %d CRTC: %dx%d@%dHz modua saiatzen %dx%d@%dHz irteerarekin (%d. pasaldia)
 Ezin izan da terminala aurkitu, xterm erabiliko da, nahiz eta agian ez funtzionatu Eramangarria Ezezaguna RANDR hedapena ez dago Moduak saiatzen %d CRTCrentzako
 ezin da %s irteerara klonatu ezin izan da CRTrik esleitu irteeretara:
%s ezin izan da %d. CRTCari buruzko informazioa lortu ezin izan da %d irteerari buruzko informazioa lortu ezin izan da pantailen tamainen barrutia lortu ezin izan da pantailen baliabideak lortu (CRTCak, irteerak, moduak) ezin izan da %d. CRTCaren konfigurazioa ezarri pantailaren gordetako konfiguraziotariko bat bera ere ez dator bat konfigurazio aktiboarekin hautatutako moduetariko bat ere ez ba modu erabilgarriekin bateragarria:
%s %s irteerak ez dauka klonatutako beste irteeren parametro berdinak:
dagoen modua = %d, modu berria = %d
dauden koordenatuak = (%d, %d), koordenatu berriak = (%d, %d)
dagoen biraketa = %s, biraketa berria = %s %s irteerak ez du %dx%d@%dHz modua onartzen eskatutako %d. CRTCaren posizioa/tamaina baimendutako mugatik at dago: posizioa=(%d, %d), tamaina=(%d, %d), gehienezkoa=(%d, %d) eskatutako tamaina birtuala ez zaio tamaina erabilgarriari doitzen: eskatutakoa=(%d, %d), gutxienekoa=(%d, %d), gehienezkoa=(%d, %d) kudeatu gabeko X errorea pantailen tamainen barrutia lortzean 