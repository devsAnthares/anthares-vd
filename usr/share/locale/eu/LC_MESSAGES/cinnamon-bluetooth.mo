��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 z  %     �	     �	     �	     �	     �	     �	  :   
     Q
     h
  $   m
     �
  +   �
  /   �
     �
            V   8     �  	   �     �      �     �  %   �       	     !        =  1   \     �     �     �     �     �     �          ,     H  3   \     �     �     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-02 21:20+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Helbidea Beti baimendu sarbidea %s(r) autorizazio eskaera Bluetooth-a Bluetooth ezarpenak Bluetooth-a desgaituta dago Bluetooth-a hardwarearen kommutadoreagatik desgaituta dago Arakatu fitxategiak... Utzi Konfiguratu Bluetooth-aren ezarpenak Konexioa %s gailuak '%s' zerbitzura sarbidea nahi du %s gailuak ordenagailu honekin parekatu nahi du Ez dator bat Errorea gailua arakatzean Baimendu oraingoan soilik Gailua ezabatzen baduzu, berriro konfiguratu beharko duzu hurrengoan erabili aurretik. Teklatuaren ezarpenak Bat dator Saguaren ezarpenak Sagu eta touchpad-aren ezarpenak Ez Ez da Bluetooth moldagailurik aurkitu Ados Parekatua %s(r)entzako parekatze berrespena Parekatze eskaera %s-(r)entzat Berretsi '%s' PINa gailuarenarekin bat datorrela. Sartu gailuan aipatutako PINa Ukatu Kendu '%s' gailuen zerrendatik? Kendu gailua Bidali fitxategiak gailura... Bidali fitxategiak... Konfiguratu gailu berria Konfiguratu gailu berria... Soinuaren ezarpenak Eskatutako gailua ezin da arakatu, errorea '%s'(e)n Mota Ikusgaitasuna "%s"(r)en ikusgaitasuna Bai konektatzen... deskonektatzen... hardwarea desgaituta 1. orria 2. orria 