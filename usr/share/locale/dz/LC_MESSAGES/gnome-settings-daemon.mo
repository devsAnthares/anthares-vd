��    J      l  e   �      P     Q  
   c     n     s  K   y  [   �  &   !     H  s   N  A   �  (     S   -      �  "   �  $   �  $   �  &   	     6	     F	     X	  >   ^	  9   �	     �	  A   �	  7   %
  8   ]
  #   �
     �
     �
     �
                '     9     E     M     S     Z     `     r     {     �     �     �     �     �     �     �     �     
  #        @  #   N  -   r  z   �  &     m   B     �     �     �     �     �  $   �  	     *     �   B  �   �  �   ]  	   �     	          5     ;  �  J  R        X  *   t     �    �  ;  �  �   �     �  �  �  �   [  �   5  '  �  V   �  n   T  �   �  q   u  �   �  1   �  1   �     �          3   8  �   l  �   Y   �   -!  �   "  e   �"  X   #     _#  ^   ~#  g   �#  g   E$  B   �$  0   �$     !%  $   @%     e%  ?   {%  0   �%  8   �%     %&  W   A&  Z   �&     �&  ?   '     S'  �   l'  Q   �'  `   I(  �   �(  3   k)  �   �)  �   1*  �  �*  �   �,  e  9-  Q   �.     �.  -   
/     8/  9   Q/  p   �/  3   �/  �   00  �  �0  �  �2  8  v4  +   �6  +   �6  \   7  +   d7  h   �7     J          C               2   A   E       B   &         (   6   3      "   ?          I   $             *       <             @   1   ;   !         '       D             9       )                                .   F   
       >   H          :       -             5         +                =   8              #   ,          	          G       %   4   /          0   7    A_vailable files: Background Beep Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Could not get default terminal. Verify that your default terminal command is set and points to a valid application. Couldn't execute command: %s
Verify that this is a valid command. Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) GConf key %s set to type %s but its expected type was %s
 Home folder It seems that another application already has access to key '%u'. Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Select sound file... Siren Slow Keys Alert Sound Sound not set for this event. Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: gnome-control-center.HEAD.dz
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-06-21 23:33+0530
Last-Translator: Tshewang Norbu <bumthap2006@hotmail.com>
Language-Team: DZONGKHA <pgeyleg@dit.gov.bt>
Language: dz
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Poedit-Language: Dzongkha
X-Poedit-Country: BHUTAN
X-Poedit-SourceCharset: utf-8
 འཐོབ་ཚུགས་པའི་ཡིག་སྣོད་ཚུ།(_v) རྒྱབ་གཞི་ བརྡ་སྐད་རྐྱབས། བོ་ཡིང་། སྣོད་ཐོ་"%s"གསར་བསྐྲུན་འབད་མི་ཚུགས་པས།
འདི་འོད་རྟགས་ཚུ བསྒྱུར་བཅོས་འབད་བཅུག་ནི་ལུ་དགོཔ་ཨིན། སྣོད་ཐོ་"%s"གསར་བསྐྲུན་འབད་མི་ཚུགས་པས།
འདི་མཱའུསི་གི་དཔག་བྱེད་བརྗོད་དོན་ བསྒྱུར་བཅོས་འབད་བཅུག་ནི་ལུ་དགོཔ་ཨིན། ལག་ལེན་པའི་ ཁྱིམ་གྱི་སྣོད་ཐོ་ ངོས་འཛིན་འབད་མི་ཚུགས་པས། ཏིང་སྒྲ། སྔོན་སྒྲིག་ཊར་མི་ནཱལ་ཐོབ་མ་ཚུགས། ཁྱོད་ཀྱི་སྔོན་སྒྲིག་ཊར་མི་ནཱལ་བརྡ་བཀོད་གཞི་སྒྲིག་འབད་ཡོད་མི་དང་ནུས་ཅན་འཇུག་སྤྱོད་ལུ་དོན་ཚན་ཚུ་བདེན་སྦྱོར་འབད། བརྡ་བཀོད་ %s ལག་ལེན་འཐབ་མ་ཚུགས། 
འདི་ནུས་ཅན་བརྡ་བཀོད་ཨིན་ཟེར་བདེན་སྦྱོར་འབད།  སྒྲ་སྐད་ཡིག་སྣོད་%sདེ་ དཔེ་ཚད་%sསྦེ་ མངོན་གསལ་འབད་མ་ཚུགས། གློག་འཕྲུལ་དེ་ གཉིད་ནང་བཙུགས་མ་ཚུགས།
གློག་འཕྲུལ་དེ་ངེས་བདེན་སྦེ་ རིམ་སྒྲིག་འབད་མ་འབད་ བདེན་སྦྱོར་འབད། ཉེན་བརྡ་འདི་ ལོག་སྟེ་མ་སྟོན།(_n) ལྡེ་མིག་ལྷོད་ཆ་ ཤུགས་ལྡན་བཟོ་ནི་ཨིན་ན? ཁྱོད་ཀྱིས་སྦྱར་རྩི་ཅན་གྱི་ ལྡེ་མིག་ཚུ་ ཤུགས་ལྡན་བཟོ་ནི་ཨིན་ན? ལྡེ་མིག་ལྷོད་ཆ་ ཤུགས་མེད་བཟོ་ནི་ཨིན་ན?། ཁྱོད་ཀྱིས་སྦྱར་རྩི་ཅན་གྱི་ ལྡེ་མིག་ཚུ་ ཤུགས་མེད་བཟོ་ནི་ཨིན་ན? ཤུགས་ལྡན་མ་བཟོ།(_n) ཤུགས་མེད་མ་བཟོ།(_n) ཕྱིར་བཏོན། ལྡེ་མིག་(%s)ལུ་འབྲེལ་མཐུད་ཡོད་མི་
(%s)གཡོག་བཀོལ་ནིའི་ འབད་རྩོལ་བསྐྱེདཔ་ད་ འཛོལ་བ་ཅིག་བྱུང་ནུག ཇི་ཀཱོནཕི་ལྡེ་མིག་%sདེ་ དབྱེ་བ་%sལུ་ གཞི་སྒྲིག་འབད་ནུག དེ་འབདཝ་ད་ རེ་བ་བསྐྱེད་མི་དབྱེ་བ་དེ་%sཨིན་པས།
 ཁྱིམ་གྱི་ལེ་སྣོད། གློག་རིམ་གཞན་ཅིག་གིས་ ཧེ་མ་ལས་རང་ ལྡེ་མིག་''%u'ནང་ འཛུལ་སྤྱོད་འབད་འབདཝ་བཟུམ་ཅིག་འདུག ཀི་ བའིན་ཌིང་(%s) གིས་ དེ་ཉིད་ཀྱི་བྱ་བ་ ལན་ཐེངས་མང་རབས་ཅིག་ ངེས་འཛིན་འབད་ནུག
 ཀི་ བའིན་ཌིང་(%s) གིས་ དེ་ཉིད་ཀྱི་བཱའིན་ཌིང་ ལན་ཐེངས་མང་རབས་ཅིག་ ངེས་འཛིན་འབད་ནུག
 ཀི་ བཱའིན་ཌིང་(%s)དེ་ ཧེམ་ལས་རང་ ལག་ལེན་འཐབ་སྟེ་འདུག
 ཀི་ བའིན་ཌིང་ (%s) ཡོངས་སྒྲུབ་མིན་འདུག
 ཀི་ བའིན་ཌིང་(%s) ནུས་མེད་ཨིན་པས།
 ལྡེ་སྒྲོམ། བརའུ་ཟར་གྲོགས་རམ་ གསར་བཙུགས་འབད། ཝེབ་བརའུ་ཟར་གྲོགས་རམ་ གསར་བཙུགས་འབད མོཌི་མེཔ་ཡིག་སྣོད་ཚུ་ མངོན་གསལ་འབད། གསལ་གཞི་ལྡེ་མིག་རྐྱབས། ཕྱིར་བསྐྱོད་འབད། ནང་བསྐྱོད། ཕྱིར་བསྐྱོད། མཱའུསི། མཱའུསི་གི་དགའ་གདམ་ཚུ། སྒྲ་སྐད་མིན་འདུག གཏང་། (ཡང་ན་ གཏང་/ཐེམ) འཚོལ་ཞིབ། སྒྲ་སྐད་ཡིག་སྣོད་སེལ་འཐུ་འབད། སྒྲ་སྐད་ཡིག་སྣོད་སེལ་འཐུ་འབད་... སྒྲ་འཕྲུལ། ལྡེ་མིག་དྲན་བརྡ་ལྷོད། སྒྲ་སྐད། བྱུང་ལས་ཀྱི་དོན་ལུ་སྒྲ་སྐད་གཞི་སྒྲིག་མ་འབད་བས། གསལ་གཞི་ཉེན་སྲུང་འགོ་བཙུགས། སྦྱར་རྩི་ཅན་གྱི་ལྡེ་མིག་དྲན་བརྡ། ཚིག་ཡིག་/ཡིག་རྐྱང་དང་ ཚིག་ཡིག་/* ལེགས་སྐྱོང་འཐབ་མི་ཚུ་ མིང་དཔྱད་འབད། རིམ་ལུགས་སྒྲ་སྐད། ཡིག་སྣོད་%sདེ་ ནུས་ཅན་གྱི་ ཝེབ་ཡིག་སྣོད་ཅིག་མེན་པས། བྱུང་ལས་འདི་གི་དོན་ལུ་ སྒྲ་སྐད་ཡིག་སྣོད་དེ་མིན་འདུག བྱུང་ལས་འདི་གི་དོན་ལུ་ སྒྲ་སྐད་ཡིག་སྣོད་དེ་མིན་འདུག
ཁྱོད་ཀྱིས་ སྔོན་སྒྲིག་སྒྲ་སྐད་ཀྱི་ཆ་ཚན་ཅིག་གི་དོན་ལུ་ཇི་ནོམ་རྣར་ཉན་ཐུམ་སྒྲིལ་་དེ་ གཞི་བཙུགས་འབད་ནི་ཨིནམ་འོང་། གྲོགས་རམ་%sབཀྲམ་སྟོན་འབད་ནི་ལུ་ འཛོལ་བ་ཅིག་བྱུང་ནུག གསལ་གཞི་ཉེན་སྲུང་ འགོ་བཙུགས་ནི་ལུ་ འཛོལ་བ་ཅིག་བྱུང་ནུག 

%s

གསལ་གཞི་ཉེན་སྲུང་གི་ ལས་འགན་ཚུ་གིས་ ལཱ་ཡུན་འདི་ནང་ ལཱ་འབད་མི་བཏུབ། ཡིག་དཔར་བརྐྱབ་ནིའི་བར་མཚམས། སྐད་ཤུགས སྐད་ཤུགས་མར་ཕབ། སྐད་མེད། སྐད་ཤུགས་ཀྱི་རིམ་པ། སྐད་ཤུགས་རིམ་པ་ སྐད་ཤུགས་བརྒྱ་ཆའི་ནང་། སྐད་ཤུགས་ཡར་སེང་། ཁྱོད་ཀྱིས་ མོཌི་མེཔ་ཡིག་སྣོད་(ཚུ) མངོན་གསལ་འབད་ནི་ཨིན་ན? ཁྱོད་ཀྱིས་སོར་ལྡེ་དེ་ སྐར་ཆ་༨ ཀྱི་རང་ལུ་ཨེབ་བཞག་དགོ ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་དེ་ ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ ལྡེ་མིག་གི་ལྷོད་ཆའི་ཁྱད་རྣམ་དོན་ལུ་ མགྱོགས་ཐབས་འདི་ཨིན། ཁྱོད་ཀྱིས་སོར་ལྡེ་དེ་ འབྱེལཝ་འབྱེལ་ས་རང་ ཚར་༥ ཨེབ། ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་དེ་ ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ སྦྱར་རྩི་ལྡེ་མིག་གི་ཁྱད་རྣམ་དོན་ལུ་ མགྱོགས་ཐབས་འདི་ཨིན། ཁྱོད་ཀྱིས་སྟབས་གཅིག་ལུ་ ལྡེ་མིག་གཉིས་ཨེབ་ ཡང་ན་ སོར་ལྡེ་དེ་ འབྱེལཝ་འབྱེལ་ས་རང་ ཚར་༥ཨེབ། འདི་གིས་ ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ སྦྱར་རྩི་ཅན་གྱི་ལྡེ་མིག་ཁྱད་རྣམ་དེ་ ཨོཕ་རྐྱབ་ཨིན། ཤུགས་ལྡན་བཟོ།(_A) ཤུགས་མེད་བཟོ།(_D) འཕྲིན་དོན་འདི་ ལོག་སྟེ་མ་སྟོན།(_D) མངོན་གསལ་འབད།(_L) མངོན་གསལ་འབད་ཡོད་པའི་ཡིག་སྣོད་ཚུ་:(_L) 