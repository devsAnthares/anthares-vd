��          D      l       �   &   �   �   �   �   <  �   �  �  m  �   Z  �  �  �  �  8  �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center.HEAD.dz
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:02+0000
Last-Translator: Tshewang Norbu <Unknown>
Language-Team: DZONGKHA <pgeyleg@dit.gov.bt>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: BHUTAN
X-Poedit-Language: Dzongkha
X-Poedit-SourceCharset: utf-8
 གྲོགས་རམ་%sབཀྲམ་སྟོན་འབད་ནི་ལུ་ འཛོལ་བ་ཅིག་བྱུང་ནུག ཁྱོད་ཀྱིས་སོར་ལྡེ་དེ་ སྐར་ཆ་༨ ཀྱི་རང་ལུ་ཨེབ་བཞག་དགོ ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་དེ་ ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ ལྡེ་མིག་གི་ལྷོད་ཆའི་ཁྱད་རྣམ་དོན་ལུ་ མགྱོགས་ཐབས་འདི་ཨིན། ཁྱོད་ཀྱིས་སོར་ལྡེ་དེ་ འབྱེལཝ་འབྱེལ་ས་རང་ ཚར་༥ ཨེབ། ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་དེ་ ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ སྦྱར་རྩི་ལྡེ་མིག་གི་ཁྱད་རྣམ་དོན་ལུ་ མགྱོགས་ཐབས་འདི་ཨིན། ཁྱོད་ཀྱིས་སྟབས་གཅིག་ལུ་ ལྡེ་མིག་གཉིས་ཨེབ་ ཡང་ན་ སོར་ལྡེ་དེ་ འབྱེལཝ་འབྱེལ་ས་རང་ ཚར་༥ཨེབ། འདི་གིས་ ཁྱོད་ཀྱི་ལྡེ་སྒྲོམ་ལཱ་འབད་ནི་ལུ་ ཕན་གནོད་ཡོད་པའི་ སྦྱར་རྩི་ཅན་གྱི་ལྡེ་མིག་ཁྱད་རྣམ་དེ་ ཨོཕ་རྐྱབ་ཨིན། 