��          �      ,      �     �     �     �     �  l   �     @     F  )   \     �     �     �  Q   �          3  
   F     Q  �  d  B     O   R     �  u   �  �  4     �  o   �  �   `  N   	  ]   n	  �   �	  �   m
  K   ^  l   �       Q   5     	                                                            
             %s's public files %s's public files on %s Always File Sharing Preferences If this is true, the Public directory in the users home directory will be shared when the user is logged in. Never Personal File Sharing Preferences for sharing of personal files Share Files Share Public directory Share Public files on network When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords When writing files _Password: _Require password: Project-Id-Version: gnome-user-share.HEAD.pot
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-11-02 12:22+0530
Last-Translator: norbu <nor_den@hotmail.com>
Language-Team: Dzongkha <pgyeleg@dit.gov.bt>
Language: dz
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Poedit-Language: Dzongkha
X-Poedit-Country: BHUTAN
X-Poedit-SourceCharset: utf-8
 %s གི་མི་མང་ཡིག་སྣོད་ཚུ་ %s གི་མི་མང་ཡིག་སྣོད་ཚུ་ %s གུ་ ཨ་རྟག་རང་ ཡིག་སྣོད་རུབ་སྤྱོད་འབད་ནིའི་དགའ་གདམ་ཚུ་ འ་ནི་འདི་བདེན་པ་ཨིན་པ་ཅིན་ ལག་ལེན་པའི་ཁྱིམ་སྣོད་ཐོ་འདི་ནང་གི་ མི་མང་གི་སྣོད་ཐོ་འདི་ ལག་ལེན་པ་འདི་ནང་བསྐྱོད་འབད་ཡོད་པའི་སྐབས་ རུབ་སྤྱོད་འབད་འོང་། ནམ་ཡང་ རང་དོན་གྱི་ཡིག་སྣོད་རུབ་སྤྱོད་འབད་ནི་ རང་དོན་གྱི་ཡིག་སྣོད་ཚུ་གི་རུབ་སྤྱོད་འབད་ནིའི་དོན་ལུ་ དགའ་གདམ་ཚུ་ ཡིག་སྣོད་ཚུ་རུབ་སྤྱོད་འབད་ མི་མང་གི་སྣོད་ཐོ་རུབ་སྤྱོད་འབད་ ཡོངགས་འབྲེལ་གུ་ལུ་ མི་མང་གི་ཡིག་སྣོད་ཚུ་རུབ་སྤྱོད་འབད་ ཆོག་ཡིག་ཚུ་གི་དོན་ལུ་ནམ་འདྲི་ནི་ཨིན་ན། འབད་ཚུགས་པའི་གནས་གོང་ "never"  "on_write"  དང་ "always" ཚུ་ཨིན། ཆོག་ཡིག་ཚུ་ནམ་དགོཔ་ཨིན་ན་ ཡིག་སྣོད་ཚུ་རྩོམ་སྒྲིག་འབད་བའི་སྐབས་ ཆོག་ཡིག་:(_P) དགོས་མཁོ་ཡོད་པའི་ཆོག་ཡིག་: (_R) 