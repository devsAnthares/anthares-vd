��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  g        w  |   �       C   0  )   t  O   �  W   �  9   F  }   �  M   �  $   L	  -   q	  C   �	  P   �	     4
  &   J
  (   q
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-04-18 13:35+0000
Last-Translator: M. Nishonov <mnishonov@gmail.com>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Экранни қулфлагич сеансдан чиройли чиқишни таъминлайди Текширилмоқда... Агар экранни қулфлагич фаол бўлса, уни тўхтатиш (экран қулфини очиш) Паролингиз хато Қулфланган экранда кўринадиган ёзув Паролингизни киритинг Экранни қулфлагич фаол бўлган вақтни сўраш Экранни қулфлагич ҳолати ҳақида маълумот сўраш Бошқа фойдаланувчи бўлиб кириш Экран қулфлагичнинг фаол жараёнига экранни дарров қулфлашни айтади Экран ҳимояси %d сониядан сўнг фаоллашади.
 Экран ҳимояси фаол.
 Экран ҳимояси фаол эмас.
 Экран ҳимояси айни пайтда фаол эмас.
 Экранни қулфлагични ёқинг (экранни қулфлаш) Қулфни очиш Ушбу дастур версияси Caps Lock тугмаси ёқилган. 