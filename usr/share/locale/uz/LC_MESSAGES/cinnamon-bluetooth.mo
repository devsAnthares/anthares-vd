��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 x  %     �	  &   �	  -   �	  	    
      

     +
  `   H
      �
     �
  3   �
       G   !  Y   i     �  F   �  9   "  �   \  +   	     5  '   I  \   q     �  I   �          $  5   9  -   o  d   �  D        G  O   W  (   �  2   �  $     *   (  $   S  !   x  a   �     �       .   "     Q     V     n  !   �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-04-19 12:49+0000
Last-Translator: M. Nishonov <mnishonov@gmail.com>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Манзил Доим рухсат берилсин %s дан авторизация талаби Bluetooth Bluetooth мосламалари Bluetooth ўчирилган Bluetooth қурилмаларни ўчириб-ёққич томонидан ўчирилган Файлларни кўриш... Бекор қилиш Bluetooth хусусиятларини созлаш Уланиш %s қурилма   '%s' хизматга рухсат сўраяпти %s қурилма компьютер билан жуфтлашишни ҳоҳлаяпти Мос келмайди Қурилмани кўришда хатолик содир бўлди Шу сафаргагина рухсат берилсин Агар қурилмани олиб ташласангиз, кейинги сафар фойдаланишда яна ўрнатишингизга тўғри келади. Клавиатура созламалари Мос келади Сичқонча мосламалари Сичқонча ва қўл билан бошқариш панели созламалари Йўқ Бирорта ҳам Bluetooth адаптерлари топилмади ОК Жуфтлашган %s учун жуфтлашувни тасдиқлаш %s учун жуфтлашишга сўров '%s' PIN қурилмадагиси билан мос келаётганини тасдиқланг. Қурилмада келтирилган PIN ни киритинг. Рад этиш '%s' қурилмалар рўйхатидан олиб ташлансинми? Қурилмани олиб ташлаш Қурилмага файлларни юбориш Файлларни жўнатиш... Янги қурилмани ўрнатиш Янги қурилмани улаш Товуш созламалари Ушбу қурилмани кўриб бўлмади, '%s' хатолик содир бўлди. Тури Кўринувчанлиги “%s” нинг кўринувчанлиги Ха боғланяпти... узиляпти... Қурилма ўчирилган 1-саҳифа 2- саҳифа 