��    )      d  ;   �      �     �     �  5   �  7        C  (   J  )   s  %   �  '   �     �       !        (     9     <     P     \     d     z  '   �  '   �  P   �     *      3  +   T     �     �     �     �  E   �       &        A     `  X   |  
   �     �     �  
   �     �  �  
  '   �	  %   �	  9   "
  K   \
  	   �
  /   �
  -   �
  /     4   @     u     �  -   �     �     �     �                !     ?  <   M  *   �  M   �  
     ,     >   ;  -   z     �     �     �  K   �     :  0   F  *   w     �  ]   �          %  
   5     @     H               
                             #   !          $                                          	       &           %                                 (          )             "            '               - the Cinnamon session manager A program is still running: Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Enable debugging code FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Lock Screen Log out Not a launchable item Not responding Override standard autostart directories Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Suspend Anyway Switch User Anyway Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: Cinnamon-ast
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:19+0000
Last-Translator: Mikel González <Unknown>
Language-Team: Asturian (http://www.transifex.com/projects/p/cinnamon-ast/language/ast/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ast
  - el remanador de sesiones de Cinnamon Tovía hai un programa executándose: L'aplicación nun aceuta documentos na llinia de comandos Nun se puen pasar los URIs de documentos a entraes d'escritoriu 'Type=Link' Encaboxar Nun pudo coneutase al alministrador de sesiones Nun se pudo crear el socket d'escucha ICE: %s Desactivar la conexón al remanador de sesiones Nun cargar les aplicaciones especificaes pol usuariu Activar códigu de depuración FICHERU El ficheru nun ye un ficheru .desktop válidu Hibernar de toes maneres ID Nun s'atopó l'iconu '%s' Candar pantalla Finar la sesión Nun ye un elementu llanciable Nun respuende Sobreescribir los direutorios d'aniciu automáticu estándar Programa llamáu con opciones conflictives Refugando la conexón d'un cliente nuevu porque la sesión ta siendo zarrada
 S_uspender Tovía hai dalgunos programes executándose: Especificar el ficheru que contién la configuración guardada Especificar la ID del remanamientu de sesión Entamando %s Suspender de toes maneres Trocar usuariu de toes maneres Nun ye posible entamar la sesión (y nun ye posible coneutar al sirvidor X) Desconocíu Versión '%s' del ficheru desktop non reconocida Opción de llanzamientu non reconocida: %d Versión d'esta aplicación Esperando a que finen los programes. Interrumpir estos programes pue facer que pierdas datos. I_vernar _Zarrar Sesión _Reaniciar _Apagar _Trocar usuariu 