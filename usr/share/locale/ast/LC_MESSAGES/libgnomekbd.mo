��    <      �  S   �      (  x   )  8   �     �     �  
                  <     W  *   t      �     �     �  
   �  )        ,     G     d          �  z   �     #     2  ,   C     p     �  3   �     �     �  7   �  (   5	  a   ^	  2   �	     �	     
     1
     N
     j
  -   
     �
  (   �
     �
  &   �
       -   0     ^  .   y  '   �     �     �               &     5     N     d  	   u  
        �  �  �  z   �  ?        F     _     v     �  #   �  #   �  &   �  >     "   K     n  %   �  
   �  2   �  %   �  #     %   ;  $   a     �  �   �     =     P  <   d  .   �     �  ;   �        "   3  >   V  <   �     �  <   R  !   �  !   �     �     �       0   %     V  3   k     �  2   �     �  8     !   <  ;   ^  %   �      �     �                4     F     e     |     �     �     �     *   :          '      (   )   +   1   3                           6   &       -   ;           /                  8                       4   %         ,   
      7      $      	         .   !   2                  9            #              5   "                         <                        0    A collection of scripts to run whenever the keyboard state is reloaded. Useful for re-applying xmodmap based adjustments A list of modmap files available in the $HOME directory. Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Failed to init GConf: %s
 Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator Test (%s) Keyboard Indicator plugins Keyboard Update Handlers Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) Suppress the "X sysconfig changed" warning message The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s modmap file list no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2010-01-05 10:50+0000
PO-Revision-Date: 2010-01-06 18:01+0100
Last-Translator: astur <malditoastur@gmail.com>
Language-Team: Asturian <gnome@softastur.org>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Language: asturian
 Una coleición de scripts pa executar cuando l'estáu del tecláu se recarga. Util pa re-aplicar axustes basaos en xmodmap Una llista de ficheros modmap disponibles nel direutoriu $HOME. Activa más complementos Com_plementos activos: Amestar Complementu Zarrar el diálogu Configurar el complementu esbilláu Desactivar el complementu esbilláu Amenorgar la prioridá del complementu Grupu por defeutu, asignáu durante la creación de la ventana Activa/Desactiva plugins instalaos Fallu nel aniciu de GConf: %s
 Aumentar la prioridá del complementu Indicador: Caltenga y alministre grupos separtaos per ventana Complementos del Indicador de Tecláu Preba del indicador de tecláu (%s) Complementos del indicador de tecláu Remanadores d'Anovamientu de Tecláu Distribución del tecláu Distribución de tecláu «%s»
Copyright &#169; la Fundación X.Org y los contribuyidores de XKeyboardConfig
Pa la llicencia llee los metadatos del paquete Modelu del tecláu Opciones de tecláu Cargar distribuciones y opciones exótiques, raramente usaes Cargar elementos de configuración adicionales Ensin descripción. Guardar/restaurar indicadores xunto colos grupos d'esquemes Grupos secundarios Amuesa banderes na miniaplicación Amuesa banderes na miniaplicación pa indicar l'esquema actual Amuesa nomes de los esquemes n'arróu de nomes de los grupos Amuesa los nomes de los esquemes n'arróu de nomes de los grupos (namái pa versiones de XFree que sofiten esquemes múltiples) Suprime'l mensax d'alvertencia «sysconfig de X modificáu» Vista Previa de Tecláu, offset X Vista Previa de Tecláu, offset Y Vista Previa de Tecláu, altor Vista Previa de Tecláu, anchor El color del fondu El color de fondu pal indicador de distribución La familia de fontes La familia de fontes pal indicador de distribución El tamañu de la fonte El tamañu de fonte pal indicador de distribución El color del primer planu El color del primer planu pal indicador de distribución La llista de complementos activos La llista de complementos del Indicador de Tecláu activaos Hebo un fallu al cargar una imaxe: %s Nun pudo abrise'l ficheru d'aida Fallu d'anicialización de XKB C_omplementos disponibles: distribución de tecláu modelu de tecláu esquema «%s» esquemes «%s» modelu «%s», %s y %s llista de ficheros modmap ensin esquema ensin opciones opción «%s» opciones «%s» 