��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     ^     w  B   �  $   �     �       e   &  u   �       *                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-11 
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Spanish <gnome-es-list@gnome.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Archivos públicos de %s Archivos públicos de %s en %s Lanzar la compartición de archivos personales si están activadas Compartición de archivos personales Compartición Opciones de compartición Active la compartición de archivos personales para compartir el contenido de esta carpeta en la red. Cuando pedir contraseñas. Las opciones posibles son: "never" (nunca), "on_write" (al escribir) y "always" (siempre). Cuando requerir contraseñas compartir;archivos;http;red;copiar;enviar; 