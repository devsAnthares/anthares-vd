��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  I   U  /   �  3   �  .     #   2     V     u  (   �  I   �                    	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Jorge González <aloriel@gmail.com>
Language-Team: Spanish (http://www.transifex.com/projects/p/freedesktop/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Para cambiar la configuración de la pantalla de acceso debe autenticarse Para cambiar datos de usuario debe autenticarse Para cambiar sus datos de usuario debe autenticarse Cambie la configuración de pantalla de acceso Cambie sus propios datos de usuario Activar código de depuración Gestione cuentas de usuario Mostrar información de versión y salir Proporciona interfaces DBus para consultar y manipular
cuentas de usuario Reemplazar la instancia actual 