��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �               '  	   6     @     M     ]     p     y  )   �  %   �  L   �  !   "	     D	  "   T	  1   w	  K   �	  �   �	  *   �
  w   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-09-24 11:51+0200
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: es <gnome-es-list@gnome.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.7
Plural-Forms: nplurals=2; plural=(n != 1);
 %R %R:%S %a, %d de %b, %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p el CRTC %d no puede conducir la salida %s el CRTC %d no soporta la rotación=%d CRTC %d: intentando el modo %dx%d@%dHz con salida en %dx%d@%dHz (pasada %d)
 Intentando modos para el CRTC %d
 Sin especificar no se puede clonar la salida en %s no se pudieron asignar los CRTC a las salidas:
%s ninguno de los modos seleccionados es compatible con los modos posibles:
%s la salida %s no tiene los mismos parámetros que otra salida clonada:
modo existente = %d, modo nuevo = %d
coordenadas existentes = (%d, %d), coordenadas nuevas = (%d, %d)
rotación existente = %d, rotación nueva = %d la salida %s no soporta el modo %dx%d@%dHz el tamaño virtual requerido no se ajusta al espacio disponible: requerido=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 