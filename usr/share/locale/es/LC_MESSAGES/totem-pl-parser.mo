��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       i     v     ~     �     �  :   �  2   �                          	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-12-20 17:01+0100
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: es <gnome-es-list@gnome.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.6
Plural-Forms: nplurals=2; plural=(n != 1);
 CD de sonido Blu-ray DVD Televisión digital Falló al montar %s. No hay un soporte en la unidad para el dispositivo «%s». Compruebe que haya un disco presente en la unidad. CD de vídeo 