��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 x  %  
   �	     �	      �	  	   �	     �	     
  9   
     X
     m
     v
  	   �
  3   �
  3   �
                1  b   I     �     �     �  -   �       '        C  	   K  %   U     {  ;   �  -   �       .        =  !   U     w     �  "   �     �  B   �     *     /     ;     Q     U     c     t  	   �  	   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-05-06 09:49+0000
Last-Translator: toniem <toni.estevez@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Dirección Conceder acceso siempre Solicitud de autorización de %s Bluetooth Configuración de Bluetooth Bluetooth desactivado El Bluetooth está desactivado por el interruptor físico Examinar archivos… Cancelar Configuración de Bluetooth Conexión El dispositivo %s quiere acceder al servicio «%s» El dispositivo %s quiere vincularse con este equipo No coincide Error al examinar el dispositivo Conceder sólo esta vez Si elimina el dispositivo tendrá que configurarlo de nuevo antes de poder usarlo la próxima vez. Configuración del teclado Coincide Configuración del ratón Configuración del ratón y del panel táctil No No se encontraron adaptadores Bluetooth Aceptar Vinculado Confirmación de vinculación para %s Solicitud de vinculación de %s Confirme que el PIN «%s» coincide con el del dispositivo. Introduzca el PIN indicado en el dispositivo. Rechazar ¿Eliminar «%s» de la lista de dispositivos? Eliminar el dispositivo Enviar archivos al dispositivo... Enviar archivos… Configurar un dispositivo nuevo Configurar un nuevo dispositivo... Configuración del sonido El dispositivo solicitado no se puede examinar, el error es «%s» Tipo Visibilidad Visibilidad de «%s» Sí conectando... desconectando... hardware desactivado página 1 página 2 