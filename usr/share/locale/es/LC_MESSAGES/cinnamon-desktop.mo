��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
     )
     6
     9
     ?
     H
  )   T
  %   ~
  L   �
  U   �
  	   G     Q     e  %   q  !   �  "   �  1   �  2     6   A  <   x  L   �  7     Z   :  K   �  �   �  *   �  �   �  w   z  H   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-06 18:18+0200
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Gtranslator 2.91.5
 %A, %e de %B, %R %A, %e de %B, %R:%S %A, %e de %B, %l:%M %p %A, %e de %B, %l:%M:%S %p %A, %e de %B %R %R:%S %l:%M %p %l:%M:%S %p el CRTC %d no puede conducir la salida %s el CRTC %d no soporta la rotación=%s CRTC %d: intentando el modo %dx%d@%dHz con salida en %dx%d@%dHz (pasada %d)
 No se puede encontrar un terminal, usando xterm, incluso aunque puede que no funcione Portátil Pantallas en espejo Desconocida la extensión RANDR no está presente Intentando modos para el CRTC %d
 no se puede clonar la salida en %s no se pudieron asignar los CRTC a las salidas:
%s no se pudo obtener información acerca del CRTC %d no se pudo obtener información acerca de la salida %d no se pudo obtener el rango de los tamaños de las pantallas no se pudieron obtener los recursos de las pantallas (CRTCs, salidas, modos) no se pudo establecer la configuración para el CRTC %d ninguna de las configuraciones de pantalla guardadas coincide con la configuración activa ninguno de los modos seleccionados es compatible con los modos posibles:
%s la salida %s no tiene los mismos parámetros que otra salida clonada:
modo existente = %d, modo nuevo = %d
coordenadas existentes = (%d, %d), coordenadas nuevas = (%d, %d)
rotación existente = %s, rotación nueva = %s la salida %s no soporta el modo %dx%d@%dHz la posición y/o tamaño requeridos para el CRTC %d está fuera de los límites permitidos: posición=(%d, %d), tamaño=(%d, %d), máximo=(%d, %d) el tamaño virtual requerido no se ajusta al espacio disponible: requerido=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) error X no manejado al obtener el rango de los tamaños de las pantallas 