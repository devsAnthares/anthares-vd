��    %      D  5   l      @  *   A  1   l      �     �     �     �     �     �     �  
   �       /        >     ]     n     �     �     �      �  -   �  -   �  )     +   F     r     �  	   �     �     �     �  F   �  9   "  G   \     �     �     �     �  �  �  4   f  4   �  %   �     �     �     	     	     !	  
   .	     9	  	   E	  4   O	  +   �	     �	  #   �	     �	     �	     
  %   
  /   2
  6   b
  5   �
  9   �
  $   	  +   .     Z     m     q     �  D   �  D   �  M         n     w     �  %   �                  
                 	                                                                                 "   !   #                                     %         $           --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. Actions Comment Connecting... Continue Description Desktop Extensions GEOMETRY Nemo could not create the required folder "%s". Nemo's main menu is now hidden No actions found No extensions found None Operation cancelled SSH Show the version of the program. Sorry, could not change the group of "%s": %s Sorry, could not change the owner of "%s": %s The folder "%s" cannot be opened on "%s". The folder contents could not be displayed. The group could not be changed. The owner could not be changed. Try Again URL User Details WebDAV (HTTP) You do not have the permissions necessary to change the group of "%s". You do not have the permissions necessary to rename "%s". You do not have the permissions necessary to view the contents of "%s". [URI...] _Server: _Type: no information available Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-03-27 18:39+0000
Last-Translator: amm <Unknown>
Language-Team: Sardinian <sc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 Non si podet impreare --check cun àteras optziones. Non si podet impreare --geometry cun prus de un'URI. Non si podet impreare --quit cun URI. Atziones Cummentu Colleghende·si... Sighi Descritzione Iscrivania Estensiones GEOMETRIA Nemo no at pòdidu creare sa cartella rechesta "%s". Su menù printzipale de Nemo como est cuadu Peruna atzione agatada Non s'est agatada peruna estensione Perunu Operatzione annullada SSH Ammustra sa versione de su programma. Non s'est pòdidu cambiare su grupu de "%s": %s Non s'est pòdidu cambiare su propietàriu de "%s": %s Non s'est pòdidu abèrrere sa cartella "%s" in "%s". Non s'est pòdidu ammustrare su cuntenuto de sa cartella. Non s'est pòdidu cambiare su grupu. Non s'est pòdidu cambiare su propietàriu. Torra·bi a proare URL Detàllios de s'utente WebDAV (HTTP) Non tenes sos permissos netzessàrios pro cambiare su grupu de "%s". Non tenes sos permissos netzessàrios pro mudare su nùmene de "%s". Non tenes sos permissos netzessàrios pro visualizare sos cuntenutos de "%s". [URI...] _Server: _Tipu: Non b'at informatzione a disponimentu 