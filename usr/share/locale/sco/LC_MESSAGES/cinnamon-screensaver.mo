��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  *     
   6  C   A     �  '   �     �  8   �  #        :  E   G  `   �     �     	  (   &  )   O     y     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-10-14 14:27+0000
Last-Translator: Eilidh Martin <Unknown>
Language-Team: Scots <sco@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Causes the screensaver tae stop gracefully Checkin... If the screensaver is active then deactivate it (untume the screen) Wrang tryst-word Message tae be displayit in lock screen Please inter yer tryst-word... Recken the lenth o' time the screensaver has been active Recken the state o' the screensaver Switch Uiser Tells the runnin' screensaver process tae lock the screen immediately The screensaver has been active fer %d second.
 The screensaver has been active fer %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver isnae active richt noo.
 Tirn the screensaver on (tume the screen) Unlock Version o' this applicatioun Ye hae the Caps Lock key oan. 