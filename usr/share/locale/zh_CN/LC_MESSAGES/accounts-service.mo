��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  s  �  '   +  !   S  '   u     �     �     �     �     �  7        I               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: kkyeer <kkyeer@gmail.com>
Language-Team: Chinese (China) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 修改登录窗口配置：需要授权 修改用户数据：需要授权 修改您的用户数据：需要授权 修改登录窗口配置 修改您的用户数据 开启代码调试 管理用户账户 输出账户信息并退出 为查询和使用提供D-Bus界面
 用户账户信息 马上替换已存在内容 