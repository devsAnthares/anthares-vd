��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �     �     �     �  *   �  5        T     [  %   z     �  $   �     �     �          !  %   (     N     [     ^     v     �     �     �     �     �     �  	   �  !   �       !   #     E  4   L     �     �     �     �  
   �     �     �     �       *   '     R     _     {  $   �     �     �     �     �            %   8  (   ^  ;   �     �  &   �      �       N   +  N   z  #   �  %   �  
     
     
   )  
   4  
   ?     J     =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-26 05:04+0000
Last-Translator: AlephAlpha <alephalpha911@gmail.com>
Language-Team: Chinese/Simplified <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
  - Cinnamon会话管理器 有程序仍在运行中： 自动启动目录 应用程序在命令行上不接受文档 无法将文档 URI 传送给“Type=Link”桌面项 取消 无法连接到会话管理器 无法创建 ICE 监听套接字：%s 禁止连接到会话管理器 不装入用户指定的应用程序 不提示用户确认 启用调试代码 以代码 %d 退出 文件 文件不是有效的 .desktop 文件 强制休眠 ID 图标“%s”未找到 忽略现有限制因素 被信号 %d 杀死 锁定屏幕 强制注销 注销 现在注销系统吗？ 不是可调用项 无响应 取代默认的自动启动目录 关机 调用程序的选项互相冲突 重启 拒绝新客户连接，原因是会话正在关闭
 记住的应用程序 强制重新启动 现在重启该系统吗？ 会话名称 挂起(_U) 会话 会话管理选项： 要使用的会话 显示会话管理选项 显示用于测试的鲸鱼失败对话框 强制关机 现在关闭此系统吗？ 有些程序仍在运行中： 指定包含已保存配置的文件 指定会话管理 ID 正在启动 %s 被信号 %d 停止 强制挂起 强制切换用户 此程序正在阻止注销。 系统将在 %d 秒后自动重启。 系统即将在 %d 秒后自动关闭。 无法启动登录会话(并且无法连接到 X 服务器) 未知 不识别的桌面文件版本“%s” 无法识别的调用选项：%d 此应用程序的版本 正在等候程序结束。中断程序可能导致您的工作成果丢失。 正在等候程序完成。中断程序可能导致您的工作成果丢失。 您目前以“%s”身份登录。 您即将在 %d 秒后自动注销。 取消(_C) 休眠(_H) 注销(_L) 重启(_R) 关机(_S) 切换用户(_S) 