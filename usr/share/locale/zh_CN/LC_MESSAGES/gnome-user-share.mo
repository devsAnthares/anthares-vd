��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     ^     q  *   �     �     �     �  E   �  J   &     q  \   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-04-13 13:38-0600
Last-Translator: Jeff Bai <jeffbai@aosc.xyz>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.7.1
 %s 的公开文件 %s 在 %s 上的公开文件 如果已启用则启动个人文件共享 个人文件共享 共享 共享设置 打开个人文件共享以在网络上共享此文件夹的内容。 何时询问密码，可用选项为："never", "on_write" 和 "always"。 何时要求密码 share;files;bluetooth;obex;http;network;copy;send;共享;文件;蓝牙;网络;复制;发送; 