��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
      
     /
     2
     8
     A
     P
     n
  C   �
  D   �
          #     0     7     K     e  !   ~  %   �  %   �     �  0        <  0   [  -   �  �   �  $   l  i   �  ]   �  ?   Y                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-23 04:17+0800
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Language: 
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.5
 %m月%d日%R%A %m月%d日%R:%S%A %m月%d日%A %I:%M %p %m月%d日%A %I:%M:%S %p %-m月%-d日%A %R %R:%S %I:%M %p %A %I:%M:%S %p CRTC %d 不能驱动输出 %s CRTC %d 不支持旋转 =%s CRTC %d：尝试 %dx%d@%dHz 模式输出在 %dx%d@%dHz (通过 %d)
 无法找到正在使用xterm的终端程序，可能他无法工作 笔记本电脑 镜像显示 未知 没有 RANDR 扩展 为 CRTC %d 尝试模式
 无法克隆到输出 %s 无法指定 CRTC 到输出：
%s 无法获得CRTC %d的相关信息。 无法得到输出 %d 的有关信息 无法得到屏幕大小范围 无法得到屏幕来源(CRTC，输出，模式) 无法为 CRTC %d 设定配置 已保存的设置中没有匹配活动设置的 所选模式均不匹配可能的模式：
%s 输出 %s 和另一个克隆的输出没有相同参数：
当前模式 = %d，新模式 = %d
当前坐标 = (%d，%d)，新坐标 = (%d，%d)
当前旋转 = %s，新旋转 = %s 输出 %s 不支持模式 %dx%d@%dHz CRTC %d 需要的位置/大小在限制允许之外：位置=(%d, %d)，大小=(%d, %d)，最大=(%d, %d) 需要的虚拟大小大于可用大小：需求=(%d, %d)，最小=(%d, %d)，最大=(%d, %d) 试图得到屏幕大小范围时出现无法处理的 X 错误 