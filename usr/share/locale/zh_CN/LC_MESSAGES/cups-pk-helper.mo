��          �      �           	     !     A      [     |     �     �     �  3   �  ;     5   S  <   �  w   �  @   >  9     3   �  K   �  5   9  I   o     �  /   �        �  $     �  #        '  #   E     i          �     �  )   �  5   �  /   ,	  5   \	  c   �	  .   �	  *   %
  .   P
  D   
  ,   �
  ?   �
     1  )   L  !   v                                     
                                                   	                Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=cups-pk-helper
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2012-03-22 06:16+0000
Last-Translator: Tommy He <lovenemesis@gmail.com>
Language-Team: Chinese (China) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 添加/移除/编辑类 添加/移除/编辑本地打印机 添加/移除/编辑打印机 添加/移除/编辑远程打印机 更改打印机设定 启用/禁用打印机 获得可用设备列表 获得/设置服务器设定 需要授权来添加/移除/编辑类。 需要授权来添加/移除/编辑本地打印机。 需要授权来添加/移除/编辑打印机。 需要授权来添加/移除/编辑远程打印机。 需要授权来更改打印机设定。仅在从打印机系统设定面板执行操作时需要。 需要授权来启用/禁用打印机或类。 需要授权来获得可用设备列表。 需要授权来获取/设定服务器设置。 需要授权来重启/取消/编辑其他用户所拥有的任务。 需要授权来重启/取消/编辑任务。 需要授权来设定打印机，类，或者默认打印机。 重启/取消/编辑任务 重启/取消/编辑其他用户的任务 设定打印机为默认打印机 