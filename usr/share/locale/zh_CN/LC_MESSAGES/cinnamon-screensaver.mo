��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  !   �     �  A   �       !        3  -   L  !   z     �  ?   �  &   �  "     %   3  "   Y      |     �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-01-05 04:55+0000
Last-Translator: 张秋雨 <VDerGoW@hotmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 使屏幕保护程序正常退出 正在检查… 如果屏幕保护程序正活动则使之不活动(解除黑屏) 密码不正确 要在锁屏界面显示的消息 请输入您的密码... 查询屏幕保护程序已经活动的时间 查询屏幕保护程序的状态 切换用户 通知正在运行的屏幕保护程序进程立即锁定屏幕 屏幕保护程序已激活 %d 秒。
 屏幕保护程序状态为活动
 屏幕保护程序状态为不活动
 屏幕保护当前没有激活。
 开启屏幕保护程序(黑屏) 解锁 此应用程序的版本 您打开了 Caps Lock 键。 