��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >     +     >     R     _     o     �     �  $   �     �     �       *        >     T     j     w  u   �     �       '        <     U     b  +   u     �  !   �  6   �  $     Q   &     x     �     �     �     �     �  	             ,     9  	   X     b     ~  *   �     �     �     �     �               ,     9     H  	   c  	   m     w                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2011-03-14 17:36+0000
PO-Revision-Date: 2011-03-06 08:26+0800
Last-Translator: Lele Long <schemacs@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 激活更多插件 激活插件(_P)： 添加插件 关闭对话框 配置选中插件 禁用选中插件 降低插件优先级 窗口创建时赋予的默认的组 启用/禁用已安装的插件 提高插件优先级 指示器： 为每个窗口保持和管理单独的组 键盘指示器插件 键盘指示器插件 键盘布局 键盘布局 键盘布局“%s”
版权所有&#169 X.Org 基金会及 XKeyboardConfig 贡献者
许可请查看软件包元数据 键盘型号 键盘选项 部分加载，仅使用布局和选项 加载额外配置条目 无描述。 预览键盘布局 与布局分组一起保存/恢复指示器 次组 在面板小程序中显示标记 在面板小程序中显示指示当前布局的标记 显示布局名称而不是组名称 显示布局名称而不是组名称(只针对支持多种布局的 XFree 版本) 键盘预览，X 偏移量 键盘预览，Y 偏移量 键盘预览，高度 键盘预览，宽度 背景颜色 布局指示器的背景颜色 字体族 布局指示器的字体族 字体大小 布局指示器的字体大小 前景色 布局指示器的前景色 已激活插件列表 已激活的键盘指示器插件的列表 加载图片出错：%s 无法打开帮助文件 未知 XKB 初始化出错 可用插件(_A)： 键盘布局 键盘型号 布局“%s” 型号“%s”，%s 和 %s 无布局 无选项 选项“%s” 