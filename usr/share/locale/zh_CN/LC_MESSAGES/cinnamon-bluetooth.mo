��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     �	     �	     �	     �	  !    
     "
     2
     9
     L
  "   S
     v
  	   �
     �
     �
  E   �
                    %     >     B     [  	   b     l     ~  9   �  &   �     �  )   �     (     5     N     ^     n     �  +   �     �  	   �     �     �     �     �     
  	     	   $                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-28 12:57+0000
Last-Translator: AlephAlpha <alephalpha911@gmail.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 地址 总是允许访问 %s请求授权 蓝牙 蓝牙设置 蓝牙已禁用 蓝牙已通过硬件开关禁用 浏览文件... 取消 配置蓝牙设置 连接 %s设备请求使用“%s”服务 %s设备想和本机配对 不匹配 浏览设备出错 仅授权此次 如果您移除该设备，下次使用前您需要重新设置它。 键盘设置 匹配 鼠标设置 鼠标和触摸板设置 否 未找到蓝牙适配器 确定 已配对 确认和%s配对 来自%s的配对请求 请确认 PIN 码 ‘%s‘ 是否与设备上的一致。 请输入设备上提示的 PIN 码。 拒绝 将“%s”从设备列表中移除吗？ 移除设备 发送文件到设备... 发送文件... 设置新设备 安装新设备... 声音设置 要求的设备不能浏览，错误：'%s' 类型 可见性 “%s”的可见性 是 正在连接... 正在断开连接... 硬件已关闭 第 1 页 第 2 页 