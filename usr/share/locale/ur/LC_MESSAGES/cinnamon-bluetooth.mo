��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 w  %     �	  ,   �	  ;   �	     
  $   
  #   A
  8   e
     �
     �
  /   �
  
   �
  7   	  E   A     �  2   �  $   �  �   �  '   �     �     �  *   �       -        F     O  *   ]  *   �  y   �  U   -     �  5   �     �  5   �       #   1     U     s  ]   �  
   �     �          %     ,  %   @  )   f  
   �  
   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-22 02:37+0000
Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 پتہ ہمیشہ رسائی کی اجازت دیں اختیار دہی کی درخواست %s کی طرف سے بلوٹوتھ بلیوٹوتھ کی ترتیبات بلوٹوتھ غیر فعال ہے بلوٹوتھ ہارڈ وئیر بٹن سے بند ہے جستجو فائلز۔۔۔ منسوخ کریں بلوٹوتھ ترتیبات تشکیل دیں اتصال آلہ %s رسائی چاہتا ہے سروس '%s' تک آلہ %s اس کمپیوٹر سے پیئر  ہونا چاہتا ہے مشابہ نہیں ہے ڈرائیو براؤز کرتے وقت مسئلہ صرف اس بار اجازت دیں اگر آپ ڈیوائس ہٹائیں گے تو اسے دوبارہ استعمال کرنے کے لیے آپ کو پھر نصب کرنا پڑے گا۔ کلیدی تختے کی ترتیبات مشابہ ہے ماؤس کی ترتیبات ماؤس اور ٹچ پیڈ ترتیبات نہیں بلوٹوتھ ایڈاپٹر نہیں ملے ٹھیک جڑا ہوا پیرئنگ کی تصدیق برائے %s پیرئنگ درخواست %s کے لیے براہ مہربانی تصدیق کریں کہ کیا پِن '%s' آلے کی پِن سے مشابہت رکھتی ہے. براہ مہربانی، آلے پر بتایا گیا پن کوڈ درج کریں۔ مسترد کریں '%s' کو ڈیوائس فہرست سے ہٹائیں؟ آلہ ہٹائیں آلے کی جانب فائلیں ارسال کریں فائلیں بھیجیں نئی ڈیوائس نصب کریں نیا آلہ نصب کریں آواز کی ترتیبات درخواست کردہ آلہ براؤز نہیں کیا جاسکتا، مسئلہ ہے '%s' نوعیت دستیابی “%s” کی دستیابی ہاں جڑ رہا ہے... منقطع کیا جا رہا ہے... ہارڈوئیر غیر فعال کردہ صفحہ 1 صفحہ 2 