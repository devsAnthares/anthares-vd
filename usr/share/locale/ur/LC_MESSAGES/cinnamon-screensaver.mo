��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  Y   	     c  c   �     �  B   �  A   <  e   ~  =   �     "  p   6  �   �  &   -	  -   T	  A   �	  I   �	     
  &   
  ,   E
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-01-05 18:11+0000
Last-Translator: Muhammad Abid <Unknown>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 سکرین سیور سے آرام کے ساتھ بند کرنے کا سبب بنتا ہے چیک کر رہا ہے۔۔۔ اگر سکرین سیور چالو ہے تو اسے غیر فعال کریں (un-blank the screen) غلط پاسورڈ لاک سکرین میں دکھائے جانے والا پیغام برائے مہربانی اپنا پاسورڈ داخل کریں پتہ لگائیں کے سکرین سیور کتنی دورانیے کے لیے چالو رہا ہے سکرین سیور کی حالت کا پتہ(Query) کریں صارف بدلیں سکرین سیور پراسیس کو بتاتا ہے کی سکرین کو فوری طور پر لاک کر دے اسکرین محافظ %d سیکنڈ کے لیے فعال ہے۔
 اسکرین محافظ %d سیکنڈ کے لیے فعال ہے۔
 اسکرین محافظ فعال ہے
 اسکرین محافظ غیر فعال ہے
 اسکرین محافظ فی الحال فعال نہیں ہے۔
 سکرین سیور چالو کردیں (سکرین خالی کر دیں) غیر مقفل اس ایپلی کیشن کا ورژن آپ کا کیپس لاک بٹن آن ہے۔ 