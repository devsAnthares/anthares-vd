��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  3  �          	                    +     ;     N     d     p          �     �     �     �     �  M   �  L   (	  �   u	  I   �	     I
  ;   Z
  ^   �
  y   �
  ^  o  C   �  �                                                 	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop.master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-09-04 16:27+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 2.1.1
 %R %R:%S %a %R %a %R:%S %a, %e %b_%R %a, %e %b_%R:%S %a, %e %b_%l:%M %p %a, %e %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %a, %e %b_%R %a, %e %b_%R:%S %a, %e %b_%l:%M %p %a, %e %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p кантролер ЭПТ %d не можа кіраваць выхадам %s кантролер ЭПТ %d не падтрымлівае паварот=%d кантролер ЭПТ %d: выпрабоўванне рэжыму %dx%d@%dГц з выхадам на %dx%d@%dГц (спроба %d)
 Выпрабоўванне рэжымаў кантролера ЭПТ %d
 Невядома не ўдалося кланіраваць у выхад %s не ўдалося прызначыць кантролеры ЭПТ для выхадаў:
%s ні адзін з выбраных рэжымаў не сумяшчальны з магчымымі рэжымамі:
%s параметры выхаду %s адрозніваюцца ад параметраў іншага кланіраванага выхаду:
наяўны рэжым = %d, новы рэжым = %d
наяўныя каардынаты = (%d, %d), новыя каардынаты = (%d, %d)
наяўны паварот = %d, новы паварот = %d выхад %s не падтрымлівае рэжым %dx%d@%dГц запатрабаваны віртуальны памер не пасуе да даступнага памеру: запатрабавана=(%d, %d), мінімум=(%d, %d), максімум=(%d, %d) 