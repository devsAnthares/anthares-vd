��    )      d  ;   �      �     �     �     �  	   �     �     �  (        /     ?     F  
   c  *   n     �     �  K   �     	          #     ?     B     ^     a     h     �     �  %   �     �     �     �     �          %     4  
   9     D     [     _     m     ~     �  u  �  
     (     )   6  	   `      j     �  f   �     	     *	     =	     Z	  Q   k	     �	  -   �	  �   
  '   �
     �
  >   �
     5  2   :  
   m     x  5   �  #   �     �  C   �  +   8  0   d     �  8   �  )   �          1     8     I     _     f     z     �     �           &                     (   )                      '         !           "                                                                      #             
   	                  %   $               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Does not match Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-02-17 10:38+0000
Last-Translator: meequz <meequz@gmail.com>
Language-Team: Belarusian <be@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Адрас Заўсёды даваць доступ Запыт аўтарызацыі ад %s Bluetooth Настáўленні Bluetooth Bluetooth выключаны Bluetooth выключаны з дапамогай апаратнага пераключальніка Агляд файлаў... Скасаваць Настройка Bluetooth Злучэнне Прылада %s хоча атрымаць доступ да сэрвісу '%s' Не адпавядае Даць доступ толькі зараз Калі вы выдаліце гэта прыстасаванне, для яго наступнага выкарыстання прыйдзецца паўтарыць наладку. Настройкі клавіятуры Адпавядае Настройкі мышы і сэнсарнай панэлі Не Bluetooth-адаптары не знойдзены Добра Звязаныя Пацверджанне звязвання для %s Запыт звязвання з %s Адмовіць Выдаліць "%s" са спіса прыстасаванняў? Выдаліць прыстасаванне Даслаць файлы на Прыладу... Паслаць файлы... Настроіць новае прыстасаванне Дадаць новую прыладу... Настройкі гуку Тып Бачнасць Бачнасць "%s" Так злучэнне... адключэнне... старонка 1 старонка 2 