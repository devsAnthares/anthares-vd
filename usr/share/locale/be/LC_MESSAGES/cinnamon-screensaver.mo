��          L      |       �   `   �      
     %  )   B     l    �  �   �  1   ~  6   �  7   �                                              The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-04-22 14:34+0000
Last-Translator: meequz <meequz@gmail.com>
Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: be
 Ахоўнік экрана актываваны цягам %d секунды.
 Ахоўнік экрана актываваны цягам %d секунд.
 Ахоўнік экрана актываваны цягам %d секунд.
 Ахоўнік экрана актываваны
 Ахоўнік экрана не актываваны
 Ахоўнік экрана не актываваны.
 Caps Lock уключаны. 