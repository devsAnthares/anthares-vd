��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     
     
     0
  	   G
     Q
     T
     Z
     c
  M   o
  L   �
  �   
  �   �       '   -     U  -   f  I   �  ;   �  ^     W   y  J   �  e     �   �  C     r   U  y   �  ^  B  C   �  �   �  �   �  �   h                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-07 18:54+0300
Last-Translator: Yuri Matsuk <yuri@matsuk.net>
Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: UTF-8
Language: be
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 %A, %e %B, %R %A, %e %B, %R:%S %A, %e %B, %l:%M %p %A, %e %B, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p кантролер ЭПТ %d не можа кіраваць выхадам %s кантролер ЭПТ %d не падтрымлівае паварот=%s кантролер ЭПТ %d: выпрабоўванне рэжыму %dx%d@%dГц з выхадам на %dx%d@%dГц (спроба %d)
 Не ўдалося знайсці тэрмінал. Выкарыстоўваецца "xterm", нават калі ён не працуе Ноўтбук Люстраваныя маніторы Невядомы пашырэнне RANDR адсутнічае Выпрабоўванне рэжымаў кантролера ЭПТ %d
 не ўдалося кланіраваць у выхад %s не ўдалося прызначыць кантролеры ЭПТ для выхадаў:
%s не ўдалося атрымаць звесткі пра кантролер ЭПТ %d не ўдалося атрымаць звесткі аб выхадзе %d не ўдалося сабраць звесткі аб дыяпазоне памераў экрана не ўдалося сабраць звесткі аб рэсурсах экрана (кантролеры ЭПТ, выхады, рэжымы) не ўдалося настроіць кантролер ЭПТ %d ні адна з захаваных канфігурацый экрана не адпавядае актыўнай ні адзін з выбраных рэжымаў не сумяшчальны з магчымымі рэжымамі:
%s параметры выхаду %s адрозніваюцца ад параметраў іншага кланіраванага выхаду:
наяўны рэжым = %d, новы рэжым = %d
наяўныя каардынаты = (%d, %d), новыя каардынаты = (%d, %d)
наяўны паварот = %s, новы паварот = %s выхад %s не падтрымлівае рэжым %dx%d@%dГц запатрабаваная пазіцыя/памер ЭПТ %d па-за дазволенымі межамі: пазіцыя=(%d, %d), памер=(%d, %d), максімум=(%d, %d) запатрабаваны віртуальны памер не пасуе да даступнага памеру: запатрабавана=(%d, %d), мінімум=(%d, %d), максімум=(%d, %d) падчас збору звестак аб дыяпазоне памераў экрана ўзнікла неапрацаваная памылка сістэмы X 