��          L      |       �   /   �      �   =   �   +   5     a  v  i  f   �  C   G  t   �  D         E                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte.master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-09-07 14:20+0300
Last-Translator: Yuras Shumovich <shumovichy@gmail.com>
Language-Team: Belarusian <i18n-bel-googlegroups.com>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
 Памылка (%s) пераўтварэння даных ад нашчадка: ігнаруецца. Памылка чытання даных ад нашчадка: %s. GnuTLS не ўключаны; даныя будуць запісаны на дыск незашыфраванымі! Не ўдалося пераўтварыць знакі з %s у %s. АСЦЯРОЖНА 