��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  Q  w  !   �  )   �  �     >   �     �  6   �  �   0  �   �  ,   �  c   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share.master
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-04-22 12:42+0300
Last-Translator: Yuras Shumovich <shumovichy@gmail.com>
Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Poedit 1.8.11
X-Project-Style: gnome
 %s: публічныя файлы %s: публічныя файлы на %s Запуск праграмы арганізацыі супольнага доступу да файлаў (калі ўключана) Супольны доступ да ўласных файлаў Супольны доступ Настройкі супольнага доступу Уключыце супольны доступ да асабістых файлаў, каб дазволіць доступ праз сеціва да змесціва гэтай папкі. Калі запытваць паролі. Магчымыя значэнні: "never" (ніколі), "on_write" (падчас спробы запісу) і "always" (заўсёды). Калі патрабаваць паролі супольны;рэсурс;http;сетка;капіраванне;паслаць;пасылка; 