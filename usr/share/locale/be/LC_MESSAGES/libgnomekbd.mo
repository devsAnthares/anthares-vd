��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  ,   #  #   P     t  0   �  6   �  0   �  2   #  z   V  B   �  6        K  Q   _  6   �  6   �  +     +   K  �   w  !   \     ~  X   �  K   �     C  I   ^  m   �       -   4  k   b  V   �  �   %  >   �  >   ,  @   k  @   �     �  P        V  _   v     �  W   �  (   F  h   o  ,   �  e     =   k  >   �     �  %   �       +   ?  !   k  G   �     �     �       /   !                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-09-04 19:11+0300
PO-Revision-Date: 2011-09-04 19:11+0300
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian <i18n-bel-gnome@googlegroups.com>
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Уключыць болей плугінаў Уключаныя _плугіны: Новы плугін Закрыць дыялогавае акенца Настройка вылучанага плугіна Выключыць вылучаны плугін Зменшыць прыярытэт плугіна Прадвызначаная група, якая прызначаецца пры стварэнні новага акна Задзейнічанне ўсталяваных плугінаў Павялічыць прыярытэт плугіна Індыкатар: Абслугоўваць асобныя групы для кожнага акна Плугіны індыкатара раскладкі Плугіны індыкатара раскладкі Клавіятурная раскладка Клавіятурная раскладка Клавіятурная раскладка "%s"
Аўтарскія правы &#169; Фонд X.Org і ўдзельнікі XKeyboardConfig
Ліцэнзійныя ўмовы глядзіце ў метаданых пакунка Мадэль клавіятуры Опцыі клавіятуры Загружаць экзатычныя і рэдкія раскладкі і опцыі Загружаць дадатковыя элементы настройкі Няма апісання. Папярэдні агляд клавіятурных раскладак Захоўваць і аднаўляць індыкатары разам з групамі раскладак Другасныя опцыі Паказваць сцягі ў аплеце Паказваць сцягі ў аплеце для азначэння актыўнай раскладкі Паказваць назвы раскладак разам з назвамі груп Паказваць назвы раскладак замест назваў груп (толькі для версій XFree, якія падтрымліваюць некалькі раскладак) Папярэдні агляд клавіятуры, X-зрух Папярэдні агляд клавіятуры, Y-зрух Папярэдні агляд клавіятуры, вышыня Папярэдні агляд клавіятуры, шырыня Фонавы колер Фонавы колер індыкатара актыўнай раскладкі Гарнітура шрыфту Гарнітура шрыфту для індыкатара актыўнай раскладкі Памер шрыфту Памер шрыфту для індыкатара актыўнай раскладкі Колер пярэдняга плана Колер пярэдняга плана для індыкатара актыўнай раскладкі Спіс уключаных плугінаў Спіс уключаных плугінаў індыкатара актыўнай раскладкі Узнікла памылка загрузкі выявы: %s Не ўдалося адкрыць даведачны файл Невядома Памылка ініцыяцыі XKB _Наяўныя плугіны: клавіятурная раскладка мадэль клавіятуры раскладка "%s" раскладкі "%s" раскладкі "%s" мадэль "%s", %s і %s няма раскладкі няма опцый опцыя "%s" опцыі "%s" опцыі "%s" 