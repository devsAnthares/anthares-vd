��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 f  %     �	     �	     �	  	   �	     �	     �	  8   
     A
  	   Z
  &   d
  	   �
  0   �
  :   �
       "     &   6  g   ]     �     �     �     �               3     6     >     ^  I   {  ,   �     �  -   �     )  %   ?     e     y     �     �  =   �     �               ,     /     A     U     i     r                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-08 06:24+0000
Last-Translator: CD <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresă Permite întotdeauna accesul Cerere de autorizare de la %s Bluetooth Setări Bluetooth Bluetooth-ul este dezactivat Bluetooth-ul este dezactivat dintr-un comutator hardware Răsfoiește fișiere... Anulează Configurează setările Bluetooth-ului Conexiune Dispozitivul %s dorește acces la serviciul '%s' Dispozitivul %s dorește să se asocieze cu acest computer Nu se potrivește Eroare la căutarea dispozitivelor Permite accesul doar de această dată Dacă eliminați acest dispozitiv, va trebui să îl setați din nou înainte de următoarea utilizare. Setări tastatură Se potrivește Setări mouse Setări mouse și touchpad Nu Niciun adaptor Bluetooth găsit OK Asociat Confirmarea asocierii pentru %s Cerere de asociere pentru %s Confirmați dacă PIN-ul „%s” se potrivește cu cel de pe dispozitiv. Introduceți PIN-ul menționat pe dispozitiv Respinge Eliminați „%s” din lista de dispozitive? Elimină dispozitivul Trimite fișiere către dispozitiv... Trimite fișiere... Setează un dispozitiv nou Setează un dispozitiv nou... Setări sunet Dispozitivul cerut nu poate fi căutat, eroarea este „%s” Tip Vizibilitate Vizibilitate pentru „%s” Da se conectează... se deconectează... hardware dezactivat pagina 1 pagina 2 