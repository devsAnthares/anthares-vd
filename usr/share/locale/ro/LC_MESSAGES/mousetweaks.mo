��    A      $  Y   ,      �     �     �     �  "   �  A        D  :   ]     �  &   �     �     �     �     �          )     G  0   _     �     �  
   �     �     �      �     �  )     
   5     @  
   L     W     v     �     �  4   �     �  %   �     	     	     7	     C	  7   S	     �	     �	     �	     �	  e   �	     G
     \
     u
     �
  "   �
     �
  	   �
  !   �
  /        A  $   J     o     v     {     �     �     �     �     �    �  %   �  )   �  '     .   B  U   q     �  A   �  
   $  (   /     X  $   ^     �     �     �      �     �  9     
   @     K     Q     ^     }  "   �     �  -   �     	          #  ,   1     ^     y  
   �  B   �  	   �  !   �  4   �     /     O     \  3   j  *   �  #   �     �       x        �     �  .   �  +   �  (        G  	   Y  1   c  7   �  
   �  )   �          
               $     1  
   8     C                                              
                 4   >       2                 &   A   0   +   -      ?   3      1                 *                   :   	                       %                7   5       !   ,       6      =   /       (   9       ;   .   <   )       8              "   $           #   @   '    "Alt" keyboard modifier "Control" keyboard modifier "Shift" keyboard modifier - GNOME mouse accessibility daemon Applet to select different dwell-click types.
Part of Mousetweaks Area to lock the pointer Area to lock the pointer on the panel.
Part of Mousetweaks Button Style Button style of the click-type window. C_trl Capture and Release Controls Capture area Click Type Window Click-type window geometry Click-type window orientation Click-type window style Control your desktop without using mouse buttons Double Click Drag Drag Click Dwell Click Applet Enable dwell click Enable simulated secondary click Failed to Display Help Failed to Open the Universal Access Panel Horizontal Hover Click Icons only Ignore small pointer movements Keyboard modifier: Locked Mouse button Mouse button used to capture or release the pointer. Orientation Orientation of the click-type window. Pointer Capture Applet Pointer Capture Preferences Right Click Secondary Click Selecting Button 0 will capture the pointer immediately Set the active dwell mode Show a click-type window Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Size of capture area Size of the Capture Area Start mousetweaks as a daemon Start mousetweaks in login mode Temporarily lock the mouse pointer Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Width of the capture area in pixels. _About _Alt _Help _Mouse button: _Preferences _Shift _Width: pixels Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&component=general
PO-Revision-Date: 2011-03-16 15:10+0200
Last-Translator: Lucian Adrian Grijincu <lucian.grijincu@gmail.com>
Language-Team: Romanian Gnome Team <gnomero-list@lists.sourceforge.net>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Virtaal 0.6.1
 modificatorul de tastatură „Alt” modificatorul de tastatură „Control” modificatorul de tastatură „Shift” - serviciul GNOME de accesibilitate a mausului Miniaplicație pentru a alege diferite tipuri de clic staționar.
Parte a Mousetweaks Zonă de blocare indicator Zonă pentru a bloca indicatorul în panou.
Parte din Mousetweaks Stil buton Stilul butoanelor ferestrei de tip clic. C_trl Controale de capturare și eliberare Zona de captură Fereastră tip clic Geometria ferestrei de tip clic Orientarea ferestrei de tip clic Stilul ferestrei de tip clic Controlați desktopul fără a utiliza butoanele mausului Clic dublu Trage Clic tragere Miniaplicație clic staționar Activează clicul staționar Activează clicul secundar simulat Afișarea ajutorului a eșuat Deschiderea panoului Acces universal a eșuat Orizontală Clic plutitor Doar iconițe Ignoră mișcările minore ale indicatorului Modificator de tastatură: Blocat Buton maus Buton maus folosit pentru capturarea sau eliberarea indicatorului. Orientare Orientarea ferestrei de tip clic. Miniaplicația de capturare a indicatorului mausului Preferințe capturare indicator Clic dreapta Clic secundar Alegerea butonului 0 va captura imediat indicatorul Definește modul clicului staționar activ Afișează o fereastră de tip clic Închide mousetweaks Clic simplu Dimensiunea și poziția ferestrei de tip clic. Formatul este un șir de geometrie standard al Sistemului de ferestre X. Dimensiunea zonei de captură Dimensiunea zonei de captură Pornește mousetweaks ca un serviciu de fundal Pornește mousetweaks în mod autentificare Blochează temporar indicatorul mausului Text și iconițe Doar text Timp de așteptare înainte de un clic staționar Timp de așteptare înainte de un clic secundar simulat Verticală Lățimea în pixeli a zonei de captură. _Despre _Alt _Ajutor Buton _maus: _Preferințe _Shift _Lățime: pixeli 