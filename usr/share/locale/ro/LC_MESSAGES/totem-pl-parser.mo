��    	      d      �       �      �      �      �      �      	  &     1   D     v  1       �     �     �     �     �  @   �  4   3     h                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem.HEAD.ro
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2018-03-25 14:29+0200
Last-Translator: Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>
Language-Team: Gnome Romanian Translation Team
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 CD audio Blu-ray DVD Televiziune digitală Montarea %s a eșuat. Nu există niciun mediu de stocare pentru dispozitivul „%s”. Verificați dacă ați introdus un disc în unitate. CD video 