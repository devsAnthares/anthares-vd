��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  5  w  (   �  .   �  7        =  	   ]     g  q   �  `   �     U  k   p                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-03-24 23:50+0200
Last-Translator: Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>
Language-Team: Gnome Romanian Translation Team
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Fișierele publice ale utilizatorului %s Fișierele publice ale utilizatorului %s pe %s La activare, pornește partajarea de fișiere personale Partajare de fișiere personale Partajare Configurări de partajarea Activați Partajarea personală de fișiere pentru a partaja conținutul acestui dosar prin intermediul rețelei. Când să se ceară parole. Valorile posibile sunt „never”, „on_write” și „always”. Când să se ceară parole share;files;http;network;copy;send;parajare;partajează;fișiere;rețea;copiere;copiază;trimitere;trimite; 