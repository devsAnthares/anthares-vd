��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  )  �     �     �                    !     1     D     Z     f  	   u          �     �     �     �  $   �     �  L   		  $   V	     {	     �	  (   �	  R   �	  �   "
  (   �
  w                                                 	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-13 03:09+0000
PO-Revision-Date: 2018-08-20 15:00+0200
Last-Translator: Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>
Language-Team: Romanian Gnome Team <gnomero-list@lists.sourceforge.net>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Poedit 2.1.1
 %R %R:%S %a %R %a %R:%S %a %-e %b_%R %a %-e %b_%R:%S %a %-e %b_%l:%M %p %a %-e %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %-e %b_%R %-e %b_%R:%S %-e %b_%l:%M %p %-e %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d nu poate comanda ieșirea %s CRTC %d nu suportă rotație=%d CRTC %d: se încearcă module %dx%d@%dHz cu eșire la %dx%d@%dHz (pasul %d)
 Se încearcă moduri pentru CRTC %d
 Nespecificat nu se poate clona ieșirea %s nu s-a putut atribui CRTC la ieșiri:
%s niciunul dintre modurile selectate nu au fost compatibile cu modurile posibile:
%s ieșirea %s nu are aceiași parametru ca o altă ieșire clonată:
modul existent = %d, mod nou = %d
coordonate existente = (%d, %d), coordonate noi = (%d, %d)
rotire existentă = %d, rotire nouă = %d ieșirea %s nu suportă modul %dx%d@%dHz dimensiunea virtuală cerută nu se potrivește dimensiunii disponibile: cerut=(%d, %d), minim=(%d, %d), maxim=(%d, %d) 