��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  y  r  7   �     $  R   4     �  *   �     �  :   �  ,        ?  R   U  �   �  !   S	  #   u	  /   �	  3   �	     �	     

     '
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-05-14 21:25+0000
Last-Translator: Mertol Serban <mertol.serban@gmail.com>
Language-Team: Romanian Gnome Team <gnomero-list@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1 ? 0: (((n % 100 > 19) || ((n % 100 == 0) && (n != 0))) ? 2: 1));
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ro
 Determină economizorul de ecran să oprească armonios Se verifică... Dacă economizorul de ecran este activ, atunci deactivează-l (populează ecranul) Parolă incorectă Mesajul ce va fi afișat pe ecranul blocat Introdu parola... Interoghează cât timp a fost activ economizorul de ecran Interoghează starea economizorului de ecran Schimbă utilizatorul Transmite procesului activ al economizorului de ecran să blocheze imediat ecranul Economizorul de ecran a fost activ pentru %d secundă.
 Economizorul de ecran a fost activ pentru %d de secunde.
 Protectorul de ecran a fost activ pentru %d de secunde.
 Economizorul de ecran este activ
 Economizorul de ecran este inactiv
 Economizorul de ecran nu este  momentan activ.
 Pornește economizorul de ecran (curăță ecranul) Deblochează Versiunea acestei aplicații Tasta Caps Lock este activă. 