��          �   %   �      p     q     ~     �     �  	   �     �  $   �  D     <   J     �     �     �     �     �  %   �  '     )   F  '   p  :   �  +   �  I   �  F   I  �   �  *   ^  t   �  i   �  9   h  �  �     q	     ~	     �	     �	  	   �	  $   �	     �	  L   
  P   S
     �
  
   �
      �
  $   �
     �
  (     0   C  4   t  1   �  A   �  0     Y   N  R   �  �   �  (   �  �   �  w   p  B   �                                   	                       
                                                                 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-09-25 15:10+0300
Last-Translator: Lucian Adrian Grijincu <lucian.grijincu@gmail.com>
Language-Team: Romanian Gnome Team <gnomero-list@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Virtaal 0.6.1
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A, %B %e CRTC %d nu poate comanda ieșirea %s CRTC %d nu suportă rotație=%s CRTC %d: se încearcă module %dx%d@%dHz cu eșire la %dx%d@%dHz (pasul %d)
 Nu s-a găsit un terminal, se utilizează xterm, deși s-ar putea să nu meargă Laptop Necunoscut Extensia RANDR nu este prezentă Se încearcă moduri pentru CRTC %d
 nu se poate clona ieșirea %s nu s-a putut atribui CRTC la ieșiri:
%s nu s-au putut obține informații despre CRTC %d nu s-au putut obține informații despre ieșirea %d nu s-a putut obține gama dimensiunilor ecranului nu s-au putut obține resursele ecranului (CRTC, ieșiri, moduri) nu s-a putut defini configurația pentru CRTC %d nici una din configurările ecranelor salvate nu se potrivește cu configurația curentă niciunul dintre modurile selectate nu au fost compatibile cu modurile posibile:
%s ieșirea %s nu are aceiași parametru ca o altă ieșire clonată:
modul existent = %d, mod nou = %d
coordonate existente = (%d, %d), coordonate noi = (%d, %d)
rotire existentă = %s, rotire nouă = %s ieșirea %s nu suportă modul %dx%d@%dHz dimensiunea/poziția cerută pentru CRTC %d este în afara limitei admise: poziție=(%d, %d), dimensiune=(%d, %d), maxim=(%d, %d) dimensiunea virtuală cerută nu se potrivește dimensiunii disponibile: cerut=(%d, %d), minim=(%d, %d), maxim=(%d, %d) eroare X negestionată la obținerea gamei dimensiunilor ecranului 