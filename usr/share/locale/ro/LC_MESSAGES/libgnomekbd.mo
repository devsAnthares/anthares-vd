��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  ?  >     ~     �     �     �     �     �       ,   #  +   P     |  
   �  F   �     �     
     &     <  �   R     �       ?     .   V     �  )   �  J   �       "     F   A  9   �  z   �  '   =  '   e  (   �  %   �     �  0   �        ,   1     ^  0   s     �  3   �     �  -     4   3  )   h  
   �     �     �     �     �  =   �     0     J     \  5   l                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekdb
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2011-03-16 18:45+0200
Last-Translator: Lucian Adrian Grijincu <lucian.grijincu@gmail.com>
Language-Team: Romanian Gnome Team <gnomero-list@lists.sourceforge.net>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Virtaal 0.6.1
 Activează mai multe module _Module active: Adaugă modul Închide dialogul Configurează modulul selectat Dezactivează modulul selectat Redu prioritatea modulului Grup implicit, atribuit la crearea ferestrei Activează/dezactivează modulele instalate Mărește prioritatea modulului Indicator: Păstrează și administrează grupuri separate în fiecare fereastră Module Indicator tastatură Module Indicator tastatură Aranjament tastatură Aranjament tastatură Aranjament tastatură „%s”
Drepturi de autor &#169; Fundația X.Org și colaboratorii XKeyboardConfig
Pentru licențiere consultați metadatele pachetului Model tastatură Opțiuni tastatură Încarcă aranjamentele și opțiunile exotice ori rar folosite Încarcă elemente suplimentare de configurare Fără descriere. Previzualizați aranjamente de tastatură Salvează/restarurează indicatorii împreună cu grupurile de aranjamente Grupuri secundare Arată steaguri în miniaplicație Arată steaguri în miniaplicație pentru a indica aranjamentul curent Arată numele aranjamentelor în locul numelor grupurilor Arată numele aranjamentelor în locul numelor grupurilor (doar pentru versiuni de XFree ce suportă aranjamente multiple) Previzualizarea tastaturii, decalajul X Previzualizarea tastaturii, decalajul Y Previzualizarea tastaturii, înălțimea Previzualizarea tastaturii, lățimea Culoarea de fundal Culoarea de fundal a indicatorului de aranjament Familia fontului Familia fontului indicatorului de aranjament Dimensiunea fontului Dimensiunea fontului indicatorului de aranjament Culoarea de prim-plan Culoarea de prim-plan a indicatorului de aranjament Lista modulelor active Lista modulelor indicator tastatură activate S-a produs o eroare la încărcarea unei imagini: %s Nu s-a putut deschide fișierul de ajutor Necunoscut Eroare inițializare XKB _Module disponibile: aranjament tastatură model tastatură aranjament „%s” aranjamente „%s” aranjamente „%s” model „%s”, %s și %s fără aranjament fără opțiuni opțiune „%s” opțiuni „%s” opțiuni „%s” 