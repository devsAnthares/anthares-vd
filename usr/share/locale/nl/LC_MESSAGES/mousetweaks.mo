��    A      $  Y   ,      �     �     �     �  "   �  A        D  :   ]     �  &   �     �     �     �     �          )     G  0   _     �     �  
   �     �     �      �     �  )     
   5     @  
   L     W     v     �     �  4   �     �  %   �     	     	     7	     C	  7   S	     �	     �	     �	     �	  e   �	     G
     \
     u
     �
  "   �
     �
  	   �
  !   �
  /        A  $   J     o     v     {     �     �     �     �     �  V  �     
          8  /   O  R     %   �  R   �  	   K  !   U     w     }     �     �  "   �  #   �  #   �  8     
   Q     \  	   c     m     �  '   �     �  2   �     
  	           .   4     c     x     �  5   �     �  $   �  %   �          2     B  0   N  "        �     �     �  j   �     I     `     w  !   �  $   �     �     �        +     	   H  %   R     x     ~     �  
   �     �     �  	   �     �                                              
                 4   >       2                 &   A   0   +   -      ?   3      1                 *                   :   	                       %                7   5       !   ,       6      =   /       (   9       ;   .   <   )       8              "   $           #   @   '    "Alt" keyboard modifier "Control" keyboard modifier "Shift" keyboard modifier - GNOME mouse accessibility daemon Applet to select different dwell-click types.
Part of Mousetweaks Area to lock the pointer Area to lock the pointer on the panel.
Part of Mousetweaks Button Style Button style of the click-type window. C_trl Capture and Release Controls Capture area Click Type Window Click-type window geometry Click-type window orientation Click-type window style Control your desktop without using mouse buttons Double Click Drag Drag Click Dwell Click Applet Enable dwell click Enable simulated secondary click Failed to Display Help Failed to Open the Universal Access Panel Horizontal Hover Click Icons only Ignore small pointer movements Keyboard modifier: Locked Mouse button Mouse button used to capture or release the pointer. Orientation Orientation of the click-type window. Pointer Capture Applet Pointer Capture Preferences Right Click Secondary Click Selecting Button 0 will capture the pointer immediately Set the active dwell mode Show a click-type window Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Size of capture area Size of the Capture Area Start mousetweaks as a daemon Start mousetweaks in login mode Temporarily lock the mouse pointer Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Width of the capture area in pixels. _About _Alt _Help _Mouse button: _Preferences _Shift _Width: pixels Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-04-03 17:21+0200
Last-Translator: Wouter Bolsterlee <wbolster@gnome.org>
Language-Team: Dutch <vertaling@nl.gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 ‘Alt’-optietoets ‘Control’-optietoets ‘Shift’-optietoets — Muistoegankelijkheidsvoorziening voor Gnome Applet om verschillende types hangklikken te selecteren.
Onderdeel van Mousetweaks Gebied om de aanwijzer vast te zetten Gebied om de muisaanwijzer vast te zetten op het paneel.
Onderdeel van Mousetweaks Knopstijl Knopstijl van het kliktypevenster C_trl Vangen en loslaten Afvanggebied Kliktypevenster Afmetingen van het kliktypevenster Oriëntatie van het kliktypevenster Venstertype van het kliktypevenster De werkomgeving besturen zonder muisknoppen te gebruiken Dubbelklik Slepen Sleepklik Applet voor hangklikken Hangklikken aanzetten Gesimuleerd secundair klikken aanzetten Kon hulp niet tonen Kon het paneel voor universele toegang niet openen Horizontaal Zweefklik Alleen pictogrammen Kleine bewegingen van de muisaanwijzer negeren Toetsenbord-modifier Geblokkeerd Muisknop De muisknop om de aanwijzer te vangen en los te laten Oriëntatie Oriëntatie van het kliktypevenster. Applet voor afvangen van de aanwijzer Aanwijzervangvoorkeuren Rechtermuisklik Tweede klik Knop 0 selecteren zal de aanwijzer meteen vangen De actieve hangklikmodus instellen Een kliktypevenster tonen Mousetweaks afsluiten Enkele klik De afmetingen en positie van het kliktypevenster. Het formaat is een standaard X Window System-tekenreeks. Grootte van vanggebied Grootte van vanggebied Mousetweaks als daemon starten Mousetweaks in loginmodus starten De muisaanwijzer tijdelijk blokkeren Tekst en pictogrammen Alleen tekst Wachttijd voor een hangklik Wachttijd voor een gesimuleerde tweede klik Verticaal Breedte van het vanggebied in pixels. I_nfo _Alt _Hulp _Muisknop: _Voorkeuren _Shift _Breedte: pixels 