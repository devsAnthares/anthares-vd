��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �  
   �     �     �     �     �            	        (     5     E     X     a  %   m  $   �  K   �     	      	     /	  )   O	  I   y	  �   �	  ,   �
  }   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-28 09:07+0100
Last-Translator: Hannie Dumoleyn <hannie@ubuntu-nl.org>
Language-Team: Dutch <gnome-nl-list@gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 2.0
 %R %R:%S %a %R %a %R∶%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d kan uitvoer %s niet aansturen CRTC %d ondersteunt rotation=%d niet CRTC %d: uitproberen modus %dx%d@%dHz met uitvoer op %dx%d@%dHz (ronde %d)
 Proberen modi voor CRTC %d
 Niet opgegeven kan niet klonen naar uitvoer %s kon CRTC's niet aan uitvoer toewijzen:
%s geen van de geselecteerde modi waren compatibel met de mogelijke modi:
%s uitvoer %s heeft niet dezelfde parameters als een andere gekloonde uitvoer:
huidige modus = %d, nieuwe modus = %d
huidige coördinaten = (%d, %d), nieuwe coördinaten = (%d, %d)
huidige rotatie = %d, nieuwe rotatie = %d uitvoer %s ondersteunt modus %dx%d@%dHz niet vereiste virtuele afmeting past niet binnen de beschikbare afmeting: aangevraagd=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 