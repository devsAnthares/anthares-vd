��          L      |       �   /   �      �   =   �   +   5     a  �  i  A   �  1   <  Y   n  +   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GNUTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-09-09 20:19+0200
Last-Translator: Nathan Follens <nthn@unseen.is>
Language-Team: Dutch <vertaling@nl.linux.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
 Fout (%s) bij converteren gegevens voor dochter, laat het vallen. Er is een fout opgetreden bij lezen van kind: %s. GNUTLS niet ingeschakeld, gegevens zullen onversleuteld naar de schijf worden geschreven! Kan tekens niet converteren van %s naar %s. WAARSCHUWING 