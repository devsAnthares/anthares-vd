��          �   %   �      p     q     ~     �     �  	   �     �  $   �  D     <   J     �     �     �     �     �  %   �  '     )   F  '   p  :   �  +   �  I   �  F   I  �   �  *   ^  t   �  i   �  9   h  1  �     �     �     �     	  	   	  %   $	  $   J	  K   o	  W   �	     
     
     #
     @
     \
  )   |
  +   �
  .   �
  4     @   6  0   w  S   �  I   �  �   F  ,   "  �   O  }   �  M   R                                   	                       
                                                                 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-09-12 14:51+0200
Last-Translator: Wouter Bolsterlee <wbolster@gnome.org>
Language-Team: Dutch <vertaling@nl.linux.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A, %B %e CRTC %d kan uitvoer %s niet aansturen CRTC %d ondersteunt rotation=%s niet CRTC %d: uitproberen modus %dx%d@%dHz met uitvoer op %dx%d@%dHz (ronde %d)
 Geen terminal gevonden en teruggevallen op ‘xterm’, ook al werkt dat wellicht niet. Laptop Onbekend RANDR-extensie niet aanwezig Proberen modi voor CRTC %d
 kan niet klonen naar uitvoer %s kon CRTC's niet aan uitvoer toewijzen:
%s kon informatie over CRTC %d niet verkrijgen kon informatie over uitvoer %d niet verkrijgen Kon het bereik van de schermafmetingen niet opvragen kon de schermhulpbronnen niet verkrijgen (CRTCs, outputs, modes) kon de instellingen voor CRTC %d niet doorvoeren geen van de opgeslagen schermconfiguraties kwam overeen met de actieve configuratie geen van de geselecteerde modi waren compatibel met de mogelijke modi:
%s uitvoer %s heeft niet dezelfde parameters als een andere gekloonde uitvoer:
huidige modus = %d, nieuwe modus = %d
huidige coördinaten = (%d, %d), nieuwe coördinaten = (%d, %d)
huidige rotatie = %s, nieuwe rotatie = %s uitvoer %s ondersteunt modus %dx%d@%dHz niet aangevraagde positie/afmeting voor CRTC %d ligt buiten de toegestane limieten: positie=(%d, %d), afmeting=(%d, %d), maximum=(%d, %d) vereiste virtuele afmeting past niet binnen de beschikbare afmeting: aangevraagd=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) onafgehandelde X-fout bij het opvragen van het bereik van de schermafmetingen 