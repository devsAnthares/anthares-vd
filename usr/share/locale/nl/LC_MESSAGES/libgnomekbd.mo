��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  4   �
  
   �
  "   �
     �
     �
  �        �     �  3   �     �  +     =   @     ~     �  ?   �  +   �  p        �     �     �     �       -        G  *   X     �  *   �     �  +   �  %   �     #     ,     B     V  %   g  $   �     �     �     �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2017-03-19 15:43+0000
PO-Revision-Date: 2017-09-11 00:18+0200
Last-Translator: Nathan Follens <nthn@unseen.is>
Language-Team: Dutch <vertaling@vrijschrift.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
 Standaardgroep, toegewezen bij aanmaken van vensters Indicator: Aparte groep per venster bijhouden Toetsenbordindeling Toetsenbordindeling Toetsenbordindeling ‘%s’
Copyright © X.org Foundation en XKeyboardConfig-medewerkers
Zie de metadata van het pakket voor licentievoorwaarden Toetsenbordmodel Toetsenbordopties Exotische, zelden gebruikte layouts en opties laden Extra configuratie-items laden Voorbeelden van toetsenbordindelingen tonen Onthouden/laden van indicatoren tegelijk met indelingsgroepen Secundaire groepen Vlaggen tonen in het applet Vlaggen in het applet tonen om de huidige indeling aan te geven Indelingsnaam tonen in plaats van groepnaam Indelingsnaam tonen in plaats van groepnaam (alleen voor versies van XFree die meerdere indelingen ondersteunen) Toetsenbordvoorbeeld, X-afstand Toetsenbordvoorbeeld, Y-afstand Toetsenbordvoorbeeld, hoogte Toetsenbordvoorbeeld, breedte De achtergrondkleur De achtergrondkleur van de indelingsindicator De letterfamilie De letterfamilie van de indelingsindicator De lettergrootte De lettergrootte van de indelingsindicator De voorgrondkleur De voorgrondkleur van de indelingsindicator Probleem bij laden van afbeelding: %s Onbekend XKB-initialisatiefout toetsenbordindeling toetsenbordmodel indeling ‘%s’ indelingen ‘%s’ model ‘%s’, ‘%s’ en ‘%s’ geen indeling geen opties optie ‘%s’ opties ‘%s’ preferences-desktop-keyboard 