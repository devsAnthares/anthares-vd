��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     U     o  @   �     �     �     �  _     �   k  !   �  Y                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-09-12 10:50+0100
Last-Translator: Hannie Dumoleyn <hannie@ubuntu-nl.org>
Language-Team: Dutch <gnome-nl-list@gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Openbare bestanden van %s Openbare bestanden van %s op %s ‘Persoonlijke bestanden delen’ opstarten indien ingeschakeld Persoonlijke bestanden delen Delen Instellingen voor delen Zet ‘Persoonlijke bestanden delen’ aan om de inhoud van deze map over het netwerk te delen. Wanneer er om wachtwoorden moet worden gevraagd, mogelijke opties: ‘never’ (nooit), ‘on_write’ (bij schrijven), ‘always’ (altijd). Wanneer wachtwoorden vereist zijn share;files;http;network;copy;send;delen;bestanden;netwerk;kopiëren;versturen;verzenden; 