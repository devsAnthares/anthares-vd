��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  J   b  @   �  I   �     8  "   X     {     �  "   �  R   �  "   %               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-29 08:54+0000
Last-Translator: Richard E. van der Luit <nippur@fedoraproject.org>
Language-Team: Dutch (http://www.transifex.com/projects/p/freedesktop/language/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Authenticatie vereist om configuratie van loginscherm te kunnen veranderen Authenticatie vereist om gebruikersgegevens te kunnen veranderen Authenticatie vereist om uw eigen gebruikersgegevens te kunnen veranderen Bewerk configuratie loginscherm Bewerk uw eigen gebruikersgegevens Schakel debugging code in Beheer gebruikersaccounts Toon versie-informatie en sluit af Biedt D-Bus interfaces om gebruikers accountinformatie
te bevragen en te bewerken. Vervang bestaande databasegegevens 