��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  -        D  R   ]     �  1   �     �  8     -   N     |  V   �  |   �     c  $   �  +   �  5   �     
	     	     2	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-01 10:15+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <vertaling@vrijschrift.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: nl
 Zorgt dat de schermbeveiliging netjes afsluit Bezig met controleren... Indien de schermbeveiliging actief is, deactiveer haar (maak het scherm zichtbaar) Onjuist wachtwoord Boodschap om te tonen op het vergrendelingsscherm Voer a.u.b. uw wachtwoord in... Vraag op hoe lang de schermbeveiliging actief is geweest Vraag de toestand van de schermbeveiliging op Gebruiker wisselen Geeft de actieve schermbeveiliging opdracht om het scherm onmiddellijk te vergrendelen De schermbeveiliging is gedurende %d seconde actief geweest.
 De schermbeveiliging is gedurende %d seconden actief geweest.
 De schermbeveiliging is actief
 De schermbeveiliging is niet actief
 De schermbeveiliging is thans niet actief.
 Activeer de schermbeveiliging (maak het scherm zwart) Ontgrendelen Versie van deze toepassing Caps Lock staat aan. 