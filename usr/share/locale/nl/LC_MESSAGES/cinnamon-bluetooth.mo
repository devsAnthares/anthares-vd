��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 k  %     �	     �	     �	  	   �	     �	     �	  7   
     >
     Y
  #   b
  
   �
  *   �
  /   �
     �
      �
       c   9     �     �     �      �     �  ,   �     %  	   (     2     P  C   j  8   �     �  ,   �       $   2     W     n     �     �  E   �                     +     .     E     c     |     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-04-27 09:00+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adres Altijd toegang verlenen Authorisatieverzoek van %s Bluetooth Bluetooth-instellingen Bluetooth is uitgeschakeld Bluetooth is uitgeschakeld door de apparatuurschakelaar Bladeren door bestanden... Afbreken Bluetooth-instellingen configureren Verbinding Apparaat %s wil toegang tot de dienst '%s' Apparaat %s wil zich koppelen aan deze computer Komt niet overeen Fout bij doorzoeken van apparaat Alleen deze keer verlenen Als u dit apparaat verwijdert, zult u het opnieuw moeten instellen om het weer te kunnen gebruiken. Toetsenbordinstellingen Komt overeen Muisinstellingen Muis- en aanraakveldinstellingen Nee Er zijn geen Bluetooth-stekkers aangetroffen OK Gekoppeld Koppelingsbevestiging voor %s Koppelingsverzoek voor %s Bevestig a.u.b. of de pin '%s' overeenkomt met die op het apparaat. Voer a.u.b. de pin in die vermeld staat op het apparaat. Afwijzen '%s' verwijderen uit de lijst van apparaten? Apparaat verwijderen Bestanden versturen naar apparaat... Bestanden verzenden... Nieuw apparaat instellen Een nieuw apparaat instellen... Geluidinstellingen Het verzochte apparaat kan niet worden doorzocht, foutmelding is '%s' Soort Zichtbaarheid Zichtbaarheid van '%s' Ja bezig met verbinden… verbinding wordt verbroken… apparatuur uitgeschakeld bladzijde 1 bladzijde 2 