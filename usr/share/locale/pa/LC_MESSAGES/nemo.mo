��    "      ,  /   <      �  .   �  *   (  1   S      �  ,   �  o   �     C  i   K  !   �     �  2   �     *     6  
   U  "   `     �     �  /   �     �     �     �       2     (   J  
   s  
   ~     �      �  �   �     3     A     O     X  �  ]  \   		  w   f	  t   �	  Y   S
  E   �
  �   �
     �  #  �  I     C   d  h   �  '     ;   9  $   u  a   �  "   �       X   5  _   �  6   �  E   %     k  K   �  P   �     "  #   6     Z  9   ^  }  �          $     9     B                                                   
                                 	                                                                   "   !    

Browse the file system with the file manager --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Actions Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. Bookmark for Nonexistent Location Cannot find the autorun program Create the initial window with the given geometry. Disable all Don't show this message again. Enable all Error starting autorun program: %s FTP (with login) GEOMETRY Nemo could not create the required folder "%s". Nemo's main menu is now hidden No actions found No bookmarks defined None Only create windows for explicitly specified URIs. Perform a quick set of self-check tests. Public FTP Quit Nemo. SSH Show the version of the program. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. WebDAV (HTTP) Windows share [URI...] _Run Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-20 16:12+0000
Last-Translator: Gurdit Singh Bedi <gurditsbedi@gmail.com>
Language-Team: Punjabi <pa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 

ਫਾਇਲ ਮੈਨੇਜਰ ਵਿੱਚ ਫਾਇਲ ਸਿਸਟਮ ਦੀ ਝਲਕ --check ਨੂੰ ਕਿਸੇ ਹੋਰ ਚੋਣ ਨਾਲ ਨਹੀਂ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ। --geometry ਇੱਕ ਤੋਂ ਵਧੇਰੇ URI ਨਾਲ ਨਹੀਂ ਵਰਤੀ ਜਾ ਸਕਦੀ ਹੈ। --quit ਨੂੰ URI ਨਾਲ ਨਹੀਂ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ। <big><b>ਆਟੋ-ਰਨ ਸਾਫਟਵੇਰ ਗਲਤੀ</b></big> <big><b>ਇਸ ਮੀਡਿਅਮ ਵਿੱਚ ਸਾਫਟਵੇਅਰ ਹਨ, ਜੋ ਕਿ ਆਟੋਮੈਟਿਕ ਹੀ ਚੱਲਦੇ ਹਨ। ਕੀ ਤੁਸੀਂ ਚਲਾਉਣਾ ਚਾਹੁੰਦੇ ਹੋ?</b></big> ਕਾਰਵਾਈਆਂ ਨਿਮੋ  ਚਲਾਉਣ ਤੋਂ ਪਹਿਲਾਂ, ਕਿਰਪਾ ਕਰਕੇ ਦਸੇ ਗਏ ਫੋਲਡਰ ਨੂੰ ਬਣਾਓ, ਜਾਂ ਇਜਾਜ਼ਤਾਂ ਨੂੰ ਸੈੱਟ ਕਰੋ ਤਾਂ ਜੋ ਨਿਮੋ ਫੋਲਡਰ ਨੂੰ ਬਣਾਓ ਸਕੇ | ਨਾ-ਮੌਜੂਦ ਟਿਕਾਣੇ ਲਈ ਬੁੱਕਮਾਰਕ ਆਟੋ-ਰਨ ਪਰੋਗਰਾਮ ਨਹੀਂ ਲੱਭਿਆ ਦਿੱਤੀ ਜੁਮੈਟਰੀ ਨਾਲ ਸ਼ੁਰੂਆਤੀ ਵਿੰਡੋ ਬਣਾਓ। ਸਭ ਨੂੰ ਅਯੋਗ ਕਰੋ ਮੁੜ ਇਹ ਸੁਨੇਹਾ ਨਾ ਵੇਖਾਓ| ਸਭ ਨੂੰ ਯੋਗ ਕਰੋ ਆਟੋ-ਰਨ ਪਰੋਗਰਾਮ ਸਟਾਰਟ ਕਰਨ ਦੌਰਾਨ ਗਲਤੀ: %s FTP (ਲਾਗਇਨ ਸਮੇਤ) ਜੁਮੈਟਰੀ ਨਿਮੋ ਲੋੜੀਦਾ ਫੋਲਡਰ "%s" ਨਹੀਂ ਬਣਾ ਸਕੀ। ਨਿਮੋ  ਦਾ ਮੁੱਖ ਮੇਨੂ ਗੁਪਤ ਹੋ ਚੁੱਕਅਿਾ ਹੈ ਕੋਈ ਕਾਰਵਾਈ ਨਹੀਂ ਲੱਭੀ ਕੋਈ ਬੁੱਕਮਾਰਕ ਪ੍ਰਭਾਸ਼ਿਤ ਨਹੀ ਕੁੱਝ ਨਹੀਂ ਦਿੱਤੇ URI ਲਈ ਹੀ ਖਾਸ ਵਿੰਡੋ ਬਣਾਓ। ਇੱਕ ਤੁਰੰਤ ਸਵੈ-ਪੜਤਾਲ ਟੈਸਟ ਚਲਾਓ। ਪਬਲਿਕ FTP ਨਿਮੋ ਬੰਦ ਕਰੋ। SSH ਪਰੋਗਰਾਮ ਦਾ ਵਰਜਨ ਵੇਖੋ। ਸਾਫਟਵੇਅਰ "%s" ਮਾਧਿਅਮ ਤੋਂ ਸਿੱਧਾ ਚੱਲੇਗਾ। ਤੁਹਾਨੂੰ ਉਹ ਸਾਫਟਵੇਅਰ ਨਹੀਂ ਚਲਾਉਣੇ ਚਾਹੀਦੇ, ਜਿੰਨ੍ਹਾਂ ਉੱਤੇ ਤੁਹਾਨੂੰ ਭਰੋਸਾ ਨਹੀਂ ਹੈ।

ਜੇ ਸ਼ੱਕ ਹੋਵੇ ਤਾਂ ਰੱਦ ਕਰੋ ਦੇਵੋ। WebDAV (HTTP) Windows ਸਾਂਝ [URI...] _ਚਲਾਓ 