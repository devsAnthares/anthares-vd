��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9       J     �	     �	     �	     �	     
     
     
     "
     +
  Q   7
  K   �
  �   �
  �   Z       /        J  8   Z  F   �  S   �  Z   .  O   �  `   �  U   :  h   �  Y   �  �   S     �  �  h  X   ;  �   �  �   j  j   =                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-12 07:21+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pa
Plural-Forms: nplurals=2; plural=n != 1;

X-Generator: Lokalize 1.4
 %A %e %B, %R %A, %e %B %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A %e %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ਆਉਟਪੁੱਟ %s ਡਰਾਇਵ ਨਹੀਂ ਕਰ ਸਕਦਾ CRTC %d ਘੁੰਮਾਉਣ=%s ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ CRTC %d: %dx%d@%dHz ਮੋਡ ਨਾਲ %dx%d@%dHz ਉੱਤੇ ਆਉਟਪੁੱਟ ਨਾਲ ਕੋਸ਼ਿਸ਼(ਪਾਸ %d)
 ਟਰਮੀਨਲ ਨਹੀਂ ਲੱਭਿਆ ਜਾ ਸਕਦਾ, xtrerm ਦੀ ਵਰਤੋਂ, ਭਾਵੇਂ ਇਹ ਵੀ ਸ਼ਾਇਦ ਹੀ ਕੰਮ ਕਰੇ ਲੈਪਟਾਪ ਮਿਰਰ ਕੀਤੇ ਡਿਸਪਲੇਅ ਅਣਜਾਣ RANDR ਸਹਿਯੋਗੀ ਮੌਜੂਦ ਨਹੀਂ CRTC %d ਲਈ ਮੋਡ ਨਾਲ ਕੋਸ਼ਿਸ਼ ਜਾਰੀ
 %s ਆਉਟਪੁੱਟ ਕਲੋਨ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕਦੀ CRTCs ਆਉਟਪੁੱਟ ਨੂੰ ਦਿੱਤਾ ਨਹੀਂ ਜਾ ਸਕਿਆ:
%s CRTC %d ਬਾਰੇ ਜਾਣਕਾਰੀ ਨਹੀਂ ਲਈ ਜਾ ਸਕੀ ਆਉਟਪੁੱਟ %d ਬਾਰੇ ਜਾਣਕਾਰੀ ਨਹੀਂ ਲਈ ਜਾ ਸਕੀ ਸਕਰੀਨ ਅਕਾਰ ਦੀ ਸੀਮਾ ਨਹੀਂ ਲਈ ਜਾ ਸਕੀ ਸਕਰੀਨ ਸਰੋਤ ਲਏ ਨਹੀਂ ਜਾ ਸਕੇ (CRTC,ਆਉਟਪੁੱਟ,ਮੋਡ) CRTC %d ਲਈ ਸੰਰਚਨਾ ਸੈੱਟ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ ਕੋਈ ਵੀ ਸੰਭਾਲੀ ਵੇਖਣ ਸੰਰਚਨਾ ਚਾਲੂ ਸੰਰਚਨਾ ਨਾਲ ਮੇਲ ਨਹੀਂ ਖਾਂਦੀ ਚੁਣੇ ਮੋਡ 'ਚੋਂ ਕੋਈ ਵੀ ਸੰਭਵ ਮੋਡ ਨਾਲ ਅਨੁਕੂਲ ਨਹੀਂ ਹੈ:
 %s ਆਉਟਪੁੱਟ %s ਦੇ ਉਹੀ ਪੈਰਾਮੀਟਰ ਨਹੀਂ ਹਨ, ਜੋ ਕਿ ਹੋਰ ਕਲੋਨ ਕੀਤੀ ਆਉਟਪੁੱਟ ਦੇ ਹਨ:
ਮੌਜੂਦਾ ਮੋਡ:  %d, ਨਵਾਂ ਮੋਡ = %d
ਮੌਜੂਦਾ ਕੋਆਰਡੀਨੇਟ = (%d, %d), ਨਵੇਂ ਕੋਆਰਡੀਨੇਟ = (%d, %d)
ਮੌਜੂਦਾ ਘੁੰਮਾਉਣ ਦਿਸ਼ਾ= %s, ਨਵੀਂ ਘੁੰਮਾਉਣ ਦਿਸ਼ਾ = %s ਆਉਟਪੁੱਟ %s %dx%d@%dHz ਮੋਡ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ CRTC %d ਲਈ ਲੋੜੀਦੀ ਸਥਿਤੀ/ਅਕਾਰ ਮਨਜ਼ੂਰਸ਼ੁਦਾ ਸੀਮਾ ਤੋਂ ਬਾਹਰ ਸਥਿਤੀ=(%d, %d), ਆਕਾਰ=(%d, %d), ਵੱਧੋ-ਵੱਧ=(%d, %d) ਲੋੜੀਦਾ ਫ਼ਰਜ਼ੀ ਅਕਾਰ ਉਪਲੱਬਧ ਆਕਾਰ ਲਈ ਫਿੱਟ ਨਹੀਂ: ਲੋੜੀਦਾ=(%d, %d), ਘੱਟੋ-ਘੱਟ=(%d, %d), ਵੱਧੋ-ਵੱਧ=(%d, %d) ਸਕਰੀਨ ਅਕਾਰ ਦੀ ਸੀਮਾ ਲੈਣ ਲਈ ਨਾ-ਸੰਭਾਲੀ X ਗਲਤੀ 