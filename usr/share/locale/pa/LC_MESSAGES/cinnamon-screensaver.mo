��          L      |       �   `   �      
     %  )   B     l  (  �  �   �  4   d  1   �  Q   �  J                                            The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-10-21 00:18+0000
Last-Translator: A S Alam <alam.yellow@gmail.com>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: pa
 ਸਕਰੀਨ-ਸੇਵਰ %d ਸਕਿੰਟ ਤੋਂ ਸਰਗਰਮ ਹੈ।
 ਸਕਰੀਨ-ਸੇਵਰ %d ਸਕਿੰਟਾਂ ਤੋਂ ਸਰਗਰਮ ਹੈ।
 ਸਕਰੀਨ-ਸੇਵਰ ਚਾਲੂ ਹੈ।
 ਸਕਰੀਨ-ਸੇਵਰ ਬੰਦ ਹੈ।
 ਸਕਰੀਨ-ਸੇਵਰ ਹਾਲੇ ਐਕਟਿਵ ਨਹੀਂ ਹੈ।
 ਤੁਹਾਡਾ ਕੈਪਸ (Caps) ਬਟਨ ਦੱਬਿਆ ਹੈ। 