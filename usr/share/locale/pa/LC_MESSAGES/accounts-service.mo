��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  ~   \  h   �  {   D  ?   �  6      &   7  2   ^  J   �  �   �  (   {               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: A S Alam <apreet.alam@gmail.com>
Language-Team: Panjabi (Punjabi) (http://www.transifex.com/projects/p/freedesktop/language/pa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pa
Plural-Forms: nplurals=2; plural=(n != 1);
 ਲਾਗਇਨ ਸਕਰੀਨ ਸੰਰਚਨਾ ਬਦਲਣ ਲਈ ਪਰਮਾਣਿਤ ਹੋਣ ਦੀ ਲੋੜ ਹੈ ਯੂਜ਼ਰ ਡਾਟਾ ਬਦਲਣ ਲਈ ਪਰਮਾਣਿਤ ਹੋਣ ਦੀ ਲੋੜ ਹੈ ਤੁਹਾਡਾ ਯੂਜ਼ਰ ਡਾਟਾ ਬਦਲਣ ਲਈ ਪਰਮਾਣਿਤ ਹੋਣ ਦੀ ਲੋੜ ਹੈ ਲਾਗਇਨ ਸਕਰੀਨ ਸੰਰਚਨਾ ਬਦਲੋ ਆਪਣਾ ਯੂਜ਼ਰ ਡਾਟਾ ਬਦਲੋ ਡੀਬੱਗ ਕੋਡ ਚਾਲੂ ਯੂਜ਼ਰ ਅਕਾਊਂਟ ਪਰਬੰਧ ਵਰਜਨ ਜਾਣਕਾਰੀ ਦਿਉ ਅਤੇ ਬੰਦ ਕਰੋ ਯੂਜ਼ਰ ਅਕਾਊਂਟ ਜਾਣਕਾਰੀ ਲੈਣ ਅਤੇ ਬਦਲਣ ਲਈ ਡੀ-ਬੱਸ ਇੰਟਰਫੇਸ ਦਿੰਦਾ ਹੈ ਮੌਜੂਦਗੀਆਂ ਬਦਲੋ 