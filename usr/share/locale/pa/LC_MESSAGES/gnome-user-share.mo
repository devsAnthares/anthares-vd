��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  2   c  B   �  a   �  )   ;     e  %   r  �   �  �   e  9   �  b   5                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share.HEAD
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-03-06 22:48-0600
Last-Translator: A S Alam <alam.yellow@gmail.com>
Language-Team: Punjabi <punjabi-translation@googlegroups.com>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=2; plural=(n != 1);
 %s ਦੀਆਂ ਪਬਲਿਕ ਫਾਇਲਾਂ %s ਦੀਆਂ %s ਉੱਤੇ ਪਬਲਿਕ ਫਾਇਲਾਂ ਜੇ ਚਾਲੂ ਹੋਵੇ ਤਾਂ ਨਿੱਜੀ ਫਾਇਲ ਸਾਂਝ ਚਲਾਓ ਨਿੱਜੀ ਫਾਇਲ ਸਾਂਝ ਸਾਂਝ ਸਾਂਝ ਸੈਟਿੰਗਾਂ ਇਸ ਫੋਲਡਰ ਦੀ ਸਮੱਗਰੀ ਨੂੰ ਨੈੱਟਵਰਕ ਉੱਤੇ ਸਾਂਝਾ ਕਰਨ ਲਈ ਨਿੱਜੀ ਫਾਇਲ ਸਾਂਝ ਨੂੰ ਚਾਲੂ ਕਰੋ। ਜਦੋਂ ਪਾਸਵਰਡ ਪੁੱਛਿਆ ਜਾਵੇ ਤਾਂ ਸੰਭਵ ਮੁੱਲ ਹਨ "never", "on_write", ਅਤੇ "always"। ਪਾਸਵਰਡ ਕਦੋਂ ਚਾਹੀਦੇ ਹਨ ਸਾਂਝ;ਫਾਇਲਾਂ;ਸ਼ੇਅਰ;http;ਨੈੱਟਵਰਕ;ਕਾਪੀ;ਭੇਜੋ; 