��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %    >  2   C  '   v  "   �  &   �  9   �  @   "  /   c     �  O     /   c     �  �   �  ?   /  ?   o  #   �  #   �  �   �      �  #   �  x     F   �  0   �  -     y   9  %   �  6   �  y     W   �  �   �  3   �  0   �  +   �  .   )  %   X  U   ~     �  L   �     >  L   [  %   �  U   �  <   $  `   a  \   �  F        f  &   v  *   �  #   �      �  )     "   7     Z      w  #   �                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2011-08-23 01:15+0000
PO-Revision-Date: 2011-09-18 00:44+0530
Last-Translator: A S Alam <aalam@users.sf.net>
Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
Language: pa
 ਸਰਗਰਮ ਹੋਰ ਪਲੱਗਇਨਾਂ ਸਰਗਰਮ ਪਲੱਗਇਨ(_p): ਪਲੱਗਇਨ ਸ਼ਾਮਲ ਡਾਈਲਾਗ ਬੰਦ ਕਰੋ ਚੁਣੀ ਪਲੱਗਇਨ ਦੀ ਸੰਰਚਨਾ ਚੁਣੀ ਪਲੱਗਇਨ ਨਾ-ਸਰਗਰਮ ਕਰੋ ਪਲੱਗਇਨ ਤਰਜੀਹ ਘਟਾਓ ਡਿਫਾਲਟ ਗਰੁੱਪ, ਜੋ ਕਿ ਵਿੰਡੋ ਨਿਰਮਾਣ ਲਈ ਦਿੱਤਾ ਗਿਆ ਹੈ। ਇੰਸਟਾਲ ਕੀਤੀਆਂ ਪਲੱਗਇਨ ਚਾਲੂ/ਬੰਦ ਪਲੱਗਇਨ ਤਰਜੀਹ ਵਧਾਓ ਇੰਡੀਕੇਟਰ: ਹਰੇਕ ਵਿੰਡੋ ਲਈ ਵੱਖਰਾ ਗਰੁੱਪ ਰੱਖਦਾ ਅਤੇ ਪਰਬੰਧ ਕਰਦਾ ਹੈ ਕੀ-ਬੋਰਡ ਇੰਡੀਕੇਟਰ ਪਲੱਗਇਨ ਕੀ-ਬੋਰਡ ਇੰਡੀਕੇਟਰ ਪਲੱਗਇਨ ਕੀ-ਬੋਰਡ ਲੇਆਉਟ ਕੀ-ਬੋਰਡ ਲੇਆਉਟ ਕੀਬੋਰਡ ਲੇਆਉਟ "%s"
Copyright &#169; X.Org ਫਾਊਂਡੇਸ਼ਨ ਅਤੇ XKeyboardConfig ਯੋਗਦਾਨੀ
ਲਾਈਸੈਂਸ ਲਈ ਪੈਕੇਜ ਮੇਟਾਡਾਟਾ ਵੇਖੋ ਕੀ-ਬੋਰਡ ਮਾਡਲ ਕੀ-ਬੋਰਡ ਚੋਣਾਂ ਪੁਰਾਣੇ, ਘੱਟ ਵਰਤੇ ਜਾਂਦੇ ਲੇਆਉਟ ਅਤੇ ਚੋਣਾਂ ਲੋਡ ਕਰੋ ਵਾਧੂ ਸੰਰਚਨਾ ਆਈਟਮਾਂ ਲੋਡ ਕਰੋ ਕੋਈ ਵੇਰਵਾ ਨਹੀਂ ਹੈ। ਕੀ-ਬੋਰਡ ਲੇਆਉਟ ਝਲਕ ਲੇਆਉਟ ਗਰੁੱਪਾਂ ਨਾਲ ਇੰਡੀਕੇਟਰ ਸੰਭਾਲੋ/ਰੀ-ਸਟੋਰ ਕਰੋ ਸੈਕੰਡਰੀ ਗਰੁੱਪ ਐਪਲਿਟ ਵਿੱਚ ਝੰਡੇ ਵੇਖੋ ਮੌਜੂਦਾ ਲੇਆਉਟ ਨੂੰ ਵੇਖਾਉਦੇ ਝੰਡੇ ਐਪਲਿਟ ਵਿੱਚ ਵੇਖੋ ਗਰੁੱਪ ਨਾਂ ਦੀ ਬਜਾਏ ਲੇਆਉਟ ਨਾਂ ਵੇਖਾਓ ਗਰੁੱਪ ਨਾਂ ਦੀ ਬਜਾਏ ਲੇਆਉਟ ਨਾਂ ਵੇਖੋ (XFree ਵਲੋਂ ਸਹਾਇਕ ਬਹੁ ਲੇਆਉਟ ਦੇ ਵਰਜਨ ਲਈ ਹੀ) ਕੀ-ਬੋਰਡ ਝਲਕ, X ਆਫਸੈੱਟ ਕੀ-ਬੋਰਡ ਝਲਕ, Y ਆਫਸੈਟ ਕੀ-ਬੋਰਡ ਝਲਕ, ਉਚਾਈ ਕੀ-ਬੋਰਡ ਝਲਕ, ਚੌੜਾਈ ਬੈਕਗਰਾਊਂਡ ਰੰਗ ਲੇਆਉਟ ਇੰਡੀਕੇਟਰ ਲਈ ਬੈਕਗਰਾਊਂਡ ਰੰਗ ਫੋਂਟ ਫੈਮਲੀ ਲੇਆਉਟ ਇੰਡੀਕੇਟਰ ਲਈ ਫੋਂਟ ਫੈਮਲੀ ਫੋਂਟ ਸਾਈਜ਼ ਲੇਆਉਟ ਇੰਡੀਕੇਟਰ ਲਈ ਫੋਂਟ ਸਾਈਜ਼ ਫਾਰਗਰਾਊਂਡ ਰੰਗ ਲੇਆਉਟ ਇੰਡੀਕੇਟਰ ਲਈ ਫਾਰਗਰਾਊਂਡ ਰੰਗ ਸਰਗਰਮ ਪਲੱਗਇਨਾਂ ਦੀ ਲਿਸਟ ਚਾਲੂ ਕੀ-ਬੋਰਡ ਇੰਡੀਕੇਟਰ ਪਲੱਗਇਨ ਦੀ ਲਿਸਟ ਇੱਕ ਚਿੱਤਰ ਲੋਡ ਕਰਨ ਦੌਰਾਨ ਗਲਤੀ ਆਈ ਹੈ: %s ਮੱਦਦ ਫਾਇਲ ਖੋਲ੍ਹਣ ਲਈ ਅਸਮਰੱਥ ਅਣਜਾਣ XKB ਸ਼ੁਰੂਆਤ ਗਲਤੀ ਉਪਲੱਬਧ ਪਲੱਗਇਨ(_A): ਕੀ-ਬੋਰਡ ਲੇਆਉਟ ਕੀ-ਬੋਰਡ ਮਾਡਲ ਲੇਆਉਟ "%s" "%s" ਲੇਆਉਟ ਮਾਡਲ "%s", %s ਅਤੇ %s ਲੇਆਉਟ ਨਹੀਂ ਕੋਈ ਚੋਣ ਨਹੀਂ ਚੋਣ "%s" ਚੋਣਾਂ "%s" 