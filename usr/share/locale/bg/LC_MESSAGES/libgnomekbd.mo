��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  5   �  &   �  (     =   F  A   �  9   �  J      �   K  X   �  J   %     p  T   �  N   �  N   (  .   w  .   �  �   �  (   �  0   �  g   .  G   �     �  (   �  �         �  6   �  ]   �  p   R  �   �  I   �  I   �  >   5  >   t     �  J   �  
     >         _  R   ~     �  K   �  5   5  }   k  N   �  I   8     �  =   �  "   �  .   �  (   %  3   N     �     �     �  7   �                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-25 21:44+0200
PO-Revision-Date: 2011-03-25 21:44+0200
Last-Translator: Alexander Shopov <ash@kambanaria.org>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Задействане на още приставки _Действащи приставки Добавяне на приставка Затваряне на диалоговия прозорец Настройване на избраната приставка Спиране на избраната приставка Понижаване на приоритета на приставката Стандартната група, която се присвоява на прозорец при създаването му Задействане/спиране на инсталираните приставки Повишаване на приоритета на приставката Индикатор: Управление на отделна група за всеки прозорец Приставки за обозначаване на клавиатурата Приставки за обозначаване на клавиатурата Подредба на клавиатурата Подредба на клавиатурата Клавиатурна подредба „%s“
Авторски права &#169; X.Org Foundation и сътрудници на XKeyboardConfig
Вижте мета-данните на пакета за информация относно лиценза Модел на клавиатурата Настройки на клавиатурата Зареждане на по-рядко използваните подредби и настройки Зареждане на допълнителните настройки Без описание. Преглед на подредбите Запазване/възстановяване на индикаторите заедно с групите по подредбата Втори групи Показване на знамена в аплета Показване на знамена в аплета за текущата подредба Показване на имената на подредбите вместо имената на групите Показване на имената на подредбите вместо имената на групите (само при версии на XFree, които поддържат множество подредби) Преглед на клавиатурата, отместване по X Преглед на клавиатурата, отместване по Y Преглед на клавиатурата, височина Преглед на клавиатурата, широчина Цвят на фона Цвят на фона за индикатора на подредбата Шрифт Шрифт за индикатора на подредбата Размер на шрифта Размер на шрифта за индикатора на подредбата Основен цвят Основен цвят за индикатора на подредбата Списък с действащи приставки Списък със задействаните приставки за обозначаване на клавиатурата Грешка при зареждането на изображението: %s Файлът с помощта не може да бъде отворен Непозната Грешка при инициализирането на XKB _Налични приставки подредба на клавиатурата модел на клавиатурата подредба „%s“ подредби „%s“ модел „%s“, %s и %s без подредба без опции настройка „%s“ настройки „%s“ 