��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 l  %  
   �	  5   �	  0   �	  	   
  !   
     0
  B   N
  %   �
     �
  !   �
     �
  D   �
  V   8     �  @   �     �  �   �  0   �     �  &   �  G        b  )   g     �     �  6   �  2   �  h     A   v     �  U   �  .     G   N  )   �  3   �  8   �  "   -  g   P     �     �     �     �     �     
  $   "     G     Z                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-18 06:39+0000
Last-Translator: spacy01 <Unknown>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Адрес Осигуряване на достъп винаги Искане за ауторизация от %s Bluetooth Настройки на Bluetooth Bluetooth е забранен Bluetooth е забранен чрез хардуерен ключ Преглед на файлове… Отмени Настройки на Bluetooth Връзка Устройство %s иска достъп да услуга '%s' Устройството %s иска да се сдвои с този компютър Не съвпада Грешка при достъпа до устройството Само този път Ако премахнете устройството, следващия път ще трябва да го добавите отново, преди да го използвате. Настройки на клавиатурата Съвпада Насторйки на мишката Настройки на мишката и сензорния панел Не Липсват Bluetooth адаптери OK Сдвоени Потвърждение за сдвояване с %s Запитване за сдвояване за %s Моля потвърдете че ПИН "%s" съвпада с този на устройството. Моля въведете кода на устройството. Отхвърли Да се премахне ли „%s“ от списъка с устройства? Премахване на устройство Изпращане на файлове до устройството... Изпращане на файлове... Добавяне на ново устройство Настройка на ново устройство... Настройки на звука Исканото устройство не може да бъде достъпено, грешка '%s' Вид Видимост Видимост на „%s“ Да свързване... прекъсване... Хардуера е изключен страница 1 страница 2 