��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     _  J     �	     �	     �	     �	  	   �	     �	     
     
     
  M   
  C   j
  d   �
  �     !   �  %   �     �  +   �  H     I   b  `   �  j     Z   x  _   �  �   3  l   �     *  f   �  _    ?   q  �   �  �   �  �   :                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-20 06:32+0300
Last-Translator: Alexander Shopov <ash@kambanaria.org>
Language-Team: Bulgarian <dict@fsa-bg.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 %A, %e %B, %R %A, %e %B, %R:%S %A, %e %B, %l:%M %p %A, %e %B, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p Видеокарта %d не може да управлява изхода %s Видеокарта %d не поддържа завъртане=%s Видеокарта %d: проба на режим %dx%d@%dHz с изход %dx%d@%dHz (%d пас)
 Не може да бъде открит терминал. Ще се пробва с xterm, дори и да не сработи Преносим компютър Еднакво изображение Непознат разширението RANDR липсва Изпробване на режимите за видеокарта %d
 повтаряне на изображението към изхода %s на следните изходи не може да се зададе видеокарта:
%s информацията за %d-ата видео карта не може да бъде получена информацията за изхода %d не може да бъде получена диапазонът от размери на екрана не може да се получи ресурсите на екрана не могат да бъдат получени (видео карта, изходи, режими) настройките на %d-ата видео карта не могат да бъдат зададени никоя от запазените конфигурации на дисплея не съвпада с действащата никой от избраните режими не е съвместим с възможните:
%s изходът %s е с различни параметри от този, чието изображение трябва да повтори:
текущ режим = %d, нов режим = %d
текущи координати = (%d, %d), нови координати = (%d, %d)
текущо завъртане = %s, ново завъртане = %s екранът %s не поддържа режима %dx%d@%dHz заявената позиция/размер към %d-ата видео карта е извън позволения диапазон: позиция=(%d, %d), размер=(%d, %d), максимум=(%d, %d) изисканият виртуален размер не пасва на действащия: изискан=(%d, %d), минимален=(%d, %d), максимален=(%d, %d) неприхваната грешка на X при получаването на диапазона от размери на екрана 