��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  _  w  +   �  9     b   =  &   �     �  .   �  �   	  �   �  1   Y  �   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-02-01 09:38+0200
Last-Translator: Alexander Shopov <ash@kambanaria.org>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Публичните файлове на %s Публичните файлове на %s върху %s Стартиране на споделянето на файлове, ако е позволено Споделяне на файлове Споделяне Настройки на споделянето Включване на споделянето, така че съдържанието на тази папка да е достъпно по мрежата. Кога да се пита за пароли. Възможните опции са: „never“ (никога), „on_write“ (при запис), „always“ (винаги). Кога да се изискват пароли? споделяне;файлове;уеб;мрежа;копиране;изпращане;share;files;bluetooth;obex;http;network;copy;send; 