��          �   %   �      `     a     n     ~     �     �  $   �  D   �  <   0     m     t     �     �     �  %   �  '     )   ,  '   V  :   ~  +   �  I   �  F   /  �   v  *   D  t   o  i   �  9   N  l  �     �     	     	     %	  ?   ;	  ?   {	  k   �	  A   '
  	   i
     s
     |
  >   �
     �
  H   �
  &   @  -   g  %   �  B   �  )   �  O   (  Q   x  �   �  -   �  x   �  x   _  ;   �                                                           	                            
                                     %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-03-17 19:15-0500
Last-Translator: Reşat SABIQ <tilde.birlik@gmail.com>
Language-Team: QIRIMTATARCA <tilde-birlik-meydan@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: UTF-8
Language: 
Plural-Forms: nplurals=1; plural=0
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p Katod-Nur Tübü Kontrolcisi (CRTC) %d çıqtını süralmay %s Katod-Nur Tübü Kontrolcisi (CRTC) %d aylanış=%s desteklemey Katod-Nur Tübü Kontrolcisi (CRTC) %d: %dx%d@%dHz tarzı, %dx%d@%dHz çıqtısı ile deñene (keçiş %d)
 Bir terminal tapılalmay, xterm qullanıla, belki çalışmasa da Tizüstü Namalüm RANDR uzantısı mevcut degil Katod-Nur Tübü Kontrolcisi (CRTC) %d içün tarzlar deñene
 %s çıqtısına klonlanalmay Katod-Nur Tübü Kontrolcileri (CRTC) çıqtılarğa tayin etilalmay:
%s CRTC %d haqqında malümat alınamadı %d çıqtısı haqqında malümat alınamadı Ekran ölçüleri menzilini alamadım ekran çoqraqlarını alamadım (CRTC'ler, çıqtılar, kelişler) CRTC %d içün ayarlama tesbit etilamadı saqlanğan kösterim ayarlamalarınıñ hiç biri faal ayarlama ile eşleşmedi saylanğan tarzlarnıñ hiç biri mümkün tarzlar ile qabili telif degil edi:
%s %s çıqtısı, klonlanğan diger bir çıqtı ile aynı parametrelerge sahip 
degildir:\n
mevcut tarz = %d, yañı tarz = %d\n
mevcut koordinatlar = (%d, %d), yañı koordinatlar = (%d, %d)\n
mevcut aylanış = %s, yañı aylanış = %s çıqtı %s, %dx%d@%dHz tarzını desteklemey CRTC %d içün istemlengen mevam/ölçü caiz hadnıñ tışındadır: mevam=(%d, %d), ölçü=(%d, %d), azamiy=(%d, %d) talap etilgen sanal ölçü faydalanılışlı ölçüge sığmay: istengen=(%d, %d), asğariy=(%d, %d), azamiy=(%d, %d) Ekran ölçüleri menzilini alğanda qollanmağan X hatası 