��    a      $  �   ,      8     9     A     I     V     i     z  I   �     �     �     �  	   �     �     	     *	     I	     i	     �	     �	     �	     �	     �	  )   �	     
     $
     9
  	   F
  
   P
     [
     m
      �
  (   �
  .   �
             	        (     /     8     F     Z  
   b     m     ~     �     �     �     �  
   �     �     �     �     �     �     �  /   �  
   $     /     =     K     \     m     s     �  N   �     �     �     
          .     4     <     K  &   d     �     �  +   �  <   �  _     O   z  >   �  !   	  3   +  6   _     �     �     �     �  	   �     �     �     �     �  I   �     2     8     @  �  N     5     >     F     S     Y     b  e   j     �     �     �     �     �       *   ,  )   W  0   �  .   �  0   �          (     >  -   J     x     }     �     �     �     �     �     �  %     /   7     g     }     �     �     �     �     �  	   �     �                    !     )     1  
   9     D     P     \     e     m     {  <   �  
   �     �     �     �     �               (  w   D     �      �      �          +     4     =     N  H   f     �     �  &   �  5     m   9  I   �  <   �  $   .  5   S  /   �     �     �     �  
   �     �                      O   "     r  
   z     �             Z       7   6   P                  .   )           *          8   ]   @   
   U           L      ;   :           1       4   	   V         !      S   0   %      a   Q       R              9   ^         /           =   X       B   2                         (          Y      H   E   +      M      D   "   5       #   &      J       C         ?   T                       W       K   A      '   $   [       _   <      G   \             O       I       N   >   -   F   `   ,          3    %d Mb/s %d x %d %d x %d (%s) %i month %i months %i week %i weeks %i year %i years %s
Run '%s --help' to see a full list of available command line options.
 %s VPN - System Settings Ad-hoc All files Authentication required Available Profiles Available Profiles for Cameras Available Profiles for Displays Available Profiles for Printers Available Profiles for Scanners Available Profiles for Webcams British English Cable unplugged Calibration Cannot remove automatically added profile China Chinese (simplified) Colorspace:  Connected Connecting Connection failed Could not detect displays Could not get screen information Could not save the monitor configuration Create a color profile for the selected device Create virtual device Default Default:  Device Disabled Disconnecting Enable verbose mode English Enterprise Firmware missing France French German Germany Help IP Address IPv4 Address IPv6 Address Infrastructure Layout Less than 1 week Monitor No devices supporting color management detected No profile Not connected Not specified Other profile… Panel to display Proxy Remove Device Select ICC Profile File Select a monitor to change its properties; drag it to rearrange its placement. Select a region Set for all users Show help options Show the overview Spain Spanish Status unknown Status unknown (missing) Stop hotspot and disconnect any users? Supported ICC profiles Test profile:  The device type is not currently supported. The measuring instrument does not support printer profiling. The measuring instrument is not detected. Please check it is turned on and correctly connected. This device does not have a profile suitable for whole-screen color correction. This device has an old profile that may no longer be accurate. This device is not color managed. This device is using manufacturing calibrated data. This is not recommended for untrusted public networks. Unavailable Uncalibrated United States Unknown Unmanaged Unspecified WEP WPA WPA2 Web Proxy Autodiscovery is used when a Configuration URL is not provided. Wired _Import _Stop Hotspot Project-Id-Version: cinnamon-control-center
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-08-26 10:56+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: QIRIMTATARCA (Qırım Türkçesi) <tilde-birlik-meydan@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
Language: 
 %d Mb/sn %d x %d %d x %d (%s) %i ay %i hafta %i yıl %s
Kullanılabilir komut satırı seçeneklerini görmek için '%s --help' komutunu çalıştırın.
 %s VPN - Sistem Ayarları Ad-hoc Tüm dosyalar Sahihlenim kerekli Faydalanışlı Profiller Kameralar içün Faydalanışlı Profiller Ekranlar içün Faydalanışlı Profiller Bastırıcılar içün Faydalanışlı Profiller Tarayıcılar içün Faydalanışlı Profiller Ağ Kameraları içün Faydalanışlı Profiller Britaniya İnglizcesi Kablo takılı değil Kalibrasyon Otomatik olarak eklenen profil kaldırılamaz Çin Çince (basitleştirilgen) Renk fezası:  Bağlanğan Bağlanıla Bağlantı muvaffaqiyetsiz Ekranlar tespit edilemedi Ekran bilgisi alınamadı Ekran yapılandırması kaydedilemedi Seçilen aygıt için bir renk profili oluştur Sanal aygıt oluştur Ögbelgilengen Ögbelgilengen:  Cihaz Ğayrıqabilleştirilgen Bağlantı qoparıla Detay kipini etkinleştir İnglizce Kurumsal Firmware kayıp Fransa Frenkçe Almanca Almanya Yardım İP Adresi IPv4 Adresi IPv6 Adresi Altyapı Tizilim 1 haftadan az Ekran Renk yönetimini destekleyen hiçbir aygıt tespit edilemedi Profil yoq Bağlanmağan Belirtilmemiş Diger profil... Kösterilecek panel Proksi Cihaznı Çetleştir ICC Profil Dosyasını Seç Hasiyetlerini deñiştirmek içün bir ekran saylañız; yerleştirilmesini kene tertiplemek içün onı süyrekleñiz. Bir bölge saylañız Tüm kullanıcılar için ayarla Yardım seçeneklerini köster Üstbaqışnı köster İspanya İspanca Durum bilinmiyor Status bilinmey (eksik) Etkin nokta durdurulup tüm kullanıcıların bağlantısı kesilsin mi? Desteklenen ICC profilleri Sınama profili:  Aygıt türü şu anda desteklenmiyor. Ölçüm cihazı yazıcı profillemeyi desteklemiyor. Ölçüm cihazı algılanamadı. Lütfen açık durumda olduğundan ve doğru bağlandığından emin olunuz. Bu aygıt, tam ekran renk düzeltme için uygun bir profile sahip değil. Bu aygıt artık uygun olmayabilecek eski bir profile sahip. Bu aygıt renk yönetimli değildir. Bu aygıt, fabrika önayarlı verilerini kullanıyor. Bu güvenli olmayan açık ağlarda önerilmez. Faydalanışsız Ayarlanmamış Birleşik Devletler Bilinmegen Yönetilmeyen Belirtilmegen WEP WPA WPA2 Yapılandırma URL'si verilmediğinde vekil sunucu otomatik bulma kullanılır. Kabelli _İthal Et Etkin Noktayı _Durdur 