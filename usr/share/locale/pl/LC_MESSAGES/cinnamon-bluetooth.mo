��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 |  %     �	     �	      �	  	   �	     �	     
  :    
     [
     p
      w
     �
  4   �
  3   �
  
              :  b   R     �     �     �  $   �       $        1  
   4     ?  #   ^  G   �  6   �       !   	     +     =     ]     n     �     �  D   �     �                     #     A  (   R     {     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-09 17:33+0000
Last-Translator: Piotr Strębski <strebski@gmail.com>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adres Stałe zezwolenie na dostęp Prośba o uwierzytelnienie od %s Bluetooth Ustawienia Bluetooth Bluetooth jest wyłączony Bluetooth został wyłączony przełącznikiem sprzętowym Przeglądaj pliki... Anuluj Konfiguracja ustawień Bluetooth Połączenie Urządzenie %s chce uzyskać dostęp do usługi '%s' Urządzenie %s chce sparować się z tym komputerem Nie pasuje Błąd przeglądania urządzenia Zezwól tylko tym razem Usunięcie urządzenia spowoduje konieczność jego ponownej konfiguracji przed kolejnym użyciem. Ustawienia klawiatury Pasuje Ustawienia myszy Ustawienia myszy i panelu dotykowego Nie Nie odnaleziono adapterów Bluetooth OK Powiązane Potwierdzenie parowania dla %s Prośba o sparowanie urządzenia %s Prosimy potwierdzić, że numer PIN '%s' pasuje do tego na urządzeniu. Prosimy wpisać numer PIN wyświetlony na urządzeniu. Odrzuć Usunąć "%s" z listy urządzeń? Usuń urządzenie Wyślij pliki do urządzenia... Wyślij pliki... Ustaw nowe urządzenie Ustaw nowe urządzenie... Ustawienia dźwięku Przeglądanie pożądanego urządzenia jest niemożliwe; błąd '%s' Rodzaj Widoczność Widoczność "%s" Tak nawiązywanie połączenia... rozłączanie... Wyłączono przełącznikiem sprzętowym strona 1 strona 2 