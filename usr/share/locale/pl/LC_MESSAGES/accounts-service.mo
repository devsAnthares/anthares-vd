��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  K   �  >   �  9     $   Q     v     �  #   �  4   �  `      #   a               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-07-22 23:36+0000
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish (http://www.transifex.com/projects/p/freedesktop/language/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Wymagane jest uwierzytelnienie, aby zmienić konfigurację ekranu logowania Wymagane jest uwierzytelnienie, aby zmienić dane użytkownika Wymagane jest uwierzytelnienie, aby zmienić własne dane Zmiana konfiguracji ekranu logowania Zmiana własnych danych Włącza kod debugowania Zarządzanie kontami użytkowników Wyświetla informację o wersji i kończy działanie Dostarcza interfejsy D-Bus do odpytywania i manipulowania
informacjami o kontach użytkowników. Zastępuje istniejące wystąpienie 