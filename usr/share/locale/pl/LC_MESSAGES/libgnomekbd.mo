��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  .   C
  
   r
  I   }
     �
     �
  �   �
     �     �  <   �  -   �       @   -     n     �  D   �  .   �  v     $   �  $   �     �      �  
              >  #   O     s  #   �     �  (   �  0   �          !     6     H  1   Y     �     �  
   �  ,   �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-15 10:36+0000
PO-Revision-Date: 2016-08-21 15:37+0200
Last-Translator: Piotr Drąg <piotdrag@gmail.com>
Language-Team: Polish <community-poland@mozilla.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Domyślna grupa, ustawiana przy tworzeniu okna Wskaźnik: Przechowywanie i zarządzanie grupami, oddzielnymi dla każdego z okien Układ klawiatury Układ klawiatury Układ klawiatury „%s”
Copyright &#169; X.Org Foundation oraz współtwórcy XKeyboardConfig
Warunki licencji znajdują się w metadanych pakietu Model klawiatury Opcje klawiatury Wczytanie egzotycznych, rzadko używanych układów i opcji Wczytanie dodatkowych elementów konfiguracji Podgląd układów klawiatury Zapisywanie/przywracanie wskaźników razem z grupami układów Grupy drugorzędne Wyświetlanie flag w aplecie Wyświetlanie flag w aplecie, w celu wskazania bieżącego układu Wyświetlanie nazw układów zamiast nazw grup Wyświetlanie nazw układów zamiast nazw grup (ma zastosowanie tylko do wersji XFree obsługujących wiele układów) Przesunięcie X podglądu klawiatury Przesunięcie Y podglądu klawiatury Wysokość podglądu klawiatury Szerokość podglądu klawiatury Kolor tła Kolor tła wskaźnika układu Rodzina czcionek Rodzina czcionek wskaźnika układu Rozmiar czcionki Rozmiar czcionki wskaźnika układu Kolor pierwszoplanowy Kolor pierwszoplanowy wskaźnika układu Wystąpił błąd podczas wczytywania obrazu: %s Nieznany Błąd inicjacji XKB układ klawiatury model klawiatury układ „%s” układy „%s” układy „%s” model „%s”, %s i %s brak układu brak opcji opcja „%s” opcje „%s” opcje „%s” preferences-desktop-keyboard 