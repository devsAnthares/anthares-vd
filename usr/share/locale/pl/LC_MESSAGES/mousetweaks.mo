��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  /   c     �  (   �      �  !   �          *     @  "   Q  1   t  '   �     �     �     �     	  '   	  
   F	  "   Q	     t	  (   �	  '   �	     �	  e   �	  +   Y
  0   �
     �
     �
  ,   �
  7   �
     5                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-04 19:27+0200
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish <gnomepl@aviary.pl>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Poedit-Language: Polish
X-Poedit-Country: Poland
 - usługa dostępności myszy środowiska GNOME Styl przycisku Styl przycisków okna typów kliknięć. Geometria okna typów kliknięć Orientacja okna typów kliknięć Styl okna typów kliknięć Podwójne kliknięcie Przeciągnięcie Włączenie klikania spoczynkowego Włączenie symulowanego kliknięcia pomocniczego Wyświetlenie pomocy się nie powiodło Ukrycie okna typów kliknięć Pozioma Kliknięcie przez najechanie Tylko ikony Ignorowanie niewielkich ruchów kursora Orientacja Orientacja okna typów kliknięć. Kliknięcie pomocnicze Ustawienie aktywnego trybu spoczynkowego Kończy działanie programu mousetweaks Pojedyncze kliknięcie Rozmiar i położenie okna typów kliknięć. Format to standardowy ciąg geometrii systemu X Window. Uruchamia program mousetweaks jako usługę Uruchamia program mousetweaks w trybie logowania Tekst i ikony Tylko tekst Opóźnienie przed kliknięciem spoczynkowym Opóźnienie przed symulowanym kliknięciem pomocniczym Pionowa 