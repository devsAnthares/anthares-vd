��    	      d      �       �      �      �      �      �      	  &     1   D     v  �                  (     ,  )   >  4   h  :   �     �                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-10-11 16:11+0200
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish <community-poland@mozilla.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 CD-Audio Blu-ray DVD Telewizja cyfrowa Zamontowanie „%s” się nie powiodło. Brak nośnika w napędzie dla urządzenia „%s”. Proszę sprawdzić, czy płyta znajduje się w napędzie. Video CD 