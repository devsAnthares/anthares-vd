��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r     �     �  @   �     �  /   	     9  -   M  !   {     �  E   �  �   �     �  !   �  +   �     �     
	     	  #   (	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-10 12:47+0000
Last-Translator: Damian Woźniak <fiszet@gmail.com>
Language-Team: Polish <gnomepl@aviary.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: Poland
Language: pl
X-Poedit-Language: Polish
 Wyłącza wygaszacz ekranu Sprawdzam... Jeżeli wygaszacz jest aktywny, to przełącza w stan nieaktywny Nieprawidłowe hasło Wiadomość wyświetlana podczas blokady ekranu Wprowadź hasło... Wyświetla czas aktywności wygaszacza ekranu Wyświetla stan wygaszacza ekranu Przełącz użytkownika Powoduje natychmiastowe zablokowanie ekranu w uruchomionym wygaszaczu Wygaszacz ekranu jest aktywny od %d sekundy.
 Wygaszacz ekranu jest aktywny od %d sekund.
 Wygaszacz ekranu jest aktywny od %d sekund.
 Wygaszacz ekranu jest aktywny
 Wygaszacz ekranu jest nieaktywny
 Wygaszacz ekranu nie jest obecnie aktywny.
 Aktywuje wygaszacz ekranu Odblokuj Wersja tego programu Klawisz Caps Lock jest wciśnięty. 