��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w       %   0  C   V  !   �     �     �  n   �  �   T     �  `   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-08-23 00:50+0200
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish <community-poland@mozilla.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Pliki publiczne użytkownika %s Pliki publiczne użytkownika %s na %s Uruchamia udostępnianie plików osobistych, jeśli jest włączone Udostępnianie plików osobistych Udostępnianie Ustawienia udostępniania Włączenie udostępniania plików osobistych umożliwia udostępnianie zawartości tego katalogu przez sieć. Kiedy pytać o hasła. Możliwe wartości to „never” (nigdy), „on_write” (podczas zapisywania) i „always” (zawsze). Kiedy wymagać haseł udostępnianie;współdzielenie;pliki;plików;http;sieć;sieci;skopiuj;kopia;wyślij;wysyłanie; 