��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     
     
     (
     ;
  	   Q
     [
     ^
     d
     m
  (   y
  "   �
  V   �
  W        t     {     �  $   �      �  $   �  +     (   /  ,   X  -   �  <   �  ,   �  R     C   p  �   �  +   �  �   �  y   X  F   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-03 21:40+0200
Last-Translator: Piotr Drąg <piotrdrag@gmail.com>
Language-Team: Polish <gnomepl@aviary.pl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-Country: Poland
X-Poedit-Language: Polish
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A, %e %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d nie może prowadzić wyjścia %s CRTC %d nie obsługuje rotation=%s CRTC %d: próbowanie trybu %dx%d@%dHz za pomocą wyjścia w %dx%d@%dHz (przeszło %d)
 Nie można odnaleźć terminala, używanie xterm, nawet jeśli może to nie zadziałać Laptop Powielenie ekranów Nieznany rozszerzenie RandR jest niedostępne Próbowanie trybów dla CRTC %d
 nie można sklonować do wyjścia %s nie można przydzielić CRTC do wyjść:
%s nie można uzyskać informacji o CRTC %d nie można uzyskać informacji o wyjściu %d nie można uzyskać zakresu rozmiarów ekranu nie można uzyskać zasobów ekranu (CTRC, wyjść, trybów) nie można ustawić konfiguracji dla CRTC %d żadna z zapisanych konfiguracji wyświetlania nie pasuje do aktywnej konfiguracji żaden z wybranych trybów nie jest zgodny z możliwymi trybami:
%s wyjście %s nie posiada takich samych parametrów, co inne sklonowane wyjście:
istniejący tryb = %d, nowy tryb = %d
istniejące współrzędne = (%d, %d), nowe współrzędne = (%d, %d)
istniejący obrót = %s, nowy obrót = %s wyjście %s nie obsługuje trybu %dx%d@%dHz żądane położenie/rozmiar dla CRTC %d znajduje się poza dozwolonym ograniczeniem: położenie=(%d, %d), rozmiar=(%d, %d), maksymalny=(%d, %d) wymagany rozmiar wirtualny nie pasuje do dostępnego rozmiaru: żądany=(%d, %d), minimalny=(%d, %d), maksymalny=(%d, %d) nieobsługiwany błąd X podczas uzyskiwania zakresu rozmiarów ekranu 