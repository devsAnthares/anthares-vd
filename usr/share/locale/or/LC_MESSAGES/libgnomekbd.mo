��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  R   3  ;   �  /   �  5   �  N   (  T   w  Z   �  �   '  �   �  ]   W     �  �   �  6   J  6   �  ,   �  ,   �      &     )   E  �   o  p   $  ;   �  d   �  �   6  %   �  w     �   �  �   i  ]    V   r  S   �  L     F   j  %   �  a   �  +   9  g   e  %   �  a   �  .   U  j   �  N   �  �   >  �   �  K   W     �  5   �  +   �  ,     &   ?  5   f  %   �  5   �  2   �  /   +                     !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master.or
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2011-03-07 16:21+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms:  nplurals=2; plural=(n!=1);




X-Generator: Lokalize 1.1
 ଅଧିକ ପ୍ଲଗଇନ ମାନଙ୍କୁ ଯୋଗ କରନ୍ତୁ ସକ୍ରିୟ ପ୍ଲଗଇନ ଗୁଡିକ (_p): ପ୍ଲଗଇନ ଯୋଗ କରନ୍ତୁ ସଂଳାପକୁ ବନ୍ଦ କରନ୍ତୁ ଚୟିତ ପ୍ଲଗଇନକୁ ବିନ୍ୟାସ କରନ୍ତୁ ଚୟିତ ପ୍ଲଗଇନକୁ ନିଷ୍କ୍ରିୟ କରନ୍ତୁ ପ୍ଲଗଇନ ଅଗ୍ରାଧିକାରକୁ ହ୍ରାସ କରନ୍ତୁ ୱିଣ୍ଡୋ ପ୍ରସ୍ତୁତି ସମୟରେ ନ୍ଯସ୍ତ କରାଯାଇଥିବା ପୂର୍ବନିର୍ଦ୍ଧାରିତ ସମୂହ ସ୍ଥାପିତ ପ୍ଲଗଇନ ମାନଙ୍କୁ ସକ୍ରିୟ/ନିଷ୍କ୍ରିୟ କରନ୍ତୁ ପ୍ଲଗଇନ ଅଗ୍ରାଧିକାରକୁ ବୃଦ୍ଧି କରନ୍ତୁ ସୂଚକ:  ୱିଣ୍ଡୋ ପ୍ରତି ଅଲଗା ସମୂହ ରଖନ୍ତୁ ଏବଂ ପରାଚାଳନା କରନ୍ତୁ କି-ବୋର୍ଡ ସୂଚକ ପ୍ଲଗଇନ କି-ବୋର୍ଡ ସୂଚକ ପ୍ଲଗଇନ କି-ବୋର୍ଡ ବିନ୍ଯାସ କି-ବୋର୍ଡ ବିନ୍ଯାସ କିବୋର୍ଡ ସଂରଚନା "%s"
Copyright &#169; X.Org ପ୍ରତିଷ୍ଠାନ ଏବଂ XKeyboardConfig ସହଯୋଗୀମାନେ
ଅନୁମତି ପତ୍ର ପାଇଁ ପ୍ୟାକେଜ ଅଧିତଥ୍ୟ ଦେଖନ୍ତୁ କି-ବୋର୍ଡ ନମୁନା କି-ବୋର୍ଡ ବିକଳ୍ପ ଆକର୍ଷକ, ଅତିକମ ବ୍ୟବହାର ହେଉଥିବା ସଂରଚନା ଏବଂ ବିକଳ୍ପଗୁଡ଼ିକୁ ଧାରଣ କରନ୍ତୁ ଅତିରିକ୍ତ ସଂରଚନା ବସ୍ତୁଗୁଡ଼ିକୁ ଧାରଣ କରନ୍ତୁ କୌଣସି ବର୍ଣ୍ଣନା ନାହିଁ। କି-ବୋର୍ଡ ବିନ୍ଯାସକୁ ପ୍ରାକଦର୍ଶନ କରନ୍ତୁ ବିନ୍ଯାସ ସମୂହ ମାନଙ୍କ ସହିତ ସୂଚକ ମାନଙ୍କୁ ଏକତ୍ର ସଂରକ୍ଷଣ/ପୁନଃସ୍ଥାପନ କରନ୍ତୁ ଦ୍ବିତୀୟକ ସମୂହ ଆପ୍ଲେଟରେ ବିଶେଷ ସୂଚକ ମାନଙ୍କୁ ପ୍ରଦର୍ଶନ କରନ୍ତୁ ପ୍ରଚଳିତ ବିନ୍ଯାସକୁ ସୂଚୀତ କରିବା ପାଇଁ ଆପ୍ଲେଟରେ ବିଶେଷ ସୂଚକ ମାନଙ୍କୁ ପ୍ରଦର୍ଶନ କରନ୍ତୁ ସମୂହ ନାମ ମାନଙ୍କ ପରିବର୍ତ୍ତେ ବିନ୍ଯାସ ନାମ ମାନଙ୍କୁ ପ୍ରଦର୍ଶନ କରନ୍ତୁ ସମୂହ ନାମ ମାନଙ୍କ ପରିବର୍ତ୍ତେ ବିନ୍ଯାସ ନାମ ମାନଙ୍କୁ ପ୍ରଦର୍ଶନ କରନ୍ତୁ (କେବଳ ଏକାଧିକ ବିନ୍ଯାସ ମାନଙ୍କୁ ସମର୍ଥନ କରୁଥିବା XFree ସଂସ୍କରଣ ମାନଙ୍କ ପାଇଁ) କି-ବୋର୍ଡ ପୂର୍ବାବଲୋକନ, ଏକ୍ସ ଅଫସେଟ କି-ବୋର୍ଡ ପୂର୍ବାବଲୋକନ, ୱାଇ ଅଫସେଟ କି-ବୋର୍ଡ ପୂର୍ବାବଲୋକନ, ଉଚ୍ଚତା କି-ବୋର୍ଡ ପୂର୍ବାବଲୋକନ, ଓସାର ପୃଷ୍ଠଭାଗ ରଙ୍ଗ ବିନ୍ୟାସକାରୀ ସୂଚକ ପାଇଁ ପୃଷ୍ଠଭାଗ ରଙ୍ଗ ଅକ୍ଷରରୂପ ଶ୍ରେଣୀ ବିନ୍ୟାସକାରୀ ସୂଚକ ପାଇଁ ଅକ୍ଷରରୂପ ଶ୍ରେଣୀ ଅକ୍ଷରରୂପ ଆକାର ବିନ୍ୟାସକାରୀ ସୂଚକ ପାଇଁ ଅକ୍ଷରରୂପ ଆକାର ପ୍ରଚ୍ଛଦଭୂମି ରଙ୍ଗ ବିନ୍ୟାସକାରୀ ସୂଚକ ପାଇଁ ପ୍ରଚ୍ଛଦଭୂମି ରଙ୍ଗ ସକ୍ରିୟ ପ୍ଲଗଇନ ମାନଙ୍କର ତାଲିକା ସକ୍ରିୟ କରାଯାଇଥିବା କି-ବୋର୍ଡ ସୂଚକ ପ୍ଲଗଇନ ମାନଙ୍କର ତାଲିକା ଗୋଟିଏ ପ୍ରତିଛବି ଧାରଣ କରିବା ସମୟରେ ଗୋଟିଏ ତୃଟି ଘଟିଲା: %s ସହାୟତା ଫାଇଲ ଖୋଲିବାରେ ଅସମର୍ଥ ଅଜଣା XKB ପ୍ରାରମ୍ଭିକରଣ ତୃଟି ଉପଲବ୍ଧ ପ୍ଲଗଇନ (_A): କି-ବୋର୍ଡ ବିନ୍ଯାସ କି-ବୋର୍ଡ ନମୁନା "%s" ବିନ୍ଯାସ "%s" ବିନ୍ଯାସ "%s", %s ଏବଂ %s ନମୁନା କୌଣସି ବିନ୍ଯାସ ନାହିଁ କୌଣସି ବିକଳ୍ପ ନାହିଁ "%s" ବିକଳ୍ପ "%s" ବିକଳ୍ପ 