��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  h        �	     �	     �	     �	     �	     �	     �	     �	  M   �	  V   ;
  �   �
  �   7     �  =        I  G   V  \   �  U   �  e   Q  W   �  U     I   e  �   �  `   [  �   �  �   �  �  _  ]   >  �   �  �   h  �   Z             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: or
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-11-20 13:12+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: or
Plural-Forms: nplurals=2; plural=(n!=1);


X-Generator: Lokalize 1.5
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ଫଳାଫଳ %s କୁ ଚଲାଇପାରିବ ନାହିଁ CRTC %d ଆବର୍ତ୍ତନ=%s କୁ ସମର୍ଥନ କରେନାହିଁ CRTC %d: %dx%d@%dHz ଧାରାକୁ ଫଳାଫଳ ସହିତ %dx%d@%dHz ରେ ଚେଷ୍ଟାକରୁଅଛି (%d କୁ ପାସ କରନ୍ତୁ)
 କୌଣସି ଟର୍ମିନାଲ ମିଳିଲା ନାହିଁ, xterm ବ୍ଯବହାର କରାଯାଉଛି, ଏହା କାର୍ଯ୍ଯ ନ କରି ପାରେ ଲ୍ୟାପଟପ ପ୍ରତିବିମ୍ବୀତ ପ୍ରଦର୍ଶନ ଅଜଣା RANDR ଅନୁଲଗ୍ନଟି ଉପସ୍ଥିତ ନାହିଁ CRTC %d ପାଇଁ ଧାରାଗୁଡ଼ିକୁ ଚେଷ୍ଟାକରୁଅଛି
 ଫଳାଫଳ %s କୁ କ୍ଲୋନ କରିପାରିବେ ନାହିଁ ଫଳାଫଳରେ CRTCs କୁ ନ୍ୟସ୍ତ କରିପାରିଲା ନାହିଁ:
%s CRTC %d ବିଷୟରେ ସୂଚନା ପାଇ ପାରିଲା ନାହିଁ ଫଳାଫଳ %d ବିଷୟରେ ସୂଚନା ପାଇଲା ନାହିଁ ପରଦା ଆକାରର ସୀମା ପାଇଲା ନାହିଁ ପରଦା ଉତ୍ସଗୁଡ଼ିକୁ ପାଇଲା ନାହିଁ (CRTC ଗୁଡ଼ିକ, ଫଳାଫଳଗୁଡ଼ିକ, ଧାରାଗୁଡ଼ିକ) CRTC %d ପାଇଁ ବିନ୍ୟାସ ସେଟ କରିପାରିଲା ନାହିଁ ସଂରକ୍ଷିତ ପ୍ରଦର୍ଶନ ବିନ୍ୟାସଗୁଡ଼ିକ ମଧ୍ଯରୁ କୌଣସିଟି ସକ୍ରିୟ ବିନ୍ୟାସ ସହିତ ମେଳ ଖାଉନାହିଁ ବଚ୍ଛିତ ଧାରାଗୁଡ଼ିକ ମଧ୍ଯରୁ କୌଣସିଟି ସମ୍ଭାବ୍ୟ ଧାରାଗୁଡ଼ିକ ସହିତ ସୁସଙ୍ଗତ ନୁହଁ:
%s ଫଳାଫଳ %s ରେ ଅନ୍ୟ ପ୍ରକାରର କ୍ଲୋନ ହୋଇଥିବା ଫଳାଫଳ ସମାନ ପ୍ରାଚଳ ନାହିଁ:
ସ୍ଥିତବାନ ଧାରା = %d, ନୂତନ ଧାରା = %d
ସ୍ଥିତବାନ ନିର୍ଦ୍ଦେଶାଙ୍କ = (%d, %d), ନୂତନ ନିର୍ଦ୍ଦେଶାଙ୍କ = (%d, %d)
ସ୍ଥିତବାନ ଆବର୍ତ୍ତନ = %s, ନୂତନ ଆବର୍ତ୍ତନ = %s ଫଳାଫଳ %s ଧାରା %dx%d@%dHz କୁ ସମର୍ଥନ କରେନାହିଁ CRTC %d ପାଇଁ ଅନୁରୋଧିତ ସ୍ଥାନ/ଆକାର ସୀମା ବାହାରେ ଅଛି: ସ୍ଥିତି=(%d, %d), ଆକାର=(%d, %d), ସର୍ବାଧିକ=(%d, %d) ଆବଶ୍ୟକୀୟ ଆଭାସୀ ଆକାର ଉପଲବ୍ଧ ଆକାର ସହିତ ମେଳ ହେଉନାହିଁ: ଅନୁରୋଧିତ=(%d, %d), ସର୍ବନିମ୍ନ=(%d, %d), ସର୍ବାଧିକ=(%d, %d) ପରଦା ଆକାରର ସୀମା ପାଇବା ସମୟରେ ଅନିୟନ୍ତ୍ରିତ X ତ୍ରୁଟି 