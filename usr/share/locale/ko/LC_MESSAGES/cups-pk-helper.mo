��          �      �           	     !     A      [     |     �     �     �  3   �  ;     5   S  <   �  w   �  @   >  9     3   �  K   �  5   9  I   o     �  /   �        �  $     �  %   �     	  %   (     N      f  +   �  '   �  E   �  L   !	  E   n	  L   �	  z   
  U   |
  O   �
  K   "  m   n  F   �  Z   #     ~  <   �  '   �                                     
                                                   	                Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2015-01-17 11:53+0000
Last-Translator: Shinjo Park <kde@peremen.name>
Language-Team: Korean (http://www.transifex.com/freedesktop/cups-pk-helper/language/ko/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ko
Plural-Forms: nplurals=1; plural=0;
 클래스 추가/삭제/편집 지역 프린터 추가/삭제/편집 프린터 추가/삭제/편집 원격 프린터 추가/삭제/편집 프린터 설정 변경 프린터 활성화/비활성화 사용 가능한 장치 목록 가져오기 서버 설정 가져오기/설정하기 클래스를 추가/삭제/편집하려면 권한이 필요합니다. 지역 프린터를 추가/삭제/편집하려면 권한이 필요합니다. 프린터를 추가/삭제/편집하려면 권한이 필요합니다. 원격 프린터를 추가/제거/편집하려면 권한이 필요합니다. 프린터 설정을 변경하려면 권한이 필요합니다. 프린터 시스템 설정 패널에서만 필요합니다. 프린터나 클래스를 활성화/비활성화 하려면 권한이 필요합니다. 사용 가능한 장치의 목록을 가져오려면 권한이 필요합니다. 서버 설정을 가져오거나 설정하려면 권한이 필요합니다. 다른 사용자가 소유하고 있는 작업을 재시작/취소/편집하려면 권한이 필요합니다. 작엽을 재시작/취소/편집 하려면 권한이 필요합니다. 프린터나 클래스를 기본 프린터로 설정하려면 권한이 필요합니다ㅗ 작업 재시작/취소/편집 다른 사용자가 소유한 작업 재시작/취소/편집 프린터를 기본 프린터로 설정 