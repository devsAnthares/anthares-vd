ή    !      $  /   ,      θ     ι     φ            	   /     9     <     B     K     W  $   v  D     <   ΰ          $     6     M     l       %      '   Ζ  )   ξ  '     :   @  +   {  I   §  F   ρ  Ν   8  *     t   1  i   ¦  9     U  J      	     ²	     Η	     ί	     ϊ	     
     	
     
     
  /   $
  4   T
  F   
  O   Π
           4     E  #   T  #   x  '     0   Δ  :   υ  <   0  6   m  C   €  (   θ  ^     Z   p  ½   Λ  ;        Ε  |   O  d   Μ                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-09 05:47+0900
Last-Translator: Changwoo Ryu <cwryu@debian.org>
Language-Team: GNOME Korea <gnome-kr@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=1; plural=0;
 %B %eμΌ (%A), %R %B %eμΌ (%A), %R:%S %B %eμΌ (%A), %p %l:%M %B %eμΌ (%A), %p %l:%M:%S %B %eμΌ %A %R %R:%S %p %l:%M %p %l:%M:%S CRTC %dλ²μ %s μΆλ ₯μ ν  μ μμ΅λλ€ CRTC %dλ²μ %s νμ μ μ§μνμ§ μμ΅λλ€ CRTC %d: %dx%d@%dHz λͺ¨λλ₯Ό %dx%d@%dHz μΆλ ₯μ μλ (ν¨μ€ %d)
 ν°λ―Έλμ΄ μμΌλ―λ‘, xtermμ (λμνμ§ μλλΌλ) μ¬μ©ν©λλ€ λΈνΈλΆ μ»΄ν¨ν° λμΌν νλ©΄ μ μ μμ RANDR νμ₯ κΈ°λ₯μ΄ μμ΅λλ€ CRTC %dλ²μ νλ©΄ λͺ¨λ μλ
 %s μΆλ ₯μ λ³΅μ¬ν  μ μμ΅λλ€ CRTCλ₯Ό μΆλ ₯μ ν λΉν  μ μμ΅λλ€:
%s CRTC %dλ²μ λν μ λ³΄λ₯Ό μ½μ΄ μ¬ μ μμ΅λλ€ μΆλ ₯ %dλ²μ λν μ λ³΄λ₯Ό μ½μ΄ μ¬ μ μμ΅λλ€ νλ©΄ ν¬κΈ°μ λͺ©λ‘μ μ½μ΄ μ¬ μ μμ΅λλ€ νλ©΄ μ λ³΄λ₯Ό μ½μ΄ μ¬ μ μμ΅λλ€ (CRTC, μΆλ ₯, λͺ¨λ) CRTC %dλ²μ μ€μ ν  μ μμ΅λλ€ μ μ₯ν νλ©΄ μ€μ μ μ¬μ© κ°λ₯ν μ€μ  μ€μ μΌμΉνλ μ€μ μ΄ μμ΅λλ€. μ νν λͺ¨λ μ€μ μ΄λ λͺ¨λλ κ°λ₯ν λͺ¨λμ νΈνλμ§ μμ΅λλ€:
%s %s μΆλ ₯μ λ³΅μ¬ν μΆλ ₯κ³Ό κ°μ νλΌλ―Έν°κ° μμ΅λλ€:
νμ¬ λͺ¨λ = %d, μ λͺ¨λ = %d
νμ¬ μ’ν = (%d, %d), μ μ’ν = (%d, %d)
νμ¬ νμ  = %s, μ νμ  = %s %s μΆλ ₯μ %dx%d@%dHz λͺ¨λλ₯Ό μ§μνμ§ μμ΅λλ€ CRTC %dλ²μ μμ²­ν μμΉ/ν¬κΈ°κ° νμ©νλ ν¬κΈ° μ νμ λμ΄κ°λλ€. μμΉ=(%d, %d), ν¬κΈ°=(%d, %d), μ΅λ=(%d, %d) νμν κ°μ νλ©΄ ν¬κΈ°κ° κ°λ₯ν ν¬κΈ°μ λ§μ§ μμ΅λλ€. μμ²­=(%d, %d), μ΅μ=(%d, %d), μ΅λ=(%d, %d) μ€ν¬λ¦° ν¬κΈ°μ λ²μλ₯Ό κ°μ Έμ€λ λ° μ²λ¦¬νμ§ λͺ»νλ X μ€λ₯κ° λ°μνμ΅λλ€ 