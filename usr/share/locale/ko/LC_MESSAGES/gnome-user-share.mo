��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w       $   $  5   I          �     �  d   �  V        e  Y   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-06 02:45+0900
Last-Translator: Changwoo Ryu <cwryu@debian.org>
Language-Team: GNOME Korea <gnome-kr@googlegroups.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s의 공용 파일 %2$s에 있는 %1$s의 공용 파일 사용자 공유를(사용할 경우) 시작합니다 개인 파일 공유 공유 공유 설정 네트워크를 통해 이 폴더의 내용을 공유하려면 개인 파일 공유를 켜십시오. 언제 암호를 물어볼 지 지정. 가능한 값은 "never", "on_write", "always". 암호가 필요한 경우 share;공유;files;파일;http;network;네트워크;인터넷;copy;복사;send;보내기; 