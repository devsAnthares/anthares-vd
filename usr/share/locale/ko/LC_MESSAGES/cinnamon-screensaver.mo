��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  0   �     �  T   �     4  !   Q  "   s  \   �  -   �     !  Y   2  6   �  .   �  6   �  =   )	  0   g	     �	      �	  (   �	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-11-11 15:48+0000
Last-Translator: Jung-Kyu Park <bagjunggyu@gmail.com>
Language-Team: GNOME Korea <gnome-kr@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: Korean
 화면 보호기를 정상적으로 끝냅니다 확인하고 있습니다... 화면 보호기가 실행 중이면 화면보호기를 끕니다 (화면 채우기) 비밀번호와 다릅니다 잠금 화면에 보일 메시지 비밀번호를 입력하세요... 화면 보호기가 얼마나 오랜 시간 동안 작동하고 있었는지 알아봅니다 화면 보호기의 상태를 알아봅니다 사용자 전환 실행 중인 화면 보호기 프로세스에게 화면을 바로 잠그도록 합니다 화면 보호기가 %d 초 동안 작동했습니다.
 화면 보호기가 작동하고 있습니다
 화면 보호기가 작동하고 있지  않습니다
 현재 화면 보호기는 작동하고 있지 않습니다.
 화면 보호기를 켭니다 (화면 비우기) 잠금 해제 이 애플리케이션의 버전 Caps Lock 키가 눌려져 있습니다. 