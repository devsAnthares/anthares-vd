��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >          %     D     X     j  '   �  $   �  '   �  $   �  $   "     G  /   O          �     �     �  {   �     Y     j  O   {  $   �     �  $   �  @   $     e     s  B   �  9   �  �     )   �  )   �     �       
   9  "   D     g  %   u     �  %   �  
   �  )   �        2   %  >   X  (   �     �     �  "   �               )     5     L     Z     h                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-27 17:42+0900
PO-Revision-Date: 2011-02-27 17:43+0900
Last-Translator: Changwoo Ryu <cwryu@debian.org>
Language-Team: GNOME Korea <gnome-kr@googlegroups.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 다른 플러그인 활성화 활성화한 플러그인(_P): 플러그인 추가 대화 창 닫기 선택한 플러그인 설정 선택한 플러그인 활성화 해제 플러그인 우선 순위 낮추기 기본 그룹, 창을 만들 때 할당 설치한 플러그인 사용/해제 플러그인 우선 순위 높이기 알림: 창마다 별도의 그룹을 유지 및 관리 키보드 알림 플러그인 키보드 알림 플러그인 키보드 배치 키보드 배치 키보드 배치 "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata 키보드 모델 키보드 옵션 거의 사용하지 않는 희한한 키보드 배치와 옵션 읽어들이기 추가 설정 항목 읽어들이기 설명 없음. 키보드 배치를 미리 봅니다 알림 기능을 키보드 배치 그룹과 같이 저장/복구 다른 그룹 애플릿에 깃발 표시 애플릿에 현재 키보드 배치를 나타내는 깃발 표시 그룹 이름이 아니라 키보드 배치 이름 표시 그룹 이름 대신에 키보드 배치 이름을 표시 (키보드 배치 여러 개 동시 사용을 지원하는 XFree 버전의 경우에만 해당) 키보드 미리 보기, 가로 오프셋 키보드 미리 보기, 세로 오프셋 키보드 미리 보기, 높이 키보드 미리 보기, 너비 배경 색 키보드 배치 창의 배경 색 글꼴 계열 키보드 배치 창의 글꼴 계열 글꼴 크기 키보드 배치 창의 글꼴 크기 전경 색 키보드 배치 창의 글꼴 전경 색 활성화한 플러그인 목록 사용 중인 키보드 알림 플러그인 목록 그림을 읽어들이는 데 오류가 발생했습니다: %s 도움말 파일을 열 수 없습니다 알 수 없음 XKB 초기화 오류 사용 가능한 플러그인(_A): 키보드 배치 키보드 모델 배치 "%s" 모델 "%s", %s 및 %s 배치 없음 옵션 없음 옵션 "%s" 