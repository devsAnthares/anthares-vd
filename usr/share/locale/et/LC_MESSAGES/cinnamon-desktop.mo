��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     J  J     �	     �	     �	     �	  
   �	     �	     �	     �	      
  &   
  !   3
  J   U
  \   �
     �
     	          '     D  '   c  3   �  +   �  0   �  0     H   M  /   �  I   �  4     �   E  (   &  v   O  o   �  ?   6                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-23 09:38+0300
Last-Translator: Mattias Põldaru <mahfiaz@gmail.com>
Language-Team: Estonian <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n!=1);
 %A, %e. %B, %R %A, %e. %B, %R:%S %A, %e. %B, %l:%M %p %A, %e. %B, %l:%M:%S %p %A, %e. %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ei suuda juhtida väljundit %s CRTC %d ei toeta pöördenurka=%s CRTC %d: režiimi %dx%d@%dHz proovimine väljundiga %dx%d@%dHz (käik %d)
 Terminali pole võimalik leida, kasutatakse xtermi, isegi juhul, kui see ei pruugi töötada Sülearvuti Kuvad on peegeldatud Tundmatu RANRD-laiendus pole saadaval CRTC %d režiimide proovimine
 pole võimalik väljundisse %s kloonida CRTC-sid pole võimalik väljunditele määrata:
%s CRTC %d kohta pole võimalik teavet hankida väljundi %d kohta pole võimalik teavet hankida ekraanisuuruste vahemikku pole võimalik hankida ekraani ressursse (CRTC-d, väljundeid, režiime) pole võimalik hankida CRTC %d jaoks pole võimalik sätteid määrata salvestatud kuvasätete hulgast ei sobi aktiivsete sätetega mitte ükski ükski valitud režiimidest ei sobi võimalikega:
%s väljundil %s ei ole samad parameetrid nagu teisel kloonitud väljundil:
praegune režiim = %d, uus režiim = %d
praegused koordinaadid = (%d, %d), uued koordinaadid = (%d, %d)
praegune pöördenurk = %s, uus pöördenurk %s väljund %s ei toeta režiimi %dx%d@%dHz CRTC %d jaoks määratud asukoht või suurus ületab lubatud piire: asukoht=(%d, %d), suurus=(%d, %d), suurim=(%d, %d) vajalik virtuaalsuurus ei sobi saadaoleva suurusega: küsitud suurus=(%d, %d), vähim=(%d, %d), suurim=(%d, %d) X-serveri käsitlematu viga ekraanisuuruste vahemiku hankimisel 