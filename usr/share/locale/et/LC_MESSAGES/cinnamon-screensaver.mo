��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  3   �     �  U   �        !   ,     N  4   j     �     �  R   �  `   "     �     �  )   �  9   �     !	     4	  (   M	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver MASTER
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-02-26 07:27+0000
Last-Translator: Ivar Smolin <okul@linux.ee>
Language-Team: Estonian <gnome-et@linux.ee>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: et
 Ekraanisäästjat töö lõpetamine ilma veakoodita Kontrollimine... Aktiivse ekraanisäästja väljalülitamine (ekraani tühjendamise tagasipööramine) Vale parool Lukustatud ekraanil kuvatav teade Palun sisesta oma parool... Päring, kui kaua on ekraanisäästja olnud aktiivne Ekraanisäästja oleku päring Vaheta kasutajat Töötavale ekraanisäästja protsessile kohese ekraani lukustamise käsu saatmine Ekraanisäästja on olnud aktiivne %d sekundi.
 Ekraanisäästja on olnud aktiivne %d sekundit.
 Ekraanisäästja on aktiivne
 Ekraanisäästja on jõude
 Ekraanisäästja ei ole hetkel aktiivne.
 Ekraanisäästja sisselülitamine (ekraani tühjendamine) Võta lukust lahti Selle rakenduse versioon Sul on Caps Lock klahv sisse lülitatud. 