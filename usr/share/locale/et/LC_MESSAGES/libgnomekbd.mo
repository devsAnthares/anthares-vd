��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  $        +     @     Q     d     �     �  -   �  +   �          9  3   B     v     �     �     �  �   �     e     w  A   �  "   �     �  #     @   &     g       ;   �  0   �  b     "   k  "   �     �     �     �      �       (   0     Y  &   m     �  !   �     �  .   �        #   3     W     `     w     �     �     �     �     �     �     	                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd MASTER
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2011-02-25 11:46+0200
Last-Translator: Ivar Smolin <okul@linux.ee>
Language-Team: Estonian <gnome-et@linux.ee>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
 Täiendavate pluginate aktiveerimine Aktiivsed _pluginad: Plugina lisamine Dialoogi sulgemine Valitud plugina seadistamine Valitud plugina deaktiveerimine Plugina tähtsuse suurendamine Vaikimisi grupp, mis on seotud akna loomisega Paigaldatud pluginate lubamine ja keelamine Plugina tähtsuse vähendamine Näidik: Iga akna kohta hoitakse ja hallatakse eraldi gruppi Klaviatuurinäidiku pluginad Klaviatuurinäidiku pluginad Klaviatuuri paigutus Klaviatuuri paigutus Klaviatuuripaigutus "%s"
Autoriõigused &#169; X.Org Foundation ja XKeyboardConfigi kaasaaitajad
Litsentsi andmed leiad paki metaandmetest Klaviatuuri mudel Klaviatuuri valikud Eksootiliste, harva kasutatavate paigutuste ja valikute laadimine Seadistuste lisavalikute laadimine Kirjeldus puudub. Klaviatuuripaigutuste eelvaatlemine Näidikute salvestamine ja taastamine koos paigutuste gruppidega Teisejärgulised grupid Rakendis lippude näitamine Rakendis lippude näitamine hetkel aktiivse paigutuse kohta Grupinimede asemel paigutuste nimede näitamine  Grupinimede asemel paigutuste nimede näitamine (ainult mitme paugutuse toega XFree versioonidele) Klaviatuuri eelvaade, X-telje nihe Klaviatuuri eelvaade, Y-telje nihe Klaviatuuri eelvaade, kõrgus Klaviatuuri eelvaade, laius Taustavärvus Paigutuse näidiku taustavärvus Kirjatüübi perekond Paigutuse näidiku kirjatüübi perekond Kirjatüübi suurus Paigutuse näidiku kirjatüübi suurus Teksti värvus Paigutuse näidiku teksti värvus Aktiivsete pluginate nimekiri Lubatud klaviatuurinäidiku pluginate nimekiri Pildi laadimisel tekkis viga: %s Abiteabe faili pole võimalik avada Tundmatu XKB lähtestamise viga S_aadaolevad pluginad: klaviatuuri paigutus klaviatuuri mudel paigutus "%s" paigutused "%s" mudel "%s", %s ja %s paigutus puudub valikud puuduvad valik "%s" valikud "%s" 