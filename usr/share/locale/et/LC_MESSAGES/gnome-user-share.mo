��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w       %   ;  5   a     �     �     �  X   �     $     �  Z   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share MASTER
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-02-14 14:48+0200
Last-Translator: Mart Raudsepp <leio@gentoo.org>
Language-Team: Estonian <et@li.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
 Kasutaja %s avalikud failid Kasutaja %s avalikud failid hostil %s Isiklike failide jagamise avamine, kui see on lubatud Isiklike failide jagamine Jagamine Jagamise sätted Isiklike failide jagamise sisse lülitamine, et selle kataloogi sisu üle võrgu jagada. Millal peab paroole küsima. Võimalikud väärtused on "never" (mitte kunagi), "on_write" (kirjutamisel), ja "always" (alati). Millal nõutakse paroole share;files;http;network;copy;send;jagamine;failid;faile;võrk;kopeerimine;saatmine;saada; 