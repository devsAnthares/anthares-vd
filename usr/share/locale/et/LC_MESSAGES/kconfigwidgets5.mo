��    u      �  �   l      �	     �	  	   �	     �	     
     
     
      
     1
     7
     ?
     G
     Z
     c
     o
     |
  	   �
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
     �
  	   �
     	  
             &     5     ;     J     W     ]     a  ;   j  -   �  #   �     �          )  
   F     Q     X  
   s     ~     �     �     �     �     �            	   (      2     S     m     �  
   �     �     �     �     �     �               #     3     G     M  
   l     w  )   �     �     �     �     �     �                    0  '   H     p     |     �     �     �     �     �     �  C   �     =  s   M      �     �     �          #     C     S      d     �  -   �  /   �     �  	   �  !        &      A     b     p     �     �  z  �          /     8     H     [     c     j     ~     �     �     �     �     �     �     �     �     �               .     A     T     Y     n     v     }  
   �     �     �  
   �     �  	   �     �     �     	            
   -     8  %   A     g     x     �  	   �     �     �     �     �     �     	  "   '  "   J     m     �  !   �     �     �     �  	   �     �               $     B     ^     z      �      �  "   �      �          +     7     G      ^          �     �     �     �     �  
   
       1   .  5   `     �     �  !   �     �     �          #     ;  D   W     �  n   �  +   !  $   M  "   r  (   �     �     �     �  '        0  (   =  )   f  	   �  	   �  %   �  $   �  #   �               "     (     5   H   p                d   $          J              `   L      C   ;   #   X           q   4   O          Q       M   +   S   "          t       ,              '   &          @       n   ?   Y   %       m   6   7   	   B      o          F   e      E      Z   :   !   A       N   
      .           \                 =              b   V   u   h   W   c   i   2   T          g   _          l       j   (       8   >       3   U              D       K   f      9   )   R   s   1           k           0   ]   ^   /      <   r                  -      I   P              [   G             *   a    %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2016-09-09 01:17+0300
Last-Translator: Marek Laane <qiilaq69@gmail.com>
Language-Team: Estonian <kde-i18n-doc@kde.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=n != 1;
 %1 &käsiraamat %1 &info &Tegelik suurus &Lisa järjehoidja Ta&gasi S&ulge %1 &seadistamine... &Kopeeri &Kustuta A&nneta &Redigeeri järjehoidjaid... &Otsi... &Esimene lehekülg &Mahuta leheküljele &Edasi &Mine... &Mine reale... &Mine leheküljele... &Viimane lehekülg &Saada kirjaga ... &Viska prügikasti &Uus &Järgmine lehekülg &Ava... &Aseta &Eelmine lehekülg &Trüki... &Välju &Näita uuesti &Asenda... Saada vea&raport... &Salvesta &Salvesta seadistused Õi&gekirja kontroll... &Võta tagasi Ü&les &Suurendus ... &Järgmine &Eelmine Nõuandeid näidatakse käivitami&sel Kas teadsid...?
 Seadistamine Päeva nõuanne &KDE info Pu&hasta Dokumendi õigekirja kontroll Puhasta nimekiri Dokumendi sulgemine Märgua&nnete seadistamine... Kiirkla&hvide seadistamine... Tööriistari&bade seadistamine... Valiku kopeerimine lõikepuhvrisse Uue dokumendi loomine L&õika Valiku lõikamine lõikepuhvrisse Tühista va&lik qiilaq69@gmail.com Automaatne tuvastamine Vaikimisi Täisekraanirežii&m Otsi &järgmine Otsi &eelmine Mahuta lehekülje &kõrgusele Mahuta lehekülje &laiusele Liikumine dokumendis tagasi Liikumine dokumendis edasi Liikumine esimesele leheküljele Liikumine viimasele leheküljele Liikumine järgmisele leheküljele Liikumine eelmisele leheküljele Liikumine üles Marek Laane Kirjed puuduvad Ava &viimati kasutatud Viimati avatud dokumendi avamine Olemasoleva dokumendi avamine Lõikepuhvri sisu asetamine Trükkimise eel&vaatlus Dokumendi trükkimine Rakendusest väljumine &Tee uuesti &Lähtesta Dokumendi taasnäitamine Viimase tagasivõetud toimingu uuesti sooritamine Dokumenti tehtud salvestamata muudatuste tühistamine Salvesta &kui ... Dokumendi salvestamine Dokumendi salvestamine uue nimega Va&li kõik Suurendustaseme valimine Dokumendi saatmine e-postiga &Menüüriba näitamine &Tööriistariba näitamine Menüüriba näitamine<p>Menüüriba näitamine, kui on peidetud</p> &Olekuriba näitamine Olekuriba näitamine<p>Näitab olekuriba ehk riba akna allservas, kus näeb mitmesugust infot oleku kohta.</p> Dokumendi trükitava eelvaatluse näitamine Menüüriba näitamine või peitmine Olekuriba näitamine või peitmine Tööriistariba näitamine või peitmine Vaheta rakenduse kee&lt... P&äeva nõuanne Viimase toimingu tühistamine Dokumendi näitamine tegelikus suuruses Mis &see on? Sul ei ole õigust seadistust salvestada Sul palutakse enne salvestamist autentida &Suurenda &Vähenda Lehekülje sobitamine akna kõrgusega Lehekülje sobitamine akna suurusega Lehekülje sobitamine akna laiusega &Tagasi &Edasi &Kodu &Abi 