��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	  #   �	  	   �	     �	     
  /   
     L
     ]
     c
     }
  )   �
  &   �
     �
     �
     �
  L        ^     q     y     �     �     �     �     �     �     �  1     '   >  	   f  /   p     �     �     �     �     �  
     :        M  	   T     ^     x     |     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-02-26 07:48+0000
Last-Translator: mahfiaz <mahfiaz@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: et
 Aadress Luba alati ligipääs Autoriseerimise päring seadmest %s Bluetooth Bluetoothi seaded Bluetooth on keelatud Bluetooth on keelatud riistvaralisest lülitist Lehitse faile... Loobu Bluetoothi häälestamine Ühendus Seade %s tahab ligipääsu teenusele '%s' Seade %s tahab paarduda selle arvutiga Ei ole sama Viga seadme sirvimisel Luba ainult sel korral Kui eemaldad seadme, pead selle enne järgmist kasutamist uuesti seadistama. Klaviatuuri seaded On sama Hiire seaded Hiire ja puuteplaadi seaded Ei Bluetoothi adaptereid ei leitud OK Paaris Paardumise kinnitus seadmega %s Paarsuse päring seadmele %s Palun kinnita, kas PIN '%s' on sama nagu seadmes. Palun sisesta PIN kood, mis on seadmel. Ära luba Kas eemaldada '%s' tuntud seadmete nimekirjast? Seadme eemaldamine Failide saatmine seadmesse... Saada faile... Uue seadme lisamine Seadista Uus Seade Heliseaded Soovitud seadet polnud võimalik sirvida, esines viga '%s' Tüüp Nähtavus Seadme “%s” nähtavus Jah ühendamine... lahtiühendamine... Riistvara keelatud leht 1 leht 2 