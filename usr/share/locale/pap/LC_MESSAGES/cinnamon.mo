��    A      $  Y   ,      �     �     �     �  	   �  9   �  1     I   D  �   �  `        x     �     �     �     �     �     �     �     �             	   "     ,     1  5   >     t     {     �  	   �     �  
   �  	   �     �     �     �     �     �     �     �  e   	     w	     �	      �	     �	     �	     �	     �	     �	     
     :
  "   Z
     }
  ,   �
  ,   �
  	   �
     �
  �   �
     �     �  	   �     �      �     �  i   �  	   >  �  H     �  
             .  7   =  3   u  R   �  x   �  K   u     �     �     �     �     �     �               5     J     c     x     �  	   �  7   �     �     �     �  
   �               &     4     8     ;     A     W     u     z  h   �  '         (  #   9     ]     b     o     �  "   �     �  '   �  1   �     -  4   3  2   h     �     �  q   �  	   6  
   @     K     ]  #   f     �  �   �  
        ;   )      "      6       @   ?      ,                                             !             >   #      2                                       +   
             <       /       	       -      7      =   '      :          .   &              3   (   A       $   1          4      5   8      %          *      9       0     troubleshooting purposes. About... Add applets to the panel Add panel Are you sure you want to clear all applets on this panel? Are you sure you want to remove workspace "%s"?

 Check your system log and the Cinnamon LookingGlass log for any issues.   Cinnamon is currently running without video hardware acceleration and, as a result, you may observe much higher than normal CPU usage.

 Cinnamon started successfully, but one or more applets, desklets or extensions failed to load.

 Clear all applets Close Configure... Contributors: Copy Copy applet configuration Error Execution of '%s' failed: Execution time (ms):  Failed to launch '%s' File System Hide Text Home Initializing Invalid overview options: Incorrect number of corners Loaded Loaded successfully Looking Glass Main Menu Modify panel Move panel Ne_w Item No OK Open Out of date Panel edit mode Paste Paste applet configuration Pasting applet configuration will remove all existing applets on this panel. Do you want to continue? Please contact the developer. Print version Problems during Cinnamon startup Remove Remove panel Remove this desklet Restart Cinnamon Restore System Configuration Restore all settings to default Restore the default menu layout Running in software rendering mode Search Select new position of panel. Esc to cancel. Select position of new panel. Esc to cancel. Show Text System Information There could be a problem with your drivers or some other issue.  For the best experience, it is recommended that you only use this mode for Troubleshoot Unknown WORKSPACE Website: Wrong password, please try again Yes You can disable the offending extension(s) in Cinnamon Settings to prevent this message from recurring.   _New Menu Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-11 23:59+0000
Last-Translator: Frank <f2b_owesome@hotmail.com>
Language-Team: Papiamento <pap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
  resolvementu di problema Tokante... añadi applet na e bentana añadi bentana bo tin sigur bo ke kita tur applets riba e bentana aki? Bo tin sigur ku bo ke kita e luga di trabou "%s"?

 Check bo buki di log i e buki di log di Cinnamon LookinGlass pa kualke problema.   Cinnamon ta drai awor aki sin aselerashon di video hardware i dor di esaki por ripara uso mas haltu ku normal di e CPU

 Cinnamon a inisia korektamente pero un of mas programma a faya di inisia.

 kita tur applet Sera Konfigura... Kontribuido Kopia kopia konfigurashon di applet Eror Fayo den ehekutashon di '%s' Tempu ehekutá(ms):  No a logra pa start '%s' Sistema di Dokumento Skonde e teksto Inisio Inisiando opshon di resumen invalido: number i hukinan inkorekto. Kargá Kargá kompletamente glas mirado Prome Menu bentana pa modifika movè bentana Artikulo nobo Nò OK Habri E fecha no ta korekto modo pa editamentu di bentana Plak pega konfigurashon di applet pegamentu di e konfigurashon di applet lo kita tur applet eksistente riba e bentana aki. Bo ke kontinua? Por fabor tuma kontakto ku e diseñado. Vershon pa print Problema durante inisio di Cinnamon Kita kita bentana Kita e programa aki sende di nobo Restaura kambio hasi den e sistema standardisa tur instalashonmod Restaura e standard aparensia di e menu Ehekutando den e modo di prosesamentu di software Buska selekta e posishon nobo dj'e bentana. Esc pa kansela selekta posishon dj'e bentana nobo. Esc pa kansela Mustra e teksto Informashon di e sistema Por tin un problema ku bo programanan di bo piesanan di kompiuter, ta rekomenda pa bo usa e modo aki solamente pa solushona Deskonosí Espasio pa trabou Wèpsite Kontraseña inkorekto, purba atrobe Sí Bo por paga e ekstenshon kousante di problema den e konfigurashonnan di Cinnamon Settings pa preveni e mensahe aki di bolbe ripiti.   _Menu nobo 