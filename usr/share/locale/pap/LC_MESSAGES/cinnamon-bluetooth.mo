��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 v  %     �	     �	     �	  	   �	     �	     �	  :   
     L
     h
     q
  	   �
  '   �
  (   �
     �
     �
       H        g     �     �  (   �     �  "   �     �  	   �            ?   8  -   x     �  '   �     �  !   �               =     X  7   r     �     �     �     �     �     �     �                          /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-27 11:47+0000
Last-Translator: Frank <f.boye@hotmail.nl>
Language-Team: Papiamento <pap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adrès Semper permití akseso Petishon pa autorisashon di %s Bluetooth Optionnan di Bluetooth Bluetooth a desaktivá Bluetooth a wòrdu di desaktivá via di un switch mekaniko Explora den Dokumentonan... Kanselá Konfigura Bluetooth Konekshon Aparato %s ke akseso na e servisio '%s' Aparato %s ke kòpel ku e kòmpiuter aki No ta kuadra Eror eksplorando e aparato Permití esun bia aki Si bo kita e aparato, bo lo tin ku stèl e in di nobo na e proksimo uso. Konfigurashonnan di keyboard E ta kuadra Opshonnan di e Mouse Konfigurashonnan di Mousepad ku Touchpad Nò No a haña ni un Bluetooth adapter OK Di kòpel Konfirmashon pa kòpel pa %s Petishon pa kòpel ku %s Porfabor konfirma si e PIN '%s' ta kuadra ku esun di e aparato. Por fabor yena e PIN menshona riba e aparato. Rechasa Kita '%s' for di e lista di aparatonan? Kita e aparato Pasa dokumentonan na e aparato... Manda Dokumentonan... Añadi i konfigura aparato nobo Prepara un Aparato Nobo... Konfigrashonnan di zonido E aparato pidí no por wòrdu di eksplora, eror ta '%s' Tipo Visibilidad Visibilidad di "%s" Si konektando... deskonektá... hardware desaktivá pagina 1 pagina 2 