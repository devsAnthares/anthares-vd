��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  t   8
     �
  q   �
  )   /  )   Y  &  �  )   �  +   �  {      =   |  H   �  v     '   z  A   �  �   �  Y   e  �   �  M   �  M   �  ?   %  C   e  '   �  O   �  4   !  ]   V  0   �  Y   �  %   ?  M   e  S   �       +     )   B  )   l  +   �  !   �     �     �  )                    
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd 2.19.91
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-02-22 23:49+0000
PO-Revision-Date: 2012-06-19 19:34+0200
Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>
Language-Team: Greek <team@gnome.gr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 0.2
 Προεπιλεγμένη ομάδα, ορίζετε κατά την δημιουργία του παραθύρου Ένδειξη: Διατήρηση και διαχείριση ξεχωριστών ομάδων για κάθε παράθυρο Διάταξη πληκτρολογίου Διάταξη πληκτρολογίου Διάταξη πληκτρολογίου "%s"
Πνευματικά δικαιώματα &#169; X.Org Foundation και συντελεστές XKeyboardConfig
Για πληροφορίες για την άδεια διάθεσης, δείτε τα μεταδεδομένα του πακέτου Μοντέλο πληκτρολογίου Επιλογές πληκτρολογίου Φόρτωση εξωτικών, σπάνια χρησιμοποιούμενων διατάξεων και επιλογών Φόρτωση έξτρα στοιχεία ρυθμίσεων Προεπισκόπηση διατάξεων πληκτρολογίου Αποθήκευση/επαναφορά των ενδείξεων μαζί με τις ομάδες διατάξεων Δευτερεύουσες ομάδες Εμφάνιση σημαιών στη μικροεφαρμογή Εμφάνιση σημαιών στη μικροεφαρμογή ως ένδειξη της τρέχουσας διάταξης Εμφάνιση ονομάτων διάταξης αντί ονομάτων ομάδας Εμφάνιση ονομάτων διάταξης αντί ονομάτων ομάδας (μόνο για εκδόσεις του XFree με υποστήριξη πολλαπλών διατάξεων) Προεπισκόπηση πληκτρολογίου, μετατόπιση X Προεπισκόπηση πληκτρολογίου, μετατόπιση Y Προεπισκόπηση πληκτρολογίου, ύψος Προεπισκόπηση πληκτρολογίου, πλάτος Το χρώμα παρασκηνίου  Το χρώμα παρασκηνίου της ένδειξης διάταξης Η οικογένεια γραμματοσειρών Η οικογένεια γραμματοσειρών της ένδειξης διάταξης Το μέγεθος γραμματοσειράς Το μέγεθος γραμματοσειράς της ένδειξης διάταξης Το χρώμα προσκηνίου  Το χρώμα προσκηνίου της ένδειξης διάταξης Υπήρξε σφάλμα κατά την φόρτωση της εικόνας:·%s Άγνωστη Σφάλμα αρχικοποίησης XKB διάταξη πληκτρολογίου μοντέλο πληκτρολογίου διάταξη "%s" διατάξεις "%s" μοντέλο "%s", %s και %s χωρίς διάταξη χωρίς επιλογές επιλογή "%s" επιλογές "%s" 