��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  b   �     b  v   t  8   �  _   $  R   �  e   �  X   =     �  �   �  �   C	  ;   �	  ?   ;
  ^   {
  W   �
     2  1   G  :   y                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: el
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-21 19:19+0000
Last-Translator: Tony <tonyzet988@gmail.com>
Language-Team: Greek <team@gnome.gr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: el
 Ενεργοποιεί την προστασία οθόνης για να βγείτε ομαλά. Έλεγχος... Αν η προστασία οθόνης  είναι ενεργή τότε να γίνει απενεργοποίηση Λανθασμένος κωδικός πρόσβασης Το μήνυμα που θα εμφανίζεται στην οθόνη κλειδώματος Παρακαλώ εισάγετε τον κωδικό πρόσβασης σας... Ερώτημα για το χρόνο που ήταν ενεργή η προστασία οθόνης Ερώτημα για την κατάσταση της προστασίας οθόνης Αλλαγή Χρήστη Ειδοποιεί την διεργασία της προστασίας οθόνης για να κλειδώσει την οθόνη άμεσα. Η προστασία οθόνης ήταν ενεργή για %d δευτερόλεπτο.
 Η προστασία οθόνης ήταν ενεργή για %d δευτερόλεπτα.
 Η προστασία οθόνης είναι ενεργή
 Η προστασία οθόνης είναι ανενεργή
 Η προστασία οθόνης δεν είναι ενεργή αυτή τη στιγμή.
 Ενεργοποίηση της προστασίας οθόνης (κενή οθόνη) Ξεκλείδωμα Έκδοση αυτής της εφαρμογής Έχετε πατημένο το πλήκτρο Caps Lock. 