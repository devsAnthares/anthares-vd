��    )      d  ;   �      �     �     �     �     �     �     �  	             !     :     C  	   ^     h     y     �     �     �     �     �     �  	                  ,     1     G  F   L     �     �     �  
   �     �     �     �          !     4  ,   O     |     �  �  �     �     �  2   �  .   �  F   	  /   `	     �	  6   �	  ^   �	     B
  E   S
     �
  5   �
  :   �
  K     8   g     �  '   �  $   �  #   �     "  (   1  )   Z  #   �  6   �     �  �   �  '   �     �     �  /   �  7     6   I  6   �  #   �  0   �  2     Z   ?  4   �  .   �         %           '         )               "       
                                                              !                                                     $      	   (                 #   &    Abort Alarm clock Background read from tty Background write to tty Bad argument to system call Broken pipe Bus error CPU limit exceeded Child status has changed Continue Don't fork into background EMT error Enable debugging Enable verbose output File size limit exceeded Floating-point exception Hangup I/O now possible Illegal instruction Information request Interrupt Invoked from inetd Keyboard stop Kill Profiling alarm clock Quit Run '%s --help' to see a full list of available command line options.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket User defined signal 1 User defined signal 2 Virtual alarm clock Window size change read %d byte read %d bytes read %lu byte of data read %lu bytes of data read data size wrote %d byte wrote %d bytes Project-Id-Version: el
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgtop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2013-12-19 14:39+0000
PO-Revision-Date: 2014-07-09 13:59+0200
Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>
Language-Team: Greek <nls@tux.hellug.gr>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.6.5
 Εγκατάλειψη Ξυπνητήρι Ανάγνωση παρασκηνίου από tty Εγγραφή παρασκηνίου σε tty Ακατάλληλο όρισμα σε κλήση συστήματος Κατεστραμμένη διοχέτευση Σφάλμα διαύλου Έχει ξεπεραστεί το όριο της CPU Έχει αλλάξει η κατάσταση της θυγατρικής διεργασίας Συνέχεια Να μη γίνει διακλάδωση στο παρασκήνιο Σφάλμα EMT Ενεργοποίηση αποσφαλμάτωσης Ενεργοποίηση αναλυτικής εξόδου Έχει ξεπεραστεί το όριο μεγέθους αρχείου Εξαίρεση κινητής υποδιαστολής Κλείσιμο Η I/O είναι τώρα δυνατή Μη επιτρεπτή εντολή Αίτηση πληροφορίας Διακοπή Εκτελεσμένο από το inetd Διακοπή πληκτρολογίου Βίαιος τερματισμός Έλεγχος απόδοσης ξυπνητηριού Έξοδος Εκτελέστε '%s --help' για να δείτε μια πλήρη λίστα των διαθέσιμων επιλογών της γραμμής εντολών.
 Παραβίαση κατάτμησης Διακοπή Τερματισμός Παγίδευση παρακολούθησης Επείγουσα συνθήκη σε υποδοχέα Σήμα 1 οριζόμενο από το χρήστη Σήμα 2 οριζόμενο από το χρήστη Εικονικό ξυπνητήρι Αλλαγή μεγέθους παραθύρου ανάγνωση %d byte ανάγνωση %d bytes ανάγνωση %lu byte δεδομένων ανάγνωση %lu bytes δεδομένων ανάγνωση μεγέθους δεδομένων εγγραφή %d byte εγγραφή %d bytes 