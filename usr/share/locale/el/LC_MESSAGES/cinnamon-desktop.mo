��            )   �      �     �  	   �     �     �     �     �     �  $   �  D     <   _     �     �     �     �     �       %     '   E  )   m  '   �  :   �  +   �  I   &  F   p  �   �  *   �  t   �  i   %  9   �  �  �     l	  	   y	     �	     �	     �	     �	  K   �	  B   �	  �   0
  �   �
  %   Z  %   �     �  /   �  O   �  @   7  F   x  J   �  R   
  Q   ]  h   �  K     �   d  �   �  �  �  l   >  �   �  �   l  o   =                                                                                  	                                        
               %A %B %e, %R %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-03-09 08:44+0300
Last-Translator: Dimitris Spingos (Δημήτρης Σπίγγος) <dmtrs32@gmail.com>
Language-Team: team@gnome.gr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 %A %e %B, %R %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p Το CRTC %d δεν μπορεί να οδηγήσει την έξοδο %s Το CRTC %d δεν υποστηρίζει περιστροφή=%s Το CRTC %d: δοκιμάζει την κατάσταση λειτουργίας %dx%d@%dHz με την έξοδο στα %dx%d@%dHz (επιτυχής %d)
 Αδύνατη η εύρεση τερματικού, χρήση xterm, παρόλο που μπορεί να μην λειτουργήσει Φορητός υπολογιστής Κατοπτρισμός οθονών Άγνωστος Δεν υπάρχει η επέκταση RANDR Δοκιμή καταστάσεων λειτουργίας για το CRTC %d
 αδυναμία κλωνοποίησης στην έξοδο %s αδύνατη η αντιστοίχιση CRTC σε εξόδους
%s αδυναμία εύρεσης πληροφοριών για το CRTC %d Αδυναμία εύρεσης πληροφοριών για την έξοδο %d αδύνατη η λήψη του εύρους των μεγεθών οθόνης αδυναμία λήψης των πηγών οθόνης (CRTCs, έξοδοι, λειτουργίες) αδυναμία ορισμού της ρύθμισης για το CRTC %d καμία από τις αποθηκευμένες ρυθμίσεις οθόνης δεν ταίριαζε με την ενεργή ρύθμιση καμία από τις επιλεγμένες καταστάσεις λειτουργίας δεν ήταν συμβατή με τις δυνατές καταστάσεις:
%s η έξοδος %s δεν έχει τις ίδιες παραμέτρους όπως μια άλλη κλωνοποιημένη έξοδος:
υπάρχουσα κατάσταση = %d, νέα κατάσταση = %d
υπάρχουσες συντεταγμένες = (%d, %d), νέες συντεταγμένες = (%d, %d)
υπάρχουσα περιστροφή = %s, νέα περιστροφή = %s η έξοδος %s δεν υποστηρίζει την κατάσταση λειτουργίας %dx%d@%dHz η ζητούμενη θέση/μέγεθος για το CRTC %d είναι έξω από το επιτρεπτό όριο: θέση=(%d, %d), μέγεθος=(%d, %d), μέγιστο=(%d, %d) το απαιτούμενο εικονικό μέγεθος δεν ταιριάζει με το διαθέσιμο μέγεθος: αιτήθηκε=(%d, %d), ελάχιστο=(%d, %d), μέγιστο=(%d, %d) Απρόσμενο σφάλμα X κατά τη λήψη του εύρους των μεγεθών οθόνης 