��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       S     _     t  !   x  +   �  \   �  F   #     j                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser.HEAD
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2017-04-08 20:01+0300
Last-Translator: Tom Tryfonidis <tomtryf@gnome.org>
Language-Team: Greek <team@gnome.gr>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 CD ήχου Δίσκος Blu-ray DVD Ψηφιακή τηλεόραση Αποτυχία προσάρτησης %s. Δεν υπάρχει πολυμέσο στην μονάδα της συσκευής «%s». Ελέγξτε αν υπάρχει δίσκος στην μονάδα. Βίντεο CD 