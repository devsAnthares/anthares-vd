��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  �   V  f   �  x   O  D   �  8     M   F  :   �  J   �  �     F   �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Dimitris Glezos <glezos@transifex.com>
Language-Team: Greek (http://www.transifex.com/projects/p/freedesktop/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Απαιτείται πιστοποίηση για αλλαγή των ρυθμίσεων της οθόνης αναγνώρισης χρήστη Απαιτείται πιστοποίηση για αλλαγή των δεδομένων χρήστη Απαιτείται πιστοποίηση για αλλαγή των δικών σας δεδομένων χρήστη Αλλαγή των ρυθμίσεων οθόνης σύνδεσης Αλλάξτε τα δεδομένα χρήστη σας Ενεργοποίηση κώδικα εντοπισμού σφαλμάτων Διαχείριση λογαριασμών χρηστών Εμφάνιση πληροφοριών έκδοσης και έξοδος Παροχή διεπαφών D-Bus για ερωτήματα και χειρισμό
των πληροφοριών λογαριασμών χρηστών. Αντικατάσταση υπάρχοντως στιμιότυπου 