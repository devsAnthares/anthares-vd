��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  *        <  L   O     �  /   �     �  2   �     -     M  4   Y  a   �     �          )  -   I     w     ~     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-06-28 18:59+0000
Last-Translator: Silvio Celeste <Unknown>
Language-Team: Neapolitan <nap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Fa che s'esce d''o screensaver chian chian Sto cuntrullann... Si 'o screensaver è appicciato allora s'adda stutà ('o schermo s'appiccia) 'A password è sbagliata 'O messaggio ca se vere dint o schermo bloccato Miett 'a password... Chier 'a quantu tiemp sta appicciat 'o screensaver Chier 'o stato d''o screensaver Cagn Utente Fa ca 'o screensaver blocca 'o schermo subito subito 'O screensaver è stat appicciat pe %d secondo.
 'O screensaver è stat appicciat pe %d secondi.
 'O screensaver è appicciato
 'O screensaver è stutato
 'O screensaver mo sta stutato.
 Appiccia 'o screensaver ('o schermo se stuta) Sblocc 'A versione 'e stu programm Tien 'o Caps Lock appicciat. 