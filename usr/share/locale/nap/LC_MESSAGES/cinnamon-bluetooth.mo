��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 u  %  	   �	     �	      �	  	   �	     �	     �	  -   
     <
  	   P
  %   Z
     �
  6   �
  :   �
     �
  (        7  9   N     �     �     �  #   �     �  !   �       	     !        @  H   `  6   �     �  $   �          #     @     N     i     �  E   �     �     �     �          
          5     F     O                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-06-28 18:49+0000
Last-Translator: Silvio Celeste <Unknown>
Language-Team: Neapolitan <nap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Indirizzo Daie semp l'accesso %s sta chierenn l'autorizzazione Bluetooth Settaggi d''o Bluetooth Bluetooth stutato 'o Bluetooth è stutato cu nu switch hardware Vai Truann Files... Liev Mano Acconcia l'impostazion d''o Bluetooth Connessione 'O fattapposta %s va truann l'accesso 'o servizio '%s' 'O fattapposta %s va truann 'e s'accuppià cu stu computer Nun corrisponde Errore a ment ca esploravo 'o fattaposta Daie l'accesso sulo mo Si liev 'o fattapposta l'ea settà n''ata vota pe l'usà. Settaggi d''a Tastiera Corrispondenze Settaggi d''o Mouse Settaggi d''o Mouse e d''o Touchpad No Nun ce stann adattatori Bluetooth Va buono Accuppiat Conferma 'e l'accoppiamento pe %s Richiesta d'accoppiamento pe %s Daie conferma ca 'o PIN '%s' è tale e quale a chill ngopp o fattappost. Cortesemente scriv 'o PIN ca esce ngopp o fattapposta. Nun va buono Liev '%s' d''a list dei fattapposta? Liev Fattapposta Mann Files 'o Fattapposta... Mann Files... Setta nu fattapposta nuovo Setta nu Fattapposta Nuovo... Settaggi d''o Suono 'O fattapposta che m'hè chiest nun se po' esplorà, l'errore è '%s' Scriv Visibilità Visibilità 'e "%s" Si m sto connettenn... m sto disconnettenn... hardware stutato pagina 1 pagina 2 