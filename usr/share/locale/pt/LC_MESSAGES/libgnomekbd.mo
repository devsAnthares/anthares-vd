��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  /   �
  
   �
  *   �
     �
       �        �     �  =   �  !         4  ?   U     �  !   �  C   �  ;     �   K  '   �  '   �           =     ^  +   m     �  4   �     �  3   �     )  8   A  %   z     �     �     �     �  $   �          0     E     U     p     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: 3.8
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2016-08-15 10:36+0000
PO-Revision-Date: 2016-09-13 17:37+0100
Last-Translator: Tiago Santos <tiagofsantos81@sapo.pt>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 Grupo por omissão, atribuido ao criar a janela Indicador: Manter e gerir grupos distintos por janela Disposição de Teclado Disposição de teclado Disposição de teclado "%s"
Copyright &#169; X.Org Foundation e contribuidores do XKeyboardConfig
Sobre a licença consulte os metadados do pacote Modelo do teclado Opções de teclado Ler disposições e opções exóticas e raramente utilizadas Ler itens de configuração extra Antever disposições de teclado Gravar/repor indicadores juntamente com grupos de disposições Grupos secundários Apresentar as bandeiras na applet Apresentar as bandeiras na applet para indicar a disposição atual Apresentar nomes de disposições em vez de nomes de grupos Apresentar nomes de disposições em vez de nomes de grupos (apenas para versões do XFree que suportem múltiplas disposições) A Antevisão de Teclado, deslocamento X A Antevisão de Teclado, deslocamento Y A Antevisão de Teclado, altura A Antevisão de Teclado, largura A cor de fundo A cor de fundo do indicador de disposição A família de fonte A família de fonta para o indicador de disposição O tamanho da fonte O tamanho da fonte para o indicador de disposição A cor de primeiro plano A cor de primeiro plano para o indicador de disposição Ocorreu um erro ao ler uma imagem: %s Desconhecida Erro de inicialização do XKB Disposição do teclado Modelo do teclado disposição "%s" disposições "%s" modelo "%s", %s e %s nenhuma disposição nenhuma opção opção "%s" opções "%s" preferences-desktop-keyboard 