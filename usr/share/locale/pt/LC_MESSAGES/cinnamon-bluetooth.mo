��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 n  %  	   �	     �	     �	  	   �	     �	     �	  (   
     <
     R
  $   [
  	   �
  +   �
  2   �
     �
     �
       ]   2     �     �     �  &   �     �  '   �             &   ,      S  M   t  3   �     �  &   �     &  &   :     a     u  !   �     �  >   �     	               4     8     F     W  	   l  	   v                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-18 01:14+0000
Last-Translator: Tiago S. <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Endereço Permitir sempre acesso Pedido de autorização por %s Bluetooth Definições de Bluetooth Bluetooth está desativado O Bluetooth está inativo no equipamento Explorar ficheiros... Cancelar Configurar definições de Bluetooth Ligação Dispositivo %s quer aceder ao serviço '%s' Dispositivo %s quer emparelhar com este computador Não corresponde Erro a pesquisar dispositivo Conceder apenas este tempo Se remover o dispositivo, terá de o configurar novamente antes de o poder voltar a utilizar. Definições de teclado Corresponde Definições do Rato Definições de rato e painel de toque Não Nenhum dispositivo Bluetooth encontrado OK Emparelhado Confirmação de emparelhamento por %s Pedido de emparelhamento para %s Por favor, confirme se o PIN '%s' corresponde ao que encontra no dispositivo. Por favor, introduza o PIN indicado no dispositivo. Rejeitar Remover '%s' da lista de dispositivos? Remover dispositivo Enviar Ficheiros para o Dispositivo... Enviar ficheiros... Configurar um Novo Dispositivo Configurar um Novo Dispositivo... Definições de som O dispositivo pedido não pôde ser pesquisado. O erro é '%s' Tipo Visibilidade Visibilidade de “%s” Sim a conectar... a desconectar... hardware desactivado página 1 página 2 