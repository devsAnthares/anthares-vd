��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  3        ?  S   N     �  ,   �     �  9   �  &   2     Y  [   m  b   �      ,  "   M  2   p  *   �     �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: 3.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-03 03:08+0000
Last-Translator: Tiago Santos <Unknown>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: pt
 Força o protetor de ecrã a terminar graciosamente A verificar... Se a proteção de ecrã estiver ativa, desativá-la (o ecrã deixa de estar preto) Senha incorreta Mensagem a ser mostrada no bloqueio de ecrã Introduza a sua senha... Consultar à quanto tempo o protetor de ecrã está ativo Consulta o estado do protetor de ecrã Mudar de utilizador Indica ao processo de proteção de ecrã em execução para bloquear o ecrã imediatamente O protetor de ecrã está ativo há %d segundo.
 O protetor de ecrã está ativo há %d segundos.
 O protetor de ecrã está ativo
 O protetor de ecrã está inativo
 Atualmente, o protetor de ecrã não está ativo.
 Ativar a proteção de ecrã (ecrã preto) Desbloquear Versão desta aplicação Tem a tecla Caps Lock ativa. 