��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     d     g     m     s     |     �     �     �     �     �     �     �  (   �  !     L   =     �     �  #   �  *   �  I   	  �   R	  (   '
  j   P
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: 3.10
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-06-07 18:48+0000
PO-Revision-Date: 2015-06-19 11:18+0100
Last-Translator: Pedro Albuquerque <palbuquerque73@gmail.com>
Language-Team: Pedro Albuquerque
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.6
X-Project-Style: gnome
 %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d incapaz de controlar a saída %s CRTC %d não suporta rotação=%d CRTC %d: a tentar o modo %dx%d@%dHz com a saída a %dx%d@%dHz (passagem %d)
 A tentar modos para CRTC %d
 Não especificado impossível clonar para a saída %s impossível atribuir CRTCs às saídas:
%s nenhum dos modos selecionados era compatível com os modos possíveis:
%s saída %s não tem os mesmos parâmetros que outra saída clonada:
modo existente = %d, novo modo = %d
coordenadas existentes = (%d, %d), novas coordenadas = (%d, %d)
rotação existente = %d, nova rotação = %d saída %s não suporta o modo %dx%d@%dHz tamanho virtual pedido não cabe no tamanho permitido: pedido=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 