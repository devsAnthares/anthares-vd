��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  ,  �  +   �       -     %   A  (   g  "   �     �     �     �  %   �     	  #   %  
   I     T     h  '   w     �  )   �     �  "   �     	     #	  z   1	  $   �	  3   �	     
     
  -   "
  8   P
     �
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: 3.8
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-03-17 00:10+0000
Last-Translator: Duarte Loreto <happyguy_pt@hotmail.com>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 - Daemon de acessibilidade do rato do GNOME Estilo do Botão Estilo do botão da janela de tipo de clique. Geometria da janela de tipo de clique Orientação da janela de tipo de clique Estilo da janela de tipo de clique Clique Duplo Arrastar Ativar o movimento-clique Ativar um clique secundário simulado Falha ao Apresentar a Ajuda Esconder a janela de tipo de clique Horizontal Clique ao Sobrevoar Apenas ícones Ignorar movimentos pequenos do ponteiro Orientação Orientação da janela de tipo de clique. Clique Secundário Definir o modo de movimento-clique Desligar o mousetweaks Clique Único Tamanho e posição da janela de tipo de clique. O formato é uma expressão padrão de geometria do Sistema de Janelas X. Iniciar o mousetweaks como um daemon Iniciar o mousetweaks no modo de início de sessão Texto e Ícones Apenas texto Tempo a aguardar antes de um movimento-clique Tempo a aguardar antes de um clique secundário simulado Vertical 