��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     Z     t  1   �     �  	   �     �  ]     V   f     �  +   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: 3.12
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-04 11:13+0100
Last-Translator: Pedro Albuquerque <palbuquerque73@gmail.com>
Language-Team: Português <palbuquerque73@gmail.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Ficheiros públicos de %s Ficheiros públicos de %s em %s Inicia a partilha de ficheiros pessoais, se ativa Partilha de ficheiros pessoais Partilhar Definições de partilha Ligue Partilha de ficheiros pessoais para partilhar o conteúdo desta pasta através da rede. Quando perguntar por senhas. Valores possíveis são "nunca", "ao gravar", e "sempre". Quando exigir senhas partilha;ficheiros;http;rede;copiar;enviar; 