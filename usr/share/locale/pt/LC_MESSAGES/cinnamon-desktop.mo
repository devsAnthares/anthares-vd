��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     *  J     u	     �	     �	     �	  	   �	     �	     �	     �	     �	  (   �	  !   
  L   .
  T   {
  	   �
     �
     �
  (   �
     "  "   ?  )   b  -   �  1   �  1   �  =     2   \  S   �  J   �  �   .  (     �   ,  j   �  <                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: 3.6
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-10 00:22+0000
Last-Translator: Duarte Loreto <happyguy_pt@hotmail.com>
Language-Team: Português <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d incapaz de controlar a saída %s CRTC %d não suporta rotação=%s CRTC %d: a tentar o modo %dx%d@%dHz com a saída a %dx%d@%dHz (passagem %d)
 Incapaz de encontrar uma consola, a utilizar o xterm, mesmo que possa não funcionar Portátil Ecrãs em Espelho Desconhecido a extensão RANDR não está disponível A tentar modos para CRTC %d
 incapaz de clonar para a saída %s incapaz de atribuir CRTCs às saídas:
%s incapaz de obter informação sobre o CRTC %d Incapaz de obter a informação sobre a saída %d incapaz de obter o intervalo de tamanhos de ecrã incapaz de obter os recursos de ecrã (CRTCs, saídas, modos) incapaz de definir a configuração para o CRTC %d nenhuma das configurações de ecrã gravadas coincidia com a configuração activa nenhum dos modos seleccionados era compatível com os modos possíveis:
%s saída %s não tem os mesmos parâmetros que outra saída clonada:
modo existente = %d, novo modo = %d
coordenadas existentes = (%d, %d), novas coordenadas = (%d, %d)
rotação existente = %s, nova rotação = %s saída %s não suporta o modo %dx%d@%dHz posição/tamanho pedido para o CRTC %d está fora dos limites permitidos: posição=(%d, %d), tamanho=(%d, %d), máximo=(%d, %d) tamanho virtual pedido não cabe no tamanho permitido: pedido=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) erro X não-gerido ao obter o intervalo de tamanhos de ecrã 