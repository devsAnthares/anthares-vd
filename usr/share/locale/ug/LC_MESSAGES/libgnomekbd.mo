��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >  ?     (   ^     �     �  6   �  2   �  <   %  X   b  I   �  :        @  m   T  @   �  @     3   D  3   x    �     �  '   �  �   �  ;   �     �  K   �  }   1  #   �  :   �  �     [   �  �   �  h   �  h   O  b   �  ^        z  Q   �     �  O   �     J  O   `     �  O   �  <     e   S  ?   �  2   �     ,  G   ;  6   �  3   �     �  %        4  '   Q     y     �                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd gnome-2-32
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-03-07 22:07+0000
PO-Revision-Date: 2010-10-02 23:22+0000
Last-Translator: Sahran <sahran@live.com>
Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>
Language: ug
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 تېخىمۇ كۆپ قىستۇرمىلارنى ئاكتىپلا ئاكتىپ قىستۇرمىلار(_P): قىستۇرما قوش سۆزلەشكۈنى ياپ تاللانغان قىستۇرما سەپلىمىسى تاللانغان قىستۇرمىنى چەكلە قىستۇرمىنىڭ ئالدىنلىقىنى كېمەيت كۆزنەك قۇرۇلغاندا بېكىتىلگەن كۆڭۈلدىكى گۇرۇپپا ئورنىتىلغان قىستۇرمىلارنى قوزغات/چەكلە قىستۇرمىنىڭ ئالدىنلىقىنى ئاشۇر كۆرسەتكۈچ: ھەر بىر كۆزنەككە ئايرىم-ئايرىم گۇرۇپپا ساقلاپ قال ۋە باشقۇر ھەرپتاختا كۆرسەتكۈچى قىستۇرمىلىرى ھەرپتاختا كۆرسەتكۈچى قىستۇرمىلىرى ھەرپتاختا ئورۇنلاشتۇرۇلۇشى ھەرپتاختا ئورۇنلاشتۇرۇلۇشى ھەرپتاختا ئورۇنلاشتۇرۇلۇشى "%s"
نەشر ھوقۇقى &#169; X.Org ۋەخپىسى ۋە XKeyboardConfig تۆھپىكارلارغا تەۋە
ئىجازەتنامىنى بوغچىنىڭ مېتا سانلىق-مەلۇماتىدىن كۆرۈڭ ھەرپتاختا مودېلى ھەرپتاختا تاللانمىسى ئۆزگەرگەن، ئاز ئۇچرايدىغان ھەرپتاختا ئورۇنلاشتۇرۇلۇشى ۋە سەپلىمىسىنى يۈكلە قوشۇمچە سەپلىمە تۇرلىرىنى يۈكلە چۈشەندۈرۈشى يوق. ھەرپتاختا ئورۇنلاشتۇرۇلۇشى ئالدىن كۆرۈش كۆرسەتكۈچنى جايلاشتۇرۇش گۇرۇپپىسى بىلەن قوشۇپ ساقلا/ئەسلىگە قايتۇر ئىككىلەمچى گۇرۇپپا قوللانچاقتا بايراقلارنى كۆرسەت قوللانچاقتا بايراقلارنى كۆرسىتىش ئارقىلىق ھازىرقى ئورۇنلاشتۇرۇشنى كۆرسەت گۇرۇپپا ئاتىنى ئەمەس ئورۇنلاشتۇرۇش ئاتىنى كۆرسەت گۇرۇپپا ئاتىنى ئەمەس ئورۇنلاشتۇرۇش ئاتىنى كۆرسەت(بىر نەچچە خىل ئورۇنلاشتۇرۇشنى ئىشلەتكىلى بولىدىغان XFree نىڭ نەشرىدىلا كۈچكە ئىگە) ھەرپتاختىنى ئالدىن كۆرسىتىدىغان كۆزنەكنىڭ X كوئوردېناتى ھەرپتاختىنى ئالدىن كۆرسىتىدىغان كۆزنەكنىڭ Y كوئوردېناتى ھەرپتاختىنى ئالدىن كۆرسىتىدىغان كۆزنەكنىڭ ئېگىزلىكى ھەرپتاختىنى ئالدىن كۆرسىتىدىغان كۆزنەكنىڭ كەڭلىكى تەگلىك رەڭگى ئورۇنلاشتۇرۇلۇش كۆرسەتكۈچىنىڭ تەگلىك رەڭگى خەت نۇسخىسى ئورۇنلاشتۇرۇلۇش كۆرسەتكۈچىنىڭ خەت نۇسخىسى خەت چوڭلۇقى ئورۇنلاشتۇرۇلۇش كۆرسەتكۈچىنىڭ خەت چوڭلۇقى ئالدى رەڭگى ئورۇنلاشتۇرۇلۇش كۆرسەتكۈچىنىڭ ئالدى رەڭگى ئاكتىپ قىستۇرمىلارنىڭ تىزىملىكى ئورۇنلاشتۇرۇلۇش كۆرسەتكۈچى قىستۇرمىلىرىنىڭ تىزىملىكى سۈرەتنى ئوقۇشتا خاتالىق كۆرۈلدى: %s ياردەم ھۆججىتىنى ئاچالمىدى نامەلۇم XKB نى دەسلەپلەشتۈرۈشتە خاتالىق كۆرۈلدى ئىشلىتىلىشچان قىستۇرمىلار(_A): ھەرپتاختا ئورۇنلاشتۇرۇلۇشى ھەرپتاختا مودېلى ئورۇنلاشتۇرۇلۇشى "%s" مودېل "%s"، %s ۋە %s ئورۇنلاشتۇرۇلۇشى يوق تاللانما يوق تاللانما "%s" 