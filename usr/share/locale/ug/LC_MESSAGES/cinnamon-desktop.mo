��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     G  J     �	     �	     �	     �	  
   �	     �	     �	     �	     �	  6   
  :   <
  X   w
  z   �
     K  /   e     �  %   �  5   �  -      <   .  6   k  M   �  P   �  b   A  7   �  w   �  _   T  �  �  B   6  �   y  �   G  �                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-08-19 09:30+0600
Last-Translator: Sahran <sahran@live.com>
Language-Team: GNOME Uighur Translation Project <gnome-uighur@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 %A %B %e، %R %A %B %e، %R:%S %A %B %e، %l:%M %p %A %B %e، %l:%M:%S %p %A، %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d قوزغاتقۇچ چىقىرالمايدۇ %s CRTC %d ئايلاندۇرۇشنى قوللىمايدۇ=%s CRTC %d: سىناش ھالىتى %dx%d@%dHz چىقىرىش %dx%d@%dHz (ئۆتكۈز %d)
 تېرمىنال تېپىلمىدى، xterm ئىشلىتىدۇ، ئەمما ئۇمۇ ئىشلىمەسلىكى مۇمكىن يان كومپيۇتېر نۇسخىلانغان كۆرسەتكۈچلەر نامەلۇم RANDR كېڭەيتىلمىسى يوق CRTC %d نىڭ ھالىتىنى سىناۋاتىدۇ
 %s گە كولونلىغىلى بولمىدى CRTC چىقىرىشقا تەقسىملىيەلمەيدۇ:
%s CRTC %d نىڭ ئۇچۇرىغا ئېرىشەلمىدى %d  نىڭغا مۇناسىۋەتلىك ئۇچۇرغا ئېرىشەلمىدى ئېكران چوڭ كىچىكلىك دائىرىسىگە ئېرىشەلمىدى ئېكران مەنبەسىگە ئېرىشەلمىدى ( CRTC، چىقىرىش، ھالەتلەر) CRTC %d سەپلىمىسىنى تەڭشىيەلمىدى ساقلانغان كۆرسىتىش سەپلىمىسىدە ماس كېلىدىغان ئاكتىپ سەپلىمە يوق تاللىغان ھالەتنىڭ بىرىمۇ ماسلىشىشچان ھالەت ئەمەس:
%s %s چىقىرىش يەنە بىر كۇلونلانغان چىقىرىشنىڭ ئوخشاش پارامېتىرى قىلىنمايدۇ:
نۆۋەتتىكى ھالىتى = %d، يېڭى ھالىتى = %d
نۆۋەتتىكى كوئوردېناتى = (%d، %d)، يېڭى كوئوردېناتى = (%d، %d)
نۆۋەتتىكى ئايلاندۇرۇش = %s، يېڭى ئايلاندۇرۇش = %s %s چىقىرىش %dx%d@%dHz ھالىتىنى قوللىمايدۇ CRTC %d كېرەكلىك ئورنى/چوڭلۇقى چەكلىمە يول قويىدىغان دائىرە سىرتىدا: ئورنى: =(%d، %d)، چوڭلۇقى=(%d، %d)، ئەڭ چوڭ چېكى=(%d، %d) لازىملىق مەۋھۇم چوڭلۇق ئىشلەتكىلى بولىدىغان چوڭلۇقتىن چوڭ: ئېھتىياج=(%d، %d)، ئەڭ كىچىك=(%d، %d)، ئەڭ چوڭ=(%d، %d) ئېكران چوڭ كىچىكلىك دائىرىسىگە ئېرىشىۋاتقاندا بىر تەرەپ قىلالمايدىغان X خاتالىقىغا يولۇقتى 