��          �   %   �      `     a     n     ~     �     �  $   �  D   �  <   0     m     t     �     �     �  %   �  '     )   ,  '   V  :   ~  +   �  I   �  F   /  �   v  *   D  t   o  i   �  9   N  C  �     �     �     �     �  �   	  e   �	  �   �	  �   �
     _     l  2   }  �   �  I   9  �   �  �   )  P   �  H     �   O  �     �   �  �   5  e  �  N   &    u  �   �  �   9                                                           	                            
                                     %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-02-27 23:18-0400
Last-Translator: Bahodir Mansurov <6ahodir@gmail.com>
Language-Team: Uzbek@cyrillic
Language: uz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %d чиқиш йўли %sни бошқара олмайди ЭНУ (Электрон Нурли Ускуна) %d айлантириш=%sни қўлламайди ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %d: %dx%d@%dHz режим %dx%d@%dHz (йўлак %d) чиқиш йўлига қўлланиляпти
 Мос келадиган терминал топилмади, ишламаслик эҳтимоллиги бўлишига қарамасдан xterm ишлатиляпти Лэптор Номаълум RANDR кенгайтмаси мавжуд эмас ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %d учун режимлар қўллаб кўриляпти
 чиқиш йўли %sнинг нусхасини олиб бўлмади ЭНУ (Электрон Нурли Ускуна) бошқарувчисини қуйидаги чиқиш йўлларига бириктириб бўлмади:
%s ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %d ҳақидаги маълумотни олиб бўлмади %d чиқиш йўли ҳақида маълумот олиб бўлинмади экран ҳажмлар диапазонини олиб бўлмади экран ресурслари (ЭНУ (Электрон Нурли Ускуна) бошқарувчилари, чиқиш йўллари, режим)ни олиб бўлмади ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %d учун конфигурацияни ўрнатиб бўлмади сақлаб қўйилган экран конфигурацияларининг ҳеч бири актив конфигурацияга мос келмади белгиланган режимларнинг ҳеч бири рухсат этилган режимларга мос келмади:
%s чиқиш йўли %s бошқа нусхалаштирилган чиқиш йўли параметрларидан фарқ қилади:
мавжуд режим = %d, янги режим = %d
мавжуд координаталар = (%d, %d), янги координаталар = (%d, %d)
мавжуд айланиш = %s, янги айланиш = %s %s чиқиш йўли %dx%d@%dHz режимини ишлата олмайди ЭНУ (Электрон Нурли Ускуна) бошқарувчиси %dнинг сўралган жойлашуви/ўлчами рухсат берилган чегарадан ташқарида: жойлашуви=(%d, %d), ўлчами=(%d, %d), максимум=(%d, %d) керакли виртуал ўлчам мавжуд ўлчамга мос келмади: сўралган=(%d, %d), минимум=(%d, %d), максимум=(%d, %d) экран ҳажмлар диапазонини олаётганда ечилмаган X системаси хатолиги содир бўлди 