��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J      
     
     
     0
  	   F
     P
     S
     Y
     b
  S   n
  P   �
  �     �   �     F  #   U     y  1   �  A   �  ;     ^   @  ]   �  L   �  S   J  �   �  c   -  d   �  }   �  �  t  C   �  �   ?  �   5  �                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: ru
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-14 20:14+0400
Last-Translator: Yuri Myasoedov <omerta13@yandex.ru>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
X-Generator: KBabel 1.11.4
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %e %b %R %R:%S %l:%M %p %l:%M:%S %p контроллер ЭЛТ %d не может управлять выходом %s контроллер ЭЛТ %d не поддерживает вращение=%s контроллер ЭЛТ %d: проверка режима %dx%d@%dГц с выходом на %dx%d@%dГц (попытка %d)
 Не удалось найти подходящий терминал, используется приложение xterm, хотя и оно может не работать Ноутбук Дублируемые экраны Неизвестный Расширение RANDR отсутствует Проверка режимов контроллера ЭЛТ %d
 не удалось клонировать в выход %s не удалось назначить контроллеры ЭЛТ для выходов:
%s не удалось получить информацию о контроллере ЭЛТ %d не удалось получить информацию о выходе %d не удалось получить диапазон размеров экрана не удалось получить доступ к ресурсам экрана (контроллеры ЭЛТ, выходы, режимы) не удалось установить настройки для контроллера ЭЛТ %d ни одна из сохранённых настроек не совпадает с текущей ни один из выбранных режимов не совместим с разрешёнными режимами:
%s параметры выхода %s отличаются от параметров другого клонированного выхода:
существующий режим = %d, новый режим = %d
существующие координаты = (%d, %d), новые координаты = (%d, %d)
существующее вращение = %s, новое вращение = %s выход %s не поддерживает режим %dx%d@%dГц требуемое положение или размер для контроллера ЭЛТ %d выходит за рамки допустимых пределов: положение=(%d, %d), размер=(%d, %d), максимум=(%d, %d) требуемый виртуальный размер не умещается в доступное пространство: требуется=(%d, %d), минимум=(%d, %d), максимум=(%d, %d) при получении диапазона размеров экрана получена необработанная ошибка системы X 