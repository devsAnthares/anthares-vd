��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S    w  1   �  9   �  ^   �  4   Y     �  !   �  �   �  �   N  *      N   +                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: ru
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-03-26 20:26+0300
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: русский <gnome-cyr@gnome.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Общие файлы пользователя %s Общие файлы пользователя %s на %s Открыть общий доступ к личным файлам, если включено Общий доступ к личным файлам Общий доступ Параметры доступа Включите общий доступ, чтобы сделать содержимое папки доступным по сети. Когда запрашивать пароль. Возможные значения: «never» (никогда), «on_write» (при записи) и «always» (всегда). Когда требовать пароли доступ;файлы;http;сеть;копировать;отправить; 