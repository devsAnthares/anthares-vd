��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  W  r  n   �     9  |   M  %   �  y   �  8   j  �   �  K   5	  '   �	  �   �	  �   ;
  /   6  1   f  A   �  L   �     '  !   D     f                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-01-24 19:37+0000
Last-Translator: Snow fire <vovan_kireev@mail.ru>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Приводит к корректному завершению работы экранной заставки Проверка... Если заставка активна, тогда деактивировать ее (восстановить экран) Неправильный пароль Сообщение, которое будет отображаться при заблокированном экране Пожалуйста, введите ваш пароль Запрашивает промежуток времени, в течение которого работала экранная заставка Запрашивает состояние экранной заставки Сменить пользователя Сообщает запущенному процессу заставки экрана немедленно заблокировать экран Хранитель экрана работал в течение %d секунды.
 Хранитель экрана работал в течение %d секунд.
 Хранитель экрана работал в течение %d секунд.
 Хранитель экрана включён
 Хранитель экрана выключен
 Хранитель экрана сейчас неактивен.
 Включить экранную заставку (чёрный экран) Разблокировать Версия приложения Включён Caps Lock. 