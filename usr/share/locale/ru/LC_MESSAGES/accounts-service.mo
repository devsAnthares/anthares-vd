��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  �   �  w   !  �   �  M     K   l  ,   �  Q   �  U   7  �   �  <   =               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian (http://www.transifex.com/projects/p/freedesktop/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Для изменения настроек экрана входа в систему требуется аутентификация Для изменения пользовательских данных требуется аутентификация Для изменения личных пользовательских данных требуется аутентификация Изменить настройки экрана входа в систему Изменить личные пользовательские данные Включить отладочный код Управление учётными записями пользователей Вывод информации о версии и выход из программы Предоставляет интерфейс D-Bus для опроса и изменения
информации об учётных данных пользователей. Заменить существующий экземпляр 