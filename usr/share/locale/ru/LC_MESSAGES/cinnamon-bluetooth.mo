��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 r  %  
   �	  4   �	  0   �	  	   	
     
     0
  A   K
     �
     �
  /   �
     �
  R   �
  f   Q     �  8   �  E   	  �   O  '   �          +  >   G     �  4   �     �     �  1   �  '     r   6  F   �     �  ;     #   =  :   a      �  2   �  5   �     &  r   D     �     �     �     �     �       +         L     _                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-05-18 18:36+0000
Last-Translator: Dmitriy Kulikov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Адрес Всегда предоставлять доступ Запрос на авторизацию от %s Bluetooth Параметры Bluetooth Bluetooth отключен Bluetooth отключен на аппаратном уровне Выбрать файлы... Отмена Настроить параметры Bluetooth Подключение Устройство %s запрашивает доступ к службе «%s» Устройство %s запрашивает соединение с этим компьютером Не совпадают Ошибка обнаружения устройства Предоставить доступ только в этот раз Если удалить устройство, при следующем использовании потребуется повторная настройка. Настройки клавиатуры Совпадают Настройки мыши Настройки мыши и сенсорной панели Нет Адаптеры Bluetooth не обнаружены OK Сопряженные Подтвердить сопряжение с %s Запрос сопряжения с %s Пожалуйста, подтвердите, что PIN-код «%s» и устройства совпадают. Введите PIN-код, заданный на устройстве. Отказать Удалить «%s» из списка устройств? Удалить устройство Отправить файлы на устройство... Отправить файлы… Настроить новое устройство Настроить новое устройство... Настройки звука Запрашиваемое устройство не может быть обнаружено, ошибка «%s» Тип Видимость Видимость «%s» Да соединение... отключение... оборудование отключено страница 1 страница 2 