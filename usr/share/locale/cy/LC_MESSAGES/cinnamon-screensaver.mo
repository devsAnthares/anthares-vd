��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  )   =  	   g  B   q     �  #   �     �  5        :     W  E   h  �   �  !   �  %   �  )   �  ,   	     9	     A	  &   W	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: gnome-backgrounds
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-11 13:59+0000
Last-Translator: Rhoslyn Prys <rprys@yahoo.com>
Language-Team: Welsh <gnome-cy@lists.linux.org.uk>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1 ? 0 : n==2 ? 1 : (n != 8 && n != 11) ? 2 : 3;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Achosi i'r arbedwr sgrin orffen yn daclus Gwirio... Os yw'r arbedwr sgrin ar waith, ei ddi-ysgogi (dad-glirio'r sgrin) Cyfrinair anghywir Y neges i'w ddangos ar y sgrin cloi Rhowch eich cyfrinair... Holi hyd yr amser mae'r arbedwr sgrin wedi bod yn fyw Holi cyflwr yr arbedwr sgrin Newid Defnyddiwr Gorchymyn i'r broses arbedwr sgrin sy'n rhedeg i gloi'r sgrin yn syth Mae'r arbedwr sgrin wedi bod yn weithredol ers %d eiliad.
 Mae'r arbedwr sgrin wedi bod yn weithredol ers %d eiliad.
 Mae'r arbedwr sgrin wedi bod yn weithredol ers %d eiliad.
 Mae'r arbedwr sgrin wedi bod yn weithredol ers %d eiliad.
 Mae'r arbedwr sgei yn weithredol
 Nid yw'r arbedwr sgrin yn weithredol
 Nid yw'r arbedwr sgrin y weithredol eto.
 Troi'r arbedwr sgrin ymlaen (clirio'r sgrin) Datgloi Fersiwn y rhaglen hon Mae'r fysell Caps Lock ymlaen gennych. 