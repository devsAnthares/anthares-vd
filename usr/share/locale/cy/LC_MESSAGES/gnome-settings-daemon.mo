��    G      T  a   �                #     (  K   .  [   z  &   �     �  (     S   ,      �  "   �  $   �  $   �  &        5     E     W  >   ]     �  9   �     �  A   �  7   )	  8   a	  #   �	     �	     �	     �	     
     
     +
     =
     I
     Q
     W
     ^
     d
     v
     
     �
     �
     �
     �
     �
     �
     �
     �
  #        /  #   =  -   a  z   �  &   
  m   1     �     �     �     �     �  $   �  	   �  *     �   1  �   �  �   L  	   �     �          $     *  e  9     �     �     �  H   �  Y     /   `     �  *   �  Q   �  $     +   8  .   d  -   �  /   �     �            <   %     b  =   h     �  A   �  7   �  6   0  (   g  )   �  &   �     �     �               -  
   :     E  
   Q     \     e     x     �     �     �     �     �     �  /   �          $      =     ^  %   m  0   �  �   �     X  e   v     �  
   �     �     	       %   %     K  '   ]  �   �  �     �   �  	   v     �  #   �     �     �            6               C   >      7   @   "              8                        =   #      4   G       $       -      3   F   5   /         B                :               9      	   ?               .   *   2                         +      !   D       ;   (   ,         &                        %   <   '       
   0      )   E           A       1       A_vailable files: Beep Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder It seems that another application already has access to key '%u'. Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Siren Slow Keys Alert Sound Sound not set for this event. Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: gnome-control-center
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-03-08 12:53-0000
Last-Translator: Rhys Jones <rhys@sucs.org>
Language-Team: Cymraeg <gnome-cy@pengwyn.linux.org.uk>
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n == 2) ? 1 : 0;
 _Ffeiliau ar gael: Bîp Boing Methu creu'r cyfeiriadur "%s".
Mae angen hwn i ganiatáu newid cyrchwyr. Methu creu'r cyfeiriadur "%s".
Mae angen hwn i ganiatáu newid thema pwyntydd y llygoden. Methwyd canfod cyfeiriadur cartref y defnyddiwr Clinc Methwyd llwytho ffeil sain %s fel sampl %s Methwyd rhoi'r peiriant i gysgu.
Gwirio fod y peiriant wedi ei gyflunio yn gywir. _Peidio â dangos y rhybudd yma eto. Ydych chi eisiau troi Bysellau Araf ymlaen? Ydych chi eisiau troi Bysellau Gludiog ymlaen? Ydych chi eisiau troi Bysellau Araf i ffwrdd? Ydych chi eisiau troi Bysellau Gludio i ffwrdd? _Peidio ag actifadu _Peidio â dad-actifadu Allfwrw Gwall wrth geisio gweithredu (%s)
sy'n rhwym i'r fysell (%s) Ffont Gosodwyd yr allwedd GConf %s i fath %s ond disgwylid math %s
 Plygell cartref Mae'n debyg fod gan raglen arall fynediad i'r fysell '%u' eisoes. Diffinnir gweithred y Rhwymiad Bysell (%s) sawl gwaith
 Diffinnir rhwymiad y Rhwymiad Bysell (%s) sawl gwaith
 Defnyddir y Rhwymiad Bysell (%s) eisoes
 Mae'r Rhwymiad Bysell (%s) yn anghyflawn
 Mae'r Rhwymiad Bysell (%s) yn annilys
 Bysellfwrdd Lansio'r porwr cymorth Lansio porwr gwe Llwytho ffeiliau modmap Cloi'r sgrin Allgofnodi Mewngofnodi Allgofnodi Llygoden Hoffterau Llygoden Dim sain Chwarae (neu chwarae/seibio) Chwilio Dewiswch Ffeil Sain Seiren Rhybudd Bysellau Araf Sain Seiniau heb eu gosod ar gyfer y digwyddiad hwn. Dechrau'r arbedwr sgrin Rhybudd Bysellau Gludiog Trinwyr testun/plaen a thestun/* Synau'r System NId yw'r ffeil %s yn ffeil WAV ddilys Nid yw'r ffeil sain i'r digwyddiad yma'n bodoli. Nid yw'r ffeil sain ar gyfer y digwyddiad yma'n bodoli.
Efallai yr hoffech chi sefydlu'r pecyn gnome-audio er mwyn gael set o seiniau rhagosodedig. Gwall wrth ddangos cymorth:%s Bu gwall wrth gychwyn yr arbedwr sgrin:

%s

Fydd arbed sgrin ddim yn gweithio yn ystod y sesiwn yma. Gorffwys Teipio Lefel Sain Lefel sain i lawr Mudo'r sain Gris lefel sain Gris lefel sain fel canran o'r lefel. Lefel sain i fyny Hoffech chi lwytho'r ffeil(iau) modmap? Rydych newydd ddal lawr y fysell Shift am 8 eiliad. Dyma'r byrlwybr ar gyfer y nodwedd Bysellau Araf, sy'n effeithio sut mae'ch bysellfwrdd yn gweithio Rydych newydd wasgu'r fysell Shift 5 gwaith mewn rhes. Dyma'r byrlwybr ar gyfer y nodwedd Bysellau Gludiog, sy'n effeithio sut mae'ch bysellfwrdd yn gweithio Rydych newydd wasgu dwy fysell ar unwaith neu wasgu'r fysell Shift 5 gwaith mewn rhes. Mae hyn yn troi'r nodwedd Bysedd Gludiog ymlaen, sy'n effeithio sut mae'ch bysellfwrdd yn gweithio. _Actifadu _Dad-actifadu _Peidiwch â dangos y neges yma eto _Llwytho _Ffeiliau wedi'u llwytho: 