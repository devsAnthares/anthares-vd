��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 u  %  	   �	     �	     �	  	   �	     �	     �	  5   
     I
     Z
      b
  
   �
  /   �
  0   �
     �
       #     Y   7     �     �     �  %   �     �     �                    2  C   H  ,   �     �      �     �     �               3     J  '   Z     �  	   �     �     �     �     �     �  	   �  	   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-03 16:21+0000
Last-Translator: Rhoslyn Prys <rprys@yahoo.com>
Language-Team: Welsh <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Cyfeiriad Caniatáu mynediad bob tro Cais i awdurdodi gan %s Bluetooth Gosodiadau Bluetooth Bluetooth wedi ei analluogi Mae Bluetooth wedi ei analluogi drwy switch caledwedd Pori Ffeiliau... Diddymu Ffurddweddu gosodiadau Bluetooth Cysylltiad Mae dyfais %s eisiau mynediad at wasanaeth '%s' Mae dyfais %s eisiau paru gyda'r cyfrifiadur yma Nid yw'n cydweddu Gwall dyfais pori Caniatáu mynediad y tro yma'n unig Os ydych chi'n tynnu'r ddyfais o'r rhestr, bydd rhaid i chi ei gosod eto cyn ei defyddio. Gosodiadau'r Bysellfwrdd Cydweddu Gosodiadau llygoden Gosodiadau'r Llygoden a Phad Cyffwrdd Na Heb ganfod addaswyr Bluetooth Iawn Paredig Cadarnhau paru gyda %s Cais paru ar gyfer %s Cadarnhewch p'un ai yw PIN '%s' yn cydweddu gyda'r un ar y ddyfais. Rhowch y PIN sy'n cael ei nodi ar y ddyfais. Gwrthod Tynnu '%s' o'r rhestr dyfeisiau? Tynnu Dyfais Anfon Ffeiliau i'r Ddyfais... Anfon Ffeiliau... Gosod Dyfais Newydd Gosod Dyfais Newydd... Gosodiadau Sain Nid oes modd pori'r ddyfais, gwall '%s' Math Gwelededd Gwelededd "%s" Iawn cysylltu... datgysylltu... analluogi'r caledwedd tudalen 1 tudalen 2 