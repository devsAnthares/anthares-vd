��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                   %     �	     �	     �	  	   �	     �	     �	  )   
     7
     G
     N
  	   e
  +   o
  5   �
  	   �
  ,   �
       m        �     �     �     �     �  #   �     �     �       !   (  .   J  &   y     �  %   �     �     �     	          .     D  @   R  	   �     �     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-12-12 12:02+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Javanese <jv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Alamat Tansah ngawenehake akses Diaturi wewenang teka %s Bluetooth Setelan Bluetooth Bluetooth dipateni Bluetooth dipateni kangge tombol hardware Telusuri berkas Wurung Atur setelan Bluetooth Sambungan Perangkat %s bade ngakses marang layanan %s Perangkat %s kepengin disandingake kaleh komputer iki Ora cocog Wonten kasalahan ing wayah njlajahi piranti. Ijintkan saniki wae Samisale sampeyan ngilangake prangkat iki teka daftar, sampeyan kudu ngatur maneh teka awal sadurunge digawe. Setelan Papan Ketik Cocog Setelan Mouse Setelan Mouse lan Touchpad Mboten Ora ono adapter Bluetooth ditemuake OK Dipasangaken Panjalukan konfirmasi kanggo %s Permintaan penyandingan marang %s PIN '%s' kudu  cocog ing piranti sapanunggale. Ketikake PIN sing kasebut ing piranti. Tolak Ilangake '%s' teka daftar perangkat ? Ilangno prangkat teka daftar Kirim berkas menyang piranti... Kirim berkas... Gawe perangkat anyar Atur piranti anyar... Setelan Suara Piranti kang kepilih ora bisa di jelajahi, kasalahane yaiku '%s' Jenisipun ketok Kanampakan "%s" Iyo Nyambungaken... Ngaputusake... Piranti keras dipatèni Kaca 1 Kaca 2 