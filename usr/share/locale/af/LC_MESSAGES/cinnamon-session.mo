��    7      �  I   �      �     �  5   �  7        ;  (   B  )   k  %   �  '   �  "   �               0  !   5     W     h     k           �     �     �     �     �     �     	  '     	   @  '   J  P   r     �     �     �     �          (      C  +   d     �     �     �     �     �      �  E   	     X	  &   `	     �	  X   �	  X   �	  $   X
  
   }
     �
     �
  
   �
     �
  �  �
     �  5   �  H   �  
     '   "  &   J  (   q  5   �  '   �     �          #  *   )     T     k     n  *   �      �     �     �     �      �          :  "   F  	   i  (   s  H   �     �     �               5     L     g  2   �     �     �     �     �  !        /  M   L     �  (   �     �  k   �  j   U     �  
   �     �  	   �  
   �                4   ,   1       
      	         /   "   !          #   5                 +   $      6   %                     &       *             3                                 )                 2   (                   -          .       7          '       0       A program is still running: Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Session management options: Session to use Show session management options Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session 2.6-branch
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:17+0000
Last-Translator: Friedel Wolff <Unknown>
Language-Team: translate-discuss-af@lists.sourceforge.net
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Project-Style: gnome
Language: af
 'n Program loop steeds: Toepassing aanvaar nie dokumente op die opdraglyn nie Kan nie dokument URI's na 'n 'Type=Link' "desktop"-inskrywing aangee nie Kanselleer Kon nie met sessiebestuurder koppel nie Kon nie 'n ICE-luistersok skep nie: %s Deaktiveer konneksie na sessiebestuurder Moenie gebruiker-gespesifiseerde toepassings laai nie Moenie vra vir gebruikerbevestiging nie Aktiveer ontfoutkode Afgesluit met kode %d LÊER Lêer is nie 'n geldige .desktop-lêer nie Hiberneer in elk geval ID Ikoon '%s' nie gevind nie Gaan enige bestaande inhibeerders ignoreer Afsluiting geforseer met sein %d Sluit skerm Meld in elk geval af Meld af Meld nou af van hierdie stelsel? Nie 'n lanseerbare item nie Reageer nie Oorheers standaard outobegin gidse Skakel af Program geroep met konflikterende opsies Nuwe klientverbinding word geweier omdat die sessie tans afgesluit word
 Sl_uimer Sessiebestuurkeuses: Sessie om te gebruik Wys keuses vir sessiebestuur Skakel in elk geval af Skakel die stelsel nou af? Sommige programme loop steeds: Spesifiseer lêer wat gestoorde konfigurasie bevat Spesifiseer sessiebestuurs-ID Begin tans %s Gestop met sein %d Sluimer in elk geval Wissel in elk geval van gebruiker Dié program keer afmelding. Kon nie 'n aanmeldsessie begin nie (en kon nie aan die X-bediener koppel nie) Onbekend Nieherkende "desktop"-lêerweergawe '%s' Nieherkende lanseerkeuse: %d Wag nog vir programme om te voltooi.  Onderbreking van dié programme kan veroorsaak dat werk verloor word. Wag nog vir die program om te voltooi.  Onderbreking van die program kan veroorsaak dat werk verloor word. U is tans aangemeld as "%s". _Hiberneer Me_ld af He_rbegin _Skakel af Wi_ssel gebruiker 