��    g      T  �   �      �     �     �     �     �     	  .   	  
   N	     Y	     k	  K   p	  [   �	  &   
  	   ?
     I
     Z
  -   u
  A   �
  (   �
  S     
   b      m  "   �  $   �  $   �  &   �     "     2     D     S     d     j  >   �     �     �  9   �          /     K     W     j     s     �     �     �     �  $   �     �     �  
   �                  H   2     {     �     �  /   �     �     �     �     �  (   �     %     +     ;     A     _     l     ~  #   �  *   �  -   �  z   	  &   �  m   �          &     :     W     c     {     �     �     �     �  $   �  	   �  *   �     "  
   6     A  �   H  �   �  �   c  	             /     ;  *   [     �     �  9   �  �  �  #   b     �     �  $   �     �  6   �  
        $     6  P   ;  [   �  )   �               /  1   N  J   �  .   �  Y   �  
   T  &   _  %   �     �  '   �      �          )     @     T     j     s  L   �  
   �     �  E   �  !   ?     a     �     �     �     �     �     �  
   �  	   �  (        *     @     `     n     �     �  S   �     �  
          <        R     W     \     l  (        �     �     �  0   �               -  ,   ;  ,   h  3   �  �   �  /   M  w   }     �                
   A   !   L      n      �      �   
   �      �   -   �      �      �      !     2!     @!  �   G!  �   �!  �   �"  	   ?#  #   I#     m#  %   y#  0   �#     �#     �#  A   �#     ]   G      9      b       J   3   \           +   E   X   '       6       K   B   [   :      @              &   T       Z           a                    0      f   %   P       Y             2             _              /   A          U       e   N      ,           8       V          4       L   1   ?      
   d      M   !   S   7   H   ;   O          =      >   .       	           `          F   g                  ^       C      $   (   D         I              )      #   -      *   5   "   <   Q   R   W   c    <i>Rotation not supported</i> A_vailable files: Accessibility Keyboard Accessibility keyboard plugin Activate An error occurred while configuring the screen Background Background plugin Beep Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clipboard Clipboard plugin Configure display settings Could not enable mouse accessibility features Couldn't execute command: %s
Verify that this is a valid command. Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Deactivate Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Don't activate Don't deactivate Eject Enhance _contrast in colors Error while trying to run (%s)
which is linked to the key (%s) Font Font plugin GConf key %s set to type %s but its expected type was %s
 Key binding (%s) is incomplete Key binding (%s) is invalid Keybindings Keybindings plugin Keyboard Keyboard plugin Left Load modmap files Login Logout Make _text larger and easier to read Manage X Settings Manage the X resource database Media keys Media keys plugin Mouse Mouse Preferences Mouse accessibility requires mousetweaks to be installed on your system. Mouse plugin No sound Normal Press and _hold keys to accept them (Slow Keys) Right Search Select Sound File Select sound file... Set up screen size and rotation settings Siren Slow Keys Alert Sound Sound not set for this event. Sound plugin Sticky Keys Alert System Sounds The file %s is not a valid wav file The selected rotation could not be applied The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Typing break plugin Universal Access Preferences Upside Down Use on-screen _keyboard Use screen _magnifier Use screen _reader Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? X Resource Database X Settings XRandR You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Configure Display Settings ... _Deactivate _Do not show this message again _Ignore duplicate keypresses (Bounce Keys) _Load _Loaded files: _Press keyboard shortcuts one key at a time (Sticky Keys) Project-Id-Version: gnome-control-center 2.6-branch
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2008-09-15 23:10+0200
Last-Translator: F Wolff <friedel@translate.org.za>
Language-Team: translate-discuss-af@lists.sourceforge.net
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: VirTaal 0.1
 <i>Rotasie nie geondersteun nie</i> _Beskikbare lêers: Toeganklikheidsleutelbord Inprop vir toeganklikheidsleutelbord Aktiveer 'n Fout het voorgekom met die opstelling van die skerm Agtergrond Agtergrond-inprop Biep Kan nie die gids "%s" skep nie.
Dit word benodig vir die verandering van wysers. Kan nie die gids "%s" skep nie.
Dit word benodig vir die verandering van die muiswysertema. Kan nie gebruiker se tuisgids vasstel nie Knipbord Inprop vir knipbord Opstelling van visuele vertoon Kon nie muis-toeganklikheidskenmerke aktiveer nie Kon nie opdrag uitvoer nie: %s
Verifieer dat hierdie 'n geldige opdrag is. Kon nie klanklêer %s laai as voorbeeld %s nie Kon nie die masjien in slaap stel nie.
Verifieer dat die masjien korrek gekonfigureer is. Deaktiveer Moe_nie weer hierdie boodskap wys nie. Wil jy die Stadige-sleutels aktiveer? Wil jy Taai-sleutels aktiveer? Wil jy die Stadige-sleutels deaktiveer? Wil jy Taai-sleutels deaktiveer? Moe_nie aktiveer nie Moe_nie deaktiveer nie Moenie aktiveer nie Moenie deaktiveer nie Uitskiet Verbeter _kleurkontras Fout terwyl probeer is om (%s)
 te laat loopwat gekoppel is aan sleutel (%s) Lettertipe Inprop vir lettertipes GConf-sleutel %s is ingestel na tipe %s maar sy verwagte tipe was %s
 Sleutelbinding (%s) is onvolledig Sleutelbinding (%s) is ongeldig Sleutelbindings Inprop vir sleutelbindings Sleutelbord Inprop vir die sleutelbord Links Laai modmap-lêers Aanmelding Afmelding Maak _teks groter en makliker om te lees Bestuur X-instellings Bestuur die X-hulpbrondatabasis Mediasleutels Inprop vir mediasleutels Muis Muisvoorkeure Muis-toeganklikheidskenmerke benodig dat mousetweaks op u stelsel geïnstalleer is. Inprop vir die muis Geen klank Normaal Druk en _hou sleutels om hulle te aanvaar (Stadige-sleutels) Regs Soek Kies Klanklêer Kies klanklêer... Instellings vir skermgrootte en -rotasie Sirene Stadige-sleutels waarskuwing Klank Geen klank opgestel vir hierdie gebeurtenis nie. Inprop vir klank Taaisleutels-waarskuwing Stelselklanke Die lêer %s is nie 'n geldige wav-lêer nie Die gekose rotasie kon nie toegepas word nie Die klanklêer vir hierdie gebeurtenis bestaan nie. Die klanklêer vir hierdie gebeurtenis bestaan nie.
Oorweeg om die pakket gnome-audio te installeer vir 'n stel van verstek klanke. Daar was 'n fout met die vertoning van hulp: %s Daar was 'n probleem met die laai van die skermskut:

%s

Skermskut funksionaliteit sal nie in hierdie sessie werk nie. Tikonderbreking Inprop vir tikonderbreking Voorkeure vir universele toegang Onderstebo Gebruik sleutelbord op die s_kerm Gebruik skerm_vergrootglas Gebruik _skermleser Volume sagter Volume uit Volume trap Volume trap as 'n persentasie van die volume. Volume harder Wil u die modmap-lêer(s) laai? X-hulpbrondatabasis X-instellings XRandR Jy het pas die Shift-sleutel vir 8 sekondes gedruk.  Dit is die kortpad vir die Stadige-sleuteleienskap wat die manier waarop jou sleutelbord werk, affekteer. Jy het pas die Shift-sleutel 5 keer namekaar gedruk.  Dit is die kortpad vir die Taai-sleutels eienskap wat die manier waarop jou sleutelbord werk, affekteer. Jy het pas twee sleutels gelyktydig gedruk, of die Shift-sleutel 5 keer namekaar gedruk.  Dit skakel die Taai-sleutels eienskap af wat die manier waarop jou sleutelbord werk, affekteer. _Aktiveer _Opstelling van visuele vertoon ... _Deaktiveer _Moenie weer hierdie boodskap wys nie _Ignoreer duplikaat sleuteldrukke (bonssleutels) _Laai Ge_laaide lêers: _Druk sleutelbordkortpaaie een sleutel op 'n slag (Taai-sleutels) 