��    &      L  5   |      P     Q     Y  	   w     �     �  (   �     �     �     �  
          K   !     m          �     �     �     �     �  B   �     #  %   *     P     ^     v     �     �     �     �  
   �     �     �     �     �                 �  &     �     �  	   �     �     �  3         4  
   I     T  
   f     q  X   �     �     �     	  #   	     <	     ?	     F	  I   ^	     �	  (   �	     �	     �	     
     
     )
     E
     W
     \
     h
     �
  
   �
     �
     �
     �
     �
        $   #                                        &                 !          	                                            "                                 
   %                    Address Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Does not match If you remove the device, you will have to set it up again before next use. Keyboard Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Please confirm whether the PIN '%s' matches the one on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-05-06 12:22+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Afrikaans <af@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adres Magtiging benodig van %s Bluetooth Bluetooth-instellings Bluetooth is gedeaktiveer Bluetooth is gedeaktiveer met 'n hardewareskakelaar Blaai deur Lêers... Kanselleer Stel Bluetooth op Verbinding Dit stem nie ooreen As die toestel verwyder word, sal dit weer opgestel moet word voor die volgende gebruik. Sleutelbord Instellings Muis- en Raakblad Instellings Nee Geen Bluetooth-hardeware gevind nie OK Gepaar Paar bevestiging vir %s Bevestig asseblief of die PIN '%s' ooreenstem met die een op die toestel. Weier Verwyder '%s' uit die lys van toestelle? Verwyder Toestel Stuur lêers na toestel ... Stuur Lêers... Stel nuwe toestel op Stel 'n nuwe toestel op ... Klank Instellings Tipe Sigbaarheid Sigbaarheid van “%s” Ja verbind... breek verbinding... Hardeware gedeaktiveer bladsy 1 bladsy 2 