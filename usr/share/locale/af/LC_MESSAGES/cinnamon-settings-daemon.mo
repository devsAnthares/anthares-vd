��          L      |       �   &   �      �   �   �   �   y  �     �  �  /   p      �  �   �  �   `  �   �                                         There was an error displaying help: %s Universal Access Preferences You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center 2.6-branch
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:01+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: translate-discuss-af@lists.sourceforge.net
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Daar was 'n fout met die vertoning van hulp: %s Voorkeure vir universele toegang Jy het pas die Shift-sleutel vir 8 sekondes gedruk.  Dit is die kortpad vir die Stadige-sleuteleienskap wat die manier waarop jou sleutelbord werk, affekteer. Jy het pas die Shift-sleutel 5 keer namekaar gedruk.  Dit is die kortpad vir die Taai-sleutels eienskap wat die manier waarop jou sleutelbord werk, affekteer. Jy het pas twee sleutels gelyktydig gedruk, of die Shift-sleutel 5 keer namekaar gedruk.  Dit skakel die Taai-sleutels eienskap af wat die manier waarop jou sleutelbord werk, affekteer. 