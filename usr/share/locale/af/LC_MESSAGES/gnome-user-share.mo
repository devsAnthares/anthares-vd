��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S    w     �     �  /   �     �            V   "  W   y     �  '   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-01-29 12:52+0200
Last-Translator: Pieter Schoeman <pieter@sonbesie.co.za>
Language-Team: Afrikaans <pieter@sonbesie.co.za>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
X-DamnedLies-Scope: partial
X-Project-Style: gnome
 %s se openbare lêers %s se openbare lêers op %s Begin persoonlike lêerdeling indien geaktiveer Persoonlike lêerdeling Deel Deel instellings Skakel persoonlike lêerdeling aan om die inhoud van die gids oor die netwerk te deel. Wanneer om vir wagwoorde te vra. Moontlike waardes is "never", "on_write", en "always". Wanneer om wagwoorde te vereis deel;lêers;http;netwerk;kopieer;stuur; 