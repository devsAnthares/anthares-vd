��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     d  J     �	     �	     �	     �	  	   �	     �	     
     
     
  A   
  A   _
  �   �
  �   9       ;   (     d  D   w  i   �  I   &  r   p  l   �  e   P  h   �  w     `   �  �   �  �   �  �  �  W   >  �   �  �   h  �   \                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: as
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-11 20:49+0530
Last-Translator: Nilamdyuti Goswami <ngoswami@redhat.com>
Language-Team: as_IN <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d এ আউটপুট %s চলাব নোৱাৰে CRTC %d এ ঘূৰ্ণন=%s সমৰ্থন নকৰে CRTC %d: %dx%d@%dHz থকা আউটপুটৰ সৈতে অৱস্থা %dx%d@%dHz চেষ্টা কৰা হৈ আছে (pass %d)
 টাৰ্মিনেল পোৱা নাযায়, xterm ব্যৱহাৰৰ প্ৰচেষ্টা কৰা হৈছে, যদিও এই চেষ্টা ব্যৰ্থ হ'ব পাৰে। লেপটপ আইনা কৰা প্ৰদৰ্শনসমূহ অজ্ঞাত RANDR সম্প্ৰসাৰন উপস্থিত নাই CRTC %d ৰ কাৰণে অৱস্থাসমূহ চেষ্টা কৰা হৈ আছে
 আউটপুট %s লে ক্লন কৰিব নোৱাৰি CRTCসমূহক আউটপুটসমূহলে ধাৰ্য্য কৰিব নোৱাৰি:
%s CRTC %d সংক্ৰান্ত তথ্য প্ৰাপ্ত কৰিবলৈ ব্যৰ্থ আউটপুট %d সম্বন্ধে কোনো তথ্য পোৱা নাযায় পৰ্দাৰ মাপৰ সীমা প্ৰাপ্ত কৰিবলৈ ব্যৰ্থ পৰ্দা সম্পদসমূহ (CRTC, আউটপুট, অৱস্থা) পোৱা নাযায় CRTC %d ৰ বিন্যাস নিৰ্ধাৰণ কৰিবলৈ ব্যৰ্থ প্ৰদৰ্শন সংক্ৰান্ত সংৰক্ষিত কোনো বিন্যাসৰ সৈতে সক্ৰিয় বিন্যাসৰ মিল পোৱা নাযায় বাচনি কৰা অৱস্থাসমূহৰ কোনোটোৱে সম্ভাব্য অৱস্থাসমূহৰ লগত খাপ খোৱা নাছিল:
%s আউটপুট %s ৰ অন্য ক্লন্ড আউটপুটৰ নিচিনা একে প্ৰাচল নাই:
উপলব্ধ অৱস্থা = %d, নতুন অৱস্থা = %d
উপলব্ধ স্থানাংকসমূহ= (%d, %d), নতুন স্থানাংকসমূহ = (%d, %d)
উপলব্ধ ঘূৰ্ণন = %s, নতুন ঘূৰ্ণন = %s আউটপুট %s এ %dx%d@%dHz অৱস্থা সমৰ্থন নকৰে CRTC %d ৰ বাবে অনুৰোধ কৰা অবস্থান/মাপ সীমাবহিৰ্ভূত: অবস্থান=(%d, %d), মাপ=(%d, %d), সৰ্বাধিক=(%d, %d) আবশ্যক ভাৰ্চুৱেল মাপ, উপলব্ধ মাপৰ সৈতে নিমিলে: অনুৰোধ কৰা মাপ=(%d, %d), সৰ্বনিম্ন=(%d, %d), সৰ্বাধিক=(%d, %d) পৰ্দাৰ মাপৰ সীমা প্ৰাপ্ত কৰাৰ সময় উৎপন্ন X সংক্ৰান্ত এটা ত্ৰুটিৰ ব্যৱস্থাপনা কৰা সম্ভব নহয় 