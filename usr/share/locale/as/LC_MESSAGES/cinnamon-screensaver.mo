��          L      |       �   `   �      
     %  )   B     l  �  �  �   x  L   j  U   �  d     >   r                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: as
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-10-06 13:21+0000
Last-Translator: maxspice <Unknown>
Language-Team: American English <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 স্ক্রীনছেইভাৰখন %d ছেকেণ্ড ধৰি সক্ৰিয় হৈ আছে।
 স্ক্রীনছেইভাৰখন %d ছেকেণ্ড ধৰি সক্ৰিয় হৈ আছে।
 স্ক্রীনছেইভাৰ সক্ৰিয় হৈ আছে
 স্ক্রীনছেইভাৰ নিষ্ক্ৰিয় হৈ আছে
 স্ক্রীনছেইভাৰখন বৰ্তমান সক্ৰিয় নহয়।
 আপোনাৰ Caps Lock কি অন হৈ আছে। 