��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  t   @
     �
  �   �
  +   M  +   y  m  �  %     1   9  �   k  P   �  `   A  �   �  (   .  J   W  �   �  k   ?  �   �  Q   �  Q   �  Q   5  Q   �     �  O   �     F  R   f     �  O   �     &  R   F  _   �     �  &     +   3  %   _  A   �  %   �  B   �  ?   0  ;   p                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-07-07 17:04+0000
PO-Revision-Date: 2011-03-22 19:25+0000
Last-Translator: Nilamdyuti Goswami <ngoswami@redhat.com>
Language-Team: as_IN <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: as
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.0
 অবিকল্পিত দল, উইন্ডো নিৰ্মাণকালত নিৰ্ধাৰিত সূচক: প্ৰতি উইন্ডোৰ বাবে পৃথক দল ৰাখক আৰু ব্যৱস্থাপনা কৰক কিবৰ্ডৰ বিন্যাস কিবৰ্ডৰ বিন্যাস কিবৰ্ড বিন্যাস "%s"
স্বত্বাধিকাৰ &#169; X.Org Foundation আৰু XKeyboardConfig অৱদানকাৰীসমূহ
অনুজ্ঞাপত্ৰ সংক্ৰান্ত তথ্য জনাৰ বাবে অনুগ্ৰহ কৰি পেকেইজৰ মেটাডাটা চাওক কিবৰ্ডৰ আৰ্হি কিবৰ্ড বিকল্পসমূহ বিৰল আৰু কম ব্যবহৃত বিন্যাস আৰু বিকল্পসমূহ ল'ড কৰক অতিৰিক্ত সংৰূপ সামগ্ৰী ল'ড কৰক কিবৰ্ডৰ বিন্যাসসমূহ পূৰ্ৱদৰ্শন কৰক বিন্যাসৰ দলৰ সৈতে সূচকসমূহ সংৰক্ষণ/পুনৰসংৰক্ষণ কৰক  দ্বিতীয় দলসমূহ এপ্লেটত ফ্লেগসমূহ দেখুৱাওক বৰ্তমানে বিন্যাস সূচিত কৰিবলে এপ্লেটত ফ্লেগসমূহ দেখুৱাওক দলৰ নামৰ পৰিবৰ্তে বিন্যাসৰ নাম দেখুৱাওক দলৰ নামৰ পৰিবৰ্তে বিন্যাসৰ নাম দেখুৱাওক (অকল একাধিক বিন্যাস বিশিষ্ট XFreeৰ সংস্কৰণৰ বাবে) কিবৰ্ডৰ পূৰ্বপ্ৰদৰ্শন, X অফ-ছেট কিবৰ্ডৰ পূৰ্বপ্ৰদৰ্শন, Y অফ-ছেট কিবৰ্ডৰ পূৰ্বপ্ৰদৰ্শন, উচ্চতা কিবৰ্ডৰ পূৰ্বপ্ৰদৰ্শন, প্ৰস্থ পটভূমিৰ ৰঙ বিন্যাস সূচকৰ বাবে পটভূমিৰ ৰঙ ফন্ট পৰিয়াল বিন্যাস সূচকৰ বাবে ফন্ট পৰিয়াল ফন্টৰ আকাৰ বিন্যাস সূচকৰ বাবে ফন্টৰ আকাৰ পুৰভূমিৰ ৰঙ বিন্যাস সূচকৰ বাবে পুৰভূমিৰ ৰঙ ছবি ল'ড কৰোতে এটা ত্ৰুটি দেখা দিছিল: %s অজ্ঞাত XKB আৰম্ভ ত্ৰুটি কিবৰ্ডৰ বিন্যাস কিবৰ্ডৰ আৰ্হি বিন্যাস "%s" বিন্যাসসমূহ "%s" আৰ্হি "%s", %s আৰু %s কোনো বিন্যাস উপস্থিত নাই কোনো বিকল্প উপস্থিত নাই বিকল্প "%s" বিকল্পসমূহ "%s" 