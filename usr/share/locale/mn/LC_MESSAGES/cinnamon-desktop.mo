��          �      �       H  <   I     �     �     �  '   �  )   �  '     :   =  +   x  I   �  t   �  i   c  9   �  x    ~   �     �       *   #  Q   N  V   �  H   �  r   @  =   �  z   �  �   l  �   7	  j   �	               
                                    	              Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD.mn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-03-22 01:21+0200
Last-Translator: Badral <badral@openmn.org>
Language-Team: Mongolian <openmn-core@lists.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mn
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.0.3
 Терминал олдсонгүй. Ажиллах боломжтой байсан ч xterm хэрэглэгдэх болно Лаптоп Тодорхойгүй RANDR өргөтгөл алга байна CRTC »%d«-н тухай мэдээлэл авах боломжгүй байна »%d« гаралтын тухай мэдээллийг авах боломж алга Дэлгэцийн хэмжээг авах боломжгүй байна Дэлгэцийн тухай мэдээлэл авагдсангүй. (CRTCs, Гаралт, Нарийвчлал) CRTC »%d«-н тохиргоо олгох боломжгүй Идэвхтэй тохиргоотой таарах хадгалагдсан дэлгэцийн тохиргоо алга CRTC %d -н сонгосон байрлал/хэмжээ тогтоосон хязгаараас халижээ. Хязгаар: байрлал=(%d, %d), хэмжээ=(%d, %d), Махсимум=(%d, %d) Сонгосон виртуал хэмжээ боломжит хэмжээтэй таарахгүй байна. Хүссэн=(%d, %d), Минимум=(%d, %d), Максимум=(%d, %d) Дэлгэцийн хэмжээг асууж байхад тодорхойгүй X алдаа гарлаа 