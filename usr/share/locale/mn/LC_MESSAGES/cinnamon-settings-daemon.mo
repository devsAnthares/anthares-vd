��          D      l       �   &   �   �   �   �   <  �   �  �  m  =     �   K  �   &                              There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: mn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:18+0000
Last-Translator: Sanlig Badral <Badral@openmn.org>
Language-Team: Mongolian <openmn-translation@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Тусламж үзүүлэхэд алдаа гарлаа: %s Та Shift товчлуурыг 8 секунт даржээ. Энэ нь гарын ажиллагаанд нөлөөлдөг удаашруулах товчлуурын үйл ажиллагааны хослол юм. Та Shift товчлуурыг 5 удаа дараалуулан даржээ. Энэ нь таны гарын ажиллагаанд нөлөөлдөг наалт товчлуурын үйл ажиллагааны хослол юм. Та хоёр товчлуурыг нэг удаа эсвэл Shift товчлуур 5 удаа дараалуулан даржээ. Энэ нь таны гарын ажиллагаанд нөлөөлдөг наалт шорткатын үйл ажиллагааг унтраана. 