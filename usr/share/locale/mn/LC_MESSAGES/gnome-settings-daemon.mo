��    G      T  a   �                #     (  K   .  [   z  &   �     �  (     S   ,      �  "   �  $   �  $   �  &        5     E     W  >   ]     �  9   �     �  A   �  7   )	  8   a	  #   �	     �	     �	     �	     
     
     +
     =
     I
     Q
     W
     ^
     d
     v
     
     �
     �
     �
     �
     �
     �
     �
     �
  #        /  #   =  -   a  z   �  &   
  m   1     �     �     �     �     �  $   �  	   �  *     �   1  �   �  �   L  	   �     �          $     *  �  9  !   �  
   �     �  z   �  �   u  R        Z  J   k  �   �  9   i  i   �  ]     o   k  a   �     =  "   Z     }  l   �     �  z        �  l   �  S     J   a  E   �  &   �  %        ?  .   F  "   u  +   �     �  
   �  
   �  
   �     �  #        2  6   ?     v  '        �  6   �  
   �  /   �  ,     *   L  _   w     �  4   �  D   *  �   o  =   A  �     *        3     :     T  .   d  L   �     �  @   �  �   ?  �              !     8!  3   W!     �!  '   �!            6               C   >      7   @   "              8                        =   #      4   G       $       -      3   F   5   /         B                :               9      	   ?               .   *   2                         +      !   D       ;   (   ,         &                        %   <   '       
   0      )   E           A       1       A_vailable files: Beep Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder It seems that another application already has access to key '%u'. Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Siren Slow Keys Alert Sound Sound not set for this event. Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: mn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-08-16 00:49+0200
Last-Translator: Badral <badral@openmn.org>
Language-Team: Mongolian <openmn-translation@lists.sourceforge.net>
Language: mn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);X-Generator: KBabel 1.10.2
X-Generator: KBabel 1.10.2
 _Боломжит файлууд: Дохио Boing "%s" лавлах үүсгэж чадсангүй.
Энэ нь түүчээ өөрчилөхөд шаардлагатай. "%s" лавлах үүсгэж чадсангүй.
Энэ нь түүчээний загварыг өөрчилөхөд шаардлагатай. Хэрэглэгчийн гэр лавлахыг илрүүлж чадсангүй Жингэнэх  %s дууны файл %s жишээгээр ачаалагдсангүй Тооцоолуурыг унтуулах горимд оруулж чадсангүй.
Тооцоолуураа зөв тохируулсан эсэхээ нягтлана уу. _Энэ сануулгыг дахин бүү харуул Та удаашруулах товчлуурыг идэвхижүүлэхийг хүсч байна уу? Та наалт товчлуурыг идэвхижүүлэхийг хүсч байна уу? Та удаашруулах товчлуурыг идэвхигүйжүүлэхийг хүсч байна уу? Та наалт товчлуурыг идэвхгүйжүүлэхийг хүсч байна уу? _Бүү идэвхижүүл _Бүү идэвхигүйжүүл Түлхэх (%2$s) товчлуурт холбогдсон (%1$s)-г ажиллуулах үед алдаа гарлаа Фонтны хэлбэр GConf түлхүүр %s нь %s төрөлтэй боловч түүний тохирох төрөл нь %s байлаа
 Гэр хавтас Өөр програм хэдийнээ '%u' түлхүүрт олгогдсон бололтой байна. Шорткатын (%s) хувьд олон дахин үйлдэл тогтоох
 Шорткат (%s) нь олон удаагаар тогтоогддог
 Шорткат (%s) хэдийнээ хэрэглээнд байна
 Шорткат (%s) бүрэн бус
 Шорткат (%s) хүчингүй
 Гар Тусламж хөтчийг эхлүүлэх Вэб хөтөч эхлүүлэх modmap файлыг ачаалж байна Дэлгэц түгжих Гарах Логин Гарах Хулгана Хулганы тохируулга Дуугүй Тоглуулах (Тоглуулах/Зогсоох) Хайх Дууны файл сонгоно уу Siren Удаашруулах товчлуурын дохио Аудио Энэ үйлдэлд чимээ байхгүй Дэлгэц гамнагч эхлүүлэх Наалт товчлуурын дохио »text/plain« ба »text/*«-тодорхойлогчуудын зэрэгцүүлэх (Sync) Системийн чимээ %s файл хүчингүй wav файл байна Энэ үйлдийн хувьд дууны файл байхгүй. Энэ үйлдийн хувьд тогтоосон дууны файл алга.
Та стандарт дуугаар авахыг хүсэж байвал »gnome-audio« пакетыг суулгана уу. Тусламж үзүүлэхэд алдаа гарлаа: %s Дэлгэцийг эхлүүлэхэд алдаа гарлаа:

%s

Дэлгэцийн энэ хугацаанд ажиллахгүй. Бичих үеийн завсарлага Дуу Дуу сулруулах Дуу хаах Дуу чангаруулалтын алхам Дуу чангаруулалтын алхамын чимээний хувь Дуу чангаруулах modmap файлыг ачаалахыг хүсч байна уу? Та Shift товчлуурыг 8 секунт даржээ. Энэ нь гарын ажиллагаанд нөлөөлдөг удаашруулах товчлуурын үйл ажиллагааны хослол юм. Та Shift товчлуурыг 5 удаа дараалуулан даржээ. Энэ нь таны гарын ажиллагаанд нөлөөлдөг наалт товчлуурын үйл ажиллагааны хослол юм. Та хоёр товчлуурыг нэг удаа эсвэл Shift товчлуур 5 удаа дараалуулан даржээ. Энэ нь таны гарын ажиллагаанд нөлөөлдөг наалт шорткатын үйл ажиллагааг унтраана. _Идэвхижүлэх _Идэвхгүй болгох _Энэ мэдээг дахин бүү харуул _Ачаалах _Ачаалагдсан файлууд: 