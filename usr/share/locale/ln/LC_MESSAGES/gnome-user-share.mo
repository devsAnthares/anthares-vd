��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     #     8  $   S     x  	   �     �  2   �  _   �     =  '   V                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-03-02 11:27+0100
Last-Translator: René Manassé GALEKWA <renemanasse@gmail.com>
Language-Team: Lingala <ubuntu-cd@lists.ubuntu.com>
Language: ln
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Fichier polele ya %s fichier polele ya %s na %s Tinda fichier oyo okaboli soki ezali Fichier oyo okaboli Ko kabola Lolenge ya kokabola Pesa ndingisa na rézo mpo na fichier oyo okaboli. Soki etuni yo mot de passe na yo.Okoki kotia kaka "mokolo moko te","ekomama", na "mpo na seko". Soki esengi mot de passe Kabola;fichiers;http;rezo;copier;tinda; 