��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     �	  	   �	     �	     
  -   &
     T
  	   j
  %   t
  
   �
  -   �
  2   �
  
     $        6  [   N     �     �     �     �     �  )   �     '  	   *     4     R  U   k  9   �     �  $        )  !   :     \     m     �     �  @   �     �     �                    )     ?     R     Z                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-26 17:51+0000
Last-Translator: Andre Tippel <andre.tippel@googlemail.com>
Language-Team: Low German <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Zugriff immer erlauben Authorisierungsanfrage von %s Bluetooth Bluetooth Einstellungen Bluetooth ist deaktiviert Bluetooth durch Hardware-Schalter deaktiviert Durchsuche Dateien... Abbrechen Bluetooth Einstellungen Konfigurieren Verbindung Gerät %s möchte Zugriff auf den Dienst '%s' Gerät %s möchte sich mit dem Computer verbinden. Passt nich Fehler beim Durchsuchen des Gerätes Nur dieses mal erlauben Wenn Sie das Gerät entfernen, müssen Sie es vor dem nächsten Gebrauch neu konfigurieren. Tastatureinstellungen Passt Mauseinstellungen Maus und Touchpad Einstellungen Nein Keine internen Bluetooth-Geräte gefunden OK Gekoppelt Kopplungsbestätigung für %s Kopplungsanfrage für %s Bitte bestätigen Sie ob der PIN '%s' dem, auf dem Gerät angezeigten PIN entspricht. Bitte geben Sie den auf Ihrem Gerät angezeigten PIN ein. Ablehnen "%s" von der Geräteliste entfernen? Gerät entfernen Dateien zum Gerät übertragen... Sende Dateien... Neues Gerät konfigurieren Neues Gerät konfigurieren... Klangeinstellungen Das angefragte Gerät kann nicht durchsucht werden (Fehler '%s') Typ Sichtbar Sichtbar von "%s" Ja verbinden... Verbindung trennen... Gerät deaktiviert Seite 1 Seite 2 