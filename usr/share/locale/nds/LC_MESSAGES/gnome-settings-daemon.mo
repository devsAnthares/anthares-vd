��    7      �  I   �      �     �     �     �     �     �  
   �       	        $  
   5     @     P     b     q     �     �     �     �     �     �     �     �     �     �     �     �     	          1     E     X     ]     i  
   q     |     �     �  
   �     �     �     �     �     �     �     �  �   �     �     �  
   �  	   �     �     �     �     �  �  �     �	     �	     �	  	   
     
     
     '
     9
     H
     ]
     i
     y
     �
     �
  
   �
     �
     �
     �
     �
  
   �
                    '     3     E     T     l     �     �     �     �     �     �     �     �     �       
               !   &     H     X     ]  �   i  	             )  
   6     A     N     b     i               4   %                         1   -       '                 "      &   !           5   )      +   2   (      
            /                  $             *      0      	               ,   7      6           #                           .   3          %u Input %u Inputs %u Output %u Outputs Accessibility Activate Allowed keys Background Background plugin Clipboard Clipboard plugin Deactivate Do_n't activate Do_n't deactivate Don't activate Don't deactivate Dummy Dummy plugin Eject Empty Trash Emptying the trash Font Font plugin From:  Home folder Keyboard Keyboard plugin Launch calculator Launch email client Launch help browser Launch media player Launch web browser Left Lock screen Log out Media keys Media keys plugin Mouse Mouse Preferences Mouse keys Mouse plugin Normal Right Screen magnifier Screen reader Search System Sounds The display will be reset to its previous configuration in %d second The display will be reset to its previous configuration in %d seconds Upside Down Use screen _reader X Settings _Activate _Deactivate _Empty Trash _Load _Loaded files: Project-Id-Version: gnome-settings-daemon master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-settings-daemon&component=general
PO-Revision-Date: 2009-11-28 03:59+0100
Last-Translator: Nils-Christoph Fiedler <fiedler@medienkompanie.de>
Language-Team: Low German <nds-lowgerman@lists.sourceforge.net>
Language: nds
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
plural-Forms: nplurals=2; plural=(n!=1);
 %u Ingav %u Ingaven %u Utgav %u Utgaven Tongangelikheit Aktiveren Tolaten Knöppe Achtergrund Achtergrundplugin Twüschenavlag Twüschenavlagplugin Deaktiveren _Nich aktiveren _Nich deaktiveren Nich aktiveren nich deaktiveren Frieholler Friehollerplugin Utwarfen Papierkörv leeren Leere de Papierkörv Schriftart Schriftartplugin Vun: Hemverteeknis Knöppboord Knöppboordplugin Rekner starten E-Post Programm starten Hölpkieker starten Medienspeeler starten Netkieker starten Links Billschirm avsluten Avmellen Medienknöppe Medienknöppeplugin Muus Muusinstellens Muusknöppe Muusplugin Normal Rechts Billschirmgrötenännernsprogramm Billschirmleser Sök Systemtöne De Billschirmoplösen warrt torüggsett to de Standardinstellens in %d Sekunn De Billschirmoplösen warrt torüggsett to de Standardinstellens in %d Sekunnen Koppöver _Billschirmleser bruken X Instellens _Aktiveren _Deaktiveren Papierkörv _leeren _Laden _Laden Dateien: 