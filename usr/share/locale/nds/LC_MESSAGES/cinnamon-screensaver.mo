��          �      �       H  E   I     �  &   �     �     �  `   �     T     o  )   �  *   �     �     �         #  F   +     r  6   �  #   �     �  t   �      d  "   �  *   �  "   �  
   �                                	                                    
       If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Switch User The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&component=general
PO-Revision-Date: 2017-03-20 21:18+0000
Last-Translator: Sebastian Bohmholt <Unknown>
Language-Team: Low German <nds-lowgerman@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Falls der Bildschirmschoner aktiv ist, dann deaktivieren Sie ihn bitte Passwort falsch Meldung welche im gesperrten Bildschirm angezeigt wird Bitte geben Sie ihr Passwort ein... Benutzer wechseln Der Bildschirmschoner ist für %d Sekunde aktiv gewesen.
 Der Bildschirmschoner ist für %d Sekunden aktiv gewesen.
 Der Bildschirmschoner ist aktiv
 Der Bildschirmschoner ist inaktiv
 De Billschirmschoner is grade nich aktiv.
 Schaltet den Bildschirmschoner ein Entsperren Version dieses Programms De CapsLock-Knopp is an. 