��          D      l       �   &   �   �   �   �   <  �   �  �  m  +   /  �   [  �     �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:31+0000
Last-Translator: Zuza Software Foundation <Unknown>
Language-Team: Northern Sotho <translate-discuss-nso@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Launchpad-Export-Date: 2018-06-26 10:18+0000
X-Generator: Launchpad (build 18688)
 Go bile le phošo ya go bontšha thušo: %s O sa tšwa go gatelela senotlelo sa Shift metsotswana e 8.  Ye ke kgaoletšo ya sebopego sa Dinotlelo tše Nanyago, yeo e amago tsela yeo boroto ya gago ya dinotlelo e šomago ka yona. O sa tšwa go kgotla senotlelo sa Shift makga a 5 ka tatelano.  Ye ke kgaoletšo ya sebopego sa Dinotlelo tše Kgomarelago, yeo e amago tsela yeo ka yona boroto ya gago ya dinotlelo e šomago ka yona. O sa tšwa go gatelela dinotlelo tše pedi ka nako e tee, goba o gateletše senotlelo sa Shift makga a 5 ka tatelano.  Se se tima sebopego sa Dinotlelo tše Kgomarelago, seo se amago tsela yeo ka yona boroto ya gago ya dinotlelo e šomago ka yona. 