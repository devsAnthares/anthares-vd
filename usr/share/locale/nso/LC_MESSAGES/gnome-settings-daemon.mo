��    0      �  C         (  K   )  &   u  (   �  S   �  "     $   <  $   a  &   �     �  >   �     �  9   �     1  7   =  8   u  #   �     �     �               ,     ?     K     S     Y     k     �     �     �     �     �  #   �  #   �  -   	  &   7  m   ^     �     �     �     �     �  $   	  	   )	  �   3	  �   �	  �   N
     �
  �    q   �  .     A   ;  j   }  0   �  4     3   N  7   �     �  Q   �       i        �  R   �  Z   �  7   C  /   {  1   �     �     �          /     @  
   H     S  #   p     �     �  !   �     �  %   �  X     )   _  )   �  +   �  v   �     V     n     u     �     �  /   �     �  �   �  �   �  �   r  %   j     *   '      +       	                          $   .                 )         !   (       
             -      "                  /           ,              0                                                  &   %             #          Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot determine user's home directory Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Lock screen Log out Mouse Mouse Preferences Play (or play/pause) Search Select Sound File Slow Keys Alert Sound Sticky Keys Alert Sync text/plain and text/* handlers The file %s is not a valid wav file The sound file for this event does not exist. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Do not show this message again Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2004-11-27 17:02+0200
Last-Translator: Zuza Software Foundation <info@translate.org.za>
Language-Team: Northern Sotho <translate-discuss-nso@lists.sourceforge.net>
Language: nso
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
 E ka se kgone go hlama tšhupetšo ya "%s".
Se se a nyakega go dumelela go fetola leswao le bontšhago mo o lego. E ka se lemoge tšhupetšo ya gae ya modiriši E ka se kgone go laiša faele ya modumo ya %s bjalo ka mohlala %s E ka se kgone go robatša motšhene.
Tiišetša gore motšhene o fetotšwe sebopego ka mo go nepagetšego. Na o nyaka go diragatša Dinotlelo tše Nanyago? Na o nyaka go diragatša Dinotlelo tše Kgomarelago? Na o nyaka go diragatšolla Dinotlelo tše Nanyago? Na o nyaka go diragatšolla Dinotlelo tše Kgomarelago? Ntšha Phošo ya ge go lekwa go dirišwa (%s)
yeo e kgokagantšwego le senotlelo sa (%s) Fonto Senotlelo sa GConf sa %s se beakantšwe go mohuta wa %s eupša mohuta wa sona o letetšwego e be e le %s
 Sephuthedi sa gae Go Kopanya ga Senotlelo ga (%s) go na le mogato o hlaloswago e le makga a mantši
 Go Kopanya ga Senotlelo ga (%s) go na le go kopanya mo go hlaloswago e le makga a mantši
 Go Kopanya ga Senotlelo ga (%s) go šetše go dirišwa
 Go Kopanya ga Senotlelo ga (%s) ga go a felela
 Go Kopanya ga Senotlelo ga (%s) ga se ga kgonthe
 Boroto ya dinotlelo Tsebagatša sefetleki sa thušo Tsebagatša sefetleki sa wepe Notlela sekirini E-tšwa Legotlwana Tše Ratwago tša Legotlwana Bapala (goba bapala/emiša nakwana) Nyakišiša Kgetha Faele ya Modumo Temošo ya Dinotlelo tše Nanyago Modumo Temošo ya Dinotlelo tše Kgomarelago text/plain (sengwalwa/se se nago selo) e rulagantšwego le baswari ba text/* (sengwalwa) Faele ya %s ga se faele ya kgonthe ya wav Faele ya modumo ya tiragalo ye ga e gona. Go bile le phošo ya go bontšha thušo: %s Go bile le phošo ya go thoma seboloki sa sekirini:

%s

Go šoma ga seboloki sa sekirini go ka se šome lenaneong le. Go Khutša ga go Tlanya Bolumo Bolumo e tlase Go nolofatšwa ga bolumo Peakanyo ya bolumo Peakanyo ya bolumo go ya ka phesente ya bolumo. Bolumo e godimo O sa tšwa go gatelela senotlelo sa Shift metsotswana e 8.  Ye ke kgaoletšo ya sebopego sa Dinotlelo tše Nanyago, yeo e amago tsela yeo boroto ya gago ya dinotlelo e šomago ka yona. O sa tšwa go kgotla senotlelo sa Shift makga a 5 ka tatelano.  Ye ke kgaoletšo ya sebopego sa Dinotlelo tše Kgomarelago, yeo e amago tsela yeo ka yona boroto ya gago ya dinotlelo e šomago ka yona. O sa tšwa go gatelela dinotlelo tše pedi ka nako e tee, goba o gateletše senotlelo sa Shift makga a 5 ka tatelano.  Se se tima sebopego sa Dinotlelo tše Kgomarelago, seo se amago tsela yeo ka yona boroto ya gago ya dinotlelo e šomago ka yona. _O seke wa bontšha molaetša wo gape 