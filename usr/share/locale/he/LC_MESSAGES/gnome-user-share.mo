��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  Y  w  '   �  /   �  7   )     a  
   �     �  f   �  Q   
     \  3   v                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: 1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-30 10:27+0300
Last-Translator: Yosef Or Boczko <yoseforb@gnome.org>
Language-Team: עברית <>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 2.91.7
Plural-Forms: nplurals=2; plural= (n!=1);
 הקבצים הציבוריים של %s הקבצים הציבוריים של %s על %s טעינת שיתוף קבצים אישי אם פעיל שיתוף קבצים אישי שיתוף הגדרות שיתוף יש להפעיל שיתוף קבצים פרטיים לשיתוף תוכן תיקייה זו ברשת. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords שיתוף;קבצים;רשת;העתקה;שליחה; 