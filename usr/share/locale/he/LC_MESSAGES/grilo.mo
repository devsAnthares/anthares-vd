��    &      L  5   |      P     Q  '   q     �  "   �     �  ,   �  .   "  !   Q     s  $   �     �  #   �     �               )     9  !   P      r  8   �  &   �     �       '   ,     T     l     �     �  ;   �     �  2        E     X  !   w  7   �  (   �     �  e       u	  '   �	     �	  "   �	     �	  ,   
  .   F
  !   u
     �
  $   �
     �
  #   �
          0     >     M     ]  !   t      �  8   �  &   �          0  '   P     x     �     �     �  ;   �       2   6     i     |  !   �  7   �  (   �                        
   !       %                                               	                     #                             &                          $                  "        '%s' is not a valid plugin file All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Colon-separated list of Grilo plugins to use Colon-separated paths containing Grilo plugins Could not access mock content: %s Could not find mock content %s Could not resolve media for URI '%s' Data not available Failed to initialize plugin from %s Failed to load plugin from %s Grilo Options Invalid URL %s Invalid path %s Invalid plugin file %s Invalid request URI or header: %s Media has no 'id', cannot remove Metadata key '%s' already registered in different format Metadata key '%s' cannot be registered No mock definition found No searchable sources available None of the specified keys are writable Operation was cancelled Plugin '%s' already exists Plugin '%s' is already loaded Plugin '%s' not available Plugin configuration does not contain 'plugin-id' reference Plugin not found: '%s' Semicolon-separated paths containing Grilo plugins Show Grilo Options Some keys could not be written Source with id '%s' was not found The entry has been modified since it was downloaded: %s The requested resource was not found: %s Unhandled status: %s Project-Id-Version: grilo master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-12-31 17:28+0200
Last-Translator: Yosef Or Boczko <yoseforb@gmail.com>
Language-Team: עברית <>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.6
 '%s' is not a valid plugin file All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Colon-separated list of Grilo plugins to use Colon-separated paths containing Grilo plugins Could not access mock content: %s Could not find mock content %s Could not resolve media for URI '%s' Data not available Failed to initialize plugin from %s Failed to load plugin from %s Grilo Options Invalid URL %s Invalid path %s Invalid plugin file %s Invalid request URI or header: %s Media has no 'id', cannot remove Metadata key '%s' already registered in different format Metadata key '%s' cannot be registered No mock definition found No searchable sources available None of the specified keys are writable Operation was cancelled Plugin '%s' already exists Plugin '%s' is already loaded Plugin '%s' not available Plugin configuration does not contain 'plugin-id' reference Plugin not found: '%s' Semicolon-separated paths containing Grilo plugins Show Grilo Options Some keys could not be written Source with id '%s' was not found The entry has been modified since it was downloaded: %s The requested resource was not found: %s Unhandled status: %s 