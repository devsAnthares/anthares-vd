��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  Y  >  $   �     �     �  "   �  %         6  "   W  *   z  6   �  "   �     �  )         5      V     w     �  �   �     q     �  ,   �     �     �  4   �  3   %     Y     j  7   �  (   �  a   �     F     e     �     �     �  -   �        (        9  &   G     n  -   �  (   �  .   �  /   	  1   9     k     y     �     �     �  !   �     �     	       '   3                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-02-27 22:56+0200
PO-Revision-Date: 2011-02-27 22:57+0200
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>
Language-Team: he
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 הפעלת תוספים נוספים תוספים _פעילים: הוספת תוסף סגירת תיבת הדו־שיח הגדרת את התוסף הנבחר כיבוי התוסף הנבחר הקטנת עדיפות התוסף Default group, assigned on window creation הפעלה/כיבוי של תוספים מותקנים הגדלת עדיפות התוסף מחוון: Keep and manage separate group per window תוספי מחוון מקלדת תוספי מחוון מקלדת פריסת מקלדת Keyboard layout פריסת מקלדת "%s"
כל הזכויות שמורות &#169; קרן X.Org ותורמי XKeyboardConfig
למידע על אודות הרישוי ניתן לעיין בקבצים המצורפים לחבילה Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items אין תיאור. הצגה מקדימה של פריסות המקלדת Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator רשימת התוספים הפעילים The list of enabled Keyboard Indicator plugins ארעה שגיאה בטעינת תמונה: %s לא ניתן לפתוח את קובץ העזרה לא ידוע שגיאה בהפעלת XKB תוספים _זמינים: keyboard layout keyboard model פריסה "%s" פריסות "%s" דגם "%s", %s ו־%s אין פריסה אין אפשרויות אפשרות "%s" אפשרויות "%s" 