��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     
  
   #
     .
     1
     7
     @
  2   L
  7   
  Y   �
  X        j     q  
   �  !   �  (   �  %   �  *     -   /  4   ]  +   �  H   �  2     T   :  F   �  �   �  7   �  �   �  t   ~  M   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-04 15:48+0200
Last-Translator: Gabor Kelemen <kelemeng at gnome dot hu>
Language-Team: Hungarian <gnome-hu-list at gnome dot org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.11.4
 %A %B %e, %k.%M %A %B %e, %k.%M.%S %A %B %e, %p %l.%M %A %B %e, %p %l.%M.%S %B %e., %A %R %R.%S %l.%M %p %l.%M.%S %p A(z) %d. CRTC nem képes %s kimenet meghajtására A(z) %d. CRTC nem támogatja a(z) %s forgatásértéket %d. CRTC: %dx%d@%dHz mód kipróbálása a(z) %dx%d@%dHz módú kimenettel (%d. lépés)
 Nem található terminál, az xterm lesz használva, még ha az esetleg nem is működik Laptop Tükrözött kijelzők Ismeretlen A RANDR kiterjesztés nincs jelen Módok kipróbálása a(z) %d. CRTC-hez
 nem lehet klónozni a(z) %s kimenetre nem rendelhetők CRTC-k a kimenetekhez:
%s Nem kérhető információ a(z) %d. CRTC-ről Nem lehet információkat lekérni a kimenetről: %d Nem kérhető le a képméretek tartománya A képernyő erőforrásai nem kérhetők le (CRTC-k, kimenetek, módok) Nem állítható be a(z) %d. CRTC konfigurációja A mentett képernyő-beállítások egyike sem felel meg az aktív konfigurációnak a kijelölt módok egyike sem kompatibilis a lehetséges módokkal:
%s %s kimenet nem rendelkezik egy másik klónozott kimenetével egyező paraméterekkel:
létező mód = %d, új mód = %d
létező koordináták = (%d, %d), új koordináták = (%d, %d)
létező forgatás = %s, új forgatás = %s A(z) %s kimenet nem támogatja ezt a módot: %dx%d@%dHz A kért pozíció/méret a(z) %d. CRTC-hez kívül esik az engedélyezett korláton: pozíció=(%d, %d), méret=(%d, %d), maximum=(%d, %d) A kért virtuális méret nem illeszkedik az elérhető méretre: kért=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Kezeletlen X hiba történt a képméretek tartományának lekérése közben 