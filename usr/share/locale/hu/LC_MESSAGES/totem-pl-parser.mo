��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       r     z     �     �     �  <   �  0   �     &                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-10-19 05:17+0200
Last-Translator: Gabor Kelemen <kelemeng at ubuntu dot com>
Language-Team: Hungarian <openscope at googlegroups dot com>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms:  nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 Hang CD Blu-ray DVD Digitális televízió %s csatolása meghiúsult. Nincs adathordozó a meghajtóban a(z) „%s” eszközhöz. Ellenőrizze, hogy a lemez a meghajtóban van-e. Video CD 