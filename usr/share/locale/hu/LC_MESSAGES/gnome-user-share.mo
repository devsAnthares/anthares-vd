��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     d     {  8   �     �  
   �     �  \     �   q  (   �  5   '                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-31 18:50+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <openscope at googlegroups dot com>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.2
Plural-Forms:  nplurals=2; plural=(n != 1);
 %s nyilvános fájljai %s nyilvános fájljai ezen: %s Személyes fájlmegosztás indítása, ha engedélyezett Személyes fájlmegosztás Megosztás Megosztás beállításai Személyes fájlmegosztás bekapcsolása a mappa tartalmának megosztásához a hálózaton. Mikor történjen a jelszavak bekérése. A lehetséges értékek: „never” (soha), „on_write” (íráskor) és „always” (mindig). Mikor történjen a jelszavak bekérése megosztás;fájlok;http;hálózat;másolás;küldés; 