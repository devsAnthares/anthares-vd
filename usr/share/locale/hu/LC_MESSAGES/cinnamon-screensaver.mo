��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  (        +  L   ;     �  -   �     �  0   �  .        ?  Q   U  W   �     �  "     %   A  <   g  	   �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-29 10:39+0000
Last-Translator: KAMI <kami911@gmail.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: hu
 A képernyővédő szabályos bezárása Ellenőrzés… Ha a képernyővédő fut, akkor állítsa le (képernyő visszaállítása) Helytelen jelszó Üzenet megjelenítése a zárolt képernyőn Adja meg a jelszót… A képernyővédő futásidejének lekérdezése A képernyővédő állapotának lekérdezése Felhasználóváltás A futó képernyővédő-folyamatot utasítja a képernyő azonnali zárolására A képernyővédő %d másodpercig futott.
 A képernyővédő %d másodpercig futott.
 A képernyővédő éppen fut
 A képernyővédő éppen nem fut
 A képernyővédő jelenleg nem fut.
 Képernyővédő bekapcsolása (képernyő elsötétítése) Feloldás Ezen alkalmazás verziója A Caps Lock be van kapcsolva. 