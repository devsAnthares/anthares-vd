��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  v  �  X   .  C   �  J   �  8     -   O      }      �  ,   �  k   �     X               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: kelemeng <kelemeng@gnome.hu>
Language-Team: Hungarian <openscope@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Hitelesítés szükséges a bejelentkező képernyő beállításainak módosításához Hitelesítés szükséges a felhasználói adatok módosításához Hitelesítés szükséges a saját felhasználói adatai módosításához Bejelentkező képernyő beállításainak módosítása Saját felhasználói adatainak módosítása Hibakereső kód engedélyezése Felhasználói fiókok kezelése Verzióinformációk kiírása és kilépés D-Bus felületeket biztosít felhasználói fiókok információinak\n lekérdezéséhez és kezeléséhez. Létező példány cseréje 