��          �      l      �  #   �       S      7   t  D   �  J   �     <     P      o     �     �  4   �  (   �  #     &   7  9   ^     �     �     �  /   �  o    4   �  #   �  i   �  @   E  b   �  n   �  '   X  (   �  (   �     �      �  D   	  ?   R	  0   �	  8   �	  M   �	      J
     k
     �
  ;   �
                       
                                                       	                    Admin name or password is not valid Already joined to a domain Authentication is required to change the policy of who can log in on this computer. Authentication is required to discover a kerberos realm Authentication is required to join this machine to a realm or domain Authentication is required to remove this computer from a realm or domain. Change login policy Configured command invalid: %s Configured command not found: %s Discover realm Join machine to realm LDAP on this system does not support UDP connections Need credentials for leaving this domain Not currently joined to this domain Process was terminated with signal: %d Received invalid or unsupported Netlogon data from server Remove machine from realm Skipped command: %s The operation was cancelled Unsupported or unknown membership software '%s' Project-Id-Version: realmd
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-10-20 19:12+0000
Last-Translator: Stef Walter <stefw@gnome.org>
Language-Team: Hungarian (http://www.transifex.com/freedesktop/realmd/language/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Az adminisztrátor neve vagy jelszava nem érvényes Már csatlakozott egy tartományhoz Hitelesítés szükséges a házirend módosításához, hogy ki jelentkezhet be erre a számítógépre. Hitelesítés szükséges a kerberos realm feltérképezéséhez Hitelesítés szükséges ennek a gépnek a realmhoz vagy tartományhoz való csatlakoztatásához Hitelesítés szükséges ennek a számítógépnek a realmból vagy tartományból való eltávolításához. Bejelentkezési házirend módosítása A beállított parancs érvénytelen: %s Nem található beállított parancs: %s A realm feltérképezése Gép csatlakoztatása a realmhoz Az ezen a rendszeren lévő LDAP nem támogatja az UDP kapcsolatokat Hitelesítési adatok szükségesek a tartomány elhagyásához Jelenleg nincs csatlakozva ehhez a tartományhoz A folyamat a következő szignállal fejeződött be: %d Érvénytelen vagy nem támogatott Netlogon adat érkezett a kiszolgálótól Gép eltávolítása a realmból Kihagyott parancs: %s A műveletet megszakították Nem támogatott vagy ismeretlen tagsági szoftver: „%s” 