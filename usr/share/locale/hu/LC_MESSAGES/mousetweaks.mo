��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  ,   �     �  (   �  #   �  !        4     T     e  &   m  1   �     �  #   �     		     	     -	  /   9	  	   i	  $   s	     �	  (   �	     �	     �	  u   	
  $   
  0   �
     �
     �
  /   �
  :   %     `                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&keywords=I18N+L10N&component=general
PO-Revision-Date: 2012-02-29 01:56+0100
Last-Translator: Gabor Kelemen <kelemeng at gnome dot hu>
Language-Team: Magyar <gnome-hu-list at gnome dot org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
X-Generator: Lokalize 1.0
Plural-Forms:  nplurals=2; plural=(n != 1);
 – GNOME egér-akadálymentesítési démon Gombstílus Gombstílus a Kattintástípus ablakban. Kattintástípus ablak geometriája Kattintástípus ablak tájolása Kattintástípus ablak stílusa Dupla kattintás Húzás Elidőzési kattintás engedélyezése Szimulált másodlagos kattintás engedélyezése A súgó nem jeleníthető meg A Kattintástípus ablak elrejtése Vízszintes Rámutatási kattintás Csak ikonok Kis mutatómozgások figyelmen kívül hagyása Tájolás A Kattintástípus ablak tájolása. Másodlagos kattintás Az aktív elidőzési mód beállítása A Mousetweaks leállítása Egyszeres kattintás A Kattintástípus ablak mérete és pozíciója. A formátum az X ablakkezelő szabványos geometria-karakterlánca. A Mousetweaks indítása démonként A Mousetweaks indítása bejelentkezési módban Szöveg és ikonok Csak szöveg Várakozás ideje elidőzési kattintás előtt Várakozás ideje szimulált másodlagos kattintás előtt Függőleges 