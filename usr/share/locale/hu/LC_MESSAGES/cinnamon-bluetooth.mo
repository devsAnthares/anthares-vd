��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 s  %     �	     �	  ,   �	  	   �	     �	     
  )   !
     K
     b
  &   i
  	   �
  M   �
  <   �
     %  .   1      `  ]   �     �  
   �       '        C  $   J     o     r  +   ~  (   �  N   �  ,   "     O  A   \     �  !   �     �     �          $  9   7     q     x     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: cinnamon-bluetooth
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-02-01 21:00+0000
Last-Translator: Vörös László <vlacko0930@gmail.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: hu
 Cím Mindig biztosít hozzáférést Hitelesítési kérés a következőhöz: %s Bluetooth Bluetooth beállítások Bluetooth letiltva Bluetooth hardveres kapcsolóval letiltva Fájlok tallózása… Mégse Bluetooth beállítások módosítása Kapcsolat A(z) %s eszköz hozzáférést kér a következő szolgáltatáshoz: „%s” A(z) %s eszköz párosítást kér ehhez a számítógéphez Nem egyezik Hiba lépett fel az eszköz tallózása során Egyszer biztosít hozzáférést Ha eltávolítja az eszközt, a következő használat előtt újra be kell majd állítania. Billentyűzet-beállítások Találatok Egérbeállítások Egér és érintőtábla beállításai Tiltva Nem találhatók Bluetooth-adapterek OK Párosítva Párosítási kérés a következőhöz: %s Párosítás kérés a(z) %s eszköztől Ellenőrizze, hogy a „%s” PIN-kód megegyezik-e az eszköz PIN-kódjával. Adja meg az eszközön olvasható PIN-kódot Elutasítás Eltávolítja a(z) „%s” eszközt az eszközök listájából? Eszköz eltávolítása Fájlok küldése az eszközre… Fájlok küldése… Új eszköz beállítása Új eszköz beállítása… Hangbeállítások A megadott eszköz nem tallózható. A hiba oka: „%s” Típus Láthatóság „%s” láthatósága Igen kapcsolódás… kapcsolat bontása… kapcsolóval kikapcsolva 1. oldal 2. oldal 