��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �               .     =     L     ^     s     |  2   �  7   �  Y   �  (   M	     v	  %   �	  *   �	  F   �	  �   
  7     t   9                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-31 14:15+0200
Last-Translator: Meskó Balázs <mesko.balazs@fsf.hu>
Language-Team: Hungarian <gnome-hu-list at gnome dot org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.8
Plural-Forms: nplurals=2; plural=(n != 1);
 %R %R.%S %a %k.%M %a %R.%S %a %b %e_%k.%M %a %b %e_%R.%S %a %b %e_%p %l.%M %a %b %e_%p %l.%M.%S %a %l.%M %p %a %l.%M.%S %p %a %b %e_%k.%M %a %b %e_%R.%S %a %b %e_%p %l.%M %a %b %e_%p %l.%M.%S %l.%M %p %l.%M.%S %p A(z) %d. CRTC nem képes %s kimenet meghajtására A(z) %d. CRTC nem támogatja a(z) %d forgatásértéket %d. CRTC: %dx%d@%dHz mód kipróbálása a(z) %dx%d@%dHz módú kimenettel (%d. lépés)
 Módok kipróbálása a(z) %d. CRTC-hez
 Nincs megadva nem lehet klónozni a(z) %s kimenetre nem rendelhetők CRTC-k a kimenetekhez:
%s a kijelölt módok egyike sem kompatibilis a lehetséges módokkal:
%s %s kimenet nem rendelkezik egy másik klónozott kimenetével egyező paraméterekkel:
létező mód = %d, új mód = %d
létező koordináták = (%d, %d), új koordináták = (%d, %d)
létező forgatás = %d, új forgatás = %d A(z) %s kimenet nem támogatja ezt a módot: %dx%d@%dHz A kért virtuális méret nem illeszkedik az elérhető méretre: kért=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 