��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  A   �
     �
  :   �
     
     !  �   8     �     �  G     .   ]  $   �  B   �     �  -   
  \   8  -   �  e   �  '   )  '   Q  &   y  (   �     �      �     �  )        <     J     i      z  .   �  
   �     �     �       %        @     [     k  +   �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-07-28 21:05+0000
PO-Revision-Date: 2016-08-05 00:03+0200
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <gnome at fsf dot hu>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms:  nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
 Az ablak létrehozásához hozzárendelt alapértelmezett csoport Jelző: Külön csoport fenntartása és kezelése minden ablakhoz Billentyűzetkiosztás Billentyűzetkiosztás „%s” billentyűzetkiosztás
Copyright &#169; X.Org Foundation és az XKeyboardConfig közreműködői
Licencelési információkért tekintse meg a csomag metaadatait. Billentyűzetmodell Billentyűzet beállításai Egzotikus, ritkán használt kiosztások és beállítások betöltése Kiegészítő beállítási elemek betöltése Billentyűzetkiosztások előnézete Jelzők mentése/visszaállítása a kiosztáscsoportokkal együtt Másodlagos csoportok Zászlók megjelenítése a kisalkalmazásban Zászlók megjelenítése a kisalkalmazásban a jelenlegi billentyűzetkiosztás jelzésére Kiosztásnevek mutatása csoportnevek helyett Kiosztásnevek mutatása csoportnevek helyett (csak az XFree több kiosztást támogató verzióihoz) A billentyűzet előnézete, X eltolás A billentyűzet előnézete, Y eltolás A billentyűzet előnézete, magasság A billentyűzet előnézete, szélesség A háttérszín A kiosztásjelző háttérszíne A betűkészlet-család A kiosztásjelző betűkészlet-családja A betűméret A kiosztásjelző betűmérete Az előtérszín A kiosztásjelző előtérszíne hiba történt egy kép betöltése során: %s Ismeretlen XKB-előkészítési hiba billentyűzetkiosztás billentyűzetmodell „%s” kiosztás „%s” kiosztás „%s” típus, %s és %s nincs kiosztás nincsenek beállítások „%s” beállítás „%s” beállítás preferences-desktop-keyboard 