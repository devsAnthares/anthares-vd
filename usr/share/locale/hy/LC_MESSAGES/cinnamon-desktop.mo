��          �      �       H  <   I     �     �     �  '   �  )   �  '     :   =  +   x  I   �  t   �  i   c  9   �  I    �   Q     �     �  4   �  M   *  Y   x  E   �  t     D   �  �   �  �   }  �   D	  h   
               
                                    	              Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.HEAD.hy
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-03-10 10:13+0400
Last-Translator: Narine Martirosyan <training@instigate.am>
Language-Team:  <norayr@arnet.am>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
 Նույնիսկ չաշխատելու դեպքում, օգտագործելով xterm–ը, չի կարող գտնել տերմինալը  Լափթոփ Անհայտ RANDR ընդլայնումը բացակայում է չի կարող CRTC %d–ի մասին տեղեկություն ստանալ չի կարող գտնել տեղեկություն արտածում  %d–ի մասին  չի կարող գտնել էկրանի չափերի տիրույթը չի կարող գտնել էկրանի պաշարները (CRTC–ներ, արտածումներ, ռեժիմներ) չի կարող կոնֆիգուրացնել CRTC %d–ի համար պահպանված ցուցադրվող կոնֆիգուրացումներից ոչ մեկը չի համապատասխանում ակտիվ կոնֆիգուրացմանը պահանջված դիրքը/չափսը CRTC %d–ի համար թույլատրելի սահմաններից դուրս է. դիրք=(%d, %d), չափս=(%d, %d), առավելագույն=(%d, %d)  պահանջված վիրտուալ չափը չի համապատասխանում հասանելի չափին. հարցված=(%d, %d), նվազագույն=(%d, %d), առավելագույն=(%d, %d) չգտնված X սխալ էկրանի չափերի տվյալները գտնելու ընթացքում 