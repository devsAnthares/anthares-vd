��    G      T  a   �                #     (  K   .  [   z  &   �     �  (     S   ,      �  "   �  $   �  $   �  &        5     E     W  >   ]     �  9   �     �  A   �  7   )	  8   a	  #   �	     �	     �	     �	     
     
     +
     =
     I
     Q
     W
     ^
     d
     v
     
     �
     �
     �
     �
     �
     �
     �
     �
  #        /  #   =  -   a  z   �  &   
  m   1     �     �     �     �     �  $   �  	   �  *     �   1  �   �  �   L  	   �     �          $     *  s  9     �     �  
   �  W   �  l   (  :   �     �  .   �  _     '   p  !   �  -   �  %   �  1     
   @     K     Z  T   a     �  [   �     !  8   =  1   v  4   �     �     �       
   9     D     d     �     �     �     �  	   �     �     �     �     �            	   (     2     N  0   R     �  '   �  <   �       $     /   6  �   f  1   �  r        �     �     �     �     �  $   �       )     �   6  �   �  �   �     B  	   K  #   U     y     �            6               C   >      7   @   "              8                        =   #      4   G       $       -      3   F   5   /         B                :               9      	   ?               .   *   2                         +      !   D       ;   (   ,         &                        %   <   '       
   0      )   E           A       1       A_vailable files: Beep Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder It seems that another application already has access to key '%u'. Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Siren Slow Keys Alert Sound Sound not set for this event. Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-05-23 22:19+0300
Last-Translator: Thierry Randrianiriana <randrianiriana@gmail.com>
Language-Team: Malagasy <i18n-malagasy-gnome@gna.org>
Language: mg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
 Ireo rakitra _misy: Bip Fanakorana Tsa afaka mamorona ny laha-tahiry "%s".
Ilaina izy io mba ahafahana manova ny kitondro. Tsy afaka mamorona ny laha-tahiry"%s".
Ilaina izy io mba ahafahana manova ny endriky ny kitondron'ny totozy. Tsy afaka namaritra ny laha-tahiry fandraisan'ny mpampiasa Feo mikarantsana Tsy afaka naka ny raki-peo %s hatao sombiny %s Tsy afaka nampiato ny solosaina.
Amarino hoe voakirakira araka ny tokony ho izy ilay solosaina. Aza aseho _intsony ity fampilazana ity. Tianao alefa ve ny kitendry meda? Tianao alefa ve ny kitendry miraikidraikitra? Tianao atsahatra ve ny kitendry meda? Tianao atsahatra ve ny kitendry miraikidraikitra? _Aza alefa _Aza atsahatra Tsoahy Nisy olana teo am-panandramana mandefa ny (%s)
izay mifanaraka amin'ny kitendry (%s) Endri-tsoratra Voafarirtra manaraka ny karazana %s ny famaha GConf %s, nefa ny karazany nampoizina dia %s
 Laha-tahirin'ny fandraisana Toa efa misy rindran'asa hafa mampiasa ny kitendry '%u'. Voasoritra imbetsaka ny asan'ny Key Binding (%s)
 Voasoritra imbetsaka ny binding'ny Key Binding (%s)
 Key miasa ny Key Binding (%s)
 Tsy feno ny Key Binding (%s)
 Tsy ekena ny Key Binding (%s)
 Fafan-teny Alefaso ny mpizaha toro-làlana Alefaso ny mpitety tranonkala Alaivo ireo rakitra modmap Gejao ny efijery Hivoaka Fidirana Fivoahana Totozy Safidy manokan'ny totozy Tsy misy feo Mamaky (na mamaky/miato) Karohy Hisafidy raki-peo Anjombona Fampilazan'ny kitendry meda Feo Tsy voafaritra ny feo ho an'ity zava-miseho ity. Alefaso ny sary mitsitsy Fampilazan'ny kitendry miraikidraikitra Ireo mpandray an-tànana ny text/plain and text/* an'ny sync Feon'ny rafitra Rakitra .wav tsy ekena ny rakitra %s Tsy misy ny raki-peo ho an'ity zava-miseho ity. Tsy misy ny raki-peo ho an'ity zava-miseho ity.
Mety mila mametraka ny fehy gnome-audio izay misy feo tsotra maromaro angamba ianao. Nisy olana teo am-manehoana ireo toro-làlana: %s Nisy olana teo am-pandefasana ny sary mitsitsy:

%s
Tsy handeha amin'ity session ity fahasahazan'ny sary mitsitsy. Fiatoan'ny fanoratana Fanamafisam-peo Ambany Malefaka Dingan'ny fanamafisam-peo Dingana mampiseho ny hamafin'ny feo. Ambony Tianao alaina ilay (ireo) rakitra modmap? Notsindrianao nandritra ny 8 segaondra ny kitendry Shift. Io no hitsin-dàlana mampadeha ny fahasahazan'ny kitendry meda izay mampiova ny fiasan'ny fafan-teninao.  Notsindrianao in-5 misesisesy ny kitendry Shift. Io no hitsin-dàlana mampandeha ny fahasahazan'ny kitendry miraikidraikitra izay mampiova ny fiasan'ny fafan-teninao. Nanindry kitendry roa miaraka ianao, na nanindry ny kitendry Shift in-5 misesisesy. Izany dia manatsahatra ny fahasahazan'ny kitendry miraikidraikitra izay mampiova ny fiasan'ny fafan-teninao. _Alefaso _Atsaharo _Aza aseho intsony ity filazana ity _Alaivo _Ireo rakitra azo: 