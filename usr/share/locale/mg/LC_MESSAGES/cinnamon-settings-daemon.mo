��          D      l       �   &   �   �   �   �   <  �   �  �  m  1   &  �   X  �   �  �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:17+0000
Last-Translator: Thierry Randrianiriana <randrianiriana@gmail.com>
Language-Team: Malagasy <i18n-malagasy-gnome@gna.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Nisy olana teo am-manehoana ireo toro-làlana: %s Notsindrianao nandritra ny 8 segaondra ny kitendry Shift. Io no hitsin-dàlana mampadeha ny fahasahazan'ny kitendry meda izay mampiova ny fiasan'ny fafan-teninao. Notsindrianao in-5 misesisesy ny kitendry Shift. Io no hitsin-dàlana mampandeha ny fahasahazan'ny kitendry miraikidraikitra izay mampiova ny fiasan'ny fafan-teninao. Nanindry kitendry roa miaraka ianao, na nanindry ny kitendry Shift in-5 misesisesy. Izany dia manatsahatra ny fahasahazan'ny kitendry miraikidraikitra izay mampiova ny fiasan'ny fafan-teninao. 