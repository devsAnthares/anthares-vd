��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     ?     X  E   t  !   �     �     �  x   �  f   l  $   �  b   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-24 15:01+0900
Last-Translator: Tsuji Kento <tuziemon@pclives.org>
Language-Team: Japanese <gnome-translation@gnome.gr.jp>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 %s の公開ファイル %s@%s の公開ファイル パーソナルファイル共有を起動します (可能な場合) パーソナルファイル共有 共有 共有の設定 パーソナルファイル共有をオンにするとネットワーク上でこのフォルダーを共有できます いつパスワードを問い合わせるか。設定可能な値: "never"、"on_write"、"always"。 パスワードを要求する時期 share;files;http;network;copy;send;共有;ファイル;ネットワーク;コピー;複製;送信; 