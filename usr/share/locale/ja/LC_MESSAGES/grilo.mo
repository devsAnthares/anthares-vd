��    $      <  5   \      0     1  '   Q     y  "   �     �  ,   �  .        1     O  $   n     �  #   �     �     �     �  !         /  &   P     w     �  '   �     �     �          )  ;   C       2   �     �     �  !   �  7     (   U     ~     �  �  �  D   d	  <   �	     �	  0    
  $   1
  C   V
  E   �
  9   �
  7     <   R  !   �  6   �  6   �       1   8  <   j  ?   �  9   �  *   !  9   L  E   �  '   �  -   �  9   "  -   \  N   �  -   �  K     '   S  -   {  3   �  O   �  =   -  0   k     �                   
   $       "                                                                                       	                              #                    !              '%s' is not a valid plugin file All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Colon-separated list of Grilo plugins to use Colon-separated paths containing Grilo plugins Could not access mock content Could not find mock content %s Could not resolve media for URI '%s' Data not available Failed to initialize plugin from %s Failed to load plugin from %s Grilo Options Invalid plugin file %s Invalid request URI or header: %s Media has no 'id', cannot remove Metadata key '%s' cannot be registered No mock definition found No searchable sources available None of the specified keys are writable Operation was cancelled Plugin '%s' already exists Plugin '%s' is already loaded Plugin '%s' not available Plugin configuration does not contain 'plugin-id' reference Plugin not found: '%s' Semicolon-separated paths containing Grilo plugins Show Grilo Options Some keys could not be written Source with id '%s' was not found The entry has been modified since it was downloaded: %s The requested resource was not found: %s Unable to load plugin '%s' Unhandled status: %s Project-Id-Version: grilo master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=grilo&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-07-24 22:00+0900
Last-Translator: Nishio Futoshi <fut_nis@d3.dion.ne.jp>
Language-Team: Japanese <gnome-translation@gnome.gr.jp>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 '%s' は有効なプラグインのファイルではありません 設定されたプラグインのパスが全て無効です 認証が必要です: %s プロキシサーバーへ接続できません サーバーへ接続できません 使用する Grilo プラグインのリスト (コロン区切り) Grilo プラグインを含むパスのリスト (コロン区切り) モックのコンテンツにアクセスできません モックのコンテンツ %s が見つかりません URI '%s' に対応するメディアが判別できません データは利用できません %s のプラグインの初期化に失敗しました %s のプラグインのロードに失敗しました Grilo のオプション プラグインのファイル %s は無効です 要求された URI またはヘッダーが無効です: %s メディアに 'id' がありません。移動できません メタデーターのキー '%s' は登録できません モックの定義が見つかりません 利用できる検索可能なソースがありません 指定されたキーに書き込み可能なものはありません 操作がキャンセルされました プラグイン '%s' は既に存在します プラグイン '%s' は既にロードされています プラグイン '%s' は利用できません プラグインの設定に、'plugin-id' の参照が含まれていません プラグインが見つかりません: '%s' Grilo プラグインを含むパスのリスト (セミコロン区切り) Grilo のオプションを表示する 書き込めなかったキーがあります 識別子 '%s' のソースが見つかりません その項目は、ダウンロード後に変更が加えられています: %s 要求したリソースが見つかりませんでした: %s プラグイン '%s' はロードできません 扱えないステータス: %s 