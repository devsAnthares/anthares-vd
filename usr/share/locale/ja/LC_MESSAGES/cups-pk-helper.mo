��          �      �           	     !     A      [     |     �     �     �  3   �  ;     5   S  <   �  w   �  @   >  9     3   �  K   �  5   9  I   o     �  /   �        �  $      
  2   +  &   ^  2   �     �  %   �  -   �  "   +	  >   N	  P   �	  D   �	  P   #
  �   t
  U   �
  K   U  @   �  h   �  G   K  x   �  )     J   6  B   �                                     
                                                   	                Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=cups-pk-helper
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2012-02-14 08:27+0000
Last-Translator: Tomoyuki KATO <tomo@dream.daynight.jp>
Language-Team: Japanese (http://www.transifex.net/projects/p/freedesktop/language/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0
 クラスの追加/削除/編集 ローカルプリンターの追加/削除/編集 プリンターの追加/削除/編集 リモートプリンターの追加/削除/編集 プリンター設定の変更 プリンターの有効化/無効化 利用可能なデバイスの一覧の取得 サーバー設定の取得/設定 クラスの追加/削除/編集には特権が必要です。 ローカルプリンターの追加/削除/編集には特権が必要です。 プリンターの追加/削除/編集には特権が必要です。 リモートプリンターの追加/削除/編集には特権が必要です。 プリンター設定の変更には特権が必要です。これはプリンターシステム設定パネルからのみ必要です。 プリンターまたはクラスの有効化/無効化には特権が必要です。 利用可能なデバイスの一覧の取得には特権が必要です。 サーバー設定の取得/設定には特権が必要です。 他のユーザーが所有するジョブの再起動/取り消し/編集には特権が必要です。 ジョブの再起動/取り消し/編集には特権が必要です。 デフォルトのプリンターとしてプリンターまたはクラスを設定するには特権が必要です。 ジョブの再起動/取り消し/編集 他のユーザーが所有するジョブの再起動/取り消し/編集 デフォルトのプリンターとしてプリンターの設定 