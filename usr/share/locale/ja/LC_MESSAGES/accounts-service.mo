��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  E   R  B   �  N   �  *   *  -   U  '   �  *   �  0   �  �     -   �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Tomoyuki KATO <tomo@dream.daynight.jp>
Language-Team: Japanese (http://www.transifex.com/projects/p/freedesktop/language/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 ログイン画面の設定を変更するには認証が必要です ユーザーのデータを変更するには認証が必要です 自分自身のユーザーデータを変更するには認証が必要です ログイン画面の設定を変更する 自身のユーザーデータを変更する デバッグコードを有効にする ユーザーアカウントを管理する バージョン情報を出力して終了する ユーザーアカウント情報を問い合わせおよび操作するための 
D-Bus インターフェースを提供します。 既存のインスタンスを置き換える 