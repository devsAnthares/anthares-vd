��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  0   �     �  l   �  *   9  0   d  -   �  Q   �  9        O  N   k  G   �  (   	  (   +	  I   T	  N   �	     �	  0    
  +   1
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: https://launchpad.net/~linuxmint-translation-team-japanese
PO-Revision-Date: 2017-03-26 15:33+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Japanese <https://launchpad.net/~linuxmint-translation-team-japanese>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ja
 スクリーンセーバーを強制終了する 確認しています... 有効になっているスクリーンセーバーを無効にする (ブランク・スクリーン解除) パスワードが正しくありません ロック画面に表示されるメッセージ パスワードを入力してください… スクリーンセーバーが有効になっている時間を問い合わせる スクリーンセーバーの状態を問い合わせる ユーザーの切り替え 起動中のスクリーンセーバーに今すぐ画面をロックさせる スクリーンセーバーは %d 秒間有効になっています。
 スクリーンセーバは有効です
 スクリーンセーバは無効です
 スクリーンセーバーは現在、有効になっていません。
 スクリーンセーバーを有効にする (ブランク・スクリーン) ロックの解除 このアプリケーションのバージョン Caps Lock キーが押されています。 