��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  G   R  0   �  9   �  1     #   7     [     z  -   �  h   �  "   0               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Gil Forcada <gforcada@gnome.org>
Language-Team: Catalan (http://www.transifex.com/projects/p/freedesktop/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Cal autenticació per canviar la configuració de la pantalla d'entrada Cal autenticació per canviar les dades d'usuari Cal autenticació per canviar les dades d'usuari pròpies Canvieu la configuració de la pantalla d'entrada Canvieu les dades d'usuari pròpies Habilita el codi de depuració Gestioneu els comptes d'usuari Mostra la informació sobre la versió i surt Ofereix interfícies de D-Bus per realitzar consultes i manipular
la informació del compte de l'usuari. Reemplaça una instància existent 