��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     8  J     �	     �	     �	     �	  	   �	     �	     �	     �	     �	  0   �	  0   .
  ]   _
  P   �
  	          
   -     8  2   S  (   �  =   �  5   �  8   #  6   \  J   �  6   �  W     E   m  �   �  1   �  �   �  z   ]  K   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 2.12.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-09-27 22:48+0200
Last-Translator: Gil Forcada <gilforcada@guifi.net>
Language-Team: Catalan <tradgnome@softcatala.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
 %A %d de %B, %R %A %d de %B, %R:%S %A %d de %B, %l:%M %p %A %d de %B, %l:%M:%S %p %A, %e %B %R %R:%S %l:%M %p %l:%M:%S %p la pantalla CRTC %d no pot mostrar la sortida %s la pantalla CRTC %d no permet la rotació = %s» Pantalla CRTC %d: s'està provant el mode %dx%d@%dHz amb la sortida a %dx%d@%dHz (intent %d)
 No s'ha trobat cap terminal, s'utilitzarà l'xterm encara que potser no funcioni Portàtil Pantalles duplicades Desconegut no hi ha l'extensió RANDR S'estan provant els modes per la pantalla CRTC %d
 no s'ha pogut clonar cap a la sortida %s no s'han pogut assignar les pantalles CRTC a les sortides:
%s no s'ha pogut obtenir la informació sobre el CRTC %d no s'ha pogut obtenir la informació sobre la sortida %d no s'ha pogut obtenir el rang de les mides de pantalla no s'han pogut obtenir els recursos de la pantalla (CRTC, sortides, modes) no s'ha pogut establir la configuració per al CRTC %d cap de les configuracions desades de la pantalla coincideix amb la configuració activa cap mètode seleccionat era compatible amb els mètodes possibles:
%s la sortida %s no permet utilitzar els mateixos paràmetres que l'altra sortida clonada:
el mode existent = %d, el mode nou = %d
les coordenades existents = (%d, %d), les coordenades noves (%d, %d)
la rotació existent = %s, la rotació nova = %s la sortida %s no pot utilitzar el mode %dx%d@%dHz la posició/mida sol·licitades per al CRTC %d són fora dels límits permesos: posició=(%d, %d), mida=(%d, %d), màxim=(%d, %d) la mida virtual sol·licitada no s'ajusta a la mida disponible: sol·licitada=(%d, %d), mínima=(%d, %d), màxima=(%d, %d) s'ha produït un error no gestionat de l'X en obtenir les mides de pantalla 