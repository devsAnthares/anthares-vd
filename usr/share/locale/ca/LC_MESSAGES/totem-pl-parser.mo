��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       T     `     h     l       6   �  )   �     �                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem 2.14
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2017-09-01 00:45+0200
Last-Translator: Gil Forcada <gilforcada@guifi.net>
Language-Team: Catalan <tradgnome@softcatala.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.11
 CD d'àudio Blu-ray DVD Televisió digital No s'ha pogut muntar %s. No hi ha cap suport a la unitat del dispositiu «%s». Comproveu que hi ha un disc en la unitat. CD de vídeo 