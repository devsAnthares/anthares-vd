��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  4   &     [  G   q     �  /   �        <     )   Z     �  N   �  k   �  "   O  $   r  &   �  -   �     �     �  3   	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-16 10:18+0000
Last-Translator: Robert Antoni Buj Gelonch <Unknown>
Language-Team: Catalan <tradgnome@softcatala.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ca
 Fa que l'estalvi de pantalla acabi de forma correcta S'està comprovant... Desactiva l'estalvi de pantalla si està actiu (restableix la pantalla) Contrasenya incorrecta Missatge que es mostra en bloquejar la pantalla Introduïu la contrasenya... Consulta quant de temps ha estat actiu l'estalvi de pantalla Consulta l'estat de l'estalvi de pantalla Canvia d'usuari Li diu al procés d'estalvi de pantalla que bloquegi la pantalla immediatament Fa %d segons que l'estalvi de pantalla s'ha activat.
 Fa %d segons que l'estalvi de pantalla s'ha activat.
 L'estalvi de pantalla està actiu
 L'estalvi de pantalla està inactiu
 L'estalvi de pantalla no està actiu.
 Activa l'estalvi de pantalla (pantalla negra) Desbloqueja Versió d'aquesta aplicació Teniu activada la tecla de fixació de majúscules. 