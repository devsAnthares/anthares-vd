��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 |  %     �	     �	  "   �	  	   �	     �	     
  *   "
     M
  
   d
  "   o
  	   �
  +   �
  5   �
     �
          ,  X   I     �  
   �     �  (   �     �  &   �     %  
   -  "   8  "   [  ;   ~  +   �     �  4   �     #     ;     Z     k     �     �  ?   �     �     �                     6     O  	   e  	   o                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-12-11 07:45+0000
Last-Translator: Robert Antoni Buj Gelonch <Unknown>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adreça Atorga sempre l'accés Sol·licitud d'autorització de %s Bluetooth Ajusts del Bluetooth El Bluetooth està inhabilitat El Bluetooth està inhabilitat físicament Navega pels fitxers... Cancel·la Configureu els ajusts de Bluetooth Connexió El dispositiu %s vol accedir al servei '%s' El dispositiu %s vol vincular-se amb aquest ordinador No coincideix Error en navegar pel dispositiu Atorga només aquesta vegada Si suprimiu el dispositiu, l'haureu de configurar de nou abans de tornar-lo a utilitzar. Ajusts del teclat Coincideix Ajusts del ratolí Ajusts del ratolí i del ratolí tàctil No No s'ha trobat cap adaptador Bluetooth D'acord Emparellat Confirmació de vinculació per %s Sol·licitud de vinculació per %s Confirmeu que el PIN '%s' coincideix amb el del dispositiu. Introduïu el PIN indicat en el dispositiu. Rebutja Voleu eliminar «%s» de la llista dels dispositius? Suprimeix el dispositiu Envia fitxers al dispositiu... Envia fitxers... Configura un dispositiu nou Configura un dispositiu nou... Ajusts del so No es pot navegar pel dispositiu sol·licitat, l'error és '%s' Tipus Visibilitat Visibilitat de «%s» Sí s'està connectant... s'està desconnectant... maquinari inhabilitat pàgina 1 pàgina 2 