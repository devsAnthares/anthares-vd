��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  a  B  8   �
     �
  +   �
          (  ~   ;     �     �  ;   �  '        ?  7   [     �     �  E   �  -     f   6     �     �     �     �       /        L  &   Q     x  1   �     �  -   �  2   �  	   '     1     I     \  +   l     �     �     �  #   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-02-26 22:46+0000
PO-Revision-Date: 2015-02-04 16:15+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2015-02-05 07:03+0000
X-Generator: Launchpad (build 17331)
 Podrazumijevana grupa, dodijeljena po pravljenju prozora Pokazivač: Drži i radi sa odvojenom grupom po prozoru Raspored tastature Raspored tastature Raspored tastature "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing seje package metadata Model tastature Opcije tastature Učitava neobične, rijetko korišćenje rasporede i opcije Učitava dodatne stavke za podešavanje Pregled rasporeda tastature Sačuvaj/vrati pokazatelje zajedno sa grupama rasporeda Sekundarne grupe Prikaži zastavice u programu Prikaži zastavice u programu koje će ukazivati na trenutni raspored Prikaži nazive rasporeda umjesto imena grupa Prikaži nazive rasporeda umjesto imena grupa (samo za XFree izdanja koja podržavaju više rasporeda) Pregled tastature, X pomjeraj Pregled tastature, Y pomjeraj Pregled tastature, visina Pregled tastature, širina Boja pozadine Boja pozadine u pokazivaču rasporeda tastature Font Font u pokazivaču rasporeda tastature Veličina fonta Veličina fonta u pokazivaču rasporeda tastature Boja teksta Boja teksta u pokazivaču rasporeda tastature Dogodila se greška prilikom učitavanja slike: %s Nepoznato Ne mogu da pokrenem XKB raspored tastature model tastature raspored "%s" rasporedi "%s" rasporedi "%s" model "%s", %s i %s nema rasporeda nema opcija opcija "%s" opcije "%s" opcije "%s" 