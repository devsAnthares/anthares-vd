��    %      D  5   l      @     A  '   a     �  "   �     �  ,   �  .        A     _  $   ~     �  #   �     �     �            !   ,      N  &   o     �     �  '   �     �          *     H  ;   b     �  2   �     �     �  !     7   <  (   t     �     �  �  �     �	  -   �	     �	  "   
     3
  4   P
  1   �
  $   �
      �
  #   �
     !  (   6  #   _     �     �     �  (   �  !   �  2        :  "   V  /   y     �     �     �     �  5        E  5   a     �  $   �     �  )   �  $     %   A     g                   
          #                                                                    !                  	         %                    $                    "               '%s' is not a valid plugin file All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Colon-separated list of Grilo plugins to use Colon-separated paths containing Grilo plugins Could not access mock content Could not find mock content %s Could not resolve media for URI '%s' Data not available Failed to initialize plugin from %s Failed to load plugin from %s Grilo Options Invalid URL %s Invalid plugin file %s Invalid request URI or header: %s Media has no 'id', cannot remove Metadata key '%s' cannot be registered No mock definition found No searchable sources available None of the specified keys are writable Operation was cancelled Plugin '%s' already exists Plugin '%s' is already loaded Plugin '%s' not available Plugin configuration does not contain 'plugin-id' reference Plugin not found: '%s' Semicolon-separated paths containing Grilo plugins Show Grilo Options Some keys could not be written Source with id '%s' was not found The entry has been modified since it was downloaded: %s The requested resource was not found: %s Unable to load plugin '%s' Unhandled status: %s Project-Id-Version: bosnianuniversetranslation
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=grilo&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-01-08 15:57+0000
Last-Translator: Almir Husic <ahusic3@etf.unsa.ba>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-10-23 07:21+0000
X-Generator: Launchpad (build 17203)
 '%s' nije validan plugin fajl Svi konfigurisani plugin putevi su nevažeći Potrebna autentifikacija: %s Ne mogu se spojiti na proxy server Ne mogu se spojiti na server Lista razdvojenih kolona Grilo pruginova za upotrebu Kolone-razdojene putevima sadrže Grilo pluginove Ne mogu pristupiti lažnom sadržaju Ne mogu naći lažni sadržaj %s Ne mogu riješiti medij za URL '%s' Podaci nisu dostupni Neuspjelo inicijaliziranje plugina iz %s Neuspjelo učitavanje plugina iz %s Grilo Opcije Nevažeći URL %s Neispravan plugin fajl %s Nevažeći zahtjev URL ili zaglavlje: %s Medij nema 'id', ne mogu ukloniti Ključ metapodataka '%s' ne može biti registriran Nema pronađene lažne def. Nemadostupnih  pretraživih izvora Nijedan od specificiranih ključeva nije upisiv Operacija je prekinuta Plugin '%s' već postoji Plugin '%s' je već učitan Plugin '%s' nije dostupan Plugin konfiguracija ne sadrži 'plugin-id' referencu Plugin nije pronađen: '%s' Semikolone-razdvojene putevima sadrže Grilo puginove Prikaži Grilo Opcije Neki ključevi ne mogu biti zapisani Izvor sa id '%s' nije pronađen Unos je modificiran otkako je preuzet: %s Zahtjevani resurs nije pronađen: %s Nije moguće učitavanje plugina '%s' Nepodržan status: %s 