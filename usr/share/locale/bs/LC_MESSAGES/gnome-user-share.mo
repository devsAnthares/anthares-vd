��          �   %   �      `     a     s     �     �  c   �  ,     '   ;     c  	   �     �     �     �     �     �     �  (   �  [     Q   `     �  8   �  -        3  ,   M     z     �  2   �  �  �  !   �  .   �          
  q   &  ;   �  0   �     	     %	     5	     Q	     Z	     f	  
   x	     �	  1   �	  g   �	  R   5
     �
  ;   �
  2   �
       +   &  )   R     |  8   �                                                 	                         
                                                  %s's public files %s's public files on %s Cancel File reception complete If this is true, Bluetooth devices can send files to the user's Downloads directory when logged in. Launch Bluetooth ObexPush sharing if enabled Launch Personal File Sharing if enabled May be shared over the network Open File Personal File Sharing Preferences Receive Reveal File Sharing Sharing Settings When to accept files sent over Bluetooth When to accept files sent over Bluetooth. Possible values are "always", "bonded" and "ask". When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords Whether Bluetooth clients can send files using ObexPush. Whether to notify about newly received files. You have been sent a file You have been sent a file "%s" via Bluetooth You received "%s" via Bluetooth You received a file share;files;bluetooth;obex;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-02-16 22:42+0000
Last-Translator: Samir Ribić <Unknown>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-17 06:20+0000
X-Generator: Launchpad (build 17341)
 Javne datoteke korisnika „%s“ Javne datoteke korisnika „%s“ na  „%s“ Otkaži Prijem datoteke je završen Ako je ovo istina, Bluetooth uređaji mogu slati fajlove u korisnikovu Downloads direktoriju kada su prijavljeni. Pokreni dijeljenje blututa obeks guranjem ako je uključeno Pokreni lično dijeljenje datoteka ako jemoguće Može biti deljeno preko mreže Otvori datoteku Dijeljenje ličnih datoteka Postavke Preuzimanje Prikaži datoteku Dijeljenje Podešavanja dijeljenja Kada da prihvatim fajlove poslane preko Bluetooth Kada da prihvatim fajlove poslane preko Bluetooth. Moguće vrijednosti su "uvijek", "vezan", i "pitaj". Kada pitati za šifre. Moguće vrijednosti su "nikada", "pri_pisanju", i "uvijek". Kada da zehtijevam šifre Da li Bluetooth klijenti mogu slati fajlove preko ObexPush. Da li da obavijestim o novim primljenim fajlovima. Poslali ste datoteku Poslali ste datoteku „%s“ preko blututa Primili ste „%s“ preko Bluetooth veze Primili ste datoteku deli;datoteke;blutut;obeks;http;mreža;umnoži;pošalji; 