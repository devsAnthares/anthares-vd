��          D      l       �   &   �   �   �   �   <  �   �  �  m  0   l  �   �  �     �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center.HEAD.bs
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:01+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Bosnian <lokal@linux.org.ba>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Dogodila se greška pri prikazivanju pomoći: %s Upravo ste držali pritisnutim tipku Shift 8 sekundi.  Ovo je kratica za osobinu spore tipke koja utječe na rad vaše tastature. Upravo ste pritisnuli tipku Shift 5 puta za redom.  Ovo je kratica za osobinu ljepljive tipke koja utječe na rad vaše tastature. Upravo ste istovremeno pritisnuli dvije tipke ili ste pritisnuli tipku Shift 5 puta za redom. Ovo isključuje osobinu ljepljive tipke koja utječe na rad vaše tastature. 