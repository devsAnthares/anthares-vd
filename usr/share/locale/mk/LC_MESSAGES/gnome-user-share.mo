��          �      �       H     I     [     b  l   {     �     �     �       Q   /     �     �  
   �     �  N  �  #        ?  F   N  �   �     l  #   {  0   �  >   �  �     %   �  3   �     �  $   	        
                   	                                      %s's public files Always File Sharing Preferences If this is true, the Public directory in the users home directory will be shared when the user is logged in. Never Share Files Share Public directory Share Public files on network When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords When writing files _Password: _Require password: Project-Id-Version: mk
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-08-14 23:28+0200
Last-Translator: Арангел Ангов <ufo@linux.net.mk>
Language-Team: Macedonian <ossm-members@hedona.on.net.mk>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
 Јавни датотеки на %s Секогаш Преференци за споделување на датотеки Ако е точно, јавниот директориум во домашниот директориум на корисниците ќе биде споделен по најавата на корисникот. Никогаш Споделени датотеки Сподели јавен директориум Сподели датотеки јавно на мрежата Кога да бара лозинки. Можни вредности се "никогаш", "при запишување" и "секогаш". Кога да бара лозинки Кога се запишуваат датотеки _Лозинка: _Потребна е лозинка: 