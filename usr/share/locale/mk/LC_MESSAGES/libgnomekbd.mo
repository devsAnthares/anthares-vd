��    2      �  C   <      H  x   I  8   �     �       
   "     -     >     \     w  *   �      �     �     �  
     )   "     L     g     �     �     �  z   �     C     R     c  3   s     �     �  7   �  (   	  a   2  2   �     �     �     	     "	     >	  .   Y	  '   �	     �	     �	     �	     �	     
     
     .
     D
  	   U
  
   _
     j
  �  �
     +  _   L  0   �  %   �       7   !  C   Y  A   �  D   �  _   $  Q   �  A   �  F        _  U   s  D   �  E     D   T  D   �  *   �  �   	  "     "   '     J  n   ]     �  /   �  r     l   �  �   �  i   �  6   :  6   q  8   �  <   �  7     m   V  G   �  M     =   Z  '   �  (   �  "   �  S        `  '   ~     �     �  =   �                       (                      !                      1       -             ,   /         '   #                     +   )                .      &      %                    "   0                      	   *       2   $   
       A collection of scripts to run whenever the keyboard state is reloaded. Useful for re-applying xmodmap based adjustments A list of modmap files available in the $HOME directory. Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Failed to init GConf: %s
 Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator Test (%s) Keyboard Indicator plugins Keyboard Update Handlers Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options No description. Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) Suppress the "X sysconfig changed" warning message The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s modmap file list no layout no options option "%s" options "%s" Project-Id-Version: mk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-08-14 19:56+0000
PO-Revision-Date: 2008-02-23 19:47+0100
Last-Translator: Arangel Angov <ufo@linux.net.mk>
Language-Team: Macedonian <ossm-members@hedona.on.net.mk>
Language: mk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural= n==1 || n%10==1 ? 0 : 1
X-Generator: KBabel 1.11.4
 Колекција од скрипти која ќе се изврши кога одново ќе се вчита состојбата на тастатурата. Корисно за повторно применување на прилагодувањата базирани наxmodmap Листа на modmap датотеки достапни во $HOME директориумот. Активирај уште приклучоци Активни _приклучоци: Додај приклучок Затвори го дијалог прозорецот Конфигурирај го избраниот приклучок Деактивирај го избраниот приклучок Намали го приоритетот на приклучокот Стандардна група, назначена за креирање на прозорци Овозможи/оневозможи инсталирани приклучоци Не можам да го иницијализирам GConf: %s
 Зголеми го приоритетот на приклучокот Индикатор: Чувај и управувај со посебна група по прозорец Приклучоци за индикатор на тастатура Тест за индикаторот на тастатурата (%s) Приклучоци за индикатор на тастатура Справувачи за ажурирање на тастатура Распоред на тастатаура Распоред на тастатура "%s"
Авторски права &#169; X.Org Foundation и придонесувачите во XKeyboardConfig
За лиценцата проверете ги мета податоците за пакетот Модел на тастатура Опции на тастатура Нема опис. Зачувај/врати ги индикаторите заедно со групите за распоред Секундарни групи Покажи знамиња во аплетот Покажи знамиња во аплетот за означување ан тековниот распоред Покажи ги имињата на распоредот наместо имињата на групите Покажи ги имињата на распоредот, наместо имињата на групите (само за верзии на XFree кои поддржуваат повеќе распореди) Не ја покажувај пораката за предупредување „X sysconfig changed“ Преглед на тастатурата, X оска Преглед на тастатурата, Y оска Преглед на тастатурата, висина Преглед на тастатурата, широчина Листата со активни приклучоци Листата на овозможени приклучоци за индикатор на тастатура Имаше грешка во вчитувањето на слика: %s Не можам да ја отворам датотеката со помош Грешка во иницијализацијата на XKB _Достапни приклучоци: распоред на тастатура модел на тастатура распореди „%s“ распореди „%s“ распореди „%s“ модел „%s“, %s и %s листа со modmap датотеки нема изглед нема опции опција „%s“ опции „%s“ опции „%s“ 