��    (      \  5   �      p     q  5   �  7   �     �  (     )   +  %   U  '   {     �     �  !   �     �     �     �                    8     N  '   ]  '   �  P   �     �           "  +   C     o     �     �     �     �  &   �     �  X   	  
   b     m     v  
        �  �  �  ,   w	  f   �	  �   
     �
  Q   �
  7     J   H  S   �  -   �       E        `     �  (   �     �     �  <   �  7        J  t   ^  I   �  e        �  9   �  9   �  j     [   z     �  !   �  &        7  c   J  =   �  �   �     �     �     �     �     �                      "   &          #                '      	                                    (            %                     !                     
                $             A program is still running: Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Enable debugging code FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Lock Screen Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Suspend Anyway Switch User Anyway Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Waiting for programs to finish.  Interrupting these programs may cause you to lose work. _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session.HEAD.mk
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:14+0000
Last-Translator: Јован Наумовски <jovan@lugola.net>
Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 || n%10==1 ? 0 : 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: mk
 Програма сè уште работи: Апликацијата не прифаќа документи на командната линија Не можам да ја префрлам адресата за документот во запис на работната површина со Тип=Врска Откажи Не може да се приклучи на менаџерот за сесии. Не можам да креирак socket за ICE: %s Исклучи ги врските до менаџерот за сесии Не вчитувај апликации одредени од корисникот Вклучи код за дебагирање FILE Датотеката не е валидна .desktop датотека Сепак хибернирај Ид Иконата %s не е најдена Заклучи екран Одјава Да се одјавам од системот веднаш? Предметот не може да се изврши Не реагира Запиши врз стандардните директориуми за автоматско стартување Програмата се повика со конфлитни опции Одбивам нови врски од клиенти бидејќи сесијата се гаси
 С_успендирај Да го исклучам системот веднаш? Некои програми сè уште работат: Одредете датотека што ја содржи зачуваната конфигурација Одредете го бројот за ид. за менаџмент на сесијата Подигнувам %s Сепак суспендирај Сепак смени корисник Непознато Непозната верзија на датотека за работната површина %s Непозната опција за извршување: %d Чекам да завршат програми.  Прекинувањето на овие програми може да предизвика загуба на податоци. _Хибернирај _Одјава _Рестартирај _Исклучи _Смени корисник 