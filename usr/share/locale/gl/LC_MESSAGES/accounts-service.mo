��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  x  �  K   0  8   |  E   �  .   �  (   *      S     t  -   �  `   �  !                  	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Fran Diéguez <frandieguez@ubuntu.com>
Language-Team: Galician <proxecto@trasno.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl
Plural-Forms: nplurals=2; plural=(n != 1);
 Requirse autenticación para cambiar a configuración da pantalla de inicio Requirse autenticación para cambiar os datos do usuario Requirse autenticación para cambiar os seus propios datos de usuario Cambiar a configuración da pantalla de inicio Cambiar os seus propios datos de usuario Activar o código de depuración Xestionar contas de usuario Mostrar a información de versión e saír Fornece interfaces de D-Bus para consulta e manipulación 
de información de contas de usuario. Substituír a instancia existente 