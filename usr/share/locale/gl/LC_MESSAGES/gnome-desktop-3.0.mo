��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h    �     �     �     �     �     �                '     <     K     ]     l     ~     �     �     �  '   �     �  G   	     L	     l	  "   }	  .   �	  G   �	  �   
  (   �
  {                                                 	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop-master-po-gl-54183.merged
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-29 23:33+0200
Last-Translator: Fran Dieguez <frandieguez@gnome.org>
Language-Team: Proxecto Trasno <proxecto@trasno.gal>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 %R %R:%S %a %R %a %R:%S %a %e de %b_%R %a %e de %b_%R:%S %a %e de %b_%l:%M %p %a %e de %b_%l:%M:%S O %A ás %k:%M %a de %l:%M:%S %p %a %e de %b_%R %a %e de %b_%R:%S %a %e de %b_%l:%M %p %a %e de %b_%l:%M:%S %l:%M %p %l:%M:%S %p o CRTC %d non pode conducir a saída %s CRTC %d non admite rotation=%d CRTC %d: tentando o modo %dx%d@%dHz coa saída en %dx%d@%dHz (paso %d)
 Tentando os modos para CRTC %d
 Non especificado non é posíbel clonar a saída %s non foi posíbel asignar os CRTC a saídas:
%s ningún dos modos seleccionados era compatíbel cos modos posíbeis:
%s a saída %s non ten os mesmos parámetros que a outra saída clonada:
modo existente = %d, new mode = %d
coordenadas existentes = (%d, %d), novas coordenadas = (%d, %d)
rotación existente = %d, nova rotación = %d a saída %s non admite o modo %dx%d@%dHz o tamaño virtual solicitado non se axusta ao tamaño dispoñíbel: solicitado=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 