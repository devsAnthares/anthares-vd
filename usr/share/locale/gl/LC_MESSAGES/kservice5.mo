��            )   �      �     �  /   �     �     �  _   	  6   i  `   �  N     n   P  E   �  W     @   ]     �     �      �  !   �  %     )   =      g  c   �  -   �  D        _     ~  !   �  B   �  @   �     ?  �  ]     D
  -   J
     x
     �
  C   �
     �
  B   �
  9   9  R   s  ;   �  G     3   J      ~  (   �  *   �  %   �  -     /   G  >   w  g   �  0     Q   O  1   �  &   �  #   �  P     2   o     �                        
                                                                                  	                                @info:creditAuthor @info:creditCopyright 1999-2014 KDE Developers @info:creditDavid Faure @info:creditWaldo Bastian @info:shell command-line optionCheck file timestamps (deprecated, no longer having any effect) @info:shell command-line optionCreate global database @info:shell command-line optionDisable checking files (deprecated, no longer having any effect) @info:shell command-line optionDisable incremental update, re-read everything @info:shell command-line optionDo not signal applications to update (deprecated, no longer having any effect) @info:shell command-line optionPerform menu generation test run only @info:shell command-line optionSwitch QStandardPaths to test mode, for unit tests only @info:shell command-line optionTrack menu id for debug purposes Could not launch Browser Could not launch Mail Client Could not launch Terminal Client Could not launch the browser:

%1 Could not launch the mail client:

%1 Could not launch the terminal client:

%1 EMAIL OF TRANSLATORSYour emails Error launching %1. Either KLauncher is not running anymore, or it failed to start the application. Function must be called from the main thread. KLauncher could not be reached via D-Bus. Error when calling %1:
%2
 NAME OF TRANSLATORSYour names No service implementing %1 The provided service is not valid The service '%1' provides no library or the Library key is missing application descriptionRebuilds the system configuration cache. application nameKBuildSycoca Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2017-02-02 21:34+0100
Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail.com>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde, development
X-Accelerator-Marker: &
X-Text-Markup: kde4
 Autor Copyright 1999-2014 Os desenvolvedores de KDE David Faure Waldo Bastian Comprobar os selos de tempo dos ficheiros (obsoleto, non funciona). Crear unha base de datos global Desactivar a comprobación dos ficheiros (obsoleto, non funciona). Desactivar a actualización incremental, ler de novo todo Non enviar sinais aos aplicativos para que se actualicen (obsoleto, non funciona). Realizar só unha execución de proba da xeración do menú Cambiar QStandardPaths para o modo de probas, só para probas unitarias Rastrexar o identificador do menú para depuración Non se puido iniciar o navegador Non se puido iniciar o cliente de correo Non se puido iniciar o cliente de terminal Non se puido iniciar o navegador:

%1 Non se puido iniciar o cliente de correo:

%1 Non se puido iniciar o cliente de terminal:

%1 xosecalvo@gmail.com, mvillarino@gmail.com, proxecto@trasno.gal Produciuse un erro ao iniciar %1. Ou KLauncher deixou de funcionar, ou non puido arrincar o aplicativo. A función debe chamarse desde o fío principal. Non se puido acadar KLauncher mediante D-Bus. Ocorreu un erro ao chamar a %1:
%2
 Xabier García Feal, marce villarino, Xosé Calvo Non hai ningún servizo que realice %1 O servizo fornecido non é correcto Ou o servizo «%1» non fornece ningunha biblioteca ou falta a chave «Library» Reconstrúe a caché da configuración do sistema. KBuildSycoca 