��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     
     #
     <
     I
     L
     R
     [
  '   g
  !   �
  G   �
  I   �
  	   C     M     `  %   m     �  "   �  .   �  3     5   9  :   o  E   �  4   �  T   %  G   z  �   �  (   �  �   �  {   I  S   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop-master-po-gl-54183.merged
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-04 22:39+0200
Last-Translator: Fran Dieguez <frandieguez@gnome.org>
Language-Team: gnome-l10n-gl@gnome.org
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.0
X-Project-Style: gnome
 %A %e de %B, %R %A %e de %B, %R:%S %A %e de %B, %l:%M %p %A %e de %B, %l:%M:%S %p %A, %e de %B %R %R:%S %l:%M %p %l:%M:%S %p o CRTC %d non pode conducir a saída %s CRTC %d non é admite rotation=%s CRTC %d: tentando o modo %dx%d@%dHz coa saída en %dx%d@%dHz (paso %d)
 Non é posíbel atopar un terminal, usando xterm, aínda que non funcione Portátil Espellar pantallas Descoñecido A extensión RANDR non está presente Tentando os modos para CRTC %d
 non é posíbel clonar a saída %s non foi posíbel asignar os CRTC a saídas:
%s non foi posíbel obter información sobre o CRTC %d non foi posíbel obter información sobre a saída %d non foi posíbel obter o intervalo de tamaños de pantalla non foi posíbel obter os recursos da pantalla (CRTC, saídas, modos) non foi posíbel definir a configuración do CRTC %d ningunha das configuracións de pantalla gardadas coincide coa configuración activa ningún dos modos seleccionados era compatíbel cos modos posíbeis:
%s a saída %s non ten os mesmos parámetros que a outra saída clonada:
modo existente = %d, new mode = %d
coordenadas existentes = (%d, %d), novas coordenadas = (%d, %d)
rotación existente = %s, nova rotación = %s a saída %s non admite o modo %dx%d@%dHz a posición ou tamaño solicitados ao CRTC %d está fóra do límite permitido: posición=(%d, %d), tamaño=(%d, %d), máximo=(%d, %d) o tamaño virtual solicitado non se axusta ao tamaño dispoñíbel: solicitado=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) produciuse un erro de X non manipulado ao obter o intervalo de tamaños de pantalla 