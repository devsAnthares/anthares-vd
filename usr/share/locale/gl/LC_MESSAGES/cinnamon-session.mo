��    ?        Y         p     q     �  5   �  7   �     	  (     )   9  %   c  '   �  "   �     �     �     �  !        %     6     9      M     n     �     �     �     �     �     �  '   �  	     '        @  P   G     �     �     �     �     �     �     		     	  &   8	     _	     p	      �	  +   �	     �	     �	     
     
     &
      9
  E   Z
     �
  &   �
     �
     �
  X   
  X   c  $   �  
   �     �     �  
   �     	  �    '   �     �  4     T   J     �  .   �  2   �  /   
  4   :  %   o      �     �     �  /   �          $  &   '  &   N     u     �      �     �  +   �     �       8   %     ^  -   e  	   �  Z   �     �       !   '     I  
   Y      d     �  *   �  %   �     �     �  .     ;   I  '   �     �     �     �  "   �  3   
  L   >     �  6   �  (   �     �  o     f   �  2   �  	        &  
   8     C     K     "       4   -   .       :   0   	           9       !                     7                 #   8   6             ;                               +          /   >      5             $             2       ,   ?       (              *       <                       &   '              =         
   3      )   %      1          A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:13+0000
Last-Translator: Leandro Regueiro <leandro.regueiro@gmail.com>
Language-Team: Galician <gnome-l10n-gl@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: gl
 Aínda se está executando un programa: CARTAFO_DE_AUTOINICIO O aplicativo non acepta documentos na liña de ordes Non é posíbel pasar os URI dos documentos a unha entrada de escritorio 'Type=Link' Cancelar Non foi posíbel conectar co xestor de sesión Non foi posíbel crear o socket de escoita ICE: %s Desactivar a conexión para o xestor de sesión Non cargar os aplicativos especificados polo usuario Non requirir confirmación do usuario Activar o código de depuración Saíu co código de erro %d FICHEIRO O ficheiro non é un ficheiro .desktop correcto Hibernar de todas formas ID Non foi posíbel atopar a icona «%s» Ignorando calquera inhibidor existente Matado co sinal %d Bloquear a pantalla Saír da sesión de todas formas Saír da sesión Quere saír da sesión neste sistema agora? Non é un elemento iniciábel Non responde Ignorar os directorios de inicio automático estándares Apagar Chamouse un programa con opcións en conflito Reiniciar Rexeitouse a conexión co novo cliente porque neste momento a sesión está sendo apagada
 Aplicativo recordado Reiniciar de todas formas Desexa reiniciar o sistema agora? NOME_DA_SESIÓN S_uspender Opcións de xestión de sesión: Sesión a usar Mostrar as opcións de xestión de sesión Mostrar o diálogo da balea de fallos Apagar de todas formas Quere apagar o sistema agora? Aínda se están executando algúns programas: Especifique o ficheiro que contén a configuración gardada Especifique o ID de xestión de sesión Iniciando %s Detido co sinal %d Suspender de todas formas Cambiar de usuario de todas formas Este programa está bloqueando a saída da sesión. Non é posíbel iniciar a sesión (e non é posíbel conectar co servidor X) Descoñecido Non se recoñece a versión de ficheiro desktop «%s» Non se recoñece a opción de inicio: %d Versión deste aplicativo Esperando a que os programas rematen. A interrupción destes programas podería causar a perda do seu traballo. Esperando a que os programas rematen. Se interrompe este programa pode causar a perda do seu traballo. Actualmente ten unha sesión iniciada como «%s». _Hibernar _Saír da sesión _Reiniciar _Apagar _Cambiar de usuario 