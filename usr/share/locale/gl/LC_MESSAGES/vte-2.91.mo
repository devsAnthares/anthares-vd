��          L      |       �   /   �      �   =   �   +   5     a  �  i  C     ,   E  E   r  7   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: gl
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-30 00:33+0200
Last-Translator: Fran Dieguez <frandieguez@gnome.org>
Language-Team: Proxecto Trasno <proxecto@trasno.gal>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
 Produciuse un erro (%s) ao converter os datos do fillo; eliminando. Produciuse un erro ao ler desde o fillo: %s. GnuTLS non está activado; os datos escribiranse no disco sen cifrar! Non foi posíbel converter os caracteres desde %s a %s. AVISO 