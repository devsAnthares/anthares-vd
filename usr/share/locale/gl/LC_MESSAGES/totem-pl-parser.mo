��    	      d      �       �      �      �      �      �      	  &     1   D     v  �    	   U     _     g     k        9   �  /   �  	   
                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: gl
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2017-03-11 03:16+0100
Last-Translator: Fran Dieguez <frandieguez@gnome.org>
Language-Team: Galician <gnome-l10n-gl@gnome.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 CD de son Blu-ray DVD Televisión dixital Produciuse un erro ao montar %s. Non hai ningún soporte na unidade do dispositivo «%s». Verifique que hai un disco presente na unidade. Vídeo CD 