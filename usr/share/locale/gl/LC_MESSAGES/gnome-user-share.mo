��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w          1  ?   Q     �     �     �  _   �  ^   :     �  ,   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share.master.gl
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-19 01:14+0200
Last-Translator: Fran Dieguez <frandieguez@gnome.org>
Language-Team: Galician <gnome-l10n-gl@gnome.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
X-Project-Style: gnome
 Ficheiros públicos de %s Ficheiros públicos de %s en %s Inicia a Compartición de ficheiros persoais, se está activado Compartir ficheiros persoais Compartición Preferencias de compartición Activar a Compartición de Ficheiros persoais para compartir contidos deste cartafol pola rede. Cando hai que pedir os contrasinais. Os valores posíbeis son "never", "on_write", e "always". Cando requirir os contrasinais compartir;ficheiros;http;rede;copiar;enviar; 