��          �      L      �  
   �     �  J   �           (     ,  
   9     D     L     T     [  	   g     q          �  2   �     �     �  �  �     �     �  R   �                    "     .     ;  	   D     N  
   _     j     z     �  =   �     �     �                                   	                                                         
        &Browse... &Search: *.ico *.png *.xpm *.svg *.svgz|Icon Files (*.ico *.png *.xpm *.svg *.svgz) Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2018-10-20 13:16+0100
Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>
Language-Team: Galician <kde-i18n-doc@kde.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
X-Environment: kde
X-Accelerator-Marker: &
X-Text-Markup: kde4
 &Examinar… &Buscar: *.ico *.png *.xpm *.svg *.svgz|Ficheiros de icona (*.ico *.png *.xpm *.svg *.svgz) Accións Todas Aplicativos Categorías Dispositivos Emblemas Emocións Orixe das iconas Tipos mime Ou&tras iconas: Lugares &Iconas do sistema: Buscar interactivamente os nomes das iconas (p.ex. cartafol). Seleccionar a icona Estado 