��          L      |       �   `   �      
     %  )   B     l  �  �  v   K  %   �  '   �  5     #   F                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-11-12 11:26+0000
Last-Translator: Miguel Anxo Bouzada <mbouzada@gmail.com>
Language-Team: Galician <gnome-l10n-gl@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: gl
 O protector de pantalla estivo activo durante %d segundo.
 O protector de pantalla estivo activo durante %d segundos.
 O protector de pantalla está activo
 O protector de pantalla está inactivo
 O protector de pantalla non está activo actualmente
 A tecla Bloq Maiús está activada. 