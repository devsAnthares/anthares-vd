��          �      �           	  $   (  D   M  <   �     �     �     �          &  %   @  '   f  )   �  '   �  :   �  +     I   G  F   �  �   �  *   �  t   �  i   F  9   �  B  �     -  "   L  I   o  O   �     		  	   	     $	  !   >	     `	  1   |	  %   �	  (   �	  /   �	  B   -
  +   p
  U   �
  D   �
  �   7  8   �  q   3  v   �  6              	                              
                                                               CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-10-04 21:48+0100
Last-Translator: Kibavuidi Nsiangani <lundombe01@zaya-dio.com>
Language-Team: Kikongo, kg <lundombe01@zaya-dio.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 crtc %d, bu kidiatisa %s nkatu CRTC %d kayizeye mveengulani=%s ko CRTC %d: ngita meka  nsongolo yi %dx%d@%dHz ye %dx%d@%dHz (bua mbote %d)
 nanga ilemba sala kansi ngieti sadila xterm kaka, kadi nkele ka yi monekene ko. nsikidisi a koko dianzenza ki randr ka i monekene ko nsongolo za crtc ngieti meka: %d
 kileendi fuanikisa ku %s ko zi crtc ye binsongalala, bu i ba waawasa nkatu
%s nsangu za crtc %d, bu yavewa zo nkatu nsangu za kiduukilu %d bu ibaka zo nkatu biteezolo bia kiansongalala, bu ivewa bio nkatu mambu ma kiansonga (crtc, biduukilu, nsongolo), bu yavewa mo nkatu nkubukulu za crtc %d, bu itatanesa zo nkatu mu nkubukulu zazo zi monekene mu nti yi, ka vena mosi ko yikuenda betila ye zaku bubu mu nsongolo zazo zi meni solua, ka yena mosi ko yina ya beetila:
 %s kiansongalala %s  ye kifuanisu kiandi, mosi mosi ye nkubukulu andi:
nsongolo yankulu = %d, yampa= %d
bintulani biankulu= (%d, %d), biampa = (%d, %d)
mveengulani akulu = %s, mveengulani ampa = %s  nduukilu %s ka yilendi sadila nsongolo zozo %dx%d@%dHz, nkubu ulombele mu crtc %d kena va kati kua ndelo zabetila ko: kitudilu=(%d, %d), teezolo=(%d, %d), yongi=(%d, %d) teezo biangindula ka bina bia betila ko ye bia lundombe luaku: bi uyuvudi=(%d, %d), bia nsi=(%d, %d), bianene=(%d, %d) mpitakani mu X bu ikele baka teezolo bia kiansongalala 