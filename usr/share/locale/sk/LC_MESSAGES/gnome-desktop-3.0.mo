��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     t     w     }     �     �     �     �     �     �     �     �     �                      &  '   /      W  K   x      �     �  %   �  ,   	  K   J	  �   �	  (   r
  }   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-09-07 17:29+0000
PO-Revision-Date: 2018-10-27 21:10+0200
Last-Translator: Dušan Kazik <prescott66@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
 %R %R:%S %A %R %A %R:%S %A %e. %B, %R %A %e. %B, %R:%S %A %e. %B_%k:%M %A %e. %B_%k:%M:%S %A %k:%M %A %k:%M:%S %A %e. %B_%R %A %e. %B_%R:%S %e. %B_%k:%M %A %e. %B_%k:%M:%S %k:%M %k:%M:%S CRTC %d nemôže obsluhovať výstup %s CRTC %d nepodporuje otočenie=%d CRTC %d: skúšanie režimu %dx%d@%dHz s výstupom %dx%d@%dHz (prechod %d)
 Skúšanie režimov pre CRTC %d
 Nešpecifikovaný nepodarilo sa klonovať na výstup %s nepodarilo sa priradiť CRTC k výstupom:
%s žiaden z vybraných režimov nebol kompatibilný s možnými režimami:
%s výstup %s nemá rovnaké parametre ako iný klonovaný výstup:
existujúci režim = %d, nový režim = %d
existujúce súradnice = (%d, %d), nové súradnice = (%d, %d)
existujúce otočenie = %d, nové otočenie = %d výstup %s nepodporuje režim %dx%d@%dHz vyžadovaná virtuálna veľkosť nezodpovedá dostupnej veľkosti: vyžadované=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 