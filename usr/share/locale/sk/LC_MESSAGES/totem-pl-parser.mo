��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       x     �     �     �     �  /   �  /   �     #                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-10-12 19:54+0200
Last-Translator: Dušan Kazik <prescott66@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Generator: Poedit 1.8.9
 Zvukové CD Blu-ray DVD Digitálna televízia Zlyhalo pripojenie „%s“. V mechanike zariadenia „%s“ nie je médium. Prosím, skontrolujte, či je disk v mechanike. Video CD 