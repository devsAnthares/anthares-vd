��          �   %   �      p     q     ~     �     �  	   �     �  $   �  D     <   J     �     �     �     �     �  %   �  '     )   F  '   p  :   �  +   �  I   �  F   I  �   �  *   ^  t   �  i   �  9   h  +  �     �     �     �     	  	   	  '   $	      L	  K   m	  Z   �	     
     )
  $   2
      W
  %   x
  ,   �
  ,   �
  0   �
  2   )  A   \  1   �  R   �  K   #  �   o  (   K  y   t  }   �  C   l                                   	                       
                                                                 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-04-21 23:17+0200
Last-Translator: Marcel Telka <marcel@telka.sk>
Language-Team: Slovak <gnome-sk-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
 %A %e. %B, %R %A %e. %B, %R:%S %A, %e. %B, %l:%M %p %A, %e. %B, %l:%M:%S %p %A, %B %e CRTC %d nemôže obsluhovať výstup %s CRTC %d nepodporuje otočenie=%s CRTC %d: skúšanie režimu %dx%d@%dHz s výstupom %dx%d@%dHz (prechod %d)
 Nepodarilo sa nájsť terminál. Použije sa xterm, aj napriek tomu, že nemusí fungovať Prenosný počítač Neznámy rozšírenie RANDR nie je prítomné Skúšanie režimov pre CRTC %d
 nepodarilo sa klonovať na výstup %s nepodarilo sa priradiť CRTC na výstupy:
%s nepodarilo sa získať informáciu o CRTC %d nepodarilo sa získať informácie o výstupe %d nepodarilo sa získať rozsah veľkostí obrazovky nepodarilo sa získať zdroje obrazovky (CRTC, výstupy, režimy) nepodarilo sa nastaviť konfiguráciu pre CRTC %d žiadna z uložených konfigurácií displeja nezodpovedá aktívnej konfigurácii žiaden z vybraných režimov nebol kompatibilný s možnými režimami:
%s výstup %s nemá rovnaké parametre ako iný klonovaný výstup:
existujúci režim = %d, nový režim = %d
existujúce súradnice = (%d, %d), nové súradnice = (%d, %d)
existujúca otočenie = %s, nové otočenie = %s výstup %s nepodporuje režim %dx%d@%dHz vyžiadaná poloha/veľkosť pre CRTC %d je mimo povolenú hranicu: poloha=(%d, %d), veľkosť=(%d, %d), maximum=(%d, %d) vyžadovaná virtuálna veľkosť nezodpovedá dostupnej veľkosti: vyžadované=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) neobslúžená chyba X pri získavaní rozsahu veľkostí obrazovky 