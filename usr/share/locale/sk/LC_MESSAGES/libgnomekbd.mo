��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j    �  3   �
     �
  =   �
          2  �   J     �     �  B     &   F      m  B   �     �     �  >     5   @  t   v     �     	     '     E     c  (   q     �  (   �     �  +   �       )     *   G     r     {     �     �  2   �     �            ,   $     Q     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-07-28 21:05+0000
PO-Revision-Date: 2016-08-05 12:55+0200
Last-Translator: Dušan Kazik <prescott66@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Generator: Poedit 1.8.8
 Predvolená skupina, priradená pri vytvorení okna Indikátor: Udržiavať a spravovať samostatnú skupinu pre každé okno Rozloženie klávesnice Rozloženie klávesnice Rozloženie klávesnice "%s"
Copyright &#169; X.Org Foundation a prispievajúci k XKeyboardConfig
Licencovanie si pozrite v metaúdajoch balíka Model klávesnice Možnosti klávesnice Načítať exotické, zriedka používané rozloženia a možnosti Nahrať prídavné položky nastavenia Náhľad rozloženia klávesnice Ukladať/obnovovať indikátory spoločne so skupinami rozloženia Sekundárne skupiny Zobraziť zástavy v aplete Zobraziť zástavy v aplete označujúce aktuálne rozloženie Zobraziť názvy rozložení namiesto názvov skupín Zobraziť názvy rozložení namiesto názvov skupín (iba u verzií XFree podporujúcich viacnásobné rozloženia) Náhľad klávesnice, posun X Náhľad klávesnice, posun Y Náhľad klávesnice, výška Náhľad klávesnice, šírka Farba pozadia Farba pozadia pre indikátor rozloženia Rodina písma Rodina písma pre indikátor rozloženia Veľkosť písma Veľkosť písma pre indikátor rozloženia Farba popredia Farba popredia pre indikátor rozloženia Nastala chyba pri načítaní obrázku: %s Neznáme Chyba inicializácie XKB rozloženie klávesnice model klávesnice rozloženia "%s" rozloženie "%s" rozloženia "%s" model "%s", %s a %s bez rozloženia bez možností možnosti "%s" možnosť "%s" možnosti "%s" preferences-desktop-keyboard 