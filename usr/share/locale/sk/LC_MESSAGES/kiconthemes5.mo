��          �      <      �  
   �     �     �     �     �  
   �     �     �     �        	             $     +  2   :     m     y  �  �       
   !     ,     2  
   :  
   E  
   P     [  	   d     n  	   z     �     �     �  6   �     �     �                   
                                                                          	        &Browse... &Search: Actions All Applications Categories Devices Emblems Emotes Icon Source Mimetypes O&ther icons: Places S&ystem icons: Search interactively for icon names (e.g. folder). Select Icon Status Project-Id-Version: kio4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2016-12-27 12:57+0100
Last-Translator: Roman Paholik <wizzardsk@gmail.com>
Language-Team: Slovak <kde-sk@linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 2.0
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 &Prechádzať... &Hľadať: Akcie Všetko Aplikácie Kategórie Zariadenia Emblémy Emotikony Zdroj ikony Typy MIME &Iné ikony: Miesta &Systémové ikony: Hľadať interaktívne názvy ikon (napr. priečinok). Vyberte ikonu Stav 