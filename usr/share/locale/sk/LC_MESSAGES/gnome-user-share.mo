��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  "   '  (   J  :   s  !   �     �     �  \   �  s   V     �  q   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-15 11:33+0200
Last-Translator: Dušan Kazik <prescott66@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.4
 Verejné súbory používateľa %s Verejné súbory používateľa %s na %s Ak je povolené, spustí sprístupnenie osobných súborov Sprístupnenie osobných súborov Sprístupnenie Nastavenia sprístupnenia Zapnutím sprístupnenia osobných súborov sprístupníte obsah tohto priečinku cez sieť. Kedy pýtať heslá. Možné hodnoty sú „never“ (nikdy), „on_write“ (pri zápise), a „always“ (vždy). Kedy vyžadovať heslá sprístupniť;sprístupnenie;zdieľať;zdieľanie;súbory;http;sieť;kopírovať;kopírovanie;odoslať;odoslanie; 