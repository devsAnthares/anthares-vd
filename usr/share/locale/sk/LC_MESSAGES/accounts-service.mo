��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  M   m  F   �  Q     *   T  /        �  #   �  *   �  a         v               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Pavol Šimo <palo.simo@gmail.com>
Language-Team: Slovak (http://www.transifex.com/projects/p/freedesktop/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Na zmenu nastavení prihlasovacej obrazovky je potrebné overenie totožnosti Na zmenu používateľských údajov je potrebné overenie totožnosti Na zmenu vlastných používateľských údajov je potrebné overenie totožnosti Zmeniť nastavenia prihlasovacej obrazovky Zmeniť svoje vlastné používateľské údaje Povoliť ladiaci kód Spravovať používateľské účty Vypísať informácie o verzii a skončiť Poskytuje rozhrania systému D-Bus na dotazovanie
a manipulovanie s používateľskými účtami. Nahradiť existujúcu inštanciu 