��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 z  %     �	     �	  4   �	  	   �	     �	     
  1   )
     [
     r
     {
  
   �
  7   �
  6   �
       !         B  T   Z     �  
   �     �     �     �  '        +     .     :      W  E   x  9   �  
   �  +        /  !   F     h     |     �     �  @   �                    0     5     D     R     i     r                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-19 17:09+0000
Last-Translator: Dusan Kazik <prescott66@gmail.com>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresa Vždy povoliť prístup Požiadavka na overenie totožnosti zo zariadenia %s Bluetooth Nastavenia Bluetooth Bluetooth je vypnutý Bluetooth je zakázaný hardvérovým vypínačom Prehliadať súbory… Zrušiť Upraviť nastavenia Bluetooth Pripojenie Zariadenie %s chce získať prístup k službe „%s“ Zariadenie %s sa chce spárovať s týmto počítačom Nezhoduje sa Chyba pri prehliadaní zariadenia Povoliť iba tentokrát Ak odstránite zariadenie, musíte ho opätovne nastaviť pred ďalším použitím. Nastavenia klávesnice Zhoduje sa Nastavenia myši Nastavenia myši a touchpadu Nie Nenašli sa žiadne Bluetooth adaptéry OK Spárované Potvrdenie párovania pre %s Požiadavka na spárovánie s %s Prosím, potvrďte, či sa PIN „%s“ zhoduje s tým zo zariadenia. Prosím, zadajte PIN, ktorý je spomenutý na zariadení. Odmietnuť Odstrániť „%s“ zo zoznamu zariadení? Odstrániť zariadenie Odoslať súbory do zariadenia... Odoslať súbory... Nastaviť nové zariadenie Nastaviť nové zariadenie... Nastavenia zvuku Požadované zariadenie sa nedá prehliadať. Chyba je „%s“. Typ Viditeľnosť Viditeľnosť „%s“ Áno pripája sa... odpája sa... hardvérovo zakázané strana 1 strana 2 