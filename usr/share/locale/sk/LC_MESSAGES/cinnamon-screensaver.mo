��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S     r  1   �     �  O   �     &  ;   7     s  7   �  #   �     �  P     �   Y     �  #   	  *   <	  /   g	  
   �	     �	  !   �	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&component=general
PO-Revision-Date: 2016-12-31 17:36+0000
Last-Translator: Dusan Kazik <prescott66@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: sk
 Spôsobí správne ukončenie šetriča obrazovky Kontroluje sa... Deaktivuje šetrič obrazovky, ak je aktívny (zruší vyprázdnenie obrazovky) Nesprávne heslo Správa, ktorá má byť zobrazená na uzamknutej obrazovke Prosím, zadajte vaše heslo... Požiada o dĺžku trvania aktivity šetriča obrazovky Požiada o stav šetriča obrazovky Prepnúť používateľa Oznámi spustenému procesu šetriča obrazovky, aby okamžite uzamkol obrazovku Šetrič obrazovky bol aktívny po dobu %d sekúnd.
 Šetrič obrazovky bol aktívny po dobu %d sekundy.
 Šetrič obrazovky bol aktívny po dobu %d sekúnd.
 Šetrič obrazovky je aktívny
 Šetrič obrazovky nie je aktívny
 Šetrič obrazovky teraz nie je aktívny.
 Zapne šetrič obrazovky (vyprázdni obrazovku) Odomknúť Verzia tejto aplikácie Máte zapnutú funkciu Caps Lock. 