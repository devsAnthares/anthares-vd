��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  +  w     �  %   �  H   �  $   1     V     c  l   �  l   �  "   [  a   ~                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-03-02 11:25+0100
Last-Translator: GunChleoc <fios@foramnagaidhlig.net>
Language-Team: Fòram na Gàidhlig
Language: gd
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 Na faidhlichean poblach aig %s Na faidhlichean poblach aig %s air %s Tòisich co-roinneadh fhaidhlichean pearsanta nuair a bhios seo an comas Co-roinneadh fhaidhlichean pearsanta Co-roinneadh Roghainnean a’ cho-roinnidh Cuir co-roinneadh fhaidhlichean pearsanta air gus susbaint a’ phasgain seo a cho-roinneadh air an lìonra. Cuin a thèid facal-faire iarraidh. Seo na luachan dligheach: “never”, “on_write” agus “always”. Cuin a thèid facal-faire iarraidh share;files;http;network;copy;send;co-roinneadh;faidhle;faidhlichean;lìonra;lethbhreac;cuir;cur; 