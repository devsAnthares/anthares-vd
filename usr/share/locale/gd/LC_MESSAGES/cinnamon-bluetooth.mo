��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 y  %     �	  '   �	     �	  	   �	     �	     

  ;   !
     ]
     y
  !   �
     �
  :   �
  F   �
     1      P     q  o   �                7  -   O     }  ,   �     �     �  &   �      �  >     2   Z     �  B   �     �  !   �          &     ;     S  G   i     �     �     �     �     �     �       
      
   +                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-12-16 19:26+0000
Last-Translator: GunChleoc <Unknown>
Language-Team: Fòram na Gàidhlig
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: gd
 Seòladh Ceadaich an t-inntrigeadh an-còmhnaidh Chaidh cead iarraidh o %s Bluetooth Roghainnean Bluetooth Tha Bluetooth à comas Chaidh Bluetooth a chur à comas le suidse bathair-chruaidh Brabhsaich  faidhlichean... Sguir dheth Rèitich na roghainnean Bluetooth Ceangal Tha an t-uidheam %s airson an t-seirbheis "%s" inntrigeadh Tha an t-uidheam %s ag iarraidh paidhreachadh leis a' choimpiutair seo Chan eil iad a-rèir a chèile Mearachd a' brabhsadh an uidheim Ceadaich an turas seo a-mhàin Ma bheir thu air falbh an t-uidheam seo, feumaidh tu a shuidheachadh a-rithist mus cleachd thu e an ath-thuras. Roghainnean a' mheur-chlàir Tha iad a-rèir a chèile Roghainnean na luchaige Roghainnean na luchaige 's a' phada-suathaidh Chan eil Cha deach freagarraichearan Bluetooth a lorg Ceart ma-thà Air paidhreachadh Dearbhadh a' phaidhreachaidh airson %s Iarrtas paidhreachaidh airson %s Dearbhaich a bheil am PIN "%s" a-rèir an fhir air an uidheam. cuir a-steach am PIN a dh'innis an t-uidheam dhut. Diùlt A bheil thu airson "%s" a thoirt air falbh o liosta nan uidheaman? Thoir uidheam air falbh Cuir faidhlichean dhan uidheam... Cuir faidhlichean... Suidhich uidheam ùr Suidhich uidheam ùr... Roghainnean na fuaime Cha ghabh brabhsadh san uidheam a chaidh iarraidh, is "%s" a' mhearachd Seòrsa Faicsinneachd Faicsinneachd "%s" Tha a' ceangal... 'ga dhì-cheangal... tha am bathar-cruaidh à comas duilleag 1 duilleag 2 