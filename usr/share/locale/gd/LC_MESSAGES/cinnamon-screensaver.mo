��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  >   T     �  U   �     �  3     $   A  @   f  0   �     �  P   �  �   I  &   	  +   C	  7   o	  5   �	     �	     �	  "   
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-03-10 17:39+0000
Last-Translator: GunChleoc <Unknown>
Language-Team: Fòram na Gàidhlig
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: gd
 Bheir seo air an t-sàbhalaiche-sgrìn gum fàg e gu dòigheil 'Ga dhearbhadh... Cuir an sàbhalaiche-sgrìn à gnìomh ma tha e gnìomhach (neo-dhuibhrich an sgrìn) Facal-faire cearr An teachdaireachd a nochdas air an sgrìn-ghlasaidh Cuir a-steach am facal-faire agad... Faighnich dè cho fad 's a tha an sàbhalaiche-sgrìn gnìomhach Faighnich airson staid an t-sàbhalaiche-segrìn Gearr leum gu cleachdaiche eile Innsidh seo do phròiseas an t-sàbhalaiche-sgrìn gun glais e an sgrìn sa bhad Tha an sàbhalaiche-sgrìn gnìomhach fad %d diog.
 Tha an sàbhalaiche-sgrìn gnìomhach fad %d dhiog.
 Tha an sàbhalaiche-sgrìn gnìomhach fad %d diogan.
 Tha an sàbhalaiche-sgrìn gnìomhach fad %d diog.
 Tha an sàbhalaiche-sgrìn gnìomhach
 Chan eil an sàbhalaiche-sgrìn gnìomhach
 Chan eil an sàbhalaiche-sgrìn gnìomhach an-dràsta.
 Cuir an sàbhalaiche-sgrìn air (duibhrich an sgrìn) Thoir a' ghlas dheth Tionndadh na h-aplacaid seo Tha an iuchair Caps Lock agad air. 