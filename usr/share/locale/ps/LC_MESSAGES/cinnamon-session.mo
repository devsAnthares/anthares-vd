��          �   %   �      p     q  5   �     �  (   �  %   �  '        E  !   J     l     o     �     �     �     �  P   �       +   5     a          �  &   �     �  
   �     �     �  
   �       8    (   G  G   p  
   �  7   �  C   �  @   ?  
   �  /   �  
   �  (   �     �     	  3   	  %   @	  `   f	  0   �	  J   �	  4   C
     x
     �
  >   �
  -   �
               #  	   5     ?                                                    
                                                  	                      - the Cinnamon session manager Application does not accept documents on command line Cancel Could not connect to the session manager Disable connection to session manager Do not load user-specified applications FILE File is not a valid .desktop file ID Icon '%s' not found Lock Screen Log out Log out of this system now? Not a launchable item Refusing new client connection because the session is currently being shut down
 Shut down this system now? Specify file containing saved configuration Specify session management ID Starting %s Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session.head
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&component=general
PO-Revision-Date: 2017-09-06 11:18+0000
Last-Translator: Zabeeh Khan <zabeehkhan@gmail.com>
Language-Team: Pashto <pathanisation@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: AFGHANISTAN
X-Poedit-Language: Pashto, Pushto
  د ګنوم ناستې سمبالګر - کاريال په بولۍ ليکه کې لاسوندونه نه مني بندول ناسته سمبالګر سره نه شي نښلېدی د ناسته سمبالګر سره نښلېدنه ناتوانول کارن-پېژندلي کاريالونه نه پرانيستل دوتنه دوتنه نه ده .desktop دوتنه سمه پېژند انځورن ونه موندل شو '%s' پرده کولپول وتل دې غونډال نه دستي وتل غواړئ؟ پېلېدونکی توکی نه دی د نوي پېرن نښلېدنه نه منل کيږي ځکه چې ناسته اوس ګليږي
 دا غونډال دستي ګلول غواړئ؟ هغه دوتنه چې ساتل شوې سازونه لري ويې ټاکئ د ناستې سمبالونې پېژند وټاکئ پېلېږي ‎%s ناپېژندلی '%s' ‎د سرپاڼه دوتنې ناپېژندلې نسخه %d ‎:ناپېژندلی پېل غوراوی پرکالول_ وتل_ بياپېلول_ ګلول_ کارن ونجول_ 