��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )    B  �   D
     �
     �
  4     4   �  �   �  .   �  @   �  �   �  r   �  e   	  �   o  :   0  k   k  �   �  �   �  K  <  A   �  A   �  E     E   R  (   �  W   �  4     c   N  O   �  f     .   i  ]   �  q   �     h  D   �  4   �  .   �  J   *  "   u  1   �  *   �  D   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.master.ml
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-02-22 23:49+0000
PO-Revision-Date: 2012-09-19 00:27+0000
Last-Translator: Ani Peter <apeter@redhat.com>
Language-Team: Malayalam <discuss@lists.smc.org.in>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.0
 സ്വതേയുള്ള ഗ്രൂപ്പ്, ജാലകനിര്‍മ്മിതിയുടെ സമയത്തു് കൊടുത്തതു് സൂചകം ഓരോ ജാലകത്തിനും വെവ്വേറെ ഗ്രൂപ്പ് പാലിയ്ക്കുക കീബോര്‍ഡ് വിന്യാസം കീബോര്‍ഡ് വിന്യാസം കീബോര്‍ഡ് വിന്യാസം "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata കീബോര്‍ഡ് മോഡല്‍ കീബോര്‍ഡ് ഐച്ഛികങ്ങള്‍ അപൂര്‍വമായുപയോഗിക്കുന്ന ശൈലികളും ഉപാധികളും ലഭ്യമാക്കുക അധികമായ ക്രമീകരണ വസ്തുക്കള്‍ ലഭ്യമാക്കുക കീബോര്‍ഡ് വിന്യാസത്തിന്റെ തിരനോട്ടം വിന്യാസ കൂട്ടങ്ങള്‍ക്കൊപ്പം സൂചികകള്‍ സൂക്ഷിക്കുക/തിരികെ കൊണ്ടു വരുക ദ്വിതീയ ഗ്രൂപ്പുകള്‍ ലഘുപ്രയോഗത്തില്‍ പതാകകള്‍ കാണിയ്ക്കുക ഇപ്പോളത്തെ വിന്യാസം കാണീയ്ക്കാന്‍  ലഘുപ്രയോഗത്തില്‍ പതാകകള്‍ കാണിയ്ക്കുക ഗ്രൂപ്പ് പേരിനുപകരം വിന്യാസത്തിന്റെ പേരു് കാണിയ്ക്കുക കൂട്ട പേരുകള്‍ക്കു പകരം വിന്യാസം പേരു്‌ ഉപയോഗിക്കുക (ഒന്നിലേറെ വിന്യാസങ്ങള്‍ താങ്ങുന്ന XFree-യുടെ പതിപ്പുകള്‍ക്കു്‌ മാത്രം) കീബോര്‍ഡ് തിരനോട്ടം, X offset കീബോര്‍ഡ് തിരനോട്ടം, Y offset കീബോര്‍ഡ് തിരനോട്ടം, ഉയരം കീബോര്‍ഡ് തിരനോട്ടം, വീതി പശ്ചാത്തല നിറം വിന്യാസ സൂചനയുടെ പശ്ചാത്തല നിറം അക്ഷരസഞ്ചയ കുടുംബം വിന്യാസ സൂചനയുടെ അക്ഷരസഞ്ചയ കുടുംബം അക്ഷരസഞ്ചയത്തിന്റെ വ്യാപ്തി വിന്യാസ സൂചനയുടെ അക്ഷരസഞ്ചയ വ്യാപ്തി മുമ്പിലുള്ള നിറം വിന്യാസ സൂചനയുടെ മുമ്പിലുള്ള നിറം ചിത്രം  %s ലോഡ് ചെയ്യുന്നതില്‍ പിശകു് പറ്റി അപരിചിതം XKB തുടങ്ങുന്നതില്‍ പിശകു് കീബോര്‍ഡ് വിന്യാസം കീബോര്‍ഡ് മോഡല്‍ വിന്യാസം "%s" വിന്യാസങ്ങള്‍ "%s" മോഡല്‍ "%s", %s and %s വിന്യാസം ലഭ്യമല്ല ഐച്ഛികങ്ങളില്ല ഐച്ഛികം "%s" ഐച്ഛികങ്ങള്‍ "%s" 