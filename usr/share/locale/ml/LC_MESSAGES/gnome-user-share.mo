��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  >   t  K   �  �   �  D   �     �  F       X  �   w  �   V  #   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: ml
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-08-06 11:43+0530
Last-Translator: Anish Sheela <aneesh.nl@gmail.com>
Language-Team: Swatantra Malayalam Computing <discuss@lists.smc.org.in>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
 %s ന്റെ പബ്ലിക് ഫയലുകള്‍ %s ന്റെ പബ്ലിക് ഫയലുകള്‍ %s ല്‍ പ്രവര്‍ത്തന സജ്ജമെങ്കില്‍ സ്വകാര്യ ഫയല്‍ പങ്കിടല്‍ ലഭ്യമാക്കുക സ്വകാര്യ ഫയല്‍ പങ്കിടല്‍ പങ്കിടല്‍ പങ്കിടല്‍ ക്രമീകരണങ്ങള്‍ ഈ ഫോള്‍ഡറിലെ ഉള്ളടക്കം നെറ്റ്‍വര്‍ക്ക് വഴി പങ്കിടാന്‍ സ്വകാര്യ ഫയല്‍ പങ്കിടല്‍ പ്രവര്‍ത്തനസജ്ജമാക്കുക. പാസ്‌വേര്‍ഡുകള്‍ക്കായി ആവശ്യപ്പെടണമോ എന്നു്. സാധ്യമായ മൂല്ല്യങ്ങള്‍ "never", "on_write", "always". പാസ്‌വേര്‍ഡുകള്‍ എപ്പോഴാണു് ആവശ്യപ്പടേണ്ടതു് share;files;http;network;copy;send; 