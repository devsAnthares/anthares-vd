��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )    B  k   F
     �
  �   �
  2   e  2   �  n  �  )   :  E   d  x   �  q   #  Q   �  �   �  %   t  Z   �  �   �  �   �  !  W  X   y  X   �  X   +  X   �     �  �   �     �  �   �     7  �   T  "   �  �     7   �     �  K   �  2   1  )   d  5   �  "   �  B   �  ?   *  /   j                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: bn_IN
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2012-02-22 23:49+0000
PO-Revision-Date: 2012-09-21 12:26+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali (India) <anubad@lists.ankur.org.in>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
Language: bn_IN
 ডিফল্ট দল, উইন্ডো নির্মাণকালে নির্ধারিত নির্দেশক: প্রতিটি উইন্ডোর জন্য পৃথক সংকলন নির্ধারণ ও পরিচালনা করুন কি-বোর্ডের বিন্যাস কি-বোর্ডের বিন্যাস কি-বোর্ড বিন্যাস "%s"
স্বত্বাধিকার &#169; X.Org Foundation ও XKeyboardConfig নির্মাতাবৃন্দ
লাইসেন্স সংক্রান্ত তথ্য জানার জন্য অনুগ্রহ করে প্যাকেজের মিটাডাটা দেখুন কি-বোর্ডের মডেল কি-বোর্ড সংক্রান্ত বিকল্প বিরল ও কম ব্যবহৃত বিন্যাস ও বিকল্প লোড করা হবে কনফিগারেশনের অতিরিক্ত সামগ্রী লোড করা হবে কি-বোর্ড বিন্যাসের পূর্বদৃশ্য বিন্যাসের সংকলনের সাথে সংরক্ষণ/পুনরুদ্ধার নির্দেশক দ্বিতীয় সংকলন অ্যাপ্লেটে ফ্ল্যাগ প্রদর্শিত হবে বর্তমানে ব্যবহৃত বিন্যাস নির্দেশের জন্য অ্যাপ্লেটে ফ্ল্যাগ প্রদর্শন করা হবে সংকলনের নামের পরিবর্তে বিন্যাসের নাম প্রদর্শন করা হবে সংকলনের নামের পরিবর্তে বিন্যাসের নাম প্রদর্শন করা হবে (শুধুমাত্র একাধিক বিন্যাস বিশিষ্ট XFree-র সংস্করণের জন্য) কি-বোর্ডের পূর্বপ্রদর্শন, X অফ-সেট কি-বোর্ডের পূর্বপ্রদর্শন, Y অফ-সেট কি-বোর্ডের পূর্বপ্রদর্শন, উচ্চতা কি-বোর্ডের পূর্বপ্রদর্শন, প্রস্থ পটভূমির রং বিন্যাস নির্দেশকের ক্ষেত্রে ব্যবহারযোগ্য পটভূমির রং ফন্ট সংকলন বিন্যাস নির্দেশকের ক্ষেত্রে ব্যবহারযোগ্য ফন্ট সংকলন ফন্টের মাপ বিন্যাস নির্দেশকের ক্ষেত্রে ব্যবহারযোগ্য ফন্টের মাপ অগ্রভূমির রং বিন্যাস নির্দেশকের ক্ষেত্রে ব্যবহারযোগ্য অগ্রভূমির রং ছবি লোড করতে সমস্যা: %s অজানা XKB প্রারম্ভ সংক্রান্ত সমস্যা কি-বোর্ডের বিন্যাস কি-বোর্ডের মডেল বিন্যাস "%s" বিন্যাস "%s" মডেল "%s", %s এবং %s কোনো বিন্যাস উপস্থিত নেই কোনো বিকল্প উপস্থিত নেই বিকল্প "%s" বিকল্প "%s" 