��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  v        �	     �	     �	     �	     �	     �	     �	     �	  a   �	  Q   ]
  �   �
  �   ;     ,  1   B     t  A   �  R   �  H     e   b  f   �  e   /  e   �  �   �  c   �  �   �  �   �  �  i  n   B  �   �  �   �  �   z             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: bn_IN
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-21 19:36+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bengali (India) <anubad@lists.ankur.org.in>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1);

X-Generator: Lokalize 1.2
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d দ্বারা %s আউপুট ড্রাইভ করা সম্ভব নয় CRTC %d দ্বারা rotation=%s সমর্থন করা হয় না CRTC %d: %dx%d@%dHz মোড প্রয়োগ করা হচ্ছে, %dx%d@%dHz (pass %d)-র আউটপুটের সাথে
 টার্মিন্যাল পাওয়া যায়নি। xterm ব্যবহারের প্রচেষ্টা করা হচ্ছে এবং এই চেষ্টাও ব্যর্থ হতে পারে। ল্যাপটপ প্রতিরূপ প্রদর্শন অজানা RANDR এক্সটেনশন উপস্থিত নেই CRTC %d-র মোডের প্রচেষ্টা করা হচ্ছে
 %s আউটপুটে ক্লোন করতে ব্যর্থ আউটপুটের ক্ষেত্রে CRTCs ধার্য করা যায়নি:
%s CRTC %d সংক্রান্ত তথ্য প্রাপ্ত করতে ব্যর্থ ফলাফল %d সম্বন্ধে কোনো তথ্য পাওয়া যায়নি পর্দার মাপের সীমা প্রাপ্ত করতে ব্যর্থ পর্দা সম্বন্ধীয় সামগ্রী (CRTC, আউটপুট, মোড) পাওয়া যায়নি CRTC %d-র কনফিগারেশন নির্ধারণ করতে ব্যর্থ প্রদর্শন সংক্রান্ত সংরক্ষিত কোনো কনফিগারেশনের সাথে সক্রিয় কনফিগারেশনের মিল পাওয়া যায়নি নির্বাচিত মোডগুলি কোনো সম্ভাব্য মোডের সাথে সুসংগত নয়:
%s %s আউটপুটের ক্ষেত্রে ক্লোন করা অন্য একটি আউটপুটের সমতূল্য পরামিতি উপস্থিত নেই:
উপস্থিত মোড = %d, নতুন মোড = %d
উপস্থিত কো-ওরডিনেট = (%d, %d), নতুন কো-ওরডিনেট = (%d, %d)
উপস্থিত রোটেশন = %s, নতুন রোটেশন = %s %s আউটপুট দ্বারা %dx%d@%dHz মোড সমর্থন করা যাবে না CRTC %d-র জন্য অনুরোধ করা অবস্থান/মাপ সীমাবহির্ভূত: অবস্থান=(%d, %d), মাপ=(%d, %d), সর্বাধিক=(%d, %d) আবশ্যক ভার্চুয়াল মাপ, উপলব্ধ মাপের সাথে মেলেনি: অনুরোধ করা মাপ=(%d, %d), সর্বনিম্ন=(%d, %d), সর্বাধিক=(%d, %d) পর্দার মাপের সীমা প্রাপ্ত করার সময় উৎপন্ন X সংক্রান্ত একটি ত্রুটির ব্যবস্থাপনা করা সম্ভব হয়নি 