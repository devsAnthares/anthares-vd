��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  Z   b  C   �  M     ,   O  #   |  "   �     �  *   �  a        i               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German (http://www.transifex.com/projects/p/freedesktop/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Zur Änderung der Konfiguration des Anmeldebildschirms ist eine Legitimierung erforderlich Zur Änderung von Benutzerdaten ist eine Legitimierung erforderlich Zur Änderung Ihrer eigenen Benutzerdaten ist eine Legitimierung erforderlich Konfiguration des Anmeldebildschirms ändern Ändern Ihrer eigenen Benutzerdaten Code zur Fehlerdiagnose aktivieren Benutzerkonten verwalten Versionsinformationen ausgeben und beenden Stellt D-Bus-Schnittstellen zur Abfrage und Änderung
von Informationen zu Benutzerkonten bereit. Existierende Instanz ersetzen 