��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  
  �  8   �
     �
  5   �
     
       �   ,     �     �  9   �  (        E  M   e     �     �  B   �  *   '  u   R      �      �     
     &     C  /   X     �  -   �     �  -   �     
  /     4   O  	   �     �     �     �  !   �     �               #     A     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-07-28 21:05+0000
PO-Revision-Date: 2016-08-04 11:57+0200
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: Deutsch <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.8
 Vorgegebene Gruppe, zugewiesen bei der Fenstererstellung Anzeige: Separate Gruppe pro Fenster beibehalten und verwalten Tastaturbelegung Tastaturbelegung Tastaturbelegung »%s«
Copyright &#169; X.Org Foundation und XKeyboardConfig-Mitwirkende
Für Informationen über die Lizenz siehe Paket-Metadaten Tastatur-Modell Tastatur-Optionen Exotische, wenig verwendete Belegungen und Optionen laden Zusätzliche Konfigurationsobjekte laden Vorschau auf Tastaturbelegungen Speichern und Wiederherstellen der Anzeigen zusammen mit den Belegungsgruppen Sekundäre Gruppen Markierungen im Applet zeigen Markierungen im Applet zeigen, um die aktuelle Belegung anzuzeigen Belegungsnamen anstatt Gruppennamen zeigen Belegungsnamen anstatt Gruppennamen anzeigen (Nur für XFree-Versionen, die mehrere Tastaturbelegungen unterstützen) Die Tastaturvorschau, X-Position Die Tastaturvorschau, Y-Position Die Tastaturvorschau, Höhe Die Tastaturvorschau, Breite Die Hintergrundfarbe Die Hintergrundfarbe für den Tastaturindikator Die Schriftfamilie Die Schriftfamilie für den Tastaturindikator Die Schriftgröße Die Schriftgröße für den Tastaturindikator Die Vordergrundfarbe Die Vordergrundfarbe für den Tastaturindikator Beim Laden des Bildes ist ein Fehler aufgetreten: %s Unbekannt XKB-Initialisierungsfehler Tastaturbelegung Tastatur-Modell Belegung »%s« Belegungen »%s« Modell »%s«, %s und %s Keine Belegung Keine Optionen Option »%s« Optionen »%s« preferences-desktop-keyboard 