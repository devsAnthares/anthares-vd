��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  1   �     �  P   �       4   .     c  3   �  *   �     �  D   �  d   9      �  "   �  /   �  7   	  
   J	     U	  !   n	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-01-02 18:07+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Veranlasst den Bildschirmschoner sanft zu beenden Überprüfung läuft … Wenn der Bildschirmschoner aktiv ist, diesen deaktivieren (Abdunkelung aufheben) Falsches Passwort Nachricht die auf dem Sperrbildschirm angezeigt wird Bitte Ihr Passwort eingeben … Die Länge der Bildschirmschoneraktivität abfragen Den Status des Bildschirmschoners abfragen Benutzer wechseln Teilt dem laufenden Bildschirm mit, den Bildschirm sofort zu sperren Der Bildschirmschoner war für %d Sekunde aktiv.
 Der Bildschirmschoner war für %d Sekunden aktiv.
 Der Bildschirmschoner ist aktiv
 Der Bildschirmschoner ist inaktiv
 Der Bildschirmschoner ist derzeit nicht aktiv.
 Den Bildschirmschoner aktivieren (Bildschirm abdunkeln) Entsperren Version dieser Anwendung Die Feststelltaste ist aktiviert. 