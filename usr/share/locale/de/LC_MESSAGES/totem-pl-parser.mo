��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       \     e     m     q     �  +   �  @   �                          	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=totem-pl-parser&keywords=I18N+L10N&component=General
PO-Revision-Date: 2016-10-13 11:20+0200
Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.9
 Audio-CD Blu-ray DVD Digitales Fernsehen Einbinden von %s gescheitert. Kein Medium im Laufwerk des Geräts »%s«. Bitte überprüfen Sie, ob sich ein Medium im Laufwerk befindet. Video-CD 