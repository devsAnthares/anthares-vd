��          L      |       �   /   �      �   =   �   +   5     a  `  i  ?   �     
  ]   *  8   �     �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-07-27 14:47+0200
Last-Translator: Tim Sabsch <tim@sabsch.com>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.9
 Fehler (%s) beim Konvertieren der Daten für Kind, abgebrochen. Fehler beim Lesen von Kind: %s. GnuTLS ist nicht aktiviert, die Daten werden unverschlüsselt auf die Festplatte geschrieben! Zeichen konnten nicht von %s nach %s konvertiert werden. WARNUNG 