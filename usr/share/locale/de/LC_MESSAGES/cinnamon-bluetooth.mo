��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 |  %     �	     �	     �	  	   �	     �	      
  3   
     N
  	   f
  "   p
  
   �
  0   �
  1   �
       $        <  ]   U     �     �     �  $   �                9  	   E     O     m  K   �  1   �       +        9     J     h     {     �     �  D   �                    4     7     U     r     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-18 14:13+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Zugriff immer erlauben Legitimierungsanfrage von %s Bluetooth Bluetooth-Einstellungen Bluetooth ist deaktiviert Bluetooth ist durch den Geräteschalter deaktiviert Dateien durchsuchen … Abbrechen Bluetooth-Einstellungen bearbeiten Verbindung Gerät %s möchte Zugriff auf den Dienste »%s« Gerät %s möchte sich mit diesem Rechner koppeln Stimmt nicht überein Fehler beim Durchsuchen des Gerätes Nur dieses Mal gewähren Falls Sie das Gerät entfernen, müssen Sie es vor der nächsten Benutzung erneut einrichten. Tastatureinstellungen Übereinstimmungen Mauseinstellungen Einstellungen für Maus und Touchpad Nein Keine Bluetooth-Adapter gefunden Bestätigen Gekoppelt Kopplungsbestätigung für %s Kopplungsanfrage für %s Bitte bestätigen, ob die PIN »%s« mit der auf dem Gerät übereinstimmt. Bitte die auf dem Gerät angegebene PIN eingeben. Ablehnen »%s« aus der Liste der Geräte entfernen? Gerät entfernen Dateien zum Gerät senden … Dateien senden … Neues Gerät einrichten Ein neues Gerät einrichten … Klangeinstellungen Das angeforderte Gerät kann nicht durchsucht werden. Fehler: »%s« Typ Sichtbarkeit Sichtbarkeit von »%s« Ja Verbindung wird aufgebaut … Verbindung wird getrennt … Gerät deaktiviert Seite 1 Seite 2 