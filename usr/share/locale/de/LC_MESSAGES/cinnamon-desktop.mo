��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     l  J     �	     �	     �	     �	  
   �	     

     
     
     
  1   "
  ,   T
  V   �
  _   �
     8     ?  	   W  )   a     �  /   �  6   �  <     F   L  ;   �  d   �  =   4  F   r  E   �  �   �  :   �     "  z   �  ;                         !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-19 10:03+0000
Last-Translator: Hedda Peters <hpeters@redhat.com>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.0
 %A, %e. %B, %R %A, %e. %B, %R:%S %A, %e. %B, %l:%M %A, %e. %B, %l:%M:%S %A, %e. %B %R %R:%S %l:%M %l:%M:%S CRTC »%d« unterstützt die Ausgabe »%s« nicht CRTC »%d« unterstützt Drehung um %s nicht CRTC »%d«: Teste Modus »%dx%d@%dHz« mit Ausgabe bei »%dx%d@%dHz« (Durchlauf %d)
 Kein Terminal gefunden. xterm wird verwendet, auch wenn es gegebenfalls nicht einsatzbereit ist Laptop Gespiegelte Bildschirme Unbekannt Die RANDR-Erweiterung ist nicht vorhanden Teste Modi für CRTC »%d«
 Auf Ausgabe »%s« kann nicht dupliziert werden CRTCs konnten nicht als Ausgaben festgelegt werden:
%s Informationen über CRTC »%d« konnten nicht erfragt werden Es konnten keine Informationen über die Ausgabe »%d« erfragt werden Mögliche Bildschirmgrößen konnten nicht abgefragt werden Es konnten keine Informationen über den Bildschirm gesammelt werden (CRTCs, Ausgaben, Auflösungen) Konfiguration für CRTC »%d« konnte nicht angewendet werden Keine der gespeicherten Bildschirmkonfigurationen gleichen der Aktiven Keiner der gewählten Modi ist kompatibel mit den möglichen Modi:
%s Ausgabe »%s« hat nicht die gleichen Parameter wie eine andere duplizierte Ausgabe:
bestehender Modus = %d, neuer Modus = %d
bestehende Koordinaten = (%d, %d), neue Koordinaten = (%d, %d)
bestehende Drehung = %s, neue Drehung = %s Ausgabe »%s« unterstützt den Modus »%dx%d@%dHz« nicht Gewählte Position/Größe für CRTC »%d« ist über dem erlaubten Limit:Position=(%d, %d), Größe=(%d, %d), Maximum=(%d, %d) Gewählte virtuelle Größe passt nicht zur verfügbaren Größe: Erwünschte=(%d, %d), Minimum=(%d, %d), Maximum=(%d, %d) Unbehandelter X Fehler beim Abfragen der Bildschirmgrößen 