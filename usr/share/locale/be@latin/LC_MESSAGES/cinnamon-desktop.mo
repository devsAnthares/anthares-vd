��          �      �       0  <   1     n     u  '   �  )   �  '   �  :     +   I  I   u  t   �  i   4  9   �  n  �  P   G     �     �  -   �  1   �  <     ?   U  -   �  T   �  �     �   �  Z   1                      
   	                                 Cannot find a terminal, using xterm, even if it may not work Laptop RANDR extension is not present could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-02-25 23:14+0200
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Language: 
X-Poedit-Country: BELARUS
X-Poedit-Language: Belarusian
 Nie ŭdałosia znajści terminał. Užyvajem "xterm", navat kali jon nie pracuje Laptop Pašyreńnia RANDR niama nie ŭdałosia atrymać źviestak pra CRTC %d nie ŭdałosia atrymać źviestak pra vyjście %d nie ŭdałosia atrymać absiah mahčymych pamieraŭ ekranaŭ nie ŭdałosia atrymać resursy ekranu (CRTC, vyjści, režymy) nie ŭdałosia akreślić nałady dla CRTC %d nivodny z zapisanych naboraŭ naładaŭ dla ekranu nie adpaviadaje dziejnym naładam zapatrabavanaja pazycyja/pamier dla CRTC %d nie adpaviadaje dazvolenym miežam: pazycyja=(%d, %d), pamier=(%d, %d), najbolšaja miaža=(%d, %d) zapatrabavany virtualny pamier nia źmieścicca ŭ najaŭnych pamierach: zapatrabavana=(%d, %d), minimalna=(%d, %d), maksymalna=(%d, %d) uźnikła nieabsłužanaja pamyłka X pry atrymańni absiahu mahčymych pamieraŭ ekranaŭ 