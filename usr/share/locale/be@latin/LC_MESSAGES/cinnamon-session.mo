��    (      \  5   �      p     q  5   �  7   �     �  (     )   +  %   U  '   {     �     �  !   �     �     �     �                    8     N  '   ]  '   �  P   �     �           "  +   C     o     �     �     �     �  &   �     �  X   	  
   b     m     v  
        �  �  �     �	  5   �	  B   �	     $
  ,   +
  A   X
  %   �
  6   �
     �
       2        E     [  &   ^     �     �     �     �     �  0   �  2     B   J     �     �     �  1   �            !     1      D  
   e  '   p  )   �  i   �  
   ,     7     ?  	   M     W                      "   &          #                '      	                                    (            %                     !                     
                $             A program is still running: Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Enable debugging code FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Lock Screen Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Suspend Anyway Switch User Anyway Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Waiting for programs to finish.  Interrupting these programs may cause you to lose work. _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:21+0000
Last-Translator: Ihar Hrachyshka <Unknown>
Language-Team: Belarusian Latin <be-latin.open-tran.eu>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
 Dasiul pracuje prahrama: Aplikacyja nie pryjmaje dakumentaŭ z zahadnaha radka Nielha pieradavać adrasy dakumentaŭ elementam stała "Type=Link" Anuluj Niemahčyma spałučycca z kiraŭnikom sesij Nie ŭdałosia stvaryć sokiet ICE dziela prasłuchoŭvańnia: %s Nie spałučaj z kiraŭnikom sesijaŭ Nie ŭklučaj aplikacyjaŭ, akreślenych karystalnikam Uklučy debugavy kod FAJŁ Fajł nie źjaŭlajecca pravilnym fajłam .desktop Usio roŭna zhibernuj ID Nie ŭdałosia znajści ikonu „%s” Zablakuj ekran Vyjdzi Zaraz ža vyjści z systemy? Element nia dziela uklučeńnia Nie adkazvaje Ułasnyja katalohi aŭtamatyčnaha ŭklučeńnia Prahrama ŭklučanaja ź niaŭzhodnienymi opcyjami Admoŭlena ŭ novym spałučeńni, bo sesija ŭžo zakončvajecca
 Z_aśni Zaraz ža vyklučyć systemu? Dasiul pracujuć prahramy: Akreśli fajł, dzie zachoŭvajecca kanfihuracyja Akreśli ID kiravańnia sesijami Uklučeńnie %s Usio roŭna zaśni Usio roŭna źmiani karystalnika Nieviadoma Niezrazumieły fajł stała versii "%s" Niezrazumiełaja opcyja ŭklučeńnia: %d Čakajem vychadu z prahramaŭ. Kali ty pierapyniš hetyja prahramy, možaš stracić karysnyja źviestki. Z_hibernuj _Vyjdzi _Pierazahruzi _Vyklučy Ź_miani karystalnika 