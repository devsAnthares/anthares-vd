��    2      �  C   <      H  x   I  8   �     �       
   "     -     >     \     w  *   �      �     �     �  
     )   "     L     g     �     �     �  z   �     C     R     c  3   s     �     �  7   �  (   	  a   2  2   �     �     �     	     "	     >	  .   Y	  '   �	     �	     �	     �	     �	     
     
     .
     D
  	   U
  
   _
     j
  �  �
  s   `  3   �                5     B     ]     v     �  6   �  (   �  "        /  
   L  ,   W      �  "   �      �     �       �        �     �     �  :   �     "     3  D   O  1   �  �   �  V   H  (   �  (   �  !   �  !        5  0   P  .   �  '   �     �     �     	        /   2     b     v     �     �  !   �                       (                      !                      1       -             ,   /         '   #                     +   )                .      &      %                    "   0                      	   *       2   $   
       A collection of scripts to run whenever the keyboard state is reloaded. Useful for re-applying xmodmap based adjustments A list of modmap files available in the $HOME directory. Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Failed to init GConf: %s
 Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator Test (%s) Keyboard Indicator plugins Keyboard Update Handlers Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options No description. Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) Suppress the "X sysconfig changed" warning message The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s modmap file list no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd.HEAD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-11 12:49+0300
PO-Revision-Date: 2008-01-06 01:26+0200
Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>
Language-Team: Belarusian Latin <i18n@mova.org>
Language: be@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Zbor skryptoŭ, kab vykanać pry pieračytańni stanu klavijatury. Karysna, kali treba aktyvizavać nałady xmodmap Śpis fajłaŭ modmap, dastupnych z katalohu $HOME. Uklučy bolš pluginaŭ Aktyŭnyja _pluginy: Dadaj plugin Začyni dyjalohavaje vakno Kanfihuruj abrany plugin Deaktyvizuj abrany plugin Pamienš pryjarytet pluginu Zmoŭčanaja hrupa, pryznačanaja pry stvareńni vakna Uklučy/adklučy zainstalavanyja pluginy Niemahčyma inicyjavać GConf: %s
 Pavialič pryjarytet pluginu Indykatar: Zachoŭvaj dla kožnaha vakna asobnuju hrupu Pluginy indykataraŭ klavijatury Test indykataraŭ klavijatury (%s) Pluginy indykataraŭ klavijatury Aktualizavańnie klavijatury Raskładka klavijatury Raskładka klavijatury "%s"
Aŭtarskija pravy &#169; Fundacyja X.Org i ŭdzielniki XKeyboardConfig
Umovy licenzii hladzi ŭ metaźviestkach pakunka Madel klavijatury Opcyi klavijatury Niama apisańnia. Zachoŭvaj/adnaŭlaj indikatary razam z hrupami raskładak Druhasnyja hrupy Pakazvaj ściahi ŭ aplecie Pakazvaj ściahi ŭ aplecie, kab identyfikavać dziejnuju raskładku Pakazvaj nazvy raskładak zamiest nazvaŭ hrupaŭ Pakazvaj nazvy raskładak zamiest nazvaŭ hrupaŭ (tolki dla versij XFree, jakija ŭmiejuć pracavać ź niekalkimi raskładkami) Nie pakazvaj papieradžalnyja paviedamleńni, što "Systemnyja nałady X źmianilisia" Pieradahlad klavijatury, vyraŭnańnie X Pieradahlad klavijatury, vyraŭnańnie Y Pieradahlad klavijatury, vyšynia Pieradahlad klavijatury, šyrynia Śpis aktyŭnych pluginaŭ Śpis uklučanych pluginaŭ indykacyi raskładak Pry adčytańni vyjavy adbyłasia pamyłka: %s Nie ŭdałosia adčynić fajł dapamohi Pamyłka inicyjalizacyi XKB _Dastupnyja pluginy: raskładka klavijatury madel klavijatury raskładka "%s" raskładki "%s" raskładki "%s" madel "%s", %s i %s śpis fajłaŭ modmap niama raskładki niama opcyj opcyja "%s" opcyi "%s" opcyi "%s" 