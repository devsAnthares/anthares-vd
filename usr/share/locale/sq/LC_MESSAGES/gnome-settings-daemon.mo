��    G      T  a   �                #     (  K   .  [   z  &   �     �  (     S   ,      �  "   �  $   �  $   �  &        5     E     W  >   ]     �  9   �     �  A   �  7   )	  8   a	  #   �	     �	     �	     �	     
     
     +
     =
     I
     Q
     W
     ^
     d
     v
     
     �
     �
     �
     �
     �
     �
     �
     �
  #        /  #   =  -   a  z   �  &   
  m   1     �     �     �     �     �  $   �  	   �  *     �   1  �   �  �   L  	   �     �          $     *  }  9     �     �     �  X   �  a   1  ;   �     �  1   �  c     &   k      �     �      �     �               .  M   <     �  D   �     �  @   �  3   *  A   ^  .   �     �  "   �               9     P     e     t     �     �     �     �     �     �     �     �       "        6  ,   <     i      �  -   �     �  -   �  )     {   >  7   �  �   �     s     �  	   �     �  !   �  6   �     �  &     �   .  �   �  �   z     ?  	   G     Q     n     v            6               C   >      7   @   "              8                        =   #      4   G       $       -      3   F   5   /         B                :               9      	   ?               .   *   2                         +      !   D       ;   (   ,         &                        %   <   '       
   0      )   E           A       1       A_vailable files: Beep Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder It seems that another application already has access to key '%u'. Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Siren Slow Keys Alert Sound Sound not set for this event. Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. The sound file for this event does not exist.
You may want to install the gnome-audio package for a set of default sounds. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2006-04-19 12:33+0200
Last-Translator: Elian Myftiu <elian@alblinux.net>
Language-Team: albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 File në d_ispozicion: Bip Boing I pamundur krijimi kartelës "%s".
Nevoitet për të mundësuar ndryshimin e kursorëve. I pamundur krijimi kartelës "%s".
Nevoitet për të mundësuar ndryshimin e temës së kursorit. I pamundur përcaktimi i directory shtëpi të përdoruesit Clink I pamundur ngarkimi i tingullit %s si shembull %s E pamundur vënja e kompjuterit në gjumë.
Kontrollo që makina të jetë konfiguruar me saktësi. _Mos e shfaq më këtë paralajmërim. Aktivizon ngadalsimin e tasteve? Aktivon qendrimin e tasteve? Çaktivon ngadalsimin e tasteve? Çaktivon qendrimin e tasteve? Mo_s aktivizo Mo_s çaktivizo Nxirr jashtë Gabim në përpjekjen për të ekzekutuar (%s)
që shoqërohet me tastin (%s) Gërmat Çelësi GConf %s është caktuar si lloj %s por duhet të jetë %s
 Kartela personale Mesa duket një program tjetër përdor rregullisht tastin '%u'. Veprimi i shtegut (%s) është përdorur njëherë
 Kombinimi i tasteve të shtegut (%s) është përdorur njëherë
 Shtegu (%s) është rregullisht në përdorim
 Shtegu (%s) është i paplotë
 Shtegu (%s) është i pavlefshëm
 Tastiera Lësho shfletuesin e ndihmës Lësho shfletuesin web Ngarko file-t modmap Blloko ekranin Përfundo seancën Fillimi i seancës Përfundimi i seancës Miu Preferimet e miut Asnjë tingull Luaj (ose luaj/pusho) Kërko Zgjidh file e tingullit Sirenë Paralajmërim ngadalsimi i tasteve Zëri Asnjë tingull i caktuar për këtë ndodhi. Fillo ruajtësin e ekranit Paralajmërim qendrimi i tasteve Sinkronizo trajtuesit e text/plain dhe text/* Tingujt e sistemit File %s nuk është një file i vlefshëm wav Tingulli për këtë veprim nuk ekziston. Tingulli për këtë veprim nuk ekziston.
Duhet të instaloni paketin gnome-audio për një sërë tingujsh të zakonshëm. U verifikua një gabim gjatë shfaqjes së ndihmës: %s U ndesh një gabim në nisjen e ruajtësit të ekranit:

%s

Funksioni i ruajtes së ekranit nuk do punojë për këtë seancë. Pushim gjatë shkrimit Volumi Ul zërin Pa zë Shkalla për ndryshimin e volumit Hapi për ndryshimin e zërit si përqindje e volumit. Ngre zërin Dëshiron të ngarkosh file(t) modmap? Është mbajtur i shtypur për 8 sekonda tasti Shift.  Ky veprim është shtegu për funksionin e ngadalsimit të tasteve, që ndryshon funksionimin aktual të tastierës. Është shtypur 5 herë rresht tasti Shift.  Ky veprim është shtegu për funksionin e qendrimit të tasteve, që ndryshon funksionimin aktual të tastierës. Janë shtypur njëkohësisht dy taste, apo është shtypur tasti·Shift·5·herë· rradhazi.··Kjo·çaktivon funksionin e qendrimit të tasteve, që ndryshon funksionimin aktual të tastierës. _Aktivo Ç_aktivo _Mos shfaq më këtë mesazh _Ngarko File-t e nga_rkuar: 