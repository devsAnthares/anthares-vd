��          D      l       �   &   �   �   �   �   <  �   �  �  m  7   %  �   ]  �   	  �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:28+0000
Last-Translator: Elian Myftiu <Unknown>
Language-Team: albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:18+0000
X-Generator: Launchpad (build 18688)
 U verifikua një gabim gjatë shfaqjes së ndihmës: %s Është mbajtur i shtypur për 8 sekonda tasti Shift.  Ky veprim është shtegu për funksionin e ngadalsimit të tasteve, që ndryshon funksionimin aktual të tastierës. Është shtypur 5 herë rresht tasti Shift.  Ky veprim është shtegu për funksionin e qendrimit të tasteve, që ndryshon funksionimin aktual të tastierës. Janë shtypur njëkohësisht dy taste, apo është shtypur tasti·Shift·5·herë· rradhazi.··Kjo·çaktivon funksionin e qendrimit të tasteve, që ndryshon funksionimin aktual të tastierës. 