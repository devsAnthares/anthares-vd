��          L      |       �   `   �      
     %  )   B     l  �  �  d   Q  "   �  &   �  -      #   .                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-01 18:45+0000
Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>
Language-Team: Albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Ruajtësi i ekranit ishte aktiv për %d sekondë.
 Ruajtësi i ekranit ishte aktiv për %d sekonda.
 Ruajtësi i ekranit është aktiv
 Ruajtësi i ekranit nuk është aktiv
 Screensaver nuk është aktiv për momentin.
 Butoni Caps Lock është i shtypur. 