��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %     �	     �	     �	  	   �	     �	     
  6   )
     `
     u
  %   {
     �
  /   �
  :   �
          "     B  \   ]     �  
   �     �  #   �       %        >     A  #   S     w  @   �  /   �               .     <     \     o     �     �  ?   �     �     �     	               0     E     ]     d                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-04-28 17:42+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresa Gjithmonë lejo hyrjen Kërkesë për autorizim nga %s Bluetooth Parametrat e Bluetooth-it Bluetooth-i është i fikur Bluetooth-i është i fikur nga çelësi i hardware-it Shfleto skedarët... Anulo Konfiguroni parametrat e Bluetooth-it Lidhja Pajisja %s dëshiron hyrjen në shërbimin '%s' Pajisja %s dëshiron të binjakëzohet me këtë kompjuter Nuk përputhet Gabim në shfletimin e pajisjes Lejoje vetëm këtë herë Nëse e hiqni pajisjen, do t'iu duhet ta instaloni përsëri para se ta përdorni përsëri. Parametrat e tastierës Përputhet Parametrat e Mausit Parametrat e Mausit dhe Touchpad-it Jo Nuk u gjetën përshtatësa Bluetooth OK Të binjakëzuara Konfirmimi për binjakëzim për %s Kërkesë binjakëzimi për %s Konfirmo nëse PIN-i '%s' përputhet me atë në këtë pajisje. Shkruaj PIN-in e përmendur në këtë pajisje. Refuzo Hiq '%s' nga lista e pajisjeve? Hiqe pajisjen Dërgo skedarë në pajisjen... Dërgo skedarë... Instalo pajisje të re Konfiguro një pajisje të re Parametrat e zërit Pajisja e kërkuar nuk mund të shfletohet, gabimi është '%s' Tipi Dukshmëria Dukshmëria e "%s" Po duke u lidhur... duke u shkëputur... hardware i çaktivizuar faqe 1 faqe 2 