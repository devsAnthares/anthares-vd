��          �      �       H     I     [     b  l   {     �     �     �       Q   /     �     �  
   �     �  N  �       
   1      <  �   ]     �       "      ,   C  ]   p     �     �     �             
                   	                                      %s's public files Always File Sharing Preferences If this is true, the Public directory in the users home directory will be shared when the user is logged in. Never Share Files Share Public directory Share Public files on network When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords When writing files _Password: _Require password: Project-Id-Version: gnome-user-share HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2005-09-07 19:05+0200
Last-Translator: Elian Myftiu <elian@alblinux.net>
Language-Team: Albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>
Language: sq
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Skedat publike të %s Gjithmonë Preferimet e Ndarjes së Skedave Nëse e vërtetë, directory Public në directory e shtëpisë së përdoruesve do të jetë e përbashkët me të tjerë kur përdoruesi nis sesionin e tij. Asnjëherë Ndaj Skeda me të Tjerë Ndaj skedarin Public me të tjerë Ndaj skedat e Public në rrjet me të tjerë Kur duhet kërkuar fjalëkalimi. Vlerat e mundëshme janë "never", "on_write", dhe "always". Kur kërkon fjalëkalime Kur shkruan skeda _Fjalëkalimi: _Kërko fjalëkalimin: 