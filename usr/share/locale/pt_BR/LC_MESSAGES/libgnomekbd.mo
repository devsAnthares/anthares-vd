��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  6  �  6   �
  
   �
  ,         -     ?  �   W     �     �  <     &   D  !   k  7   �     �  "   �  D   �  .   A  �   p  )   �  )   &  !   P  "   r     �  8   �     �  4   �     /  3   B     v  8   �  *   �     �     �          6  ,   H     u     �     �  "   �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2016-10-11 14:04+0000
PO-Revision-Date: 2016-12-28 17:34-0200
Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 1.0.0-beta1
X-Project-Style: gnome
 Grupo padrão, designado durante a criação da janela Indicador: Manter e gerenciar grupo separado por janela Layout de teclado Disposição do teclado Layout de teclado “%s”
Copyright © X.org Foundation e colaboradores do XKeyboardConfig
Para a licença, veja os metadados do pacote. Modelo do teclado Opções do teclado Carrega opções e disposições exóticas, raramente usadas Carregar itens de configuração extra Pré-visualize layouts de teclado Salvar/restaurar indicadores com grupos de disposição Grupos secundários Mostra bandeiras no miniaplicativo Mostra bandeiras no miniaplicativo para indicar a disposição atual Mostrar nomes de disposição em vez de grupos Mostra nomes de disposição (”layout”) em vez de nomes de grupo (apenas para versões do XFree com suporte a múltiplas disposições) Visualização do teclado, deslocamento X Visualização do teclado, deslocamento Y Visualização do teclado, altura Visualização do teclado, largura A cor do plano de fundo A cor do plano de fundo para o indicador de disposição A família da fonte A família da fonte para o indicador de disposição O tamanho da fonte O tamanho da fonte para o indicador de disposição A cor do primeiro plano A cor do primeiro plano para o indicador de disposição Ocorreu um erro ao carregar uma imagem: %s Desconhecido Erro de inicialização do XKB disposição do teclado modelo do teclado disposição “%s” disposições “%s” modelo “%s”, %s e %s sem disposição sem opções opção “%s” opções “%s” preferences-desktop-keyboard 