��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h    �     �     �     �     �     �     �          &     ?     K     Z     g     w     �     �     �  *   �  *   �  H   	     W	     u	  !   �	  5   �	  L   �	  �   ,
  2   �
  ~   -                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-27 10:51-0200
Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 1.0.0-beta1
X-Project-Style: gnome
 %R %R:%S %a %R %a %R:%S %a %-e de %b_%R %a %-e de %b_%R:%S %a %-e de %b_%l:%M %p %a %-e de %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %-e de %b_%R %-e de %b_%R:%S %a %-e de %b_%l:%M %p %-e de %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d não pode se conectar à saída %s CRTC %d não possui suporte a rotação=%d CRTC %d: tentando o modo %dx%d@%dHz com saída em %dx%d@%dHz (passo %d)
 Tentando modos para CRTC: %d
 Não especificado não pode clonar para a saída %s não foi possível atribuir CRTCs para as saídas:
%s nenhum dos modos selecionados foram compatíveis com os modos possíveis:
%s a saída %s não possui os mesmos parâmetros que a outra saída clonada:
modo atual = %d, novo modo = %d
coordenadas atuais = (%d,%d), novas coordenadas = (%d,%d)
rotação atual = %d, nova rotação = %d a saída %s não possui suporte ao modo %dx%d@%dHz o tamanho virtual requisitado não se encaixa no tamanho disponível: requisitado=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 