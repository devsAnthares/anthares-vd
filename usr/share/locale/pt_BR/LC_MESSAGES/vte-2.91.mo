��          L      |       �   /   �      �   =   �   +   5     a  �  i  5   *     `  W   z  6   �     	                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: 2.0
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/vte/issues
PO-Revision-Date: 2018-08-11 23:58-0300
Last-Translator: Ricardo Silva Veloso <ricvelozo@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.1
X-Project-Style: gnome
 Erro (%s) ao converter dados para filho, descartando. Erro ao ler do filho: %s. O GnuTLS não está habilitado; os dados serão escritos descriptografados em um disco! Não foi possível converter caracteres de %s para %s. AVISO 