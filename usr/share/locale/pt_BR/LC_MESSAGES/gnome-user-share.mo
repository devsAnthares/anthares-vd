��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     f       E   �  %   �     
  #     i   ?  T   �     �  .                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-08 10:38-0300
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.5.4
 Arquivos públicos de %s Arquivos públicos de %s em %s Caso seja habilitado, executa o compartilhamento de arquivos pessoais Compartilhamento de arquivos pessoais Compartilhamento Configurações de compartilhamento Ativa o compartilhamento de arquivos pessoais para compartilhar o conteúdo desta pasta através da rede. Quando perguntar por senhas. Valores possíveis são "never", "on_write" e "always". Quando exigir senhas compartilhar;arquivos;http;rede;copiar;enviar; 