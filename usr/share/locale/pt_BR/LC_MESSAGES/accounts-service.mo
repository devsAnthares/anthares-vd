��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  K   j  C   �  E   �  (   @  "   i      �     �  %   �  Z   �     L               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-03-24 23:25+0000
Last-Translator: Rafael Ferreira <rafael.f.f1@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/projects/p/freedesktop/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Autenticação é necessária para alterar configurações da tela de login Autenticação é necessária para alteração de dados de usuário Autenticação é necessária para alterar dados do próprio usuário Alterar configurações da tela de login Alterar dados do próprio usuário Habilitar depuração de código Gerenciar contas de usuários Exibir informação da versão e sair Fornece interfaces de D-Bus para consultar e manipular
informações de conta de usuário. Substitui instância existente 