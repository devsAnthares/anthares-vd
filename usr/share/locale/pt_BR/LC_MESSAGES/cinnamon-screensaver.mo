��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  (   d     �  F   �     �  (   �       8   ;  '   t     �  8   �  d   �  !   J  #   l  2   �  +   �     �     �      	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-31 22:05+0000
Last-Translator: Sitonir de Oliveira <sitonir@outlook.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: Brazil
X-Poedit-Language: Portuguese
 Faz a proteção de tela sair suavemente Verificando... Se a proteção de tela estiver ativa então desative-a (exibe a tela) Senha incorreta Mensagem a ser exibida na tela bloqueada Por favor, insira sua senha... Consultar quanto tempo a proteção de tela ficou  ativa Consulta o estado da proteção de tela Trocar Usuário Faz o processo da proteção travar a tela imediatamente A proteção de tela está ativa por %d segundo.
 A proteção de tela está ativa por %d segundos.
 A proteção de tela está ativa
 A proteção de tela está inativa
 A proteção de tela não está ativa atualmente.
 Ativa a proteção de tela (tela em branco) Desbloquear Versão deste aplicativo A tecla Caps Lock está ativada. 