��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 �  %  	   �	     �	     �	  	   �	     
      
  )   ;
     e
     z
     �
     �
  /   �
  2   �
               0  ^   I     �  	   �     �  #   �     
  %        5     8  '   D  '   l  <   �  1   �       &        3  %   G     m     �     �     �  A   �               &     ?     C     Q     b  	   v  	   �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-01-03 13:56+0000
Last-Translator: Sitonir de Oliveira <sitonir@outlook.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Endereço Sempre permitir acesso Autorização requerida para %s Bluetooth Configurações do Bluetooth Bluetooth está desativado O Bluetooth está desativado via hardware Procurar arquivos... Cancelar Configurar Bluetooth Conexão O dispositivo %s deseja acessar o serviço '%s' O dispositivo %s deseja parear com este computador Não corresponde Erro ao buscar equipamento Permitir apenas esta vez Se você remover o dispositivo, você terá de configurá-lo novamente para poder utilizá-lo. Configurações de teclado Coincidem Configurações do Mouse Configurações de Mouse e Touchpad Não Nenhum adaptador Bluetooth encontrado OK Emparelhado Confirmação de emparelhamento para %s Solicitação de emparelhamento para %s Por favor, confirme se o PIN '%s' é o mesmo do dispositivo. Por favor, digite o PIN informado no dispositivo. Rejeitar Remover '%s' da lista de dispositivos? Remover dispositivo Enviar arquivos para o Dispositivo... Enviar arquivos... Configurar novo dispositivo Configurar Novo Dispositivo... Configurações de Som O dispositivo solicitado não pode ser encontrado, o erro é '%s' Tipo Visibilidade Visibilidade de “%s” Sim conectando... desconectando... hardware desativado página 1 página 2 