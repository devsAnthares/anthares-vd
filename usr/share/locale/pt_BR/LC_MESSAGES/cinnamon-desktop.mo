��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     r  J     �	     �	     �	     �	  	   
     
     
     
     
  *   #
  !   N
  H   p
  [   �
               -  #   :     ^  !   |  5   �  4   �  8   	  8   B  D   {  8   �  M   �  L   G  �   �  *   i  �   �  ~     B   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-07 13:41-0300
Last-Translator: Rafael Ferreira <rafael.f.f1@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 %A, %e de %B, %R %A, %e de %B, %R:%S %A, %e de %B, %R %A, %e de %B, %R:%S %A, %B %e %R %R:%S %R %R:%S CRTC %d não pode se conectar à saída %s CRTC %d não suporta rotação=%s CRTC %d: tentando o modo %dx%d@%dHz com saída em %dx%d@%dHz (passo %d)
 Não foi possível localizar um terminal, usando xterm, mesmo que isso possa não funcionar Laptop Telas espelhadas Desconhecido Extensão RANDR não está presente Tentando modos para CRTC: %d
 não pode clonar para a saída %s não foi possível atribuir CRTCs para as saídas:
%s não foi possível obter informações sobre CRTC %d não foi possível obter informações sobre a saída %d não foi possível obter o intervalo de tamanhos da tela não foi possível obter os recursos da tela (CRTCs, saídas, modos) não foi possível definir a configuração para CRTC %d nenhuma das configurações de exibição correspondem a configuração ativa nenhum dos modos selecionados foram compatíveis com os modos possíveis:
%s a saída %s não tem os mesmos parâmetros que a outra saída clonada:
modo existente = %d, novo modo %d
coordenadas existentes = (%d,%d), novas coordenadas = (%d,%d)
rotação existente = %s, nova rotação = %s a saída %s não suporta o modo %dx%d@%dHz a posição requisitada/tamanho para CRTC %d está fora do limite permitido: posição=(%d, %d), tamanho=(%d, %d), máximo=(%d, %d) o tamanho virtual requisitado não se encaixa no tamanho disponível: requisitado=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) erro não manipulado do X ao obter o intervalo de tamanhos da tela 