��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  ~        �	     �	     �	     �	     �	     �	     �	     �	  �   
  m   �
  �   �
  �   �  !   �  C   �     �  U     n   d  x   �  p   L  b   �  v      ~   �  �     �   �  �   D  �   �    �  �   �  �   ]    8  �   V             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master.kn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-11-20 11:55+0530
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: Kannada <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kn
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ಯು ಔಟ್‌ಪುಟ್ %s ಅನ್ನು ಚಲಾಯಿಸಲು ಸಾಧ್ಯವಾಗುತ್ತಿಲ್ಲ CRTC %d ಯು ತಿರುಗಿಸುವುದನ್ನು ಬೆಂಬಲಿಸುವುದಿಲ್ಲ=%s CRTC %d: %dx%d@%dHz ಎಂಬ ಕ್ರಮವನ್ನು  %dx%d@%dHz ಎಂಬಲ್ಲಿ ಔಟ್‌ಪುಟ್‌ನೊಂದಿಗೆ ಪ್ರಯತ್ನಿಸಲಾಗುತ್ತಿದೆ (ಪಾಸ್ %d)
 ಟರ್ಮಿನಲ್ ಕಂಡುಬಂದಿಲ್ಲ, xterm ಅನ್ನು ಬಳಸಲಾಗುತ್ತಿದೆ, ಅದೂ ಸಹ ಕೆಲಸ ಮಾಡದೆ ಇರಬಹುದು ಲ್ಯಾಪ್‌ಟಾಪ್ ಪ್ರತಿಬಿಂಬಿತ ಪ್ರದರ್ಶಕಗಳು ಗೊತ್ತಿರದ RANDR ವಿಸ್ತರಣೆಯು ಅಸ್ತಿತ್ವದಲ್ಲಿಲ್ಲ CRTC %d ಗಾಗಿ ಕ್ರಮಗಳನ್ನು ಪ್ರಯತ್ನಿಸಲಾಗುತ್ತಿದೆ
 %s ಎಂಬ ಔಟ್‌ಪುಟ್‌ಗೆ ತದ್ರೂಪುಗೊಳಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ ಔಟ್‌ಪುಟ್‌ಗಳಿಗೆ CRTCಗಳನ್ನು ನಿಯೋಜಿಸಲಾಗಿಲ್ಲ:
%s CRTC %d ನ ಬಗೆಗೆ ಮಾಹಿತಿಯನ್ನು ಪಡೆಯಲಾಗಲಿಲ್ಲ ಔಟ್‌ಪುಟ್ %d ನ ಬಗೆಗೆ ಮಾಹಿತಿಯನ್ನು ಪಡೆಯಲಾಗಲಿಲ್ಲ ತೆರೆಯ ಗಾತ್ರಗಳ ಶ್ರೇಣಿಯನ್ನು ಪಡೆದುಕೊಳ್ಳಲಾಗಲಿಲ್ಲ ತೆರೆಯ ಸಂಪನ್ಮೂಲಗಳನ್ನು ಪಡೆಯಲಾಗಲಿಲ್ಲ (CRTCಗಳು, ಔಟ್‌ಪುಟ್‌ಗಳು, ಕ್ರಮಗಳು) CRTC %d ಗಾಗಿ ಸಂರಚನೆಯನ್ನು ಸಿದ್ಧಗೊಳಿಸಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ ಉಳಿಸಲಾದ ಯಾವುದೆ ಸಂರಚನೆಗಳು ಸಕ್ರಿಯ ಸಂರಚನೆಗೆ ತಾಳೆಯಾಗುತ್ತಿಲ್ಲ ಆಯ್ಕೆ ಮಾಡಲಾದ ಯಾವ ಕ್ರಮಗಳೂ ಸಹ ಸಾಧ್ಯವಿರುವ ಯಾವುದೆ ಕ್ರಮಗಳಿಗೆ ತಾಳೆಯಾಗುತ್ತಿಲ್ಲ:
%s %s ಎನ್ನುವ ಔಟ್‌ಪುಟ್ ಬೇರೊಂದು ತದ್ರೂಪುಗೊಳಿಸಲಾದ ಔಟ್‌ಪುಟ್‌ ರೀತಿಯ ನಿಯತಾಂಕಗಳನ್ನು ಹೊಂದಿಲ್ಲ:
ಈಗಿರುವ ಸ್ಥಿತಿ = %d, ಹೊಸ ಸ್ಥಿತಿ = %d
ಈಗಿರುವ ನಿರ್ದೇಶಾಂಕಗಳು = (%d, %d), ಹೊಸ ನಿರ್ದೇಶಾಂಕಗಳು = (%d, %d)
ಈಗಿರುವ ಪರಿಭ್ರಮಣೆ = %s, ಹೊಸ ಪರಿಭ್ರಮಣೆ = %s %s ಎಂಬ ಔಟ್‌ಪುಟ್‌ನಿಂದ %dx%d@%dHz ಎಂಬ ಕ್ರಮವನ್ನು ಬೆಂಬಲಿಸಲಾಗುತ್ತಿಲ್ಲ CRTC %d ಗಾಗಿ ಮನವಿ ಸಲ್ಲಿಸಲಾದ ಸ್ಥಾನ/ಗಾತ್ರವು ಮಿತಿಯ ಹೊರಗಿದೆ: ಸ್ಥಾನ=(%d, %d), ಗಾತ್ರ=(%d, %d), ಗರಿಷ್ಟ=(%d, %d) ಅಗತ್ಯವಿರುವ ವರ್ಚುವಲ್ ಗಾತ್ರವು ಲಭ್ಯವಿರುವ ಗಾತ್ರಕ್ಕೆ ಸರಿಹೊಂದುವುದಿಲ್ಲ: ಮನವಿ ಮಾಡಿದ್ದು=(%d, %d), ಕನಿಷ್ಟ=(%d, %d), ಗರಿಷ್ಟ=(%d, %d) ತೆರೆಯ ಗಾತ್ರಗಳ ಶ್ರೇಣಿಯನ್ನು ಪಡೆದುಕೊಳ್ಳುವಾಗ ನಿಭಾಯಿಸಲಾಗದ X ದೋಷ 