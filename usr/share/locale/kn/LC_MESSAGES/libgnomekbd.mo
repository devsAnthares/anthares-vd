��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  �   �	     r
  �   �
  +   )  (   U  <  ~  "   �  +   �  �   
  ^   �  D     �   V  7   �  V   !  �   x  �      $  �  G   �  G     <   Y  6   �  %   �  ]   �  .   Q  f   �  +   �  f     %   z  ]   �  n   �     m  2   �  (   �  "   �  >     (   D  7   m  /   �  2   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-08-06 14:46+0530
PO-Revision-Date: 2013-10-27 02:30-0400
Last-Translator: Shankar Prasad <svenkate@redhat.com>
Language-Team: Kannada <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kn
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Zanata 3.2.3
 ಪೂರ್ವನಿಯೋಜಿತ ಗುಂಪು, ವಿಂಡೋ ನಿರ್ಮಾಣದಲ್ಲಿ ನಿಯೋಜಿಸಲಾಗಿದೆ ಸೂಚಕ: ಪ್ರತಿ ವಿಂಡೋಗಾಗಿ ಪ್ರತ್ಯೇಕ ಗುಂಪನ್ನು ಇರಿಸಿಕೊಳ್ಳಿ ಹಾಗು ನಿರ್ವಹಿಸಿ ಕೀಲಿಮಣೆ ವಿನ್ಯಾಸ ಕೀಲಮಣೆ ವಿನ್ಯಾಸ ಕೀಲಿ ಮಣೆ ವಿನ್ಯಾಸ "%s"
ಹಕ್ಕು &#169; X.Org ಫೌಂಡೇಶನ್ ಹಾಗು XKeyboardConfig ಸಹಾಯಕರು
ಲೈಸೆನ್ಸಿಂಗ್ ಬಗೆಗಿನ ಮಾಹಿತಿಗಾಗಿ ಪ್ಯಾಕೇಜಿನ ಮೆಟಾಡಾಟವನ್ನು ನೋಡಿ ಕೀಲಮಣೆ ಮಾದರಿ ಕೀಲಮಣೆ ಆಯ್ಕೆಗಳು ನವೀನ, ವಿರಳವಾಗಿ ಬಳಸಲಾದ ಲೇಔಟ್‌ಗಳನ್ನು ಹಾಗು ಆಯ್ಕೆಗಳನ್ನು ಲೋಡ್ ಮಾಡು ಹೆಚ್ಚಿನ ಸಂರಚನಾ ಅಂಶಗಳನ್ನು ಲೋಡ್ ಮಾಡಿ ಕೀಲಿಮಣೆ ವಿನ್ಯಾಸಗಳ ಅವಲೋಕನ ವಿನ್ಯಾಸ ಗುಂಪುಗಳೊಂದಿಗೆ ಸೂಚಕಗಳನ್ನು ಉಳಿಸು/ಮರಳಿ ಸ್ಥಾಪಿಸಿ ಅಪ್ರಮುಖವಾದ ಗುಂಪುಗಳು ಆಪ್ಲೆಟ್‌ನಲ್ಲಿ ಗುರುತನ್ನು ತೋರಿಸು ಪ್ರಸಕ್ತ ವಿನ್ಯಾಸವನ್ನು ಸೂಚಿಸಲು ಆಪ್ಲೆಟ್‌ನಲ್ಲಿ ಗುರುತನ್ನು ತೋರಿಸು ಗುಂಪಿನ ಹೆಸರುಗಳ ಬದಲು ವಿನ್ಯಾಸದ ಹೆಸರುಗಳನ್ನು ತೋರಿಸು ಗುಂಪಿನ ಹೆಸರುಗಳ ಬದಲು ವಿನ್ಯಾಸದ ಹೆಸರುಗಳನ್ನು ತೋರಿಸು (ಕೇವಲ XFree ಬೆಂಬಲವಿರುವ ಅನೇಕ ವಿನ್ಯಾಸಗಳಿಗಾಗಿನ ಆವೃತ್ತಿಗಾಗಿ ಮಾತ್ರ) ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, X ಆಫ್‌ಸೆಟ್ ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, Y ಆಫ್‌ಸೆಟ್ ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, ಎತ್ತರ ಕೀಲಿಮಣೆ ಮುನ್ನೋಟ, ಅಗಲ ಹಿನ್ನೆಲೆ ಬಣ್ಣ ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಹಿನ್ನೆಲೆ ಬಣ್ಣ ಅಕ್ಷರಶೈಲಿ ಪರಿವಾರ ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಅಕ್ಷರಶೈಲಿ ಪರಿವಾರ ಅಕ್ಷರಶೈಲಿ ಗಾತ್ರ ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಅಕ್ಷರಶೈಲಿಯ ಗಾತ್ರ ಮುನ್ನೆಲೆ ಬಣ್ಣ ವಿನ್ಯಾಸ ಸೂಚಕಕ್ಕಾಗಿನ ಮುನ್ನೆಲೆ ಬಣ್ಣ ಚಿತ್ರವನ್ನು ಲೋಡ್ ಮಾಡುವಲ್ಲಿ ದೋಷ ಉಂಟಾಗಿದೆ: %s ಗೊತ್ತಿರದ XKB ಆರಂಭಿಸುವಲ್ಲಿ ದೋಷ ಕೀಲಮಣೆ ವಿನ್ಯಾಸ ಕೀಲಮಣೆ ಮಾದರಿ ವಿನ್ಯಾಸ "%s" ವಿನ್ಯಾಸಗಳು "%s" ಮಾದರಿ "%s", %s ಹಾಗು %s ಯಾವುದೆ ವಿನ್ಯಾಸವಿಲ್ಲ ಯಾವುದೆ ಆಯ್ಕೆ ಇಲ್ಲ ಆಯ್ಕೆ "%s" ಆಯ್ಕೆಗಳು "%s" 