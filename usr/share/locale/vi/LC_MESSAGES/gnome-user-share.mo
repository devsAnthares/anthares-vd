��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     b  &   �  F   �     �  	          f   ,     �  %     Z   9                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-29 07:08+0700
Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>
Language-Team: Vietnamese <gnome-vi-list@gnome.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.7
 Các tập tin công của %s Các tập tin công của %s trên %s Khởi chạy “Chia sẻ tập tin cá nhân” nếu được bật Chia sẻ tập tin cá nhân Chia sẻ Cài đặt chia sẻ Bật “Chia sẻ tập tin cá nhân” để chia sẻ nội dung của thư mục này qua mạng. Khi nào nên yêu cầu mật khẩu. Giá trị có thể là:
 * never	không bao giờ
 * on_write	khi ghi
 * always	luôn. Khi nào thì yêu cầu mật khẩu share;chia;sẻ;se;files;tập;tin;tap;network;mạng;mang;copy;chép;chep;send;gửi;gui; 