��          L      |       �   `   �      
     %  )   B     l  �  �  ?   �  0   �  1   �  @   .  2   o                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver GNOME TRUNK
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-11-03 04:05+0000
Last-Translator: Saki <Unknown>
Language-Team: Vietnamese <vi-VN@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Bộ bảo vệ màn hình đã hoạt động trong %d giây.
 Bộ bảo vệ màn hình đang hoạt động
 Bộ bảo vệ màn hình không hoạt động
 Bộ bảo vệ màn hình hiện thời không hoạt động.
 Bạn đã bật phím Khoá Chữ Hoa (CapsLock). 