��    ?        Y         p     q     �  5   �  7   �     	  (     )   9  %   c  '   �  "   �     �     �     �  !        %     6     9      M     n     �     �     �     �     �     �  '   �  	     '        @  P   G     �     �     �     �     �     �     		     	  &   8	     _	     p	      �	  +   �	     �	     �	     
     
     &
      9
  E   Z
     �
  &   �
     �
     �
  X   
  X   c  $   �  
   �     �     �  
   �     	  �    )   �     �  E     v   R     �  >   �  0     7   ?  9   w     �     �     �  	     ?        M     `  "   c  &   �  '   �     �     �     �  4     1   =     o  !   �  
   �  =   �     �  N        Q     i  7   �     �     �  %   �     �  +     7   G       (   �  (   �  .   �  (        :      Q     r  $     5   �  ^   �     9  =   L  .   �     �  v   �  v   O  *   �     �     �          #     /     "       4   -   .       :   0   	           9       !                     7                 #   8   6             ;                               +          /   >      5             $             2       ,   ?       (              *       <                       &   '              =         
   3      )   %      1          A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session 2.25.90
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:17+0000
Last-Translator: Nguyễn Thái Ngọc Duy <pclouds@gmail.com>
Language-Team: Vietnamese <vi-VN@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: vi
 Một chương trình vẫn đang chạy: AUTOSTART_DIR Ứng dụng không chấp nhận tài liệu thông qua dòng lệnh Không thể gởi địa chỉ URI của tài liệu cho một mục nhập màn hình nền « Kiểu=Liên_kết » Thôi Không thể kết nối với trình quản lý phiên chạy Không thể tạo ổ cắm lắng nghe ICE: %s Tắt kết nối với trình quản lý phiên chạy Đừng nạp ứng dụng do người dùng xác định Không yêu cầu xác nhận Bật mã hỗ trợ tìm lỗi Thoát với mã %d TẬP_TIN Tập tin không phải là một tập tin .desktop hợp lệ Vẫn Ngủ đông ID Không thấy biểu tượng '%s' Bỏ qua các ứng dụng ngăn cản Bị buộc thoát bởi tín hiệu %d Khoá màn hình Vẫn đăng xuất Đăng xuất Đăng xuất ra hệ thống này ngay bây giờ ? Không phải một mục có thể khởi chạy Không trả lời Đè thư mục autostart chuẩn Tắt máy Chương trình được gọi với tuỳ chọn xung đột Khởi động lại Đang từ chối kết nối khách mới vì phiên chạy đang bị tắt
 Ứng dụng đã nhớ Vẫn cứ khởi động lại Khởi động lại hệ thống này ngay bây giờ? SESSION_NAME N_gưng Tùy chọn quản lý phiên chạy: Phiên làm việc cần dùng Hiện tùy chọn quản lý phiên chạy Hiện cửa sổ cá voi thất bại để kiểm tra Vẫn tắt máy Tắt hệ thống này ngay bây giờ? Vài chương trình vẫn đang chạy: Ghi rõ tập tin chứa cấu hình đã lưu Xác định ID quản lý phiên chạy Đang khởi chạy %s Bị dừng bởi tín hiệu %d Vẫn Ngưng Vẫn chuyển đổi người dùng Chương trình này đang ngăn cản đăng xuất. Không thể khởi chạy phiên đăng nhập (và không thể kết nối đến X server) Chưa xác định Không nhận ra tập tin màn hình nền phiên bản '%s' Không nhận ra tùy chọn khởi chạy: %d Phiên bản ứng dụng này Đang chờ chương trình kết thúc. Ngắt những chương trình này có thể dẫn đến mất dữ liệu. Đang chờ chương trình kết thúc. Ngắt những chương trình này có thể dẫn đến mất dữ liệu. Hiện bạn đăng nhập với tên "%s" _Ngủ đông Đăng _xuất _Khởi chạy lại _Tắt máy Đổ_i người dùng 