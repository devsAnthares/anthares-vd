��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 j  %     �	     �	     �	  	   �	     �	     �	  (   

     3
     H
  #   N
     r
  2   ~
  8   �
     �
     �
       f   .     �     �     �  "   �     �  /   �  
   &     1     B     a  R     8   �       5        M  &   d     �      �  $   �     �  ?   �     <     C     O     `     d     y     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-21 17:45+0000
Last-Translator: Saki <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Địa chỉ Luôn cho phép truy cập Yêu cầu cấp phép từ %s Bluetooth Thiết đặt Bluetooth Bluetooth tắt Bluetooth tắt bởi nút phần cứng Duyệt Tập tin... Hủy Cấu hình thiết lập Bluetooth Kết nối Thiết bị %s muốn truy cập dịch vụ '%s' Thiết bị %s muốn ghép cặp với máy tính này Không khớp Lỗi duyệt thiết bị Chỉ cho phép lần này Nếu bạn gỡ bỏ thiết bị, bạn sẽ phải thiết lập lại cho lần dùng kế tiếp. Thiết lập Bàn phím Khớp Thiết đặt Chuột Thiết lập Chuột và Touchpad Không Không tìm thấy bộ điều hợp Bluetooth Đồng ý Đã ghép cặp Xác nhận ghép cặp cho %s Yêu cầu ghép cặp cho %s Xin hãy xác nhận mã PIN '%s' có khớp với cái trên thiết bị không. Xin hãy nhập mã PIN hiển thị trên thiết bị. Từ chối Gỡ bỏ '%s' khỏi danh sách thiết bị chứ ? Gỡ bỏ thiết bị Gửi Tập tin đến Thiết bị... Gửi Tập tin... Thiết lập Thiết bị Mới Thiết đặt Thiết bị Mới... Thiết lập Âm thanh Thiết bị yêu cầu không duyệt xem được, lỗi '%s' Kiểu Tầm nhìn Tầm nhìn "%s" Có đang kết nối... đang ngắt kết nối... phần cứng vô hiệu hoá trang 1 trang 2 