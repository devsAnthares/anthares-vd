��             +         �     �     �     �     �  	                  "     +     7  $   V  D   {  <   �     �               :     T  %   n  '   �  )   �  '   �  :     +   I  I   u  F   �  �     *   �  t   �  i   t  9   �  �       �	     �	     �	     �	  	   �	     �	     �	     �	      
  !   
  !   .
  N   P
  P   �
     �
     �
  #        0     O  3   m  *   �  2   �  2   �  Q   2  +   �  n   �  _     �     5   t  �   �  �   A  U   �             
   	                                                                                                                                %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 2.25.90
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-01 22:00+0700
Last-Translator: Nguyen Vu Hung <vuhung16plus@gmail.com>
Language-Team: Vietnamese <vi-VN@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
X-Generator: LocFactoryEditor 1.8
 %A %R, %e %B %A %R:%S, %e %B %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d không thể xuất ra %s CRTC %d không hỗ trợ quay=%s CRTC %d: Thử chế độ %dx%d@%dHz với đầu ra %dx%d@%dHz (bước %d)
 Không tìm thấy thiết bị, dùng xterm dù nó có thể chạy hay không Máy xách tay Chưa biết không có phần mở rộng RANDR Thử chế độ cho CRTC %d
 không thể sao đầu ra %s không thể chỉ định CRTC với đầu ra:
%s không thể lấy thông tin về CRTC %d không thể lấy thông tin về kết xuất %d không thể lấy phạm vi kích cỡ màn hình không thể lấy các tài nguyên màn hình (CRTC, kết xuất, chế độ) không thể đặt cấu hình cho CRTC %d phần lớn các cấu hình trình bày đã lưu cũng tương ứng với cấu hình vẫn hoạt động các chế độ đã chọn không tương thích với mọi chế độ có thể chọn:
%s đầu ra %s không chứa đúng thông số như đầu ra khác đã được nhân tính:
chế độ cũ = %d, chế độ mới = %d
tọa độ cũ = (%d, %d), tọa độ mới = (%d, %d)
hướng quay cũ = %s, hướng quay mới = %s Đầu ra %s không hỗ trợ chế độ %dx%d@%dHz vị trí/kích cỡ yêu cầu cho CRTC %d nằm ở ngoại giới hạn được phép: vị trí=(%d, %d), kích cỡ=(%d, %d), tối đa=(%d, %d) kích cỡ ảo cần thiết không phải tương ứng với kích cỡ sẵn sàng: yêu cầu=(%d, %d), tối thiểu=(%d, %d), tối đa=(%d, %d) gặp lỗi X (không thể xử lý) trong khi lấy phạm vi kích cỡ màn hình 