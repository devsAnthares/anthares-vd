��    N      �  k   �      �  #   �  #   �  !   �  !        5     >     G     O     U     [     a     i     q     w     �     �  :   �     �     �                 .        I     Q     _  4   v  %   �     �     �  
   �  5   �  	   +	     5	     =	     I	     V	     d	  4   �	     �	     �	     �	     �	      
  
   
     
      
     (
     4
  %   G
  .   m
  "   �
     �
     �
     �
               .     G     T     a     h     u     �     �     �     �     �     �     �     �     �     �     �          
       �  $  ,   �  ,   �  +     +   D     p     y     �     �     �     �     �     �     �     �  *   �     �  L        d     u  
   |  	   �     �  !   �     �     �  "   �  S   �  2   H     {     �     �  C   �  
   �     �     �          -     A  7   ]     �  "   �  &   �     �     	          3     G      ]  2   ~  ,   �  E   �  (   $     M  5   j  )   �     �  "   �     �  (        B  
   U     `     q     �  
   �  !   �     �     �     �            '   !     I  	   V     `     m  N   t     &   :          1       !   
         G   -   "   F             =   ?      )       '   H   4       *           ;       E   B                              ,                   %   L       6       M       7      I             J   9   <          K   (           @         5   3   .      /   >       D          A   $               8   0   2                	   C      +       #                  N        %s of %s - %u minutes left (%s/sec) %s of %s - %u seconds left (%s/sec) %s of %s - 1 minute left (%s/sec) %s of %s - 1 second left (%s/sec) 1024x768 1280x960 128x128 16x16 32x32 48x48 640x480 800x600 96x96 <b>Image Size</b> <i>Resizing "%s"</i> <i>Rotating "%s"</i> A GTK+ utility for computing message digests or checksums. Album Artist: Album: Artist: Audio Bitrate: Check against the specified digest or checksum Compare Compare Later Compare selected files Compare selected files to the file remembered before Compare to the file remembered before Compare to:  Compress... Copyright: Create a compressed archive with the selected objects DIRECTORY Emblems Encoded by: Extract Here Extract To... Extract the selected archive Extract the selected archive to the current position Failed to add "%s":
%s Failed to read "%s" Failed to read "%s":
%s Genre: Length: N-Way Diff Nemo Compare No Info Normal Diff Not a regular file Please enter a valid filename suffix! Read program data from the specified directory Remember file for later comparison Resize Images Resize each selected image Resizing image: %d of %d Ro_tate Images... Rotate each selected image Rotating image: %d of %d Sample Rate: Save Digests Scale: Select Files Select a size: Show version information TEXT Three-Way Diff Title: Track #: Unknown error Year: [FILE|URI...] _Resize Images... _Retry _Skip percent pixels translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-10-14 16:35+0000
Last-Translator: Hoang Minh Hieu <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:18+0000
X-Generator: Launchpad (build 18688)
 %s của %s - %u phút còn lại (%s/giây) %s của %s - %u giây còn lại (%s/giây) %s của %s - 1 phút còn lại (%s/giây) %s của %s - 1 giây còn lại (%s/giây) 1024x768 1280x960 128x128 16×16 32x32 48x48 640x480 800x600 96x96 <b> Kích cỡ ảnh </b> <i> Đang thay đổi kích cỡ "%s" </i> <i> Đang xoay ảnh "%s" </i> Một công cụ GTK cho việc tính toán thiết lập hoặc kiểm tra. Tác giả Album Album: Tác giả Âm thanh Bitrate Kiểm tra thiết lập đã có So sánh So sánh sau So sánh những tệp đã chọn So sánh những tệp đã chọn với những tệp đã ghi nhớ trước đó So sánh với tập tin đã nhớ trước đây So sánh với  Nén… Bản quyền Tạo một kho nén chứa những đối tượng được chọn THƯ MỤC Hình tượng Được mã hóa bởi Giải nén ở đây Giải nén vào… Giải nén kho đã chọn Giải nén kho đã chọn vào vị trí hiện tại Không thể thêm "%s":
%s Thất bại khi đọc tệp "%s" Thất bại khi đọc tệp "%s":
%s Thể loại: Độ dài: Sử dụng Diff bằng N-Way So sách với Nemo Không có thông tin Sử dụng Diff bình thường Không phải là một tập tin thông thường Vui lòng nhập vào tên file hợp lệ ! Đọc dữ liệu chương trình từ cây thư mục đã định. Ghi nhớ tệp cho lần so sánh tới Thay đổi kích cỡ ảnh Tùy chỉnh kích cỡ mỗi ảnh đã lựa chọn Đang thay đổi kích cỡ: %d của %d Xoay ảnh... Xoay mỗi ảnh đã lựa chọn Đang xoay ảnh: %d của %d Số lần lấy mẫu trên một giây Lưu thiết lập Tỷ lệ: Chọn tập tin Chọn một kích thước: Hiện thông tin phiên bản VĂN BẢN Sử dụng Diff bằng Three-Way Tiêu đề Bản nhạc Lỗi không xác định Năm: [FILE|URI...] _Tùy chỉnh lại kích cỡ ảnh... _Thử lại _Bỏ qua phần trăm pixels Launchpad Contributions:
  Hoang Minh Hieu https://launchpad.net/~hyperion0201 