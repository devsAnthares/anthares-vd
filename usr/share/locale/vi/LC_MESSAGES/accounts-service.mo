��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  e  �  H     >   f  Q   �  0   �  9   (     b  &   w     �  c   �     !               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accountsservice master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-06-04 16:59+0700
Last-Translator: Nguyễn Thái Ngọc Duy <pclouds@gmail.com>
Language-Team: Vietnamese <vi-VN@googlegroups.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Cần xác thực để thay đổi cấu hình màn hình đăng nhập Cần xác thực để thay đổi dữ liệu người dùng Cần xác thực để thay đổi dữ liệu người dùng của chính mình Thay đổi cấu hình màn hình đăng nhập Thay đổi dữ liệu người dùng của chính mình Bật mã tìm lỗi Quản lý tài khoản người dùng Hiện phiên bản và thoát Cung cấp giao tiếp D-Bus để truy vấn và thao tác thông tin tài khoản người dùng. Thay thế bản hiện có 