��    `        �         (     )     1     9     F     Y     j  I   {     �     �     �  	   �     �     �     	     !	     @	     `	     �	     �	     �	     �	     �	  )   �	     
     
     0
  	   =
  
   G
     R
     d
      ~
  (   �
  .   �
     �
       	             &     /     =     Q  
   Y     d     u     |     �     �  
   �     �     �     �     �     �     �  /   �  
        !     /     =     N     _     e     m  N   �     �     �     �                     (     7     P     g  +   v  <   �  _   �  O   ?  >   �  !   �  3   �  6   $     [     g     t     �  	   �     �     �     �     �  I   �     �     �       #       7     ?     G     T  +   n     �  �   �     j  -   q     �     �     �  7   �  1     Z   4  W   �  `   �  `   H  `   �  (   
  +   3     _  �   {  	     $         :     [  /   n  %   �  H   �  d     �   r  q   �  ;   e  *   �  ,   �     �       5   (  <   ^     �  $   �  =   �          '     7     M     f     |     �     �  $   �  -   �       v   +  ,   �     �  (   �  4     4   J       	   �  R   �  �   �  5   �  ^     h   y  7   �           *   "   F   @   i   2   �   )   �   [   !  �   c!    "  �   #  �   �#  C   n$  �   �$  �   I%     �%  %   
&  :   0&     k&     ~&  !   �&     �&     �&     �&  �   �&  2   �'     �'     �'             Y   M   7   6   O   !              /   *           +          8   \   @   
   T           K      ;   :           1       4   	   U         "      R   B   &      %   P       Q              9   ]         0           =   W           2                         )          X      H   E   ,      L      D   #   5       $   '              C             S      _                 V       J   A      (      Z       ^   <      G   [             N       I       ?   >   .   F   `   -          3    %d Mb/s %d x %d %d x %d (%s) %i month %i months %i week %i weeks %i year %i years %s
Run '%s --help' to see a full list of available command line options.
 %s VPN - System Settings Ad-hoc All files Arabic Authentication required Available Profiles Available Profiles for Cameras Available Profiles for Displays Available Profiles for Printers Available Profiles for Scanners Available Profiles for Webcams British English Cable unplugged Calibration Cannot remove automatically added profile China Chinese (simplified) Colorspace:  Connected Connecting Connection failed Could not detect displays Could not get screen information Could not save the monitor configuration Create a color profile for the selected device Create virtual device Default Default:  Device Disabled Disconnecting Enable verbose mode English Enterprise Firmware missing France French German Germany IP Address IPv4 Address IPv6 Address Infrastructure Layout Less than 1 week Monitor No devices supporting color management detected No profile Not connected Not specified Other profile… Panel to display Proxy Russian Select ICC Profile File Select a monitor to change its properties; drag it to rearrange its placement. Select a region Set for all users Show help options Show the overview Spain Spanish Status unknown Status unknown (missing) Supported ICC profiles Test profile:  The device type is not currently supported. The measuring instrument does not support printer profiling. The measuring instrument is not detected. Please check it is turned on and correctly connected. This device does not have a profile suitable for whole-screen color correction. This device has an old profile that may no longer be accurate. This device is not color managed. This device is using manufacturing calibrated data. This is not recommended for untrusted public networks. Unavailable Uncalibrated United States Unknown Unmanaged Unspecified WEP WPA WPA2 Web Proxy Autodiscovery is used when a Configuration URL is not provided. Wired _All Settings _Import Project-Id-Version: bn
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-control-center&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-01-26 19:21+0000
Last-Translator: Rashik Ishrak Nahian <r.i.nahian@engineer.com>
Language-Team: Bengali <ankur-bd-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
X-Language: bn_BD
 %d Mb/s %d x %d %d x %d (%s) %i মাস %i মাস %i সপ্তাহ %i সপ্তাহ %i বছর %i বছর %s
Run '%s --বিদ্যমান কমান্ড লাইন অপশনের সম্পূর্ণ তালিকা দেখতে সহায়তা করে।
 %s VPN - সিস্টেমের সেটিং এডহক সব নথি আরবি প্রমাণীকরণ আবশ্যকীয় বিদ্যমান প্রোফাইল ক্যামেরার জন্য বিদ্যমান প্রোফাইল ডিসপ্লের জন্য বিদ্যমান প্রোফাইল স্ক্যানারের জন্য বিদ্যমান প্রোফাইল স্ক্যানারের জন্য বিদ্যমান প্রোফাইল ওয়েবক্যামের জন্য বিদ্যমান প্রোফাইল ব্রিটিশ ইংরেজী কেবল আনপ্লাগকৃত ক্রমাঙ্কন স্বয়ংক্রিভাবে সংযুক্ত বৃত্তান্ত অপসারণ করা যাচ্ছেনা চীন চীনা (সরলীকৃত) কালার স্পেস  যোগকৃত সংযুক্ত করা হচ্ছে সংযোগে ব্যর্থ ডিসপ্লে সনাক্ত করতে ব্যর্থ পর্দা সংক্রান্ত পরিবর্তন পাওয়া যায়নি মনিটর সংক্রান্ত কনফিগারেশন সংরক্ষণ করতে ব্যর্থ নির্বাচিত ডিভাইসের জন্য রং বৃত্তান্ত তৈরি ভার্চুয়াল ডিভাইস তৈরি পূর্বনির্ধারিত পূর্বনির্ধারিত:  ডিভাইস নিষ্ক্রিয় বিচ্ছিন্ন করা হচ্ছে ভার্বোজ মোড সক্রিয় করা ইংরেজি এন্টারপ্রাইজ ফার্মওয়্যার অনুপস্থিত ফ্রান্স ফরাসি জার্মান জার্মানী IP ঠিকানা IPv4 ঠিকানা IPv6 ঠিকানা পরিকাঠামো বহির্বিন্যাস ১ সপ্তাহের কম সময় মনিটর রং ব্যবস্থাপনা সনাক্ত করার মত কোন ডিভাইস নেই কোন প্রোফাইল নেই সংযুক্ত নয় নির্ধারিত হয়নি অন্যান্য প্রোফাইল... প্রদর্শনের প্যানেল প্রক্সি রুশ ICC বৃত্তান্ত ফাইল নির্বাচন করুন মনিটরের বৈশিষ্ট্য পরিবর্তন করতে নির্বাচন করুন; এর অবস্থান পুনরায় নির্ধারণ করতে টেনে আনুন। এলাকা নির্বাচন করুন সব ব্যবহারকারীর জন্য নির্ধারণ করুন সহায়তা সংক্রান্ত অপশন প্রদর্শন করা হবে সারসংক্ষেপ প্রদর্শন স্পেন স্প্যানিশ অজানা অবস্থা অজানা অবস্থা (অনুপস্থিত) সমর্থিত ICC প্রোফাইল টেস্ট প্রোফাইল  বর্তমানে ডিভাইসের ধরন সমর্থিত নয়। পরিমাপের ইনস্ট্রুমেন্ট প্রিন্তেরের প্রোফাইল করা সমর্থন করেনা। পরিমাপের ইনস্ট্রুমেন্ট সনাক্ত করা যায়নি। অনুগ্রহ করে এটিকে চালু করুন এবং সঠিক ভাবে সংযুক্ত করুন। সম্পূর্ণ পর্দার রং ঠিক করার জন্য এই ডিভাইসের উপযুক্ত প্রোফাইল নেই। এই ডিভাইসের পুরোনো প্রোফাইল রয়েছে যা কিনা নিখুঁত নাও হতে পারে। এই ডিভাইস রং পরিচালিত নয়। এই ডিভাইস ম্যানুফ্যাকচারকৃত ক্রমাঙ্ক ডাটা ব্যবহার করে। অবিশ্বস্ত পাবলিক নেটওয়ার্কের জন্য এটি সুপারিশ করা হচ্ছেনা। বিদ্যমান নয় ক্রমাঙ্কিত নয় মার্কিন যুক্তরাষ্ট্র অজ্ঞাত অপরিকল্পিত অনুল্লেখকৃত WEP WPA WPA2 যখন কনফিগারেশন URL দেওয়া থাকেনা তখন স্বয়ংক্রিয় ওয়েব প্রক্সি উদঘাটন ব্যবহৃত হয়। তার দ্বারা সংযুক্ত সব সেটিং (_A) ইম্পোর্ট (_I) 