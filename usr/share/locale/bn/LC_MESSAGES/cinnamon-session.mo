��    7      �  I   �      �     �  5   �  7        ;  (   B  )   k  %   �  '   �  "   �               0  !   5     W     h     k           �     �     �     �     �     �     	  '     	   @  '   J  P   r     �     �     �     �          (      C  +   d     �     �     �     �     �      �  E   	     X	  &   `	     �	  X   �	  X   �	  $   X
  
   }
     �
     �
  
   �
     �
  �  �
  C   ^  �   �  �   E     �  t   �  Q   Q  x   �  �     c   �  R     ,   n     �  F   �  ?   �     '  A   *  ]   l  R   �  #     /   A     q  \   �  <   �  &     �   C     �  x   �  �   j  !   W  3   y  =   �  _   �  +   K  K   w  I   �  k     H   y  )   �  U   �  6   B  ^   y  p   �  �   I     �  Y     B   a    �    �  _   �          ,  .   B     q  L   �           4   ,   1       
      	         /   "   !          #   5                 +   $      6   %                     &       *             3                                 )                 2   (                   -          .       7          '       0       A program is still running: Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Refusing new client connection because the session is currently being shut down
 S_uspend Session management options: Session to use Show session management options Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: bn
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:09+0000
Last-Translator: Israt Jahan <Unknown>
Language-Team: Bengali <ankur-bd-l10n@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: bn
X-Language: bn_BD
 একটি প্রোগ্রাম এখনও চলছে: কমান্ড লাইনে এই অ্যাপ্লিকেশন দ্বারা কোনো নথিপত্র গৃহীত হয় না 'Type=Link' ডেস্কটপ এন্ট্রির জন্য নথির URI উল্লেখ করা যায় নয় বাতিল সেশন ম্যানেজারের সাথে সংযোগ স্থাপনে ব্যর্থ ICE লিসেন সকেট তৈরি করতে ব্যর্থ: %s সেশন ম্যানেজারের সাথে সংযোগ বিচ্ছিন্ন করা হয় ব্যবহারকারী দ্বারা নির্ধারিত অ্যাপ্লিকেশন লোড করা হবে না ব্যবহারকারীর নিশ্চিতকরণ প্রয়োজন নেই কোড ডিবাগ ব্যবস্থা সক্রিয় করুন কোড %d সহ বিদ্যমান FILE ফাইলটি একটি বৈধ .desktop ফাইল নয় তথাপি হাইবারনেট করা হবে ID '%s' আইকন খুঁজে পাওয়া যায়নি যেকোনো উপস্থিত ইনহিবিটরকে উপেক্ষা সিগন্যাল %d দ্বারা কিল করা হয়েছে পর্দা লক করুন যেকোনোভাবে লগ-আউট লগ-আউট এখনই সিস্টেম থেকে লগ-আউট করা হবে কি? চালু করার অযোগ্য আইটেম সাড়া দিচ্ছে না আদর্শ স্বয়ংক্রিয়ভাবে শুরু ডিরেক্টরি উপেক্ষা করা হবে পাওয়ার বন্ধ দ্বন্দ্বযুক্ত অপশন সহ প্রোগ্রাম কল করা হয়েছে সেশনটি বর্তমানে বন্ধ করা হচ্ছে এবং এ কারনে নতুন ক্লায়েন্ট সংযোগ প্রত্যাখ্যান করা হচ্ছে।
 স্থগিত করা (_u) সেশন পরিচালনা অপশন: যে সেশন ব্যবহার করা হবে সেশন পরিচালনা অপশন প্রদর্শন করা হবে যেকোনোভাবে বন্ধ এখনই সিস্টেম বন্ধ করা হবে কি? কয়েকটি প্রোগ্রাম এখনও চলছে: সংরক্ষিত কনফিগারেশন সহ ফাইল উল্লেখ করুন সেশন পরিচালনা ID উল্লেখ করুন শুরু করা হচ্ছে %s সিগন্যাল %d দ্বারা বন্ধ করা হয়েছে তথাপি স্থগিত করা হবে তথাপি ব্যবহারকারী পরিবর্তন করা হবে এই প্রোগ্রাম দ্বারা লগ-আউট ব্লক করা হচ্ছে। লগ-ইন সেশন শুরু করতে ব্যর্থ (X সার্ভারের সাথে সংযোগ স্থাপনে ব্যর্থ) অজানা অপরিচিত ডেস্কটপ ফাইলের সংস্করণ '%s' চালুকরণের অপরিচিত অপশন: %d প্রোগ্রাম সমাপ্তির জন্য অপেক্ষা করা হচ্ছে। প্রোগ্রাম বিঘ্নিত হলে আপনার কাজের ব্যাঘাত  ঘটতে পারে। প্রোগ্রাম সমাপ্তির জন্য অপেক্ষা করা হচ্ছে। প্রোগ্রাম বিঘ্নিত হলে আপনার কাজের ব্যাঘাত ঘটতে পারে। বর্তমানে আপনি "%s" নামে লগ-ইন করে আছেন। সুপ্ত করা (_H) লগ আউট (_L) পুনরায় চালু করা (_R) বন্ধ করা (_S) ব্যবহারকারী পরিবর্তন করুন (_S) 