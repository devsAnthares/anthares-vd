��    +      t  ;   �      �  
   �     �     �     �     �                         $     *     0     J     `     w  	   �     �     �     �     �     �     �     �     �     �     �     �     �     �          -     3     C     J  	   S     ]     p     w  	         �     �     �  �  �  
   �     �     �     �     �     �     �     �  	   �     �               (      F  	   g     q          �     �     �     �     �     �     �     �     �     �     �     �  !   	     '	     6	     E	     J	     X	     d	     v	     ~	     �	     �	     �	     �	                   !                                                             %          	         "   *      #              '      
   +                                   $       (   )          &           %1$s: %2$s Accessibility Add applets to the panel Administrator Applets Cancel Connect to... Copy Disabled Eject Error Execution of '%s' failed: Failed to launch '%s' Failed to unmount '%s' File System Hide Text Home Keyboard Looking Glass Menu No Open Out of date Panel Panel settings Paste Please enter a command: Remove Restart Cinnamon Restore all settings to default Retry Screen Keyboard Search Settings Show Text System Information Themes Unknown WORKSPACE Wrong password, please try again Yes Zoom Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-26 02:18+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:09+0000
X-Generator: Launchpad (build 18688)
 %1$s: %2$s Tilgjengefunksjonar Legg miniprogram til i panelet Administrator Småprogram Avbryt Kopla til … Kopier Slått av Løys ut Feil Klarte ikkje køyra «%s»: Klarte ikkje å starta «%s» Klarte ikkje å avmontera «%s» Filsystem Gøym teksten Heimeområde Tastatur Spegl Meny Nei Opna Utdatert Panel Panelinnstillingar Lim inn Skriv ein kommando: Fjern Start Cinnamon på nytt Gjenopprett standardinnstillingar Prøv på nytt Skjermtastatur Søk Innstillingar Vis teksten Systeminformasjon Drakter Ukjend ARBEIDSOMRÅDE Feil passord, prøv på nytt Ja Zoom 