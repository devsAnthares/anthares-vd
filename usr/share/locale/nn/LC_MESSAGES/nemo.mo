��    $      <  5   \      0  .   1  *   `  1   �      �  ,   �  o     i   {     �  Q   �     @     `     n  2   w  "   �     �     �  /   �  2     (   J      s  
   �  
   �     �     �      �  )   �  #     �   3  #   �  '   �  	                  *     3  �  8  ,   �	  *   %
  2   P
  $   �
  9   �
  l   �
  b   O  
   �  R   �  /        @  	   O  6   Y  8   �     �     �  5   �  3     ,   Q     ~     �     �     �     �     �  )   �       �   '  '   �  *   �                 	   1     ;            "                                          
          $       	                                                           !       #                                           

Browse the file system with the file manager --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. C_onnect Can't load the supported server method list.
Please check your gvfs installation. Cannot find the autorun program Connecting... Continue Create the initial window with the given geometry. Error starting autorun program: %s FTP (with login) GEOMETRY Nemo could not create the required folder "%s". Only create windows for explicitly specified URIs. Perform a quick set of self-check tests. Please verify your user details. Public FTP Quit Nemo. SSH Secure WebDAV (HTTPS) Show the version of the program. The folder "%s" cannot be opened on "%s". The server at "%s" cannot be found. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There was an error displaying help. There was an error displaying help: 
%s Try Again WebDAV (HTTP) Windows share [URI...] _Run Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-11-27 17:19+0000
Last-Translator: Elias Johan Liavaag <elias.johan.liavaag@gmail.com>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 

Bla gjennom filsystemet med filhandsamaren --check kan ikkje nyttast med andre flagg. --geometry kan ikkje nyttast med meir enn ein URI. --quit kan ikkje nyttast med URI-ar. <big><b>Feil ved automatisk køyring av program</b></big> <big><b>Dette mediet inneheld programvare som kan køyrast automatisk. Vil du køyra programvaren?</b></big> Før du køyrer Nemo, opprett den følgjande mappa, eller gi løyve slik at Nemo kan opprette den. K_opla til Klarte ikkje lesa lista over støtta tenarmetodar.
Kontroller gvfs-installasjonen. Kan ikkje finna program for automatisk køyring Koplar til … Hald fram Lag det fyrste vindauget med den oppgjevne geometrien. Feil ved oppstart av program for automatisk køyring: %s FTP (med pålogging) GEOMETRY Nemo kunne ikkje opprette den nødvendige mappa "%s". Berre lag vindauge for eksplisitt oppgjevne URI-ar. Gjennomfør eit sett med raske sjølvtestar. Kontroller brukardata. Offentleg FTP Avslutt Nemo. SSH Trygg WebDAV (HTTPS) Vis programversjon. Mappa «%s» kan ikkje opnast på «%s». Finn ikkje tenaren på «%s». Programvaren vil køyra direkte frå mediet «%s». Du bør aldri køyra program du ikkje stoler på.

Trykk Avbryt viss du er i tvil. Ein feil oppstod under vising av hjelp. Det oppstod feil under vising av hjelp:
%s Prøv om att WebDAV (HTTP) Delt Windows-ressurs [URI …] _Køyr 