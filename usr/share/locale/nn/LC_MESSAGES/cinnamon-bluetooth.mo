��    	      d      �       �      �   (   �   K         l     o  %   �  
   �     �  �  �     O  +   f  \   �     �     �  &        8     ?                                    	          Bluetooth is disabled Bluetooth is disabled by hardware switch If you remove the device, you will have to set it up again before next use. No No Bluetooth adapters found Remove '%s' from the list of devices? Visibility Yes Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-02-08 23:19+0000
Last-Translator: Åsmund Skjæveland <aasmunds@ulrik.uio.no>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Blåtann er deaktivert Bluetooth er slått av med maskinvarebrytar Om du fjernar denne eininga så må du setja henne opp igjen om du vil bruka henne på nytt. Nei Fann ingen blåtann-adapterar Fjern «%s» frå lista over einingar? Synleg Ja 