��          �   %   �      @     A     I  	   V     `     f      �  (   �     �     �     �     �     �  
   �     �            N        h     x     �     �     �     �     �    �     �     �  
   �     �     �  (     '   A     i     r     y  	   |     �  
   �     �     �     �  =   �     �     	               "     )     6                     
       	                                                                                                  %d x %d %d x %d (%s) All files China Could not detect displays Could not get screen information Could not save the monitor configuration Default Device Disabled France Germany IP Address Layout Monitor Not connected Select a monitor to change its properties; drag it to rearrange its placement. Select a region Show help options Spain United States Unknown Unspecified _All Settings Project-Id-Version: nn
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-control-center&component=general
PO-Revision-Date: 2013-08-26 10:56+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Norwegian Nynorsk <i18n-nn@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: nn
 %d × %d %d × %d (%s) Alle filer Kina Klarte ikkje oppdaga skjermar Klarte ikkje få informasjon om skjermen Klarte ikkje lagra skjerminnstillingane Standard Eining Av Frankrike Tyskland IP-adresse Formgjeving Skjerm Ikkje tilkopla Vel skjermen du vil endra. Drag den for å endra plasseringa. Vel ein region Vis hjelpeval Spania USA Ukjend Uspesifisert _Alle innstillingar 