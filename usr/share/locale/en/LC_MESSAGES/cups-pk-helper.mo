��          �      �           	     !     A      [     |     �     �     �  3   �  ;     5   S  <   �  w   �  @   >  9     3   �  K   �  5   9  I   o     �  /   �        �  $     �     �                @     X     q     �  3   �  ;   �  5   	  <   M	  w   �	  @   
  9   C
  3   }
  K   �
  5   �
  I   3     }  /   �      �                                     
                                                   	                Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer Project-Id-Version: cups-pk-helper
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-22 20:04+0100
PO-Revision-Date: 2013-11-20 09:29+0000
Last-Translator: Marek Kasik <mkasik@redhat.com>
Language-Team: English (http://www.transifex.com/freedesktop/cups-pk-helper/language/en/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Add/Remove/Edit a class Add/Remove/Edit a local printer Add/Remove/Edit a printer Add/Remove/Edit a remote printer Change printer settings Enable/Disable a printer Get list of available devices Get/Set server settings Privileges are required to add/remove/edit a class. Privileges are required to add/remove/edit a local printer. Privileges are required to add/remove/edit a printer. Privileges are required to add/remove/edit a remote printer. Privileges are required to change printer settings. This should only be needed from the Printers system settings panel. Privileges are required to enable/disable a printer, or a class. Privileges are required to get list of available devices. Privileges are required to get/set server settings. Privileges are required to restart/cancel/edit a job owned by another user. Privileges are required to restart/cancel/edit a job. Privileges are required to set a printer, or a class, as default printer. Restart/Cancel/Edit a job Restart/Cancel/Edit a job owned by another user Set a printer as default printer 