��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  S  w     �  "   �  @        L  	   f     p  F   �  j   �     7  7   L                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-09-08 22:19+0100
Last-Translator: Matej Urbančič <mateju@svn.gnome.org>
Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.7.5
 %s - javno dostopne datoteke %s - javno dostopne datoteke na %s Zaženi možnost souporabe osebnih datotek, če je ta omogočena Souporaba osebnih datotek Souporaba Nastavitve souporabe Omogoči osebno izmenjavo in souporabo datotek te mape preko omrežja. Kdaj zahtevati geslo. Mogoče vrednosti so "never" (nikoli), "on_write" (ob pisanju), in "always" (vedno). Kdaj zahtevati geslo souporaba;datoteke;http;omrežje;kopiranje;pošiljanje; 