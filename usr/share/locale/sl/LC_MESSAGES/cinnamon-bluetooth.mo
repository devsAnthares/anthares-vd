��    '      T  5   �      `     a     i  	   }     �     �  (   �     �     �     �  
             '  K   <     �     �     �     �     �     �     �     �     �       %        :     H     `     n     �     �  
   �     �     �     �     �     �     �     �  t  �     n     u  	   �     �     �  2   �     �  	             .     7     C  V   U     �     �     �  (   �     	     		     '	  
   .	     9	     U	  ,   \	     �	     �	     �	     �	     �	     �	     �	     
     
     
     .
     G
     c
     k
           $                    %                   '                 "   !      	                                            #                                  
   &                    Address Always grant access Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Does not match Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing request for %s Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-02-15 09:21+0000
Last-Translator: Matic Gradišer <Unknown>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Naslov Zmeraj dovoli dostop Bluetooth Nastavitve Bluetooth Bluetooth je onemogočen Bluetooth je onemogočen s stikalom strojne opreme Prebrskaj datoteke ... Prekliči Nastavitve za Bluetooth Povezava Se ne ujema Dovoli samo sedaj če boste napravo odstranili, jo bo potrebno pred naslednjo uporabo ponovno nastaviti. Nastavitve tipkovnice Se ujema Nastavitve miške Nastavitve miške in sledilne ploščice Ne Ni navzočih Bluetooth naprav V redu Seznanjeno Zahteva po seznanitvi za %s Zavrni Ali naj bo '%s' odstranjen s seznama naprav? Odstrani napravo Pošlji datoteke na napravo... Pošlji datoteke ... Nastavi novo napravo Nastavitve zvoka Vrsta Vidnost Vidnost naprave “%s” Da povezovanje ... prekinjanje povezave ... strojna oprema onemogočena stran 1 stran 2 