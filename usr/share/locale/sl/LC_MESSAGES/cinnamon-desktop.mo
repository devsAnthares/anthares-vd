��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     
  J     U
     b
     r
     �
  
   �
     �
     �
     �
     �
     �
     �
  K   �
  Q   K     �     �     �     �     �      
  $   +  $   P  &   u  -   �  8   �  *     I   .  <   x  �   �  &   �  �   �  �   P  E   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-06 19:36+0100
Last-Translator: Matej Urbančič <mateju@svn.gnome.org>
Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Poedit-Country: SLOVENIA
X-Poedit-Language: Slovenian
X-Poedit-SourceCharset: utf-8
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %e. %B %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d ne podpira odvoda %s CRTC %d ne podpira vrtenja=%s CRTC %d: preizkusni način %dx%d@%dHz z odvodom pri %dx%d@%dHz (prehod %d)
 Ni mogoče najti terminala, zato bo uporabljen xterm, četudi morda ne bo deloval Prenosni računalnik Zrcaljena zaslona Neznano Razširitev RANDR ni na voljo Preizkušanje načinov CRTC %d
 ni mogoče klonirati na odvod %s ni mogoče dodeliti CRTC odvodom:
%s ni mogoče dobiti podatkov o CRTC %d ni mogoče dobiti podatkov o odvodu %d ni mogoče dobiti območja velikosti zaslonov ni mogoče dobiti virov zaslonov (CRTC, odvodi, načini) ni mogoče določiti nastavitev za CRTC %d nobena od shranjenih nastavitev zaslona ni primerna za dejavno nastavitev noben od izbranih načinov ni skladen z načini na voljo:
%s odvod %s nima določenih enakih parametrov kot drugi kloniran odvod:
obstoječi način = %d, novi način = %d
obstoječe koordinate = (%d, %d), nove koordinate = (%d, %d)
obstoječe vrtenje = %s, novo vrtenje = %s odvod %s ne podpira načina %dx%d@%dHz zahtevan podatek položaja ali velikosti za CRTC %d je izven dovoljenih omejitev: položaj=(%d, %d), velikost=(%d, %d), največja dovoljena vrednost=(%d, %d) zahtevana navidezna velikost ne sovpada z razpoložljivo velikostjo: zahtevano=(%d, %d), najmanjša vrednost=(%d, %d), največja vrednost=(%d, %d) nepričakovana X napaka med pridobivanjem območja velikosti zaslonov 