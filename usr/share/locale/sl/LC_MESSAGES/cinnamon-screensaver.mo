��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  /   0     `  M   p     �  ,   �     �  +     '   A     i  J   |  �   �  "   �	  "   �	  +   �	  0    
     1
     9
     M
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=cinnamon-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-02-15 09:25+0000
Last-Translator: Matic Gradišer <Unknown>
Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: SLOVENIA
Language: 
X-Poedit-Language: Slovenian
X-Poedit-SourceCharset: utf-8
 Omogoča enostaven izhod ohranjevalnika zaslona Preverjanje ... V primeru, da je ohranjevalnik zaslona dejaven, ga izključi (napolni zaslon) Napačno geslo Sporočilo, prikazano na zaklenjenem zaslonu Prosimo, vpišite geslo... Poizvedba o trajanju ohranjevalnika zaslona Poizvedba stanja ohranjevalnika zaslona Preklop uporabnika Opravilu ohranjevalnika zaslona v teku ukaže, naj nemudoma zaklene zaslon Ohranjevalnik zaslona je bil dejaven %d sekund.
 Ohranjevalnik zaslona je bil dejaven %d sekundo.
 Ohranjevalnik zaslona je bil dejaven %d sekundi.
 Ohranjevalnik zaslona je bil dejaven %d sekunde.
 Ohranjevalnik zaslona je dejaven.
 Ohranjevalnik zaslona ni dejaven.
 Ohranjevalnik zaslona trenutno ni dejaven.
 Vključi ohranjevalnik zaslona (izprazni zaslon) Odkleni Različica programa Vključene so velike črke. 