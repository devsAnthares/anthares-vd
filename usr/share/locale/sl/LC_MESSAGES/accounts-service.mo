��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  5   k  7   �  3   �  (     &   6     ]     |  '   �  Z   �                    	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Andrej Znidarsic <andrej.znidarsic@gmail.com>
Language-Team: Slovenian <lugos-slo@lugos.si>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Za spremembo prijavnega zaslona je zahtevana overitev Za spremembo podatkov uporabnikov je zahtevana overitev Za spremembo lastnih podatkov je zahtevana overitev Spremenite nastavitev prijavnega zaslona Spremenite lastne uporabniške podatke Omogoči razhroščevanje kode Upravljaj račune uporabnikov Izpiši podatke o različici in končaj Zagotavlja vmesnike D-Bus za povpraševanje in upravljanje
s podatki računov uporabnikov. Zamenjaj obstoječo pojavitev 