��          L      |       �   /   �      �   =   �   +   5     a  �  i  G   �      F  M   g  +   �  
   �                                         Error (%s) converting data for child, dropping. Error reading from child: %s. GnuTLS not enabled; data will be written to disk unencrypted! Unable to convert characters from %s to %s. WARNING Project-Id-Version: vte vte-0-34
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=vte&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-26 11:00+0200
Last-Translator: Fabio Tomat <f.t.public@gmail.com>
Language-Team: Friulian <fur@li.org>
Language: fur
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
 Erôr (%s) convertint i dâts par il proces fî, a restaran come prime. Erôr leint dal procès fî: %s. GnuTLS no abilitât; i dâts a vignaran scrits tal disc cence jessi cifrâts! Impussibil convertî i caratars di %s a %s. AVERTIMENT 