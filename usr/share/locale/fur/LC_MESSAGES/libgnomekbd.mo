��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  �  B  2   8
     k
  ,   w
     �
     �
  �   �
     ^     m  -   ~  %   �  /   �  D        G     V  A   q  7   �  {   �  +   g  *   �      �  "   �       .        D  1   Y     �  5   �     �  6   �  7   (     `     n     �     �  &   �     �     �     �                      
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2013-03-19 21:39+0000
PO-Revision-Date: 2013-08-20 14:47+0100
Last-Translator: Fabio Tomat <f.t.public@gmail.com>
Language-Team: Friulian <fur@li.org>
Language: fur
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.4
 Grup predefinît, assegnât ae creazion dal balcon Indicadôr: Manten e ministre grups separâts par balcon Disposizion tastiere Disposizion tastiere Disposizion di tastiere «%s»
Copyright &#169; X.Org Foundation e colaboradôrs di XKeyboardConfig
Par le licenze viôt i metadâts dal pachet Model tastiere Opzions tastiere Cjame disposizions e opzions dopradis di râr Cjame elements di configurazion extra Mostre une anteprime de disposizion di tastiere Salve/torne a meti i indicadôrs insieme cun i grups di disposizions Grup secondari Mostre bandiere tal applet Mestre lis bandieris tal applet par indicâ le disposizion curint Mostre i nons di disposizion invezit che i nons di grup Mostre i nons di disposizion invezit che i nons di grup (dome par versions di XFree che a supuartin disposizions multiplis) Le anteprime di tastiere, offset orizontâl Le anteprime di tastiere, offset verticâl Le anteprime di tastiere, altece Le anteprime di tastiere, largjece Il colôr di fonts Il colôr di fonts par il seletôr de posizion La famee dal caratar La famee dal caratar par l'indicadôr de posizion La dimension dal caratar La dimension dal caratar par l'indicadôr de posizion Il colôr di prin plan Il colôr di prin plan par l'indicadôr de disposizion Al è saltât fûr un erôr tal cjariâ une imagjin: %s No cognossude Erôr di iniziazion di XKB disposizion tastiere model tastiere disposizion «%s» disposizions «%s» model «%s», %s e %s nissune disposizion nissune opzion opzion «%s» opzions «%s» 