��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �     �     �  	                  -     @     I  '   U  "   }  X   �  +   �     %	     4	  +   R	  \   ~	  �   �	  2   �
  v   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop 2.19.x
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-07 09:54+0000
PO-Revision-Date: 2018-08-08 06:42+0200
Last-Translator: Fabio Tomat <f.t.public@gmail.com>
Language-Team: Friulian <massimo.furlani@libero.it>
Language: fur
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
 %k.%M %k.%M.%S %a %k.%M %a %k.%M.%S %a %-e %b_%R %a %-e %b_%R.%S %a %-e %b_%l.%M %p %a %-e %b_%l.%M.%S %p %a %l.%M %p %a %l.%M.%S %p %-e %b_%R %-e %b_%R.%S %-e %b_%l.%M %p %-e %b_%l.%M.%S %p %l:%M %p %l.%M.%S %p CRTC %d no puès comandà la jessude %s CRTC %d no supuarte la rotazion=%d CRTC %d: O stoi provant la modalitât %dx%d@%dHz cun la jessude a %dx%d@%dHz (passe %d)
 O stoi provant las modalitâts par CRTC %d
 No specificât no puès clonâ la jessude %s impussibil assegnâ i CRTC aes jessudis:
%s nissune des modalitâts selezionadis a jerin compatibilis cun lis modalitâts pussibilis:
%s la jessude %s no à i stes parametris come un'altre jessude clonade:
modalitât esistent = %d, gnove modalitât = %d
coordinadis esistentis = (%d, %d), gnovis coordinadis = (%d, %d)
rotazion esistent = %d, gnove rotazion = %d la jessude %s no supuarte la modalitât %dx%d@%dHz la dimension virtual domandade a no sta te dimension disponibil: domandade=(%d, %d), minime=(%d, %d), massime=(%d, %d) 