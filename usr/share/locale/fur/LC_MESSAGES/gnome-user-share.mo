��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     #     6  9   O     �     �     �  W   �  T   !     v  '   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-03-02 18:53+0100
Last-Translator: Fabio Tomat <f.t.public@gmail.com>
Language-Team: Friulian <fur@li.org>
Language: fur
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
 File publics di %s File publics di %s su %s Fâs partî la Condivision dai File Personai se abilitade Condivision file personâi Condivision Impostazions condivision Pie Condivision file personâi par condividi il contignût di cheste cartele supe rêt. Cuand domandâ lis password. Valôrs pussibii a son "never", "on_write", e "always". Cuant domandâ lis password condividi;file;http;rêt;copiâ;inviâ; 