��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  8   r     �  O   �       0   #  %   T  I   z  ,   �     �  d     v   l  "   �  $   	  6   +	  2   b	     �	     �	  .   �	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-11-10 17:07+0000
Last-Translator: AO <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Demande à l'économiseur d'écran de quitter proprement Vérification... Si l'économiseur d'écran est actif alors le désactiver (réaffiche l'écran) Mot de passe incorrect Message à afficher sur l'écran de verrouillage Veuillez saisir votre mot de passe… Indiquer la durée pendant laquelle l'économiseur d'écran a été actif Vérifier l'état de l'économiseur d'écran Changer d'utilisateur Demande à l'économiseur d'écran en cours de fonctionnement de verrouiller l'écran immédiatement L'économiseur d'écran a été actif pendant %d seconde.
 L'économiseur d'écran a été actif pendant %d secondes.
 L'économiseur d'écran est actif
 L'économiseur d'écran est inactif
 L'économiseur d'écran n'est pas actif actuellement.
 Démarrer l'économiseur d'écran (cache l'écran) Déverrouiller Version de cette application Votre verrouillage des majuscules est activé. 