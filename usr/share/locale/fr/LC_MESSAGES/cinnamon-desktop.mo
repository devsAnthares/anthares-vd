��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     7  J     �	     �	     �	     �	     �	     �	     �	     �	     �	  ,   �	  4   
  L   Q
  Z   �
     �
            %   $      J  %   k  0   �  4   �  6   �  >   .  C   m  7   �  \   �  G   F  �   �  6   �  �   �  �   B  U   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-04 23:21+0200
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %l:%M %p %A %e %B, %l:%M:%S %p %A %e %B %R %R:%S %l:%M %p %l:%M:%S %p le CRTC %d ne peut pas piloter une sortie %s le CRTC %d ne prend pas en charge la rotation = %s CRTC %d : test du mode %dx%d@%dHz avec une sortie à %dx%d@%dHz (passe %d)
 Impossible de trouver un terminal, xterm sera utilisé mais il pourrait ne pas fonctionner Ordinateur portable Écrans clones Inconnu l'extension RANDR n'est pas présente Tests des modes pour le CRTC %d
 impossible de cloner sur la sortie %s impossible d'assigner des CRTC aux sorties :
%s impossible d'obtenir des informations sur le CRTC %d impossible d'obtenir des informations sur la sortie %d impossible d'obtenir la liste des tailles d'écran disponibles impossible d'obtenir les ressources d'écran (CRTC, sorties, modes) impossible de définir la configuration pour le CRTC %d aucune des configurations d'affichage enregistrées ne correspond à la configuration active aucun des modes choisis n'est compatible avec les modes possibles :
%s La sortie %s ne possède pas les mêmes paramètres que l'autre sortie clone :
mode actuel = %d, nouveau mode = %d
coordonnées actuelles = (%d, %d), nouvelles coordonnées = (%d, %d)
rotation actuelle = %s, nouvelle rotation = %s la sortie %s ne prend pas en charge le mode %dx%d@%dHz la taille et la position demandées au CRTC %d sont hors des limites autorisées : position=(%d, %d), taille=(%d, %d), maximum=(%d, %d) la taille virtuelle demandée n'est pas adaptée à la taille disponible : demande=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) erreur X non gérée lors de l'obtention de la liste des tailles d'écran disponibles 