��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w          /  8   M     �     �     �  g   �  �   ,     �  4   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-15 13:23+0200
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Fichiers publics de %s Fichiers publics de %s sur %s Lance le partage de fichiers personnels s'il est activé Partage de fichiers personnels Partage Paramètres de partage Activer le Partage de fichiers personnels pour partager le contenu de ce dossier à travers le réseau. Quand demander des mots de passe. Les valeurs possibles sont : « never » (jamais), « on_write » (à l'écriture) et « always » (toujours). Quand exiger les mots de passe partager;fichiers;http;réseau;copier;envoi;envoyer; 