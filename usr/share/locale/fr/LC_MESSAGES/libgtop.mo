��    )      d  ;   �      �     �     �     �     �     �     �  	             !     :     C  	   `     j     {     �     �     �     �     �     �  	                   .     3     I  J   N     �     �     �  
   �     �     �     �          '     :  ,   U     �     �  �  �  
   �     �      �  "   �  !   �      	  
   ,	     7	     V	  	   p	     z	  
   �	     �	     �	     �	     �	     

     
     )
     ?
     U
     b
     
     �
     �
     �
  U   �
          )     0     >     X     u     �     �      �     �  B   �     .  ,   G         %           '         )               "       
                                                             !                                                     $       	   (                 #   &    Abort Alarm clock Background read from tty Background write to tty Bad argument to system call Broken pipe Bus error CPU limit exceeded Child status has changed Continue Don’t fork into background EMT error Enable debugging Enable verbose output File size limit exceeded Floating-point exception Hangup I/O now possible Illegal instruction Information request Interrupt Invoked from inetd Keyboard stop Kill Profiling alarm clock Quit Run “%s --help” to see a full list of available command line options.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket User defined signal 1 User defined signal 2 Virtual alarm clock Window size change read %d byte read %d bytes read %lu byte of data read %lu bytes of data read data size wrote %d byte wrote %d bytes Project-Id-Version: libgtop 2.9.91
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgtop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2017-04-07 11:45+0000
PO-Revision-Date: 2017-08-15 09:18+0200
Last-Translator: Stéphane Raimbault <stephane.raimbault@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
 Annulation Alarme d'horloge Lecture sur tty en arrière-plan Écriture sur tty en arrière-plan Mauvais argument d'appel système Tube cassé Erreur bus Limite de temps CPU dépassée L'état du fils a changé Continuer Ne pas lancer en tâche de fond Erreur EMT Active le débogage Active la sortie bavarde Taille de fichier excessive Exception virgule flottante Hangup E/S maintenant possible Instruction illégale Demande d'information Interruption Invoqué à partir de inetd Arrêt depuis le clavier Tuer Profile de l'alarme Quitter Lancer « %s --help » pour afficher la liste des options de la ligne de commande.
 Violation de segmentation Arrêt Signal de fin Point d'arrêt rencontré Condition urgente sur socket Signal utilisateur 1 Signal utilisateur 2 Alarme virtuelle Redimensionnement de la fenêtre %d octet lu %d octets lus lecture de %lu octet de données lecture de %lu octets de données taille des données lues écriture de %d octet écriture de %d octets 