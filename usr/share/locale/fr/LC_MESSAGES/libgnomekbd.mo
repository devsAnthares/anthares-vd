��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >          +     ?     R  "   n  #   �     �  A   �  ,         D     e  5   s  $   �  $   �     �     	  �        �     �  5   �  4   &     [  )   o  E   �     �  "   �  D     8   [  �   �  !   !  !   C     e     �     �  9   �     �  7        G  2   [     �  9   �     �  :   �  6   8  %   o     �     �     �     �     �  ,   �     (     E     V  $   d                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd HEAD fr
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2011-02-25 01:17+0000
PO-Revision-Date: 2010-08-15 21:32+0200
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
 Activer plus de greffons _Greffons actifs : Ajouter un greffon Ferme la boîte de dialogue Configure le greffon sélectionné Désactive le greffon sélectionné Diminue la priorité du greffon Groupe par défaut, attribué lors de la création d'une fenêtre Active ou désactive les greffons installés Augmente la priorité du greffon Indicateur : Gère et maintient des groupes distincts par fenêtre Greffons de l'Indicateur de claviers Greffons de l'Indicateur de claviers Agencement de clavier Agencement de clavier Agencement de clavier « %s »
Copyright &#169; X.Org Foundation et contributeurs XKeyboardConfig
Les détails de licence se trouvent dans les méta-données du paquet. Modèle de clavier Options de clavier Charger les agencements et options rarement utilisés Charger les objets de configuration supplémentaires Aucune description. Prévisualiser les agencements de clavier Enregistre et restaure les indicateurs avec les groupes d'agencements Groupes secondaires Affiche les drapeaux dans l'applet Affiche des drapeaux dans l'applet pour indiquer l'agencement actuel Affiche les noms d'agencement au lieu des noms de groupe Affiche les noms d'agencement au lieu des noms de groupe (uniquement pour les versions de XFree prenant en charge les agencements multiples) L'aperçu du clavier, décalage X L'aperçu du clavier, décalage Y L'aperçu du clavier, hauteur L'aperçu du clavier, largeur La couleur d'arrière-plan La couleur d'arrière-plan pour l'indicateur d'agencement La police de caractères La police de caractères pour l'indicateur d'agencement La taille de police La taille de police pour l'indicateur d'agencement La couleur de premier plan La couleur de premier plan pour l'indicateur d'agencement Liste des greffons actifs La liste des greffons activés de l'Indicateur de claviers Une erreur s'est produite en chargeant une image : %s Impossible d'ouvrir le fichier d'aide Inconnu Erreur d'initialisation XKB Greffons _disponibles : agencement de clavier modèle de clavier agencement « %s » agencements « %s » modèle « %s », %s et %s aucun agencement aucune option option « %s » options « %s » 