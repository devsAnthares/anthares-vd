��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     e     h     n     t     }     �     �     �     �     �  	   �     �     �               !  ,   -  4   Z  L   �      �     �  %   	  2   1	  I   d	  �   �	  6   �
  �   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-04 21:21+0000
PO-Revision-Date: 2018-08-09 10:30+0200
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %R %R:%S %a %R %a %R:%S %a %-e %b_%R %a %-e %b_%R:%S %a %-e %b_%l:%M %p %a %-e %b_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %-e %b_%R %-e %b_%R:%S %-e %b_%l:%M %p %-e %b_%l:%M:%S %p %l:%M %p %l:%M:%S %p le CRTC %d ne peut pas piloter une sortie %s le CRTC %d ne prend pas en charge la rotation = %d CRTC %d : test du mode %dx%d@%dHz avec une sortie à %dx%d@%dHz (passe %d)
 Tests des modes pour le CRTC %d
 Non précisé impossible de cloner sur la sortie %s impossible d’assigner des CRTC aux sorties :
%s aucun des modes choisis n’est compatible avec les modes possibles :
%s La sortie %s ne possède pas les mêmes paramètres que l’autre sortie clone :
mode actuel = %d, nouveau mode = %d
coordonnées actuelles = (%d, %d), nouvelles coordonnées = (%d, %d)
rotation actuelle = %d, nouvelle rotation = %d la sortie %s ne prend pas en charge le mode %dx%d@%dHz la taille virtuelle demandée n’est pas adaptée à la taille disponible : demande=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) 