��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 }  %     �	     �	     �	  	   �	     �	     
  :   !
     \
     v
  $   ~
  	   �
  6   �
  K   �
     0  -   B  !   p  h   �     �  
          ,   6     c  $   g     �     �     �     �  V   �  :   +     f  5   n     �     �     �  $   �          2  >   E     �     �     �     �     �     �     �     �     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-17 11:09+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Toujours autoriser l'accès Demande d'autorisation de %s Bluetooth Paramètres Bluetooth Le bluetooth est désactivé Le Bluetooth est désactivé par un interrupteur matériel Parcourir les fichiers... Annuler Configurer les paramètres Bluetooth Connexion Le périphérique %s souhaite accéder au service '%s' Le périphérique %s a effectué une demande de pairage avec cet ordinateur Ne correspond pas Une erreur est survenue lors de la navigation Autoriser cette fois-ci seulement Si vous retirez le périphérique, vous devrez le paramétrer à nouveau avant la prochaine utilisation. Paramètres du clavier Correspond Paramètres de la souris Paramètres de la souris et du pavé tactile Non Aucun adaptateur Bluetooth détecté Ok Apparié Confirmation de pairage pour %s Requête de pairage pour %s Veuillez confirmer que le PIN '%s' correspond à celui affiché sur le périphérique. Veuillez entrer le code PIN affiché sur le périphérique Refuser Retirer « %s » de la liste des périphériques ? Retirer le périphérique Envoyer des fichiers ... Envoyer des fichiers... Configurer un nouveau périphérique Ajouter un périphérique ... Paramètres du son La navigation n'est pas possible pour ce périphérique : '%s' Type Visibilité Visibilité de « %s » Oui connexion en cours... déconnexion en cours... matériel désactivé page 1 page 2 