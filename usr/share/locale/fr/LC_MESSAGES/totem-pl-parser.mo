��    	      d      �       �      �      �      �      �      	  &     1   D     v  �       <     E     M     Q     i  :   �  6   �  	   �                     	                         Audio CD Blu-ray DVD Digital Television Failed to mount %s. No media in drive for device “%s”. Please check that a disc is present in the drive. Video CD Project-Id-Version: totem-pl-parser HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/totem-pl-parser/issues
PO-Revision-Date: 2018-11-19 13:41+0100
Last-Translator: Charles Monzat <charles.monzat@numericable.fr>
Language-Team: français <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gtranslator 3.30.0
 CD audio Blu-ray DVD Télévision numérique Le montage de %s a échoué. Aucun média dans le lecteur du périphérique « %s ». Vérifiez qu’un disque est présent dans le lecteur. Vidéo CD 