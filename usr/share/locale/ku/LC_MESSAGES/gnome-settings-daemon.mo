��    D      <  a   \      �     �     �  K   �  [   E  &   �     �  (   �  S   �      K  "   l  $   �  $   �  &   �                "  >   (     g  9   l     �  7   �  8   �  #   #	     G	     g	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     /
     6
     H
     N
     ^
     d
     v
  #   �
     �
  #   �
  -   �
  &     m   3     �     �     �     �     �  $   �  	   �  *     �   3  �   �  �   N  	   �     �          &     ,  �  ;     �     	  U     d   e  *   �     �  *   �  T   )  )   ~  /   �  7   �  *     (   ;     d     q       7   �     �  L   �       C   *  I   n  3   �  -   �  )        D     K     f          �     �     �     �     �     �     �     �  %   �  
        (     @  !   G     i  #   n     �  ?   �     �  &     .   *  1   Y  u   �                    -     9  1   J     |  #   �  �   �  �   c  �        �     �  $   �  	                    3                   ;      4   =   !              5                     
   :   "      2   D      #       ,       1   C       -         ?                 7           )   6         <               @      0                         *          A       8   '   +         %                        $   9   &       	   .      (   B           >       /       A_vailable files: Boing Cannot create the directory "%s".
This is needed to allow changing cursors. Cannot create the directory "%s".
This is needed to allow changing the mouse pointer theme. Cannot determine user's home directory Clink Couldn't load sound file %s as sample %s Couldn't put the machine to sleep.
Verify that the machine is correctly configured. Do _not show this warning again. Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Eject Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Home folder Key Binding (%s) has its action defined multiple times
 Key Binding (%s) has its binding defined multiple times
 Key Binding (%s) is already in use
 Key Binding (%s) is incomplete
 Key Binding (%s) is invalid
 Keyboard Launch calculator Launch help browser Launch web browser Load modmap files Lock screen Log out Login Logout Mouse Mouse Preferences No sound Play (or play/pause) Search Select Sound File Siren Slow Keys Alert Sound Start screensaver Sticky Keys Alert Sync text/plain and text/* handlers System Sounds The file %s is not a valid wav file The sound file for this event does not exist. There was an error displaying help: %s There was an error starting up the screensaver:

%s

Screensaver functionality will not work in this session. Typing Break Volume Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. _Activate _Deactivate _Do not show this message again _Load _Loaded files: Project-Id-Version: gnome-control-center.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2007-03-04 18:21+0100
Last-Translator: Erdal Ronahi <erdal.ronahi@gmail.com>
Language-Team: Kurdish <gnu-ku-wergerandin@lists.sourceforge.net>
Language: ku
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: KBabel 1.10
X-Rosetta-Export-Date: 2007-03-04 16:18+0000
 Pelên _heyî: Boing Nikare pêrista "%s" ava bike.
Ev pêwiste da tu bikaribî nîşankeran biguherînî. Nikare pêrista "%s" ava bike.
Ev pêwiste da tu bikaribî mijara nîşankerê mişkê biguherînî. Makepêrista bikarhêner nehate diyarkirin Tikandin Pelê dengê %s wekî mînaka %s bar nekir Mekîne nekete moda razanê.
Saxtî bike bê mekîne rast hatiye mîhengkirin an na. Vê hişyariyê careke din nîşan _nede. Tu dixwazî Hêdîkirina Bişkokan çalak bike? Tu dixwazî taybetiya Bişkokên Mezeloqî çalak bike? Tu dixwazî Hêdîkirina Bişkokan bigire? Tu dixwazî Bişkokên Mezeloqî bigire? Çalak _neke Ra_newestîne Bavêje Dema (%s) dixebitî çewtî
bi bişkoka (%s) girêdayî Cureyê nivîsê Mifteya GConf %s, li cureyê %s hatiye mîhengkirin, lê cureyê pêwist %s
 Peldanka mal Bikêrhatina (%s) ya Girêdana Bişkokan gelekî hatiye diyarkirin
 Girêdayîna (%s) ya Girêdana Bişkokan gelekî zêde hatiye diyarkirin
 Girêdana Bişkokê (%s) jixwe niha tê bikaranîn
 Girêdana Bişkokê (%s) nehatiye temamkirin
 Girêdana Bişkokê (%s) ne derbasdar e.
 Klavye Jimêreyê bide xebitandin Gerokê alîkariyê veke Geroka webê veke Pelên modmap bar bike Ekranê qifil bike Derkeve Teketin Derketin Mişk Vebijêrkên mikşê Bêdeng Lê bide (an jî lê bide/rawestîne) Lê bigere Pelê Dengê Hilbijêre Sîren Hişyariya Hêdîkirina Bişkokan Deng Dîmenderparêzê bide destpêkirin Hişyariya Bişkokên Mezeloqî Xebatkerên text/plain ve text/* bi awayekî hevpar bikar bîne Dengên Pergalê Pelê %s ne pelekî wav ya derbasdar e Ji bo vê bûyerê pelê dengan nehate dîtin. Di nîşandana alîkariyê de çewtî çêbû: %s Di destpêkirina dîmender parêzê de çewtî çêbûbû:

%s

Di vê beşê de dê dîmenderparêz neyê xebitandin Bêhnvedana Nivîsînê Deng Deng kêm bike Deng bibire Rêjekirina deng Rêjekirina deng li gor rêjeya deng ya ji sedî. Deng zêde bike Tu dixwazî pelên modmap bar bike? Bila qederê 8 çirkeyan tiliya te li ser bişkoka Shift be. Ev, kurteriya ku ji bo taybetmendiya Hêdîkirina Bişkokan e da ku celebê xebitandina kompîtura te bi bandor bike. 5 caran li ser hev pêl bişkoka Shift bike.Ev, kurteriya ku ji bo taybetmendiya Bişkokên Mezeloqî ye da ku celebê xebitandina kompîtura te bi bandor bike. Te bi carekê re pêl du heb bişkokan kir yan jî te li ser hev 5 caran pêl bişkoka Shift kir. Ev, taybetmendiya Bişkokên Mezeloqî ye da ku ji bo celebê xebitandina klavyeya te bandor dike digire. _Çalak bike _Neçalak bike _Vê peyamê careke din nîşan nede _Bar bike _Pelên barkirî: 