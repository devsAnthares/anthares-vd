��          D      l       �   &   �   �   �   �   <  �   �  �  m  1   +  �   ]  �     �   �                          There was an error displaying help: %s You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works. You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works. You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works. Project-Id-Version: gnome-control-center.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 10:11+0000
Last-Translator: Erdal Ronahi <erdal.ronahi@gmail.com>
Language-Team: Kurdish <gnu-ku-wergerandin@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:17+0000
X-Generator: Launchpad (build 18688)
 Di nîşandana alîkariyê de çewtî çêbû: %s Bila qederê 8 çirkeyan tiliya te li ser bişkoka Shift be. Ev, kurteriya ku ji bo taybetmendiya Hêdîkirina Bişkokan e da ku celebê xebitandina kompîtura te bi bandor bike. 5 caran li ser hev pêl bişkoka Shift bike.Ev, kurteriya ku ji bo taybetmendiya Bişkokên Mezeloqî ye da ku celebê xebitandina kompîtura te bi bandor bike. Te bi carekê re pêl du heb bişkokan kir yan jî te li ser hev 5 caran pêl bişkoka Shift kir. Ev, taybetmendiya Bişkokên Mezeloqî ye da ku ji bo celebê xebitandina klavyeya te bandor dike digire. 