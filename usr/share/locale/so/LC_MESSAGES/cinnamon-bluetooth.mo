��    &      L  5   |      P     Q     Y  	   m     w  (   �     �     �     �  *   �          $  K   9     �     �     �     �     �     �     �     �        B     -   Z     �  %   �     �     �     �     �     �          !  
   &     1     H     L     S  l  Z     �     �  	   �     �  *   
     5     B     J  3   \     �     �  3   �     �  
   �     		     (	     -	     B	     E	     T	     p	  9   �	  #   �	     �	     �	     
     
     1
     >
     P
     d
     r
  	   w
     �
     �
  
   �
  
   �
            $   "                             !      &                     #                                      
                                              	   %                    Address Always grant access Bluetooth Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Device %s wants to pair with this computer Does not match Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings Type Visibility Visibility of “%s” Yes page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-30 14:05+0000
Last-Translator: Faisal Ali <Unknown>
Language-Team: Somali <so@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Cinwaan Mar walba u ogolow Bluetooth Bluetooth Ku wuu xidhan yahay Bluetoothku wuxuu ka xidhan yahay badhanka Eeg Faylasha Ka noqo Rakib Bluetoothka Qalabkan %s ayaa raba in uu kombuyuutarka ku xidhmo la mid maaha Iminka oo keliya u ogolow Hadaad qalabkan ka saarto, waa inaad dib u rakibtaa Dejinta Keyboardka Waa la mid Dejinta Mouseka iyo Touchpadka Maya Bluetooth lama helin OK La isku beegay Waa la isku xidhiidhiyay %s Waxaa xidhiidhin dalbaday %s Fadlan hubi in PIN '%s' la mid yahay ka qalabka ka muuqda Fadlan qor PIN'ka ka muuqda qalabka Diid Ka saar '%s' liiska qalabka Ka saar qalabka Qalabka faylal u dir Dir Faylasha Rakib qalab cusub Qalab cusub diyaari Dejinta Codka Nooc Muuqaalka Muuqaalka “%s” Haa bogga 1aad bogga 2aad 