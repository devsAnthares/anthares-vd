��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  @        L  P   ]     �  0   �  %   �  <   !  ,   ^     �  X   �  v   �  "   s  $   �  6   �  4   �     '	     6	  "   S	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-11-05 10:46+0000
Last-Translator: AO <Unknown>
Language-Team: French (Canada) <fr_CA@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Entraîne une fermeture gracieuse de l’économiseur d’écran Vérification… Si l’économiseur d’écran est actif, alors le désactiver (allume l'écran) Le mot de passe est erroné Message à afficher sur l'écran de verrouillage Veuillez saisir votre mot de passe… Obtenir la durée d’activité de l’économiseur d'écran Obtenir l’état de l'économiseur d'écran Changer d’utilisateur Indique à l’économiseur d’écran en cours de verrouiller l’écran immédiatement L'économiseur d'écran a été actif pendant %d seconde.
 L'économiseur d'écran a été actif pendant %d secondes.
 L'économiseur d'écran est actif
 L'économiseur d'écran est inactif
 L'économiseur d'écran n'est actuellement pas actif.
 Activer l'économiseur d'écran (éteindre l'écran) Déverrouiller Version de cette application La touche Verr. Maj. est activée. 