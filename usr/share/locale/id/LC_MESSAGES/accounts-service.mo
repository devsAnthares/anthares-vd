��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  @   Q  2   �  :   �            !     >     Z  $   o  V   �     �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-02-14 08:39+0000
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian (http://www.transifex.com/projects/p/freedesktop/language/id/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id
Plural-Forms: nplurals=1; plural=0;
 Otentikasi diperlukan untuk mengubah konfigurasi layar log masuk Otentikasi diperlukan untuk mengubah data pengguna Otentikasi diperlukan untuk mengubah data pengguna milikmu Ubah konfigurasi layar log masuk Ubah data penggunamu sendiri Aktifkan kode pengawakutuan Kelola akun pengguna Tampilkan informasi versi dan keluar Menyediakan antar muka D-Bus untuk menanyakan dan memanipulasi
informasi akun pengguna Ganti instansi yang ada 