��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     �	     �	  	   
     
     
     
     '
  &   3
  !   Z
  M   |
  W   �
     "     )     9     E     c  (   �  -   �  1   �  5   	  -   ?  B   m  .   �  ^   �  A   >  �   �  ,   e  �   �  ~     4   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-04 17:58+0700
Last-Translator: Dirgita <dirgitadevina@yahoo.co.id>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 1.2
 %A %e %B, %R %A %e %B, %R:%S %A %e %B, %k:%M %A %e %B, %k:%M:%S %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d tidak dapat memacu keluaran %s CRTC %d tidak mendukung rotasi=%s CRTC %d: mencoba modus %dx%d@%dHz dengan keluaran pada %dx%d@%dHz (lewat %d)
 Tidak menemukan terminal, beralih menggunakan xterm, meskipun tidak bekerja dengan baik Laptop Tampilan Kembar Tak Dikenal ekstensi RANDR tidak tersedia Mencoba modus untuk CRTC %d
 duplikasi untuk %s tidak dapat dilakukan tidak dapat menetapkan CRTC pada keluaran:
%s tidak dapat memperoleh informasi mengenai CRTC %d tidak dapat memperoleh informasi mengenai keluaran %d tidak dapat memperoleh jangkauan luas monitor tidak dapat memperoleh sumber daya monitor (CRTC, keluaran, modus) tidak dapat menyetel kondigurasi untuk CRTC %d konfigurasi yang saat ini aktif sama sekali tidak cocok dengan konfigurasi yang telah disimpan modus yang dipilih tidak cocok dengan modus yang dimungkinkan:
%s keluaran %s tidak memiliki parameter yang sama seperti keluaran hasil duplikasi:
modus yang ada = %d, modus yang baru = %d
koordinat yang ada = (%d, %d), koordinat yang baru = (%d, %d)
rotasi yang ada = %s, rotasi yang baru = %s keluaran %s tidak mendukung modus %dx%d@%dHz posisi/ukuran CRTC %d yang dipinta lebih besar dari batas yang diperbolehkan: posisi=(%d, %d), ukuran=(%d, %d), maksimal=(%d, %d) ukuran virtual yang diperlukan tidak sesuai dengan ukuran yang tersedia: dipinta=(%d, %d), minimal=(%d, %d), maksimal=(%d, %d) galat pada X sewaktu memperoleh jangkauan luas layar 