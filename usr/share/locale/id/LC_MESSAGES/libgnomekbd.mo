��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j    �  /   �
  
   �
  3   �
     �
          (     �     �  8   �        "      C   C     �     �  >   �  *   �  g        �     �     �     �     �  /   �     -  )   <     f  '   s     �  ,   �  &   �               *     ?     O     _     s     �  	   �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2016-10-11 14:04+0000
PO-Revision-Date: 2017-03-20 05:42+0700
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.12
 Grup bawaan, ditetapkan pada penciptaan jendela Indikator: Menjaga dan mengelola kelompok terpisah per jendela Tata Letak Papan Tik Tata letak papan tik Tata letak papan tik "%s" 
Hak cipta &#169; X.Org Foundation dan kontributor XKeyboardConfig
Untuk lisensi lihat metadata paket Model papan tik Opsi papan tik Muat opsi dan tata letak yang eksotis dan jarang dipakai Muat butir konfigurasi tambahan Menampilkan tata letak papan ketik Simpan/kembalikan indikator bersama-sama dengan kelompok tata letak Kelompok sekunder Tunjukkan flag di applet Tampilkan flag di applet untuk menunjukkan tata letak saat ini Tampilkan nama tata letak, bukan nama grup Tampilkan nama tata letak, bukan nama grup (hanya untuk versi XFree yang mendukung tata letak berganda) Pratilik Papan Tik, ofset X Pratilik Papan Tik, ofset Y Pratilik Papan Tik, tinggi Pratilik Papan Tik, lebar Warna latar Warna latar belakang untuk indikator tata letak Keluarga fonta Keluarga fonta untuk indikator tata letak Ukuran fonta Ukuran fonta untuk indikator tata letak Warna latar depan Warna latar depan untuk indikator tata letak Ada galat saat memuat suatu gambar: %s Tak diketahui Kesalahan inisialisasi XKB tata letak papan tik model papan tik tata letak "%s" model "%s",%s dan%s tidak ada tata letak tidak ada opsi opsi "%s" preferences-desktop-keyboard 