��    %      D  5   l      @  '   A     i  "   �     �  ,   �  .   �  !   !     C  (   b     �  #   �     �     �     �     �       !   $  $   F  <   k  *   �     �     �  '        4  ?   L     �     �  !   �     �  2        9     L  %   k  7   �  (   �  #   �  q    (   �	     �	  $   �	     �	  B   	
  5   L
     �
     �
  '   �
     �
  &   �
        
   =     H     Y     k  (   �  (   �  5   �  %        5  %   Q  0   w     �  0   �     �               9  6   S     �      �  #   �  %   �  *   
  $   5        #         	           $                 
                   %   !   "                                                                                                             All configured plugin paths are invalid Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Colon-separated list of Grilo plugins to use Colon-separated paths containing Grilo plugins Could not access mock content: %s Could not find mock content %s Could not resolve media for URI “%s” Data not available Failed to initialize plugin from %s Failed to load plugin from %s Grilo Options Invalid URL %s Invalid path %s Invalid plugin file %s Invalid request URI or header: %s Media has no “id”, cannot remove Metadata key “%s” already registered in different format Metadata key “%s” cannot be registered No mock definition found No searchable sources available None of the specified keys are writable Operation was cancelled Plugin configuration does not contain “plugin-id” reference Plugin not found: “%s” Plugin “%s” already exists Plugin “%s” is already loaded Plugin “%s” not available Semicolon-separated paths containing Grilo plugins Show Grilo Options Some keys could not be written Source with id “%s” was not found The entry has been modified since it was downloaded: %s The requested resource was not found: %s “%s” is not a valid plugin file Project-Id-Version: grilo master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/grilo/issues
PO-Revision-Date: 2018-06-23 10:11+0700
Last-Translator: Andika Triwidada <atriwidada@gnome.org>
Language-Team: Indonesian <gnome@i15n.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
 Semua path pengaya yang ditata tak valid Perlu otentikasi: %s Tak bisa menyambung ke server proksi Tak bisa menyambung ke server Daftar yang dipisah titik dua dari pengaya Grilo yang akan dipakai Path yang dipisah titik dua yang memuat pengaya Grilo Tak bisa mengakses isi mock: %s Tak bisa temukan isi mock %s Tak bisa menentukan media bagi URI "%s" Data tak tersedia Gagal menginisialisasi pengaya dari %s Gagal memuat pengaya dari %s Opsi Grilo URL %s tak valid Path %s tak valid Berkas pengaya %s tak valid URI atau header permintaan tak valid: %s Media tak punya "id", tak bisa menghapus Kunci metadata "%s" sudah terdaftar dalam format lain Kunci metadata "%s" tak bisa didaftar Definisi mock tak ditemukan Tak tersedia sumber yang dapat dicari Kunci yang dinyatakan tak ada yang bisa ditulisi Operasi dibatalkan Konfigurasi pengaya tak memuat acuan "plugin-id" Pengaya tak ditemukan: "%s" Pengaya "%s" telah ada Pengaya "%s" telah dimuat Pengaya "%s" tak tersedia Path yang dipisah titik koma yang memuat pengaya Grilo Tampilkan Opsi Grilo Beberapa kunci tak bisa ditulisi Sumber dengan id "%s" tak ditemukan Entri telah berubah sejak diunduh: %s Sumber daya yang diminta tak ditemukan: %s "%s" bukan berkas pengaya yang valid 