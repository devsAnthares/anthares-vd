��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 ~  %     �	     �	     �	  	   �	     �	     �	  3   
     D
     U
     [
  	   v
  ,   �
  6   �
     �
  !   �
       R   (     {     �     �     �     �  !   �     �     �      �       6   4  3   k     �  !   �     �     �     �               0  H   A     �  
   �     �     �     �     �     �  	   �  	                   /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-05-31 14:52+0000
Last-Translator: kevin <kevin.abdullah.m@gmail.com>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Alamat Selalu berikan akses di perlukan izin dari %s Bluetooth Pengaturan Bluetooth Bluetooth dinonaktifkan Bluetooth dinonaktifkan oleh saklar perangkat keras Rambah Berkas... Batal Atur konfigurasi Bluetooth Sambungan Perangkat %s ingin mengakses ke layanan '%s' perangkat %s yang ingin menyanding dengan komputer ini Tidak cocok Kesalahan dalam mencari perangkat izinkan kali ini saja Bila Anda menghapus perangkat, Anda harus menyiapkannya lagi sebelum dipakai lagi. Pengaturan Keyboard Cocok Setelan Mouse Pengaturan Mouse dan Touchpad Tidak Adapter Bluetooth tidak ditemukan OK Dipasangkan konfirmasi penyandingan untuk %s Permintaan pairing bagi %s Mohon konfirmasi PIN '%s' yang sesuai dengan perangkat Mohon masukkan PIN yang di sebutkan pada perangkat. Tolak Hapus '%s' dari daftar perangkat? Hapus Perangkat Kirim File ke Perangkat... Kirim Berkas... Setel Perangkat Baru Sesuaikan Perangkat Baru Pengaturan Suara Perangkat yang dibutuhkan tidak bisa ditelusuri, kesalahan ada pada '%s' Tipe Penampakan Penampakan dari "%s" Ya menyambungkan... memutus sambungan... perangkat keras tidak tersedia halaman 1 halaman 2 