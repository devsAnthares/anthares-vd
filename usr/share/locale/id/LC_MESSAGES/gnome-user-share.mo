��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     R     i  0   �     �     �     �  N   �  �   F     �  1   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-07 14:28+0700
Last-Translator: Andika Triwidada <andika@gmail.com>
Language-Team: Indonesian <gnome@i15n.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.1
Plural-Forms: nplurals=2; plural=n != 1;
 Berkas publik milik %s Berkas publik milik %s pada %s Luncurkan Berbagi Berkas Pribadi jika diaktifkan Berbagi Berkas Pribadi Berbagi Pakai Pengaturan Berbagi Pakai Nyalakan Berbagi Berkas Pribadi untuk berbagi isi folder ini melalui jaringan. Kapan meminta kata kunci. nilai-nilai yang mungkin adalah "never" (tidak pernah), "on_write" (saat menulis), dan "always" (selalu). Kapan meminta kata kunci berbagi;bersama;berkas;http;jaringan;salin;kirim; 