��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �    �  $   �     �  $   �          1     N  
   f     q     w  -   �     �  !   �  
   �     	  
   	     	  	   =	  "   G	     j	  !   x	     �	     �	  ]   �	      
      ;
     \
  
   j
  )   u
  8   �
     �
                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&keywords=I18N+L10N&component=general
PO-Revision-Date: 2012-09-04 17:33+0700
Last-Translator: Dirgita <dirgitadevina@yahoo.co.id>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Indonesian
X-Poedit-Country: Indonesia
X-Generator: Lokalize 1.2
Plural-Forms: nplurals=1; plural=0;
 - GNOME daemon aksesibilitas tetikus Gaya Tombol Gaya tombol dari jendela jenis klik. Geometri jendela jenis klik Orientasi jendela jenis klik Gaya jendela jenis klik Klik Ganda Seret Mengaktifkan klik-menetap Mengaktifkan klik sekunder yang disimulasikan Gagal Menampilkan Bantuan Sembunyikan jendela berjenis klik Horisontal Klik Bergerak Hanya ikon Abaikan gerakan kecil pointer Orientasi Orientasi dari jendela jenis klik. Klik Sekunder Mengatur modus menetap yang aktif Matikan mousetweaks Klik Tunggal  Ukuran dan posisi jendela jenis klik. Format adalah string geometri starndar Sistem X Window. Mulai mousetweaks sebagai daemon Mulai mousetweaks di modus login Teks dan Ikon Hanya teks Waktu untuk menunggu sebelum klik-menetap Waktu untuk menunggu sebelum klik sekunder disimulasikan Vertikal 