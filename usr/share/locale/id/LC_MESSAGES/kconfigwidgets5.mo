��    v      �  �   |      �	     �	  	   �	     
     
     #
     )
     0
     A
     G
     O
     W
     j
     s
     
     �
  	   �
     �
     �
  
   �
     �
     �
     �
  
   �
     �
     �
        	          
     
   *     5     A     P     V     e     r     x     |  ;   �  -   �  #   �          ,     D  
   a     l     s  
   �     �     �     �     �     �          #     (  	   C      M     n     �     �  
   �     �     �     �     �               .     >     N     b     h  
   �     �  )   �     �     �     �     
          *     0     8     K  '   c     �     �     �     �     �     �     �       C        X  s   h      �     �          )     >     ^     n           �  -   �  /   �       	     !        A      \     }     �     �     �  s  �     3     E     Q     d     s     {     �     �     �     �     �     �     �     �     �     �     �          %     7     @     V     \     p     v     ~  	   �  	   �     �     �  	   �     �     �     �  	   �                         &     2     P     e     q     ~     �     �     �     �     �     �     �          2     H     P     l  -   y     �     �     �     �     �     �          &     ?     V     p     �     �     �     �     �     �          #     9     Q     b     p  	   �     �     �  %   �  ;   �          "     1     Q     ^     o     �     �  U   �       �   %  !   �  %   �  '   �  %        ?     [     i  &   �     �  4   �  8   �     (     7  *   F  #   q  )   �     �     �     �     �     6   I   q                e   %      R   K              a   M      D   <   $   Y           r   5   P                 N   ,   T   #          u       -              (   '          A       o   @   Z   &       n   7   8   	   C      p          G   f      F      [   ;   "   B       O   
      /           ]                 >              c   W   v   i   X   d   j   3   U          h   `   !      m       k   )       9   ?       4   V              E       L   g      :   *   S   t   2           l           1   ^   _   0      =   s                  .       J   Q              \   H             +   b    %1 &Handbook &About %1 &Actual Size &Add Bookmark &Back &Close &Configure %1... &Copy &Delete &Donate &Edit Bookmarks... &Find... &First Page &Fit to Page &Forward &Go To... &Go to Line... &Go to Page... &Last Page &Mail... &Move to Trash &New &Next Page &Open... &Paste &Previous Page &Print... &Quit &Redisplay &Rename... &Replace... &Report Bug... &Save &Save Settings &Spelling... &Undo &Up &Zoom... @action:button Goes to next tip, opposite to previous&Next @action:button Goes to previous tip&Previous @option:check&Show tips on startup @titleDid you know...?
 @title:windowConfigure @title:windowTip of the Day About &KDE C&lear Check spelling in document Clear List Close document Configure &Notifications... Configure S&hortcuts... Configure Tool&bars... Copy selection to clipboard Create new document Cu&t Cut selection to clipboard Dese&lect EMAIL OF TRANSLATORSYour emails Encodings menuAutodetect Encodings menuDefault F&ull Screen Mode Find &Next Find Pre&vious Fit to Page &Height Fit to Page &Width Go back in document Go forward in document Go to first page Go to last page Go to next page Go to previous page Go up NAME OF TRANSLATORSYour names No Entries Open &Recent Open a document which was recently opened Open an existing document Paste clipboard content Print Previe&w Print document Quit application Re&do Re&vert Redisplay document Redo last undone action Revert unsaved changes made to document Save &As... Save document Save document under a new name Select &All Select zoom level Send document by mail Show &Menubar Show &Toolbar Show Menubar<p>Shows the menubar again after it has been hidden</p> Show St&atusbar Show Statusbar<p>Shows the statusbar, which is the bar at the bottom of the window used for status information.</p> Show a print preview of document Show or hide menubar Show or hide statusbar Show or hide toolbar Switch Application &Language... Tip of the &Day Undo last action View document at its actual size What's &This? You are not allowed to save the configuration You will be asked to authenticate before saving Zoom &In Zoom &Out Zoom to fit page height in window Zoom to fit page in window Zoom to fit page width in window go back&Back go forward&Forward home page&Home show help&Help Project-Id-Version: kdelibs4
Report-Msgid-Bugs-To: http://bugs.kde.org
PO-Revision-Date: 2018-12-23 14:53+0700
Last-Translator: Wantoyo <wantoyek@gmail.com>
Language-Team: Indonesian <kde-i18n-doc@kde.org>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 2.0
 &Buku Petunjuk %1 &Tentang %1 &Ukuran Sebenarnya Tambah &Markah &Mundur &Tutup &Konfigurasi %1... &Salin &Hapus &Donasi &Edit Markah... &Cari... Halaman Pertama &Paskan ke Halaman M&aju Menu&ju Ke... Menu&ju ke Baris... Menu&ju ke Halaman... H&alaman Terakhir &Mail... &Pindah ke Sesampahan &Baru Ha&laman Berikutnya &Buka &Tempel Hala&man Sebelumnya &Cetak... &Berhenti Tayangkan &Ulang &Ubah nama... &Ganti... &Laporkan Bug... &Simpan &Simpan Setelan &Ejaan... &Takjadi &Atas &Zoom... &Berikutnya &Sebelumnya &Tampilkan tip saat pemulaian Apakah Anda tau...?
 Konfigurasi Tip Hari Ini Tentang &KDE Ha&pus Pemeriksa ejaan di dokumen Hapus Daftar Tutup dokumen Konfigurasi &Notifikasi... Konfigurasi &Pintasan... Konfigurasi &Bilah Alat... Salin pilihan ke clipboard Ciptakan dokumen baru Po&tong Potong pilihan ke clipboard Tidak P&ilih andhika.padmawan@gmail.com,wantoyek@gmail.com Deteksi Otomatis Default Mode Layar Pen&uh Cari Beri&kutnya Cari Se&belumnya Paskan ke &Tinggi Halaman Paskan ke Lebar Halaman Menuju mundur di dokumen Menuju maju di dokumen Menuju ke halaman pertama Menuju ke halaman terakhir Menuju ke halaman berikutnya Menuju ke halaman sebelumnya Menuju atas Andhika Padmawan,Wantoyo Tak Ada Lema Buka &Terkini Buka dokumen yang baru dibuka Buka dokumen yang ada Tempel konten clipboard Pratinj&au Cetak Cetak dokumen Berhenti aplikasi &Jadilagi Ke&mbali Tayangkan ulang dokumen Jadilagikan tindakan takjadi terakhir Kembalikan perubahan tidak tersimpan yang dibuat ke dokumen Simpan Se&bagai... Simpan dokumen Simpan dokumen dengan nama baru Pilih &Semua Pilih level zoom Kirim dokumen melalui surat Tampilkan &Bilah Menu Tampilkan &Bilah Alat Tampilkan Bilah Menu<p>Tampilkan bilah menu lagi setelah bilah menu disembunyikan</p> Tampilkan Bilah &Status Tampilkan Bilah Status<p>Tampilkan bilah status, yaitu bilah yang berada di bawah window yang digunakan untuk informasi status.</p> Tampilkan pratinjau cetak dokumen Tampilkan atau sembunyikan bilah menu Tampilkan atau sembunyikan bilah status Tampilkan atau sembunyikan bilah alat Alihkan &Bahasa Aplikasi... Tip Hari &Ini Takjadikan tindakan terakhir Tampilkan dokumen di ukuran sebenarnya &Apakah Ini? Anda tidak diperbolehkan untuk menyimpan konfigurasi Anda akan diminta untuk mengotentikasi sebelum menyimpan Zoom Per&besar Zoom Perke&cil Zoom ke pas tinggi halaman di dalam window Zoom ke pas halaman di dalam window Zoom ke pas lebar halaman di dalam window &Mundur M&aju &Beranda Ba&ntuan 