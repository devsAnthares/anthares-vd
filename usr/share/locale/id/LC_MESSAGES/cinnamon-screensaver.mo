��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  ,  r  *   �     �  A   �        .   1     `  .   |  #   �     �  ?   �  ,        K     a     }  &   �  
   �     �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-05-05 13:21+0000
Last-Translator: Ady Suwarjono <amdersz5g7@gmail.com>
Language-Team: GNOME Indonesian Translation Team <gnome@i15n.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: id
 Menyebabkan screensaver keluar dengan baik Sedang memeriksa... Bila screensaver aktif maka nonaktifkan (layar tidak dikosongkan) Kata sandi salah Pesan untuk ditampilkan di layar yang terkunci Masukkan kata sandi anda... Menanyakan berapa lama screensaver telah aktif Menanyakan keadaan dari screensaver Ganti Pengguna Meminta proses screensaver untuk mengunci layar dengan seketika Pengaman layar telah aktif selama %d detik.
 Pengaman layar aktif
 Pengaman layar tidak aktif
 Pengaman layar saat ini aktif.
 Aktifkan screensaver (kosongkan layar) Buka Kunci Versi dari aplikasi ini Tombol Caps Lock menyala. 