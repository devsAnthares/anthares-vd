��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �     �     �       3     A   R     �  )   �  +   �  %   �  3     #   J     n     �     �  "   �     �     �     �     �          +     7     D      K  !   l     �  #   �     �  /   �     �  M        V     l  #   ~  	   �  	   �     �     �     �     �  +        .     <      Y  )   z     �  
   �     �     �     �  #   
  =   .  0   l  Q   �     �  %   �     !     A  b   T  b   �  !     )   <     f  
   m     x     �     �     �     =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-15 14:18+0000
Last-Translator: dodolzk <Unknown>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
X-Poedit-Country: Indonesia
Language: id
X-Poedit-Language: Indonesian
  - Pengaturan sesi Cinnamon Sebuah program masih berjalan: DIR_MULAI_OTOMATIS Aplikasi tidak menerima dokumen pada baris perintah Tidak dapat melewatkan URI dokumen pada entri desktop 'Type=Link' Batal Tidak dapat terhubung dengan manajer sesi Tidak dapat membuat soket pendengar ICE: %s Menonaktifkan koneksi ke manajer sesi Tidak memuat aplikasi yang ditentukan oleh pengguna Jangan tanyakan konfirmasi pengguna Mengaktifkan kode debug Keluar dengan kode %d BERKAS Ini bukan berkas .desktop yang sah Tetap Hibernasi ID Tidak menemukan ikon '%s' Mengabaikan sembarang pencegah Dimatikan oleh sinyal %d Kunci Layar Tetap Keluar Keluar Keluar dari sistem ini sekarang? Bukan objek yang dapat dieksekusi Tidak merespon Menimpa direktori standar autostart Matikan Program dipanggil dengan opsi yang bertentangan Nyalakan Ulang Menolak sambungan dari klien baru, karena sesi sedang dalam proses dimatikan
 Aplikasi Yang Diingat Tetap Start Ulang Nyalakan ulang sistem ini sekarang? NAMA_SESI S_uspensi Sesi Opsi manajemen sesi: Sesi yang dipakai Menampilkan opsi manajemen sesi Tampilkan dialog paus gagal untul pengujian Tetap Matikan Matikan sistem ini sekarang? Beberapa program masih berjalan: Menentukan berkas yang memuat konfigurasi Menentukan ID manajemen sesi Memulai %s Dihentikan oleh sinyal %d Tetap Suspensi Tetap Ganti Pengguna Program ini menghalangi log keluar. Sistem ini akan dimulai ulang secara otomatis dalam %d detik. Sistem akan mati secara otomatis dalam %d detik. Tidak dapat menjalankan sesi log masuk (serta tidak dapat menyambung ke server X) Tak dikenal Berkas desktop Versi '%s' tak dikenal Opsi peluncuran tak dikenal: %d Versi aplikasi ini Menunggu program selesai. Menginterupsi program dapat menyebabkan Anda kehilangan hasil pekerjaan. Menunggu program selesai. Menginterupsi program dapat menyebabkan Anda kehilangan hasil pekerjaan. Anda saat ini masuk sebagai "%s". Anda akan keluar otomatis dalam %d detik. _Batal _Hibernasi Ke_luar H_idupkan Ulang _Matikan _Ganti Pengguna 