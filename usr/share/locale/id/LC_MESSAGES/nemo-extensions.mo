��    5      �  G   l      �     �     �     �     �     �     �     �     �     �     �     �  :        <     J     Q     Y     _     h     p     ~  4   �  %   �     �  
   �                    $     ,     9     G  .   d     �     �     �     �     �  %   �  .   �  "   +     N     i     �     �     �     �     �     �     �     �            �       �	     �	     �	     �	     �	     �	     �	     �	     �	     
     '
  C   ;
     
     �
     �
     �
     �
  
   �
     �
  !   �
  E   �
  #   -     Q  
   e     p          �     �     �     �     �  4   �               9     A  
   I  3   T  )   �  &   �  $   �     �       !   ,     N     g     t     �     �     �     �  �   �                          $                   &   3              +   1                                !   2   .   (   5   4                                )      #                  
         /      ,         "   *       '       0      -   %      	           .resized 1024x768 128x128 32x32 48x48 640x480 800x600 96x96 <b>Image Size</b> <i>Resizing "%s"</i> <i>Rotating "%s"</i> A GTK+ utility for computing message digests or checksums. Album Artist: Album: Artist: Audio Bitrate: Compare Compare Later Compare selected files Compare selected files to the file remembered before Compare to the file remembered before Compare to:  Copyright: Custom size: DIGEST Digests Emblems Extract Here Extract To... Extract the selected archive Failed to enable any supported hash functions. Failed to read "%s" Failed to read "%s":
%s Genre: Length: No Info Please enter a valid filename suffix! Read program data from the specified directory Remember file for later comparison Resize each selected image Resizing image: %d of %d Ro_tate Images... Rotate each selected image Rotating image: %d of %d Select Files Select a size: Title: Track #: Unknown error Year: translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-06-16 04:30+0000
Last-Translator: jemmy surya <j4m13s@yahoo.com>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2018-06-26 10:18+0000
X-Generator: Launchpad (build 18688)
 .ubah ukuran 1024x768 128x128 32x32 48x48 640x480 800x600 96x96 <b>Ukuran Gambar</b> <i>Ubah ukuran "%s"</i> <i>Memutar "%s"</i> Sebuah utilitas GTK+ untuk menghitung pesan digests atau checksums. Artis Album: Album: Artis: Audio Bitrate: Bandingkan Bandingkan Nanti Bandingkan file-file yang dipilih Bandingkan file-file yang dipilih dengan file yang diingat sebelumnya Bandingkan dengan berkas sebelumnya Bandingkan dengan:  Hak Cipta: Ukuran custom: DIGEST Digests Emblem Ekstrak di sini Ekstrak Ke... Ekstrak arsip yang dipilih Gagal untuk memfungsikan semua dukungan fungsi hash. Gagal dalam membaca "%s" Gagal dalam membaca "%s":
%s Aliran: Durasi: Tanpa Info Mohon masukan sebuah berkas name suffix yang valid! Baca data program dari direktori spesifik Ingat file untuk perbandingan kemudian Perkecil setiap gambar yang terpilih Ubah ukuran gambar: %d of %d Pu_tar gambar... Putar setiap gambar yang terpilih Memutar gambar: %d of %d Pilih Berkas Pilih sebuah ukuran: Judul Track #: Kesalahan tak dikenal Tahun: Launchpad Contributions:
  Ady Suwarjono https://launchpad.net/~amdersz5g7
  Aji Aini Hanif https://launchpad.net/~adjie
  jemmy surya https://launchpad.net/~jemmy 