��          �      |      �     �       "   -     P  !   m     �  $   �     �     �               !     1  !   H     j     �     �     �     �     �  !   �  �    1   
  <   <  U   y  E   �  U     U   k  \   �  )     :   H  %   �  "   �     �  /   �  Y   	  O   v	  3   �	  H   �	  2   C
  3   v
  G   �
  "   �
                             
                                            	                        '%s' is not a valid plugin file Authentication required: %s Cannot connect to the proxy server Cannot connect to the server Could not access mock content: %s Could not find mock content %s Could not resolve media for URI '%s' Data not available Failed to load plugin from %s Grilo Options Invalid URL %s Invalid path %s Invalid plugin file %s Invalid request URI or header: %s No mock definition found Operation was cancelled Plugin '%s' already exists Plugin '%s' not available Plugin not found: '%s' Show Grilo Options Source with id '%s' was not found Project-Id-Version: Gnome Nepali Translation Project
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=grilo&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-08-16 14:21+0545
Language-Team: Nepali Translation Team <chautari@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
Last-Translator: Pawan Chitrakar <chautari@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: ne
 '%s' अवैध प्लगिन फाइल %s प्रमाणिकरण आवश्यक छ । प्रोक्सी सर्भर जडान गर्न सक्दैन सर्भरमा जडान गर्न सकिँदैन नक्कली सामग्री पहुँच गर्न सकेन:%s नक्कली सामग्री %s फेला पार्न सकेन URI '%s' को लागि मिडिया समाधान गर्न सकेन डाटा उपलब्ध छैन प्लगइन लोड गर्न असफल: %s ग्रिलो विकल्प अवैध यूआरएल %s अवैध मार्ग %s अवैध प्लगइन फाइल %s URI वा हेडरको अवैध अनुरोध URI वा हेडर: %s कुनै नक्कली परिभाषा फेला परेन सञ्चालन रद्द भएको छ प्लगिन '%s' पहिले नै अवस्थित छ प्लगइन %s उपलब्ध छैन प्लगिन फेला परेन : '%s' ग्रिलो विकल्प देखाउनुहोस् स्रोत %s पाइएन 