��    $      <  5   \      0     1     D     U  	   f     p     �     �     �     �          !  )   -     W  .   d     �     �  	   �     �     �     �     �     �  
   �     �               *     <     N     e  +   t  O   �  !   �            �  "  .   �  .      (   /     X  4   x  `   �  `   	  `   o	  Z   �	  Z   +
  $   �
  P   �
     �
  �     D   �  *   �  ,        3  ,   C     p     �  .   �  /   �  #   �  (     ^   <  g   �  M     J   Q  0   �  X   �  �   &  H   �       (   )                          "                     $           	                                                           
   #                                                        !    %i month %i months %i week %i weeks %i year %i years All files Available Profiles Available Profiles for Cameras Available Profiles for Displays Available Profiles for Printers Available Profiles for Scanners Available Profiles for Webcams Calibration Cannot remove automatically added profile Colorspace:  Create a color profile for the selected device Create virtual device Default Default:  Device Disabled Help Layout Less than 1 week No profile Not connected Other profile… Select ICC Profile File Set for all users Show help options Supported ICC profiles Test profile:  The device type is not currently supported. This device does not have a profile suitable for whole-screen color correction. This device is not color managed. Unknown _Import Project-Id-Version: cinnamon-control-center.cinnamon-2-20.ne
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-03-10 08:29+0000
Last-Translator: Bista Nirooj <nirooj56@gmail.com>
Language-Team: Nepali <info@mpp.org.np>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:12+0000
X-Generator: Launchpad (build 18688)
 %i महिना %i महिनाहरु %i हप्ता %i हप्ताहरु %i वर्ष %i वर्षहरु सबै फाइलहरू उपलब्ध प्रोफाइलहरु क्यामेराका लागि उपलब्ध प्रोफाइलहरु प्रदर्शनका लागि उपलब्ध प्रोफाइलहरु प्रिन्टरका लागि उपलब्ध प्रोफाइलहरु स्कैनरका लागि उपलब्ध प्रोफाइलहरु वेबकैमका लागि उपलब्ध प्रोफाइलहरु क्यालिब्रेसन स्वतः प्रोफाइल हटाउन सकिँदैन ! रङस्थान:  चयन यन्त्र लागि एक रङ प्रोफाइल सिर्जनागर्नु होस ! अवास्तविक यन्त्र सिर्जना पूर्वनिर्धारित पूर्वनिर्धारित:  उपकरण असक्षम पारिएको छ मद्दत सजावट 1 हप्ता भन्दा कममा कुनै प्रोफाइल छैन जडान भएको छैन अन्य प्रोफाइल... आईसीसी प्रोफाइल फाइल चयन गर्नुहोस् सबै प्रयोगकर्ताहरू लागि सेट गर्नुहोस् मद्दत विकल्पहरू देखाउनुहोस् समर्थित आईसीसी प्रोफाइलहरु परीक्षण प्रोफाइल:  यन्त्रको प्रकार हाल समर्थित छैन। यो यन्त्रको सारा-पर्दा रंग सुधार को लागि उपयुक्त प्रोफाइल छैन। यो यन्त्र रङ व्यवस्थित छैन ! अज्ञात आयात गर्नुहोस् 