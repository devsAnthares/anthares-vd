��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  2   m  =   �  w   �  A   V     �  (   �  �   �  �   �  O   G  a   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: Gnome Nepali Translation Project
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-07-25 12:38+0545
Last-Translator: Pawan Chitrakar <chautari@gmail.com>
Language-Team: Nepali Translation Team <chautari@gmail.com>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
Plural-Forms: nplurals=2;plural=(n!=1);
 %s को सार्वजनिक फाइल %s मा %s' को सार्वजनिक फाइल व्यक्तिगत फाइल साझेदारी सक्षम भए चलाउनुहोस् व्यक्तिगत फाइल साझेदारी साझेदारी साझेदारी सेटिङ नेटवर्कमा फाईल बाढ्न व्यक्तिगत फाइल साझेदारी सक्षम गर्नुहोस् पासवर्ड कति बेला माग्ने, संभाव्य मानहरू "कहिल्यै", "लेख्नेबेला", र "सधैं" छन् । पासवर्ड कति बेला आवश्यक पर्ला साझा;फाईल;http;नेटवर्क;प्रतिलिपि;पठाउन; 