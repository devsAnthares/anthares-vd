��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  �        �  �   �  '   d  W   �  Q   �  �   6  o   �  S   -	  �   �	  �   5
  ;   $  B   `  K   �  �   �  .   t  "   �  f   �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.HEAD.ne
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-24 17:15+0000
Last-Translator: Siddharth Belbase <Unknown>
Language-Team: Nepali <info@mpp.org.np>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 स्क्रिनसेभरको कारणले गर्दा उचित रुपले बाहिरियो जाँचदै... स्क्रिनसेभर सक्रिय भएकोभए यसलाई निस्क्रिय गर्नुहोस (खाली नछाड्नुहोस्) पासवर्ड गलत छ । स्क्रिनमा देखाउन खोजिएको सन्देश कृपया हजुरको पासवर्ड भन्नुहोस स्क्रिनसेभर सक्रिय भएको समयअवधि क्वेरी गर्नुहोस् स्क्रिनसेभरको स्थितिलाई क्वेरी गर्नुहोस प्रयोगकर्ता परिवर्तन गर्नुहोस चलिरहेको स्क्रिनसेभरलाई पर्दामा तत्काल ताल्चा लगाउन सुझाव दिन्छ । स्क्रीनसेवर %d सेकेन्डका लागि सक्रिय रहेको छ
 स्क्रीनसेवर %d सेकेन्डका लागि सक्रिय रहेको छ !
 स्क्रीनसेवर सक्रिय छ !
 स्क्रीनसेवर निष्क्रिय छ
 स्क्रीनसेवर हाल सक्रिय छैन !
 स्क्रिनसेभर खोल्नुहोस् (स्क्रिन खाली राख्नुहोस्) ताल्चा खोल्नुहोस यसको संस्करण तपाईँले Caps Lock कुञ्जी खुला राख्नुभएको छ । 