��    K      t  e   �      `     a  .     *   �  1   �        ,   ,  o   Y     �  i   �     ;  Q   D     �     �     �     �  2   �     	     0	     8	     D	     c	     p	  
   |	  "   �	     �	     �	  /   �	     �	     
     $
  2   9
     l
  
   �
  (   �
      �
     �
  
   �
  
   �
  x   	     �     �     �     �      �  -   �  -     3   0  &   d  )   �     �  J   �        %   @  #   f  �   �  F     #   V  '   z  	   �     �     �     �  F   �  9        V     w     �     �     �     �     �     �     �     �  �  �  S   ~  �   �  k   U  w   �  \   9  ~   �  �        �            �   )  J   �  >   5  8   t  (   �  w   �  s   N     �  )   �  M     +   P  G   |  !   �     �  4   f     �  Z   �  S     /   c  l   �  �      ,   �     �  �   �  �   �  F     4   \  8   �  P  �     !  '   4!     \!     |!  J   �!  z   �!  t   P"  �   �"  b   I#  L   �#  F   �#  �   @$  I   %  U   W%  L   �%  �   �%  �   �&  i   �'     (  ;   �(  1   �(     �(  %   )  �   ()  �   �)    R*     j-     s-     �-     �-  H   �-     .     .     .  -   2.                         .       "   5   (      J   H   *   D   ;              G                        !   )                 =   <       A   /   $   6   1             '   3       @       -   F            ,   :   2           +         8   ?                     C       B       	   
   7      9      &             %       I          E   4                  #   >   K      0       

Add connect to server mount 

Browse the file system with the file manager --check cannot be used with other options. --geometry cannot be used with more than one URI. --quit cannot be used with URIs. <big><b>Error autorunning software</b></big> <big><b>This medium contains software intended to be automatically started. Would you like to run it?</b></big> Actions Before running Nemo, please create the following folder, or set permissions such that Nemo can create it. C_onnect Can't load the supported server method list.
Please check your gvfs installation. Cannot find the autorun program Connect to Server Connecting... Continue Create the initial window with the given geometry. Delete all items in the Trash Desktop Disable all Don't show this message again. E_mpty Trash Empty Trash Enable all Error starting autorun program: %s FTP (with login) GEOMETRY Nemo could not create the required folder "%s". Nemo's main menu is now hidden No actions found No bookmarks defined Only create windows for explicitly specified URIs. Operation cancelled Pass_word: Perform a quick set of self-check tests. Please verify your user details. Print but do not open the URI Public FTP Quit Nemo. Repair the user thumbnail cache - this can be useful if you're having trouble with file thumbnails.  Must be run as root SSH Secure WebDAV (HTTPS) Server Details Sh_are: Show the version of the program. Sorry, could not change the group of "%s": %s Sorry, could not change the owner of "%s": %s Sorry, could not change the permissions of "%s": %s The desktop view encountered an error. The folder "%s" cannot be opened on "%s". The group could not be changed. The name "%s" is already used in this folder. Please use a different name. The owner could not be changed. The permissions could not be changed. The server at "%s" cannot be found. The software will run directly from the medium "%s". You should never run software that you don't trust.

If in doubt, press Cancel. There is no "%s" in this folder. Perhaps it was just moved or deleted? There was an error displaying help. There was an error displaying help: 
%s Try Again User Details WebDAV (HTTP) Windows share You do not have the permissions necessary to change the group of "%s". You do not have the permissions necessary to rename "%s". You have chosen to hide the main menu.  You can get it back temporarily by:

- Tapping the <Alt> key
- Right-clicking an empty region of the main toolbar
- Right-clicking an empty region of the status bar.

You can restore it permanently by selecting this option again from the View menu. [URI...] _Domain name: _Folder: _Port: _Remember this password _Run _Server: _Type: _User name: Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-05-23 16:09+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Nepali <ne@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:11+0000
X-Generator: Launchpad (build 18688)
 

सर्भर माउन्टमा जडान थप्नुहोस् 

फाइल प्रबन्धकसँग फाइल प्रणाली ब्राउज गर्नुहोस् -check अरु विकल्पहरुसँग प्रयोग गर्न सकिदैन। --geometry एक भन्दा बदी URIहरुसँग प्रयोग गर्न सकिदैन। --quit अरु URIहरुसँग प्रयोग गर्न सकिदैन। <big><b>प्रोग्राम स्वचालित गर्दा त्रुटि देखियो  </b></big> <big><b>यस माध्यममा स्वत: चल्ने सफ्टवेर पाइयो। के तपाईं त्यस्लाई चलाऊन चाहनुहुन्छ? </b></big> कार्यहरु निमो चलाउनु भन्दा अगाडी, कृपया उल्लेखित फोल्डर बनाउनुहोस्, वा निमोलाई आफै फोल्डर बनाउने अनुमति दिनुहोस् | C_onnect समर्थित सर्भर विधि सूची लोड गर्न सक्दैन
आफ्नो gvfs स्थापना जाँच गर्नुहोस्। स्वचालित प्रोग्राम भेटिएन। सर्भरमा जडान गर्नुहोस् जडान स्थापना गरिदैछ... जारी राख्नुहोस दिइएको ज्यामितिय अनुसार सुरुको विन्डो बनाउ। रद्दीटोकरीमा भएका सबै वस्तुहरू मेट्नुहोस् डेस्कटप सबै निस्कृय पार यो सन्देश फेरि नदेखाउनुहोस् ! खाली रद्दीटोकरी रद्दीटोकरी खाली गर्नुहोस् सबै सकृय  पार स्वचालित प्रोग्राम त्रुटिका कारण चलाऊन सकिएन : %s एफटीपी (FTP) (लगइन भएको) रेखागणित निमो चाहिएको  "%s" फोल्डर बनाउन सकेन | नेमोको मुख्य सुची अहिले गुप्त छ कुनै कार्य भेतिएन कुनै पुस्तकचिनोहरू परिभाषित गरिएको छैन ! राम्ररी निश्चित गरिएको URIका लागि मात्र विण्डो निर्माण गर्नुस्। सञ्चालन रद्द भयो पासवर्ड: आफै-जाँच गर्ने परीक्षणहरूको सेट गर्ने सम्पादन गर्नुहोस्। कृपया तपाईंको प्रयोगकर्ता विवरण प्रमाणित गर्नु होस् print गर्नुस तर URI नखोल्नु होला सार्वजनिक एफटीपी (FTP) निमो बन्द गर्नुहोस्। प्रयोगकर्ता थम्बनेलक्यास मरम्मत गर्नुस -तपाईंलाई फाइल थम्बनेल समस्या भइरहेको छ भने यो उपयोगी हुन सक्छ . मूल(root) रूपमा चलाउन पर्छ एसएसएच (SSH) सुरक्षित WebDAV (HTTPS) सर्भर विवरण सेयर: प्रोग्रामको संस्करण देखाउ। माफ गर्नुहोस्,  "%s"को समूह परिवर्तन गर्न सकिएन  :%s माफ गर्नुहोस्,"%s" मालिक परिवर्तन गर्न सकिएन :%s माफ गर्नुहोस्,"%s"को अनुमतिहरू परिवर्तन गर्न सकेन: %s डेस्कटप दृश्यले एउटा त्रुटि भेट्यो । फोल्डर "%s" "%s" मा खोल्न सकिँदैन। समूह परिवर्तन गर्न सकिएन । यस फोल्डरमा "%s" नाम पहिल्यै प्रयोग भइसकेको छ । कृपया फरक नाम प्रयोग गर्नुहोस् । मालिक परिवर्तन गर्न सकिएन । अनुमतिहरू परिवर्तन गर्न सकिएन । "%s" मा सर्भर फेला पार्न सकिएन । सफ्टवेयर सीधा  "%s" मध्यम बाट चल्ने छ |  तपाईंलाई सफ्टवेयरमा  भरोसा छैन भने कहिले पनी चलाउनु हुदैन यस फोल्डरमा "%s" छैन। सायद यो भर्खरै सारिएको वा मेटाईएको हुनसक्दछ। मद्दत प्रदर्शन गर्दा त्रुटि भएको थियो । मद्दत प्रदर्शन गर्ने क्रममा त्रुटि खेला पर्यो:
%s फेरि प्रयास गर्नुहोस् प्रयोगकर्ता विवरण WebDAV (HTTP) सञ्झ्याल सेयर "%s" को समूह परिवर्तन गर्न तपाईँसँग आवश्यक अनुमतिहरू छैनन् । "%s" पुन: नामाकरण गर्न तपाईँसँग आवश्यक अनुमतिहरू छैनन् । तपाईं मुख्य मेनु लुकाउन रोज्नु भएको छ. तपाईं यसरी अस्थायी रूपमा फिर्ता प्राप्त गर्न सक्नु हुन्छ: 
-<alt> की हानेर 
-मुख्य उपकरणपट्टी को खाली क्षेत्र दायाँ-क्लिक गरेर 
-status bar पट्टी को खाली क्षेत्र दायाँ-क्लिक गरेर 

तपाईं दृश्य मेनु बाट फेरि यो विकल्प चयन गरेर स्थायी रूपमा यसलाई पुनर्स्थापित गर्न सक्नुहुन्छ। [URI...] डोमेन नाम: _फाइलपत्र: _पोर्ट: यो पासवर्ड स्मरण गर्नुहोस् _रन सर्भर: _प्रकार: _प्रयोगकर्ता नाम: 