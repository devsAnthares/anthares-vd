��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �  u  X     �     �     �     �     �     �               ,     8  
   G     R  W   ^  ]   �  �   	  i   �	  1   
  H   :
  l   �
  �   �
  �  �  P     �   k                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: Nepali Translation Gnome  3.26
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2017-07-25 14:21+0000
PO-Revision-Date: 2017-09-26 11:57+0545
Last-Translator: Pawan Chitrakar <chautari@gmail.com>
Language-Team: Nepali Translation Team <chautari@gmail.com>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
First-Translator: Suman Raj Tiwari <tiwarisuman@hotmail.com>
X-Generator: Poedit 2.0.4
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
 %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l∶%M %p %l:%M:%S %p सिआर्टिसि %d आउटपुट %s चलाउन सक्दैन सिआर्टिसि %d रोटेसन् %d समर्थन गर्दैन सिआर्टिसि %d %dx%d@%dHz (pass %d) मा आउटपुट दिदै %dx%d@%dHz कोसिस् गरिदै
 सिआर्टिसि %d
का लागी मोडहरु कोसिस् गरिदै
 निर्दिष्ट नगरिएको आउटपुट %s मा क्लोन हुनसक्दैन सिआर्टिसिहरु %s
आउटपुटहरुमा खटाउन सकियेन कुनै पनि चयन गरियेका मोडहरु संभावित %s मोडहरु संग मिलेनन्: निर्गत %s सँग एक प्यारामिटरहरू अर्को क्लोन आउटपुटको रूपमा छैन:
अवस्थित मोड =%d, नयाँ मोड =%d
अवस्थित समकक्ष = (%d,%d), नयाँ समकक्ष = (%d,%d)
अवस्थित रोटेशन =%d, नयाँ रोटेशन =%d आउटपुट %s मोड %dx%d@%dHz समर्थन गर्दैन आवश्यक भर्चुअल आकार उपलब्ध आकार संग  फिट गर्दैन: अनुरोधित =(%d, %d), न्यूनतम =(%d, %d), अधिकतम =(%d, %d) 