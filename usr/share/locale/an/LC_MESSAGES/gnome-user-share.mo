��            )   �      �     �     �     �     �  c   �  ,   N  '   {     �  +   �  +   �  %     	   @     J     `     l     t     �     �  (   �  [   �  Q        p  8   �  -   �     �  ,        8     X  2   l  �  �     �     �     �  #   �  �   �  <   }	  <   �	  %   �	  5   
  4   S
  2   �
     �
  "   �
     �
     �
                 -   8  �   f  j   �     V  E   j  .   �     �  3   �  '   *     R  9   f                                  	                      
                                                                                 %s's public files %s's public files on %s Cancel File reception complete If this is true, Bluetooth devices can send files to the user's Downloads directory when logged in. Launch Bluetooth ObexPush sharing if enabled Launch Personal File Sharing if enabled May be shared over the network May be shared over the network or Bluetooth May be used to receive files over Bluetooth May be used to share or receive files Open File Personal File Sharing Preferences Receive Reveal File Sharing Sharing Settings When to accept files sent over Bluetooth When to accept files sent over Bluetooth. Possible values are "always", "bonded" and "ask". When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords Whether Bluetooth clients can send files using ObexPush. Whether to notify about newly received files. You have been sent a file You have been sent a file "%s" via Bluetooth You received "%s" via Bluetooth You received a file share;files;bluetooth;obex;http;network;copy;send; Project-Id-Version: gnome-user-share master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-11-10 10:30+0100
Last-Translator: Daniel Martinez <entaltoaragon@gmail.com>
Language-Team: Aragonés <softaragones@googlegroups.com>
Language: an
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.6
 Fichers publicos de %s Fichers publicos de %s en %s Cancelar A recepción de fichers ye rematada Si isto ye cierto, os dispositivos Bluetooth pueden ninviar fichers a la carpeta Descargas de l'usuario quan haiga encetau a sesión. Lanzar a compartición por ObexPush Bluetooth si ye activada Lanzar a compartición de fichers personals si son activadas Se puede compartir a traviés d'o ret Se puede compartir a traviés d'o ret u por Bluetooth Se puede fer servir ta recibir fichers por Bluetooth Se puede fer servir ta compartir u recibir fichers Ubrir fichero Compartición de fichers personals Preferencias Recibir Revelar fichero Compartición Opcions de compartición Quán acceptar fichers ninviaus por Bluetooth Quán acceptar fichers ninviaus por Bluetooth. As valors posibles son: "always" (siempre), "bonded" (vinculaus) y "ask" (preguntar). Quan pedir claus. As opcions posibles son: "never" (nunca), "on_write" (en escribir) y "always" (siempre). Quan requerir claus Si os clients Bluetooth pueden ninviar fichers fendo servir ObexPush. Si notificar sobre os nuevos fichers recibius. Has ninviau un fichero Has ninviau un fichero "%s" a traviés de Bluetooth Ha recibiu "%s" a traviés de Bluetooth Recibió un fichero compartir:fichers:bluetooth;obex;http;ret;copiar;ninviar; 