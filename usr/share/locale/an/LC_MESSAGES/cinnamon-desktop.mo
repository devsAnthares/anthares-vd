��             +         �     �     �     �     �                    !     -  $   L  D   q  <   �     �     �          #     B     \  %   v  '   �  )   �  '   �  :     +   Q  I   }  F   �  �     *   �  t     i   |  9   �  n        �	     �	     �	     �	     �	     �	     �	     �	  &   �	       
  K   A
  X   �
     �
     �
  
             /  %   J  /   p  7   �  8   �  6     K   H  1   �  A   �  C     �   L  *     �   J  s   �  P   F             	                                                                                                
                                   %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-29 00:58+0200
Last-Translator: Daniel Martinez <entaltoaragon@gmail.com>
Language-Team: Aragonés <softaragones@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Language: 
Plural-Forms: nplurals=2; plural=(n!=1);
 %A %e de %B, %R %A %e de %B, %R:%S %A %e de %B, %H:%M %A %e de %B, %H:%M:%S %R %R:%S %l:%M %p %l:%M:%S %p o CRTC %d no puede adugar a surtida %s CRTC %d no soporta a rotacion=%s CRTC %d: prebando lo modo %dx%d@%dHz con surtida en %dx%d@%dHz (pasada %d)
 No s'ha puesto trobar una terminal, fendo servir xterm, incluso si puede que no funcione Portatil Pantallas en mirallo Desconoxiu A extension RANDR no ye present Prebando modos ta CRTC %d
 no s'ha puesto clonar a la surtida %s no s'ha puesto asignar CRTCs a las surtidas:
%s No s'ha puesto obtener a informacion arredol de CRTC %d no se puede obtener a informacion arredol d'a surtida %d no s'ha puesto obtener o rango de tamanyos de pantalla no s'han puesto obtener os recursos d'as pantallas (CRTCs, surtidas, modos) no se puede establir a configuracion ta o CRTC %d dengun d'os modos trigaus no ye compatible con os modos posibles: dengun d'os modos triados son compatibles con os modos posibles:
%s a surtida %s no tien os mesmos parametros que atra surtida clonada:
modo existent = %d, modo nueu = %d
coordenadas existentz = (%d, %d), nuevas coordenadas = (%d, %d)
rotacion existent = %s, rotacion nueva = %s A surtida %s no soporta lo modo %dx%d@%dHz a posicion y/o tamanyo que calen ta lo CRTC %d ye difuera d'os límites permitius: posicion=(%d, %d), tamanyo=(%d, %d), maximo=(%d, %d) o tamanyo virtual que cal no s'achusta a lo tamanyo disponible: requeriu:(%d, %d), menimo=(%d, %d), maximo=(%d, %d) error no manellau mientras se prebaba de obtener o rango de tamanyos de pantalla 