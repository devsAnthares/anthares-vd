��          L      |       �   `   �      
     %  )   B     l  &  �  \   �          +  *   I  &   t                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-08-26 12:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Aragonés <softaragones@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 O salvapantallas ha estau activo %d segundo.
 O salvapantallas ha estau activo %d segundos.
 O salvapantallas ye activo
 O salvapantallas ye inactivo
 Actualment o salvapantallas no ye activo.
 Tiene a tecla «Bloq Mayús» pretada. 