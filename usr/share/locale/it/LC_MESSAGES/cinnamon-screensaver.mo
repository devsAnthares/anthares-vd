��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  +        A  @   W     �  &   �     �  ?   �  %   .     T  S   d  e   �          9  )   V  3   �  	   �  "   �     �                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver 2.9x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-31 21:21+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Italiano <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: 
 Causa l'uscita non forzata dal salvaschermo Controllo in corso... Disattiva il salvaschermo se attivo (sbiancamento dello schermo) Password non corretta Messaggio da mostrare nel salvaschermo Inserisci la password... Interrogare l'intervallo di tempo di attività del salvaschermo Interrogare lo stato del salvaschermo Cambiare utente Impone al processo salvaschermo in esecuzione di bloccare immediatamente lo schermo Il salvaschermo è rimasto attivo per %d secondo.
 Il salvaschermo è rimasto attivo per %d secondi.
 Il salvaschermo è attivo
 Il salvaschermo è inattivo
 l salvaschermo non è attivo al momento.
 Attiva il salvaschermo (sbiancamento dello schermo) Sbloccare La versione di questa applicazione Il tasto BlocMaiusc è attivo. 