��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  T   T  5   �  <   �  3        P     l     �  *   �  \   �     +               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-05-09 10:30+0000
Last-Translator: Luca Ferretti <elle.uca@libero.it>
Language-Team: Italian (http://www.transifex.com/projects/p/freedesktop/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 È necessario autenticarsi per cambiare la configurazione della schermata di accesso È necessario autenticarsi per cambiare i dati utente È necessario autenticarsi per cambiare i propri dati utente Cambia la configurazione della schermata di accesso Cambia i propri dati utente Abilita il codice di debug Gestisci gli account utente Stampa le informazioni di versione ed esce Fornisce interfacce D-Bus per interrogare e manipolare
le informazioni degli account utente. Sostituisce l'istanza esistente 