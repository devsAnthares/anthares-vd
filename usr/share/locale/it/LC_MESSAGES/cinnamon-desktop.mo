��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     S  J     �	     �	     �	     �	     �	     �	     �	     
     
  '   
  #   ?
  M   c
  V   �
  	             $  "   0  %   S  $   y  ,   �  -   �  0   �  =   *  G   h  6   �  V   �  R   >  �   �  1   q  �   �  �   .  T   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-06 22:11+0200
Last-Translator: Milo Casagrande <milo@ubuntu.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8-bit
Language: it
Plural-Forms: nplurals=2; plural=(n!=1);
 %A %e %B, %k.%M %A %e %B, %k.%M.%S %A %e %B, %l.%M %P %A %e %B, %l.%M.%S %P %A %e %B %k.%M %k.%M.%S %l.%M %P %l.%M.%S %P il CRTC %d non può gestire l'uscita %s il CRTC %d non supporta rotation=%s CRTC %d: prova modalità %d×%d@%dHz con uscita a %d×%d@%dHz (passaggio %d)
 Impossibile trovare un terminale; usato xterm, anche se questo potrebbe non funzionare Portatile Monitor duplicati Sconosciuto l'estensione RANDR non è presente Prova delle modalità per il CRTC %d
 impossibile duplicare sull'uscita %s impossibile assegnare i CRTC alle uscite:
%s impossibile ottenere informazioni sul CRTC %d impossibile ottenere informazioni sull'uscita %d impossibile ottenere l'intervallo delle dimensioni di schermo impossibile ottenere le risorse di schermo (i CRTC, gli output, i modi) impossibile impostare la configurazione per il CRTC %d nessuna della configurazioni di display salvate corrisponde alla configurazione attiva nessuna delle modalità selezionate era compatibile con le modalità possibili:
%s l'uscita %s non presenta gli stessi parametri dell'altra uscita duplicata:
modalità esistente: %d; nuova modalità: %d
coordinate esistenti: (%d, %d); nuove coordinate: (%d, %d)
rotazione esistente: %s; nuova rotazione: %s l'uscita %s non supporta la modalità %d×%d@%dHz la posizione/dimensione richiesta per il CRTC %d è fuori dal limite consentito: posizione=(%d, %d), dimensione=(%d, %d), massimo=(%d, %d) la dimensione virtuale richiesta non è adatta per la dimensione disponibile: richiesta=(%d, %d), minima=(%d, %d), massima=(%d, %d) errore X non gestito durante il recupero dell'intervallo delle dimensioni di schermo 