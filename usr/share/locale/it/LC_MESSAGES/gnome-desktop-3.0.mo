��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �     �     �     �     �     �     �                     0     C     Y     b  '   n  #   �  M   �  %   	     .	  $   >	  ,   c	  R   �	  �   �	  1   �
  �   �
                                              	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-13 03:09+0000
PO-Revision-Date: 2018-08-27 14:49+0200
Last-Translator: Milo Casagrande <milo@milo.name>
Language-Team: Italiano <tp@lists.linux.it>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.1.1
 %R %R:%S %a %R %a %R:%S %a %-e %b_%R %a %-e %b_%R:%S %a %-e %b_%l:%M %P %a %-e %b_%l:%M:%S %P %a %l:%M %P %a %l:%M:%S %P %a %-e %b_%R %a %-e %b_%R:%S %a %-e %b_%l:%M %P %a %-e %b_%l:%M:%S %P %l:%M %p %l:%M:%S %P il CRTC %d non può gestire l'uscita %s il CRTC %d non supporta rotation=%d CRTC %d: prova modalità %d×%d@%dHz con uscita a %d×%d@%dHz (passaggio %d)
 Prova delle modalità per il CRTC %d
 Non specificato impossibile duplicare sull'uscita %s impossibile assegnare i CRTC alle uscite:
%s nessuna delle modalità selezionate era compatibile con le modalità possibili:
%s l'uscita %s non presenta gli stessi parametri dell'altra uscita duplicata:
modalità esistente: %d; nuova modalità: %d
coordinate esistenti: (%d, %d); nuove coordinate: (%d, %d)
rotazione esistente: %d; nuova rotazione: %d l'uscita %s non supporta la modalità %d×%d@%dHz la dimensione virtuale richiesta non è adatta per la dimensione disponibile: richiesta=(%d, %d), minima=(%d, %d), massima=(%d, %d) 