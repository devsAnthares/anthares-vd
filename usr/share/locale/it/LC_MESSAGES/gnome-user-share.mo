��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     Z     n  4   �     �     �     �  `     Y   g     �  +   �                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-30 11:00+0200
Last-Translator: Gianvito Cavasoli <gianvito@gmx.it>
Language-Team: Italiano <gnome-it-list@gnome.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 File pubblici di %s File pubblici di %s su %s Avvia la condivisione di file personali se abilitata Condivisione di file personali Condivisione Impostazioni di condivisione Attivare Condivisione di file personali per condividere il contenuto di questa cartella in rete. Quando richiedere le password. Le opzioni possibili sono "never", "on_write", e "always". Quando richiedere le password condividere;file;http;rete;copiare;inviare; 