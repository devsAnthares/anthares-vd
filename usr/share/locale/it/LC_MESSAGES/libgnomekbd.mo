��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  ;   |
     �
  0   �
     �
       �   !     �     �  -   �  '   	  2   1  A   d     �     �  D   �  7     y   Q  +   �  )   �      !  "   B     e  7   y     �  =   �     	  ?   %     e  <   ~  7   �     �  !   �     !     7  '   H     p     �     �     �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2017-03-19 15:43+0000
PO-Revision-Date: 2018-03-09 13:31+0100
Last-Translator: Milo Casagrande <milo@milo.name>
Language-Team: Italian <tp@lists.linux.it>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
 Gruppo predefinito, assegnato alla creazione della finestra Indicatore: Mantiene e gestisce gruppi separati per finestra Disposizione tastiera Disposizione tastiera Disposizione di tastiera «%s»
Copyright &#169; X.Org Foundation e collaboratori di XKeyboardConfig
Per la licenza consultare i metadati del pacchetto Modello tastiera Opzioni tastiera Carica disposizioni e opzioni usate raramente Carica elementi di configurazione extra Mostra un'anteprima delle disposizioni di tastiera Salva/Ripristina gli indicatori assieme ai gruppi di disposizioni Gruppo secondario Mostra bandiere nell'applet Mostra le bandiere nell'applet per indicare la disposizione corrente Mostra i nomi di disposizione invece dei nomi di gruppo Mostra i nomi di disposizione invece dei nomi di gruppo (solo per versioni di XFree che supportano disposizioni multiple) L'anteprima di tastiera, offset orizzontale L'anteprima di tastiera, offset verticale L'anteprima di tastiera, altezza L'anteprima di tastiera, larghezza Il colore di sfondo Il colore di sfondo per il selettore della disposizione La famiglia del carattere La famiglia del carattere per l'indicatore della disposizione La dimensione del carattere La dimensione del carattere per l'indicatore della disposizione Il colore di primo piano Il colore di primo piano per l'indicatore della disposizione Si è verificato un errore nel caricare un'immagine: %s Sconosciuta Errore di inizializzazione di XKB Disposizione tastiera Modello tastiera disposizione «%s» disposizioni «%s» modello «%s», %s e %s nessuna disposizione nessuna opzione opzione «%s» opzioni «%s» preferences-desktop-keyboard 