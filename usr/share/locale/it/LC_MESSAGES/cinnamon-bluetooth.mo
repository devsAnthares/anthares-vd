��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 n  %  	   �	     �	  !   �	  	   �	     �	     �	  8   
     P
     `
  #   h
     �
  2   �
  >   �
     
  (        C  W   ^     �     �     �     �       1        B  	   E     O      m  E   �  /   �       +        8     L     j     x  !   �     �  F   �               $     8     <     T     f     {     �                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-27 21:33+0000
Last-Translator: Nio <mint@niomix.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Indirizzo Consenti sempre l'accesso Richiesta di autorizzazione da %s Bluetooth Impostazioni Bluetooth Il Bluetooth è disattivato Il Bluetooth è stato disabilitato dallo switch hardware Esplora file... Annulla Configura le impostazioni Bluetooth Connessione Il dispositivo: %s vuole accedere al servizio '%s' Il dispositivo %s richiede l'accoppiamento con questo computer Non corrisponde Errore nell'esplorazione del dispositivo Consenti solo questa volta Rimuovendo il dispositivo sarà necessario configurarlo nuovamente per poterlo riusare. Impostazioni tastiera Corrisponde Impostazioni del mouse Impostazioni mouse e touchpad No Non è stato rilevato nessun adattatore Bluetooth OK Associato Conferma accoppiamento per %s Richiesta di associazione per %s Prego confermare se il PIN '%s' corrisponde a quello del dispositivo. Prego inserire il PIN indicato sul dispositivo. Rifiuta Rimuovere "%s" dall'elenco dei dispositivi? Rimuovi dispositivo Invia files al dispositivo... Invia file... Configura un nuovo dispositivo Configura un nuovo dispositivo... Impostazioni audio Il dispositivo richiesto non può essere esplorato, causa errore: '%s' Tipo Visibilità Visibilità di "%s" Sì Connessione in corso... disconnessione... hardware disattivato pagina 1 pagina 2 