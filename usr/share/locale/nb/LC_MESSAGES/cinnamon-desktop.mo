��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     A  J     �	     �	     �	     �	  
   �	     �	     �	     �	     �	      �	  !   
  E   2
  G   x
     �
     �
     �
      �
            (   :  '   c  )   �  .   �  8   �  *     F   H  ?   �  �   �  (   �     �     L  @   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop 3.5.x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-09-05 13:02+0200
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 %A %e %B, %R %A %e %B, %R.%S %A %e %B, %H.%M %A %e %B, %H.%M.%S %A, %e. %B %R %R.%S %H.%M %H.%M.%S CRTC %d kan ikke drive utdata %s CRTC %d støtter ikke rotation=%s CRTC %d: prøver modus %dx%d@%dHz med utdata %dx%d@%dHz (forsøk %d)
 Kan ikke finne en terminal. Buker xterm selv om den kanskje ikke virker Bærbar Speilede skjermer Ukjent RANDR-tillegget er ikke tilstede Prøver modus for CRTC %d
 kan ikke klone til utgang %s kunne ikke tilegne CRTCer til utdata:
%s kunne ikke hente informasjon om CRTC %d kunne ikke hente informasjon om utdata %d kunne ikke hente område med skjermstørrelser kunne ikke hente skjermressurser (CRTCer, utdata, modus) kunne ikke sette konfigurasjon for CRTC %d ingen av de lagrede skjermkonfigurasjonene var lik aktiv konfigurasjon ingen av de valgte modusene var kompatibel med mulige modus:
%s utdata %s har ikke samme parametere som en annen klonet utdata:
eksisterende modus = %d, ny modus = %d
eksisterende koordinater = (%d, %d), nye koordinater = (%d, %d)
eksisterende rotasjon = %s, ny rotasjon = %s utdata %s støtter ikke modus %dx%d@%dHz forespurt posisjon/størrelse for CRTC %d er utenfor tillatt område: posisjon=(%d, %d), størrelse=(%d, %d), maksimum=(%d, %d) nødvendig virtuell størrelse passer ikke med tilgjengelig størrelse: forespurt=(%d, %d), minimum=(%d, %d), maksimum=(%d, %d) uhåndtert X-feil under henting av område med skjermstørrelser 