��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 v  %     �	     �	      �	     �	     �	     �	  *   
     =
     S
  %   Z
  
   �
  +   �
  0   �
     �
     �
       V   /     �     �     �  (   �     �  "   �                    5  A   Q  *   �     �  -   �     �     �          '     9     Q  9   g     �  	   �     �     �     �     �     �     �                     /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-09 12:22+0000
Last-Translator: Emil Våge <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adresse Gi alltid tilgang Autorisasjonshendvendelse fra %s Blåtann Innstillinger for blåtann Blåtann er deaktivert Blåtann er deaktivert av maskinvarebryter Bla gjennom filer … Avbryt Konfigurer innstillinger for blåtann Tilkobling Enheten %s vil ha tilgang til tjenesten'%s' Enheten %s vil knytte seg til denne datamaskinen Stemmer ikke overens Feil ved lesing av enhet Tillat bare denne gangen Hvis du fjerner enheten vil du måtte sette den opp på nytt før du bruker den igjen. Innstillinger for tastatur Stemmer Museinnstillinger Innstillinger for mus og berøringsplate Nei Ingen blåtann-adaptere ble funnet OK Sammenkoblet Paring bekreftelse for %s Parringsforespørsel fra %s Vennligst bekreft at PIN-koden '%s' er den samme som på enheten. Vennligst skriv inn PIN-koden på enheten. Avvis Vil du fjerne «%s» fra listen over enheter? Fjern enhet Send filer til enheten... Send filer … Sett opp ny enhet Sett opp en ny enhet... Innstillinger for lyd Den forespurte enheten kan ikke ble lest, feilen er: '%s' Type Synlighet Synlighet for «%s» Ja kobler til... kobler fra... maskinvare deaktivert side 1 side 2 