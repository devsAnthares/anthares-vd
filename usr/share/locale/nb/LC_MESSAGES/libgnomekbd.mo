��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >     �     �     �     
          1  #   I  0   m  %   �  $   �  
   �  -   �     "     @     ^     p  �   �            9   -  &   g     �      �  :   �     �       ;   ,  .   h  j   �  '     '   *  $   R  $   w     �  .   �     �  %   �       (        H  &   W     ~  1   �  -   �     �               1     I     [  #   j     �     �     �  %   �                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd 3.2.x
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-28 16:51+0100
PO-Revision-Date: 2012-01-28 16:52+0100
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8-bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Aktiver flere tillegg Aktiver tille_gg Legg til tillegg Lukk dialogen Konfigurer valgt tillegg Deaktiver valgt tillegg Sett lavere prioritet for tillegget Forvalgt gruppe, tildelt ved oppretting av vindu Aktiver/deaktiver installerte tillegg Sett høyere prioritet for tillegget Indikator: Behold og håndter separate grupper per vindu Tillegg for tastaturindikator Tillegg for tastaturindikator Tastaturutforming Tastaturutforming Tastaturutforming «%s»
Opphavsrett &#169; X.Org Foundation og XKeyboardConfig-bidragsyterene
For lisensdata se pakkens metadata Tastaturmodell Alternativer for tastatur Last eksotiske og lite brukte utforminger og alternativer Last ekstra konfigurasjonsoppføringer Ingen beskrivelse. Forhåndsvis tastaturutforminger Lagre/gjenopprett indikatorer sammen med utformingsgrupper Sekundære grupper Vis flagg i panelprogrammet Vis flagg for å indikere aktiv utforming i panelprogrammet Vis navn på utforming i stedet for gruppenavn Vis navn på utforming i stedet for gruppenavn (kun for versjoner av XFree som støtter flere utforminger) Forhåndsvisning av tastatur, X-avstand Forhåndsvisning av tastatur, Y-avstand Forhåndsvisning av tastatur, høyde Forhåndsvisning av tastatur, bredde Bakgrunnsfarge Bakgrunnsfarge for tastaturutformingsindikator Skriftfamilie Skriftfamilie for utformingsindikator Skriftstørrelse Skriftstørrelse for utformingsindikator Forgrunnsfarge Forgrunnsfarge for utformingsindikator Liste med aktive tillegg Liste med aktiverte tillegg for tastaturindikator Det oppsto en feil under lasting av bilde: %s Kan ikke åpne hjelpfil Ukjent Feil ved initiering av XKB _Tilgjengelige tillegg: tastaturutforming tastaturmodell utforming «%s» utforminger «%s» modell «%s», %s og %s ingen utforming ingen alternativer alternativ «%s» alternativer «%s» 