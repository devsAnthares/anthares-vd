��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  D  w     �     �      �          +     2  Q   K  R   �     �  $   	                      	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share 3.17.x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-08-06 11:06+0200
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s's offentlige filer %s's offentlige filer på %s Start brukerdeling hvis aktivert Deling av personlige filer Deling Innstillinger for deling Slå på personlig fildeling for å dele innholdet i denne mappen via nettverket. Når passord skal kreves. Mulige verdier er «never», «on_write» og «always». Når passord skal kreves del;filer;http;nettverk;kopier;send; 