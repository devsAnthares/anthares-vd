��          L      |       �   `   �      
     %  )   B     l  �  �  W   P     �     �     �     �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver 3.3.x
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-05-06 21:25+0000
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: no
 Skjermsparer har vært aktiv i %d sekund.
 Skjermsparer har vært aktiv i %d sekunder.
 Skjermsparer er aktiv
 Skjermsparer er ikke aktiv
 Skjermsparer er ikke aktiv.
 Du har Caps Lock-tasten på. 