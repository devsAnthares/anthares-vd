��    %      D  5   l      @     A  	   I     S     f  (   |     �     �     �  
   �     �     �  K        T     f     n     }     �     �     �     �     �     �  %   �               "     4     C  
   H     S     j     n     |     �     �     �  n  �          /  7   H  O   �  �   �  4   g     �  J   �     �  '   	  B   5	    x	  =   �
  '   �
  7   �
  l   +     �  i   �  	     -     D   J     �  l   �  +     =   >  P   |  (   �  	   �        %     	   9  !   C  *   e  U   �     �     �           "                    #                  %                                                                        !                                  	   $      
               Address Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Does not match Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing request for %s Reject Remove '%s' from the list of devices? Remove Device Send Files... Set Up New Device Sound Settings Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-10-28 12:39+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 முகவரி ப்ளூடூத் ப்ளூடூத் அமைப்புகள் ப்ளூடூத் முடக்கப்பட்டுள்ளது வன்பொருள் நிலைமாற்றியால் ப்ளூடூத் முடக்கப்பட்டுள்ளது கோப்புகளில் உலாவு... தவிர் ப்ளூடூத் அமைப்புகளை வடிவமை இணைப்பு பொருந்தவில்லை இந்த முறை மட்டும் அனுமதி இந்த சாதனத்தை நீங்கள் நீக்கினால் அடுத்த பயன்பாட்டிற்கு முன் நீங்கள் அதை மீண்டும் வடிவமைக்க வேண்டும். விசைப்பலகை அமைப்புகள் பொருந்துகிறது சொடுக்கி அமைப்புகள் சொடுக்கி மற்றும் தொடுதிட்டு அமைப்புகள் இல்லை ப்ளூடூத் தகைவிகள் ஏதும் காணப்படவில்லை சரி பிணைக்கப்பட்டது %s-க்கு இணைப்புக் கோரிக்கை நிராகரி சாதனங்களின் பட்டியலிருந்து '%s' ஐ நீக்கவா? சாதனத்தை நீக்கு கோப்புகளை அனுப்பவும்... புதிய சாதனத்தை வடிவமைக்கவும் ஒலி அமைப்புகள் வகை தெரிவு “%s” இன் தெரிவு ஆம் இணைக்கிறது... துண்டிக்கிறது... வன்பொருள் முடக்கப்பட்டுள்ளது பக்கம் 1 பக்கம் 2 