��    !      $  /   ,      �     �     �            	   /     9     <     B     K     W  $   v  D   �  <   �          $     6     M     l     �  %   �  '   �  )   �  '     :   @  +   {  I   �  F   �  �   8  *     t   1  i   �  9     �  J     �	     �	     
     
  	   5
     ?
     B
     H
     Q
  [   ]
  N   �
  �     �   �     j  @   �     �  7   �  \     W   u  �   �  \   �  p   �  g   V  i   �  g   (  �   �  �   x     N  c   o  �   �  �   �  �   �                      !                 	                                                                                                                 
       %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Mirrored Displays Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master.ta
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-12-09 13:52+0530
Last-Translator: Dr.T.Vasudevan <drtvasudevan@gmail.com>
Language-Team: American English <gnome-tamil-translation@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en_US
Plural-Forms: nplurals=2; plural=(n!=1);\n



X-Generator: Lokalize 1.5
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e %R %R:%S %l:%M %p %l:%M:%S %p CRTC %d, வெளியீடு  %s ஐ இயக்க முடியவில்லை CRTC %d சுழற்சியை ஆதரிக்கவில்லை=%s CRTC %d: %dx%d@%dHz பாங்கை வெளியீட்டுடன் %dx%d@%dHz இல் முயற்சிக்கிறது (pass %d)
 வேலை செய்யவில்லை என்றாலும் xtermஐ பயன்படுத்தி ஒரு முனையத்தை தேட முடியவில்லை மடிக்கணினி பிரதிபலிக்கும் திரைகள் தெரியாத RANDR விரிவாக்கம் இல்லை CRTC %d க்கு பாங்குகளை முயற்சிக்கிறது
 வெளியீடு %s ஐ நகலாக்க முடியவில்லை வெளியீடுகளுக்கு எந்த சிஆர்டிசி க்களையும் பொறுப்பாக்க முடியவில்லை:
%s CRTC %d பற்றிய தகவலைப் பெற முடியவில்லை வெளிப்பாடு %d பற்றிய தகவலை பெற முடியவில்லை திரை அளவுகளின் வரம்பை பெற முடியவில்லை திரை மூலங்களை பெற முடியவில்லை (CRTCs, outputs, modes) CRTC %dக்கு கட்டமைப்பை அமைக்க முடியவில்லை செயலிலுள்ள கட்டமைப்புடன் சேமிக்கப்பட்ட காட்சி கட்டமைப்புகள் ஒன்றும் பொருந்தவில்லை தேர்ந்தெடுக்கப்பட்ட எந்த பாங்குகளும் இருக்கும் பாங்குகளுடன் பொருந்தவில்லை:
%s வெளியீடு %s க்கு நகலெடுத்த இன்னொரு வெளியீட்டுக்கு உள்ள அதே தரு மதிப்புகள் இல்லை:
தற்போதைய பாங்கு = %d, புதிய பாங்கு = %d
தற்போதைய ஆயத்தொலைவுகள் = (%d, %d), புதிய ஆயத்தொலைவுகள் = (%d, %d)
தற்போதைய சுழற்சி = %s, புதிய சுழற்சி = %s வெளியீடு %s,  %dx%d@%dHz பாங்கை ஆதரிக்கவில்லை கோரப்பட்ட நிலை/அளவு CRTC %dக்கு வெளியேயுள்ள அனுமதிக்கபட்ட வரம்பாகும்: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) தேவையான மெய்நிகர் அளவு இருக்கும் அளவுடன் பொருந்தவில்லை: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) கையாளப்படாத X பிழை திரை அளவுகளின் வரம்பை பெறும் போது 