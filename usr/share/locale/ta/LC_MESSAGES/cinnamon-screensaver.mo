��          L      |       �   `   �      
     %  )   B     l  �  �  �   5  D     E   L  [   �  �   �                                         The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver.master.ta
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-08-03 09:40+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: American English <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 திரைகாப்பு %d வினாடியாக செயல்படுகிறது
 திரைகாப்பு %d வினாடிகளாக செயல்படுகிறது.
 திரைகாப்பு செயலிலுள்ளது
 திரைகாப்பு செயலில் இல்லை
 திரைகாப்பு தற்போது செயலிலுள்ளது.
 நீங்கள் உயர் நிலை எழுத்து பூட்டு விசையை செயலாக்கி உள்ளீர்கள். 