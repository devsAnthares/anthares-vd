��    E      D  a   l      �     �          -  5   ;  7   q     �  (   �  )   �  %     '   )  "   Q     t     �     �  !   �     �     �     �      �          "     .     =     E     a     w  '   �  	   �  '   �     �  P   �     8	     O	     ^	     w	     �	     �	     �	     �	     �	  &   �	     
     
      3
  +   T
     �
     �
     �
     �
     �
      �
  f     f   i  E   �       &        E     d  X   �  X   �  $   2  X   W     �  
   �     �     �  
   �     �  �  �     �     �     �  2   �  G   
     R  %   Y  '     (   �  )   �     �  "        8     P  +   V     �     �     �  *   �     �     �       	   #  %   -  !   S     u  8   �     �  /   �        N        _     t  )   �     �     �     �     �     �  &     8   2     k     �  $   �  5   �                3     M  !   d  *   �  �   �  l   7  B   �  
   �  /   �  &   "     I  o   c  p   �  '   D  n   l     �  
   �  
   �     �     
          =         +                     %                 !   5   3       :      <       $   6                      0             &   E      "   @          C   /   2   9   .       )       '   7             	       ,              >       ?          4   -                               B             (      ;      #   1   8              *         A   
   D            - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. This system will be automatically restarted in %d second. This system will be restarted in %d seconds. This system will be automatically shut down in %d second. This system will be shut down in %d seconds. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". You will be automatically logged out in %d second. You will be logged out in %d seconds. _Cancel _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-23 18:40+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Türkçe <gnome-turk@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: tr
  - Cinnamon oturum yöneticisi Bir program hala çalışıyor: AUTOSTART_DIR Uygulama belgeleri komut satırında kabul etmiyor Belge URI'lerini bir 'Tür=Bağ' masaüstü girişi olarak geçilemiyor İptal Oturum yöneticisine bağlanılamadı ICE dinleme soketi oluşturulamadı: %s Oturum yöneticisine bağlantıyı kapat Kullanıcıya özel programları yükleme Kullanıcıdan onay isteme Hata ayıklama kodunu etkinleştir %d kodu ile çıkıldı DOSYA Dosya geçerli bir .desktop dosyası değil Yine de Uyku Kipine Geç ID Simge '%s' bulunamadı Mevcut tüm engelleyiciler yok sayılıyor %d sinyali ile öldürüldü Ekranı Kilitle Yine de Oturumu Kapat Çıkış Bu sistemden şimdi çıkılsın mı? Başlatılabilir bir öğe değil Cevap vermiyor Standart kendiliğinden başlat dizinlerinin yerine geç Bilgisayarı kapat Program çakışan seçeneklerle çağırıldı Yeniden Başlat Oturum şu anda kapatıldığı için yeni istemci bağlantısı reddediliyor
 Hatırlanan Uygulama Yine de Yeniden Başlat Sistem şimdi yeniden başlatılsın mı? SESSION_NAME _Beklemeye Al Oturum Oturum yönetim seçenekleri: Kullanılacak oturum Oturum yönetim seçeneklerini göster Sınama için büyük hata iletişim penceresini göster Yine de Bilgisayarı Kapat Sistem şimdi kapatılsın mı? Bazı programlar hala çalışıyor: Kayıtlı yapılandırmayı içeren dosyayı belirtin Oturum yönetim ID'si belirtin %s Başlatılıyor %d sinyali ile durduruldu Yine de Beklemeye Geç Yine de Kullanıcıyı Değiştir Bu program, oturumu kapatmayı engelliyor. Bu sistem %d saniye içinde kendiliğinden yeniden başlatılacak. Bu sistem %d saniye içinde kendiliğinden yeniden başlatılacak. Bu sistem %d saniye içerisinde kendiliğinden kapanacaktır. Bu sistem %d saniye içerisinde kapanacaktır. Giriş oturumu başlatılamıyor (ve X sunucuya bağlanılamıyor) Bilinmiyor Algılanamayan masa üstü dosya sürümü '%s' Algılanamayan başlama seçeneği: %d Bu uygulamanın sürümü Programların bitmesi bekleniyor.  Bu programları kesmek yaptığınız işlerin kaybolmasına sebep olabilir. Programın bitmesi bekleniyor.  Programı kesintiye uğratmak çalışmalarınızı kaybetmenize sebep olabilir. Şu anda "%s" olarak giriş yaptınız. Oturumunuz %d saniye içerisinde kendiliğinden kapanacaktır. Oturumunuz %d saniye içerisinde kapanacaktır. _İptal _Uyku Kipi Çı_kış _Yeniden Başlat _Kapat _Kullanıcı Değiştir 