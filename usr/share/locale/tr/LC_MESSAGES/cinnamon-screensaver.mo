��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S    r  C   �     �  D   �     +  $   :     _  ?   }  "   �     �  H   �  G   @     �     �  $   �  #   �  
   	     	  $   *	                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-01-04 00:30+0000
Last-Translator: Ali Kömesöğütlü <byzlo685@gmail.com>
Language-Team: Turkish <gnome-turk@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: tr
 Ekran koruyucudan uygun biçimde çıkış yapılmasına sebep olur Kontrol ediliyor... Eğer ekran koruyucu etkin ise etkinliğini kaldır (ekranı doldur) Hatalı şifre Kilit ekranında gösterilecek mesaj Lütfen şifrenizi giriniz... Ekran koruyucusunun etkin olma süresinin genişliğini sorgula Ekran koruyucunun durumunu sorgula Kullanıcı Değiştir Çalışan ekran koruyucu sürecine ekranı hemen kilitlemesini belirtir Ekran koruyucu %d saniyedir etkin.
 Ekran koruyucu %d saniyedir etkin.
 Ekran koruyucu etkin
 Ekran koruyucu devre dışı
 Ekran koruyucu şu an etkin değil.
 Ekran koruyucuyu aç (boş ekranda) Kilidi Aç Bu uygulamanın sürümü Büyük harf kilidi tuşunuz açık. 