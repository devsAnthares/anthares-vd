��          �   %   �      p     q     t     z     �     �     �     �     �     �     �  	   �     �               $     -     9  $   X  D   }     �     �     �  %     F   (  �   o  *   =  i   h  �  �     �     �     �  
   �     �     �     �     �            	   0     :     G     [     t     }  (   �  /   �  H   �     +	     F	  0   U	  $   �	  F   �	  �   �	  /   �
  f                                                 	              
                                                               %R %R:%S %a %R %a %R:%S %a %b %-e_%R %a %b %-e_%R:%S %a %b %-e_%l:%M %p %a %b %-e_%l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %b %-e_%R %b %-e_%R:%S %b %-e_%l:%M %p %b %-e_%l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-desktop/issues
POT-Creation-Date: 2018-08-06 07:17+0000
PO-Revision-Date: 2018-08-08 10:14+0300
Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.7
 %R %R:%S %a %R %a %R∶%S %-e %b %a_%R %-e %b %a_%R:%S %-e %b %a_%l∶%M %p %-e %b %a_%l∶%M∶%S %p %a %l∶%M %p %a %l∶%M∶%S %p %-e %b_%R %-e %b_%R:%S %-e %b_%l∶%M %p %-e %b_%l∶%M∶%S %p %l:%M %p %l:%M:%S %p CRTC %d, %s çıktısını kullanamıyor CRTC %d, rotasyon=%d özelliğini desteklemiyor CRTC %d: %dx%d@%dHz kipi, %dx%d@%dHz çıkışıyla (pass %d) deneniyor
 CRTC %d kipleri deneniyor
 Belirtilmemiş %s çıktısına birebir kopyalama yapılamıyor CRTC'ler çıktılara atanamadı:
%s seçilen kiplerden hiçbiri kullanılabilir kiplerle uyumlu değil:
%s %s çıktısı, birebir kopyalanmış başka bir çıktıyla aynı değiştirgelere sahip değil:
geçerli kip = %d, yeni kip = %d
geçerli koordinatlar = (%d, %d), yeni koordinatlar = (%d, %d)
geçerli rotasyon = %d, yeni rotasyon = %d %s çıktısı, %dx%d@%dHz kipini desteklemiyor gereken sanal boyut kullanılabilir boyuta uymuyor: istenen=(%d, %d), en az=(%d, %d), en çok=(%d, %d) 