��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  ]   P  I   �  O   �  6   H  &        �      �  "   �  Q   	     [               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
PO-Revision-Date: 2012-05-08 16:04+0000
Last-Translator: Necdet Yücel <necdetyucel@gmail.com>
Language-Team: Turkish (http://www.transifex.com/projects/p/freedesktop/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=1; plural=0;
 Oturum açma ekranı yapılandırmasını değiştirmek için kimlik kanıtlaması gereklidir Kullanıcı bilgisini değiştirmek için kimlik kanıtlaması gereklidir Kendi kullanıcı bilginizi değiştirmek için kimlik kanıtlaması gereklidir Oturum açma ekranı yapılandırmasını değiştirin Kullanıcı bilgilerinizi değiştirin Hata ayıklama kodunu aktif et Kullanıcı hesaplarını yönet Sürüm bilgisini göster ve çık Kullanıcı hesap bilgisini sorgulamak
ve düzenlemek için D-Bus arayüzü sunar Mevcut örneği değiştir 