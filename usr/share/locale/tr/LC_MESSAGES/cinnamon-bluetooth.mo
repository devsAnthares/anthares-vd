��    0      �  C         (     )     1     E  	   c     m     �  (   �     �     �     �  
   �  *   �  *   )     T     c     y  K   �     �     �     �               "     >     A     H     d  B   {  -   �     �  %   �          '     ?     M     _     v  5   �     �  
   �     �     �     �     �                 l  %     �	     �	     �	  	   �	     �	     �	  <   �	     <
     P
     W
     w
     �
  .   �
     �
     �
     �
  [        ]     n     z  "   �     �  *   �     �     �  #   �       ^   7  3   �     �  /   �               0     A     T     l  -   z     �     �     �     �     �     �               &                /                0   !                         
                 	                 ,             %      #                                )   &             .   (   *       "             '             +      $   -               Address Always grant access Authorization request from %s Bluetooth Bluetooth Settings Bluetooth is disabled Bluetooth is disabled by hardware switch Browse Files... Cancel Configure Bluetooth settings Connection Device %s wants access to the service '%s' Device %s wants to pair with this computer Does not match Error browsing device Grant this time only If you remove the device, you will have to set it up again before next use. Keyboard Settings Matches Mouse Settings Mouse and Touchpad Settings No No Bluetooth adapters found OK Paired Pairing confirmation for %s Pairing request for %s Please confirm whether the PIN '%s' matches the one on the device. Please enter the PIN mentioned on the device. Reject Remove '%s' from the list of devices? Remove Device Send Files to Device... Send Files... Set Up New Device Set up a New Device... Sound Settings The requested device cannot be browsed, error is '%s' Type Visibility Visibility of “%s” Yes connecting... disconnecting... hardware disabled page 1 page 2 Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-10-21 10:08+0000
Last-Translator: Butterfly <Unknown>
Language-Team: Turkish <tr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 Adres Daima erişim izni ver Aygıt %s yetkilendirme istiyor Bluetooth Bluetooth Ayarları Bluetooth kapalı Bluetooth donanımsal anahtar ile devre dışı bırakıldı Dosyalara Gözat... İptal Bluetooth Ayarlarını Düzenle Bağlantı Aygıt %s  '%s' Aygıt %s bu bilgisayarla eşleştirme istiyor Eşleşmiyor Aygıt tarama hatası Sadece bu seferlik izin ver Eğer cihazı kaldırırsanız, bir sonraki kullanım için yeniden ayarlamanız gerekecek. Klavye Ayarları Eşleşiyor Fare Ayarları Fare ve Dokunmatik Panel Ayarları Hayır Bluetooth bağdaştırıcısı bulunamadı Tamam Eşleştirilmiş Aygıt %s için eşleştirme onayı %s için eşleştirme isteği Lütfen PIN '%s' numarasının bir eşleşir olup olmadığını cihaz üstünde teyit ediniz. Aygıt üzerinde belirtilen PIN numarasını girin. Reddet Cihazlar listesinden '%s' aygıtı silinsin mi? Aygıtı kaldır Dosyaları Aygıta Gönder... Dosya Gönder... Yeni Aygıt Ayarla Yeni Aygıt Kurulumu... Ses Ayarları İstenen cihaz görüntülenemez, hata : '%s' Tür Görünürlük “%s” için Görünürlük Evet bağlanıyor... bağlantı kesiliyor... donanım devre dışı sayfa 1 sayfa 2 