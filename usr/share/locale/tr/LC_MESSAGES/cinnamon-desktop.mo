��          �   %   �      p     q     ~     �     �  	   �     �  $   �  D     <   J     �     �     �     �     �  %   �  '     )   F  '   p  :   �  +   �  I   �  F   I  �   �  *   ^  t   �  i   �  9   h  S  �     �     	     	     &	  	   <	  (   F	  /   o	  H   �	  C   �	  	   ,
  
   6
     A
     `
  0   {
  $   �
  #   �
  '   �
  +     =   I  6   �  O   �  F     �   U  /   ?  |   o  `   �  R   M                                   	                       
                                                                 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d cannot drive output %s CRTC %d does not support rotation=%s CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Cannot find a terminal, using xterm, even if it may not work Laptop Monitor vendorUnknown RANDR extension is not present Trying modes for CRTC %d
 cannot clone to output %s could not assign CRTCs to outputs:
%s could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %s, new rotation = %s output %s does not support mode %dx%d@%dHz requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-06-25 18:18+0300
Last-Translator: Muhammet Kara <muhammet.k@gmail.com>
Language-Team: Turkish <gnome-turk@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=1; plural=0;
 %A %B %e, %R %A %B %e, %R:%S %A %B %e, %l:%M %p %A %B %e, %l:%M:%S %p %A, %B %e CRTC %d, %s çıktısını kullanamıyor CRTC %d, rotasyon=%s özelliğini desteklemiyor CRTC %d: %dx%d@%dHz kipi, %dx%d@%dHz çıkışıyla (pass %d) deneniyor
 Terminal bulunamadı.Çalışmıyor olsa bile xterm kullanılıyor. Dizüstü Bilinmeyen RANDR uzantısı mevcut değil CRTC %d kipleri deneniyor
 %s çıktısına birebir kopyalama yapılamıyor CRTC'ler çıktılara atanamadı:
%s CRTC %d hakkında bilgi alınamadı Çıktı hakkında bilgi alınamadı %d Ekran boyutlarının aralığı alınamadı Ekran kaynakları alınamadı(CRTC'ler, çıkışlar, kipler) CRTC %d için yapılandırma ayarları tanımlanamadı Hiçbir kayıtlı ekran yapılandırması aktif yapılandırma ile eşleşmiyor seçilen kiplerden hiçbiri kullanılabilir kiplerle uyumlu değil:
%s %s çıktısı, birebir kopyalanmış başka bir çıktıyla aynı parametrelere sahip değil:
geçerli kip = %d, yeni kip = %d
geçerli koordinatlar = (%d, %d), yeni koordinatlar = (%d, %d)
geçerli rotasyon = %s, yeni rotasyon = %s %s çıktısı, %dx%d@%dHz kipini desteklemiyor CRTC %d için istenilen konum/boyut izin verilen limitlerin dışında: pozisyon=(%d, %d), boyut=(%d, %d), en fazla=(%d, %d) Gerekli sanal boyut mevcut boyuta uymuyor: istenilen=(%d, %d), en az=(%d, %d), en fazla=(%d, %d) Ekran boyutlarının aralığı alınırken idare edilemeyen bir X hatası oluştu 