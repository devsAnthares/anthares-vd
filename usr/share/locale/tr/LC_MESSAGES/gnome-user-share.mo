��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w  &   P  5   w  :   �     �  
          _   $  u   �     �  *                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-08-25 23:22+0300
Last-Translator: Muhammet Kara <muhammetk@gmail.com>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.7
 %s kullanıcısının genel dosyaları %s kullanıcısının %s üzerindeki genel dosyaları Eğer etkinse Kişisel Kullanıcı Paylaşımını başlat Kişisel Dosya Paylaşımı Paylaşım Paylaşım Ayarları Bu klasörün içeriğini ağ üzerinde paylaşmak için Kişisel Dosya Paylaşımını açın. Parola sor seçildiğinde. Geçerli değerler "never" (asla), "on_write" (yazıldığında), ve "always" (her zaman). Ne zaman parola gerektiği paylaş;dosyalar;http;ağ;kopyala;gönder; 