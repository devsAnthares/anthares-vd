��    )      d  ;   �      �  *   �  
   �  )   �     �     	  z        �     �  ,   �     �        3        M     ^  7   w  (   �  a   �     :     Y     x     �     �  -   �     �  (        -  &   ;     b  -   w  '   �     �     �     �     �          &  	   <  
   F     Q     j  �  �  3   �
  
   �
  +   �
     �
     �
  �        �     �  A   �  #        )  ;   C       "   �  C   �  .   �  l   '     �     �      �     �       (   #     L  )   ^     �  *   �     �  '   �  #   �  
         +     A     P     ^     j  
        �     �     �     $   (   )                                                                                 
      "                               !          #            &         	                            %   '        Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" preferences-desktop-keyboard Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=Indicator
POT-Creation-Date: 2017-04-27 15:27+0000
PO-Revision-Date: 2018-03-14 12:54+0300
Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.7
 Öntanımlı küme, pencere oluşturulurken atanır Belirteç: Her pencere için ayrı küme tut ve yönet Klavye Düzeni Klavye düzeni Klavye düzeni "%s"
Telif Hakkı &#169; X.Org Vakfı ve XKeyboardConfig katkıda bulunanları
Lisans bilgileri için paket üst verisine göz atın Klavye modeli Klavye seçenekleri Egzotik, çok nadir kullanılan düzenleri ve seçenekleri yükle Ek yapılandırma ögelerini yükle Klavye düzeni ön izleme Düzen kümeleriyle birlikte belirteçleri de kaydet/yükle İkincil kümeler Uygulamacıkta bayrakları göster Geçerli düzeni belirtmek için uygulamacıkta bayrakları göster Küme adları yerine düzen adlarını göster Küme adları yerine düzen adlarını göster (yalnızca çoklu düzen destekleyen XFree sürümleri için) Klavye Ön İzlemesi, X ofseti Klavye Ön İzlemesi, Y ofseti Klavye Ön İzlemesi, yükseklik Klavye Ön İzlemesi, genişlik Arka plan rengi Düzen göstergesi için arka plan rengi Yazı tipi ailesi Düzen göstergesi için yazıtipi ailesi Yazı tipi boyutu Düzen göstergesi için yazı tipi boyutu Ön alan rengi Düzen göstergesi için ön alan rengi Resim yüklenirken hata oluştu: %s Bilinmeyen XKB başlatma hatası klavye düzeni klavye modeli düzen "%s" model "%s", %s ve %s düzen yok seçenek yok seçenek "%s" preferences-desktop-keyboard 