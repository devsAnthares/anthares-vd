��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  %   {     �  -   �  "   �  !        $     D  	   S  !   ]  +     $   �  !   �     �     �     	  #   	     ?	  '   H	     p	     �	     �	     �	  i   �	  #   3
  $   W
     |
     �
  '   �
  E   �
     	                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&keywords=I18N+L10N&component=general
PO-Revision-Date: 2012-08-12 19:42+0300
Last-Translator: Muhammet Kara <muhammetk@acikkaynak.name.tr>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.1
Plural-Forms: nplurals=1; plural=0;
 - GNOME fare erişilebilirlik hizmeti Düğme Biçimi Tıklama türü penceresinin düğme biçimi. Tıklama Türü Pencere Geometrisi Tıklama türü pencere yönelimi Tıklama türü pencere biçimi Çift tıklama Sürükle Durağan tıklamayı etkinleştir Benzetimli ikincil tıklamayı etkinleştir Yardım Gösterimi Başarısız Oldu Tıklama türü penceresini gizle Yatay Durağan Tıklama Sadece simgeler Ufak belirteç hareketlerini yoksay Yönelim Tıklama türü penceresinin yönelimi. İkincil Tıklama Etkin bekleme kipini ayarla Fare Ayarlayıcısını kapat Tek Tıklama Tıklama türü penceresinin boyutu ve konumu. Standart X Pencere Sistemi geometri dizgesi biçimindedir. Mousetweaks'i hizmet olarak başlat mousetweaks'i giriş modunda başlat Metin ve Simgeler Sadece metin Durak tıklama öncesi beklenecek süre Benzetimli ikincil tıklamayı etkinleştirmek için beklenecek süre Dikey 