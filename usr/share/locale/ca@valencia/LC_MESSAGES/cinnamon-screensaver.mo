��          L      |       �   `   �      
     %  )   B     l  �  �  k   ;  "   �  $   �  $   �  3                                            The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 You have the Caps Lock key on. Project-Id-Version: cinnamon-screensaver
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-10-10 10:11+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Catalan <tradgnome@softcatala.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ca
 Fa %d segons que l'estalvi de pantalla s'ha activat.
 Fa %d segons que l'estalvi de pantalla s'ha activat.
 L'estalvi de pantalla està actiu
 L'estalvi de pantalla està inactiu
 L'estalvi de pantalla no és actiu.
 Teniu la tecla de fixació de majúscules activada. 