��    8      �  O   �      �     �     �  
                   :     U  *   r      �     �  
   �  )   �          +     F     V  z   f     �     �  ,        .     M     ]  3   v     �     �  7   �  (     a   5     �     �     �     �     	  -   #	     Q	  (   a	     �	  &   �	     �	  -   �	     
  .   
  '   L
     t
     �
     �
     �
     �
     �
     �
     �
  	     
        %  �  >     �     �     �       #     "   8  "   [  ;   ~  1   �  #   �  
     /     $   K  #   p     �     �  �   �     `     q  @   �  '   �     �  &      D   '     l  *   }  L   �  @   �  �   6  :   �  7   �  (   5  (   ^     �  1   �     �  @   �     +  <   G     �  7   �      �  8   �  1   -  %   _  
   �      �     �     �     �  &   �          /     D     U                    !             0                           7               )          	   8   5           +   3   "             '           ,         4   %       $   6      2       1          -   #   (                               *           
   &         /                .           Activate more plugins Active _plugins: Add Plugin Close the dialog Configure the selected plugin Deactivate selected plugin Decrease the plugin priority Default group, assigned on window creation Enable/disable installed plugins Increase the plugin priority Indicator: Keep and manage separate group per window Keyboard Indicator Plugins Keyboard Indicator plugins Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items No description. Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator The list of active plugins The list of enabled Keyboard Indicator plugins There was an error loading an image: %s Unable to open help file Unknown XKB initialization error _Available plugins: keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-05-29 18:52+0200
PO-Revision-Date: 2011-02-27 10:22+0100
Last-Translator: Gil Forcada <gilforcada@guifi.net>
Language-Team: Catalan <tradgnome@softcatala.org>
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Activa més connectors _Connectors actius: Afig un connector Tanca el diàleg Configureu el connector seleccionat Desactiva el connector seleccionat Redueix la prioritat del connector Grup predeterminat, s'assigna en la creació d'una finestra Habiliteu/inhabiliteu els connectors instal·lats Augmenta la prioritat del connector Indicador: Mantén i gestiona un grup separat per finestra Connectors de l'indicador del teclat Connectors de l'indicador de teclat Disposició del teclat Disposició del teclat Disposició de teclat «%s»
Copyright &#169; la Fundació X.Org i els col·laboradors del XKeyboardConfig
Vegeu les metadades del paquet per a la llicència Model del teclat Opcions del teclat Carrega disposicions i opcions utilitzades rarament i exòtiques Carrega elements de configuració extra Sense descripció. Previsualitzeu disposicions del teclat Alça/restaura els indicadors juntament amb els grups de disposició Grups secundaris Mostra els indicadors en la miniaplicació Mostra els indicadors en la miniaplicació per indicar la disposició actual Mostra els noms de les disposicions en comptes dels noms de grup Mostra els noms de les disposicions en comptes dels noms de grup (només per a les versions del XFree que permeten múltiples disposicions) La previsualització del teclat, desplaçament horitzontal La previsualització del teclat, desplaçament vertical La previsualització del teclat, alçada La previsualització del teclat, amplada El color de fons El color de fons per a l'indicador de disposició La família del tipus de lletra La família del tipus de lletra per a l'indicador de disposició La mida del tipus de lletra La mida del tipus de lletra per a l'indicador de disposició El color de primer pla El color de primer pla per a l'indicador de disposició La llista dels connectors actius La llista de connectors d'indicador de teclat habilitats S'ha produït un error en carregar una imatge: %s No s'ha pogut obrir el fitxer d'ajuda Desconegut Error d'inicialització de l'XKB Connectors _disponibles: Disposició del teclat Model del teclat «%s» disposició «%s» disposicions model «%s, %s i %s» no hi ha disposició no hi ha opcions «%s» opció «%s» opcions 