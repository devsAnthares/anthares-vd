��          t      �                 #  '   ;     c     y     �  T   �  Q   �     9  #   S  �  w     @     W  ?   s  !   �     �     �  a         b      �  *                         	                                  
    %s's public files %s's public files on %s Launch Personal File Sharing if enabled Personal File Sharing Sharing Sharing Settings Turn on Personal File Sharing to share the contents of this folder over the network. When to ask for passwords. Possible values are "never", "on_write", and "always". When to require passwords share;files;http;network;copy;send; Project-Id-Version: gnome-user-share
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-user-share&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-07-08 12:08+0100
Last-Translator: Xavi Ivars <xavi.ivars@gmail.com>
Language-Team: Catalan <tradgnome@softcatala.org>
Language: ca-valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
 Fitxers públics de %s Fitxers públics de %s a %s Executa la compartició de fitxers personals si està habilitat Compartició de fitxers personals Compartició Preferències de compartició Activa la compartició de fitxers personals per compartir el contingut d'esta carpeta a la xarxa. Quan s'ha de demanar contrasenya. Els possibles valors són «never» (mai), «on_write» (en escriure), i «always» (sempre). Quan s'ha de demanar contrasenya comparteix;fitxers;http;xarxa;copia;envia; 