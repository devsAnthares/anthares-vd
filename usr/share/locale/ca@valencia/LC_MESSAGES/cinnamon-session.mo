��    @        Y         �     �     �     �  5   �  7        9  (   @  )   i  %   �  '   �  "   �               .  !   3     U     f     i      }     �     �     �     �     �     �       '     	   >  '   H     p  P   w     �     �     �     	     	     	     9	     H	  &   h	     �	     �	      �	  +   �	     
     &
     2
     G
     V
      i
  E   �
     �
  &   �
     �
       X   :  X   �  $   �  
             %  
   .     9  �  F  %     /   +     [  :   v  T   �  
     -     /   ?  1   o  7   �  &   �                7  %   >     d     }     �  -   �     �     �                    9     V  3   i     �  H   �     �  K   �     @     U     o     �     �  !   �     �  +   �  -        H     ^  4   x  :   �  5   �          2  "   O      r  %   �  K   �  
     8     +   I     u  s   �  c        j     �     �  	   �     �     �     #       5   .   /       ;   1   
           :       "                     8                 $   9   7             <                               ,          0   ?      6             %                    -   @   3   )          !   +       =                       '   (               >   	         4      *   &      2           - the Cinnamon session manager A program is still running: AUTOSTART_DIR Application does not accept documents on command line Can't pass document URIs to a 'Type=Link' desktop entry Cancel Could not connect to the session manager Could not create ICE listening socket: %s Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Exited with code %d FILE File is not a valid .desktop file Hibernate Anyway ID Icon '%s' not found Ignoring any existing inhibitors Killed by signal %d Lock Screen Log Out Anyway Log out Log out of this system now? Not a launchable item Not responding Override standard autostart directories Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Restart Anyway Restart this system now? SESSION_NAME S_uspend Session management options: Session to use Show session management options Show the fail whale dialog for testing Shut Down Anyway Shut down this system now? Some programs are still running: Specify file containing saved configuration Specify session management ID Starting %s Stopped by signal %d Suspend Anyway Switch User Anyway This program is blocking logout. Unable to start login session (and unable to connect to the X server) Unknown Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application Waiting for programs to finish.  Interrupting these programs may cause you to lose work. Waiting for the program to finish.  Interrupting the program may cause you to lose work. You are currently logged in as "%s". _Hibernate _Log Out _Restart _Shut Down _Switch User Project-Id-Version: cinnamon-session 2.3.7
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-06 11:18+0000
Last-Translator: David Planella <david.planella@gmail.com>
Language-Team: Catalan <tradgnome@softcatala.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
Language: ca-XV
  - el gestor de sessions del Cinnamon Hi ha un programa que encara s'està executant: DIRECTORI_INICI_AUTOMÀTIC L'aplicació no accepta documents des de la línia d'ordes No es poden passar URI de document a una entrada d'escriptori de tipus «Type=Link» Cancel·la No s'ha pogut connectar al gestor de sessions No s'ha pogut crear el sòcol ICE d'escolta: %s Inhabilita la connexió amb el gestor de sessions No carreguis les aplicacions especificades per l'usuari No demanes la confirmació de l'usuari Habilita el codi de depuració S'ha sortit amb codi %d FITXER Este no és un fitxer .desktop vàlid Hiberna de totes maneres IDENTIFICADOR No s'ha trobat la icona «%s» S'està ignorant qualsevol inhibidor existent S'ha matat per la senyal %d Bloca la pantalla Ix de totes maneres Ix Voleu eixir d'este sistema? No és un element executable No està responent Ignora els directoris estàndard d'inici automàtic Para El programa s'ha invocat amb opcions que entren en conflicte entre elles Reinicia Es refusarà la connexió de client nova perquè la sessió s'està parant
 Aplicació recordada Reinicia de totes maneres Voleu reiniciar el sistema ara? NOM_DE_SESSIÓ A_tura temporalment Opcions de gestió de la sessió: Sessió que s'utilitzarà Mostra les opcions de gestió de la sessió Mostra el diàleg de la balena per fer proves Para de totes maneres Voleu parar este sistema? Hi ha alguns programes que encara s'estan executant: Especifiqueu el fitxer que conté la configuració alçada Especifiqueu l'identificador de gestió de la sessió S'està iniciant %s S'ha aturat per la senyal %d Para temporalment de totes maneres Canvia d'usuari de totes maneres Este programa està blocant l'eixida. No s'ha pogut iniciar la sessió d'entrada (ni connectar amb el servidor X) Desconegut No es reconeix la versió «%s» del fitxer d'escriptori No és reconeix l'opció de llançament: %d Versió d'aquesta aplicació S'està esperant que finalitzin alguns programes. Si els interrompeu és possible que perdeu les dades no alçades. S'està esperant que finalitzi el programa. Si l'interrompeu podríeu perdre les dades no alçades. Heu entrat com a «%s». _Hiberna I_x _Reinicia _Atura l'ordinador Canvia d'_usuari 