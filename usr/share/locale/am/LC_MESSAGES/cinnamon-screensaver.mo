��          �      L      �  )   �     �  E   �     =  &   P     w  8   �  "   �     �  D   �  `   B     �     �  )   �  *        0     7     S  �  r  K        P  {   n  (   �  I     7   ]  f   �  7   �     4  t   Q  �   �  (   ^	  1   �	  >   �	  C   �	     <
  &   I
  1   p
                    	                
                                                                Causes the screensaver to exit gracefully Checking... If the screensaver is active then deactivate it (un-blank the screen) Incorrect password Message to be displayed in lock screen Please enter your password... Query the length of time the screensaver has been active Query the state of the screensaver Switch User Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Turn the screensaver on (blank the screen) Unlock Version of this application You have the Caps Lock key on. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-09 15:58+0000
Last-Translator: samson <Unknown>
Language-Team: Amharic <am@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2018-06-26 10:13+0000
X-Generator: Launchpad (build 18688)
 የ መመልከቻ ማዳኛ በ ሰላም እንዲወጣ ይረዳዋል በ መመርመር ላይ... የ መመልከቻ ማዳኛ ሁኔታ ንቁ ከሆነ ማቦዘኛ (መመልከቻውን ባዶ ማድረግ መተው) የ ተሳሳተ የ መግቢያ ቃል መመልከቻው በሚቆለፍ ጊዜ የሚታየው መልእክት እባክዎን የ መግቢያ ቃል ያስገቡ... የ መመልከቻ ማዳኛ ሁኔታ መጠየቂያ ምን ያህል ጊዜ ንቁ እንደሆነ የ መመልከቻ ማዳኛ ሁኔታ መጠየቂያ ተጠቃሚ መቀየሪያ እየሄደ ያለውን የ መመልከቻ ማዳኛ ሁኔታ ወዲያውኑ እንዲቆለፍ መጠየቂያ መመልከቻ ማዳኛው ንቁ ነው ከ %d ሰከንድ ጀምሮ
 መመልከቻ ማዳኛው ንቁ ነው ከ %d ሰከንዶች ጀምሮ.
 መመልከቻ ማዳኛ ንቁ ነው
 መመልከቻ ማዳኛ ንቁ አይደለም
 መመልከቻ ማዳኛው አሁን ንቁ አይደለም
 የ መመልከቻ ማዳኛ ማብሪያ (ባዶ መመልከቻ) መክፈቻ የዚህ መተግበሪያ እትም የ እርስዎ Caps Lock ቁልፍ በርቷል 