��          �      �       0  <   1     n     u  '   �  )   �  '   �  :     +   I  I   u  t   �  i   4  9   �  w  �  �   P     0  ?   C  R   �  b   �  g   9     �  P   !  �   r  t   &	  i   �	  �   
                      
   	                                 Cannot find a terminal, using xterm, even if it may not work Laptop RANDR extension is not present could not get information about CRTC %d could not get information about output %d could not get the range of screen sizes could not get the screen resources (CRTCs, outputs, modes) could not set the configuration for CRTC %d none of the saved display configurations matched the active configuration requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) unhandled X error while getting the range of screen sizes Project-Id-Version: cinnamon-desktop.master.mai
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2009-09-20 12:26+0530
Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>
Language-Team: Hindi <hindi.sf.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n!=1);




X-Generator: KBabel 1.11.4
 कोनो टर्मिनल नहि ढूँढ़ि सकैत अछि, xterm कए प्रयोग कए रहल अछि, बावजूद जे यह काज नहि करैत अछि लैपटाप RANDR विस्तार मोजुद नहि अछि CRTC %d क संबंधमे सूचना नहि पाबि सकल %d आउटपुटक संबंधीमे सूचना नहि पाबि सकल स्क्रीन आकार कए परिसर पाबि नहि सकैत अछि स्क्रीन संसाधन नहि पाबि सकैत अछि (CRTC, आउटपुट, विधि) CRTC %d क लेल विन्यास सेट नहि कए सकल सक्रिय विन्यास सँ मेल खाएत कोनो सहेजल प्रदर्शन विन्यास सहेजल नहि छल requested position/size for CRTC %d is outside the allowed limit: position=(%d, %d), size=(%d, %d), maximum=(%d, %d) required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) अनियंत्रित X त्रुटि जखन स्क्रीन आकार कए दायरा पाबि रहल अछि 