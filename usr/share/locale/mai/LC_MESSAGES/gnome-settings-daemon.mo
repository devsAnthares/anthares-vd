��    E      D  a   l      �     �                 
   '     2  &   >  	   e     o  S   �     �  
   �     �  "   �  $     $   4  &   Y     �     �     �     �     �  &   �     �  >        F  9   K     �     �  �   �     	     "	     +	     ?	     R	     W	     i	     u	     }	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     �	     �	  	   
     
     
     *
     2
  &   @
     g
     t
     �
     �
  $   �
  	   �
  *   �
  	   �
     �
          (     5     ;  �  J  $   !     F     b  '        �  %   �  }   �     g  1   �  �   �     �  %   �     �  t   �  z   @  t   �  �   0  +   �  4   �  #        9  &   Y  �   �  <     �   D     �  �   �     �     �  D  �     �       ;   ,  2   h     �  '   �  )   �     �  &        8  &   E  "   l     �     �  5   �     �          1  8   >  "   w  8   �     �     �  "     Y   6  7   �     �     �  /   �  w   -     �  \   �  !   "  *   D  a   o  +   �     �  &            #       A                 2             '   5          9   !   ?   (               =                 .                    C      6   	               B   "   :               ,      1   -             @       7                         +       &           8       
   %      *   ;   /   3         E   )   <            4         $   0       D   >            A_vailable files: Accessibility Activate Antialiasing Background Bounce keys Cannot determine user's home directory Clipboard Clipboard plugin Couldn't put the machine to sleep.
Verify that the machine is correctly configured. DPI Deactivate Disabled Do you want to activate Slow Keys? Do you want to activate Sticky Keys? Do you want to deactivate Slow Keys? Do you want to deactivate Sticky Keys? Do_n't activate Do_n't deactivate Don't become a daemon Eject Empty Trash Empty all of the items from the trash? Enable debugging code Error while trying to run (%s)
which is linked to the key (%s) Font GConf key %s set to type %s but its expected type was %s
 Hinting Home folder If you choose to empty the trash, all items in it will be permanently lost. Please note that you can also delete them separately. Ignore Keyboard Launch help browser Launch web browser Left Load modmap files Lock screen Log out Low Disk Space Mouse Mouse Preferences Mouse keys Next track Normal Play (or play/pause) Previous track Right Search Slow Keys Alert Slow keys Sticky Keys Alert Sticky keys Suspend System Sounds There was an error displaying help: %s Typing Break Volume down Volume mute Volume step Volume step as percentage of volume. Volume up Would you like to load the modmap file(s)? _Activate _Deactivate _Do not show this message again _Empty Trash _Load _Loaded files: Project-Id-Version: gnome-settings-daemon.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-settings-daemon&component=general
PO-Revision-Date: 2009-09-11 20:39+0530
Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>
Language-Team: Maithili <maithili.sf.net>
Language: mai
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);

X-Generator: KBabel 1.11.4
 उपलब्ध फाइल (_v) अभिगम्यता सक्रिय करू एंटीएलियासिंग पृष्ठभूमि उछलैत कुंजीसभ प्रयोक्ता क' घर निर्देशिका निर्धारित नहि कए सकल क्लिपबोर्ड क्लिपबोर्ड प्लगिन मसीनकेँ सुप्त अवस्थामे नहि लाए सकत.
सत्यापित करू जे मसीन उचित प्रकार विन्यस्त अछि. DPI निष्क्रिय करू अक्षम की अहाँ धीमा कुँजीसभ सक्रिय कएनाइ चाहैत अछि? की अहाँ स्टिकी कुँजीसभ सक्रिय कएनाइ चाहैत अछि? की अहाँ धीमी कुँजीसभ अक्रिय कएनाइ चाहैत अछि? की अहाँ स्टिकी कुँजीसभ निष्क्रिय कएनाइ चाहैत अछि? सक्रिय नहि करू (_n) निष्क्रिय नहि करू (_n) डेमोन नहि होउ बाहर निकालू रद्दी खाली करू अहाँ रद्दीसँ सबहि वस्तुसभकेँ खाली कएनाइ चाहैत अछि? डिबगिंग कोड सक्रिय करू (%s)केँ चलाबै क' कोशिशमे त्रुटि भेल
जे कुँजी (%s) सँ लिंक्ड अछि फोन्ट जीकॉन्फ कुँजी %s पर तय प्रकार %s अछि परंतु वांछित प्रकार छलः %s
 हिंटिंग घर फ़ोल्डर जँ अहाँ रद्दी खाली करब क'लेल चुनैत अछि, एहिमे सभ मद स्थायी रूप सँ मेट जएताह. कृप्या नोट करू जे अहाँ एकरा अलग-अलग मेटाए सकैत अछि. उपेक्षा कुँजीपटल मद्दति ब्राउज़र चलाबू वेब ब्राउज़र चलाबू बामाँ modmap फाइल लोड करू स्क्रीन लॉक करू लॉग आउट कम डिस्क स्थान माउस माउस वरीयतासभ  माउस कुंजीसभ अगिला ट्रैक सामान्य बजाउ (अथवा बजाउ/ठहरू) पछिला ट्रैक दहिन्ना खोजू धीमा कुँजीसभ चेतावनी धीमा कुंजीसभ स्टिकी कुँजी चेतावनी सटल कुंजीसभ सस्पेंड करू सिस्टम ध्वनि मद्दति देखाबैमे कोनो त्रुटि भेल: %s ब्रेक टाइप कए रहल अछि आवाज कम करू आवाज मौन आवाज निर्धारक चरण आवाज निर्धारक चरण आवाज निर्धारक क' प्रतिशतमे. आवाज बढ़ाबू की अहाँ modmap फाइल लोड कएनाइ चाहैत अछि? सक्रिय करू (_A) निष्क्रिय करू (_D) एहि संदेशकेँ फिनु कहियो नहि देखाबू (_D) रद्दी खाली करू (_E) लोड (_L) लोड कएल फाइल (_L): 