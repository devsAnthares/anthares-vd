;;; This file is part of the dictionaries-common package.
;;; It has been automatically generated.
;;; DO NOT EDIT!

;; Adding aspell dicts

(add-to-list 'debian-aspell-only-dictionary-alist
  '("brasileiro"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[---']"
     nil
     ("-d" "pt_BR")
     nil
     iso-8859-1))


;; Adding hunspell dicts

(add-to-list 'debian-hunspell-only-dictionary-alist
  '("english_american"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "en_US")
     nil
     utf-8))


;; Adding ispell dicts

(add-to-list 'debian-ispell-only-dictionary-alist
  '("brasileiro"
     "[a-z�����������������A-Z�����������������]"
     "[^a-z�����������������A-Z�����������������]"
     "[---']"
     t
     ("-d" "brasileiro")
     nil
     iso-8859-1))



;; An alist that will try to map aspell locales to emacsen names

(setq debian-aspell-equivs-alist '(
     ("pt_BR" "brasileiro")
))

;; Get default value for debian-aspell-dictionary. Will be used if
;; spellchecker is aspell and ispell-local-dictionary is not set.
;; We need to get it here, after debian-aspell-equivs-alist is loaded

(setq debian-aspell-dictionary (debian-ispell-get-aspell-default))



;; No emacsen-hunspell-equivs entries were found
