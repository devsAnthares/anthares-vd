%%% This file is part of the dictionaries-common package.
%%% It has been automatically generated.
%%% DO NOT EDIT!

#ifexists aspell_add_dictionary
  if (_slang_utf8_ok) {
    aspell_add_dictionary (
      "brasileiro",
      "pt_BR",
      "áéíóúàèìòùãõçüâêôÁÉÍÓÚÀÈÌÒÙÃÕÇÜÂÊÔ",
      "---'",
      "");
  } else {
  aspell_add_dictionary (
    "brasileiro",
    "pt_BR",
    "����������������������������������",
    "---'",
    "");
  }
#endif

#ifexists ispell_add_dictionary
  ispell_add_dictionary (
    "brasileiro",
    "brasileiro",
    "����������������������������������",
    "---'",
    "",
    "");
#endif
