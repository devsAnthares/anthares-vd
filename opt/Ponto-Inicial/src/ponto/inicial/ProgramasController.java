/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ponto.inicial;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class ProgramasController implements Initializable {

    @FXML private Button btn_instalar;
    
    @FXML private CheckBox ch_firefox;
    @FXML private CheckBox ch_chrome;
    @FXML private CheckBox ch_opera;
    @FXML private CheckBox ch_transmission;
    @FXML private CheckBox ch_kdeconnect;
    @FXML private CheckBox ch_cairo;
    @FXML private CheckBox ch_gedit;
    @FXML private CheckBox ch_kate;
    @FXML private CheckBox ch_spotify;
    @FXML private CheckBox ch_audacity;
    @FXML private CheckBox ch_stick;
    @FXML private CheckBox ch_libreoffice;
    @FXML private CheckBox ch_wps;
    @FXML private CheckBox ch_evince;
    @FXML private CheckBox ch_blender;
    @FXML private CheckBox ch_netbeans;
    @FXML private CheckBox ch_scenebuilder;
    @FXML private CheckBox ch_gparted;
    @FXML private CheckBox ch_synaptic;
    @FXML private CheckBox ch_htop;
    @FXML private CheckBox ch_systemmonitor;
    @FXML private CheckBox ch_tilix;
    @FXML private CheckBox ch_thunar;
    @FXML private CheckBox ch_steam;
    @FXML private CheckBox ch_playonlinux;
    @FXML private CheckBox ch_0ad;
    @FXML private CheckBox ch_virtualbox;
    @FXML private CheckBox ch_impressoras;
    @FXML private CheckBox ch_scanner;
    @FXML private CheckBox ch_nautilus;
    @FXML private CheckBox ch_stacer;
    @FXML private CheckBox ch_usage;
    @FXML private CheckBox ch_vlc;
    @FXML private CheckBox ch_clement;
    @FXML private CheckBox ch_kdenlive;
    @FXML private CheckBox ch_brasero;
    @FXML private CheckBox ch_cheese;
    @FXML private CheckBox ch_rhyt;
    @FXML private CheckBox ch_gimp;
    @FXML private CheckBox ch_inkscape;
    @FXML private CheckBox ch_kolourpaint;
    @FXML private CheckBox ch_gwenview;
    @FXML private CheckBox ch_kodi;
    
    String comando[] = {"apt install firefox-esr -y","bash /opt/Ponto-Inicial/src/ponto/inicial/programas/chrome/install.bash",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/opera/install.bash","apt install transmission -y","apt install kdeconnect -y",
        "apt install cairo-dock -y","apt install gedit -y","apt install kate -y",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/spotify/install.bash","apt install audacity -y",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/stick/install.bash","apt install libreoffice-writer libreoffice-impress libreoffice-calc -y",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/wps/install.bash","apt install evince -y",
        "apt install blender -y","bash /opt/Ponto-Inicial/src/ponto/inicial/programas/netbeans/install.bash",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/scenebuilder/install.bash",
        "apt install gparted -y","apt install synaptic -y","apt install htop -y",
        "apt install gnome-system-monitor -y","apt install tilix -y","apt install thunar -y",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/steam/install.bash","apt install playonlinux -y","apt install 0ad -y",
        "bash /opt/Ponto-Inicial/src/ponto/inicial/programas/virtualbox/install.bash","apt install system-config-printer -y",
        "apt install xsane -y","apt install nautilus -y","bash /opt/Ponto-Inicial/src/ponto/inicial/programas/stacer/install.bash",
        "apt install gnome-usage -y","apt install vlc -y","apt install clementine -y",
        "apt install kdenlive -y","apt install brasero -y","apt install cheese -y",
        "apt install rhythmbox -y","apt install gimp -y","apt install inkscape -y",
        "apt install kolourpaint4 -y","apt install gwenview -y","apt install kodi -y"};
    
    List<String> escolhas = new ArrayList<String>();
    String listaChk[] = {"ch_firefox","ch_chrome","ch_opera","ch_transmission",
        "ch_kdeconnect","ch_cairo","ch_gedit","ch_kate","ch_spotify","ch_audacity",
        "ch_stick","ch_libreoffice","ch_wps","ch_evince","ch_blender","ch_netbeans",
        "ch_scenebuilder","ch_gparted","ch_synaptic","ch_htop","ch_systemmonitor",
        "ch_tilix","ch_thunar","ch_steam","ch_playonlinux","ch_0ad","ch_virtualbox",
        "ch_impressoras","ch_scanner","ch_nautilus","ch_stacer","ch_usage","ch_vlc",
        "ch_clement","ch_kdenlive","ch_brasero","ch_cheese","ch_rhyt","ch_gimp",
        "ch_inkscape","ch_kolourpaint","ch_gwenview","ch_kodi"};
    String  instalar[]  = new String[43];
    String  comandoFinal = "";
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    void prepararInstalar(ActionEvent event) throws IOException {
            if (ch_firefox.isSelected()){escolhas.add(comando[0]);}
            if (ch_chrome.isSelected()){escolhas.add(comando[1]);}
            if (ch_opera.isSelected()){escolhas.add(comando[2]);}
            if (ch_transmission.isSelected()){escolhas.add(comando[3]);}
            if (ch_kdeconnect.isSelected()){escolhas.add(comando[4]);}
            if (ch_cairo.isSelected()){escolhas.add(comando[5]);}
            if (ch_gedit.isSelected()){escolhas.add(comando[6]);}
            if (ch_kate.isSelected()){escolhas.add(comando[7]);}
            if (ch_spotify.isSelected()){escolhas.add(comando[8]);}
            if (ch_audacity.isSelected()){escolhas.add(comando[9]);}
            if (ch_stick.isSelected()){escolhas.add(comando[10]);}
            if (ch_libreoffice.isSelected()){escolhas.add(comando[11]);}
            if (ch_wps.isSelected()){escolhas.add(comando[12]);}
            if (ch_evince.isSelected()){escolhas.add(comando[13]);}
            if (ch_blender.isSelected()){escolhas.add(comando[14]);}
            if (ch_netbeans.isSelected()){escolhas.add(comando[15]);}
            if (ch_scenebuilder.isSelected()){escolhas.add(comando[16]);}
            if (ch_gparted.isSelected()){escolhas.add(comando[17]);}
            if (ch_synaptic.isSelected()){escolhas.add(comando[18]);}
            if (ch_htop.isSelected()){escolhas.add(comando[19]);}
            if (ch_systemmonitor.isSelected()){escolhas.add(comando[20]);}
            if (ch_tilix.isSelected()){escolhas.add(comando[21]);}
            if (ch_thunar.isSelected()){escolhas.add(comando[22]);}
            if (ch_steam.isSelected()){escolhas.add(comando[23]);}
            if (ch_playonlinux.isSelected()){escolhas.add(comando[24]);}
            if (ch_0ad.isSelected()){escolhas.add(comando[25]);}
            if (ch_virtualbox.isSelected()){escolhas.add(comando[26]);}
            if (ch_impressoras.isSelected()){escolhas.add(comando[27]);}
            if (ch_scanner.isSelected()){escolhas.add(comando[28]);}
            if (ch_nautilus.isSelected()){escolhas.add(comando[29]);}
            if (ch_stacer.isSelected()){escolhas.add(comando[30]);}
            if (ch_usage.isSelected()){escolhas.add(comando[31]);}
            if (ch_vlc.isSelected()){escolhas.add(comando[32]);}
            if (ch_clement.isSelected()){escolhas.add(comando[33]);}
            if (ch_kdenlive.isSelected()){escolhas.add(comando[34]);}
            if (ch_brasero.isSelected()){escolhas.add(comando[35]);}
            if (ch_cheese.isSelected()){escolhas.add(comando[36]);}
            if (ch_rhyt.isSelected()){escolhas.add(comando[37]);}
            if (ch_gimp.isSelected()){escolhas.add(comando[38]);}
            if (ch_inkscape.isSelected()){escolhas.add(comando[39]);}
            if (ch_kolourpaint.isSelected()){escolhas.add(comando[40]);}
            if (ch_gwenview.isSelected()){escolhas.add(comando[41]);}
            if (ch_kodi.isSelected()){escolhas.add(comando[42]);}
            
            // Concatena todos os comandos em uma String
            comandoFinal = Arrays.toString(escolhas.toArray()); 
            
            // localização
            java.io.File diretorio = new java.io.File("/opt/Ponto-Inicial/");
            System.out.println("criando arquivo-teste em /opt");
            java.io.File arquivo = new java.io.File(diretorio, "installProg.bash");
            // criando arquivo teste
            try {
                boolean statusArq = arquivo.createNewFile();
                System.out.print(statusArq);
            } catch (IOException e) {
                e.printStackTrace(); 
            }
            FileWriter arq = new FileWriter("/opt/Ponto-Inicial/installProg.bash");
            PrintWriter gravarArq = new PrintWriter(arq);
            System.out.println("inserindo dados...");
            gravarArq.printf("#!/bin/bash\n\n"
                    + "cp /opt/Ponto-Inicial/src/sources.list /etc/apt/\n"
                    + "apt update;\n"
                    + comandoFinal+"\n"
                            + "apt remove --purge calamares systemback -y");
            System.out.println("FECHANDO arquivo");
            arq.close();
        System.out.println("chamando tela");
        Parent tela = null;
        tela = FXMLLoader.load(getClass().getResource("tela/Dicas.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(tela);
        stage.setScene(scene);
        stage.show();
        
        stage = (Stage) btn_instalar.getScene().getWindow();
        stage.close();
    }
    
    @FXML
    void fecharTela() throws IOException{
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Fechar.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(IntroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Stage stage = new Stage();
        Scene scene = new Scene(tela);
        stage.setScene(scene);
        stage.show();
        
    }
    
}
