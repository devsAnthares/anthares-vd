#!/bin/bash

# Preparar instalação
sudo apt install curl -y

# 1. Adicionar repository do Spotify
sudo curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -
sudo echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

# 2. Atualiza a lista e Instala o Spotify
sudo apt-get update && sudo apt-get install spotify-client -y
