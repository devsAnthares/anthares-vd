/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ponto.inicial;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class TermoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML private Button btn_aceitar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    void fecharPrograma(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/FecharTermo.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(IntroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Stage stage = new Stage();
        Scene scene = new Scene(tela);
        stage.setScene(scene);
        stage.show();
                
    }
    
    @FXML
    void avancar(){
        Parent tela = null;
        try {
            tela = FXMLLoader.load(getClass().getResource("tela/Programas.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(IntroController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Stage stage = new Stage();
        Scene scene = new Scene(tela);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
        
        stage = (Stage) btn_aceitar.getScene().getWindow();
        stage.close();
                
    }
    
}
