/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ponto.inicial;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class DicasController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML private Button btn_concluir;
    @FXML private Button btn_ok;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    void ok(){
        String command = "bash /opt/Ponto-Inicial/instalar.bash";
        String[] cmd = {"/bin/sh", "-c", command};
        Runtime r = Runtime.getRuntime();
        Process p = null;
        
        try {
            p = r.exec(cmd);
        } catch (IOException ex) {
            Logger.getLogger(DicasController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Scanner scanner = new Scanner(p.getInputStream());
        String resultado = scanner.next();
    }
    
    @FXML
    void concluir() throws IOException{
        Runtime r = Runtime.getRuntime();
        Process p = r.exec(new String[]{"/bin/sh", "-c", "bash /opt/Ponto-Inicial/desinstalar.bash"   });
        Scanner scanner = new Scanner(p.getInputStream());
        String resultado = scanner.next();
        System.exit(0);
    }
    
}
